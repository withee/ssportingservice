<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/js/update_lang.js?timestamp=<?php echo time()?>"></script>
    </head>
    <body>
    <body>
        <h2><a href="/com.php">กลับหน้าประเทศทั้งหมด</a></h2>
        <h1>ลีกทั้งหมด</h1>
        <table>
            <thead>
                <tr>
                    <td>ชื่อภาษาอังกฤษ</td>
                    <td>ชื่อภาษาไทย</td>

                </tr>
            </thead>
            <tbody>
                <?php
                $competitionsId = $_GET['competitionId'];
                require 'DBConfig.php';
                $db = DBConfig::getConnection();
                $sql = "select league.leagueName as leagueName,lang_league.leagueNameTh,league.leagueId
from league 
left join lang_league on lang_league.leagueId = league.leagueId
where league.competitionId=$competitionsId order by league.leagueName ASC";
                //echo $sql;exit;
                $stmt = $db->query($sql);
                $list = $stmt->fetchAll(PDO::FETCH_OBJ);
                //echo count($list);
                foreach ($list as $obj) {
                    ?>
                    <tr>
                        <td><?php echo $obj->leagueName ?></td>
                        <td><input type="text" value="<?php echo $obj->leagueNameTh ?>"  class="input-update" table="lang_league" attr="leagueName" attrValue="<?php echo $obj->leagueName ?>" attrTh="leagueNameTh" leagueId="<?php echo $obj->leagueId?>" competitionId="<?php echo $competitionsId?>"/></td>

                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </body>
</html>
