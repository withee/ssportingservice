<?php
require '../Slim/Slim.php';
require '../TotalStatement.php';

$app = new Slim();
$app->contentType('application/json; charset=utf-8');

$app->post('/exchange','exchange');
$app->get('/total_statement','total_statement');
$app->get('/reset_sgold/:id','rest_sgold');
$app->post('/deposit','deposit');
$app->post('/withdraw','withdraw');
$app->post('/exchangeDiamond','exchangeDiamond');
$app->get('/exchangeStepBonus/:id','exchangeStepBonus');
$app->run();
date_default_timezone_set('UTC');

function exchangeStepBonus($uid){
    $t = new TotalStatement();
    $config = json_decode(file_get_contents('../system_config/file.json'));

    $user=$t->getUser($uid);
    $data =array();
    $before_sgold=$user->sgold;
    $before_scoin=$user->scoin;
    $before_diamond = $user->diamond;
    $sgold_income=0;
    $scoin_income=0;
    $scoin_outcome=0;
    $diamond_outcome=0;
    $sgold_outcome=0;
    $balance_sgold = $before_sgold;
    $balance_scoin = $before_scoin;
    if($user->step_combo_count>=3){
        $db = DBConfig::getConnection();
        $step_bonus =objectToArray($config->step_combo_continues);
//        print_r($step_bonus);
//        echo $step_bonus[8];exit;
        $diamond_income=$step_bonus[$user->step_combo_count];
        $balance_diamond = $before_diamond+$diamond_income;
        $sql=$t->getTotalStatementQuery($uid,'step_bonus',"'step_bonus_total'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,'success',"NULL","NULL",'',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
        $sql_update_facebook ="update facebook_user set step_combo_count=0,diamond=$balance_diamond where uid = $uid";
        $db->exec($sql);
        $db->exec($sql_update_facebook);
        $data['status']='success';
        $data['description']='แลกคอมโบตื่อเนื่องสำเร็จได้ '.$diamond_income .' diamond';
        $data['balance_sgold']=$before_sgold;
        $data['balance_scoin']=$before_scoin;
        $data['balance_diamond']=$balance_diamond;

    }else{
        $data['status']='fail';
        $data['description']='ต้องมีคอมโบต่อเนื่องอย่างน้อย 3 คู่ขึ้นถึงจะสามารถแลกได้';
        $data['balance_sgold']=$before_sgold;
        $data['balance_scoin']=$before_scoin;
        $data['balance_diamond']=$before_diamond;
    }
    echo json_encode($data);
}
function exchangeDiamond(){
    $t = new TotalStatement();
    $config = json_decode(file_get_contents('../system_config/file.json'));
    $ap = new Slim();
    $uid = $ap->request()->params('uid');
    $diamond = $ap->request()->params('diamond');
    $sgold_to_diamond = $config->sgold_to_diamond;
    $sgold_outcome=$diamond*$sgold_to_diamond;

    $user=$t->getUser($uid);
    $before_sgold=$user->sgold;
    $before_scoin=$user->scoin;
    $before_diamond = $user->diamond;
    $sgold_income=0;
    $scoin_income=0;
    $scoin_outcome=0;
    $diamond_income=$diamond;
    $diamond_outcome=0;

    $data =array();
    if($sgold_outcome<=$before_sgold){
        $db = DBConfig::getConnection();
        $balance_sgold = $before_sgold-$sgold_outcome;
        $balance_scoin = $before_scoin;
        $balance_diamond = $before_diamond+$diamond;
        $sql=$t->getTotalStatementQuery($uid,'exchange',"'sgold_to_diamond'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,'success',"NULL","NULL",'',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
        $sql_update_facebook ="update facebook_user set sgold=$balance_sgold,diamond=$balance_diamond where uid = $uid";
        $db->exec($sql);
        $db->exec($sql_update_facebook);
        $data['status']='success';
        $data['description']='แลกสำเร็จได้ '.$diamond_income.' diamond';
        $data['balance_sgold']=$balance_sgold;
        $data['balance_scoin']=$balance_scoin;
        $data['balance_diamond']=$balance_diamond;

    }else{
        $data['status']='fail';
        $data['description']='Sgold ไม่พอสำหรับแลก';
        $data['balance_sgold']=$before_sgold;
        $data['balance_scoin']=$before_scoin;
        $data['balance_diamond']=$before_diamond;

    }
    echo json_encode($data);

}

function deposit(){
    $t = new TotalStatement();
    $ap = new Slim();
    $uid = $ap->request()->params('uid');
    $sgold_income =$ap->request()->params('sgold_income');
    $scoin_income = $ap->request()->params('scoin_income');
    $diamond_income = $ap->request()->params('diamond_income');

    $sgold_income=$sgold_income?$sgold_income:0;
    $scoin_income=$scoin_income?$scoin_income:0;
    $diamond_income =$diamond_income?$diamond_income:0;
    $db = DBConfig::getConnection();
    $user=$t->getUser($uid);
    $before_sgold=$user->sgold;
    $before_scoin=$user->scoin;
    $before_diamond = $user->diamond;
    $sgold_outcome=0;
    $scoin_outcome=0;
    $diamond_outcome=0;
    $balance_sgold = $before_sgold+($sgold_income-$sgold_outcome);
    $balance_scoin = $before_scoin+($scoin_income-$scoin_outcome);
    $balance_diamond = $before_diamond +($diamond_income-$diamond_outcome);
    $statement_type='deposit';
    $type_addition='NULL';
    $total_statement_sql=$t->getTotalStatementQuery($uid,$statement_type,$type_addition,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,'success','null','null','',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
    $db->query($total_statement_sql);
    $update_facebook_user_sql =$t->getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin,$balance_diamond);
    $db->query($update_facebook_user_sql);
    echo json_encode(array(
        'status'=>'success',
        'balance_sgold'=>$balance_sgold,
        'balance_scoin'=>$balance_scoin,
        'balance_diamond'=>$balance_diamond,
    ));
}
function withdraw(){
    $t = new TotalStatement();
    $ap = new Slim();
    $uid = $ap->request()->params('uid');
    $sgold_outcome =$ap->request()->params('sgold_outcome');
    $scoin_outcome = $ap->request()->params('scoin_outcome');
    $diamond_outcome = $ap->request()->params('diamond_outcome');
    $db = DBConfig::getConnection();
    $user=$t->getUser($uid);
    $before_sgold=$user->sgold;
    $before_scoin=$user->scoin;
    $before_diamond = $user->diamond;
    $sgold_outcome=$sgold_outcome?$sgold_outcome:0;
    $scoin_outcome = $scoin_outcome?$scoin_outcome:0;
    $diamond_outcome = $diamond_outcome?$diamond_outcome:0;
    $sgold_income=0;
    $scoin_income=0;
    $diamond_income=0;
    $balance_sgold = $before_sgold+($sgold_income-$sgold_outcome);
    $balance_scoin = $before_scoin+($scoin_income-$scoin_outcome);
    $balance_diamond = $before_diamond+($diamond_income-$diamond_outcome);

    $statement_type='withdraw';
    $type_addition='NULL';
    $total_statement_sql=$t->getTotalStatementQuery($uid,$statement_type,$type_addition,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,'success','null','null','',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
    //echo $total_statement_sql;exit;
    $db->query($total_statement_sql);
    $update_facebook_user_sql =$t->getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin,$balance_diamond);
    $db->query($update_facebook_user_sql);
    echo json_encode(array(
        'status'=>'success',
        'balance_sgold'=>$balance_sgold,
        'balance_scoin'=>$balance_scoin,
        'balance_diamond'=>$balance_diamond,
    ));
}
function total_statement(){
    $ap = new Slim();
    $uid = $ap->request()->params('uid');
    $statement_type=$ap->request()->params('statement_type');
    $db = DBConfig::getConnection();
    if($statement_type){
        $sql ="select s.*,f.fb_uid,f.display_name from total_statement  s
    left join facebook_user as f  on f.uid = s.uid
    where s.uid =$uid and  s.statement_type='$statement_type' and   date(s.statement_timestamp)>= current_date - interval 7 day
    order by s.id ASC
    ";
    }else{
        $sql ="select s.*,f.fb_uid,f.display_name from total_statement  s
    left join facebook_user as f  on f.uid = s.uid
    where s.uid =$uid and  date(s.statement_timestamp)>= current_date - interval 7 day
    order by s.id ASC
    ";
    }

    $list=$db->query($sql)->fetchAll(PDO::FETCH_OBJ);
    echo json_encode($list);

}
function exchange(){

    $t = new TotalStatement();
    $config = json_decode(file_get_contents('../system_config/file.json'));
    $exchange_rate = $config->exchange_rate;
    $ap = new Slim();
    $exchange_array = array('sgold_to_scoin','scoin_to_sgold');
    $uid = $ap->request()->params('uid');
    $type = $ap->request()->params('type');//0 = sgold to scoin  1= scoin to sgold
    $amount = $ap->request()->params('amount');
    $user=$t->getUser($uid);
    $data =array(
        'result'=>'success',
        'description'=>'แลกสำเร็จ',
        'balance_sgold'=>null,
        'balance_scoin'=>null,
        'balance_diamond'=>null,
        'sgold'=>null,
        'scoin'=>null,
    );
    $sgold_income=0;
    $sgold_outcome=0;
    $scoin_income=0;
    $scoin_outcome=0;
    $sgold=0;
    $scoin=0;
    $before_sgold = $user->sgold;
    $before_scoin = $user->scoin;
    if($type==0){
        if($amount<=$before_sgold){

            $scoin_income = $amount*$exchange_rate[0][1];
            $sgold_outcome = $amount;
            $sgold= $sgold_outcome;
            $scoin= $scoin_income;

        }else{
            $data['result']='fail';
            $data['description']='Sgold ไม่พอสำหรับแลก';
        }
    }else if($type==1){
        if($amount*$exchange_rate[1][1]<=$before_scoin){
            $sgold_income = $amount;
            $scoin_outcome = $amount * $exchange_rate[1][1];
            $sgold=$sgold_income;
            $scoin =$scoin_outcome;
        }else{
            $data['result']='fail';
            $data['description']='Scoin ไม่พอสำหรับแลก';
        }
    }else{
        $data['result']='type_is_wrong';
        $data['description']='Type is wrong.';
    }

    if($sgold_outcome>0 || $scoin_outcome>0){
        $data['result']='success';

        $statement_type='exchange';
        $type_addition = $exchange_array[$type];
        $db = DBConfig::getConnection();
        $balance_sgold = $before_sgold+($sgold_income-$sgold_outcome);
        $balance_scoin = $before_scoin+($scoin_income-$scoin_outcome);
        $before_diamond = $user->diamond;
        $diamond_income=0;
        $diamond_outcome=0;
        $balance_diamond = $before_diamond + ($diamond_income+$diamond_outcome);
        $total_statement_sql=$t->getTotalStatementQuery($uid,$statement_type,"'$type_addition'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$data['result'],'null','null','',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
        $db->query($total_statement_sql);
        $update_facebook_user_sql =$t->getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin);
        $db->query($update_facebook_user_sql);
        $data['balance_sgold']=$balance_sgold;
        $data['balance_scoin']=$balance_scoin;
        $data['balance_diamond']=$balance_diamond;
        $data['scoin']=$scoin;
        $data['sgold']=$sgold;
    }else{
        $data['balance_sgold']=$before_sgold;
        $data['balance_scoin']=$before_scoin;
        $data['balance_diamond']=$user->diamond;
    }
    echo json_encode($data);
}
function rest_sgold($uid){
    $t = new TotalStatement();
    if($t->resetSgold($uid)){
        echo json_encode(array('status'=>'success','description'=>'รีเซ็ต Sgold สำเร็จ'));
    }else{
        echo json_encode(array('status'=>'fail','description'=>'ยอด Sgold ต้องน้อยกว่าหรือเท่ากับ -3000 ถึงจะรีเซ็ตได้ และรีเซ็ตได้อาทิตย์ละครั้ง'));
    }
}
function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}

