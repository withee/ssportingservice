var language = ['big', 'en', 'gb', 'kr', 'th', 'vn', 'la'];
$(document).ready(function() {
    $('a.7m').click(function() {
        var leagueId = $(this).attr('leagueId');
        var leagueName = $(this).attr('leagueName');
        var areaId = $(this).attr('id7m');
        $('#id7m').attr('leagueId', leagueId);
        $('#leagueName').html(leagueName);
        $('#myModal').modal();
        var all = $('input[name="all"]').prop('checked');
        var all_league = $('input[name="all_league"]').prop('checked');
        all_league = all_league == true ? 2 : '';

        if (all_league == 2) {
            all = 2;
        } else {
            all = all == true ? 1 : '';
        }

        $.get('../../../league/getLeagueList/areaId/' + areaId + '/all/' + all, function(response) {
            $('#id7m').html(response);
        });
    });
    $('.closeModal').click(function() {
        $('#myModal').modal('hide');
    });
    $('#saveBt').on('click', function() {
        var self = $('#id7m');
        var id7m = self.val();
        var leagueId = self.attr('leagueId');
        $.post('../../../league/save', {
            id7m: id7m, 
            leagueId: leagueId
        }, function(response) {
            if (response) {
                $('#myModal').modal('hide');
                for (var i = 0; i < language.length; i++) {
                    $('#' + language[i] + '-' + leagueId).val(response[i]);
                }
            } else {
                alert('บันทึกไม่สำเร็จ');
            }
        }, 'json');

    });
    //delete 7m mapping
    $('.removeLeague7m').click(function() {
        var id7m = $(this).attr('id7m');
        if (id7m) {
            var name = $(this).attr("leagueName");
            var r = confirm("คุณต้องการลบลีก " + name + " ใช่ไหม");
            if (r) {
                $.get('../../../league/remove', {
                    id7m: id7m
                }, function(response) {
                    if (response) {
                        alert('ลบสำเร็จ');
                        window.location.reload();

                    } else {
                        alert('ลบไม่สำเร็จ,ลองใหม่อีกครั้ง');
                    }
                });
            }
        }
    });
    
    $(".saveLeagueLang").click(function(){
        var agree = confirm("ต้องการบันทึกหรือไม่");
        var leagueid = $(this).attr("leagueid");
        var leaguename = $(this).attr("leaguename");
        var id7m = $(this).attr("id7m");
        var comid = $(this).attr("comid");
        var lngList = {};
        var hasempty = false;
        if(agree){
            $.each(language,function(key,val){             
                // console.log(key+":"+val);
                if($("#"+val+"-"+leagueid).val()){
                    lngList[val] = $("#"+val+"-"+leagueid).val();
                //     console.log(lngList[val]);
                }else{
                    hasempty=true;                    
                }
            });
            //   console.log(hasempty);
        
            if(hasempty){
                alert("กรอกข้อมูลไม่ครบ");
            }else{
                var res = $.ajax({
                    url:"/league/AddLeagueLng",
                    type:"POST",
                    data:{
                        leagueid:leagueid,
                        leaguename:leaguename,
                        lnglist:JSON.stringify(lngList),
                        id7m:id7m,
                        comid:comid
                    },
                    dataType:"JSON"
                });
                res.success(function(res){
                    console.log(res);
                    if(res.success){
                        alert(res.desc);
                        location.reload();
                    }else{
                        alert(res.desc);
                    }
                });
            }
        }
    });
    
});
function setLeaguePriority(leagueId, priority) {

    var input = prompt('คุณต้องการเปลี่ยนลำดับของ leagueId ' + leagueId + ' ใช่ไหม เลขน้อยไปหามาก', priority);
    if (input) {
        $.post('../../../league/setLeaguePriority/leagueId/' + leagueId + '/leaguePriority/' + input, function(response) {
            if (response) {
                window.location.reload();
            }
        });
    }
}

