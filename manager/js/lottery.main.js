$(function() {
    $('#file_upload').uploadify({
        'swf'      : '/jquery.uploadify/jquery.uploadify.swf',
        'uploader' : '/lottery/uploadFile',
        'onUploadSuccess' : function(file,data,response) {

            $('#showImg').html('<img width="100" height="100" src="'+data+'"/>');
            $('#img').val(data);
        }
        // Put your options here
    });
    $('#submit').click(function(){
        var param ={
            title:$('#title').val(),
            desc:$('#desc').val(),
            content:$('#content').val(),
            img : $('#img').attr('src')
        }

    });

});
CKEDITOR.on('instanceReady', function(e) {
    // First time
    e.editor.document.getBody().setStyle('background-color', 'black');
    //e.editor.document.getBody().setStyle('color','white');
    // in case the user switches to source and back
    e.editor.on('contentDom', function() {
        e.editor.document.getBody().setStyle('background-color', 'black');
        //e.editor.document.getBody().setStyle('color','white');
    });
});