/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    var selectCondition = [];
    var conditionAll=[];
    var dataShowAll;
    var conditionAr = ['H2H Home Hdp' , 'H2H Away Hdp' , 'Home Hdp','Away Hdp'];
    var dmy  = new Date();
    var d = dmy.getDate();
    var m = dmy.getMonth();
    var y = dmy.getYear();
    var month = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
    
    //show date  
    $('.showSelectDate').html(function(){
        var strHtml = '<option value="'+d+'"> '+d+'</option>';
        for(var i=1;i<=31;i++){
            if(i<10)
                strHtml += '<option value="0'+i+'"> '+i+' </option>';
            else
                strHtml += '<option value="'+i+'"> '+i+' </option>';
        }
        return strHtml;
    }); // end show date


    //show month
    $('.showSelectMonth').html(function(){
        var strHtml="";
        if(m<9)
            strHtml = '<option value="0'+(m+1)+'"> '+month[m]+' </option>';
        else
            strHtml = '<option value="'+(m+1)+'"> '+month[m]+' </option>';
        for(var i=0;i<12;i++){
            if(i<9)
                strHtml += '<option value="0'+(i+1)+'"> '+month[i]+' </option>';
            else
                strHtml += '<option value="'+(i+1)+'"> '+month[i]+' </option>';
        }
        return strHtml;
    }); // end show month
    
    //show year
    $('.showSelectYear').html(function(){
        var strHtml = '<option value="'+(y+1900)+'"> '+(y+2443)+' </option>';
        for(var i=114;i<=y;i++){
            strHtml += '<option value="'+(y+1900)+'"> '+(y+2443)+' </option>';
        }
        return strHtml;
    }); // end show year
    
    //show conditon
    $('#selectCon').html(function(){
        var strHtml="";
        for(var i=0;i< conditionAr.length;i++){
            strHtml+= '<option value ="'+i+'">'+conditionAr[i]+'</option>';
        }
        return strHtml;
    });
        
    //search match
    $('.searchMatch').click(function(){
        var date = $('#selectDate').val();
        var month = $('#selectMonth').val();
        var year = $('#selectYear').val();
        var ymd = year+"-"+month+"-"+date;
        $('#showMatchList').html(' ');   
        conditionAll=[];
        selectCondition=[];
        var res = $.ajax({
            url:"/analyze/GetMatch",
            type:"POST",
            data:{
                ymd:ymd
            },
            dataType:"JSON"
        });
        res.success(function(res){  
            var list = jQuery.parseJSON(res.datas);
            dataShowAll=list;
            for(var i in list){
                showDataCondition(list[i],0,0);
            }
            $('#showConditionAdd').html(' ');
            
        });
    });//end select match
        
    //click add condition and show match condition   
    $('.addCondition').click(function(){        
        var condition = $('#selectCon').val();
        var symbol = $('#selectSym').val();
        var number = $('#inputNum').val();
        var countMatch = $('#inputCountMatch').val();
        if(countMatch==null || countMatch==''){
            countMatch=0;
        }       
        var countMatchToTalHdp = $('#inputCountMatchDHP').val();
        if(countMatchToTalHdp==null||countMatchToTalHdp==''){
            countMatchToTalHdp=0;
        }
        if(condition=="" || symbol=="" || number==""){
            alert('input data !!');
        }else{
            var strHtml = "<tr>";
            if(countMatch==0){
                strHtml+="<td>"+conditionAr[condition]+" "+symbol+" "+number+"%</td>";
            }else{
                strHtml+="<td>"+conditionAr[condition]+" "+symbol+" "+number+"% จำนวน h2h hdp "+countMatch+" match ล่าสุด & จำนวน total hdp เหย้า/เยือน "+countMatchToTalHdp+" match ล่าสุด </td>";
            }
            strHtml+="<td>show</td>";
            strHtml += "</tr>";            
            $('#showConditionAdd').append(strHtml);
            selectCondition.push(condition);
            compHDP(condition,symbol,number,countMatch,countMatchToTalHdp);
        }
    });//end click add condition
              
    
    function compHDP(condition,symbol,number , countMatch , countMatchToTalHdp){
        var idA,idB,arid,scoreA,scoreB,hdp,sumHDPA,sumHDPB;
        var boxData=[];
        $('#showMatchList').html(' ');
        if(condition==0){
            for(var i in dataShowAll){
                idA = dataShowAll[i].d_ai;
                arid = jQuery.parseJSON(dataShowAll[i].d_wf_ai);
                scoreA = jQuery.parseJSON(dataShowAll[i].d_wf_Ascore);
                scoreB = jQuery.parseJSON(dataShowAll[i].d_wf_Bscore);
                hdp = jQuery.parseJSON(dataShowAll[i].d_wf_rq);
                if(hdp!=null){
                    sumHDPA=getHdpCountMatch(idA,arid,scoreA,scoreB,hdp,countMatch);
                    if(symbol==">"){
                        if(number<sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }                            
                    }else if(symbol=="<"){
                        if(number>sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }
                    }else{
                        if(number==sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);    
                        }
                    }
                }
            }    
        }
        else if(condition==1){
            for(var i in dataShowAll){
                idB = dataShowAll[i].d_bi;
                arid = jQuery.parseJSON(dataShowAll[i].d_wf_ai);
                scoreA = jQuery.parseJSON(dataShowAll[i].d_wf_Ascore);
                scoreB = jQuery.parseJSON(dataShowAll[i].d_wf_Bscore);
                hdp = jQuery.parseJSON(dataShowAll[i].d_wf_rq);
                if(hdp!=null){
                    sumHDPB=getHdpCountMatch(idB,arid,scoreA,scoreB,hdp,countMatch);
                    if(symbol==">"){
                        if(number<sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }                            
                    }else if(symbol=="<"){
                        if(number>sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }
                    }else{
                        if(number==sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);   
                        }
                    }
                }
            }
            
        }else if(condition==2){
            for(var i in dataShowAll){
                idA = dataShowAll[i].d_ai;
                idB = dataShowAll[i].d_bi;
                arid = jQuery.parseJSON(dataShowAll[i].a_th_tai);
                scoreA = jQuery.parseJSON(dataShowAll[i].a_th_as);
                scoreB = jQuery.parseJSON(dataShowAll[i].a_th_bs);
                hdp = jQuery.parseJSON(dataShowAll[i].a_th_rq);       
                if(hdp!=null){
                    sumHDPA=getHdp(idA,arid,scoreA,scoreB,hdp,countMatchToTalHdp);
                    if(symbol==">"){
                        if(number<sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }                            
                    }else if(symbol=="<"){
                        if(number>sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }
                    }else{
                        if(number==sumHDPA){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);   
                        }
                    }
                }
            }
        }else if(condition==3){
            for(var i in dataShowAll){
                idA = dataShowAll[i].d_ai;
                idB = dataShowAll[i].d_bi;
                arid = jQuery.parseJSON(dataShowAll[i].b_th_tai);
                scoreA = jQuery.parseJSON(dataShowAll[i].b_th_as);
                scoreB = jQuery.parseJSON(dataShowAll[i].b_th_bs);
                hdp = jQuery.parseJSON(dataShowAll[i].b_th_rq);       
                if(hdp!=null){
                    sumHDPB=getHdp(idB,arid,scoreA,scoreB,hdp,countMatchToTalHdp);
                    if(symbol==">"){
                        if(number<sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }                            
                    }else if(symbol=="<"){
                        if(number>sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch,countMatchToTalHdp);
                        }
                    }else{
                        if(number==sumHDPB){
                            boxData.push(dataShowAll[i]);
                            showDataCondition(dataShowAll[i],countMatch ,countMatchToTalHdp);   
                        }
                    }
                }
            }
        }else if(condition==4){
        }
      
        dataShowAll=boxData;
        conditionAll.push(boxData);
        
    }
        
    //get hdp 
    function getHdp(idH,idA,scroeA,scoreB,hdp ,count){
        var scoreWin=[];
        var w=0;
        var l=0;
        var d=0;
        var number;  
        var countx = count;
        if(count==0){
            count=hdp.length;
        }else{
            count=checkIndexNullarray(hdp,count);
        }
        for(var i =0;i<hdp.length&&i<count ;i++){
            if(hdp[i]!=null& hdp[i]!=""){
                number  = parseFloat(scroeA[i]) + parseFloat(hdp[i]) - parseFloat(scoreB[i]);
                scoreWin.push(number);
            }else{
                scoreWin.push(null); 
            }
        }
        for(var i=0;i<scoreWin.length;i++){
            if(scoreWin[i]!=null){
                number = parseFloat(scoreWin[i]);
                if(idH==idA[i]){
                    if(number>0){
                        w++;
                    }else if(number<0&&number!=-0){
                        l++;
                    }else{
                        d++;
                    }
                }else{
                    if(number>0){
                        l++;
                    }else if(number<0 && number!=-0){
                        w++;
                    }else{
                        d++;
                    }
                }
            }
        }
        if(w+l+d==0)
            return 0;
        else
            return ((w*100)/(w+l+d)).toFixed(2);
            
    }//end function gethdp
    
    //get hdp count match 
    function getHdpCountMatch(idH,idA,scroeA,scoreB,hdp,count){
        var scoreWin=[];
        var w=0;
        var l=0;
        var d=0;
        var number;
        if(count==0){
            count=hdp.length;
        }else{
            count=checkIndexNullarray(hdp,count);
        }
        for(var i =0;i<hdp.length&&i<count ;i++){
            if(hdp[i]!=null& hdp[i]!=""){
                number  = parseFloat(scroeA[i]) + parseFloat(hdp[i]) - parseFloat(scoreB[i]);
                scoreWin.push(number);
            }else{
                scoreWin.push(null); 
            }
        }
        
        for(var i=0;i<scoreWin.length;i++){
            if(scoreWin[i]!=null){
                number = parseFloat(scoreWin[i]);
                if(idH==idA[i]){
                    if(number>0){
                        w++;
                    }else if(number<0&&number!=-0){
                        l++;
                    }else{
                        d++;
                    }
                }else{
                    if(number>0){
                        l++;
                    }else if(number<0 && number!=-0){
                        w++;
                    }else{
                        d++;
                    }
                }
            }
        }
        if(w+l+d==0)
            return 0;
        else
            return ((w*100)/(w+l+d)).toFixed(2);
            
    }//end function gethdp count match
    
    
    //get h2h 
    function getH2H(idH,idA,scroeA,scoreB , count){
        var scoreWin=[];
        var w=0;
        var l=0;
        var d=0;
        var number;
        if(count==0){
            count=scroeA.length;
        }
        for(var i =0;i<scroeA.length&&i<count ;i++){
            if(scroeA[i]!=null){
                number  = parseFloat(scroeA[i]) - parseFloat(scoreB[i]);
                scoreWin.push(number);
            }else{
                scoreWin.push(null); 
            }
        }
        for(var i=0;i<scoreWin.length;i++){
            if(scoreWin[i]!=null){
                number = parseFloat(scoreWin[i]);
                if(idH==idA[i]){
                    if(number>0){
                        w++;
                    }else if(number<0){
                        l++;
                    }else{
                        d++;
                    }
                }else{
                    if(number>0){
                        l++;
                    }else if(number<0){
                        w++;
                    }else{
                        d++;
                    }
                }
            }
        }
        if(w+l+d==0)
            return 0;
        else
            return ((w*100)/(w+l+d)).toFixed(2);
            
    }//end function get h2h 
    
    //get score Hdp H2H
    function getScoreHdpH2H(idH,idA,scroeA,scoreB,hdp,count){
        var score=0;
        var match=0;
        if(count==0 &&hdp!=null){
            count=hdp.length;
        }
        for(var i =0;hdp!=null&&i<hdp.length&&i<count;i++){
            if(hdp[i]!=null& hdp[i]!=""){
                match++;
                if(idH==idA[i]){
                    score+=parseFloat(scroeA[i]);                 
                }else{
                    score+=parseFloat(scoreB[i]);
                }
            }
        }
        if(score==0 || match==0)
            return 0;
        else
            return (score/match).toFixed(2);          
    }//end function getScoreHdpH2H
    
    //get score Hdp H2H
    function getScoreHdp(idH,idA,scroeA,scoreB , count){
        var score=0;
        var match=0;
        if(count==0){
            count= scroeA.length;
        }
        for(var i =0;i<scroeA.length && i<count ;i++){
            if(scroeA[i]!=null& scroeA[i]!=""){
                match++;
                if(idH==idA[i]){
                    score+=parseFloat(scroeA[i]);                 
                }else{
                    score+=parseFloat(scoreB[i]);
                }
            }
        }
        if(score==0 || match==0)
            return 0;
        else
            return (score/match).toFixed(2);          
    }//end function getScoreHDP
    
    //check list null
    function checkArray(list){
        var count =0;
        if(list==null){
            return 0;
        }else{
            for(var i =0;i<list.length;i++){
                if(list[i]!='')
                    count++
            }
            return count;
        }
    }//end list null
    //check array null get index not null
    function checkIndexNullarray(list , max){
        var count=0;
        var returnCount=0;
        if(checkArray(list)<max){
            return list.length ;
        }else{
            for(var i = 0;i<list.length;i++){
                if(list[i]!=''){
                    count++
                }
                if(count>=max){
                    returnCount=i;
                    i=list.length+10;
                }               
            }
            return returnCount+1;
        }
    }//end array null
        
    //show data Condition()
    function showDataCondition(list,count,countHdp){
        var idA,idB ,arid , scoreA , scoreB , hdp ,sumHDPA,sumHDPB;
            
        var totalHHDP  ,totalHA, totalHScoreA , totalHScoreB , sumTotalHHDP;
        var totalAHDP  ,totalAA, totalAScoreA , totalAScoreB , sumTotalAHDP;           
        var strHtml = "";
        
        var rq='';
        var sc='';
        // set color
        var sc1='',sc2='',sc3='',sc4='';
        for(var i=0;i<selectCondition.length;i++){
            if(selectCondition[i]==0){
                sc1='background-color: #FFFF99;';
            }else if(selectCondition[i]==1){
                sc2='background-color: #FFFF99;';
            }else if(selectCondition[i]==2){
                sc3='background-color: #FFFF99;';
            }else if(selectCondition[i]==3){
                sc4='background-color: #FFFF99;';
            }
        }
        
        //hdp h2h
        idA = list.d_ai;
        idB = list.d_bi;
        arid = jQuery.parseJSON(list.d_wf_ai);
        scoreA = jQuery.parseJSON(list.d_wf_Ascore);
        scoreB = jQuery.parseJSON(list.d_wf_Bscore);
        hdp = jQuery.parseJSON(list.d_wf_rq);
        // total team home
        totalHA = jQuery.parseJSON(list.a_th_tai);
        totalHScoreA = jQuery.parseJSON(list.a_th_as);
        totalHScoreB = jQuery.parseJSON(list.a_th_bs);
        totalHHDP  = jQuery.parseJSON(list.a_th_rq);
        // total team away
        totalAA = jQuery.parseJSON(list.b_th_tai);
        totalAScoreA = jQuery.parseJSON(list.b_th_as);
        totalAScoreB = jQuery.parseJSON(list.b_th_bs);
        totalAHDP  = jQuery.parseJSON(list.b_th_rq);
        checkArray(hdp);
        if(hdp!=null && checkArray(hdp)>=count){                
            strHtml+='<tr class ="selectMatch" attrmid="'+list.match_id+'" attrdate="'+list.round_date+'">';
            strHtml+='<td style="text-align: right">'+list.d_tm+'</td>';
            strHtml+='<td style="text-align: center" >'+list.d_mn+'</td>';
            strHtml+='<td style="text-align: right">'+list.d_ta+'</td>';
            rq=' ';
            if(list.rq!=null&&list.rq!='')
                rq='<br>( '+parseFloat(list.rq).toFixed(2)+' )';
            sc=' ';
            if(list.scoreA!=null)
                sc='<br> ('+list.scoreA+'-'+list.scoreB+')';
            strHtml+='<td style="text-align: center">vs'+rq+''+sc+'</td>';
            strHtml+='<td>'+list.d_tb+'</td>';
            //hdp h2h
            if(hdp!=null){
                sumHDPA=getHdpCountMatch(idA,arid,scoreA,scoreB,hdp,count);
                strHtml+='<td style="'+sc1+'">'+sumHDPA+'</td>';
                sumHDPB=getHdpCountMatch(idB,arid,scoreA,scoreB,hdp,count);
                strHtml+='<td style="'+sc2+'">'+sumHDPB+'</td>';
            }else{
                strHtml+='<td style="'+sc1+'"> 0 </td>';
                strHtml+='<td style="'+sc2+'"> 0 </td>';                     
            }
            //hdp total team hotm
            if(totalHHDP!=null){
                sumTotalHHDP=getHdp(idA,totalHA,totalHScoreA,totalHScoreB,totalHHDP,countHdp);
                strHtml+='<td style="'+sc3+'">'+sumTotalHHDP+' </td>';
            }else{
                strHtml+='<td style="'+sc3+'"> 0 </td>';
            }
            //hdp total team away
            if(totalHHDP!=null){
                sumTotalAHDP=getHdp(idB,totalAA,totalAScoreA,totalAScoreB,totalAHDP,countHdp);
                strHtml+='<td style="'+sc4+'">'+sumTotalAHDP+' </td>';
            }else{
                strHtml+='<td style="'+sc4+'"> 0 </td>';
            }
            //score hdp h2h avg home  and away            
            if(hdp!=null){
                strHtml+='<td> '+getScoreHdpH2H(idA,arid,scoreA,scoreB,hdp,count)+' </td>';
                strHtml+='<td> '+getScoreHdpH2H(idB,arid,scoreA,scoreB,hdp,count)+' </td>';
            }else{
                strHtml+='<td> 0 </td>';
                strHtml+='<td> 0 </td>';
            }
            //hdp score total avg home and away
            if(hdp!=null){
                strHtml+='<td> '+getScoreHdpH2H(idA,totalHA,totalHScoreA,totalHScoreB,totalHHDP,count)+' </td>';
                strHtml+='<td> '+getScoreHdpH2H(idB,totalAA,totalAScoreA,totalAScoreB,totalAHDP,count)+' </td>';
            }else{
                strHtml+='<td> 0 </td>';
                strHtml+='<td> 0 </td>';
            }
            //get h2h
            strHtml+='<td> '+getH2H(idA,arid,scoreA,scoreB,count)+' </td>';
            strHtml+='<td> '+getH2H(idB,arid,scoreA,scoreB,count)+' </td>';
            
            strHtml+='</tr>';

            $('#showMatchList').append(strHtml);
        }
    }//end function showDatacondition
        
});

