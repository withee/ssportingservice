var language = ['big', 'en', 'gb', 'kr', 'th', 'vn','la'];
var completid = 0;
var completname = "";
$(document).ready(function() {
    $('a.7m').click(function() {
        var cid = $(this).attr('competitionId');
        var comName = $(this).attr('comName');
        $('#competitionId').attr('original', cid);
        $('#comName').html(comName);
        $('#comModal').modal();
        var all = $('input[name="all"]').prop('checked');
        all=all ==true?1:'';
        $.get('../site/getCompetitionList/all/' + all, function(response) {
            $('#competitionId').html(response);
        });
    });
    $('.closeModal').click(function() {
        $('#comModal').modal('hide');
        $("#zoneModal").modal("hide");
    });
    $('#saveComBt').on('click', function() {
        var self = $('#competitionId');
        var competitionId = self.val();
        var original = self.attr('original');
        $.post('../site/saveCom', {
            competitionId: competitionId, 
            original: original
        }, function(response) {
            if (response) {
                $('#comModal').modal('hide');
                for (var i = 0; i < language.length; i++) {
                    $('#' + language[i] + '-' + original).val(response[i]);                    
                }
            } else {
                alert('บันทึกไม่สำเร็จ');
            }
        }, 'json');

    });
    $('.removeCom7m').click(function() {
        var id7m = $(this).attr('id7m');
        if (id7m) {
            var name = $(this).attr("comName");
            var r = confirm("คุณต้องการลบประเทศ " + name + " ใช่ไหม");
            if (r) {
                $.get('../site/remove', {
                    id7m: id7m
                }, function(response) {
                    if (response) {
                        alert('ลบสำเร็จ');
                        window.location.reload();

                    } else {
                        alert('ลบไม่สำเร็จ,ลองใหม่อีกครั้ง');
                    }
                });
            }
        }
    });
    
    $(".saveLang").click(function(){
        completid=$(this).attr("completid");
        completname=$(this).attr("completname");  
        //  var completname = $(this).attr("completname"); 
        $("#zoneModal").modal("show");     
       
    });
    
    $("#saveZone").click(function(){
              
        var langList = {};  
        var notEmpty = true;
        $.each(language,function(key,val){
            if($("#"+val+"-"+completid).val()){
                langList[val] = $("#"+val+"-"+completid).val();
            }else{               
                notEmpty = false;
            }
                
        }); 
        if(notEmpty){
            var res = $.ajax({
                url:"/site/ManualAdd",
                type:"POST",
                data:{
                    completname:completname,
                    completid:completid,
                    langlist:JSON.stringify(langList),
                    zone:$("#zoneselect").val()
                },
                dataType:"JSON"
            });
            res.success(function(res){  
                $("#edit-"+completid).html("");
                $("#zoneModal").modal("hide"); 
                location.reload();
                console.log(res);
            });
        }else{
            alert("Have empty value");
            $("#zoneModal").modal("hide"); 
        }

    });
});

