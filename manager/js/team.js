var language = ['big', 'en', 'gb', 'kr', 'th', 'vn'];
$(document).ready(function() {
    $('a.7m').click(function() {
        var tid = $(this).attr('tid');
        var id7m = $(this).attr('id7m');
        var teamName = $(this).attr('teamName');
        $('#id7m').attr('tid', tid);
        $('#teamName').html(teamName);
        $('#myModal').modal();
        var all = $('input[name="all"]').prop('checked');
        all = all == true ? 1 : '';
        $.get('../../../team/getTeamList/id7m/' + id7m + '/all/' + all, function(response) {
            $('#id7m').html(response);
        });
    });
    $('.closeModal').click(function() {
        $('#myModal').modal('hide');
    });
    $('#saveBt').on('click', function() {
        var self = $('#id7m');
        var id7m = self.val();
        var tid = self.attr('tid');
        $.post('../../../team/save', {
            id7m: id7m, 
            tid: tid
        }, function(response) {
            if (response) {
                $('#myModal').modal('hide');
                for (var i = 0; i < language.length; i++) {
                    $('#' + language[i] + '-' + tid).val(response[i]);
                }
            } else {
                alert('บันทึกไม่สำเร็จ');
            }
        }, 'json');

    });
    $('.removeTeam7m').click(function() {
        var id7m = $(this).attr('id7m');
        if (id7m) {
            var name = $(this).attr("teamName");
            var r = confirm("คุณต้องการลบทึม " + name + " ใช่ไหม");
            if (r) {
                $.get('../../../team/remove', {
                    id7m: id7m
                }, function(response) {
                    if (response) {
                        alert('ลบสำเร็จ');
                        window.location.reload();

                    } else {
                        alert('ลบไม่สำเร็จ,ลองใหม่อีกครั้ง');
                    }
                });
            }
        }
    });
  
    $(".actbtn").click(function(){
        var key = $(this).attr("key");
        var th = $("#th-"+key).val();
        var en = $("#en-"+key).val();
        var big = $("#big-"+key).val();
        var gb = $("#gb-"+key).val();
        var kr = $("#kr-"+key).val();
        var vn = $("#vn-"+key).val();
        var la = $("#la-"+key).val();        
        var act = $(this).attr("act");   
        var lid7m=$(this).attr("lid7m");
        var teamname = $(this).attr("teamName");
        var tnpk = $(this).attr("tnPk");
        var tid = $(this).attr("tid");
        var agree = confirm("เพิ่ม/แก้ไข ชื่อทีมหรือไม่");
        if(agree){
           
            var req = $.ajax({
                type:"POST",
                url:"/team/UpdateTeam",
                data:{
                    key:key,
                    th:th,
                    en:en,
                    big:big,
                    gb:gb,
                    kr:kr,
                    vn:vn,
                    la:la,
                    act:act,
                    lid7m:lid7m,
                    teamname:teamname,
                    tnpk:tnpk,
                    tid:tid
                },
                dataType:"JSON"
            });
            req.success(function(res){
                console.log(res);
            })

        }
    });
});

