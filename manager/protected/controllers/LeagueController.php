<?php

class LeagueController extends Controller
{

    public function init()
    {
        $this->layout = 'main2';
    }

    public function actionIndex()
    {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerScriptFile('../../../js/league.js?timestamp=' . time());
        $competitionId = Yii::app()->getRequest()->getParam('competitionId');
        $query = "select lang_league.*,league.*,lang_league.leagueNameTh from league
            left join lang_league on lang_league.leagueId=league.leagueId
            where league.competitionId =$competitionId
            order by (CASE WHEN league.leaguePriority IS NULL then 1 ELSE 0 END),league.leaguePriority ASC,league.leagueId
                ";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $lang_competition = LangCompetition::model()->findByAttributes(array('cid' => $competitionId));
        $this->render('index', array('list' => $list, 'com' => $lang_competition, "competitionid" => $competitionId));
    }

    public function actionGetLeagueList()
    {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $this->layout = false;

        $areaId = Yii::app()->getRequest()->getParam('areaId');
        $all = Yii::app()->getRequest()->getParam('all', null);
        if ($all == 1 || $all == null) {
            $addition = $all ? '' : 'lang.id7m is null and ';
            $query = "select *from league_s 
            left join lang_league as lang on lang.id7m = league_s.targetId
            where $addition league_s.ln ='en'
            and league_s.areaId =$areaId
            ";
            $list = Yii::app()->db->createCommand($query)->queryAll();
            $this->render('getLeagueList', array('list' => $list));
        } else {

            $query = "
            select league_s.*,area.name as areaName from league_s 
            left join area on area.id = league_s.areaId and league_s.ln = area.ln
            where  league_s.ln ='en'  and league_s.gameType=1
            ";
            $list = Yii::app()->db->createCommand($query)->queryAll();
            $this->render('getAllLeagueList', array('list' => $list));
        }
    }

    public function actionSave()
    {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        header('Content-type: application/json');
        $this->layout = false;
        $id7m = Yii::app()->getRequest()->getParam('id7m');
        $leagueId = Yii::app()->getRequest()->getParam('leagueId');
//        $langLeague = LangLeague::model()->findByAttributes(array('id7m' => $id7m));
        $langLeague = LangLeague::model()->findByAttributes(array('leagueId' => $leagueId));
        if (!empty($langLeague)) {
            $langLeague->delete();
            $langLeague=null;
        }
        //var language = ['big', 'en', 'gb', 'kr', 'th', 'vn'];
        $query = "select *from league_s 
            where targetId=$id7m and gameType=1
            group by ln
                order by ln";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $league = League::model()->findByAttributes(array('leagueId' => $leagueId));
//        print_r($list);
//        exit;
        //print_r($langLeague);exit;

        if ($langLeague) {
            $langLeague->id7m = $id7m;
            $langLeague->leagueId = $leagueId;
            $langLeague->leagueNameBig = $list[0]['name'];
            $langLeague->leagueNameEn = $list[1]['name'];
            $langLeague->leagueNameGb = $list[2]['name'];
            $langLeague->leagueNameKr = $list[3]['name'];
            $langLeague->leagueNameTh = $list[4]['name'];
            $langLeague->leagueNameVn = $list[5]['name'];
            if ($langLeague->update()) {
                $array = array(
                    $langLeague->leagueNameBig,
                    $langLeague->leagueNameEn,
                    $langLeague->leagueNameGb,
                    $langLeague->leagueNameKr,
                    $langLeague->leagueNameTh,
                    $langLeague->leagueNameVn,

                );
                echo json_encode($array);
            }
        } else {
            $langLeague = new LangLeague();
            $langLeague->id7m = $id7m;
            $langLeague->leagueId = $leagueId;
            $langLeague->leagueNameBig = $list[0]['name'];
            $langLeague->leagueNameEn = $list[1]['name'];
            $langLeague->leagueNameGb = $list[2]['name'];
            $langLeague->leagueNameKr = $list[3]['name'];
            $langLeague->leagueNameTh = $list[4]['name'];
            $langLeague->leagueNameVn = $list[5]['name'];
            $langLeague->leagueName = $league['leagueName'];
            $langLeague->competitionId = $league['competitionId'];
            if ($langLeague->save()) {
                $array = array(
                    $langLeague->leagueNameBig,
                    $langLeague->leagueNameEn,
                    $langLeague->leagueNameGb,
                    $langLeague->leagueNameKr,
                    $langLeague->leagueNameTh,
                    $langLeague->leagueNameVn,

                );
                echo json_encode($array);
            }
            //print_r($langLeague->errors);
        }
    }

    public function actionRemove()
    {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $id7m = Yii::app()->request->getParam('id7m');
        if ($id7m) {
            echo LangLeague::model()->deleteAllByAttributes(array('id7m' => $id7m));
        }
    }

    public function actionSetLeaguePriority()
    {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $leagueId = Yii::app()->request->getParam('leagueId');
        $leaguePriority = (int)Yii::app()->request->getParam('leaguePriority', 0);
        if ($leagueId) {
            if ($leaguePriority == 0) {
                echo Yii::app()->db->createCommand("update league set leaguePriority = null where leagueId=$leagueId")->execute();
            } else {
                echo League::model()->updateAll(array('leaguePriority' => $leaguePriority), "leagueId=$leagueId");
            }
        }
    }

    public function actionAddLeagueLng()
    {
        $leagueid = $_REQUEST["leagueid"];
        $leaguename = $_REQUEST["leaguename"];
        $lngList = $_REQUEST["lnglist"];
        $id7m = $_REQUEST["id7m"];
        $comid = $_REQUEST["comid"];
        $lngList = json_decode($lngList);
        $result = array("success" => false, "desc" => "เกิดข้อผิดพลาด", "data" => array());
        if (!empty($id7m)) {
            $sqlmaxid = "SELECT MAX(id) as lastid FROM league_s";
            $maxid = Yii::app()->db->createCommand($sqlmaxid)->queryAll();
            $currentid = 0;

            if ((int)$maxid[0]["lastid"] < 500) {
                $currentid = 500;
            } else {
                $currentid = $maxid[0]["lastid"] + 1;
            }

            $sqlmaxtid = "SELECT MAX(targetId) as lasttid FROM league_s";
            $maxtid = Yii::app()->db->createCommand($sqlmaxtid)->queryAll();
            $currenttargetid = 0;
            if ((int)$maxtid[0]["lasttid"] < 1000) {
                $currenttargetid = 1000;
            } else {
                $currenttargetid = $maxtid[0]["lasttid"] + 1;
            }
            foreach ($lngList as $key => $val) {
                $leagues = new LeagueS();
                $leagues->id = $currentid;
                $leagues->targetId = $currenttargetid;
                $leagues->areaId = (int)$id7m;
                $leagues->count = 1;
                $leagues->gameType = 1;
                $leagues->type = 0;
                $leagues->ln = $key;
                $leagues->name = $val;
                $leagues->name_1 = $val;
                $leagues->name_2 = $val;
                $leagues->save();
                //print_r($leagues->getErrors());
                //exit();
                $result["data"][] = $leagues;
            }

            $lngleague = new LangLeague();
            $lngleague->leagueName = $leaguename;
            $lngleague->id7m = (int)$id7m;
            $lngleague->leagueId = $leagueid;
            $lngleague->competitionId = $comid;
            $lngleague->leagueNameBig = $lngList->big;
            $lngleague->leagueNameEn = $lngList->en;
            $lngleague->leagueNameGb = $lngList->gb;
            $lngleague->leagueNameKr = $lngList->kr;
            $lngleague->leagueNameLa = $lngList->la;
            $lngleague->leagueNameTh = $lngList->th;
            $lngleague->leagueNameVn = $lngList->vn;
            $lngleague->save();

            $result["success"] = true;
            $result["desc"] = "บันทึกเสร็จสิ้น";
        } else {
            $result["desc"] = "ยังไม่ได้ทำบันทึกข้อมูลประเทศ";
        }
        echo CJSON::encode($result);
    }

}