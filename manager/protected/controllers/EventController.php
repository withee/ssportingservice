<?php

class EventController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $criteria = new CDbCriteria();
        $criteria->condition = "status='active'";
        $criteria->order = "updated_at desc";
        $eventlist = AdminEvent::model()->findAll($criteria);
        $this->render('index', array('eventlist' => $eventlist));
    }

    public function actionFinished() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $criteria = new CDbCriteria();
        $criteria->condition = "status='finished'";
        $criteria->order = "updated_at desc";
        $eventlist = AdminEvent::model()->findAll($criteria);
        $this->render('index', array('eventlist' => $eventlist));
    }

    public function actionCancelled() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $criteria = new CDbCriteria();
        $criteria->condition = "status='cancelled'";
        $criteria->order = "updated_at desc";
        $eventlist = AdminEvent::model()->findAll($criteria);
        $this->render('index', array('eventlist' => $eventlist));
    }

    public function actionAdd() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $day = Yii::app()->request->getParam('day');
        if (empty($day)) {
            $day = date("Y-m-d");
        } else {
            $day = date("Y-m-d", strtotime($day));
        }
        $nowday = date("Y-m-d");
        $criteria = new CDbCriteria();
        if ($day == $nowday) {
            $criteria->condition = "sid='1' AND DATE(show_date)>='$day'";
            $criteria->order = "_lid";
        } else {
            $criteria->condition = "DATE(show_date)='$day'";
            $criteria->order = "_lid";
        }
        $gamelist = TimelinesGame::model()->findAll($criteria);
        $this->render('add', array('gamelist' => $gamelist, 'day' => $day));
    }

    public function actionReport() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $id = (int) Yii::app()->request->getParam('id');

        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $games = AdminEventGame::model()->findAllByAttributes(array('event_id' => $id));
        $midkey = "0";
        // print_r($games);
        foreach ($games as $g) {
            //var_dump($game);
            $midkey.=",{$g['mid']}";
        }
        //echo $midkey;
        $result = array('games' => array(), 'bet' => array(), 'comment' => array(), 'winner' => array(), 'id' => $id);
        $criteria = new CDbCriteria();
        $criteria->condition = "mid in ($midkey)";
        $criteria->order = "_lid";
        $gamelist = TimelinesGame::model()->findAll($criteria);
        $result['games'] = $gamelist;

        foreach ($gamelist as $game) {
            $criteria = new CDbCriteria();
            $criteria->condition = "match_id={$game['mid']} and choose='home'";
            $result['bet'][$game['mid']]['home'] = CommentOnMatch::model()->with('user')->findAll($criteria);

            $criteria = new CDbCriteria();
            $criteria->condition = "match_id={$game['mid']} and choose='away'";
            $result['bet'][$game['mid']]['away'] = CommentOnMatch::model()->with('user')->findAll($criteria);
        }
        $result['winner'] = AdminEventWinner::model()->findAll("event_id=$id");

//        $criteria = new CDbCriteria();
//        $criteria->alias = "t";
//        $criteria->condition = "t.fb_uid=100007546380463";
//        $user = CommentOnMatch::model()->with('user')->findAll($criteria);
//        $result['user'] = $user;
        //echo CJSON::encode($result);
        //print_r($result);
        //echo CJSON::encode($result);
        //exit();
        $this->render('report', array('result' => $result));
    }

    public function actionPrize() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $fb_uid = (int) Yii::app()->request->getParam('fb_uid');
        $prize = (int) Yii::app()->request->getParam('prize');
        $eid = (int) Yii::app()->request->getParam('eid');
        $cid = (int) Yii::app()->request->getParam('cid');
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $result = array('success' => FALSE, 'desc' => '');
        try {
            $user = User::model()->find("fb_uid=" . $fb_uid);
            $admin = User::getAdmin();
            if ($user) {
                $criteria = new CDbCriteria();
                $criteria->condition = "event_id=$eid AND comment_id=$cid";
                $duplicateprize = AdminEventWinner::model()->find($criteria);
                if (empty($duplicateprize)) {
                    $success = $this->addPrize($user, $prize);
                    if ($success) {
                        $winner = new AdminEventWinner();
                        $winner->event_id = $eid;
                        $winner->comment_id = $cid;
                        $winner->prize = $prize;
                        $winner->created_at = date('Y-m-d H:i:s');
                        $winner->updated_at = date('Y-m-d H:i:s');
                        if ($winner->save()) {
                            $result['success'] = true;
                            $result['desc'] = "คุณ " . $user->display_name . " ได้รับ $prize เพชร";
                        }
                        $msg = "คุณได้รับรางวัลจากกิจกรรม $prize เพชร";
                        $this->sendMessage($admin, $user, $msg);
                    } else {
                        $result['desc'] = 'update diamond failed';
                    }
                } else {
                    $success = $this->rollbackPrize($user, $duplicateprize->prize);
                    if ($success) {
                        $duplicateprize->prize = $prize;
                        $duplicateprize->updated_at = date('Y-m-d H:i:s');
                        $success = $duplicateprize->save();
                        if ($success) {
                            $success = $this->addPrize($user, $prize);
                            if ($success) {
                                $result['success'] = true;
                                $result['desc'] = "คุณ " . $user->display_name . " ได้รับ $prize เพชร";
                            }
                            $msg = "รางวัลกิจกรรมของคุณเปลี่ยนเป็น $prize เพชร";
                            $this->sendMessage($admin, $user, $msg);
                        }
                    }
                }
            } else {
                $result['desc'] = "user not found";
            }
        } catch (Exception $e) {
            $result['desc'] = $e->getMessage();
        }
        echo json_encode($result);
        //print_r($result);
        // $this->render('report', array('result' => $result));
    }

    function addPrize($user, $prize) {
        $beforediamond = $user->diamond;
        $user->diamond +=floatval($prize);
        $success = $user->save();
        if ($success) {
            $statement = new TotalStatement();
            $statement->uid = $user->uid;
            $statement->statement_type = 'award';
            $statement->before_diamond = $beforediamond;
            $statement->diamond_income = floatval($prize);
            $statement->balance_diamond = $user->diamond;
            $statement->result = 'win';
            $statement->note = "รางวัลกิจกรรม";
            $statement->statement_timestamp = date('Y-m-d H:i:s');
            $success = $statement->save();
        }
        return $success;
    }

    function rollbackPrize($user, $prize) {
        $beforediamond = $user->diamond;
        $user->diamond -=floatval($prize);
        $success = $user->save();
        if ($success) {
            $statement = new TotalStatement();
            $statement->uid = $user->uid;
            $statement->statement_type = 'award';
            $statement->before_diamond = $beforediamond;
            $statement->diamond_outcome = floatval($prize);
            $statement->balance_diamond = $user->diamond;
            $statement->result = 'win';
            $statement->note = "แก้ไขรางวัลกิจกรรม";
            $statement->statement_timestamp = date('Y-m-d H:i:s');
            $success = $statement->save();
        }
        return $success;
    }

    function sendMessage($admin, $user, $msg) {
        $message = new MessageBox();
        $message->message = $msg;
        $message->sender = $admin->uid;
        $message->receiver = $user->uid;
        $message->key = $message->sender . $message->receiver;
        $nowday = date('Y-m-d H:i:s');
        $message->send_at = $nowday;
        $message->stamp_at = strtotime($nowday);
        $message->save();
    }

    public function actionSave() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $name = Yii::app()->request->getParam('name');
        $desc = Yii::app()->request->getParam('desc');
        $midlist = Yii::app()->request->getParam('midlist');

        $event = new AdminEvent();
        $event->title = $name;
        $event->desc = $desc;
        $event->status = "active";
        $event->created_at = date('Y-m-d H:i:s');
        $event->updated_at = date('Y-m-d H:i:s');
        $success = $event->save();
        $result = array('success' => false, 'desc' => "");
        if ($success) {
            $result['success'] = true;
            foreach ($midlist as $mid) {
                $game = new AdminEventGame();
                $game->event_id = $event->id;
                $game->mid = $mid;
                $game->save();
            }
            $result['desc'] = "saved";
        }
        echo json_encode($result);
//        var_dump($midlist);
//        $criteria = new CDbCriteria();
//        $criteria->condition = "sid='1'";
//        $criteria->order = "_lid";
//        $gamelist = TimelinesGame::model()->findAll($criteria);
//        $this->render('add', array('gamelist' => $gamelist));
    }

    public function actionCancel() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $id = (int) Yii::app()->request->getParam('id');
        $result = array('success' => FALSE, 'desc' => '');
        $adminevent = AdminEvent::model()->findByPk($id);
        if ($adminevent) {
            $adminevent->status = "cancelled";
            $success = $adminevent->save();
            if ($success) {
                $result['desc'] = "Event cancelled";
                $result['success'] = TRUE;
                $result['data'] = $adminevent;
            } else {
                $result['desc'] = $adminevent->getErrors();
            }
        } else {
            $result['desc'] = "Event not found";
        }
        echo CJSON::encode($result);
    }

    public function actionEnd() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $id = (int) Yii::app()->request->getParam('id');
        $result = array('success' => FALSE, 'desc' => '');
        $adminevent = AdminEvent::model()->findByPk($id);
        if ($adminevent) {
            $adminevent->status = "finished";
            $success = $adminevent->save();
            if ($success) {
                $result['desc'] = "Event finished";
                $result['success'] = TRUE;
                $result['data'] = $adminevent;
            } else {
                $result['desc'] = $adminevent->getErrors();
            }
        } else {
            $result['desc'] = "Event not found";
        }
        echo CJSON::encode($result);
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
