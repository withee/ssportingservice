<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/6/2016
 * Time: 16:58
 */
class GalleryController extends Controller {

    public function init()
    {
        $this->layout = 'main2';
    }

    public function actionGallerySexy(){
        $offset= Yii::app()->request->getParam('offset', 0);
        $limit= Yii::app()->request->getParam('limit', 50);
        $search= Yii::app()->request->getParam('search', null);
        $tag= Yii::app()->request->getParam('tag', null);

        $condition=" `group`= 'sexy'";
        if(!empty($search)){
            if($condition==""){
                $condition .= " title like '%".$search."%'";
            }else {
                $condition .= " and title like '%".$search."%'";
            }
        }
        if(!empty($tag)){
            if($condition==""){
                $condition .= " tag like '%".$tag."%'";
            }else {
                $condition .= " and tag like '%".$tag."%'";
            }
        }

        $dataGallery=MediaGallery::model()->findAll(array('order' => 'id DESC', 'condition' => $condition, 'limit' => $limit,'offset'=>$offset));
        if($condition==""){
            $sql = "SELECT COUNT(*) FROM media_gallery";
        }else {
            $sql = "SELECT COUNT(*) FROM media_gallery WHERE " . $condition;
        }
        $countPage = Yii::app()->db->createCommand($sql)->queryScalar();

        $this->render('GallerySexy',array('countPage'=>$countPage,'dataGallery'=>$dataGallery,'search'=>$search,'tag'=>$tag,'offset'=>$offset,'countPage'=>$countPage,'limit'=>$limit));
    }

    public function actionAddGallerySexy(){
        $id= Yii::app()->request->getParam('id', null);
        $imagesId= Yii::app()->request->getParam('imagesId', null);
        $listImage=array();

        if(!empty($imagesId)){
            $mediaStoreObject=MediaStore::model()->findByPk($imagesId);
        }else{
            $mediaStoreObject=new MediaStore();
        }

        if(!empty($id)){
            $newGallery = MediaGallery::model()->findByPk($id);
            $newGallery->update_at=date('Y-m-d H:i:s');
            $listImage=MediaStore::model()->findAll('gall_id=?',array($id));
        }else {
            $newGallery = New MediaGallery();
            $newGallery->created_at=date('Y-m-d H:i:s');
            $newGallery->update_at=date('Y-m-d H:i:s');
        }

        if(isset($_POST['Gallery'])) {
            $newGallery->attributes=$_POST['Gallery'];
            $newGallery->fb_uid = 99999;
            $newGallery->group = 'sexy';
            $newGallery->category=$_POST['Gallery']['category'];
//            $newGallery->gall_type = 'picture';
            if($newGallery->validate()) {
                $newGallery->save();
                $this->redirect('/Gallery/GallerySexy');
            }else{
                $errores = $newGallery->getErrors();
                var_dump($errores);
                exit;
            }
        }

        $this->render('AddGallerySexy',array('newGallery'=>$newGallery,'listImage'=>$listImage,'mediaStoreObject'=>$mediaStoreObject));
    }

    public function actionAddImages(){
        if(isset($_POST['MediaStore'])) {
            if(!empty($_POST['MediaStore']['id'])){
                $MediaStoreObject=MediaStore::model()->findByPk($_POST['MediaStore']['id']);
            }else{
                $MediaStoreObject=new MediaStore();
            }
            $MediaStoreObject->attributes=$_POST['MediaStore'];
            $MediaStoreObject->typeVideo=$_POST['MediaStore']['typeVideo'];
            if($MediaStoreObject->validate()) {
                $MediaStoreObject->created_at=date('Y-m-d H:i:s');
                $MediaStoreObject->save();
                $this->redirect('/gallery/AddGallerySexy?id='.$MediaStoreObject->gall_id);
            }else{
                $errores = $MediaStoreObject->getErrors();
                var_dump($errores);
                exit;
            }
        }
    }

    public function actionAddGallery(){
        $id= Yii::app()->request->getParam('id', null);
        $title= Yii::app()->request->getParam('title', null);
        $tag= Yii::app()->request->getParam('tag', null);
        $desc= Yii::app()->request->getParam('desc', null);
        $group= Yii::app()->request->getParam('group', 'sexy');
        $gall_type= Yii::app()->request->getParam('gall_type', 'picture');
        $fb_uid= Yii::app()->request->getParam('fb_uid', 99999999999999999);
        if(!empty($id)){
            $newGallery = MediaGallery::model()->findByPk($id);
            $newGallery->update_at=date('Y-m-d H:i:s');
        }else {
            $newGallery = New MediaGallery();
            $newGallery->created_at=date('Y-m-d H:i:s');
            $newGallery->update_at=date('Y-m-d H:i:s');
        }
        $newGallery->fb_uid=$fb_uid;
        $newGallery->title=$title;
        $newGallery->desc=$desc;
        $newGallery->tag=$tag;
        $newGallery->group=$group;
        $newGallery->gall_type=$gall_type;
        if($newGallery->validate()){
            $newGallery->save();
            echo CJSON::encode($newGallery);
        }else{
            $errores = $newGallery->getErrors();
            var_dump($errores);
            exit;
        }
    }

    public function actionAddImageDesc(){
        $id= Yii::app()->request->getParam('id', null);
        $DescImage= Yii::app()->request->getParam('DescImage', null);
        MediaStore::model()->updateByPk($id,array('desc'=>$DescImage));
        echo CJSON::encode(array('id'=>$id,'DescImage'=>$DescImage));
    }

    public function actionRemoveGallery(){
        $id= Yii::app()->request->getParam('id', null);
        MediaGallery::model()->deleteByPk($id);
        $this->redirect('/Gallery/GallerySexy');
    }

    public function actionViewGallerySexy(){
        $search= Yii::app()->request->getParam('search', null);
        $tag= Yii::app()->request->getParam('tag', null);
        $criteria = new CDbCriteria();
        $criteria->compare('group', 'sexy');
        if(!empty($search) and !empty($tag)) {
            $dataGallery = MediaGallery::model()->with('Images')->findAll(array('order' => 't.id DESC', 'condition' => '`group`=:x and t.title like :s and t.tag like :t', 'limit' => 50, 'params' => array(':x' => 'sexy', ':s' => '%' . $search . '%',':t'=>'%'.$tag.'%')));
        }elseif(!empty($search)) {
            $dataGallery = MediaGallery::model()->with('Images')->findAll(array('order' => 't.id DESC', 'condition' => '`group`=:x and t.title like :s', 'limit' => 50, 'params' => array(':x' => 'sexy', ':s' => '%' . $search . '%')));
        }elseif(!empty($tag)){
            $dataGallery = MediaGallery::model()->with('Images')->findAll(array('order' => 't.id DESC', 'condition' => '`group`=:x and t.tag like :s', 'limit' => 50, 'params' => array(':x' => 'sexy', ':s' => '%' . $tag . '%')));
        }else {
            $dataGallery = MediaGallery::model()->with('Images')->findAll(array('order' => 't.id DESC', 'condition' => '`group`=:x', 'limit' => 50, 'params' => array(':x' => 'sexy')));
        }
        $this->render('viewGallerySexy',array('dataGallery'=>$dataGallery,'search'=>$search,'tag'=>$tag));
    }

    public function actionDeleteImages(){
        $id= Yii::app()->request->getParam('id', null);
        $dataMediaStore=MediaStore::model()->findByPk($id);
        MediaStore::model()->deleteByPk($id);
        $this->redirect('/gallery/AddGallerySexy?id='.$dataMediaStore->gall_id);
    }

    public function actionUploadImage(){
        $id= Yii::app()->request->getParam('id', null);
        if (!empty($_FILES)&&!empty($id)) {
            $newMediaStore=MediaStore::model()->findByPk($id);
            $number=rand(100, 10000000);
            $webroot = "..";
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $nameFile = $_FILES['Filedata']['name'];
            $typeFile = $_FILES['Filedata']['type'];
            $targetPath = $webroot . '/uploaded/gallery/'.$newMediaStore->gall_id.'/';
            $data=getimagesize($tempFile);
            if (!is_dir($webroot . '/uploaded')) {
                mkdir($webroot . '/uploaded', 0777);
                chmod($webroot . '/uploaded', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'gallery')) {
                mkdir($webroot . '/uploaded/' . 'gallery', 0777);
                chmod($webroot . '/uploaded/' . 'gallery', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'gallery/'.$newMediaStore->gall_id)) {
                mkdir($webroot . '/uploaded/' . 'gallery/'.$newMediaStore->gall_id, 0777);
                chmod($webroot . '/uploaded/' . 'gallery/'.$newMediaStore->gall_id, 0777);
            }

            list($width, $height) = getimagesize($tempFile);
            $thumb = imagecreatetruecolor(($width*0.5), ($height*0.5));
            if($data['mime']=="image/jpeg"){
                $file = str_replace('//', '/', $targetPath) . 'galleryImage_' . $newMediaStore->gall_id . '_' . $newMediaStore->id . '.jpg';
                $fileMin = str_replace('//', '/', $targetPath) . 'galleryImage_' . $newMediaStore->gall_id  . '_' . $newMediaStore->id . '_Q50.jpg';
                $source = imagecreatefromjpeg($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagejpeg($thumb, str_replace('//', '/', $targetPath) . 'galleryImage_'.$newMediaStore->gall_id.'_'.$newMediaStore->id. '_Q50.jpg');
            }else if($data['mime']=="image/png") {
                $file = str_replace('//', '/', $targetPath) . 'galleryImage_' . $newMediaStore->gall_id . '_' . $newMediaStore->id . '.png';
                $fileMin = str_replace('//', '/', $targetPath) . 'galleryImage_' . $newMediaStore->gall_id . '_' . $newMediaStore->id . '_Q50.png';
                $source = imagecreatefrompng($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagepng($thumb, str_replace('//', '/', $targetPath) . 'galleryImage_'.$newMediaStore->gall_id.'_'.$newMediaStore->id. '_Q50.png');
            }
            MediaStore::model()->updateByPk($newMediaStore->id, array('thumbnail' => 'http://api.pass90.com'.str_replace($webroot,'',$file)));
            move_uploaded_file($tempFile, $file);
            echo CJSON::encode(MediaStore::model()->findByPk($newMediaStore->id));
        }else{
            echo 'false';
        }
    }
    public function actionUploadImageNewsNone(){
        if (!empty($_FILES)) {
            $number=rand(100, 10000000);
            $webroot = "..";
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $nameFile = $_FILES['Filedata']['name'];
            $typeFile = $_FILES['Filedata']['type'];
            $targetPath = $webroot . '/uploaded/news/blackup/';
            $data=getimagesize($tempFile);
            if (!is_dir($webroot . '/uploaded')) {
                mkdir($webroot . '/uploaded', 0777);
                chmod($webroot . '/uploaded', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'news')) {
                mkdir($webroot . '/uploaded/' . 'news', 0777);
                chmod($webroot . '/uploaded/' . 'news', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'news/'.'blackup')) {
                mkdir($webroot . '/uploaded/' . 'news/'.'blackup', 0777);
                chmod($webroot . '/uploaded/' . 'news/'.'blackup', 0777);
            }

            list($width, $height) = getimagesize($tempFile);
            $thumb = imagecreatetruecolor(($width*0.5), ($height*0.5));

            if($data['mime']=="image/jpeg"){
                $file = str_replace('//', '/', $targetPath) . 'newsImage_'. date('YmdHis') . '.jpg';
                $fileMin = str_replace('//', '/', $targetPath) . 'newsImage_'. date('YmdHis') . '_Q50.jpg';
                $source = imagecreatefromjpeg($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagejpeg($thumb, str_replace('//', '/', $targetPath) . 'newsImage_'.date('YmdHis'). '_Q50.jpg');
            }else if($data['mime']=="image/png") {
                $file = str_replace('//', '/', $targetPath) . 'newsImage_'. date('YmdHis') . '.png';
                $fileMin = str_replace('//', '/', $targetPath) . 'newsImage_'. date('YmdHis') . '_Q50.png';
                $source = imagecreatefrompng($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagepng($thumb, str_replace('//', '/', $targetPath) . 'newsImage_'.date('YmdHis'). '_Q50.png');
            }
//            News::model()->updateByPk($newNews->newsId, array('imageLink' => 'http://api.pass90.com'.str_replace($webroot,'',$fileMin)));
            move_uploaded_file($tempFile, $file);
            echo CJSON::encode(array('imageLink'=>str_replace($webroot,'',$file)));
        }else{
            echo 'false';
        }
    }
    public function actionUploadImageNews(){
        $id= Yii::app()->request->getParam('id', null);
        if (!empty($_FILES)&&!empty($id)) {
            $newNews=News::model()->findByPk($id);
            $number=rand(100, 10000000);
            $webroot = "..";
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $nameFile = $_FILES['Filedata']['name'];
            $typeFile = $_FILES['Filedata']['type'];
            $targetPath = $webroot . '/uploaded/news/'.$newNews->newsId.'/';
            $data=getimagesize($tempFile);
            if (!is_dir($webroot . '/uploaded')) {
                mkdir($webroot . '/uploaded', 0777);
                chmod($webroot . '/uploaded', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'news')) {
                mkdir($webroot . '/uploaded/' . 'news', 0777);
                chmod($webroot . '/uploaded/' . 'news', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'news/'.$newNews->newsId)) {
                mkdir($webroot . '/uploaded/' . 'news/'.$newNews->newsId, 0777);
                chmod($webroot . '/uploaded/' . 'news/'.$newNews->newsId, 0777);
            }

            list($width, $height) = getimagesize($tempFile);
            $thumb = imagecreatetruecolor(($width*0.5), ($height*0.5));
            if($data['mime']=="image/jpeg"){
                $file = str_replace('//', '/', $targetPath) . 'newsImage_'. $newNews->newsId . '.jpg';
                $fileMin = str_replace('//', '/', $targetPath) . 'newsImage_'. $newNews->newsId . '_Q50.jpg';
                $source = imagecreatefromjpeg($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagejpeg($thumb, str_replace('//', '/', $targetPath) . 'newsImage_'.$newNews->newsId. '_Q50.jpg');
            }else if($data['mime']=="image/png") {
                $file = str_replace('//', '/', $targetPath) . 'newsImage_'. $newNews->newsId . '.png';
                $fileMin = str_replace('//', '/', $targetPath) . 'newsImage_'. $newNews->newsId . '_Q50.png';
                $source = imagecreatefrompng($tempFile);
                imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                imagepng($thumb, str_replace('//', '/', $targetPath) . 'newsImage_'.$newNews->newsId. '_Q50.png');
            }
            News::model()->updateByPk($newNews->newsId, array('imageLink' => 'http://api.pass90.com'.str_replace($webroot,'',$fileMin)));
            move_uploaded_file($tempFile, $file);
            echo CJSON::encode(News::model()->findByPk($newNews->newsId));
        }else{
            echo 'false';
        }
    }
    public function actionUploadFile(){
        $id= Yii::app()->request->getParam('id', null);
        if (!empty($_FILES)&&!empty($id)) {
            $dataReturn=array();
            for ($i=0;$i<count($_FILES['Filedata']['tmp_name']);$i++) {
                $number = rand(100, 10000000);
                $webroot = "..";
                $tempFile = $_FILES['Filedata']['tmp_name'][$i];
                $nameFile = $_FILES['Filedata']['name'][$i];
                $typeFile = $_FILES['Filedata']['type'][$i];
                $targetPath = $webroot . '/uploaded/gallery/' . $id . '/';
                $data = getimagesize($tempFile);
                if (!is_dir($webroot . '/uploaded')) {
                    mkdir($webroot . '/uploaded', 0777);
                    chmod($webroot . '/uploaded', 0777);
                }

                if (!is_dir($webroot . '/uploaded/' . 'gallery')) {
                    mkdir($webroot . '/uploaded/' . 'gallery', 0777);
                    chmod($webroot . '/uploaded/' . 'gallery', 0777);
                }

                if (!is_dir($webroot . '/uploaded/' . 'gallery/' . $id)) {
                    mkdir($webroot . '/uploaded/' . 'gallery/' . $id, 0777);
                    chmod($webroot . '/uploaded/' . 'gallery/' . $id, 0777);
                }

                $newMediaStore = new MediaStore();
                $newMediaStore->gall_id = $id;
                $newMediaStore->path = str_replace($webroot, '', str_replace('//', '/', $targetPath) . 'galleryImage_' . $id);
                $newMediaStore->thumbnail = "";
                $newMediaStore->created_at = date('Y-m-d H:i:s');
                $newMediaStore->desc = "";
                if ($newMediaStore->validate()) {
                    $newMediaStore->save();
                    list($width, $height) = getimagesize($tempFile);
                    $thumb = imagecreatetruecolor(($width * 0.5), ($height * 0.5));
                    if ($data['mime'] == "image/jpeg") {
                        $file = str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '.jpg';
                        $fileMin = str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '_Q50.jpg';
                        $source = imagecreatefromjpeg($tempFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width * 0.5), ($height * 0.5), $width, $height);
                        imagejpeg($thumb, str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '_Q50.jpg');
                    } else if ($data['mime'] == "image/png") {
                        $file = str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '.png';
                        $fileMin = str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '_Q50.png';
                        $source = imagecreatefrompng($tempFile);
                        imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width * 0.5), ($height * 0.5), $width, $height);
                        imagepng($thumb, str_replace('//', '/', $targetPath) . 'galleryImage_' . $id . '_' . $newMediaStore->id . '_Q50.png');
                    }
                    MediaStore::model()->updateByPk($newMediaStore->id, array('path' => 'http://api.pass90.com' . str_replace($webroot, '', $file)));
                    MediaStore::model()->updateByPk($newMediaStore->id, array('thumbnail' => 'http://api.pass90.com' . str_replace($webroot, '', $fileMin)));
                    move_uploaded_file($tempFile, $file);

                    array_push($dataReturn,MediaStore::model()->findByPk($newMediaStore->id));
//                    echo CJSON::encode(MediaStore::model()->findByPk($newMediaStore->id));
                } else {
                    $errores = $newMediaStore->getErrors();
                    var_dump($errores);
                    exit;
                }
            }
            echo CJSON::encode($dataReturn);
        }else{
            echo 'false';
        }
    }
}