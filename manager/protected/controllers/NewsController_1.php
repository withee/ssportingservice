<?php

class NewsController extends Controller {

    public $nation = array('th' => 'Th', 'en' => 'En', 'big' => 'Big', 'gb' => 'Gb', 'kr' => 'Kr', 'vn' => 'Vn', 'la' => 'La');

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $sql = "select *,FROM_UNIXTIME(createDatetime) + interval 7 hour as createDatetime,FROM_UNIXTIME(updateDatetime) + interval 7 hour as updateDatetime from news  order by newsId DESC limit 30";
        $newsList = Yii::app()->db->createCommand($sql)->queryAll();
        $this->render('index', array('newsList' => $newsList));
    }

    public function actionAdd() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');
        //$cs->registerCssFile($baseUrl.'/js-library/ckeditor/styles.js');
        $insertSuccess = null;
        $langList = array(
            'ไทย', 'อังกฤษ', 'จีนดั่งเดิม', 'จีนประยุกษ์', 'เกาหลี', 'เวียดนาม', 'ลาว'
        );
        $langs = array(
            'Th', 'En', 'Big', 'Gb', 'Kr', 'Vn', 'La'
        );
        if (Yii::app()->request->getParam('submit')) {
            $obj = new News();
            $obj->titleBig = Yii::app()->request->getParam('titleBig');
            $obj->titleEn = Yii::app()->request->getParam('titleEn');
            $obj->titleTh = Yii::app()->request->getParam('titleTh');
            $obj->titleVn = Yii::app()->request->getParam('titleVn');
            $obj->titleLa = Yii::app()->request->getParam('titleLa');
            $obj->titleGb = Yii::app()->request->getParam('titleGb');
            $obj->titleKr = Yii::app()->request->getParam('titleKr');
            $obj->imageLink = Yii::app()->request->getParam('imageLink');
            $obj->shortDescriptionBig = Yii::app()->request->getParam('shortDescriptionBig');
            $obj->shortDescriptionEn = Yii::app()->request->getParam('shortDescriptionEn');
            $obj->shortDescriptionTh = Yii::app()->request->getParam('shortDescriptionTh');
            $obj->shortDescriptionVn = Yii::app()->request->getParam('shortDescriptionVn');
            $obj->shortDescriptionLa = Yii::app()->request->getParam('shortDescriptionLa');
            $obj->shortDescriptionGb = Yii::app()->request->getParam('shortDescriptionGb');
            $obj->shortDescriptionKr = Yii::app()->request->getParam('shortDescriptionKr');

            $obj->contentBig = Yii::app()->request->getParam('contentBig');
            $obj->contentEn = Yii::app()->request->getParam('contentEn');
            $obj->contentTh = Yii::app()->request->getParam('contentTh');
            $obj->contentVn = Yii::app()->request->getParam('contentVn');
            $obj->contentLa = Yii::app()->request->getParam('contentLa');
            $obj->contentGb = Yii::app()->request->getParam('contentGb');
            $obj->contentKr = Yii::app()->request->getParam('contentKr');

            $obj->leagueId1 = Yii::app()->request->getParam('leagueId1');
            $obj->leagueId2 = Yii::app()->request->getParam('leagueId2');
            $obj->leagueId3 = Yii::app()->request->getParam('leagueId3');
            $obj->leagueId4 = Yii::app()->request->getParam('leagueId4');

            $obj->teamId1 = Yii::app()->request->getParam('teamId1');
            $obj->teamId2 = Yii::app()->request->getParam('teamId2');
            $obj->teamId3 = Yii::app()->request->getParam('teamId3');
            $obj->teamId4 = Yii::app()->request->getParam('teamId4');
            if (!empty(Yii::app()->request->getParam('worldcups'))) {
                $obj->worldcups = Yii::app()->request->getParam('worldcups');
            }

            $obj->createDatetime = new CDbExpression('UNIX_TIMESTAMP(current_timestamp)');
            $obj->updateDatetime = $obj->createDatetime;
            //echo $obj->createDatetime;
            //var_dump($obj);exit();

            if ($obj->save()) {

                $insertSuccess = 1;
                //echo $obj->newsId;exit();
                //put news to timelines
//                $nowday = date("Y-m-d H:i:s");
//                $stmp = strtotime($nowday);
                $keygen = 'news' . $obj->newsId;
                $tlsql = "INSERT INTO `timelines` (`fb_uid`, `content_type`, `key_list`, `created_at`, `updated_at`, `stamped_at`,`type`,`keygen`) VALUES ('broadcast', 'news', '{$obj->newsId}', NOW(), NOW(), UNIX_TIMESTAMP(),2,'$keygen');";
                Yii::app()->db->createCommand($tlsql)->execute();
                $this->newsWriter();

//                $list_sql = "select news.*,
//l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
//l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
//l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
//l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
//t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
//t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
//t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
//t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
//from news
//left join lang_league as l1 on l1.leagueId = news.leagueId1
//left join lang_league as l2 on l2.leagueId = news.leagueId2
//left join lang_league as l3 on l3.leagueId = news.leagueId3
//left join lang_league as l4 on l4.leagueId = news.leagueId4
//left join lang_team as t1 on t1.tid = news.teamId1
//left join lang_team as t2 on t2.tid = news.teamId2
//left join lang_team as t3 on t3.tid = news.teamId3
//left join lang_team as t4 on t4.tid = news.teamId4
//order by newsId DESC
//limit 10;
//";
//                //echo $list_sql;
//                $list_all = Yii::app()->db->createCommand($list_sql)->queryAll();
//                $lang_leauge = array();
//                $lang_team = array();
//                $new_list = array();
//                $lang_content = array();
//                $lang_title = array();
//                $lang_shortDescription = array();
//                foreach ($list_all as $object) {
//                    $lang_title_obj = array();
//                    $lang_content_obj = array();
//                    $lang_shortDescription_obj = array();
//                    foreach ($this->nation as $key => $nation) {
//                        $lang_title_obj[$key] = $object['title' . $nation];
//                        $lang_shortDescription_obj[$key] = $object['shortDescription' . $nation];
//                        $lang_content_obj[$key] = $object['content' . $nation];
//                        unset($object['title' . $nation]);
//                        unset($object['shortDescription' . $nation]);
//                        unset($object['content' . $nation]);
//                    }
//                    $lang_title[$object['newsId']] = $lang_title_obj;
//                    $lang_shortDescription[$object['newsId']] = $lang_shortDescription_obj;
//                    $lang_content[$object['newsId']] = $lang_content_obj;
//
//
//                    for ($i = 1; $i <= 4; $i++) {
//                        if ($object['leagueId' . $i]) {
//                            $leagueObj = array();
//                            $leagueObj['en'] = $object['leagueNameEn' . $i];
//                            $leagueObj['th'] = $object['leagueNameTh' . $i];
//                            $leagueObj['big'] = $object['leagueNameBig' . $i];
//                            $leagueObj['gb'] = $object['leagueNameGb' . $i];
//                            $leagueObj['kr'] = $object['leagueNameKr' . $i];
//                            $leagueObj['vn'] = $object['leagueNameVn' . $i];
//                            $leagueObj['la'] = $object['leagueNameLa' . $i];
//                            $lang_leauge[$object['leagueId' . $i]] = $leagueObj;
//                        }
//                        unset($object['leagueNameEn' . $i]);
//                        unset($object['leagueNameTh' . $i]);
//                        unset($object['leagueNameBig' . $i]);
//                        unset($object['leagueNameGb' . $i]);
//                        unset($object['leagueNameKr' . $i]);
//                        unset($object['leagueNameVn' . $i]);
//                        unset($object['leagueNameLa' . $i]);
//                    }
//                    for ($i = 1; $i <= 4; $i++) {
//                        if ($object['teamId' . $i]) {
//                            $teamObj = array();
//                            $teamObj['en'] = $object['teamNameEn' . $i];
//                            $teamObj['th'] = $object['teamNameTh' . $i];
//                            $teamObj['big'] = $object['teamNameBig' . $i];
//                            $teamObj['gb'] = $object['teamNameGb' . $i];
//                            $teamObj['kr'] = $object['teamNameKr' . $i];
//                            $teamObj['vn'] = $object['teamNameVn' . $i];
//                            $teamObj['la'] = $object['teamNameLa' . $i];
//                            $lang_team[$object['teamId' . $i]] = $teamObj;
//                        }
//                        unset($object['teamNameEn' . $i]);
//                        unset($object['teamNameTh' . $i]);
//                        unset($object['teamNameBig' . $i]);
//                        unset($object['teamNameGb' . $i]);
//                        unset($object['teamNameKr' . $i]);
//                        unset($object['teamNameVn' . $i]);
//                        unset($object['teamNameLa' . $i]);
//                    }
//                    array_push($new_list, $object);
//                }
//
//                $jsonObj = array(
//                    'data' => $new_list,
//                    'lang_league' => $lang_leauge,
//                    'lang_team' => $lang_team,
//                    'lang_title' => $lang_title,
//                    'lang_shortDescription' => $lang_shortDescription,
//                    'lang_content' => $lang_content,
//                );
//                $json = json_encode($jsonObj);
//                file_put_contents('../news/allNews.json', $json);
            } else {
                $insertSuccess = 2;
            }
            //print_r($obj->errors);
        }
        $com_sql = "select *from competitions order by competitionName";
        $comList = Yii::app()->db->createCommand($com_sql)->queryAll();
        $this->render('add', array('langList' => $langList, 'comList' => $comList, 'langs' => $langs, 'insertSuccess' => $insertSuccess));
    }

    public function ActionList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');
        //$cs->registerCssFile($baseUrl.'/js-library/ckeditor/styles.js');
        $langList = array(
            'อังกฤษ', 'ไทย', 'จีนดั่งเดิม', 'จีนประยุกษ์', 'เกาหลี', 'เวียดนาม', 'ลาว'
        );
        $langs = array(
            'En', 'Th', 'Big', 'Gb', 'Kr', 'Vn', 'La'
        );
        $news = array();
        if (file_exists("../news/allNews.json"))
            $news = json_decode(file_get_contents("../news/allNews.json"), true);
        $this->render('list', array('langList' => $langList, 'langs' => $langs, 'news' => $news));
    }

    function newsWriter() {
        $list_sql = "select news.*,
l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
from news
left join lang_league as l1 on l1.leagueId = news.leagueId1
left join lang_league as l2 on l2.leagueId = news.leagueId2
left join lang_league as l3 on l3.leagueId = news.leagueId3
left join lang_league as l4 on l4.leagueId = news.leagueId4
left join lang_team as t1 on t1.tid = news.teamId1
left join lang_team as t2 on t2.tid = news.teamId2
left join lang_team as t3 on t3.tid = news.teamId3
left join lang_team as t4 on t4.tid = news.teamId4
order by newsId DESC
limit 10;
";
        //echo $list_sql;
        $list_all = Yii::app()->db->createCommand($list_sql)->queryAll();
        $lang_leauge = array();
        $lang_team = array();
        $new_list = array();
        $lang_content = array();
        $lang_title = array();
        $lang_shortDescription = array();
        foreach ($list_all as $object) {
            $lang_title_obj = array();
            $lang_content_obj = array();
            $lang_shortDescription_obj = array();
            foreach ($this->nation as $key => $nation) {
                $lang_title_obj[$key] = $object['title' . $nation];
                $lang_shortDescription_obj[$key] = $object['shortDescription' . $nation];
                $lang_content_obj[$key] = $object['content' . $nation];
                unset($object['title' . $nation]);
                unset($object['shortDescription' . $nation]);
                unset($object['content' . $nation]);
            }
            $lang_title[$object['newsId']] = $lang_title_obj;
            $lang_shortDescription[$object['newsId']] = $lang_shortDescription_obj;
            $lang_content[$object['newsId']] = $lang_content_obj;


            for ($i = 1; $i <= 4; $i++) {
                if ($object['leagueId' . $i]) {
                    $leagueObj = array();
                    $leagueObj['en'] = $object['leagueNameEn' . $i];
                    $leagueObj['th'] = $object['leagueNameTh' . $i];
                    $leagueObj['big'] = $object['leagueNameBig' . $i];
                    $leagueObj['gb'] = $object['leagueNameGb' . $i];
                    $leagueObj['kr'] = $object['leagueNameKr' . $i];
                    $leagueObj['vn'] = $object['leagueNameVn' . $i];
                    $leagueObj['la'] = $object['leagueNameLa' . $i];
                    $lang_leauge[$object['leagueId' . $i]] = $leagueObj;
                }
                unset($object['leagueNameEn' . $i]);
                unset($object['leagueNameTh' . $i]);
                unset($object['leagueNameBig' . $i]);
                unset($object['leagueNameGb' . $i]);
                unset($object['leagueNameKr' . $i]);
                unset($object['leagueNameVn' . $i]);
                unset($object['leagueNameLa' . $i]);
            }
            for ($i = 1; $i <= 4; $i++) {
                if ($object['teamId' . $i]) {
                    $teamObj = array();
                    $teamObj['en'] = $object['teamNameEn' . $i];
                    $teamObj['th'] = $object['teamNameTh' . $i];
                    $teamObj['big'] = $object['teamNameBig' . $i];
                    $teamObj['gb'] = $object['teamNameGb' . $i];
                    $teamObj['kr'] = $object['teamNameKr' . $i];
                    $teamObj['vn'] = $object['teamNameVn' . $i];
                    $teamObj['la'] = $object['teamNameLa' . $i];
                    $lang_team[$object['teamId' . $i]] = $teamObj;
                }
                unset($object['teamNameEn' . $i]);
                unset($object['teamNameTh' . $i]);
                unset($object['teamNameBig' . $i]);
                unset($object['teamNameGb' . $i]);
                unset($object['teamNameKr' . $i]);
                unset($object['teamNameVn' . $i]);
                unset($object['teamNameLa' . $i]);
            }
            array_push($new_list, $object);
        }

        $jsonObj = array(
            'data' => $new_list,
            'lang_league' => $lang_leauge,
            'lang_team' => $lang_team,
            'lang_title' => $lang_title,
            'lang_shortDescription' => $lang_shortDescription,
            'lang_content' => $lang_content,
        );
        $json = json_encode($jsonObj);
        file_put_contents('../news/allNews.json', $json);
    }

    public function actionEdit() {
        $newsId = Yii::app()->request->getParam('newsId');
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $insertSuccess = null;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');
        //$cs->registerCssFile($baseUrl.'/js-library/ckeditor/styles.js');
        $langList = array(
            'ไทย', 'อังกฤษ', 'จีนดั่งเดิม', 'จีนประยุกษ์', 'เกาหลี', 'เวียดนาม', 'ลาว'
        );
        $langs = array(
            'Th', 'En', 'Big', 'Gb', 'Kr', 'Vn', 'La'
        );
        if (Yii::app()->request->getParam('submit')) {
            $obj = News::model()->findByPk($newsId);
            $obj->titleBig = Yii::app()->request->getParam('titleBig');
            $obj->titleEn = Yii::app()->request->getParam('titleEn');
            $obj->titleTh = Yii::app()->request->getParam('titleTh');
            $obj->titleVn = Yii::app()->request->getParam('titleVn');
            $obj->titleLa = Yii::app()->request->getParam('titleLa');
            $obj->titleGb = Yii::app()->request->getParam('titleGb');
            $obj->titleKr = Yii::app()->request->getParam('titleKr');
            $obj->imageLink = Yii::app()->request->getParam('imageLink');
            $obj->shortDescriptionBig = Yii::app()->request->getParam('shortDescriptionBig');
            $obj->shortDescriptionEn = Yii::app()->request->getParam('shortDescriptionEn');
            $obj->shortDescriptionTh = Yii::app()->request->getParam('shortDescriptionTh');
            $obj->shortDescriptionVn = Yii::app()->request->getParam('shortDescriptionVn');
            $obj->shortDescriptionLa = Yii::app()->request->getParam('shortDescriptionLa');
            $obj->shortDescriptionGb = Yii::app()->request->getParam('shortDescriptionGb');
            $obj->shortDescriptionKr = Yii::app()->request->getParam('shortDescriptionKr');

            $obj->contentBig = Yii::app()->request->getParam('contentBig');
            $obj->contentEn = Yii::app()->request->getParam('contentEn');
            $obj->contentTh = Yii::app()->request->getParam('contentTh');
            $obj->contentVn = Yii::app()->request->getParam('contentVn');
            $obj->contentLa = Yii::app()->request->getParam('contentLa');
            $obj->contentGb = Yii::app()->request->getParam('contentGb');
            $obj->contentKr = Yii::app()->request->getParam('contentKr');

            $obj->leagueId1 = Yii::app()->request->getParam('leagueId1');
            $obj->leagueId2 = Yii::app()->request->getParam('leagueId2');
            $obj->leagueId3 = Yii::app()->request->getParam('leagueId3');
            $obj->leagueId4 = Yii::app()->request->getParam('leagueId4');

            $obj->teamId1 = Yii::app()->request->getParam('teamId1');
            $obj->teamId2 = Yii::app()->request->getParam('teamId2');
            $obj->teamId3 = Yii::app()->request->getParam('teamId3');
            $obj->teamId4 = Yii::app()->request->getParam('teamId4');

            $obj->updateDatetime = new CDbExpression('UNIX_TIMESTAMP(current_timestamp)');
            //$obj->updateDatetime = $obj->createDatetime;

            if ($obj->update()) {
                $insertSuccess = 1;

                $list_sql = "select news.*,
l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
from news
left join lang_league as l1 on l1.leagueId = news.leagueId1
left join lang_league as l2 on l2.leagueId = news.leagueId2
left join lang_league as l3 on l3.leagueId = news.leagueId3
left join lang_league as l4 on l4.leagueId = news.leagueId4
left join lang_team as t1 on t1.tid = news.teamId1
left join lang_team as t2 on t2.tid = news.teamId2
left join lang_team as t3 on t3.tid = news.teamId3
left join lang_team as t4 on t4.tid = news.teamId4
order by newsId DESC
limit 10;
";
                //echo $list_sql;
                $list_all = Yii::app()->db->createCommand($list_sql)->queryAll();
                $lang_leauge = array();
                $lang_team = array();
                $new_list = array();
                $lang_content = array();
                $lang_title = array();
                $lang_shortDescription = array();
                foreach ($list_all as $object) {
                    $lang_title_obj = array();
                    $lang_content_obj = array();
                    $lang_shortDescription_obj = array();
                    foreach ($this->nation as $key => $nation) {
                        $lang_title_obj[$key] = $object['title' . $nation];
                        $lang_shortDescription_obj[$key] = $object['shortDescription' . $nation];
                        $lang_content_obj[$key] = $object['content' . $nation];
                        unset($object['title' . $nation]);
                        unset($object['shortDescription' . $nation]);
                        unset($object['content' . $nation]);
                    }
                    $lang_title[$object['newsId']] = $lang_title_obj;
                    $lang_shortDescription[$object['newsId']] = $lang_shortDescription_obj;
                    $lang_content[$object['newsId']] = $lang_content_obj;


                    for ($i = 1; $i <= 4; $i++) {
                        if ($object['leagueId' . $i]) {
                            $leagueObj = array();
                            $leagueObj['en'] = $object['leagueNameEn' . $i];
                            $leagueObj['th'] = $object['leagueNameTh' . $i];
                            $leagueObj['big'] = $object['leagueNameBig' . $i];
                            $leagueObj['gb'] = $object['leagueNameGb' . $i];
                            $leagueObj['kr'] = $object['leagueNameKr' . $i];
                            $leagueObj['vn'] = $object['leagueNameVn' . $i];
                            $leagueObj['la'] = $object['leagueNameLa' . $i];
                            $lang_leauge[$object['leagueId' . $i]] = $leagueObj;
                        }
                        unset($object['leagueNameEn' . $i]);
                        unset($object['leagueNameTh' . $i]);
                        unset($object['leagueNameBig' . $i]);
                        unset($object['leagueNameGb' . $i]);
                        unset($object['leagueNameKr' . $i]);
                        unset($object['leagueNameVn' . $i]);
                        unset($object['leagueNameLa' . $i]);
                    }
                    for ($i = 1; $i <= 4; $i++) {
                        if ($object['teamId' . $i]) {
                            $teamObj = array();
                            $teamObj['en'] = $object['teamNameEn' . $i];
                            $teamObj['th'] = $object['teamNameTh' . $i];
                            $teamObj['big'] = $object['teamNameBig' . $i];
                            $teamObj['gb'] = $object['teamNameGb' . $i];
                            $teamObj['kr'] = $object['teamNameKr' . $i];
                            $teamObj['vn'] = $object['teamNameVn' . $i];
                            $teamObj['la'] = $object['teamNameLa' . $i];
                            $lang_team[$object['teamId' . $i]] = $teamObj;
                        }
                        unset($object['teamNameEn' . $i]);
                        unset($object['teamNameTh' . $i]);
                        unset($object['teamNameBig' . $i]);
                        unset($object['teamNameGb' . $i]);
                        unset($object['teamNameKr' . $i]);
                        unset($object['teamNameVn' . $i]);
                        unset($object['teamNameLa' . $i]);
                    }
                    array_push($new_list, $object);
                }

                $jsonObj = array(
                    'data' => $new_list,
                    'lang_league' => $lang_leauge,
                    'lang_team' => $lang_team,
                    'lang_title' => $lang_title,
                    'lang_shortDescription' => $lang_shortDescription,
                    'lang_content' => $lang_content,
                );
                $json = json_encode($jsonObj);
                $path = Yii::getPathOfAlias("webroot");
                file_put_contents("$path/news/allNews.json", $json);
            } else {
                $insertSuccess = 2;
            }

            //print_r($obj->errors);
        }
        $com_sql = "select *from competitions order by competitionName";
        $comList = Yii::app()->db->createCommand($com_sql)->queryAll();

        $newsObj = News::model()->findByPk($newsId);
        $comArray = array();
        $leagueArray = array();
        $teamArray = array();
        for ($i = 1; $i <= 4; $i++) {
            if ($newsObj['leagueId' . $i]) {
                $sql = "select competitionId from league where leagueId=" . $newsObj['leagueId' . $i];
                $competitionId = Yii::app()->db->createCommand($sql)->queryScalar();
                array_push($comArray, $competitionId);
                $sql = "
        select *from league
        where competitionId=$competitionId
        order by (CASE WHEN league.leaguePriority IS NULL then 1 ELSE 0 END),league.leaguePriority ASC,league.leagueId
        ";
                $leagueList = Yii::app()->db->createCommand($sql)->queryAll();
                $leagueArray[$competitionId] = $leagueList;
                $leagueId = $newsObj['leagueId' . $i];
                if ($newsObj['teamId' . $i]) {
                    $sql = "select lt.*,ll.*,s.*,t.tn as teamName,
            lt.id7m as  tid7m
            from stat_table as s
            left join team as t  on t.tid = s.tid
            left join lang_team as lt on lt.tid=s.tid
            left join lang_league as ll  on ll.leagueId = s.leagueId
            where s.leagueId=$leagueId
            order by t.tn  ASC

            ";
                    $teamList = Yii::app()->db->createCommand($sql)->queryAll();
                    if (count($teamList) == 0) {
                        $sql = "
                select * from
                (
(select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn)
union
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union
(
select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
) t
order by t.teamName
";
                        $teamList = Yii::app()->db->createCommand($sql)->queryAll();
                    }
                }
                $teamArray[$leagueId] = $teamList;
            } else {
                array_push($comArray, '');
            }
        }
        $this->render('edit', array('langList' => $langList, 'comList' => $comList, 'langs' => $langs, 'newsObj' => $newsObj
            , 'comArray' => $comArray, 'leagueArray' => $leagueArray, 'teamArray' => $teamArray, 'insertSuccess' => $insertSuccess
        ));
    }

    public function actionDelete() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $newsId = Yii::app()->request->getParam('newsId');
        $sql = "UPDATE timelines SET remove='Y' WHERE `content_type`='news' AND `key_list`='$newsId'";
        Yii::app()->db->createCommand($sql)->execute();
        $success = News::model()->deleteByPk($newsId);
        $this->newsWriter();
        echo $success;
    }

    public function actionGetLeagueList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $comId = Yii::app()->request->getParam('comId');
        $sql = "
        select *from league
        where competitionId=$comId
        order by (CASE WHEN league.leaguePriority IS NULL then 1 ELSE 0 END),league.leaguePriority ASC,league.leagueId
        ";
        $leagueList = Yii::app()->db->createCommand($sql)->queryAll();
        $text = '<option value="">เลือกลีก</option>';
        foreach ($leagueList as $league) {

            $text = $text . '<option value="' . $league['leagueId'] . '">' . $league['leagueName'] . '</option>';
        }
        echo $text;
    }

    public function actionGetTeamList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $leagueId = Yii::app()->request->getParam('lid');
        $sql = "select lt.*,ll.*,s.*,t.tn as teamName,
            lt.id7m as  tid7m
            from stat_table as s
            left join team as t  on t.tid = s.tid
            left join lang_team as lt on lt.tid=s.tid
            left join lang_league as ll  on ll.leagueId = s.leagueId
            where s.leagueId=$leagueId
            order by t.tn  ASC

            ";
        $teamList = Yii::app()->db->createCommand($sql)->queryAll();
        if (count($teamList) == 0) {
            $sql = "
                select * from
                (
(select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn)
union
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union
(
select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
) t
order by t.teamName
";
            $teamList = Yii::app()->db->createCommand($sql)->queryAll();
        }
        //echo $sql;exit;


        $text = '<option value="">เลือกทีม</option>';
        foreach ($teamList as $team) {
            $text = $text . '<option value="' . $team['tid'] . '">' . $team['teamName'] . "</option>";
        }
        echo $text;
    }

}
