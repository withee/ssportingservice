<?php

class AnalyzeController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {

        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerScriptFile('../js/jsAnalyze.js');
//        $query = "select history_continent.match_id , history_match_id.round_date ,
//            history_continent.d_ta ,
//            history_continent.d_tb 
//            from history_continent LEFT JOIN history_match_id 
//            on history_continent.match_id = history_match_id.match_id 
//            where history_match_id.round_date = current_date";
        $query = "select * from history_continent_h2h LEFT JOIN history_continent_ta
on  history_continent_h2h.match_id = history_continent_ta.match_id 
and  history_continent_h2h.date_save = history_continent_ta.date_save 
and  history_continent_h2h.d_tm = history_continent_ta.d_tm 
LEFT JOIN  history_continent_tb
on  history_continent_h2h.match_id = history_continent_tb.match_id 
and  history_continent_h2h.date_save = history_continent_tb.date_save 
and  history_continent_h2h.d_tm = history_continent_tb.d_tm
where history_continent_h2h.date_save= current_date";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $this->render('index', array('list' => $list));
    }

    public function actionHistory() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $this->render('history');
    }

    public function actionGetMatch() {
        $ymd = Yii::app()->request->getParam('ymd');
//        $query = "select * 
//            from history_continent LEFT JOIN history_match_id 
//            on history_continent.match_id = history_match_id.match_id 
//            where history_match_id.round_date ='" . $ymd . "' GROUP BY history_continent.match_id";
//
//        $query = "select * from history_continent 
//                where history_continent.date_save = '" . $ymd . "' 
//                GROUP BY history_continent.match_id";

        $query = "select * from history_continent_h2h LEFT JOIN history_continent_ta
on  history_continent_h2h.match_id = history_continent_ta.match_id 
and  history_continent_h2h.date_save = history_continent_ta.date_save 
and  history_continent_h2h.d_tm = history_continent_ta.d_tm 
LEFT JOIN  history_continent_tb
on  history_continent_h2h.match_id = history_continent_tb.match_id 
and  history_continent_h2h.date_save = history_continent_tb.date_save 
and  history_continent_h2h.d_tm = history_continent_tb.d_tm
where history_continent_h2h.date_save='" . $ymd . "'";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        echo json_encode(array(
            'status' => 'success',
            'datas' => CJSON::encode($list)
        ));
    }

    public function actionShowAnalyze() {
        $date = Yii::app()->request->getParam('date');
        $matchid = Yii::app()->request->getParam('matchid');
        $query = "select * from history_continent where date_save = '$date' and match_id = $matchid";
        $list = Yii::app()->db->createCommand($query)->queryRow();
        echo json_encode(array(
            'status' => 'success',
            'datas' => CJSON::encode($list)
        ));
    }

}