<?php

class TeamController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {

        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerScriptFile('../../../js/team.js?timestamp=' . time());
        $leagueId = Yii::app()->getRequest()->getParam('leagueId');
        $sql = "select lt.*,ll.*,s.*,t.tn as teamName,
            lt.id7m as  tid7m
            from stat_table as s
            left join team as t  on t.tid = s.tid
            left join lang_team as lt on lt.tid=s.tid
            left join lang_league as ll  on ll.leagueId = s.leagueId
            where s.leagueId=$leagueId
            group by t.tid
            order by t.tn  ASC
                
            ";
        $list = Yii::app()->db->createCommand($sql)->queryAll();
        if (count($list) == 0) {
            //echo 1;exit;
            $sql = "
                select * from 
                (
(select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn)
union
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from latest_results_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union 
(
select r.tid2 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid2 = team.tid
left join lang_team on r.tid2 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
union
(
select r.tid1 as tid,team.tn as teamName,lang_team.teamNameBig,lang_team.teamNameGb,lang_team.teamNameKr,lang_team.teamNameTh,lang_team.teamNameLa,lang_team.teamNameVn,lang_team.teamNameEn,lang_team.id7m  as tid7m from next_matches_league r
left join team on  r.tid1 = team.tid
left join lang_team on r.tid1 = lang_team.tid
where r.leagueId =$leagueId
group by r.tid1
order by team.tn
)
) t
group by t.tid
order by t.teamName
";
            //echo $sql;
            $list = Yii::app()->db->createCommand($sql)->queryAll();
        }
        $league = LangLeague::model()->findByAttributes(array('leagueId' => $leagueId));
        $this->render('index', array('list' => $list, 'league' => $league));
        //echo json_encode(array('list' => $list, 'league' => $league));
    }

    public function actionGetTeamList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $this->layout = false;
        $leagueId = Yii::app()->getRequest()->getParam('id7m');
        $all = Yii::app()->getRequest()->getParam('all');
        $addition = $all == true ? '' : 'lang.id7m is null and ';
        $query = "select *from team_s 
            left join lang_team as lang on lang.id7m = team_s.id
            where $addition team_s.ln ='en'
            and team_s.leagueId = $leagueId
            order by team_s.name ASC
            ";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $this->render('getTeamList', array('list' => $list));
    }

    public function actionSave() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        header('Content-type: application/json');
        $this->layout = false;
        $id7m = Yii::app()->getRequest()->getParam('id7m');
        $tid = Yii::app()->getRequest()->getParam('tid');
        $langTeam = LangTeam::model()->findByAttributes(array('id7m' => $id7m));
        //var language = ['big', 'en', 'gb', 'kr', 'th', 'vn'];
        $query = "select *from team_s 
            where id=$id7m
                group by ln
                order by ln";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $team = Team::model()->findByAttributes(array('tid' => $tid));
        //print_r($list);

        if ($langTeam) {
            $langTeam->id7m = $id7m;
            $langTeam->tid = $tid;
            $langTeam->teamNameBig = $list[0]['name'];
            $langTeam->teamNameEn = $list[1]['name'];
            $langTeam->teamNameGb = $list[2]['name'];
            $langTeam->teamNameKr = $list[3]['name'];
            $langTeam->teamNameTh = $list[4]['name'];
            $langTeam->teamNameVn = $list[5]['name'];
            if ($langTeam->update()) {
                $array = array(
                    $langTeam->teamNameBig,
                    $langTeam->teamNameEn,
                    $langTeam->teamNameGb,
                    $langTeam->teamNameKr,
                    $langTeam->teamNameTh,
                    $langTeam->teamNameVn,
                );
                echo json_encode($array);
            }
        } else {
            $langTeam = new LangTeam();
            $langTeam->id7m = $id7m;
            $langTeam->tid = $tid;
            $langTeam->tnPk = $team['tnPk'];
            $langTeam->teamName = $team['tn'];
            $langTeam->teamNameBig = $list[0]['name'];
            $langTeam->teamNameEn = $list[1]['name'];
            $langTeam->teamNameGb = $list[2]['name'];
            $langTeam->teamNameKr = $list[3]['name'];
            $langTeam->teamNameTh = $list[4]['name'];
            $langTeam->teamNameVn = $list[5]['name'];
            $langTeam->teamName = $team['tn'];
            if ($langTeam->save()) {
                $array = array(
                    $langTeam->teamNameBig,
                    $langTeam->teamNameEn,
                    $langTeam->teamNameGb,
                    $langTeam->teamNameKr,
                    $langTeam->teamNameTh,
                    $langTeam->teamNameVn,
                );
                echo json_encode($array);
            }
            //print_r($langTeam->errors);
        }
    }

    public function actionRemove() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $id7m = Yii::app()->request->getParam('id7m');
        if ($id7m) {
            echo LangTeam::model()->deleteAllByAttributes(array('id7m' => $id7m));
        }
    }

    public function actionUpdateTeam() {
        $key = Yii::app()->request->getParam('key');
        $act = Yii::app()->request->getParam('act');
        $lid7m = Yii::app()->request->getParam('lid7m');
        $teamname = Yii::app()->request->getParam('teamname');
        $tnpk = Yii::app()->request->getParam('tnpk');
        $tid = Yii::app()->request->getParam('tid');
        $th = Yii::app()->request->getParam('th');
        $en = Yii::app()->request->getParam('en');
        $big = Yii::app()->request->getParam('big');
        $gb = Yii::app()->request->getParam('gb');
        $kr = Yii::app()->request->getParam('kr');
        $vn = Yii::app()->request->getParam('vn');
        $la = Yii::app()->request->getParam('la');
        $ln = array("en" => $en, "th" => $th, "big" => $big, "gb" => $gb, "kr" => $kr, "vn" => $vn, "la" => $la);
        $newid = 0;
        if ($newid == 0) {
            $criteria = new CDbCriteria;
            $criteria->order = "id DESC";
            $criteria->limit = 1;
            $maxid = TeamS::model()->find($criteria);
            if ((int) $maxid->id >= 30000) {
                $newid = (int) $maxid->id + 1;
            } else {
                $newid = 30000;
            }
        }
        foreach ($ln as $key => $l) {
            $old = TeamS::model()->find("name='$l' and leagueid=$lid7m and ln='$key'");
            if (!empty($l))
                if (!empty($old)) {
                    $newid = (int) $old->id;
                } else {
                    $old = new TeamS();
                    $old->id = $newid;
                    $old->name = $l;
                    $old->leagueId = $lid7m;
                    $old->ln = $key;
                    $old->save();
                }
        }

        $lngteam = LangTeam::model()->find("tid=$tid and id7m=$newid");

        if (empty($lngteam)) {
            $lngteam = new LangTeam();
        }
       // echo $tnpk."\n";
        $lngteam->teamName = $teamname;
        $lngteam->tnPk = $tnpk;
        $lngteam->tid = (int) $tid;
        $lngteam->id7m = (int) $newid;
        $lngteam->teamNameBig = $big;
        $lngteam->teamNameEn = $en;
        $lngteam->teamNameGb = $gb;
        $lngteam->teamNameKr = $kr;
        $lngteam->teamNameLa = $la;
        $lngteam->teamNameTh = $th;
        $lngteam->teamNameVn = $vn;
        $lngteam->save();
       var_dump($lngteam->getErrors());
        
        echo json_encode(array("result" => $lngteam));
    }

}