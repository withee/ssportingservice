<?php

require '../DBConfig.php';

class reportController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $db = DBConfig::getConnection();

        $sql = "SELECT  `id`, `message`, `report` FROM wall_status WHERE `report` > 0 AND `remove` = 'N'";
        $stmt = $db->query($sql);
        $report["wall_status"] = $stmt->fetchAll(5);
        $sql = "SELECT  `id`, `message`, `report` FROM timelines_reply WHERE `report` > 0 AND `remove` = 'N'";
        $stmt = $db->query($sql);
        $report["timelines_reply"] = $stmt->fetchAll(5);
        $sql = "SELECT  `id`, `desc` as 'message', `report` FROM board WHERE `report` > 0 AND `remove` = 'N'";
        $stmt = $db->query($sql);
        $report["board"] = $stmt->fetchAll(5);

        $this->render('index', array('report' => $report));
    }

    public function actionDelete(){
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $id = Yii::app()->request->getParam('id');
        $type = Yii::app()->request->getParam('type');
        $sql = "UPDATE ".$type." SET remove='Y' WHERE `id`='".$id."'";

        Yii::app()->db->createCommand($sql)->execute();
        //echo "success";
        echo $sql;
    }

}
