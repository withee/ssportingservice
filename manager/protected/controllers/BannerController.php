<?php

class BannerController extends Controller
{
	public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        
        $this->render('index');
    }
}