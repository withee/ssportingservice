<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function init() {
        $this->layout = 'main2';
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if ($session['admin']) {
            $this->render("index");
        } else {
            $this->render('index-no-login');
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        if (Yii::app()->request->isPostRequest) {
            $username = Yii::app()->getRequest()->getParam('username');
            $password = Yii::app()->getRequest()->getParam('password');

            if ($username == 'x3' && md5($password) == 'fe87676996dcd16d37298b49e581eaed') {
                $session = new CHttpSession;
                $session->open();
                $session['admin'] = true;
//                echo $session['admin'];
//                exit;
                $this->redirect('/site/index');
            } else {
                $this->redirect('/site/login');
            }
        } else {
            $this->render('login');
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $session = new CHttpSession;
        $session->open();
        $session->destroy();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionCompetition() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerScriptFile('../js/competition.js?timestamp=' . time());
        $query = "select *from competitions 
left join lang_competition on lang_competition.cid = competitions.competitionId
order by competitions.competitionName";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $this->render('competition', array('list' => $list));
    }

    public function actionGetCompetitionList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $this->layout = false;
        $all = Yii::app()->getRequest()->getParam('all');
        $addition = $all ? '' : 'lang.id7m is null and ';
        $query = "select id,name,lang.id7m
            from area
            left join lang_competition as lang on lang.id7m = area.id
            where $addition area.ln='en' and area.gameType=1
            group by area.id
            order by name ASC
            ";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $this->render('getCompetitionList', array('list' => $list));
    }

    public function actionSaveCom() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        header('Content-type: application/json');
        $this->layout = false;
        $id = Yii::app()->getRequest()->getParam('competitionId');
        $cid = Yii::app()->getRequest()->getParam('original');
        $query = "select *from area 
            
            where  id=$id 
                order by ln ASC";
        $list = Yii::app()->db->createCommand($query)->queryAll();
        $com = Competitions::model()->findByAttributes(array('competitionId' => $cid));
        //big,en,gb,kr,th,vn
        //var_dump($langCompetition);exit;
        $langCompetition = LangCompetition::model()->findByAttributes(array('cid' => $cid));

        if (count($list) == 6) {

            if ($langCompetition) {
                $langCompetition->id7m = $list[0]['id'];
                $langCompetition->comNameBig = $list[0]['name'];
                $langCompetition->comNameEn = $list[1]['name'];
                $langCompetition->comNameGb = $list[2]['name'];
                $langCompetition->comNameKr = $list[3]['name'];
                $langCompetition->comNameTh = $list[4]['name'];
                $langCompetition->comNameVn = $list[5]['name'];

                if ($langCompetition->update()) {
                    $array = array(
                        $langCompetition->comNameBig,
                        $langCompetition->comNameEn,
                        $langCompetition->comNameGb,
                        $langCompetition->comNameKr,
                        $langCompetition->comNameTh,
                        $langCompetition->comNameVn,
                    );
                    echo json_encode($array);
                }
            } else {
                $langCompetition = new LangCompetition();
                $langCompetition->id7m = $list[0]['id'];
                $langCompetition->cid = $cid;
                $langCompetition->comName = $com['competitionName'];
                $langCompetition->comNamePk = $com['competitionNamePk'];
                $langCompetition->comNameBig = $list[0]['name'];
                $langCompetition->comNameEn = $list[1]['name'];
                $langCompetition->comNameGb = $list[2]['name'];
                $langCompetition->comNameKr = $list[3]['name'];
                $langCompetition->comNameTh = $list[4]['name'];
                $langCompetition->comNameVn = $list[5]['name'];
                if ($langCompetition->save()) {
                    $array = array(
                        $langCompetition->comNameBig,
                        $langCompetition->comNameEn,
                        $langCompetition->comNameGb,
                        $langCompetition->comNameKr,
                        $langCompetition->comNameTh,
                        $langCompetition->comNameVn,
                    );
                    echo json_encode($array);
                }
            }
        }
    }

    public function actionPushList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        if (Yii::app()->getRequest()->getParam('submit')) {
            $senddatetime = Yii::app()->getRequest()->getParam('senddatetime');
            $message = Yii::app()->getRequest()->getParam('message');
            $pushObj = new PushListMessage();
            $pushObj->message = $message;
            $pushObj->senddatetime = new CDbExpression("'" . $senddatetime . "'- interval 7 hour");
            $pushObj->createdatetime = new CDbExpression('NOW()');
            $pushObj->save();
        }
        $criteria = new CDbCriteria();
        $criteria->order = 'messageid DESC';
        $criteria->limit = 30;
        $query = "
            select messageid,message,createdatetime + interval 7 hour as createdatetime,senddatetime + interval 7 hour as senddatetime,ios_sent,android_sent from push_list_message
            order by messageid DESC limit 30
            ";
        $list = Yii::app()->db->createCommand($query)->queryall();
        $this->render('pushList', array('list' => $list));
    }

    public function actionGetChartUsedList(){
        $date = Yii::app()->request->getParam('date', null);
        $date2 = Yii::app()->request->getParam('date2', null);

        if(!empty($date)&&!empty($date2)) {
            $date = new DateTime($date);
            $date2 = new DateTime($date2);
        }else{
            $date= new DateTime('Now');
            $date->setDate($date->format('Y'),$date->format('m'),1);
            $date2 = new DateTime($date->format('Y-m-d'));
            $date2->modify("+1 month");
            $date2->modify("-1 day");
        }

        $os = array('ios', 'android', 'web');
        $list = array();
        foreach ($os as $o) {
            $beforeCount=0;
            $sql = "
            select DATE_FORMAT(used_at,'%Y-%m-%d') as day_at, count(DISTINCT(apple_id)) as count_apple_id from  used_list
            where platform ='$o' and date(used_at)>='".$date->format('Y-m-d')."' and date (used_at)<'".$date2->format('Y-m-d')."' + interval 1 day
            group by date(used_at)
            ";
            $rows = Yii::app()->db->createCommand($sql)->queryAll();
            $row = CHtml::listData($rows,'day_at','count_apple_id');
            $moRows = array();
            for($dateFor= new DateTime($date->format('Y-m-d'));$dateFor<=$date2;$dateFor->modify("+1 day")){
//                $dateIndex= new DateTime($row['create_at']);
                if(!empty($row[$dateFor->format('Y-m-d')])) {
                    $moRows[$dateFor->format('Y-m-d')] = $row[$dateFor->format('Y-m-d')];
                }else{
                    $moRows[$dateFor->format('Y-m-d')] =0;
                }
            }
            $list[$o] = $moRows;
        }
        echo json_encode(array(
            'status' => 'success',
            'data' => $list,
        ));
    }

    public function actionGetChartNotification(){
        $date = Yii::app()->request->getParam('date', null);
        $date2 = Yii::app()->request->getParam('date2', null);

        if(!empty($date)&&!empty($date2)) {
            $date = new DateTime($date);
            $date2 = new DateTime($date2);
        }else{
            $date= new DateTime('Now');
            $date->setDate($date->format('Y'),$date->format('m'),1);
            $date2 = new DateTime($date->format('Y-m-d'));
            $date2->modify("+1 month");
            $date2->modify("-1 day");
        }

        $os = array('ios', 'android', 'web');
        $list = array();
        foreach ($os as $o) {
            $beforeCount=0;
            $sql = "
            select DATE_FORMAT(create_at,'%Y-%m-%d') as day_at, count(DISTINCT(apple_id)) as count_apple_id from  request_notification
            where platform ='$o'  and match_id!=0 and date(create_at)>='".$date->format('Y-m-d')."' and date (create_at)<'".$date2->format('Y-m-d')."' + interval 1 day
            group by date(create_at)
            ";
            $rows = Yii::app()->db->createCommand($sql)->queryAll();
            $row = CHtml::listData($rows,'day_at','count_apple_id');
            $moRows = array();
            for($dateFor= new DateTime($date->format('Y-m-d'));$dateFor<=$date2;$dateFor->modify("+1 day")){
//                $dateIndex= new DateTime($row['create_at']);
                if(!empty($row[$dateFor->format('Y-m-d')])) {
                    $moRows[$dateFor->format('Y-m-d')] = $row[$dateFor->format('Y-m-d')];
                }else{
                    $moRows[$dateFor->format('Y-m-d')] =0;
                }
            }
            $list[$o] = $moRows;
        }
        echo json_encode(array(
            'status' => 'success',
            'data' => $list,
        ));
    }

    public function actionUsedList() {

        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $os = array('ios', 'android', 'web');
        $current_mm = date('m');
        $current_yyyy = date('Y');

        $mm = Yii::app()->request->getParam('mm', $current_mm);
        $yyyy = Yii::app()->request->getParam('yyyy', $current_yyyy);
        $date = Yii::app()->request->getParam('date', null);
        $date2 = Yii::app()->request->getParam('date2', null);
        if(!empty($date)&&!empty($date2)) {
            $date = new DateTime($date);
            $date2 = new DateTime($date2);
        }else{
            $date= new DateTime('Now');
            $date->setDate($date->format('Y'),$date->format('m'),1);
            $date2 = new DateTime($date->format('Y-m-d'));
            $date2->modify("+1 month");
            $date2->modify("-1 day");
        }

        $yearListQuery = "
            select YEAR(create_at) from request_notification 
            group by YEAR(create_at)";
        $yearList = Yii::app()->db->createCommand($yearListQuery)->queryColumn();
        $monthList = array(
            null, "January", "February", "March", "April", "May",
            "June", "July", "August", "September", "October",
            "November", "December"
        );
        $list = array();
        $beforeCount=0;
        foreach ($os as $o) {
            $beforeCount=0;
            $sql = "
            select DATE_FORMAT(create_at,'%Y-%m-%d') as day_at, count(DISTINCT(apple_id)) as count_apple_id from  request_notification
            where platform ='$o'  and match_id!=0 and request_notification.create_at BETWEEN '".$date->format('Y-m-d')."' and '".$date2->format('Y-m-d')."' group by date(create_at)";
            $rows = Yii::app()->db->createCommand($sql)->queryAll();
            $row = CHtml::listData($rows,'day_at','count_apple_id');
            $moRows = array();
            for($dateFor= new DateTime($date->format('Y-m-d'));$dateFor<=$date2;$dateFor->modify("+1 day")){
//            foreach ($rows as $row) {
//                if(!empty($row['count_apple_id'])) {
                    $moRows[$dateFor->format('Y-m-d')] = array();
                    $moRows[$dateFor->format('Y-m-d')]['value'] =0;
                    if(!empty($row[$dateFor->format('Y-m-d')])) {
                        if ($row[$dateFor->format('Y-m-d')] > 600) {
                            $moRows[$dateFor->format('Y-m-d')]['value'] = $row[$dateFor->format('Y-m-d')];
                            $moRows[$dateFor->format('Y-m-d')]['class'] = 'success';
                        } elseif ($row[$dateFor->format('Y-m-d')] > 400) {
                            $moRows[$dateFor->format('Y-m-d')]['value'] = $row[$dateFor->format('Y-m-d')];
                            $moRows[$dateFor->format('Y-m-d')]['class'] = 'info';
                        } elseif ($row[$dateFor->format('Y-m-d')] > 200) {
                            $moRows[$dateFor->format('Y-m-d')]['value'] = $row[$dateFor->format('Y-m-d')];
                            $moRows[$dateFor->format('Y-m-d')]['class'] = 'warning';
                        } else {
                            $moRows[$dateFor->format('Y-m-d')]['value'] = $row[$dateFor->format('Y-m-d')];
                            $moRows[$dateFor->format('Y-m-d')]['class'] = '';
                        }
                        if ($beforeCount == 0) {
                            $moRows[$dateFor->format('Y-m-d')]['status'] = 'icon-pause';
                        } elseif ($beforeCount > $row[$dateFor->format('Y-m-d')]) {
                            $moRows[$dateFor->format('Y-m-d')]['status'] = 'icon-chevron-down';
                        } elseif ($beforeCount < $row[$dateFor->format('Y-m-d')]) {
                            $moRows[$dateFor->format('Y-m-d')]['status'] = 'icon-chevron-up';
                        } else {
                            $moRows[$dateFor->format('Y-m-d')]['status'] = 'icon-pause';
                        }
                        $beforeCount = $row[$dateFor->format('Y-m-d')];
                    }else{
                        $moRows[$dateFor->format('Y-m-d')]['value'] = 0;
                        $moRows[$dateFor->format('Y-m-d')]['class'] = '';
                        $moRows[$dateFor->format('Y-m-d')]['status'] = 'icon-question-sign';
                    }
//                }
//                $moRows[(int) $row['day_at']] =$row['count_apple_id'];
            }
            $list[$o] = $moRows;
        }
        $color = array(
            'ios' => 'Cyan',
            'android' => 'yellowgreen',
            'wp' => 'orange',
        );
        $this->render('usedList', array(
            'mm' => $mm,
            'yyyy' => $yyyy,
            'yearList' => $yearList,
            'monthList' => $monthList,
            'os' => $os,
            'list' => $list,
            'color' => $color,
            'date'=>$date,
            'date2'=>$date2
        ));
    }

    public function actionRemove() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $id7m = Yii::app()->request->getParam('id7m');
        if ($id7m) {
            echo LangCompetition::model()->deleteAllByAttributes(array('id7m' => $id7m));
        }
    }

    public function actionRemovePushList() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $messageid = Yii::app()->request->getParam('messageid');
        if ($messageid) {
            echo PushListMessage::model()->deleteByPk($messageid);
        }
    }

    public function actionHighlight() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $current_date = new CDbExpression('DATE(NOW())');
        $yesterday_date = Yii::app()->db->createCommand("select current_date - interval  1 day")->queryScalar();
        $date_selected = Yii::app()->request->getParam('date_selected', $current_date);
        $live_league_sql = "
select competitions.competitionName,live_league.leagueId,live_league.subleagueId,live_league.ln,live_league.subleagueName from live_league 
left join league on league.leagueId = live_league.leagueId
left join competitions on competitions.competitionId = live_league.competitionId
where live_league.date='$date_selected' and live_league.new='Y'
group by live_league.subleagueId 
order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC
            ";
        $live_match_sql = "
            select date,mid,lid as subleagueId,_lid  as leagueId,hn,gn,hid,gid,s1,s2,sid,kid as competitionId from live_match
            where showDate='$date_selected' and new='Y' and (sid>=5) group by mid order by date ASC,mid ASC
            ";
//        echo $live_league_sql;
//        echo "<br>";
//        echo $live_match_sql;
//        //echo $live_match_sql;
//        exit;
        $live_league_list = Yii::app()->db->createCommand($live_league_sql)->queryAll();
        $live_match_list = Yii::app()->db->createCommand($live_match_sql)->queryAll();
        $this->render('highlight', array(
            'live_league_list' => $live_league_list,
            'live_match_list' => $live_match_list,
            'yesterday_date' => $yesterday_date,
        ));
    }

    public function actionGetHighLight() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        header('Content-type: application/json');
        $mid = Yii::app()->request->getParam('mid');
        $sql = "select *from video_highlight where mid=$mid";
        $list = Yii::app()->db->createCommand($sql)->queryAll();
        echo json_encode($list);
    }

    public function actionAddVideoHighlight() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $mid = Yii::app()->request->getParam('mid');
        $hid = Yii::app()->request->getParam('hid');
        $gid = Yii::app()->request->getParam('gid');
        $leagueId = Yii::app()->request->getParam('leagueId');
        $competitionId = Yii::app()->request->getParam('competitionId');
        $size = 3;
        $datetime = new CDbExpression('current_timestamp');
        $list = array();
        $saved = FALSE;
        for ($i = 0; $i < $size; $i++) {
            $title = Yii::app()->request->getParam('title' . ($i + 1));
            $content = Yii::app()->request->getParam('content' . ($i + 1));
            $id = Yii::app()->request->getParam('id' . ($i + 1));
            $obj = new VideoHighlight();

            if ($id) {
                $obj = VideoHighlight::model()->findByPk($id);

                if ($obj && $content) {
                    $obj->title = $title;
                    $obj->content = $content;
                    $obj->create_datetime = $datetime;
                    $obj->update();
                    $video = array(
                        'video_id' => $id,
                        'title' => $title,
                        'content' => $content,
                    );
                    array_push($list, $video);
                } else if ($obj) {
                    $obj->delete();
                }
            } else {
                $obj->mid = $mid;
                $obj->title = $title;
                $obj->content = $content;
                $obj->create_datetime = $datetime;
                $obj->leagueId = $leagueId;
                $obj->hid = $hid;
                $obj->gid = $gid;
                $obj->competitionId = $competitionId;
                if ($obj->save()) {
                    $id = Yii::app()->db->getLastInsertID();
                    $video = array(
                        'video_id' => $id,
                        'title' => $title,
                        'content' => $content,
                    );
                    array_push($list, $video);
                    //push video to timelines
                    // $nowday = new CDbExpression('current_timestamp');
                    $nowday = date('Y-m-d H:i:s');
                    $stmp = new CDbExpression('UNiX_TIMESTAMP(current_timestamp)');
                    $keygen = 'highlight' . $obj->video_id;
                    $tlsql = "INSERT INTO `timelines` (`fb_uid`, `content_type`, `key_list`, `created_at`, `updated_at`, `stamped_at`,`type`,`keygen`) VALUES ('broadcast', 'highlight', '{$obj->video_id}', '$nowday', '$nowday', $stmp,6,'$keygen');";
                    $success = Yii::app()->db->createCommand($tlsql)->execute();

                    $saved = TRUE;
                }
            }
        }
        //$root = getcwd();
        //echo $root;
        $objCreate = fopen("../video_highlight/$mid.json", 'w', 775);
        if ($objCreate) {
            fwrite($objCreate, json_encode($list));
            fclose($objCreate);
        }
        $sql_competition = "select *from video_highlight where competitionId=$competitionId order by  video_id DESC limit 50";
        $sql_league = "select *from video_highlight where leagueId=$leagueId order by video_id DESC limit 50";
        $sql_team1 = "select *from video_highlight where gid =$gid or hid = $gid order by video_id DESC limit 50";
        $sql_team2 = "select *from video_highlight where gid =$gid or hid = $gid order by video_id DESC limit 50";
        $competition_video_list = Yii::app()->db->createCommand($sql_competition)->queryAll();
        $league_video_list = Yii::app()->db->createCommand($sql_league)->queryAll();
        $team1_video_list = Yii::app()->db->createCommand($sql_team1)->queryAll();
        $team2_video_list = Yii::app()->db->createCommand($sql_team2)->queryAll();
        $com_file = fopen("../video_highlight/competition/$competitionId.json", 'w', 775);
        if ($com_file) {
            fwrite($com_file, json_encode($competition_video_list));
        }
        $league_file = fopen("../video_highlight/league/$leagueId.json", 'w', 775);
        if ($league_file) {
            fwrite($league_file, json_encode($league_video_list));
        }
        $team1_file = fopen("../video_highlight/team/$hid.json", 'w', 775);
        if ($team1_file) {
            fwrite($team1_file, json_encode($team1_video_list));
        }
        $team2_file = fopen("../video_highlight/team/$gid.json", 'w', 775);
        if ($team2_file) {
            fwrite($team2_file, json_encode($team2_video_list));
        }

        echo json_encode(array('success' => $saved));
    }

    public function actionTestAndroidNoti() {
        $array = array();
        $array['message'] = 'Pele vs Cat';
        $array['mid'] = Yii::app()->request->getParam('mid');
        $array['sid'] = Yii::app()->request->getParam('sid', 2);
        $device_id = Yii::app()->request->getParam('device_id');
        //define("GOOGLE_API_KEY", "AIzaSyBTRsP5AwiKHi9l7ZLkTnATOV8Tj2pLg6s");
        define('GOOGLE_API_KEY', "AIzaSyDmNlVG1YUuBIkdUouJWT8ipWwjVLmcuok");
        define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");
        $data = json_decode(file_get_contents('../match_1200614.json'));
        //echo json_encode($data);
        $fields = array(
            'registration_ids' => array($device_id),
            'data' => $data,
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Problem occurred: ' . curl_error($ch));
        }

        curl_close($ch);

        //echo $result;
        $results = json_decode($result);
        $results->send_data = $data;
        echo json_encode($results);
    }

    public function actionLotteryAndroidNotification() {
        $array = array();
        $array['message'] = 'Test Lottery';
        $device_id = Yii::app()->request->getParam('device_id');
        define("GOOGLE_API_KEY", "AIzaSyBTRsP5AwiKHi9l7ZLkTnATOV8Tj2pLg6s");
        define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");
        $fields = array(
            'registration_ids' => array($device_id),
            'data' => $array,
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Problem occurred: ' . curl_error($ch));
        }

        curl_close($ch);
        echo $result;
    }

    public function actionManualAdd() {
        $langlist = $_REQUEST["langlist"];
        $name = $_REQUEST["completname"];
        $compettid = $_REQUEST["completid"];
        $zone = (int) $_REQUEST["zone"];
        $result = array();
        if ($langlist) {
            $langlist = json_decode($langlist);
            $sqlmaxid = "SELECT MAX(id) as lastid FROM area";
            $maxid = Yii::app()->db->createCommand($sqlmaxid)->queryAll();
            $currentid = 0;

            if ((int) $maxid[0]["lastid"] < 500) {
                $currentid = 500;
            } else {
                $currentid = $maxid[0]["lastid"] + 1;
            }
            // echo $currentid;

            foreach ($langlist as $key => $lng) {
                // $newrow = "INSERT INTO area (id,sortId,name,pic,gameType,ln) VALUES($currentid,$zone,'$lng','$currentid.jpg',1,'$key')";
                //  $resul = Yii::app()->db->createCommand($newrow)->queryAll();
                $area = new Area();
                $area->id = $currentid;
                $area->sortId = $zone;
                $area->name = $lng;
                $area->pic = $currentid . ".jpg";
                $area->gameType = 1;
                $area->ln = $key;
                $area->save();
                $result["area"][$key] = $area;
            }

            $compettition = Competitions::model()->find("competitionId=$compettid");
            $lngCompt = new LangCompetition();
            $lngCompt->comName = $name;
            $lngCompt->id7m = $currentid;
            $lngCompt->cid = $compettid;
            $lngCompt->comNamePk = $compettition->competitionNamePk;
            $lngCompt->comNameBig = $langlist->big;
            $lngCompt->comNameEn = $langlist->en;
            $lngCompt->comNameGb = $langlist->gb;
            $lngCompt->comNameKr = $langlist->kr;
            $lngCompt->comNameLa = $langlist->la;
            $lngCompt->comNameTh = $langlist->th;
            $lngCompt->comNameVn = $langlist->vn;
            $lngCompt->save();
            $result["langconpettition"] = $lngCompt;
        }
        echo CJSON::encode($result);
    }

}
