﻿<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsageReportController
 *
 * @author mrsyrop
 */
require "../../Carbon/src/Carbon/Carbon.php";

use Carbon\Carbon;

class UsagereportController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {

        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $this->render('index');
    }

    public function actionRegister() {
        $day = Yii::app()->request->getParam('day');
        $day2 = Yii::app()->request->getParam('day2');
        $sort = Yii::app()->request->getParam('sort');

        if (empty($sort)) {
            $sort = 'pts';
        }

        if (empty($day)) {
            $day = date("Y-m-d");
        }

        if (empty($day2)) {
            $day2 = date("Y-m-d");
        }
        //echo $day.":".$day2;
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $start = date("Y-m-d", strtotime($day));
        $stop = date("Y-m-d", strtotime($day2 . "+24 hour"));

        $criteria = new CDbCriteria();
        $criteria->condition = "reg_at BETWEEN '$start' AND '$stop'";
        $criteria->order = $sort . " desc";
        $userlist = FacebookUser::model()->findAll($criteria);
        $criteria = new CDbCriteria();
        $criteria->condition = 'uid>0';
        $number = FacebookUser::model()->count($criteria);

        $this->render('register', array('userlist' => $userlist, 'number' => $number, 'numberofday' => count($userlist), 'day' => $day, 'day2' => $day2));
    }

    public function actionDevice() {
        $day = Yii::app()->request->getParam('day');
        if (empty($day)) {
            $day = date("Y-m-d");
        }
        //echo $day;
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");



        $tomorrow = strtotime($day . "+1 day");
        $nowday = strtotime($day);

        //echo $nowday.",".$tomorrow;
        $criteria = new CDbCriteria();
        $criteria->condition = "platform='web'";
        $criteria->group = "fb_uid";
        $alldayweb = Bet::model()->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "platform='ios'";
        $criteria->group = "fb_uid";
        $alldayios = Bet::model()->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "platform='android'";
        $criteria->group = "fb_uid";
        $alldayandroid = Bet::model()->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "platform='web' AND betDatetime BETWEEN $nowday AND $tomorrow";
        $criteria->group = "fb_uid";
        $dayweb = Bet::model()->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "platform='ios' AND betDatetime BETWEEN $nowday AND $tomorrow";
        $criteria->group = "fb_uid";
        $dayios = Bet::model()->count($criteria);

        $criteria = new CDbCriteria();
        $criteria->condition = "platform='android' AND betDatetime BETWEEN $nowday AND $tomorrow";
        $criteria->group = "fb_uid";
        $dayandroid = Bet::model()->count($criteria);

        $this->render('device', array('alldayweb' => $alldayweb, 'alldayios' => $alldayios, 'alldayandroid' => $alldayandroid, 'dayweb' => $dayweb, 'dayios' => $dayios, 'dayandroid' => $dayandroid, 'day' => $day));
    }

    public function actionMatch() {
        $day = Yii::app()->request->getParam('day');
        if (empty($day)) {
            $day = date("Y-m-d");
        }
        //echo $day;
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $criteria = new CDbCriteria();
        $criteria->condition = "DATE(show_date)='$day'";
        $criteria->order = "show_date";
        $timelinesgame = TimelinesGame::model()->findAll($criteria);
        $playstamp = array();
        $matchelist = array();
        foreach ($timelinesgame as $tlg) {
            $criteria = new CDbCriteria();
            $criteria->condition = "mid='{$tlg['mid']}'";
            $play = Bet::model()->count($criteria);
            $playstamp[$tlg->mid] = $play;
            $matchelist[$tlg->mid] = $tlg;
            //echo $play;
        }
        arsort($playstamp);
        $timelinesgames = array();
        foreach ($playstamp as $key => $sor) {
            $timelinesgames[] = $matchelist[$key];
        }

        $this->render('match', array('matches' => $timelinesgames, 'playstamp' => $playstamp, 'day' => $day));
    }

    public function actionTime() {
        $day = Yii::app()->request->getParam('day');
        if (empty($day)) {
            $day = date("Y-m-d");
        }
        $day2 = Yii::app()->request->getParam('day2');
        if (empty($day2)) {
            $day2 = date("Y-m-d");
        }
        //echo $day;
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");


        //$carbon = Carbon::create(date('Y', strtotime($day)), date('m', strtotime($day)), date('d', strtotime($day)), 0, 0, 0);
        //echo $carbon->timestamp . "\n";
        $playlist = array('01:00:00' => 0, '02:00:00' => 0, '03:00:00' => 0, '04:00:00' => 0, '05:00:00' => 0, '06:00:00' => 0, '07:00:00' => 0, '08:00:00' => 0, '09:00:00' => 0, '10:00:00' => 0, '11:00:00' => 0, '12:00:00' => 0, '13:00:00' => 0, '14:00:00' => 0, '15:00:00' => 0, '16:00:00' => 0, '17:00:00' => 0, '18:00:00' => 0, '19:00:00' => 0, '20:00:00' => 0, '21:00:00' => 0, '22:00:00' => 0, '23:00:00' => 0, '00:00:00' => 0);

        $lasttime = strtotime($day2 . "+1 day");
        $stamp = strtotime($day);
        $summary = 0;
        while ($stamp < $lasttime) {
            //$stamp = $carbon->timestamp;
            //echo $stamp . "\n";
            $start = $stamp;
            $stop = $start;
            for ($hour = 1; $hour <= 24; $hour++) {
                $stop = ($hour * 3600) + $stamp;
                //echo $start . "-" . $stop . "=" . ($stop - $start) . "\n";
                $criteria = new CDbCriteria();
                $criteria->condition = "betDatetime BETWEEN $start AND $stop";
                $play = Bet::model()->count($criteria);
                $hkey = date("H:i:s", $stop);
                //echo "'".$hkey."'"."=>0,";
                $playlist["$hkey"] += (int) $play;
                $summary+=(int) $play;
                $start = $stop;
            }

            $stamp = $stop;
        }
        $this->render('time', array('times' => $playlist, 'summary' => $summary, 'day' => $day, 'day2' => $day2));
    }

    public function actionDiamond() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $page = (int) Yii::app()->request->getParam('page');
        $limit = 100;
        if (empty($page)) {
            $page = 1;
        }
        $start = ($page - 1) * $limit;
        $criteria = new CDbCriteria();
        $criteria->order = "diamond desc";
        $criteria->offset = $start;
        $criteria->limit = $limit;
        $userlist = User::model()->findAll($criteria);

        $allpage = 5;
        $allpage = User::model()->count();
        $this->render('diamond', array('userlist' => $userlist, 'page' => $page, 'start' => $start, 'allpages' => $allpage));
    }

    public function actionSendmsg() {
        $uid = Yii::app()->request->getParam('uid');
        $msg = Yii::app()->request->getParam('message');


        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $criteria = new CDbCriteria();
        $criteria->condition = "uid=$uid";
        $admin = User::getAdmin();
        $user = User::model()->find($criteria);
        $result = array('success' => FALSE, 'desc' => '');
        if ($user && $admin) {
            $success = $this->sendMessage($admin, $user, $msg);
            if ($success) {
                $result['success'] = TRUE;
                $result['desc'] = "message has been send";
            } else {
                $result['desc'] = "failed";
            }
        } else {
            $result['desc'] = "user not found";
        }
        echo json_encode($result);
    }

    function sendMessage($admin, $user, $msg) {
        $message = new MessageBox();
        $message->message = $msg;
        $message->sender = $admin->uid;
        $message->receiver = $user->uid;
        $message->key = $message->sender . $message->receiver;
        $nowday = date('Y-m-d H:i:s');
        $message->send_at = $nowday;
        $message->stamp_at = strtotime($nowday);
        $success = $message->save();
        return $success;
    }

    function actionExchangDiamond() {
        $uid = Yii::app()->request->getParam('uid');
        $value = (int) Yii::app()->request->getParam('value');
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $result = array('success' => FALSE, 'desc' => '');
        $criteria = new CDbCriteria();
        $criteria->condition = "uid=$uid";
        $user = User::model()->find($criteria);
        if ((int) $user->diamond > $value) {
            $succes = $this->exchang($user, $value);
            if ($succes) {
                $result['success'] = true;
                $result['desc'] = "Exchange completed";
                $admin = User::getAdmin();
                $msg = "คุณได้แลกเปลี่ยน 1500 diamond เป็นรางวัล ขณะนี้ทาง ssporting กำลังดำเนินการจัดส่งอยู่ค่ะ";
                $success = $this->sendMessage($admin, $user, $msg);
            } else {
                $result['desc'] = "user objetc or statement object not saved";
            }
        } else {
            $result['desc'] = "not enough diamond";
        }

        echo json_encode($result);
    }

    function exchang($user, $prize) {
        $beforediamond = $user->diamond;
        $user->diamond -=floatval($prize);
        $success = $user->save();
        if ($success) {
            $statement = new TotalStatement();
            $statement->uid = $user->uid;
            $statement->statement_type = 'award';
            $statement->before_diamond = $beforediamond;
            $statement->diamond_outcome = floatval($prize);
            $statement->balance_diamond = $user->diamond;
            $statement->result = 'win';
            $statement->note = "แลกรางวัลด้วยเพขร";
            $statement->statement_timestamp = date('Y-m-d H:i:s');
            $success = $statement->save();
        }
        return $success;
    }

    function reward($user, $prize,$type) {
        $success=false;
        if($type=='diamond') {
            $beforediamond = $user->diamond;
            $user->diamond += floatval($prize);
            $success = $user->save();
            if ($success) {
                $statement = new TotalStatement();
                $statement->uid = $user->uid;
                $statement->statement_type = 'award';
                $statement->before_diamond = $beforediamond;
                $statement->diamond_income = floatval($prize);
                $statement->balance_diamond = $user->diamond;
                $statement->result = 'win';
                $statement->note = "แจกรางวัลด้วยเพขร";
                $statement->statement_timestamp = date('Y-m-d H:i:s');
                $success = $statement->save();
            }
        }else if($type=='sgold'){
            $beforesgold = $user->sgold;
            $user->sgold += floatval($prize);
            $success = $user->save();
            if ($success) {
                $statement = new TotalStatement();
                $statement->uid = $user->uid;
                $statement->statement_type = 'award';
                $statement->before_sgold = $beforesgold;
                $statement->sgold_income = floatval($prize);
                $statement->balance_sgold = $user->diamond;
                $statement->result = 'win';
                $statement->note = "แจกรางวัลด้วย sgold";
                $statement->statement_timestamp = date('Y-m-d H:i:s');
                $success = $statement->save();
            }
        }
        return $success;
    }

    public function actionPrize() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $uid = (int) Yii::app()->request->getParam('uid');
        $prize = (int) Yii::app()->request->getParam('prize');
        $msg = Yii::app()->request->getParam('msg');
        $event = Yii::app()->request->getParam('event');
        $day = Yii::app()->request->getParam('day');
        $type=Yii::app()->request->getParam('type');
        if(empty($type)){
            $type = 'diamond';
        }
        if (!$day) {
            $day = date('Y-m-d');
        }

        $criteria = new CDbCriteria();
        $criteria->condition = "uid=$uid";
        $admin = User::getAdmin();
        $user = User::model()->find($criteria);
        $result = array('success' => FALSE, 'desc' => '');
        if ($user && $admin) {
            $success = $this->sendMessage($admin, $user, $msg);
            $this->reward($user, $prize,$type);
            $asreward = new AssignedReward();
            $asreward->uid = $user->uid;
            $asreward->event = $event;
            $asreward->prize = $prize;
            $asreward->day = $day;
            $asreward->created_at = date("Y-m-d h:i:s");
            $asreward->updated_at = date("Y-m-d h:i:s");
            $asreward->save();
            $result['success'] = true;
            $result['desc'] = "Exchange completed";
        } else {
            $result['desc'] = "user objetc or statement object not saved";
        }

        echo json_encode($result);
    }

    public function actionWin() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $day = Yii::app()->request->getParam('day');
        if (!$day) {
            $day = date('Y-m-d');
        }
//        $limit = 100;
//        if (empty($page)) {
//            $page = 1;
//        }
//        $start = ($page - 1) * $limit;
//        $criteria = new CDbCriteria();
//        $criteria->order = "diamond desc";
//        $criteria->offset = $start;
//        $criteria->limit = $limit;
//        $userlist = User::model()->findAll($criteria);

        $selectday = Carbon::createFromFormat('Y-m-d H', "$day 01");
        $stopday = $selectday->toDateTimeString();
        $startday = $selectday->subDay()->toDateTimeString();

        $sql = "SELECT fb_uid,COUNT(betId) as win FROM bet 
WHERE created_at BETWEEN '$startday' AND '$stopday'
AND result='win'
GROUP BY fb_uid
ORDER BY COUNT(betId) DESC";
        $ulist = Yii::app()->db->createCommand($sql)->queryAll();
        $userlist = array();
        foreach ($ulist as $user) {
            $criteria = new CDbCriteria();
            $criteria->condition = "fb_uid={$user['fb_uid']}";
            $fbuser = FacebookUser::model()->find($criteria);

            $betcriteria = new CDbCriteria();
            $betcriteria->condition = "fb_uid='{$user['fb_uid']}' AND created_at BETWEEN '$startday' AND '$stopday'";
            $bets = Bet::model()->count($betcriteria);

            $customuser = array();
            $customuser['fb_uid'] = $user['fb_uid'];
            $customuser['display_name'] = $fbuser['display_name'];
            $customuser['win'] = $user['win'];
            $customuser['uid'] = $fbuser['uid'];
            $customuser['all'] = $bets;
            $userlist[] = $customuser;
        }
        $criteria = new CDbCriteria();
        $criteria->condition = "event='TOP WIN' AND day='$day'";
        $reward = AssignedReward::model()->findAll($criteria);
        $sequence = array();
        foreach ($reward as $r) {
            $sequence[$r->uid] = (int) $r->prize;
        }

        $this->render('win', array('userlist' => $userlist, 'sql' => $sql, 'day' => $day, 'reward' => $sequence));
    }

    public function actionSgold() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $day = Yii::app()->request->getParam('day');
        if (!$day) {
            $day = date('Y-m-d');
        }
//        $limit = 100;
//        if (empty($page)) {
//            $page = 1;
//        }
//        $start = ($page - 1) * $limit;
//        $criteria = new CDbCriteria();
//        $criteria->order = "diamond desc";
//        $criteria->offset = $start;
//        $criteria->limit = $limit;
//        $userlist = User::model()->findAll($criteria);

        $selectday = Carbon::createFromFormat('Y-m-d H', "$day 01");
        $stopday = $selectday->toDateTimeString();
        $startday = $selectday->subDay()->toDateTimeString();

        $sql = "SELECT fb_uid,SUM(result_sgold) as win FROM bet 
WHERE created_at BETWEEN '$startday' AND '$stopday'
GROUP BY fb_uid
ORDER BY SUM(result_sgold) DESC";
        $ulist = Yii::app()->db->createCommand($sql)->queryAll();
        $userlist = array();
        foreach ($ulist as $user) {
            $criteria = new CDbCriteria();
            $criteria->condition = "fb_uid={$user['fb_uid']}";
            $fbuser = FacebookUser::model()->find($criteria);

            $betcriteria = new CDbCriteria();
            $betcriteria->condition = "fb_uid='{$user['fb_uid']}' AND created_at BETWEEN '$startday' AND '$stopday'";
            $bets = Bet::model()->count($betcriteria);

            $customuser = array();
            $customuser['fb_uid'] = $user['fb_uid'];
            $customuser['display_name'] = $fbuser['display_name'];
            $customuser['win'] = $user['win'];
            $customuser['uid'] = $fbuser['uid'];
            $customuser['all'] = $bets;
            $userlist[] = $customuser;
        }
        $criteria = new CDbCriteria();
        $criteria->condition = "event='TOP SGOLD' AND day='$day'";
        $reward = AssignedReward::model()->findAll($criteria);
        $sequence = array();
        foreach ($reward as $r) {
            $sequence[$r->uid] = (int) $r->prize;
        }
        $this->render('sgold', array('userlist' => $userlist, 'sql' => $sql, 'day' => $day, 'reward' => $sequence));
    }

    public function actionGivediamond() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $fb_uid = Yii::app()->request->getParam('fb_uid');

        $nowday = Carbon::now();
        $last7day = $nowday->subDays(3);

        $criteria = new CDbCriteria();
        $criteria->condition = "last_login_date>DATE('{$last7day->toDateTimeString()}')";
        $userlist = FacebookUser::model()->findAll($criteria);

        $this->render('givediamond', array('userlist' => $userlist, 'last7day' => $last7day));
    }

    public function actionGivesgold() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        $fb_uid = Yii::app()->request->getParam('fb_uid');

        $nowday = Carbon::now();
        $last7day = $nowday->subDays(3);

        $criteria = new CDbCriteria();
        $criteria->condition = "last_login_date>DATE('{$last7day->toDateTimeString()}')";
        $userlist = FacebookUser::model()->findAll($criteria);

        $this->render('givesgold', array('userlist' => $userlist, 'last7day' => $last7day));
    }

    public function actionManagematch(){
        $date = Yii::app()->request->getParam('date',null);
        $name = Yii::app()->request->getParam('name','');
        $page = Yii::app()->request->getParam('page',1);

        $dataBet=array();
        $dataEditBet=array();
        if(isset($_POST['Match'])) {
            if (!empty($_POST['Match']['date'])) {
                $dateNow = new DateTime($_POST['Match']['date']);
                $date = $dateNow->format("Y-m-d");
            }
            if (!empty($_POST['Match']['name'])) {
                $name = $_POST['Match']['name'];
            } else {
                $name = '';
            }
            if (!empty($_POST['Match']['mid'])) {
                $mid = $_POST['Match']['mid'];
            } else {
                $mid = '';
            }
            if (!empty($_POST['Match']['username'])) {
                $username = $_POST['Match']['username'];
            } else {
                $username = '';
            }
            if (!empty($_POST['Match']['type'])) {
                $type = $_POST['Match']['type'];
            } else {
                $type = '';
            }
            $page=1;
        }else{
            if(!empty($date)){
                $dateNow = new DateTime($date);
                $date = $dateNow->format("Y-m-d");
            }else {
                $dateNow = new DateTime("now");
                $date = $dateNow->format("Y-m-d");
            }
            $mid='';
            $username='';
        }

        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

//        $criteria = new CDbCriteria();
//        $criteria->select='mid,homeName,homeNameTh,awayName,awayNameTh';
//        if(!empty($name)){
//            $criteria->condition = "DATE(show_date)='$date' and (homeName like '%".$name."%' or homeNameTh like '%".$name."%' or awayName like '%".$name."%' or awayNameTh like '%".$name."%')";
//        }else{
//            $criteria->condition = "DATE(show_date)='$date'";
//        }
//        $criteria->order = "show_date";
//        $maxPage=ceil(intval(TimelinesGame::model()->count($criteria))/30);
//        $criteria->offset=(($page-1)*30);
//        $criteria->limit=30;
//        $timelinesgame = TimelinesGame::model()->findAll($criteria);

        if(!empty($name)){
            $timelinesgame=Yii::app()->db->createCommand("select timelines.pin,timelines_game.mid,homeName,homeNameTh,awayName,awayNameTh,count(*) as count
                from timelines_game
                INNER JOIN bet on bet.mid=timelines_game.mid
                INNER JOIN timelines on timelines.key_list=timelines_game.mid
                where DATE(bet.created_at)='".$date."' and (homeName like '%".$name."%' or homeNameTh like '%".$name."%' or awayName like '%".$name."%' or awayNameTh like '%".$name."%')
                GROUP by timelines_game.mid
                order by timelines_game.show_date")->queryAll();
        }else{
            $timelinesgame=Yii::app()->db->createCommand("select timelines.pin,timelines_game.mid,homeName,homeNameTh,awayName,awayNameTh,count(*) as count
                from timelines_game
                INNER JOIN bet on bet.mid=timelines_game.mid
                INNER JOIN timelines on timelines.key_list=timelines_game.mid
                where DATE(bet.created_at)='".$date."'
                GROUP by timelines_game.mid
                order by timelines_game.show_date")->queryAll();
        }

        if(!empty($mid) && !empty($username)){
            if($type=='view') {
                $dataBet = Yii::app()->db->createCommand("select bet.betId,bet.created_at,facebook_user.display_name,bet.bet_sgold
                    from bet
                    INNER JOIN facebook_user on facebook_user.fb_uid=bet.fb_uid
                    where bet.mid=" . $mid . " and facebook_user.display_name like " . "'%" . $username . "%'")->queryAll();
            }else{
                $dataEditBet = Yii::app()->db->createCommand("select bet.FT_score,bet.betId,bet.created_at,facebook_user.display_name,bet.bet_sgold
                    from bet
                    INNER JOIN facebook_user on facebook_user.fb_uid=bet.fb_uid
                    where bet.mid=" . $mid . " and facebook_user.display_name like " . "'%" . $username . "%'")->queryAll();
            }
        }
        $this->render('managematch', array('date'=>$date,'name'=>$name,'timelinesgame'=>$timelinesgame,'dataBet'=>$dataBet,'dataEditBet'=>$dataEditBet));
    }

    public function actionViewBet(){
        $mid = Yii::app()->request->getParam('mid');
        $dataBet = Yii::app()->db->createCommand('select bet.betId,bet.created_at,facebook_user.display_name,bet.bet_sgold
            from bet
            INNER JOIN facebook_user on facebook_user.fb_uid=bet.fb_uid
            where bet.mid='.$mid)->queryAll();
        $String="";
        $sum=0;
        foreach($dataBet as $key=>$value){
            $sum+=$value['bet_sgold'];
            $String.="<tr>";
            $String.="<td style='padding: 0px 5px;'><label><input class='Checkboxes' betId='".$value['betId']."'  value='option1' type='checkbox'></label></td>";
            $String.="<td style='padding: 0px 5px;'>".$value['created_at']."</td>";
            $String.="<td style='padding: 0px 5px;'>".$value['display_name']."</td>";
            $String.="<td style='padding: 0px 5px;text-align: right;'>".number_format($value['bet_sgold'])."</td>";
            $String.="</tr>";
        }
        $String.="<tr><td colspan='3' style='background-color: #cccccc'>รวม</td><td>".number_format($sum)."</td></tr>";
        echo $String;
    }

    public function actionViewEditBet(){
        $mid = Yii::app()->request->getParam('mid');
        $dateTimeLinesGame=TimelinesGame::model()->find('mid=?',array($mid));
        $dataBet = Yii::app()->db->createCommand('select bet.FT_score,bet.betId,bet.created_at,facebook_user.display_name,bet.bet_sgold
            from bet
            INNER JOIN facebook_user on facebook_user.fb_uid=bet.fb_uid
            where bet.mid='.$mid)->queryAll();
        $String="";
        $sum=0;
        if(!empty($dateTimeLinesGame)) {
            $String .= "<tr><td colspan='5' style='text-align: center;background-color: #cccccc;'>คู่บอล " . $dateTimeLinesGame->homeNameEn . " : " . $dateTimeLinesGame->score . " : " . $dateTimeLinesGame->awayNameEn . "</td><tr>";
        }
        foreach($dataBet as $key=>$value){
            $sum+=$value['bet_sgold'];
            $String.="<tr>";
            $String.="<td style='padding: 0px 5px;'><label><input class='Checkboxes' betId='".$value['betId']."'  value='option1' type='checkbox'></label></td>";
            $String.="<td style='padding: 0px 5px;'>".$value['created_at']."</td>";
            $String.="<td style='padding: 0px 5px;'>".$value['display_name']."</td>";
            $String.="<td style='padding: 0px 5px;'>".$value['FT_score']."</td>";
            $String.="<td style='padding: 0px 5px;text-align: right;'>".number_format($value['bet_sgold'])."</td>";
            $String.="</tr>";
        }
        $String.="<tr><td colspan='4' style='background-color: #cccccc'>รวม</td><td>".number_format($sum)."</td></tr>";
        echo $String;
    }

    public function actionResetBet(){
        $data = Yii::app()->request->getParam('data');
        $status='true';
        $String="";
        $beforSGold=0;
        foreach($data as $key=>$value){
            $dataBet=Bet::model()->findByPk($value);
            if(!empty($dataBet)){
                $dataFacebookUser=FacebookUser::model()->find('fb_uid=?',array($dataBet->fb_uid));
                $beforSGold=$dataFacebookUser->scoin;
                if(!empty($dataFacebookUser)) {
                    $newTotalStatement = new TotalStatement();
                    $newTotalStatement->uid = $dataFacebookUser->uid;
                    $newTotalStatement->statement_type='rollback';
                    $newTotalStatement->before_scoin=$beforSGold;
                    $newTotalStatement->scoin_outcome=$dataBet->result_sgold;
                    $newTotalStatement->balance_scoin=$newTotalStatement->before_scoin-$newTotalStatement->scoin_outcome;
                    $newTotalStatement->result='refund';
                    $newTotalStatement->bet_id=$value;
                    if($newTotalStatement->save(false)){
                        $dataFacebookUser->scoin=$dataFacebookUser->scoin-$dataBet->result_sgold;
                        if($dataFacebookUser->save(false)){
//                            $dataBet->status_id=null;
                            $dataBet->result='wait';
                            $dataBet->betResult='wait';
                            $dataBet->result_sgold=0;
                            if($dataBet->save(false)) {
//                            $dataBet->delete();
                                $String .= "<tr><td>" . ($key + 1) . "</td><td>" . $dataFacebookUser->display_name . "</td><td>" . $beforSGold . "</td><td>" . $dataBet->bet_sgold . "</td><td>" . $dataFacebookUser->scoin . "</td></tr>";
                            }
                        }else{
                            $status='Not update FacebookUser';
                        }
                    }else{
                        $status='Not Save TotalStatement';
                    }
                }else{
                    $status='Not Find fb_uid';
                }
            }else{
                $status='Not FindByPk BetId';
            }
        }
        if($status=='true') {
            echo $String;
        }else{
            echo $status;
        }
    }

    public function actionClearBet(){
        $data = Yii::app()->request->getParam('data');
        $status='true';
        $String="";
        $beforSGold=0;
        foreach($data as $key=>$value){
            $dataBet=Bet::model()->findByPk($value);
            if(!empty($dataBet)){
                $dataFacebookUser=FacebookUser::model()->find('fb_uid=?',array($dataBet->fb_uid));
                $beforSGold=$dataFacebookUser->scoin;
                if(!empty($dataFacebookUser)) {
                    $newTotalStatement = new TotalStatement();
                    $newTotalStatement->uid = $dataFacebookUser->uid;
                    $newTotalStatement->statement_type='rollback';
                    $newTotalStatement->before_scoin=$beforSGold;
                    $newTotalStatement->scoin_outcome=$dataBet->bet_sgold;
                    $newTotalStatement->balance_scoin=$newTotalStatement->before_scoin-$newTotalStatement->scoin_outcome;
                    $newTotalStatement->result='refund';
                    $newTotalStatement->bet_id=$value;
                    if($newTotalStatement->save(false)){
                        $dataFacebookUser->scoin=$dataFacebookUser->scoin+$dataBet->bet_sgold;
                        if($dataFacebookUser->save(false)){
                            $dataBet->delete();
                            $String.="<tr><td>".($key+1)."</td><td>".$dataFacebookUser->display_name."</td><td>".$beforSGold."</td><td>".$dataBet->bet_sgold."</td><td>".$dataFacebookUser->scoin."</td></tr>";
                        }else{
                            $status='Not update FacebookUser';
                        }
                    }else{
                        $status='Not Save TotalStatement';
                    }
                }else{
                    $status='Not Find fb_uid';
                }
            }else{
                    $status='Not FindByPk BetId';
            }
        }
        if($status=='true') {
            echo $String;
        }else{
            echo $status;
        }
    }

    public function actionPin()
    {
        $id = Yii::app()->request->getParam('id');
        Timelines::model()->updateAll(array('last_event' => 'N'), 'keygen=' . "game".$id);
        $dataTimeline = new Timelines();
        $dataTimeline->fb_uid="100007730416796";
        $dataTimeline->keygen="game".$id;
        $dataTimeline->content_type="game";
        $dataTimeline->key_list=$id;
        $dataTimeline->created_at=Carbon::now()->toDateTimeString();
        $dataTimeline->updated_at=Carbon::now()->toDateTimeString();
        $dataTimeline->stamped_at=Carbon::now();
        $dataTimeline->pin='Y';
        $dataTimeline->last_event='Y';
        if($dataTimeline->save(false)){
            $this->redirect('/usagereport/managematch');
        }
    }

    public function actionUnPin(){
        $id = Yii::app()->request->getParam('id');
        Timelines::model()->updateAll(array('pin'=>'N'),'key_list='.$id);
        $this->redirect('/usagereport/managematch');
    }

    public function actionAddScore(){
        if(isset($_POST['score'])){
            $score=explode('-',$_POST['score']['score']);
            if(is_numeric($score[0]) && is_numeric($score[1])){
                $dataBet=Bet::model()->findAll('mid=?',array($_POST['score']['mid']));
                foreach($dataBet as $key=>$value){
                   Bet::model()->updateByPk($value->betId,array('FT_score'=>$_POST['score']['score']));
                }
                $this->redirect('/usagereport/matchremain');
            }else{
                $this->redirect('/usagereport/matchremain');
            }
        }else{
            $this->redirect('/usagereport/matchremain');
        }
    }

    public function actionAddScoreRepairScore(){
        if(isset($_POST['score'])){
            $score=explode('-',$_POST['score']['score']);
            if(!empty($score[0]) && !empty($score[1]) && is_numeric($score[0]) && is_numeric($score[1])){
                $dataBet=Bet::model()->findAll('mid=?',array($_POST['score']['mid']));
                foreach($dataBet as $key=>$value){
                    bet::model()->updateByPk($value->betId,array('FT_score'=>$_POST['score']['score']));
                }
                $this->redirect('/usagereport/repairscores');
            }else{
                $this->redirect('/usagereport/repairscores');
            }
        }else{
            $this->redirect('/usagereport/repairscores');
        }
    }

    public function actionMatchRemain(){
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");
        $matchRemain=array();
        if(isset($_POST['Date']) && !empty($_POST['Date']['start']) && !empty($_POST['Date']['end'])) {
//            if (!empty($_POST['Date']['start']) && !empty($_POST['Date']['end'])) {
                $dateStart = new DateTime($_POST['Date']['start']);
                $dateEnd = new DateTime($_POST['Date']['end']);
                $matchRemain = Yii::app()->db->createCommand("select bet.mid,bet.FT_score,bet.result,timelines_game.show_date,timelines_game.leagueNameEn,timelines_game.leagueName,timelines_game.homeNameEn,timelines_game.homeName,timelines_game.awayNameEn,timelines_game.awayName
from bet
INNER JOIN timelines_game on timelines_game.mid=bet.mid
where bet.created_at>='" . $dateStart->format('Y-m-d 00:00:00') . "' and bet.created_at<='" . $dateEnd->format('Y-m-d 23:59:59') . "'
group by bet.mid
order by timelines_game.show_date asc")->queryAll();
//            }
        }else{
            $matchRemain = Yii::app()->db->createCommand("select bet.mid,bet.FT_score,bet.result,timelines_game.show_date,timelines_game.leagueNameEn,timelines_game.leagueName,timelines_game.homeNameEn,timelines_game.homeName,timelines_game.awayNameEn,timelines_game.awayName
from bet
INNER JOIN timelines_game on timelines_game.mid=bet.mid
where DATE(bet.created_at)='".date('Y-m-d 00:00:00')."'
group by bet.mid
order by timelines_game.show_date asc")->queryAll();
        }
        $this->render('matchremain', array('matchRemain'=>$matchRemain));
    }

    public function actionRepairscores(){
        Yii::app()->clientScript->registerCssFile("http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/jquery-1.10.2.js");
        Yii::app()->clientScript->registerScriptFile("http://code.jquery.com/ui/1.11.1/jquery-ui.js");

        if(isset($_POST['Date'])) {
            if (!empty($_POST['Date']['start']) && !empty($_POST['Date']['end'])) {
                $dateStart = new DateTime($_POST['Date']['start']);
                $dateEnd = new DateTime($_POST['Date']['end']);
                $matchRemain = Yii::app()->db->createCommand("select bet.mid,bet.FT_score,bet.result,timelines_game.show_date,timelines_game.leagueNameEn,timelines_game.homeNameEn,timelines_game.awayNameEn
from bet
INNER JOIN timelines_game on timelines_game.mid=bet.mid
where bet.result='wait' and timelines_game.show_date>='".$dateStart->format('Y-m-d 00:00:00')."' and timelines_game.show_date<='".$dateEnd->format('Y-m-d 23:59:59')."'
group by bet.mid
order by timelines_game.show_date asc")->queryAll();
            }
        }else{
            $matchRemain = Yii::app()->db->createCommand("select bet.mid,bet.FT_score,bet.result,timelines_game.show_date,timelines_game.leagueNameEn,timelines_game.homeNameEn,timelines_game.awayNameEn
from bet
INNER JOIN timelines_game on timelines_game.mid=bet.mid
where bet.result='wait' and bet.created_at<date(now())
group by bet.mid
order by timelines_game.show_date asc")->queryAll();
        }

        $this->render('repairscores', array('matchRemain'=>$matchRemain));
    }

}
