<?php
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 11/16/2015
 * Time: 13:28
 */
class ServerController extends Controller
{

    public function init()
    {
        $this->layout = 'main2';
    }

    public function actionAdd(){
        $server=new ServerStatus();
        if(isset($_POST['server'])){
            $server->attributes=$_POST['server'];
            if($server->title==""){
                $server->title=null;
            }
            if($server->message==""){
                $server->message=null;
            }
            $server->created_at=date('Y-m-d H:i:s');
            if($server->save(false)){
                $webroot = Yii::getPathOfAlias('webroot');
                file_put_contents($webroot."/../cache/server.json",json_encode($server->attributes));
                $this->redirect('/server/add');
            }else{
                $this->redirect('/server/add');
            }
        }

        $dataServer=ServerStatus::model()->findAll(array(
            "order" => "created_at DESC",
            "limit" => 20,
        ));
        $this->render('add', array('dataServer'=>$dataServer));
    }
}