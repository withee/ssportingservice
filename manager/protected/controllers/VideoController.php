<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/7/2016
 * Time: 18:12
 */
class VideoController extends Controller {

    public function init()
    {
        $this->layout = 'main2';
    }

    public function actionIndex(){
        $search= Yii::app()->request->getParam('search', '');
        $tag= Yii::app()->request->getParam('tag', '');
        $videotype= Yii::app()->request->getParam('videotype', '');
        $category= Yii::app()->request->getParam('category', '');

        $offset= Yii::app()->request->getParam('offset', 0);
        $limit= Yii::app()->request->getParam('limit', 50);
        $condition="";
        if(!empty($search)){
            if($condition==""){
                $condition .= " title like '%".$search."%'";
            }else {
                $condition .= " and title like '%".$search."%'";
            }
        }
        if(!empty($tag)){
            if($condition==""){
                $condition .= " video_tag like '%".$tag."%'";
            }else {
                $condition .= " and video_tag like '%".$tag."%'";
            }
        }
        if(!empty($videotype)){
            if($condition==""){
                $condition .= " videotype = '".$videotype."'";
            }else {
                $condition .= " and videotype = '".$videotype."'";
            }
        }
        if(!empty($category)){
            if($condition==""){
                $condition .= " category = '".$category."'";
            }else {
                $condition .= " and category = '".$category."'";
            }
        }


        $dataVideo=VideoHighlight::model()->findAll(array('order' => 'video_id DESC', 'condition' => $condition, 'limit' => $limit,'offset'=>$offset));
        if($condition==""){
            $sql = "SELECT COUNT(*) FROM video_highlight";
        }else {
            $sql = "SELECT COUNT(*) FROM video_highlight WHERE " . $condition;
        }
        $countPage = Yii::app()->db->createCommand($sql)->queryScalar();
        $this->render('index',array('category'=>$category,'videotype'=>$videotype,'tag'=>$tag,'search'=>$search,'dataVideo'=>$dataVideo,'offset'=>$offset,'countPage'=>$countPage,'limit'=>$limit));
    }

    public function actionDelete(){
        $id= Yii::app()->request->getParam('id', null);
        VideoHighlight::model()->deleteByPk($id);
        $this->redirect('/Video');
    }

    public function actionAdd(){
        $id= Yii::app()->request->getParam('id', null);
        if(!empty($id)){
            $VideoObj=VideoHighlight::model()->findByPk($id);
        }else{
            $VideoObj=new VideoHighlight();
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'formVideo') {
            echo CActiveForm::validate($VideoObj);
            Yii::app()->end();
        }

        if(isset($_POST['VideoHighlight'])){
            if(!empty($_POST['VideoHighlight']['video_id'])){
                $VideoObj=VideoHighlight::model()->findByPk($_POST['VideoHighlight']['video_id']);
            }else{
                $VideoObj=new VideoHighlight();
                $VideoObj->mid=0;
                $VideoObj->gid=0;
                $VideoObj->hid=0;
                $VideoObj->leagueId=0;
                $VideoObj->competitionId=0;
                $VideoObj->create_datetime=date('Y-m-d H:i:s');
            }
            $VideoObj->attributes=$_POST['VideoHighlight'];
//            $VideoObj->videosource=$_POST['video']['videosource'];
//            $VideoObj->videotype=$_POST['video']['videotype'];
//            $VideoObj->video_tag=$_POST['video']['video_tag'];
            $VideoObj->desc=$_POST['VideoHighlight']['desc'];
//            $VideoObj->category=$_POST['video']['category'];
            if($VideoObj->validate()) {
                $VideoObj->save();
//                if(!empty($_POST['video']['video_id'])){
                    $this->redirect('/Video');
//                }
            }else{
                $errores = $VideoObj->getErrors();
                var_dump($errores);
                exit;
            }
        }

        $this->render('add',array('VideoObj'=>$VideoObj));
    }

    public function actionDeleteVideoHighlight(){
        $id= Yii::app()->request->getParam('id', null);
        VideoHighlight::model()->deleteByPk($id);
        $this->redirect('/Video/VideoHighlight');
    }

    public function actionVideoHighlight(){
        $id= Yii::app()->request->getParam('id', null);
        $offset= Yii::app()->request->getParam('offset', 0);
        $limit= Yii::app()->request->getParam('limit', 50);
        if(!empty($id)){
            $VideoObj=VideoHighlight::model()->findByPk($id);
        }else{
            $VideoObj=new VideoHighlight();
        }
        if(isset($_POST['video'])){
            if(!empty($_POST['video']['video_id'])){
                $VideoObj=VideoHighlight::model()->findByPk($_POST['video']['video_id']);
            }else{
                $VideoObj=new VideoHighlight();
                $VideoObj->mid=0;
                $VideoObj->gid=0;
                $VideoObj->hid=0;
                $VideoObj->leagueId=0;
                $VideoObj->competitionId=0;
                $VideoObj->create_datetime=date('Y-m-d H:i:s');
            }
            $VideoObj->attributes=$_POST['video'];
            $VideoObj->videosource=$_POST['video']['videosource'];
            $VideoObj->videotype=$_POST['video']['videotype'];
            $VideoObj->video_tag=$_POST['video']['video_tag'];
            $VideoObj->desc=$_POST['video']['desc'];
            if($VideoObj->validate()) {
                $VideoObj->save();
                if(!empty($_POST['video']['video_id'])){
                    $this->redirect('/Video/VideoHighlight');
                }
            }else{
                $errores = $VideoObj->getErrors();
                var_dump($errores);
                exit;
            }
        }
        $dataVideoHighlight=VideoHighlight::model()->findAll(array('order' => 'video_id DESC', 'condition' => '`videotype`=:x', 'limit' => $limit,'offset'=>$offset, 'params' => array(':x' => 'highlight')));
        $sql = "SELECT COUNT(*) FROM video_highlight where `videotype`='highlight'";
        $countPage = Yii::app()->db->createCommand($sql)->queryScalar();
        $this->render('videoHighlight',array('dataVideoHighlight'=>$dataVideoHighlight,'VideoObj'=>$VideoObj,'offset'=>$offset,'countPage'=>$countPage,'limit'=>$limit));
    }

    public function actionDeleteVideoGeneral(){
        $id= Yii::app()->request->getParam('id', null);
        VideoHighlight::model()->deleteByPk($id);
        $this->redirect('/Video/VideoGeneral');
    }

    public function actionVideoGeneral(){
        $id= Yii::app()->request->getParam('id', null);
        $offset= Yii::app()->request->getParam('offset', 0);
        $limit= Yii::app()->request->getParam('limit', 50);
        if(!empty($id)){
            $VideoObj=VideoHighlight::model()->findByPk($id);
        }else{
            $VideoObj=new VideoHighlight();
        }
        if(isset($_POST['video'])){
            if(!empty($_POST['video']['video_id'])){
                $VideoObj=VideoHighlight::model()->findByPk($_POST['video']['video_id']);
            }else{
                $VideoObj=new VideoHighlight();
                $VideoObj->mid=0;
                $VideoObj->gid=0;
                $VideoObj->hid=0;
                $VideoObj->leagueId=0;
                $VideoObj->competitionId=0;
                $VideoObj->create_datetime=date('Y-m-d H:i:s');
            }
            $VideoObj->attributes=$_POST['video'];
            $VideoObj->videosource=$_POST['video']['videosource'];
            $VideoObj->videotype=$_POST['video']['videotype'];
            $VideoObj->video_tag=$_POST['video']['video_tag'];
            $VideoObj->desc=$_POST['video']['desc'];
            if($VideoObj->validate()) {
                $VideoObj->save();
                if(!empty($_POST['video']['video_id'])){
                    $this->redirect('/Video/VideoGeneral');
                }
            }else{
                $errores = $VideoObj->getErrors();
                var_dump($errores);
                exit;
            }
        }

        $dataVideoHighlight=VideoHighlight::model()->findAll(array('order' => 'video_id DESC', 'condition' => '`videotype`=:x', 'limit' => $limit,'offset'=>$offset, 'params' => array(':x' => 'general')));
        $sql = "SELECT COUNT(*) FROM video_highlight where `videotype`='general'";
        $countPage = Yii::app()->db->createCommand($sql)->queryScalar();
        $this->render('videoGeneral',array('dataVideoHighlight'=>$dataVideoHighlight,'VideoObj'=>$VideoObj,'offset'=>$offset,'limit'=>$limit,'countPage'=>$countPage));
    }

    public function actionUploadFile(){
        $id= Yii::app()->request->getParam('id', null);
        if (!empty($_FILES)&&!empty($id)) {
            $number=rand(100, 10000000);
            $webroot = "..";
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $nameFile = $_FILES['Filedata']['name'];
            $typeFile = $_FILES['Filedata']['type'];
            $targetPath = $webroot . '/uploaded/video/'.$id.'/';
            $data=getimagesize($tempFile);
            if (!is_dir($webroot . '/uploaded')) {
                mkdir($webroot . '/uploaded', 0777);
                chmod($webroot . '/uploaded', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'video')) {
                mkdir($webroot . '/uploaded/' . 'video', 0777);
                chmod($webroot . '/uploaded/' . 'video', 0777);
            }

            if (!is_dir($webroot . '/uploaded/' . 'video/'.$id)) {
                mkdir($webroot . '/uploaded/' . 'video/'.$id, 0777);
                chmod($webroot . '/uploaded/' . 'video/'.$id, 0777);
            }
//            $videoHighlight=VideoHighlight::model()->findByPk($id)
                list($width, $height) = getimagesize($tempFile);
                $thumb = imagecreatetruecolor(($width*0.5), ($height*0.5));
                if($data['mime']=="image/jpeg"){
                    $file = str_replace('//', '/', $targetPath) . 'Image_' . $id . '.jpg';
                    $fileMin = str_replace('//', '/', $targetPath) . 'Image_' . $id . '_Q50.jpg';
                    $source = imagecreatefromjpeg($tempFile);
                    imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                    imagejpeg($thumb, str_replace('//', '/', $targetPath) . 'Image_'.$id.'_Q50.jpg');
                }else if($data['mime']=="image/png") {
                    $file = str_replace('//', '/', $targetPath) . 'Image_' . $id . '.png';
                    $fileMin = str_replace('//', '/', $targetPath) . 'Image_' . $id . '_Q50.png';
                    $source = imagecreatefrompng($tempFile);
                    imagecopyresized($thumb, $source, 0, 0, 0, 0, ($width*0.5), ($height*0.5), $width, $height);
                    imagepng($thumb, str_replace('//', '/', $targetPath) . 'Image_'.$id. '_Q50.png');
                }
//                MediaStore::model()->updateByPk($id, array('path' => 'http://api.ssporting.com'.str_replace($webroot,'',$file)));
                VideoHighlight::model()->updateByPk($id, array('thumbnail' => 'http://api.pass90.com'.str_replace($webroot,'',$file)));
                move_uploaded_file($tempFile, $file);
                echo CJSON::encode(VideoHighlight::model()->findByPk($id));
        }else{
            echo 'false';
        }
    }

}