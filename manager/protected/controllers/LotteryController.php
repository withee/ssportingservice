<?php

class LotteryController extends Controller
{
    public function init() {
        $this->layout = 'main2';
    }
	public function actionIndex()
	{
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }

		$this->render('index');
	}
    public function actionLotteryNews(){
        $db =Yii::app()->test_db;
        $sql_list ="select *from lottery_news where lottery_news_remove='N' order by lottery_news_id DESC limit 100";
        $list=$db->createCommand($sql_list)->queryAll();
        $this->render('lotteryNews',array('list'=>$list));
    }

    public function actionLotteryTded(){
        $db =Yii::app()->test_db;
        $sql_list ="select *from lottery_tded where lottery_tded_remove='N' order by lottery_tded_id DESC limit 100";
        $list=$db->createCommand($sql_list)->queryAll();
        $this->render('lotteryTded',array('list'=>$list));
    }
    public function actionAddLotteryNews(){
        $type='news';
        $table='lottery_'.$type;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js-library/ckeditor/ckeditor.js');
        $cs->registerCssFile($baseUrl.'/jquery.uploadify/jquery.uploadify.css');
        $cs->registerScriptFile($baseUrl.'/jquery.uploadify/jquery.jquery.uploadify.min.js');
        $cs->registerScriptFile($baseUrl.'/js/lottery.main.js');
        $title =Yii::app()->request->getParam('title');
        $db=Yii::app()->test_db;
        if($title){
            //$title  = Yii::app()->request->getParam('title');
            $desc = mysql_escape_string(Yii::app()->request->getParam('desc'));
            $content = mysql_escape_string(Yii::app()->request->getParam('content'));
            $img = mysql_escape_string(Yii::app()->request->getParam('img'));
            $title = mysql_escape_string($title);
            $sql_create =
                "
                insert into $table (lottery_news_title,lottery_news_desc,lottery_news_content,lottery_news_img,lottery_news_createdatetime,lottery_news_modatetime)
                value(
                '$title',
                '$desc',
                '$content',
                '$img',
                current_timestamp,
                current_timestamp
                )
                ";
            //Yii::app()->db->createCommand()->execute()
            $result=$db->createCommand($sql_create)->execute();

            if($result){
                $this->render('addLotteryNewsSuccess');
            }else{
                $this->render('addLotteryNewsFail');
            }
            exit;
        }
        $this->render('addLotteryNews');

    }
    public function actionAddLotteryTded(){
        $type='tded';
        $table='lottery_'.$type;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js-library/ckeditor/ckeditor.js');
        $cs->registerCssFile($baseUrl.'/jquery.uploadify/jquery.uploadify.css');
        $cs->registerScriptFile($baseUrl.'/jquery.uploadify/jquery.jquery.uploadify.min.js');
        $cs->registerScriptFile($baseUrl.'/js/lottery.main.js');
        $title =Yii::app()->request->getParam('title');
        $db=Yii::app()->test_db;
        if($title){
            //$title  = Yii::app()->request->getParam('title');
            $desc = mysql_escape_string(Yii::app()->request->getParam('desc'));
            $content = mysql_escape_string(Yii::app()->request->getParam('content'));
            $img = mysql_escape_string(Yii::app()->request->getParam('img'));
            $title = mysql_escape_string($title);
            $sql_create =
                "
                insert into $table (lottery_tded_title,lottery_tded_desc,lottery_tded_content,lottery_tded_img,lottery_tded_createdatetime,lottery_tded_modatetime)
                value(
                '$title',
                '$desc',
                '$content',
                '$img',
                current_timestamp,
                current_timestamp
                )
                ";
            //Yii::app()->db->createCommand()->execute()
            $result=$db->createCommand($sql_create)->execute();

            if($result){
                $this->render('addLotteryTdedSuccess');
            }else{
                $this->render('addLotteryTdedFail');
            }
            exit;
        }
        $this->render('addLotteryTded');

    }
    public function actionEditLotteryNews(){
        $type='news';
        $table='lottery_'.$type;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js-library/ckeditor/ckeditor.js');
        $cs->registerCssFile($baseUrl.'/jquery.uploadify/jquery.uploadify.css');
        $cs->registerScriptFile($baseUrl.'/jquery.uploadify/jquery.jquery.uploadify.min.js');
        $cs->registerScriptFile($baseUrl.'/js/lottery.main.js');
        $title =Yii::app()->request->getParam('title');
        $db=Yii::app()->test_db;
        $id = Yii::app()->request->getParam('id');
        if($title){
            //$title  = Yii::app()->request->getParam('title');

            $desc = mysql_escape_string(Yii::app()->request->getParam('desc'));
            $content = mysql_escape_string(Yii::app()->request->getParam('content'));
            $img = mysql_escape_string(Yii::app()->request->getParam('img'));
            $title = mysql_escape_string($title);
            $sql_update =
                "
                update $table set
                lottery_news_title='$title',
                lottery_news_desc='$desc',
                lottery_news_content='$content',
                lottery_news_img='$img',
                lottery_news_modatetime =current_timestamp
                where lottery_news_id=$id
                ";
            //Yii::app()->db->createCommand()->execute()

            $result=$db->createCommand($sql_update)->execute();
            $obj=$db->createCommand("select *from lottery_news where lottery_news_id=$id")->queryRow();
            if($result){
                $this->render('editLotteryNews',array('success'=>1,'obj'=>$obj));
            }else{
                $this->render('editLotteryNews',array('success'=>2,'obj'=>$obj));
            }
            exit;
        }
        $obj=$db->createCommand("select *from lottery_news where lottery_news_id=$id")->queryRow();
        $this->render('editLotteryNews',array('success'=>0, 'obj'=>$obj));

    }
    public function actionEditLotteryTded(){
        $type='tded';
        $table='lottery_'.$type;
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl.'/js-library/ckeditor/ckeditor.js');
        $cs->registerCssFile($baseUrl.'/jquery.uploadify/jquery.uploadify.css');
        $cs->registerScriptFile($baseUrl.'/jquery.uploadify/jquery.jquery.uploadify.min.js');
        $cs->registerScriptFile($baseUrl.'/js/lottery.main.js');
        $title =Yii::app()->request->getParam('title');
        $db=Yii::app()->test_db;
        $id = Yii::app()->request->getParam('id');
        if($title){
            //$title  = Yii::app()->request->getParam('title');

            $desc = mysql_escape_string(Yii::app()->request->getParam('desc'));
            $content = mysql_escape_string(Yii::app()->request->getParam('content'));
            $img = mysql_escape_string(Yii::app()->request->getParam('img'));
            $title = mysql_escape_string($title);
            $sql_update =
                "
                update $table set
                lottery_tded_title='$title',
                lottery_tded_desc='$desc',
                lottery_tded_content='$content',
                lottery_tded_img='$img',
                lottery_tded_modatetime =current_timestamp
                where lottery_tded_id=$id
                ";
            //Yii::app()->db->createCommand()->execute()

            $result=$db->createCommand($sql_update)->execute();
            $obj=$db->createCommand("select *from lottery_tded where lottery_tded_id=$id")->queryRow();
            if($result){
                $this->render('editLotteryTded',array('success'=>1,'obj'=>$obj));
            }else{
                $this->render('editLotteryTded',array('success'=>2,'obj'=>$obj));
            }
            exit;
        }
        $obj=$db->createCommand("select *from lottery_tded where lottery_tded_id=$id")->queryRow();
        $this->render('editLotteryTded',array('success'=>0, 'obj'=>$obj));

    }
    public function actionUploadFile(){
        // Define a destination
        $targetFolder = '/temp'; // Relative to the root

        if (!empty($_FILES)) {
            $timestamp = time();
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = $_SERVER['DOCUMENT_ROOT'] . $targetFolder;
            $targetFile = rtrim($targetPath,'/') . '/full/' .$timestamp.'_'. $_FILES['Filedata']['name'];
            $targetFile640x375 = rtrim($targetPath,'/') . '/640x375/' .$timestamp.'_'. $_FILES['Filedata']['name'];
            $targetFile190x150 = rtrim($targetPath,'/') . '/190x150/' .$timestamp.'_'. $_FILES['Filedata']['name'];
            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array($fileParts['extension'],$fileTypes)) {
//                ImageUtility::resize($tempFile,$_SERVER['DOCUMENT_ROOT'].'/500w.jpg','weight',500);
//                ImageUtility::resize($tempFile,$_SERVER['DOCUMENT_ROOT'].'/500h.jpg','height',500);

//                ImageUtility::crop($tempFile,$_SERVER['DOCUMENT_ROOT'].'/500c.jpg',500,400);
//                ImageUtility::resizeFixed($tempFile,$_SERVER['DOCUMENT_ROOT'].'/200_fix.jpg',200,500);
                ImageUtility::crop($tempFile,$_SERVER['DOCUMENT_ROOT'].'/temp/640x375/'.$timestamp.'_'.$_FILES['Filedata']['name'],640,375);
                ImageUtility::crop($tempFile,$_SERVER['DOCUMENT_ROOT'].'/temp/190x150/'.$timestamp.'_'.$_FILES['Filedata']['name'],190,150);
                move_uploaded_file($tempFile,$targetFile);

                copy($targetFile,'../../lottery-service/public/temp/full/'.$timestamp.'_'.$_FILES['Filedata']['name']);
                copy($targetFile640x375,'../../lottery-service/public/temp/640x375/'.$timestamp.'_'.$_FILES['Filedata']['name']);
                copy($targetFile190x150,'../../lottery-service/public/temp/190x150/'.$timestamp.'_'.$_FILES['Filedata']['name']);
                echo $targetFolder.'/190x150/'.$timestamp.'_'.$_FILES['Filedata']['name'];

            } else {
                echo 'Invalid file type.';
            }
        }
    }

    public function actionRemoveNews(){
        $lottery_news_id = Yii::app()->request->getParam('lottery_news_id');
        if($lottery_news_id){
            $db = Yii::app()->test_db;
            $sql_update = "update lottery_news set lottery_news_remove='Y' where lottery_news_id=$lottery_news_id";
            echo $db->createCommand($sql_update)->execute();
        }
    }
    public function actionRemoveTded(){
        $lottery_tded_id = Yii::app()->request->getParam('lottery_tded_id');
        if($lottery_tded_id){
            $db = Yii::app()->test_db;
            $sql_update = "update lottery_tded set lottery_tded_remove='Y' where lottery_tded_id=$lottery_tded_id";
            echo $db->createCommand($sql_update)->execute();
        }
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}