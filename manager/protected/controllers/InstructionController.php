<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InstructionController
 *
 * @author mrsyrop
 */
require '../DBConfig.php';

class InstructionController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $db = DBConfig::getConnection();
        $sql = "SELECT  `id`, `menu` FROM instruction";
        $stmt = $db->query($sql);
        $instruction = $stmt->fetchAll(5);
        $this->render('index', array('instruction' => $instruction));
    }

    public function actionAdd() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');

        if (Yii::app()->request->getParam('submit')) {
            $obj = new Instruction();
            $obj->menu = Yii::app()->request->getParam('menu');
            $obj->detail = Yii::app()->request->getParam('detail');

            if($obj->save()){
                echo "<center><b>เพิ่มข้อมูล สำเร็จ"."</b></center><br>";
            }else{
                echo "<center><b style='text-align: center;'>ล้มเหลว"."</b></center><br>";
            }
        }

        $this->render('add');
    }

    public function actionEdit() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');

        $instructionId = Yii::app()->request->getParam('instructionId');

        $instruction = Instruction::model()->findByPk($instructionId);

        $menu = $instruction->menu;
        $detail = $instruction->detail;

        if (Yii::app()->request->getParam('update')) {

            $menu = Yii::app()->request->getParam('menu');
            $detail = Yii::app()->request->getParam('detail');

            $instruction->menu = $menu;
            $instruction->detail = $detail;

            if($instruction->save()){
                echo "<center><b>แก้ข้อมูล สำเร็จ"."</b></center><br>";
            }else{
                echo "<center><b style='text-align: center;'>ล้มเหลว"."</b></center><br>";
            }
        }

        $this->render('edit', array('instructionId' => $instructionId, 'menu' => $menu, 'detail' => $detail));
    }

    public function actionDelete(){
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $instructionId = Yii::app()->request->getParam('instructionId');
        $sql = "UPDATE instruction SET remove='Y' WHERE `id`='".$instructionId."'";
        Yii::app()->db->createCommand($sql)->execute();
        echo "success";
    }

}
