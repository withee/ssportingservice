<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OddsController
 *
 * @author withee 
 */
class OddsController extends Controller {

    //put your code here

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $sql = "SELECT notdup. * , od. *
FROM (

SELECT live. * , ll.leagueName, ll.leagueNameTh, ht.teamName homename, ht.teamNameTH homenameTH, gt.teamName awayname, gt.teamNameTH awaynameTH
FROM live_match AS live
LEFT JOIN lang_league AS ll ON live._lid = ll.leagueId
LEFT JOIN lang_team AS ht ON live.hid = ht.tid
LEFT JOIN lang_team AS gt ON live.gid = gt.tid
LEFT JOIN odds_7m o7 ON ll.id7m = o7.league_id
AND ht.id7m = o7.home_id
AND gt.id7m = o7.away_id
WHERE o7.match_id IS NULL
AND live.sid =1
AND live.showDate = CURDATE( )
AND ll.leagueId IS NOT NULL
AND ht.tid IS NOT NULL
AND gt.tid IS NOT NULL
) AS notdup
LEFT JOIN odds_defined od ON notdup.mid = od.match_id
ORDER BY notdup._lid, notdup.date
";

//        $db = DBConfig::getConnection();
//        $stmt = $db->query($sql);
//        $livelist = $stmt->fetch(5);
        $livelist = Yii::app()->db->createCommand($sql)->queryAll();
//        $livematches = LiveMatch::model()->findAll(array("condition" => "sid=1 AND showDate=CURDATE()", "order" => "_lid,showDate"));
        $result = array("success" => FALSE, "desc" => "nothing happen", "live" => NULL, "leagues" => NULL, "team" => NULL);
//        $leagues = array();
//        $team = array();
//        $live = array();
//        $odds = array();
//        foreach ($livematches as $match) {
//            $league = null;
//            $home = null;
//            $away = null;
//            //echo $match->mid . "<br>";
//            if (!array_key_exists($match->_lid, $leagues)) {
//                $league = LangLeague::model()->find("leagueId={$match->_lid}");
//            }
//
//            if (!array_key_exists($match->hid, $team)) {
//                $home = LangTeam::model()->find("tid={$match->hid}");
//            }
//            if (!array_key_exists($match->gid, $team)) {
//                $away = LangTeam::model()->find("tid={$match->gid}");
//            }
//
//            if (!array_key_exists($match->mid, $odds)) {
//                $odd = OddsDefined::model()->find("match_id={$match->mid}");
//                if (!empty($odd)) {
//                    $odds[$match->mid] = $odd;
//                }
//            }
//
//            if (!empty($league) && !empty($home) && !empty($away)) {
//                $live[] = $match;
//                $leagues[$match->_lid] = $league;
//                $team[$match->hid] = $home;
//                $team[$match->gid] = $away;
//                $result["success"] = true;
//                $result["desc"] = "success";
//            }
//        }
        //$this->render('index');
        $result["live"] = $livelist;
//        $result["leagues"] = $leagues;
//        $result["team"] = $team;
//        $result["odds"] = $odds;
        $this->render("index", array("result" => $result));
    }

    public function actionUpdateOdds() {
        $key = isset($_REQUEST["key"]) ? $_REQUEST["key"] : "";
        $action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "add";
        $hdp_home = isset($_REQUEST["hdp_home"]) ? $_REQUEST["hdp_home"] : "";
        $hdp_away = isset($_REQUEST["hdp_away"]) ? $_REQUEST["hdp_away"] : "";
        $hdp = isset($_REQUEST["hdp"]) ? $_REQUEST["hdp"] : "";
        $odds_home = isset($_REQUEST["odds_home"]) ? $_REQUEST["odds_home"] : "";
        $odds_away = isset($_REQUEST["odds_away"]) ? $_REQUEST["odds_away"] : "";
        $odds_draw = isset($_REQUEST["odds_draw"]) ? $_REQUEST["odds_draw"] : "";
        $ou_over = isset($_REQUEST["ou_over"]) ? $_REQUEST["ou_over"] : "";
        $ou_under = isset($_REQUEST["ou_under"]) ? $_REQUEST["ou_under"] : "";
        $ou = isset($_REQUEST["ou"]) ? $_REQUEST["ou"] : "";
        $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "";

        $result = array("success" => false, "desc" => "nothing happen", "data" => NULL);

        if (!empty($key) || !empty($action)) {
            $live = LiveMatch::model()->find("mid=$key");
            $league = LangLeague::model()->find("leagueId={$live->_lid}");
            $home = LangTeam::model()->find("tid={$live->hid}");
            $away = LangTeam::model()->find("tid={$live->gid}");
            if ($action == "add") {
                $odds = new OddsDefined();
                $odds->match_id = $key;
                $odds->league_id = $league->id7m;
                $odds->home_id = $home->id7m;
                $odds->away_id = $away->id7m;
                $odds->type = $type;

                $odds->hdp_home = $hdp_home;
                $odds->hdp_home_e = $hdp_home;
                $odds->hdp_away = $hdp_away;
                $odds->hdp_away_e = $hdp_away;
                $odds->hdp = $hdp;
                $odds->hdp_e = $hdp;
                $odds->hpd_datetime_update = date("Y-m-d h:i:s");

                $odds->odds1x2_home = $odds_home;
                $odds->odds1x2_home_e = $odds_home;
                $odds->odds1x2_away = $odds_away;
                $odds->odds1x2_away_e = $odds_away;
                $odds->odds1x2_draw = $odds_draw;
                $odds->odds1x2_draw_e = $odds_draw;
                $odds->odds1x2_datetime_update = date("Y-m-d h:i:s");

                $odds->ou_over = $ou_over;
                $odds->ou_over_e = $ou_over;
                $odds->ou_under = $ou_under;
                $odds->ou_under_e = $ou_under;
                $odds->ou = $ou;
                $odds->ou_e = $ou;
                $odds->ou_datetime_update = date("Y-m-d h:i:s");

                $odds->odds_date = date("Y-m-d");
                $odds->kick_off = strtotime($live->date);
                if (!$odds->kick_off) {
                    $odds->kick_off = strtotime($live->date . " -543 year");
                }
                $success = $odds->save();
                if ($success) {
                    $timelinegame = TimeGame::model()->find("mid=$key");
                    if ($timelinegame) {
                        $timelinegame->hdp = $odds->hdp;
                        $timelinegame->hdp_away = $odds->hdp_away;
                        $timelinegame->hdp_home = $odds->hdp_home;
                        $timelinegame->save();
                    }
                }


                $result["success"] = true;
                $result["desc"] = "save";
                $result["data"] = $odds;
            } else if ($action == "edit") {
                $odds = OddsDefined::model()->find("match_id=$key");
                $odds->type = $type;
                if ($hdp_home) {
                    $odds->hdp_home = $hdp_home;
                    $odds->hdp_home_e = $hdp_home;
                }

                if ($hdp_away) {
                    $odds->hdp_away = $hdp_away;
                    $odds->hdp_away_e = $hdp_away;
                }

                if ($hdp) {
                    $odds->hdp = $hdp;
                    $odds->hdp_e = $hdp;
                }
                if ($hdp || $hdp_away || $hdp_home)
                    $odds->hpd_datetime_update = date("Y-m-d h:i:s");

                if ($odds_home) {
                    $odds->odds1x2_home = $odds_home;
                    $odds->odds1x2_home_e = $odds_home;
                }
                if ($odds_away) {
                    $odds->odds1x2_away = $odds_away;
                    $odds->odds1x2_away_e = $odds_away;
                }
                if ($odds_draw) {
                    $odds->odds1x2_draw = $odds_draw;
                    $odds->odds1x2_draw_e = $odds_draw;
                }
                if ($odds_away || $odds_draw || $odds_home)
                    $odds->odds1x2_datetime_update = date("Y-m-d h:i:s");

                if ($ou_over) {
                    $odds->ou_over = $ou_over;
                    $odds->ou_over_e = $ou_over;
                }
                if ($ou_under) {
                    $odds->ou_under = $ou_under;
                    $odds->ou_under_e = $ou_under;
                }
                if ($ou) {
                    $odds->ou = $ou;
                    $odds->ou_e = $ou;
                }
                if ($ou || $ou_over || $ou_under)
                    $odds->ou_datetime_update = date("Y-m-d h:i:s");

                $odds->save();

                $result["success"] = true;
                $result["desc"] = "updated";
                $result["data"] = $odds;
            }
        } else {
            $result["desc"] = "match not found";
        }
        echo CJSON::encode($result);
    }

    public function actionDelOdds() {
        $key = $_REQUEST["key"];
        $odds = OddsDefined::model()->find("match_id=$key");
        $result = array("success" => false, "desc" => "nothing");
        if (!empty($odds)) {
            $odds->delete();
            $result["success"] = true;
        }
        echo json_encode($result);
    }

}

?>
