<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WBController
 *
 * @author mrsyrop
 */
class WbController extends Controller {

    public function init() {
        $this->layout = 'main2';
    }

    public function actionIndex() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $type = Yii::app()->request->getParam('type');
        $page = Yii::app()->request->getParam('page');
        if (empty($page)) {
            $page = 1;
        }
        if (empty($type)) {
            $type = 7;
        }
        $limit = 20;
        $offset = ($page - 1) * $limit;
        $criteria = new CDbCriteria();
        $criteria->condition = "type='$type'";
        $criteria->order = "pin,id DESC";
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $timelines = Timelines::model()->findAll($criteria);
        $feed = Feed_type::model()->findAll();
        $webboard = array();
        $users = array();
        
        $criteria = new CDbCriteria();
        $criteria->condition = "type='$type'";
        $criteria->order = "pin,id DESC";
        $allrecord = Timelines::model()->count($criteria);
        foreach ($timelines as $key => $timeline) {
            //echo $timeline->keygen;
            $wb = Board::model()->findByPk("{$timeline->key_list}");
            $user = User::model()->find("fb_uid='{$timeline->fb_uid}'");
            $webboard[$timeline->id] = $wb;
            $users[$timeline->id] = $user;
        }

        $this->render('index', array('timelines' => $timelines, 'webboard' => $webboard, 'users' => $users, 'feed' => $feed, 'type' => $type,'allrecord'=>$allrecord,'page'=>$page));
    }

    public function actionPin() {
        $id = Yii::app()->request->getParam('id');
        $pin = Yii::app()->request->getParam('pin');
        $results = array('success' => false, 'desc' => 'nothing');
        header('Content-type: application/json');
        $timeline = Timelines::model()->findByPk($id);
        $timeline->pin = $pin;
        $success = $timeline->save();
        if ($success)
            $results['success'] = TRUE;
        echo CJSON::encode($results);
    }

    public function actionRemove() {
        $id = Yii::app()->request->getParam('id');
        $remove = Yii::app()->request->getParam('remove');
        $results = array('success' => false, 'desc' => 'nothing');
        header('Content-type: application/json');
        $timeline = Timelines::model()->findByPk($id);
        $timeline->remove = $remove;
        $success = $timeline->save();
        if ($success)
            $results['success'] = TRUE;
        echo CJSON::encode($results);
    }

    public function actionAdmin() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $timelines = Timelines::model()->findAll("content_type='board' AND fb_uid='admin'");
        $webboard = array();
        $users = array();
        foreach ($timelines as $key => $timeline) {
            $wb = Board::model()->findByPk("{$timeline->key_list}");
            $user = User::model()->find("fb_uid='{$timeline->fb_uid}'");
            $webboard[$timeline->id] = $wb;
            $users[$timeline->id] = $user;
        }

        $this->render('admin', array('timelines' => $timelines, 'webboard' => $webboard, 'users' => $users));
    }

    public function actionAdd() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');
        $feed = Feed_type::model()->findAll();
        $this->render('add', array('category' => $feed));
    }

    public function actionSave() {
        $session = new CHttpSession;
        $session->open();
        if (!$session['admin']) {
            $this->redirect(Yii::app()->homeUrl);
        }
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($baseUrl . '/js-library/ckeditor/ckeditor.js');


        $title = $_REQUEST["title"];
        $detail = $_REQUEST['detail'];
        $video = $_REQUEST['video'];
        $category = $_REQUEST['category'];
        $mkey = "";
        $type = "none";
        if ($video) {
            $type = "video";
            $mds = new MediaStore();
            $mds->path = $video;
            $mds->created_at = date("y-m-d H:i:s");
            $mds->save();
            $mkey = $mds->id;
        } else {
            $files = $_FILES["files"];
            if ($files) {
                $type = "image";
                $imglist = array();
                for ($i = 0; $i < count($files['name']); $i++) {
                    $file = array();
                    $file['name'] = $files['name'][$i];
                    $file['type'] = $files['type'][$i];
                    $file['tmp_name'] = $files['tmp_name'][$i];
                    $file['error'] = $files['error'][$i];
                    $file['size'] = $files['size'][$i];

                    $rs = $this->FileMan("admin", $file);
                    //var_dump($rs);
                    // echo "\n";
                    $mdst = new MediaStore();
                    $mdst->path = $rs['path'];
                    $mdst->thumbnail = $rs['thumbnail'];
                    $mdst->created_at = date("y-m-d H:i:s");
                    $mdst->save();
                    //print_r($mdst->getErrors());
                    $imglist[] = $mdst;
                }

                foreach ($imglist as $md) {
                    if ($mkey) {
                        $mkey.=",{$md->id}";
                    } else {
                        $mkey = $md->id;
                    }
                }
            }
        }

        $board = new Board();
        $board->categoty = $category;
        $board->media = $mkey;
        $board->media_type = $type;
        $board->title = $title;
        $board->desc = $detail;
        $success = $board->save();
        if ($success) {
            $tl = new Timelines();
            $tl->content_type = "board";
            $tl->fb_uid = "admin";
            $tl->key_list = $board->id;
            $tl->keygen = "board" . $board->id;
            $day = date("Y-m-d H:i:s");
            $tl->created_at = $day;
            $tl->updated_at = $day;
            $tl->stamped_at = strtotime($day);
            $tl->type = $category;
            if ($tl->save()) {
                echo "success";
            } else {
                echo "failed";
            }
        }

        //var_dump($imglist);




        echo $mkey;
        $feed = Feed_type::model()->findAll();
        $this->render('add', array('category' => $feed));
    }

    function FileMan($fb_uid, $file) {
        //$file = $_FILES["file"];
        // $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "unknow";
        $rootpath = "http://ws.1ivescore.com";
        $path = "uploaded/$fb_uid";
        $dirpwd = "../";
        $results = array("success" => false, "desc" => "nothing happen", "path" => "", "thumbnail" => "");
//echo $file["name"] . ":" . $fb_uid;


        if (is_dir($dirpwd . $path)) {
            
        } else {
            mkdir($dirpwd . $path);
            chmod($dirpwd . $path, 0777);
        }


        //$allowedExts = array("gif", "jpeg", "jpg", "png");
        //$temp = explode(".", $file["name"]);
        //$extension = end($temp);
        $x = rand(0, 99);
        if ($file["size"] < 5000000) {
            if ($file["error"] > 0) {
                //echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
                $results["desc"] = $file["error"];
            } else {
                $day = date("Y-m-d h:i:s");
                $now = strtotime($day);
                $fullpath = $path . "/" . $fb_uid . $now . "x" . $x . ".png";
                $fullpathpwd = $dirpwd . $path . "/" . $fb_uid . $now . "x" . $x . ".png";
                $thumbnailpath = $path . "/thumbnail_" . $fb_uid . $now . "x" . $x . ".png";
                $thumbnailpathpwd = $dirpwd . $path . "/thumbnail_" . $fb_uid . $now . "x" . $x . ".png";
                if (file_exists($fullpath)) {
                    //echo $file["name"] . " already exists. ";                
                    $results["desc"] = "already exists";
                    $results["path"] = "$rootpath/" . $fullpath;
                } else {
                    move_uploaded_file($file["tmp_name"], $fullpathpwd);
                    $imgdetails = getimagesize($fullpathpwd);
                    if ($imgdetails) {
                        $this->resize($fullpathpwd, $thumbnailpathpwd, 200);
                        // echo "Stored in: " . "$path/" . $file["name"];
                        $results["success"] = true;
                        $results["desc"] = "completed";
                        $results["path"] = "$rootpath/" . $fullpath;
                        $results["thumbnail"] = "$rootpath/" . $thumbnailpath;
                    } else {
                        unlink($fullpath);
                        $results["desc"] = "file not image.";
                    }
                }
            }
        } else {
            //echo "Invalid file";
            $results["desc"] = "Invalid file :" . $file["type"] . "|" . $file["size"];
        }
        //echo json_encode($results);

        return $results;
    }

    function resize($path, $newPath, $max) {
        //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
        $size = GetimageSize($path);
        $_width = $size[0];
        $_height = $size[1];
        $mime = $size['mime'];
        $width = null;
        $height = null;
        if ($_width > $_height) {
            $width = $max;
            $height = round($_height * ($max / $_width));
        } else {
            $height = $max;
            $width = round($_width * ($max / $_height));
        }
        //$height=round($width*$size[1]/$size[0]);
        $new_image_ext = null;
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }
        if ($new_image_ext == 'jpg') {
            $images_orig = ImageCreateFromJPEG($path);
        } else if ($new_image_ext == 'png') {
            $images_orig = ImageCreateFromPNG($path);
        } else if ($new_image_ext == 'gif') {
            $images_orig = ImageCreateFromGIF($path);
        }

        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);

        //ImageJPEG($images_fin, $newPath);
        if ($new_image_ext == 'jpg') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageJPEG($images_fin, $newPath);
        } else if ($new_image_ext == 'png') {
            imagealphablending($images_fin, false);
            imagesavealpha($images_fin, true);
            $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
            imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
            imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
            ImagePNG($images_fin, $newPath);
        } else if ($new_image_ext == 'gif') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageGIF($images_fin, $newPath);
        }
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }

}
