<?php
//echo count($result["odds"]);
?>
<?php if (!empty($result["live"])) { ?>
    <table>
        <tr>
            <th>ลำดับ</th>
            <th>ลีก</th>
            <th>ทีมเจ้าบ้าน</th>
            <th>ทีมเยือน</th>
            <th>เวลาเตะ</th>      
            <th>HDP home</th>
            <th>HDP away</th>
            <th>HDP</th>
            <th>odds1x2 home</th>
            <th>odds1x2 away</th>
            <th>odds1x2 draw</th>
            <th>ou over</th>
            <th>ou under</th>
            <th>ou</th>
            <th>ประเภท</th>
            <th></th>
        </tr>
        <?php
        foreach ($result["live"] as $key => $live) {
            ?>
            <tr>
                <td><?php echo $key + 1; ?></td>
                <td><?php echo $live["leagueName"] . " (" . $live["leagueNameTh"] . ")" ?></td>
                <td><?php echo $live["homename"] . " (" . $live["homenameTH"] . ")" ?></td>
                <td><?php echo $live["awayname"] . " (" . $live["awaynameTH"] . ")" ?></td>
                <td><?php echo $live["showDate"]; ?></td>

                <td align="center" class="hdp_home" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["hdp_home"])) {
                        echo "-";
                    } else {
                        echo $live["hdp_home"];
                    }
                    ?>

                </td>
                <td align="center" class="hdp_away" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["hdp_away"])) {
                        echo "-";
                    } else {
                        echo $live["hdp_away"];
                    }
                    ?>
                </td>
                <td align="center" class="hdp" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["hdp"])) {
                        echo "-";
                    } else {
                        echo $live["hdp"];
                    }
                    ?>
                </td>
                <td align="center" class="odd_home" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["odds1x2_home"])) {
                        echo "-";
                    } else {
                        echo $live["odds1x2_home"];
                    }
                    ?>
                </td>
                <td align="center" class="odd_away" key="<?php echo $live["mid"] ?>">               
                    <?php
                    if (empty($live["odds1x2_away"])) {
                        echo "-";
                    } else {
                        echo $live["odds1x2_away"];
                    }
                    ?>
                </td>
                <td align="center" class="odd" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["odds1x2_draw"])) {
                        echo "-";
                    } else {
                        echo $live["odds1x2_draw"];
                    }
                    ?>
                </td>
                <td align="center" class="ou_over" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["ou_over"])) {
                        echo "-";
                    } else {
                        echo $live["ou_over"];
                    }
                    ?>
                </td>
                <td align="center" class="ou_under" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["ou_under"])) {
                        echo "-";
                    } else {
                        echo $live["ou_under"];
                    }
                    ?>
                </td>
                <td align="center" class="ou" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["ou"])) {
                        echo "-";
                    } else {
                        echo $live["ou"];
                    }
                    ?>
                </td>
                <td align="center" class="game_type" key="<?php echo $live["mid"] ?>">
                    <?php
                    if (empty($live["type"])) {
                        echo "-";
                    } else {
                        echo $live["type"];
                    }
                    ?>

                </td>
                <td class="btn-group" key = "<?php echo $live["mid"] ?>">

                    <?php
                    if (empty($live["type"])) {
                        ?>

                        <input type = "button" class = "action-btn" value = "เพิ่ม" action = "add" key = "<?php echo $live["mid"] ?>" />

                        <?php
                    } else {
                        ?>                       
                        <input type = "button" class = "action-btn" value = "แก้ไข" action = "edit" key = "<?php echo $live["mid"] ?>" />
                        <input type="button" value="ลบ" class="del-odds" key = "<?php echo $live["mid"] ?>"/>
                        <?php
                    }
                    ?>
                </td>
            </tr>
        <?php }
        ?>
    </table>
    <?php
} else {
    echo "ไม่มีข้อมูล";
}
?>
<div class="modal hide fade" id="zoneModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>เพิ่ม/แก้ไข ราคา</h3>
    </div>
    <div class="modal-body"> 
        <select id="odds_type">
            <option name="Bet365"  value="Bet365">Bet365</option>
            <option name="Ladbrokes" value="Ladbrokes">Ladbrokes</option>
            <option name="SBOBET" selected value="SBOBET">SBOBET</option>
        </select>
        <table>
            <tr>
                <td>HDP home</td>
                <td><input id="mhdp_h" type="text" placeholder="0"/></td>
            <tr>
            <tr>
                <td>HDP away</td>
                <td><input id="mhdp_a" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>HDP</td>
                <td><input id="mhdp" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>odds1x2 home</td>
                <td><input id="modd_h" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>odds1x2 away</td>
                <td><input id="modd_a" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>odds1x2 draw</td>
                <td><input id="modd" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>ou over</td>
                <td><input id="mou_over" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>ou under</td>
                <td><input id="mou_under" type="text" placeholder="0"/></td>
            </tr>
            <tr>
                <td>ou</td>
                <td><input id="mou" type="text" placeholder="0"/></td>
            </tr>
        </table>
    </div>
    <div class="modal-footer">
        <button  class="btn closeModal">ปิด</button>
        <button id="saveOdds" class="btn btn-primary">บันทึก</button>
    </div>
</div>


<script>
    $(document).ready(function(){        
        $(".action-btn").click(function(){
            var key = $(this).attr("key"); 
            var action = $(this).attr("action");
            $("#saveOdds").attr("key",key);
            $("#saveOdds").attr("action",action);
            $("#zoneModal").modal("show"); 
            
            $("#mhdp_h").val("");
            $("#mhdp_a").val("");
            $("#mhdp").val("");
            $("#modd_h").val("");
            $("#modd_a").val("");
            $("#modd").val("");
            $("#mou_over").val("");
            $("#mou_under").val("");
            $("#mou").val("");
            
            if(action=="edit"){
                var key = $(this).attr("key");
                var action = $(this).attr("action");
                var type = $(".game_type[key="+key+"]").html();
                $("#odds_type").find("option[name="+type+"]").prop("selected", true);
                $("#mhdp_h").val(parseFloat($(".hdp_home[key="+key+"]").html())?parseFloat($(".hdp_home[key="+key+"]").html()):"");
                $("#mhdp_a").val(parseFloat($(".hdp_away[key="+key+"]").html())?parseFloat($(".hdp_away[key="+key+"]").html()):"");
                $("#mhdp").val(parseFloat($(".hdp[key="+key+"]").html())?parseFloat($(".hdp[key="+key+"]").html()):"");
                $("#modd_h").val(parseFloat($(".odd_home[key="+key+"]").html())?parseFloat($(".odd_home[key="+key+"]").html()):"");
                $("#modd_a").val(parseFloat($(".odd_away[key="+key+"]").html())?parseFloat($(".odd_away[key="+key+"]").html()):"");
                $("#modd").val(parseFloat($(".odd[key="+key+"]").html())?parseFloat($(".odd[key="+key+"]").html()):"");
                $("#mou_over").val(parseFloat($(".ou_over[key="+key+"]").html())?parseFloat($(".ou_over[key="+key+"]").html()):"");
                $("#mou_under").val(parseFloat($(".ou_under[key="+key+"]").html())?parseFloat($(".ou_under[key="+key+"]").html()):"");
                $("#mou").val(parseFloat($(".ou[key="+key+"]").html())?parseFloat($(".ou[key="+key+"]").html()):"");
              
            }
        });
        $(".closeModal").click("click",function(){
            $("#zoneModal").modal("hide");
        });  
        
        $("#saveOdds").click(function(){
            var key = $(this).attr("key");
            var action = $(this).attr("action");
            var hdp_home = $("#mhdp_h").val();
            var hdp_away = $("#mhdp_a").val();
            var hdp = $("#mhdp").val();
            var odds_home = $("#modd_h").val();
            var odds_away = $("#modd_a").val();
            var odds = $("#modd").val();
            var ou_over = $("#mou_over").val();
            var ou_under = $("#mou_under").val();
            var ou = $("#mou").val();
            var type = $("#odds_type").val();
            
            var req = $.ajax({
                url:"/odds/UpdateOdds",
                type:"POST",
                data:{
                    key:key,
                    action:action,
                    hdp_home:hdp_home,
                    hdp_away:hdp_away,
                    hdp:hdp,
                    odds_home:odds_home,
                    odds_away:odds_away,
                    odds_draw:odds,
                    ou_over:ou_over,
                    ou_under:ou_under,
                    ou:ou,
                    type:type
                },
                dataType:"JSON"
            });   
            req.success(function(res){
                console.log(res);
                if(res.success){
                    $("#zoneModal").modal("hide");                
                        
                    $(".hdp_home[key="+key+"]").html(res.data.hdp_home);
                    $(".hdp_away[key="+key+"]").html(res.data.hdp_away);
                    $(".hdp[key="+key+"]").html(res.data.hdp);
                    $(".odd_home[key="+key+"]").html(res.data.odds1x2_home);
                    $(".odd_away[key="+key+"]").html(res.data.odds1x2_away);
                    $(".odd[key="+key+"]").html(res.data.odds1x2_draw);
                    $(".ou_over[key="+key+"]").html(res.data.ou_over);
                    $(".ou_under[key="+key+"]").html(res.data.ou_under);
                    $(".ou[key="+key+"]").html(res.data.ou);
                    $(".game_type[key="+key+"]").html(res.data.type);
                    $(".action-btn[key="+key+"]").attr("action","edit");
                    $(".action-btn[key="+key+"]").val("แก้ไข");
                    $(".btn-group[key="+key+"]").append("<input type=\"button\" value=\"ลบ\" class=\"del-odds\" key = "+key+"] ?>\"/>");
                      location.reload();
                }else{
                    alert(res.desc);
                }
            });
        });
        
        $(".del-odds").on("click",function(){
            var key =$(this).attr("key");
            var agree = confirm("ต้องการลบราคาคู่นี้หรือไม่");
            if(agree){
                var req = $.ajax({
                    url:"/odds/DelOdds",
                    type:"POST",
                    data:{
                        key:key
                    },
                    dataType:"JSON"
                });
                req.success(function(res){
                    if(res.success){
                        location.reload();
                    }
                });
            }
            
        })
    });
</script>