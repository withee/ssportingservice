<h2><a href="/">กลับหน้าแรก</a></h2>
<a href="/news/add">เพิ่มข่าวใหม่</a>
<div>
    <input type="text" id="page-select" value="<?php echo $offset ?>"/>
    <select id="page-limit">
        <?php
        for ($i = 1; $i <= 5; $i++) {
            $val = $i * 10;
            if ((int) $val == (int) $limit) {
                echo "<option value='$val' selected='true'>$val</option>";
            } else {
                echo "<option value='$val'>$val</option>";
            }
        }
        ?>
    </select>
    <input type="button" value="Go" id="goto-page"/>
</div>
<div id="page">
    <?php
    //var_dump($number['allnews']);
    $page = ceil((int) $number['allnews'] / (int) $limit);

    $left = 3;
    $right = $page - 2;



    echo "<a href='/news/index/offset/1/limit/$limit'><<หน้าแรก   </a>";
    for ($i = 1; $i <= $page; $i++) {
        //$shownumber = $i + 1;
        if ($i <= $left) {
            echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
        }

        if ($i > $left && $i < $right) {
            if ($i == $offset - 1) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>......$i|</a>";
            }
            if ($i == $offset) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
            }
            if ($i == $offset + 1) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>$i|......</a>";
            }
        }
        
        if ($i >= $right) {
            echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
        }
    }
    echo "<a href='/news/index/offset/$page/limit/$limit'>   หน้าสุดท้าย>></a>";
    ?>
</div>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>ชื่อหัวข้อ (ไทย)</th>
            <th>หมวดหมู่</th>
            <th>ป้ายกำกับ</th>
            <th>เวลาที่แก้ไขล่าสุด</th>
            <th>ดู/แก้ไข</th>
            <th>ลบ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($newsList as $news) {
            ?>
            <tr>
                <td><img src="<?php echo $news['imageLink'] ?>" width="100" height="100"/></td>
                <td><?php echo $news['titleTh'] ?></td>
                <td><?php echo $news['category'] ?></td>
                <td><?php echo $news['news_tag'] ?></td>
                <td><?php echo $news['createDatetime']; ?></td>
                <td><?php echo $news['updateDatetime'] ?></td>
                <td><a href="/news/edit/newsId/<?php echo $news['newsId'] ?>">คลิก</a></td>
                <td><button type="button" class="btn btn-danger removeNews" newsId="<?php echo $news['newsId'] ?>">ลบ</button></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div id="page">
    <?php
    //var_dump($number['allnews']);
    $page = ceil((int) $number['allnews'] / (int) $limit);

    $left = 3;
    $right = $page - 2;



    echo "<a href='/news/index/offset/1/limit/$limit'><<หน้าแรก   </a>";
    for ($i = 1; $i <= $page; $i++) {
        //$shownumber = $i + 1;
        if ($i <= $left) {
            echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
        }

        if ($i > $left && $i < $right) {
            if ($i == $offset - 1) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>......$i|</a>";
            }
            if ($i == $offset) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
            }
            if ($i == $offset + 1) {
                echo "<a href='/news/index/offset/$i/limit/$limit'>$i|......</a>";
            }
        }
        
        if ($i >= $right) {
            echo "<a href='/news/index/offset/$i/limit/$limit'>$i|</a>";
        }
    }
    echo "<a href='/news/index/offset/$page/limit/$limit'>   หน้าสุดท้าย>></a>";
    ?>
</div>
<script>
    $(document).ready(function () {
        $('button.removeNews').click(function () {
            var r = confirm('คุณต้องการลบข่าวนี้ใช่หรือไม่?');
            if (r) {
                var newsId = $(this).attr('newsId');
                $.post('/news/delete/newsId/' + newsId, function (data) {
                    if (data) {
                        alert('ลบสำเร็จ');
                        window.location.reload();
                    }
                });
            }

        });

        $('#goto-page').click(function () {
            var page = $("#page-select").val();
            var limit = $("#page-limit").val();
            window.location = "/news/index/offset/" + page + "/limit/" + limit;

        });
    });
</script>