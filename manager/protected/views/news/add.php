<h1>เพิ่มข่าวใหม่</h1>
<a href="/news/index">กลับหน้าข่าวทั้งหมด</a>
<?php
if ($insertSuccess == 1) {
    ?>
    <div class="alert alert-success">
        <h3>เพิ่มข้อมูลสำเร็จ</h3>
    </div>
    <?php
} else if ($insertSuccess == 2) {
    ?>
    <div class="alert alert-error">
        <h3>เพิ่มข้อมูลไม่สำเร็จ</h3>
    </div>
    <?php
} else {
    
}
?>
<div class="tabbable">
    <ul class="nav nav-tabs">
        <?php
        foreach ($langList as $index => $lang) {
            $active = $index == 0 ? 'class="active"' : '';
            ?>
            <li <?php echo $active ?>><a href="#tab<?php echo $index + 1 ?>"  data-toggle="tab"><?php echo $lang ?></a></li>
            <?php
        }
        ?>


    </ul>
    <form class="form-horizontal" action="/news/add" method="post">
        <div class="tab-content">
            <?php
            foreach ($langList as $index => $lang) {
                $active = $index == 0 ? 'active' : '';
                ?>
                <div class="tab-pane <?php echo $active ?>" id="tab<?php echo $index + 1 ?>">
                    <div class="control-group">
                        <label class="control-label">ชื่อข่าว</label>
                        <div class="controls">
                            <input type="text" placeholder="title" name="title<?php echo $langs[$index] ?>" class="span5">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">เนื้อข่าวย่อ</label>
                        <div class="controls">
                            <textarea name="shortDescription<?php echo $langs[$index] ?>" class="span5" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="control-group" style="width:800px;">
                        <label class="control-label">เนื้อข่าวเต็ม</label>
                        <div class="controls">
                            <textarea name="content<?php echo $langs[$index] ?>" class="ckeditor"></textarea>
                        </div>
                    </div>

                </div>
                <?php
            }
            ?>
        </div>
        <?php
        for ($i = 1; $i <= 4; $i++) {
            ?>
            <div class="control-group">
                <label class="control-label">เชื่อมโยง <?php echo $i ?></label>
                <div class="controls">
                    <select class="comSelect" comId="<?php echo $i ?>">
                        <option vlaue="">เลือก</option>
                        <?php
                        foreach ($comList as $com) {
                            ?>
                            <option value="<?php echo $com['competitionId'] ?>"><?php echo $com['competitionName'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select class="leagueSelect" id="leagueId<?php echo $i ?>" name="leagueId<?php echo $i ?>" lid=<?php echo $i ?>>
                    </select>
                    <select class="teamStat" name="teamId<?php echo $i ?>" id="teamId<?php echo $i ?>">
                    </select>
                </div>
            </div>
            <?php
        }
        ?>
        <div class="control-group">
            <label class="control-label">ลิงค์รูปภาพ</label>
            <div class="controls">
                <input type="text" placeholder="image link" name="imageLink" class="span7">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">อัพรูปภาพ</label>
            <div class="controls">
                <label id="select-file-work-sheet" for="upload-file-work-sheet" class="btn btn-inverse"><span class="oi oi-image"></span> อัพรูป</label>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">World Cups</label>
            <div class="controls">
                <input type="checkbox" name="worldcups" value="Y">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Category</label>
            <div class="controls">
<!--                other,thai,'premier league','la liga','bundesliga','serie a','league 1'-->
                <select name="category">
                    <option value="other">other</option>
                    <option value="thai">thai</option>
                    <option value="premier league">premier league</option>
                    <option value="la liga">la liga</option>
                    <option value="bundesliga">bundesliga</option>
                    <option value="serie a">serie a</option>
                    <option value="league 1">league 1</option>
                    <option value="ucl">ucl</option>
                    <option value="europa">europa</option>
                    <option value="other league">other league</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">groupNews</label>
            <div class="controls">
<!--                common,highlight-->
                <select name="groupNews">
                    <option value="common">common</option>
                    <option value="highlight">highlight</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">news_tag</label>
            <div class="controls">
                <input type="text" name="news_tag" placeholder="news_tag" class="span7">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">typeVideo</label>
            <div class="controls">
                <!--                other,thai,'premier league','la liga','bundesliga','serie a','league 1'-->
                <select name="typeVideo">
                    <option value="youtube">youtube</option>
                    <option value="facebook">facebook</option>
                    <option value="twitter">twitter</option>
                    <option value="dailymotion">dailymotion</option>
                    <option value="streamable">streamable</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputEmail">ลิ้งVideo</label>
            <div class="controls">
                <input type="text" class="span4" name="linkVideo" placeholder="ลิ้งVideo">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">recommentNews</label>
            <div class="controls">
                <!--                common,highlight-->
                <select name="recommentNews">
                    <option value="0">No Recomment</option>
                    <option value="1">Recomment</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" class="btn btn-primary" name="submit"  value="บันทึก"/>
            </div>
        </div>
    </form>

    <form id="imageUploadWorkSheet" enctype="multipart/form-data" style="margin-top: 10px; display: none;">
        <input type="file" name="Filedata" id="upload-file-work-sheet">
    </form>
</div>
<script type="text/javascript">


    $(document).ready(function() {

        $(document).on('change', "#upload-file-work-sheet", function (e) {
            $("#imageUploadWorkSheet").submit();
        });
        $(document).on('submit', "#imageUploadWorkSheet", function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "/Gallery/UploadImageNewsNone",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    response=jQuery.parseJSON(response);
                    console.log(response);
                    $('input[name=imageLink]').val(response['imageLink']);
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        });

//        $("input.uploadFileImageNews").each(function( index ) {
//            $("input#uploadFileImageNews").uploadify({
//                'fileSizeLimit' : '150KB',
//                'uploader': '/js/jquery.uploadify/uploadify.swf',
//                'script': "/Gallery/UploadImageNewsNone",
//                'cancelImg': '/js/jquery.uploadify/cancel.png',
//                'multi': false,
//                'auto': true,
//                'fileExt': '*.png;*.jpg',
//                'fileDesc': 'Image Files (.jpg,.png)',
//                'onComplete': function (event, queueID, fileObj, response, data) {
//                    response=jQuery.parseJSON(response);
//                    console.log(response);
//                    $('input[name=imageLink]').val(response['imageLink']);
////                    $('#ImageThumbnail_'+response['id']).attr('src',(response['thumbnail']+'?'+Math.random()));
////                console.log(event.attr('gallery_id'));
////                response=response.split(',');
////                $('div#box_photo_'+response[0]).append("<img id='img_addactivity' src='" + response[1] + "' class='img-thumbnail' style='width:20%;' />");
//                },
//                'onError': function (event, queueID, fileObj, response, data) {
//                }
//            });
//        });

        $('select.comSelect').change(function() {
            var value = $(this).val();
            var id = $(this).attr('comId');
            if (value) {
                $.post('/news/getLeagueList/comId/' + value, function(response) {
                    if (response) {
                        $('#leagueId' + id).html(response);
                        $('#teamId' + id).html('');
                    } else {

                    }
                });
            } else {
                $('leagueId' + id).html('');
            }
        });
        $('select.leagueSelect').on('change', function() {
            var value = $(this).val();
            var id = $(this).attr('lid');
            if (value) {
                $.post('/news/getTeamList/lid/' + value, function(response) {
                    if (response) {
                        $('#teamId' + id).html(response);
                    }
                });
            } else {
                $('teamId' + id).html('');
            }
        });
    });
</script>