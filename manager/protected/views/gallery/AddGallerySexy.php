<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/14/2016
 * Time: 10:39
 */
?>
<style>
    .Gallery_Image_Box {
        display: block;
        float: left;
        width: 18%;
        padding: 1%;
        position: relative;
    }
    .EditDesc{
        width:100%;
        font-size: 11px;
        cursor: pointer;
        position: absolute;
        background-color: rgba(255,255,255,0.3);
    }
    .delete-image{
        cursor: pointer;
        position: absolute;
        bottom: 0px;
    }
</style>
<div class="row">
    <div class="span12">
        <a href="/Gallery/GallerySexy" role="button" class="btn btn-primary" data-toggle="modal" style="">กลับ</a>
    </div>
</div>
<div class="row">
    <div class="span6">
        <form method="post" class="form-horizontal" id="GalleryAdd" style="width: 500px;">
            <input type="hidden" name="Gallery[id]"  value="<?php echo $newGallery->id ?>">
            <div class="control-group">
                <label class="control-label" for="inputEmail">title</label>
                <div class="controls">
                    <input type="text" id="title" placeholder="title" name="Gallery[title]" value="<?php echo $newGallery->title ?>" style="width: 100%;">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">tag</label>
                <div class="controls">
                    <input type="text" id="tag" placeholder="tag" name="Gallery[tag]" value="<?php echo $newGallery->tag ?>" style="width: 100%;">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="inputPassword">type</label>
                <div class="controls">
                    <select class="controls" name="Gallery[gall_type]" style="margin: 0px;">
                        <option <?php echo (($newGallery->gall_type=='picture')?'selected':'')?> value="picture">picture</option>
                        <option <?php echo (($newGallery->gall_type=='video')?'selected':'')?> value="video">video</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="inputPassword">category</label>
                <div class="controls">
                    <select class="controls" name="Gallery[category]" style="margin: 0px;">
                        <option <?php echo (($newGallery->category=='ฝรั่ง')?'selected':'')?> value="ฝรั่ง">ฝรั่ง</option>
                        <option <?php echo (($newGallery->category=='เอเชีย')?'selected':'')?> value="เอเชีย">เอเชีย</option>
                        <option <?php echo (($newGallery->category=='ไทย')?'selected':'')?> value="ไทย">ไทย</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"  for="inputPassword">desc</label>
                <div class="controls">
                    <textarea name="Gallery[desc]" id="desc" rows="6" style="width: 100%;"><?php echo $newGallery->desc ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <div class="controls" id="buttonManage">
                    <?php if(!empty($newGallery->id)){ ?>
                        <button style="float: right;" type="submit" class="btn btn-warning">แก้ไข</button>
                    <?php }else{ ?>
                        <button style="float: right;" type="submit" class="btn btn-primary">เพิ่ม</button>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <div class="span6">
        <?php if(!empty($newGallery->id) && $newGallery->gall_type=='picture'){ ?>
            <div style="float: left;width: 100%;">
                <div style="width: 100%">
                    <div style="width: 100%">
                        <form gallery_id="<?php echo $newGallery->id; ?>" id="imageUploadForm" enctype="multipart/form-data" style="margin-top: 10px;">
<!--                            <input type="hidden" name="alumni_id" value="--><?php //echo $value->alumni_id; ?><!--">-->
                            <label for="upload-file-transfer-2" style="border: solid 1px;border-radius: 5px;padding: 5px;color: #fff;background-color: #000;" class="custom-file-upload">
                                Select File
                            </label>
                            <!--                                                    <div class="bt-normal bt-upload">-->
                            <input multiple style="display: none;" type="file" name="Filedata[]" id="upload-file-transfer-2">
                            <!--                                                    </div>-->
                        </form>
<!--                        <input type="file" gallery_id="--><?php //echo $newGallery->id; ?><!--" id="inputUpload_--><?php //echo $newGallery->id; ?><!--"  class="inputUpload">-->
                    </div>
                </div>
                <div style="width: 100%">
                    <div style="width: 100%" id="Gallery_Image_<?php echo $newGallery->id; ?>">
                        <?php foreach ($listImage as $keyI=>$valueI){ ?>
                            <div class="Gallery_Image_Box" style="">
                                <a onclick="return confirm('ยืนยันการลบรูปนี้')" href="/gallery/DeleteImages?id=<?php echo $valueI->id; ?>"><img id="<?php echo $valueI->id; ?>" style="width: 100%;display: block;float: left;" src="<?php echo $valueI->thumbnail ?>"></a>
                                <div class="EditDesc" href="#myModalImage" data-toggle="modal" imageId="<?php echo $valueI->id; ?>" id="desc_<?php echo $valueI->id; ?>" style=""><?php echo ((!empty($valueI->desc)||$valueI->desc!=="")?$valueI->desc:'คลิกเพื่อใส่ข้อความ'); ?></div>
                            </div>
                            <?php if(($keyI+1)%5==0){?>
                                <div style="clear: both"></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php }else if(!empty($newGallery->id) && $newGallery->gall_type=='video'){ ?>
            <div style="float: left;width: 100%;">
                <form class="form-inline" method="post" action="/Gallery/AddImages" style="margin:5px 0px;">
                    <input type="hidden" name="MediaStore[id]" id="MediaStore_id" value="<?php echo $mediaStoreObject->id ?>">
                    <input type="hidden" name="MediaStore[gall_id]" value="<?php echo ((!empty($mediaStoreObject->id))?$mediaStoreObject->gall_id:$newGallery->id); ?>">
                    <div class="control-group">
                        <input style="width:520px;" type="text" class="input-small" name="MediaStore[path]" placeholder="link" value="<?php echo $mediaStoreObject->path ?>">
                    </div>
                    <div class="control-group">
                        <input style="width:520px;" type="text" class="input-small" name="MediaStore[thumbnail]" placeholder="thumbnail" value="<?php echo $mediaStoreObject->thumbnail ?>">
                    </div>
                    <div class="control-group">
                        <select name="MediaStore[typeVideo]">
                            <option value="youtube">youtube</option>
                            <option value="facebook">facebook</option>
                            <option value="twitter">twitter</option>
                            <option value="dailymotion">dailymotion</option>
                            <option value="streamable">streamable</option>
                        </select>
                        <input style="width:240px;" type="text" class="input-small" name="MediaStore[desc]" placeholder="desc" value="<?php echo $mediaStoreObject->desc ?>">
                        <?php if(!empty($mediaStoreObject->id)){ ?>
                            <button type="submit" class="btn btn-warning">edit</button>
                        <?php } else { ?>
                            <button type="submit" class="btn btn-primary">add</button>
                        <?php } ?>
                    </div>
                </form>
                <div style="clear: both"></div>
                <table style="margin-top: 10px;" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>thumbnail</th>
                        <th>Url</th>
                        <th>type</th>
                        <th>desc</th>
<!--                        <th>created_at</th>-->
                        <th>manage</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($listImage as $keyI=>$valueI){ ?>
                        <tr>
                            <td>
                                <?php echo $valueI->created_at; ?>
                                <a class="btn" href="<?php echo $valueI->path ?>" target="_blank" >
                                    <i class="icon-film"></i>
                                </a>
                            </td>
                            <td><img id="ImageThumbnail_<?php echo $valueI->id; ?>" style="width: 100%;" src="<?php echo $valueI->thumbnail;?>"/></td>
                            <td><?php echo $valueI->path ?></td>
                            <td><?php echo $valueI->typeVideo ?></td>
                            <td><?php echo $valueI->desc ?></td>
<!--                            <td>--><?php //echo $valueI->created_at ?><!--</td>-->
                            <td>
<!--                                <input type="file" class="uploadFileThumbnailImage" inImage="--><?php //echo $valueI->id ?><!--" id="uploadFileThumbnailImage_--><?php //echo $valueI->id ?><!--">-->
                                <form inImage="<?php echo $valueI->id; ?>" id="imageUploadForm-1" enctype="multipart/form-data" style="margin-top: 10px;">
                                    <!--                            <input type="hidden" name="alumni_id" value="--><?php //echo $value->alumni_id; ?><!--">-->
                                    <label for="upload-file-transfer-1" style="border: solid 1px;border-radius: 5px;padding: 5px;color: #fff;background-color: #000;" class="custom-file-upload">
                                        Select File
                                    </label>
                                    <!--                                                    <div class="bt-normal bt-upload">-->
                                    <input multiple style="display: none;" type="file" name="Filedata" id="upload-file-transfer-1">
                                    <!--                                                    </div>-->
                                </form>
                                <a class="btn btn-primary btn-mini" href="/gallery/AddGallerySexy?id=<?php echo $newGallery->id ?>&imagesId=<?php echo $valueI->id ?>" >แก้ไข</a>
                                <a class="btn btn-danger btn-mini"onclick="return confirm('ยืนยันการลบรูปนี้')" href="/gallery/DeleteImages?id=<?php echo $valueI->id; ?>">ลบ</a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
    </div>
</div>

<div class="modal" id="myModalImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">เพิ่มข้อความอธิบายใต้ภาพ</h3>
    </div>
    <div class="modal-body">
        <form id="AddImageDesc">
            <input type="hidden" name="id" id="ImageId">
            <div class="control-group">
                <!--                <label class="control-label"  for="inputPassword">desc</label>-->
                <div class="controls">
                    <input type="text" name="desc" id="DescImage"  style="width: 90%;"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input style="float: right;margin: 5px;" type="submit" class="btn" value="เพิ่มข้อความ">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer hide">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>

<script>

    $( "#myModalImage" ).submit(function( event ) {
        $.ajax({
            url: "/Gallery/AddImageDesc",
            data: {
                id: $('#ImageId').val(),
                DescImage: $('#DescImage').val()
            },
            method: 'GET'
        }).done(function (response) {
            response=jQuery.parseJSON(response);
            $('#desc_'+response['id']).html(response['DescImage']);
            $('#myModalImage').modal('hide');
        });
        event.preventDefault();
    });

    $(document).on('click','.EditDesc',function(){
//        console.log($(this).html());
        $('#ImageId').val($(this).attr('imageId'));
        $('#DescImage').val($(this).html());
    });

    $(document).on('change', "#upload-file-transfer-1", function (e){
        $("#imageUploadForm-1").submit();
    });

    $(document).on('submit', "#imageUploadForm-1", function (e){
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type:'POST',
            url: "/Gallery/UploadImage/" + $(this).attr('inImage'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            dataType: "json",
            success:function(response){
//                response=jQuery.parseJSON(response);
                $('#ImageThumbnail_'+response['id']).attr('src',(response['thumbnail']+'?'+Math.random()));
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    })

//    $("input.uploadFileThumbnailImage").each(function( index ) {
//        $("input#uploadFileThumbnailImage_" + $(this).attr('inImage')).uploadify({
//            'fileSizeLimit' : '150KB',
//            'uploader': '/js/jquery.uploadify/uploadify.swf',
//            'script': "/Gallery/UploadImage/" + $(this).attr('inImage'),
//            'cancelImg': '/js/jquery.uploadify/cancel.png',
//            'multi': true,
//            'auto': true,
//            'fileExt': '*.png;*.jpg',
//            'fileDesc': 'Image Files (.jpg,.png,.gif)',
//            'onComplete': function (event, queueID, fileObj, response, data) {
//                response=jQuery.parseJSON(response);
//                console.log(response);
//                $('#ImageThumbnail_'+response['id']).attr('src',(response['thumbnail']+'?'+Math.random()));
////                console.log(event.attr('gallery_id'));
////                response=response.split(',');
////                $('div#box_photo_'+response[0]).append("<img id='img_addactivity' src='" + response[1] + "' class='img-thumbnail' style='width:20%;' />");
//            },
//            'onError': function (event, queueID, fileObj, response, data) {
//            }
//        });
//    });

    $(document).on('change', "#upload-file-transfer-2", function (e){
        $("#imageUploadForm").submit();
    });

    $(document).on('submit', "#imageUploadForm", function (e){
        e.preventDefault();
        var formData = new FormData(this);
        var gall_id=$(this).attr('gallery_id');
        $.ajax({
            type:'POST',
            url: "/Gallery/UploadFile/" + $(this).attr('gallery_id'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            dataType: "json",
            success:function(response){
//                response=jQuery.parseJSON(response);
//                console.log(response);
                for (i = 0; i < response.length; ++i) {
//                    console.log(response[i]);
                    $('#Gallery_Image_'+response[i]['gall_id']).prepend("<div class='Gallery_Image_Box' style=''><img id='"+response[i]['id']+"' style='width: 100%;display: block;float: left;' src='"+response[i]['thumbnail']+"'></div>");


//                    data=response[i].split(',');
//                    $('div#box_photo_'+data[0]).append("<img id='img_addactivity' src='" + data[1] + "' class='img-thumbnail' style='width:25%;' />");
                };
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    });

//    $("input.inputUpload").each(function( index ) {
//        $("input#inputUpload_"+$(this).attr('gallery_id')).uploadify({
//            'fileSizeLimit' : '150KB',
//            'uploader': '/js/jquery.uploadify/uploadify.swf',
//            'script': "/Gallery/UploadFile/" + $(this).attr('gallery_id'),
//            'cancelImg': '/js/jquery.uploadify/cancel.png',
//            'multi': true,
//            'auto': true,
//            'fileExt': '*.png;*.jpg',
//            'fileDesc': 'Image Files (.jpg,.png,.gif)',
//            'onComplete': function (event, queueID, fileObj, response, data) {
//                response=jQuery.parseJSON(response);
//                $('#Gallery_Image_'+response['gall_id']).prepend("<div class='Gallery_Image_Box' style=''><img id='"+response['id']+"' style='width: 100%;display: block;float: left;' src='"+response['thumbnail']+"'></div>");
////                console.log(event.attr('gallery_id'));
////                response=response.split(',');
////                $('div#box_photo_'+response[0]).append("<img id='img_addactivity' src='" + response[1] + "' class='img-thumbnail' style='width:20%;' />");
//            },
//            'onError': function (event, queueID, fileObj, response, data) {
//            }
//        });
//    });
</script>
