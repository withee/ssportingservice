<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/6/2016
 * Time: 17:04
 */
?>
<div class="row">
    <form class="form-inline" style="margin: 5px;">
        <input type="text" class="input-medium" placeholder="title" name="search" value="<?php echo $search ?>">
        <input type="text" class="input-medium" placeholder="tag" name="tag" value="<?php echo $tag ?>">
        <button type="submit" class="btn"><i class="icon-search"></i></button>
        <a href="#myModal" role="button" class="btn btn-success" data-toggle="modal">เพิ่มใหม่</a>
    </form>
    <div class="span4" style="overflow: scroll;height: 500px;margin: 0px;">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
<!--                <th>desc</th>-->
                <th>tag</th>
            </tr>
            </thead>
            <tbody id="GalleryList">
            <?php foreach ($dataGallery as $key=>$value){ ?>
            <tr id="GalleryTable_<?php echo $value->id; ?>" class="GalleryTable"
                Gallery_id="<?php echo $value->id; ?>"
                Gallery_title="<?php echo $value->title?>"
                Gallery_desc="<?php echo $value->desc?>"
                Gallery_tag="<?php echo $value->tag?>"
            >
                <td><?php echo date('y-M-j H:i', strtotime($value->update_at)) ?></td>
                <td id="GalleryTitle_<?php echo $value->id; ?>"><?php echo $value->title ?></td>
<!--                <td>--><?php //echo $value->desc ?><!--</td>-->
                <td><?php echo $value->tag ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="span4">
            <form id="GalleryAdd">
                <input type="hidden" name="id" id="id">
                <div class="control-group">
                    <label class="control-label" for="inputEmail">title</label>
                    <div class="controls">
                        <input type="text" id="title" placeholder="title" name="title" style="width: 100%;">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">tag</label>
                    <div class="controls">
                        <input type="text" id="tag" placeholder="tag" name="tag" style="width: 100%;">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"  for="inputPassword">desc</label>
                    <div class="controls">
                        <textarea name="desc" id="desc" rows="6" style="width: 100%;"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls hide" id="buttonManage">
                        <a style="float: right;margin: 5px;" class="btn btn-danger" id="removeGallery">ลบ</a>
                        <input style="float: right;margin: 5px;" type="submit" class="btn btn-primary" value="บันทึก">
                    </div>
                </div>
            </form>
    </div>
    <div class="span4">
        <?php foreach ($dataGallery as $key=>$value){ ?>
        <div id="Gallery_<?php echo $value->id; ?>" class="Gallery_Image" style="display: none;float: left;width: 100%;">
            <div style="width: 100%">
                <div style="width: 100%">
                    <input type="file" gallery_id="<?php echo $value->id; ?>" id="inputUpload_<?php echo $value->id; ?>"  class="inputUpload">
                </div>
            </div>
            <div style="width: 100%">
                <div style="width: 100%" id="Gallery_Image_<?php echo $value->id; ?>">
                    <?php foreach ($value->Images as $keyI=>$valueI){ ?>
                        <div class="Gallery_Image_Box" style="">
                            <img id="<?php echo $valueI->id; ?>" style="width: 100%;display: block;float: left;" src="<?php echo $valueI->thumbnail ?>">
                            <div class="EditDesc" href="#myModalImage" data-toggle="modal" imageId="<?php echo $valueI->id; ?>" id="desc_<?php echo $valueI->id; ?>" style=""><?php echo ((!empty($valueI->desc)||$valueI->desc!=="")?$valueI->desc:'คลิกเพื่อใส่ข้อความ'); ?></div>
                        </div>
                        <?php if(($keyI+1)%4==0){?>
                            <div style="clear: both"></div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<div class="modal" id="myModalImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">เพิ่มข้อความอธิบายใต้ภาพ</h3>
    </div>
    <div class="modal-body">
        <form id="AddImageDesc">
            <input type="hidden" name="id" id="ImageId">
            <div class="control-group">
<!--                <label class="control-label"  for="inputPassword">desc</label>-->
                <div class="controls">
                    <input type="text" name="desc" id="DescImage"  style="width: 90%;"/>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input style="float: right;margin: 5px;" type="submit" class="btn" value="เพิ่มข้อความ">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer hide">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>

<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">สร้าง Gallery</h3>
    </div>
    <div class="modal-body">
        <form id="GalleryAddNew">
            <input type="hidden" name="id" id="id">
            <div class="control-group">
                <label class="control-label" for="inputEmail">title</label>
                <div class="controls">
                    <input type="text" id="titleNew" placeholder="title" name="title" style="width: 90%;">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword">tag</label>
                <div class="controls">
                    <input type="text" id="tagNew" placeholder="tag" name="tag" style="width: 90%;">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="inputPassword">desc</label>
                <div class="controls">
                    <textarea name="desc" id="descNew" rows="6" style="width: 90%;"></textarea>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <input style="float: right;margin: 5px;" type="submit" class="btn" value="เพิ่มใหม่">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer hide">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-primary">Save changes</button>
    </div>
</div>
<style>
    .Gallery_Image_Box {
        display: block;
        float: left;
        width: 23%;
        padding: 1%;
        position: relative;
    }
    .EditDesc{
        width:100%;
        font-size: 11px;
        cursor: pointer;
        position: absolute;
        background-color: rgba(255,255,255,0.3);
    }
</style>

<script>

    $( "#myModalImage" ).submit(function( event ) {
        $.ajax({
            url: "/Gallery/AddImageDesc",
            data: {
                id: $('#ImageId').val(),
                DescImage: $('#DescImage').val()
            },
            method: 'GET'
        }).done(function (response) {
            response=jQuery.parseJSON(response);
            $('#desc_'+response['id']).html(response['DescImage']);
            $('#myModalImage').modal('hide');
        });
        event.preventDefault();
    });

    $(document).on('click','.EditDesc',function(){
        console.log($(this).html());
       $('#ImageId').val($(this).attr('imageId'));
       $('#DescImage').val($(this).html());
    });

    $( "#GalleryAddNew" ).submit(function( event ) {
        $.ajax({
            url: "/Gallery/AddGallery",
            data: {id:$('#idNew').val(),title:$('#titleNew').val(),tag:$('#tagNew').val(),desc:$('#descNew').val()},
            method: 'GET'
        }).done(function (response) {
            response=jQuery.parseJSON(response);
//            console.log($('tr#GalleryTable_'+response['id']).length);
            if($('tr#GalleryTable_'+response['id']).length){
                $('tr#GalleryTable_'+response['id']).attr('Gallery_id',response['id']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_title',response['title']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_desc',response['desc']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_tag',response['tag']);
                $('td#GalleryTitle_'+response['id']).html(response['title']);
            }else{
                location.reload();
//                $('#GalleryList').prepend("<tr id='GalleryTable_"+response['id']+"' class='GalleryTable' Gallery_id='"+response['id']+"' Gallery_title='"+response['title']+"' Gallery_desc='"+response['desc']+"' Gallery_tag='"+response['tag']+"'><td>"+response['id']+"</td><td id='GalleryTitle_"+response['id']+"'>"+response['title']+"</td></tr>");
            }
            $('#idNew').val("");
            $('#titleNew').val("");
            $('#tagNew').val("");
            $('#descNew').val("");
        });
        event.preventDefault();
    });

    $( "#GalleryAdd" ).submit(function( event ) {
        $.ajax({
            url: "/Gallery/AddGallery",
            data: {id:$('#id').val(),title:$('#title').val(),tag:$('#tag').val(),desc:$('#desc').val()},
            method: 'GET'
        }).done(function (response) {
            response=jQuery.parseJSON(response);
//            console.log($('tr#GalleryTable_'+response['id']).length);
            if($('tr#GalleryTable_'+response['id']).length){
                $('tr#GalleryTable_'+response['id']).attr('Gallery_id',response['id']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_title',response['title']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_desc',response['desc']);
                $('tr#GalleryTable_'+response['id']).attr('Gallery_tag',response['tag']);
                $('td#GalleryTitle_'+response['id']).html(response['title']);
            }else{
                location.reload();
//                $('#GalleryList').prepend("<tr id='GalleryTable_"+response['id']+"' class='GalleryTable' Gallery_id='"+response['id']+"' Gallery_title='"+response['title']+"' Gallery_desc='"+response['desc']+"' Gallery_tag='"+response['tag']+"'><td>"+response['id']+"</td><td id='GalleryTitle_"+response['id']+"'>"+response['title']+"</td></tr>");
            }
            $('#id').val("");
            $('#title').val("");
            $('#tag').val("");
            $('#desc').val("");
        });
        event.preventDefault();
    });

    $(document).on('click','#removeGallery',function(){
        var result = confirm("ยืนยันการ ลบ?");
        if (result) {
            $.ajax({
                url: "/Gallery/RemoveGallery",
                data: {id:$('#id').val()},
                method: 'GET'
            }).done(function (response) {
                location.reload();
            });
        }
    });


    $(document).on('click','.GalleryTable',function(){
        $('.GalleryTable').removeClass('error');
        $(this).addClass('error');
        $('.Gallery_Image').hide();
//        console.log($(this).attr('Gallery_id'));
        $('#Gallery_'+$(this).attr('Gallery_id')).show();

        var Gallery_id=$(this).attr('Gallery_id');
        var Gallery_title=$(this).attr('Gallery_title');
        var Gallery_desc=$(this).attr('Gallery_desc');
        var Gallery_tag=$(this).attr('Gallery_tag');
        $('#id').val(Gallery_id);
        $('#title').val(Gallery_title);
        $('#desc').val(Gallery_desc);
        $('#tag').val(Gallery_tag);
        $('#buttonManage').removeClass('hide');
    });


    $("input.inputUpload").each(function( index ) {
        $("input#inputUpload_"+$(this).attr('gallery_id')).uploadify({
            'fileSizeLimit' : '150KB',
            'uploader': '/js/jquery.uploadify/uploadify.swf',
            'script': "/Gallery/UploadFile/" + $(this).attr('gallery_id'),
            'cancelImg': '/js/jquery.uploadify/cancel.png',
            'multi': true,
            'auto': true,
            'fileExt': '*.png;*.jpg',
            'fileDesc': 'Image Files (.jpg,.png,.gif)',
            'onComplete': function (event, queueID, fileObj, response, data) {
                response=jQuery.parseJSON(response);
                $('#Gallery_Image_'+response['gall_id']).prepend("<div class='Gallery_Image_Box' style=''><img id='"+response['id']+"' style='width: 100%;display: block;float: left;' src='"+response['thumbnail']+"'></div>");
//                console.log(event.attr('gallery_id'));
//                response=response.split(',');
//                $('div#box_photo_'+response[0]).append("<img id='img_addactivity' src='" + response[1] + "' class='img-thumbnail' style='width:20%;' />");
            },
            'onError': function (event, queueID, fileObj, response, data) {
            }
        });
    });

    $('#submit').click(function(){
        var param ={
            title:$('#title').val(),
            desc:$('#desc').val(),
            content:$('#content').val(),
            img : $('#img').attr('src')
        }

    });
</script>

