<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/14/2016
 * Time: 10:28
 */
?>
<div class="row">
    <div class="span12">
        <form class="form-inline" style="margin: 5px;">
            <input type="text" class="input-medium" placeholder="หัวข้อ" name="search" value="<?php echo $search ?>">
            <input type="text" class="input-medium" placeholder="ป้ายกำกับ" name="tag" value="<?php echo $tag ?>">
            <button type="submit" class="btn"><i class="icon-search"></i></button>
            <a href="/Gallery/AddGallerySexy" role="button" class="btn btn-primary" data-toggle="modal" style="">เพิ่ม GallerySexy</a>
        </form>
    </div>
</div>

<div class="row">
    <div class="span12">
        <div id="page">
            <?php
            //var_dump($number['allnews']);
            $page = ceil((int) $countPage / (int) $limit);

            $left = 3;
            $right = $page - 2;



            echo "<a href='/Video/VideoGeneral/offset/1/limit/$limit'><<หน้าแรก   </a>";
            for ($i = 1; $i <= $page; $i++) {
                //$shownumber = $i + 1;
                if ($i <= $left) {
                    echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                }

                if ($i > $left && $i < $right) {
                    if ($i == $offset - 1) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>......$i|</a>";
                    }
                    if ($i == $offset) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                    }
                    if ($i == $offset + 1) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|......</a>";
                    }
                }

                if ($i >= $right) {
                    echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                }
            }
            echo "<a href='/Video/VideoGeneral/offset/".(($page-1)*$limit)."/limit/$limit'>   หน้าสุดท้าย>></a>";
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="span12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>วันที่สร้าง</th>
                <th>หัวข้อ</th>
                <th>รายละเอียด</th>
                <th>ประเภท</th>
                <th>ป้ายกำกับ</th>
                <th>เวลาที่แก้ไขล่าสุด</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody id="GalleryList">
            <?php foreach ($dataGallery as $key=>$value){ ?>
                <tr class="GalleryTable" >
                    <td><?php echo date('y-M-j H:i', strtotime($value->created_at)) ?></td>
                    <td><?php echo $value->title ?></td>
                    <td><?php echo $value->desc ?></td>
                    <td><?php echo $value->category ?></td>
                    <td><?php echo $value->tag ?></td>
                    <td><?php echo date('y-M-j H:i', strtotime($value->update_at)) ?></td>
                    <td>
                        <a href="/gallery/AddGallerySexy?id=<?php echo $value->id; ?>" class="btn btn-primary">แก้ไข</a>
                        <?php if($value->gall_type=='picture'){ ?>
                            <a href="/gallery/AddGallerySexy?id=<?php echo $value->id; ?>" class="btn btn-warning">อัพรูป</a>
                        <?php }else if($value->gall_type=='video'){ ?>
                            <a href="/gallery/AddGallerySexy?id=<?php echo $value->id; ?>" class="btn btn-info">อัพลิ้ง</a>
                        <?php } ?>
                        <a href="/gallery/RemoveGallery?id=<?php echo $value->id; ?>" class="btn btn-danger">ลบ</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
