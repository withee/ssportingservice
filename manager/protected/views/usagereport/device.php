<a href="/usagereport/index">กลับหน้ารายงาน</a>

<div class="control-group">
    <label class="control-label">วันที่</label>
    <div class="controls">
        <input type="text" value="<?php echo date("Y-m-d", strtotime($day)); ?>" id="datepicker"  />
    </div>
</div>
<div class="control-group">
    <label class="control-label">ทั้งหมด</label>
    <div class="controls">
        <table border="1" width="100%">
            <tr>
                <td>web</td>
                <td>ios</td>
                <td>android</td>
                <td>summary (คน)</td>

            </tr>

            <tr>
                <td><?php echo $alldayweb; ?></td>
                <td><?php echo $alldayios; ?></td>
                <td><?php echo $alldayandroid; ?></td>
                <?php $sum = $alldayweb + $alldayios + $alldayandroid; ?>
                <td><?php echo $sum; ?></td>

            </tr>

        </table>
    </div>
    <label class="control-label">วันนี้</label>
    <div class="controls">
        <table border="1" width="100%">
            <tr>
                <td>web</td>
                <td>ios</td>
                <td>android</td>
                <td>summary (คน)</td>

            </tr>

            <tr>
                <td><?php echo $dayweb; ?></td>
                <td><?php echo $dayios; ?></td>
                <td><?php echo $dayandroid; ?></td>
                <?php $sum = $dayweb + $dayios + $dayandroid; ?>
                <td><?php echo $sum; ?></td>

            </tr>

        </table>
    </div>
</div>


<script>
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker").datepicker();
    });

    $(document).ready(function() {
        $("#datepicker").change(function() {
            //alert($(this).val());
            //$.post('/manager/usagereport/register', {day: $(this).val()});
            location = "/usagereport/device?day=" + $(this).val();
        });

    });

</script>