<a href="/usagereport/index">กลับหน้ารายงาน</a>

<div class="control-group">
    <label class="control-label">วันที่</label>
    <div class="controls">
        <input type="text" value="<?php echo date("Y-m-d", strtotime($day)); ?>" id="datepicker"  />
    </div>
    <div class="controls">
        <input type="text" value="<?php echo date("Y-m-d", strtotime($day2)); ?>" id="datepicker2"  />
    </div>
    <div class="controls">
        <input type="button" value="Find" id="find"  />
    </div>
</div>
<div class="control-group">
    <label class="control-label">ทั้งหมด</label>
    <div class="controls">
        <?php echo $number; ?>
    </div>
    <label class="control-label">ช่วงเวลานี้</label>
    <div class="controls">
        <?php echo $numberofday; ?>
    </div>
</div>
<table border="1" width="100%">
    <tr>
        <td>ID</td>
        <td>User</td>
        <td>Last logins</td>
        <td><input type="button" value="Play" class="sort" by="pts" /></td>
        <td><input type="button" value="Sgold" class="sort" by="sgold" /></td>
    </tr>
    <?php
    foreach ($userlist as $user) {
        ?>
        <tr>
            <td><?php echo $user['uid'] ?></td>
            <td><?php echo $user['display_name'] ?></td>
            <td><?php echo $user['last_login_date'] ?></td>
            <td><?php echo $user['pts'] ?></td>
            <td><?php echo $user['sgold'] ?></td>
        </tr>
        <?php
    }
    ?>
</table>

<script>
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker").datepicker();
    });

    $("#datepicker2").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker2").datepicker();
    });

    $(document).ready(function() {
        $("#find").click(function() {
            //alert($(this).val());
            //$.post('/manager/usagereport/register', {day: $(this).val()});
            location = "/usagereport/register?day=" + $("#datepicker").val() + "&day2=" + $("#datepicker2").val();
        });

        $(".sort").click(function() {
            //alert($(this).val());
            //$.post('/manager/usagereport/register', {day: $(this).val()});
            location = "/usagereport/register?day=" + $("#datepicker").val() + "&day2=" + $("#datepicker2").val()+"&sort="+$(this).attr("by");
        });

    });

</script>