<a href="/usagereport/index">กลับหน้ารายงาน</a>

<div class="control-group">
    <label class="control-label">วันที่</label>
    <div class="controls">
        <input type="text" value="<?php echo date("d/m/Y", strtotime($day)); ?>" id="datepicker"  />
    </div>
</div>
<div class="control-group">
    <label class="control-label">วันนี้</label>
    <div class="controls">
        <table border="1" width="100%">
            <tr>
                <td>match</td>
                <td>date</td>
                <td>play</td>           
            </tr>
            <?php
            foreach ($matches as $match) {
                ?>
                <tr>
                    <td><?php echo $match['homeName'] ."({$match['homeNameTh']})". " - " . $match['awayName']."({$match['awayNameTh']})"; ?></td>
                    <td><?php echo $match['show_date']; ?></td>
                    <td><?php echo $playstamp[$match['mid']]; ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>


<script>
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker").datepicker();
    });

    $(document).ready(function() {
        $("#datepicker").change(function() {
            //alert($(this).val());
            //$.post('/manager/usagereport/register', {day: $(this).val()});
            location = "/usagereport/match?day=" + $(this).val();
        });

    });

</script>