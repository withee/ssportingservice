<?php
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 9/10/2015
 * Time: 10:54
 */
?>
<style>
    td{
        border: solid 1px
    }
</style>
<script>
    $(document).ready(function(){
        $(document).on('click','button#Btn-ViewBet',function(){
            $('input#reportMid').val($(this).attr('mid'));
            $.post("/Usagereport/ViewBet",{
                mid:$(this).attr('mid')
            },function(response){
                $('input.AllCheckBox').removeAttr('checked');
                $('tbody#View-Table-Bet').html(response);
                $('div#modelBet').show();
            });
        });

        $(document).on('click','button#Btn-EditBet',function(){
            $('input#reportMid').val($(this).attr('mid'));
            $.post("/Usagereport/ViewEditBet",{
                mid:$(this).attr('mid')
            },function(response){
                $('input.AllCheckBox').removeAttr('checked');
                $('tbody#View-Table-EditBet').html(response);
                $('div#modelEditBet').show();
            });
        });

        $(document).on('click','button#Btn-ConfirmEditBet',function(){
            var data = [];
            $("input.Checkboxes").each(function (index) {
                if ($(this).is(':checked')) {
                    data.push($(this).attr('betId'));
                }
            });

            $.post("/Usagereport/ResetBet",{
                data:data
            },function(response){
                if(response=='Not update FacebookUser') {
                    alert(response);
                }else if(response=='Not Save TotalStatement'){
                    alert(response);
                }else if(response=='Not Find fb_uid'){
                    alert(response);
                }else if(response=='Not FindByPk BetId'){
                    alert(response);
                }else{
                    $('tbody#View-Table-report').html(response);
                    $('div#modelEditBet').hide();
                    $('div#modelReport').show();
                }
            });
        });

        $(document).on('click','button#Btn-Confirm',function(){
            var data = [];
            $("input.Checkboxes").each(function (index) {
                if ($(this).is(':checked')) {
                    data.push($(this).attr('betId'));
                }
            });

            $.post("/Usagereport/ClearBet",{
                data:data
            },function(response){
                if(response=='Not update FacebookUser') {
                    alert(response);
                }else if(response=='Not Save TotalStatement'){
                    alert(response);
                }else if(response=='Not Find fb_uid'){
                    alert(response);
                }else if(response=='Not FindByPk BetId'){
                    alert(response);
                }else{
                    $('tbody#View-Table-report').html(response);
                    $('div#modelBet').hide();
                    $('div#modelReport').show();
                }
            });
        });


        $(document).on('click','button#Btn-hide,button#Bet-Close',function(){
            $('div#modelBet').hide();
            $('div#modelEditBet').hide();
        });

        $(document).on('click','button#Btn-report-hide,button#Report-Close',function(){
            $('div#modelReport').hide();
        });

        $(document).on('change', "input.AllCheckBox", function () {
            $("input.Checkboxes").each(function (index) {
                if (!$(this).is(':checked')) {
                    $(this).click();
                } else {
                    $(this).removeAttr('checked');
                }
            })
        })

    })
</script>


<div class="row">
    <div class="span16 columns">
        <div class="control-group">
            <label class="control-label">วันที่</label>
            <form method="post">
                <div style="float:left;margin-left: 5px;" class="controls">
                    <input name="Match[date]" type="text" value="<?php echo date("Y-m-d", strtotime($date)); ?>" id="datepicker"  />
                </div>
                <div style="float:left;margin-left: 5px;" class="controls">
                    <input name="Match[name]" type="text" value="<?php echo $name?>" id="datepicker" placeholder="ชื่อทีม"  />
                </div>
                <div style="float:left;margin-left: 5px;" class="controls">
                    <input class="btn btn-primary" type="submit" value="ค้นหา" />
                </div>
            </form>
        </div>
    </div>
</div>
<!--<div class="row">-->
<!--    <div class="span16 columns" style="width: 100%;">-->
<!--        <div class="pagination" style="float: right;padding: 0px;margin: 0px;">-->
<!--            <ul>-->
<!--                --><?php //if($page>1){ ?>
<!--                    <li class="prev"><a href="#">&larr; Previous</a></li>-->
<!--                --><?php //} ?>
<!--                --><?php //for($i=1;$i<=$maxPage;$i++){ ?>
<!--                    <li class="--><?php //echo (($i==$page)?'active':'') ?><!--" ><a href="/usagereport/managematch/date/--><?php //echo $date ?><!--/name/--><?php //echo $name ?><!--/page/--><?php //echo $i; ?><!--">--><?php //echo $i; ?><!--</a></li>-->
<!--                --><?php //} ?>
<!--                --><?php //if($page<$maxPage){ ?>
<!--                    <li class="next"><a href="#">Next &rarr;</a></li>-->
<!--                --><?php //} ?>
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<div class="row">
    <div class="span16 columns" style="width: 100%;">
        <table class="zebra-striped">
            <thead>
                <tr>
                    <th>คู่บอล</th>
                    <th>จำนวน</th>
                    <th>จัดการ</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($timelinesgame as $key=>$value){ ?>
                    <tr style="" >
                        <td style=""><?php echo $value['homeName'].'('.$value['homeNameTh'].') :: '.$value['awayName'].'('.$value['awayNameTh'].')'; ?></td>
                        <td style="text-align: right;"><?php echo $value['count'] ?></td>
                        <td style="padding: 5px;">
                            <?php if($value['pin']=='N'){ ?>
                                <a href="/usagereport/Pin/<?php echo $value['mid'] ?>" class="btn btn-warning">ปักมุด</a>
                            <?php }else{ ?>
                                <a href="/usagereport/UnPin/<?php echo $value['mid'] ?>" class="btn btn-warning">ถอดมุด</a>
                            <?php } ?>
<!--                            <button disabled class="btn btn-primary">ใส่ผลบอล</button>-->
                            <button class="btn btn-danger" mid="<?php echo $value['mid'] ?>" id="Btn-ViewBet">Refund</button>
                            <button class="btn btn-success" mid="<?php echo $value['mid'] ?>" id="Btn-EditBet">ResetBet</button>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="modelReport" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="Report-Close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<!--                <button type="button" class="Close" id="Report-Close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">รายการ สมาชิก ที่ทำการ Refund</h4>
            </div>
            <div class="modal-body">
                <table>
                    <thead>
                    <tr>
                        <td rowspan="2" style="padding: 0px 5px;">#</td>
                        <td rowspan="2">ชื่อ</td>
                        <td colspan="3">Scoin</td>
                    </tr>
                    <tr>
                        <td>ของเดิม</td>
                        <td>จำนวนBet</td>
                        <td>ปัจจุบัน</td>
                    </tr>
                    </thead>
                    <tbody id="View-Table-report">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
<!--                <button onclick="return confirm('ยืนยันการ เคลียร์คู่บอล')" type="button" class="btn btn-primary" id="Btn-Confirm">Confirm</button>-->
                <button type="button" class="btn btn-default" id="Btn-report-hide">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="modelBet" style="<?php echo (empty($dataBet)?'display: none;':'display: block;')?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="Bet-Close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<!--                <button type="button" class="Close" id="Bet-Close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">รายการที่ Refund</h4>
            </div>
            <div class="modal-body">
                <form name="search" method="post" class="form-inline" >
                    <input name="Match[date]" type="hidden" value="<?php echo date("Y-m-d", strtotime($date)); ?>" />
                    <input name="Match[name]" type="hidden" value="<?php echo $name?>" id="datepicker" placeholder="ชื่อทีม"  />
                    <input type="hidden" name="Match[mid]" id="reportMid">
                    <input type="hidden" name="Match[type]" value="view">
                    <input type="text" name="Match[username]" placeholder="ชื่อ">
                    <input type="submit" class="btn btn-primary">
                </form>
                <table>
                    <thead>
                        <tr>
                            <td style="padding: 0px 5px;"><label><input class="AllCheckBox" value='option1' type='checkbox'></label></td>
                            <td>วันที่</td>
                            <td>ชื่อ</td>
                            <td>จำนวน</td>
                        </tr>
                    </thead>
                    <tbody id="View-Table-Bet">
                        <?php
                        if(!empty($dataBet)){
                            $sum=0;
                            foreach($dataBet as $key=>$value){
                                $sum+=$value['bet_sgold']; ?>
                                <tr>
                                    <td style="padding: 0px 5px;"><label><input class="Checkboxes" betId="<?php echo $value['betId'] ?>"  value="option1" type="checkbox"></label></td>
                                    <td style="padding: 0px 5px;"><?php echo $value['created_at'] ?>"</td>
                                    <td style="padding: 0px 5px;"><?php echo $value['display_name'] ?>"</td>
                                    <td style="padding: 0px 5px;text-align: right;"><?php echo number_format($value['bet_sgold']) ?></td>
                                </tr>
                        <?php } ?>
                            <tr><td colspan='3' style='background-color: #cccccc'>รวม</td><td><?php echo number_format($sum) ?></td></tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button onclick="return confirm('ยืนยันการ เคลียร์คู่บอล')" type="button" class="btn btn-primary" id="Btn-Confirm">Confirm</button>
                <button type="button" class="btn btn-default" id="Btn-hide">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal" id="modelEditBet" style="<?php echo (empty($dataEditBet)?'display: none;':'display: block;')?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="Bet-Close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <!--                <button type="button" class="Close" id="Bet-Close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title">รายการที่จะให้คำนวนใหม่</h4>
            </div>
            <div class="modal-body">
                <form name="search" method="post" class="form-inline" >
                    <input name="Match[date]" type="hidden" value="<?php echo date("Y-m-d", strtotime($date)); ?>" />
                    <input name="Match[name]" type="hidden" value="<?php echo $name?>" id="datepicker" placeholder="ชื่อทีม"  />
                    <input type="hidden" name="Match[mid]" id="reportMid">
                    <input type="hidden" name="Match[type]" value="edit">
                    <input type="text" name="Match[username]" placeholder="ชื่อ">
                    <input type="submit" class="btn btn-primary">
                </form>
                <table>
                    <thead>
                    <tr>
                        <td style="padding: 0px 5px;"><label><input class="AllCheckBox" value='option1' type='checkbox'></label></td>
                        <td>วันที่</td>
                        <td>ชื่อ</td>
                        <td>ผลบอล</td>
                        <td>จำนวน</td>
                    </tr>
                    </thead>
                    <tbody id="View-Table-EditBet">
                    <?php
                    if(!empty($dataEditBet)){
                        $sum=0;
                        foreach($dataEditBet as $key=>$value){
                            $sum+=$value['bet_sgold']; ?>
                            <tr>
                                <td style="padding: 0px 5px;"><label><input class="Checkboxes" betId="<?php echo $value['betId'] ?>"  value="option1" type="checkbox"></label></td>
                                <td style="padding: 0px 5px;"><?php echo $value['created_at'] ?>"</td>
                                <td style="padding: 0px 5px;"><?php echo $value['display_name'] ?>"</td>
                                <td style="padding: 0px 5px;"><?php echo $value['FT_score'] ?>"</td>
                                <td style="padding: 0px 5px;text-align: right;"><?php echo number_format($value['bet_sgold']) ?></td>
                            </tr>
                        <?php } ?>
                        <tr><td colspan='3' style='background-color: #cccccc'>รวม</td><td><?php echo number_format($sum) ?></td></tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button onclick="return confirm('ยืนยันการ คำนวนผลการเล่นใหม่')" type="button" class="btn btn-primary" id="Btn-ConfirmEditBet">Confirm</button>
                <button type="button" class="btn btn-default" id="Btn-hide">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    });
</script>