<h2><a href="/usagereport/index">กลับหน้าแรก</a></h2>
<input type="text" id="datepicker" value="<?php echo $day; ?>"/>
<?php
//echo $sql;
?>
<table class="table">
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th>user<th>  
            <th>sgold</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($userlist as $key => $user) {
            ?>
            <tr>
                <th><?php echo ($key + 1); ?></th>
                <td><?php echo "<img src=//graph.facebook.com/{$user['fb_uid']}/picture>"; ?> </td>
                <td><?php echo htmlspecialchars($user['display_name']); ?></td>
                <td><?php echo $user['win'] . "/" . $user['all']; ?></td>
                <td>
                    <select class="prize" uid="<?php echo $user['uid']; ?>">
                        <?php
                        for ($i = 0; $i <= 100; $i+=5) {
                            if (array_key_exists($user['uid'], $reward)) {
                                if ($i == $reward[$user['uid']]) {
                                    ?>
                                    <option selected="true" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                            } else {
                                ?>

                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>               
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
        $("#datepicker").change(function () {
            var day = $(this).val();
            window.location = "/usagereport/sgold?day=" + day;
        });
        $('.prize').change(function () {
            var uid = $(this).attr("uid");
            var value = $(this).val();
            var r = confirm('ยืนยันการการแจกรางวัล');
            if (r) {
                //var message = $("#" + uid).val();
                $.post('/usagereport/prize',
                        {
                            uid: uid,
                            prize: value,
                            msg: "คุณได้รับ " + value + " เพชร จากกิจกรรม TOP WIN",
                            day: $("#datepicker").val(),
                            event: "TOP SGOLD"
                        }
                , function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                    location.reload();

                });
            } else {
                location.reload();
            }

        });
    });
</script>