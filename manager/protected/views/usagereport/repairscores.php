<div class="row">
    <div class="span16"><h3>จัดการคู่บอลค้าง</h3></div>
</div>
<div class="row">
    <div class="span8"></div>
    <div class="span4">
        <form method="post" class="form-inline" >
            <input type="text" name="Date[start]" class="input-small" placeholder="วันที่เริ่ม" id="datepicker">
            <input type="text" name="Date[end]" class="input-small" placeholder="ถึงวันที่" id="datepicker1">
            <button type="submit" class="btn">ค้นหา</button>
        </form>
    </div>
</div>
<div class="row">
    <div class="span16">
        <table class="table table-bordered">
            <tr>
                <td>วันที่</td>
                <td>ลีก</td>
                <td>เจ้าบ้าน</td>
                <td>ผล</td>
                <td>เยือน</td>
                <td>สถานะ</td>
                <td>จัดการ</td>
            </tr>
            <?php foreach($matchRemain as $key=>$value){ ?>
                <tr style="<?php echo ((!empty($value['FT_score'])?'background-color: #cccccc':'')) ?>">
                    <td><?php echo $value['show_date'] ?></td>
                    <td><?php echo $value['leagueNameEn'] ?></td>
                    <td><?php echo $value['homeNameEn'] ?></td>
                    <td>
                        <?php if(!empty($value['FT_score'])){ ?>
                        <form action="/usagereport/AddScoreRepairScore" method="post" class="form-search" style="padding: 0px;margin: 0px;">
                            <input type="hidden" name="score[mid]" value="<?php echo $value['mid'] ?>">
                            <input type="text" name="score[score]" class="input-small" value="<?php echo $value['FT_score'] ?>"/>
                            <input type="submit" value="add score" class="btn">
                        </form>
                        <?php } ?>
                    </td>
                    <td><?php echo $value['awayNameEn'] ?></td>
                    <td><?php echo $value['result'] ?></td>
                    <td>
                        <a href="" class="btn btn-info">จัดการผู้เล่น</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<script>
    $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
    $("#datepicker1").datepicker({dateFormat: "yy-mm-dd"});
    $(function() {
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
        $("#datepicker1").datepicker({dateFormat: "yy-mm-dd"});
    });
</script>