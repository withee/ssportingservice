<h2><a href="/usagereport/index">กลับหน้าแรก</a></h2>
<div class="control-group">
    <label class="control-label">Page</label>
    <div class="controls">
        <select class="page-select">
            <?php
            for ($i = 1; $i <= $allpages; $i++) {
                ?>
                <option value="<?php echo $i; ?>" <?php echo ($i == $page) ? "selected" : ""; ?>><?php echo $i; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>
<table class="table">
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th>user<th>  
            <th>diamond</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($userlist as $key => $user) {
            ?>
            <tr>
                <th><?php echo $start + $key + 1; ?></th>
                <td><?php echo "<img src=//graph.facebook.com/{$user['fb_uid']}/picture>"; ?> </td>
                <td><?php echo htmlspecialchars($user->display_name); ?></td>
                <td><?php echo $user->diamond; ?></td>
                <td><input type="text" class="message" id="<?php echo $user->uid ?>"><input type="button" class="send" value="Send Message" uid="<?php echo $user->uid; ?>"></td>
                <td><?php
                    if ((int) $user->diamond >= 1500) {
                        ?>
                        <select class="exchange" uid="<?php echo $user->uid ?>">
                            <option value="0">แลกรางวัล</option>
                            <option value="1500">เสื้อ</option>
                            <option value="11000">XBOX</option>
                            <option value="14000">IPAD</option>
                            <option value="19000">PS4</option>
                            <option value="20000">สร้อยคอทองคำ</option>
                        </select>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('.send').click(function () {
            var uid = $(this).attr("uid");
            var r = confirm('ยืนยันการส่งข้อความ');
            if (r) {
                var message = $("#" + uid).val();
                $.post('/usagereport/sendmsg',
                        {
                            uid: uid, message: message
                        }
                , function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                });
            }

        });
        $('.exchange').change(function () {
            var uid = $(this).attr("uid");
            var value = $(this).val();
            var r = confirm('ยืนยันการการแลกรางวัล');
            if (r) {
                //var message = $("#" + uid).val();
                $.post('/usagereport/exchangdiamond',
                        {
                            uid: uid, value: value
                        }
                , function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                    location.reload();

                });
            } else {
                location.reload();
            }

        });
        $(".page-select").change(function () {
            var page = $(this).val();
            // alert(type);
            location = "/usagereport/diamond?page=" + page;
        });
    });
</script>