<h1>รายชื่อทีมทั้งหมด</h1>
<h3><a href="javascript:history.back();">กลับหน้าลีกทั้งหมด</a></h3>
<input type="checkbox"  name="all" /> ต้องการข้อมูลทุกทีม
<table class="table table-bordered">
    <thead>
        <tr>
            <th>ชื่อทีม</th>
            <th>ไทย</th>
            <th>อังกฤษ</th>
            <th>จีนดังเดิ่ม</th>
            <th>จีนประยุกษ์</th>
            <th>เกาหลี</th>
            <th>เวียดนาม</th>
            <th>ลาว</th>
            <th></th>
            <th></th>
        </tr>
    </thead><tbody>
        <?php
        foreach ($list as $obj) {
            if (!empty($obj["id7m"])) {
                ?>
                <tr>
                    <?php
                    if ($obj['tid7m']) {
                        ?>
                        <td ><a href="javascript:void(0)" class="removeTeam7m" teamName="<?php echo $obj['teamName'] ?>" id7m="<?php echo $obj['tid7m'] ?>"><?php echo $obj['teamName'] ?></a></td>

                        <?php
                    } else {
                        ?>
                        <td><?php echo $obj['teamName'] ?></td>
                        <?php
                    }
                    ?>

                    <td><input type="text" value="<?php echo $obj['teamNameTh'] ?>" id="th-<?php echo $obj['tid'] ?>" class="span1"/></td>
                    <td><input type="text" value="<?php echo $obj['teamNameEn'] ?>" id="en-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><input type="text" value="<?php echo $obj['teamNameBig'] ?>" id="big-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><input type="text" value="<?php echo $obj['teamNameGb'] ?>" id="gb-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><input type="text" value="<?php echo $obj['teamNameKr'] ?>" id="kr-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><input type="text" value="<?php echo $obj['teamNameVn'] ?>" id="vn-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><input type="text" value="<?php echo $obj['teamNameLa'] ?>" id="la-<?php echo $obj['tid'] ?>" class="span1" /></td>
                    <td><a href="javascript:void(0)" class="7m" tid="<?php echo $obj['tid'] ?>" teamName="<?php echo $obj['teamName'] ?>" id7m="<?php echo $league['id7m'] ?>">7m</a></td>
                    <td>
                        <?php
                        if (empty($obj['teamNameTh']) && empty($obj['teamNameEn']) && empty($obj['teamNameBig']) && empty($obj['teamNameGb']) && empty($obj['teamNameKr']) && empty($obj['teamNameVn']) && empty($obj['teamNameLa'])) {
                            ?>
                            <i class="icon-plus actbtn" act="add" key="<?php echo $obj['tid'] ?>" lid7m="<?php echo $obj["id7m"]; ?>" teamName="<?php echo $obj["teamName"] ?>" tnPk="<?php echo $obj["tnPk"] ?>" tid="<?php echo $obj["tid"] ?>"/>
                            <?php
                        } else {
                            ?>
                            <i class="icon-pencil actbtn" act="edit" key="<?php echo $obj['tid'] ?>" lid7m="<?php echo $obj["id7m"]; ?>" teamName="<?php echo $obj["teamName"] ?>" tnPk="<?php echo $obj["tnPk"] ?>" tid="<?php echo $obj["tid"] ?>"/>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>
<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>รายชื่อทีมจาก 7m</h3>
    </div>
    <div class="modal-body">
        <p>ชื่อทีมจาก futbol24 : <span id="teamName"></span></p>
        ชื่อทีมจาก 7m
        <select id="id7m" tid="">
            <option value="เลือก">เลือก</option>
        </select>
    </div>
    <div class="modal-footer">
        <button  class="btn closeModal">ปิด</button>
        <button id="saveBt" class="btn btn-primary">บันทึก</button>
    </div>
</div>
