<!-- select date -->
<form>
    วันที่ : 
    <select class="showSelectDate span1" id="selectDate">
    </select>
    &nbsp;&nbsp;
    เดือน : 
    <select class="showSelectMonth span2" id="selectMonth">
    </select>
    &nbsp;&nbsp;
    ปี :
    <select class="showSelectYear span2" id="selectYear">
    </select> 
</form>
<button type="button" class="searchMatch btn">ค้นหา</button>  
<br>
<br>

<!-- select condition -->
<form>
    เงื่อนไข :
    <select class="span3" id="selectCon">
        <option value ="0">H2H Home Hdp</option>
        <option value ="1">H2H Away Hdp</option>
    </select>
    &nbsp;&nbsp;
    %การชนะ &nbsp;&nbsp;
    <select class="span1" id="selectSym">
        <option value=">"> > </option>
        <option value="<"> < </option>
        <option value="="> = </option>
    </select>  
    &nbsp;&nbsp;

    <input type="text" id ="inputNum" class="span1" placeholder="number"> 
    <span id="cheangType"> % </span>
    &nbsp;จำนวน hdp h2h
    <input type="text" id ="inputCountMatch" value="" class="span1" placeholder="all"> 
    match ล่าสุด
    &nbsp;จำนวน hdp
    <input type="text" id ="inputCountMatchDHP" value="" class="span1" placeholder="all"> 
    match ล่าสุด

</form>
<button type="button" class="addCondition btn">เพิ่มเงื่อนไข</button>  
<br>
<br>

<!-- show condition -->
<div style="">
    <table class="table table-hover table-bordered">
        <thead>
            <tr style="background: gainsboro;">
                <th style="text-align: center" >เงื่อนไข</th>
                <th >view</th>
            </tr>
        </thead>
        <tbody id="showConditionAdd">
<!--            <tr>
                <td>...</td>
                <td>...</td>
                <td>...</td>
            </tr>-->
        </tbody>
    </table>
</div>
<button type="button" class="searchMatch btn">clear</button>  
<br>
<br>
<!-- show match -->
<div style="">
    <table class="table table-hover table-bordered" style="font-size: 12px;">
        <thead>
            <tr style="background: gainsboro;">
                <th>วันที่</th>
                <th style="text-align: center ;">ลีค</th>
                <th style="text-align: right;">ทีมเหย้า</th>
                <th style="text-align: center; width: 50px;" >vs</th>
                <th >ทีมเยือน</th>
                <th>HDP h2h ทีมเหย้า(%)</th>
                <th>HDP h2h ทีมเยือน(%)</th>
                <th>HDP ทีมเหย้า(%)</th>
                <th>HDP ทีมเยือน(%)</th>
                <th>HDP h2h ทีมเหย้าเฉลี่ย</th>
                <th>HDP h2h ทีมเยือนเฉลี่ย</th>
                <th>score ทีมเหย้าเฉลี่ย</th>
                <th>score ทีมเยือนเฉลี่ย</th>
                <th>h2h ทีมเหย้า(%)</th>
                <th>h2h ทีมเยือน(%)</th>
            </tr>
        </thead>
        <tbody id="showMatchList">

        </tbody>
    </table>
</div>

