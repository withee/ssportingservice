<?php
/**
 * Created by PhpStorm.
 * User: sootipong
 * Date: 11/16/2015
 * Time: 13:31
 */
?>
<h3>เพิ่มสถานะ เซริฟเวอร์</h3>
<div class="row">
    <div class="span12">
        <form method="post" class="form-inline">
            <span>ข่าว</span>
            <select class="span1" name="server[news]">
                <option value="Y">Y</option>
                <option value="N">N</option>
            </select>
            <span>เวริฟเวอร์</span>
            <select class="span1" name="server[server_online]">
                <option value="Y">Y</option>
                <option value="N">N</option>
            </select>
            <input type="text" name="server[title]" class="input-small span3" placeholder="title">
            <textarea type="text" name="server[message]" class="input-small span5" placeholder="message"></textarea>
            <button type="submit" class="btn">เพิ่ม</button>
        </form>
    </div>
</div>
<div class="row">
    <div class="span12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>news</th>
                <th>server_online</th>
                <th>title</th>
                <th>message</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($dataServer as $key=>$value){ ?>
            <tr>
                <td><?php echo $value->created_at ?></td>
                <td><?php echo $value->news ?></td>
                <td><?php echo $value->server_online ?></td>
                <td><?php echo $value->title ?></td>
                <td><?php echo $value->message ?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>