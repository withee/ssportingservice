<h1>รายชื่อลีกทั้งหมด</h1>
<h3><a href="/site/competition">กลับหน้าประเทศทั้งหมด</a></h3>
<input type="checkbox"  name="all" /> ต้องการข้อมูลทุกลีก
<input type="checkbox"  name="all_league" /> ต้องการลีกทั้งหมด
<table class="table table-striped">
    <thead>
        <tr>
            <th>ชื่อลีก</th>
            <th>ไทย</th>
            <th>อังกฤษ</th>
            <th>จีนดังเดิ่ม</th>
            <th>จีนประยุกษ์</th>
            <th>เกาหลี</th>
            <th>เวียดนาม</th>
            <th>ลาว</th>
            <th></th>
            <th>ลำดับ</th>         
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($list as $obj) {
//            var_dump($obj);
//            exit();
            ?>
            <tr>
                <?php
                if ($obj['id7m']) {
                    ?>
                    <td ><a href="javascript:void(0)" class="removeLeague7m" leagueName="<?php echo $obj['leagueName'] ?>" id7m="<?php echo $obj['id7m'] ?>"><?php echo $obj['leagueName'] ?></a></td>

                    <?php
                } else {
                    ?>
                    <td ><?php echo $obj['leagueName'] ?></td>
                    <?php
                }
                ?>
                <td><input type="text" value="<?php echo $obj['leagueNameTh'] ?>" id="th-<?php echo $obj['leagueId'] ?>" class="span1"/></td>
                <td><input type="text" value="<?php echo $obj['leagueNameEn'] ?>" id="en-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['leagueNameBig'] ?>" id="big-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['leagueNameGb'] ?>" id="gb-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['leagueNameKr'] ?>" id="kr-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['leagueNameVn'] ?>" id="vn-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['leagueNameLa'] ?>" id="la-<?php echo $obj['leagueId'] ?>" class="span1" /></td>
                <?php
                if (!empty($obj['leagueNameTh']) && !empty($obj['leagueNameEn']) && !empty($obj['leagueNameBig']) && !empty($obj['leagueNameGb']) && !empty($obj['leagueNameKr']) && !empty($obj['leagueNameVn']) && !empty($obj['leagueNameLa'])) {
                    ?>
                    <td id="edit-<?php echo $obj['leagueId']; ?>"></td>
                <?php } else { ?>
                    <td id="edit-<?php echo $obj['leagueId']; ?>"><a href="javascript:void(0)" data-toggle="tooltip" title="Save"><i class="icon-hdd saveLeagueLang" leaguename="<?php echo $obj['leagueName']; ?>" leagueid="<?php echo $obj['leagueId']; ?>" id7m="<?php echo $com['id7m'] ?>" comid="<?php echo $competitionid; ?>"></i></a></td>
                <?php } ?>
                <td><a href="javascript:setLeaguePriority('<?php echo $obj['leagueId'] ?>','<?php echo $obj['leaguePriority'] ?>')"><?php echo $obj['leaguePriority'] ? $obj['leaguePriority'] : '?' ?></a></td>
                <td><a href="javascript:void(0)" class="7m" leagueId="<?php echo $obj['leagueId'] ?>" leagueName="<?php echo $obj['leagueName'] ?>" id7m="<?php echo $com['id7m'] ?>">7m</a></td>
                <td><a href="/team/index/leagueId/<?php echo $obj['leagueId'] ?>">ดูทีมทั้งหมด</a>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>รายชื่อลีกจาก 7m</h3>
    </div>
    <div class="modal-body">
        <p>ชื่อลีกจาก futbol24 : <span id="leagueName"></span></p>
        ชื่อลีกจาก 7m
        <select id="id7m" leagueId="">
            <option value="เลือก">เลือก</option>
        </select>
    </div>
    <div class="modal-footer">
        <button  class="btn closeModal">ปิด</button>
        <button id="saveBt" class="btn btn-primary">บันทึก</button>
    </div>
</div>