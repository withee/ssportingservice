<h1>แก้ไข instruction</h1>
<a href="/instruction/index">กลับหน้า instruction ทั้งหมด</a>
<form class="form-horizontal" action="/instruction/edit/instructionId/<?php echo $instructionId ?>" method="post">
    <div class="control-group">
        <div class="controls">
            <input type="submit" name="update" value="Update">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">ชื่อเมนู</label>

        <div class="controls">
            <input type="text" placeholder="title" name="menu" class="span5" value="<?php echo $menu ?>">
        </div>
    </div>
    <div class="control-group" style="width:800px;">
        <label class="control-label">รายละเอียด</label>

        <div class="controls">
            <textarea name="detail" class="ckeditor"><?php echo $detail ?></textarea>
        </div>
    </div>
</form>
