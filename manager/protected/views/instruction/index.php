<h2><a href="/">กลับหน้าแรก</a></h2>
<a href="/instruction/add">เพิ่ม instruction ใหม่</a>
<table class="table">
    <thead>
        <tr>
            <th>#</th>
            <th>เมนู</th>  
            <th>ดู/แก้ไข</th>
            <th>ลบ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($instruction as $key => $ins) {
            ?>
            <tr>
                <td><?php echo $ins->id ?></td>
                <td><?php echo $ins->menu ?></td>
                <td><a href="./edit/instructionId/<?php echo $ins->id?>">คลิก</a></td>
                <td><button type="button" class="btn btn-danger removeInstruction" instructionId="<?php echo $ins->id?>">ลบ</button></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('button.removeInstruction').click(function() {
            var r = confirm('คุณต้องการลบวิธีใช้นี้ใช่หรือไม่?');
            if (r) {
                var newsId = $(this).attr('instructionId');
                $.post('/instruction/delete/instructionId/' + newsId, function(data) {
                    if (data) {
                        alert('ลบสำเร็จ');
                        window.location.reload();
                    }
                });
            }

        });
    });
</script>