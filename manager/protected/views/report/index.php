<h2><a href="/">กลับหน้าแรก</a></h2>
<table class="table" style="table-layout: fixed">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th style="width: 200px">เนื้อหา</th>
        <th style="width: 10px">จำนวน</th>
        <th style="width: 10px">ลบ</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($report as $key => $ins) {
        foreach($ins as $keySlice => $insSlice){
    ?>
            <tr>
                <td style="width: 10px"><?php echo $insSlice->id ?></td>
                <td style="width: 200px;word-wrap: break-word;"><?php echo $insSlice->message ?></td>
                <td style="width: 10px"><?php echo $insSlice->report ?></td>
                <td style="width: 10px"><button type="button" class="btn btn-danger removeRow" attrType="<?php echo $key ?>" attrId="<?php echo $insSlice->id?>">ลบ</button></td>
            </tr>
    <?php
        }
    }
    ?>
    </tbody>
</table>
<script>
    $(document).ready(function() {
        $('button.removeRow').click(function() {
            var r = confirm('คุณต้องการลบเนื้อหานี้ใช่หรือไม่?');
            if (r) {
                var type = $(this).attr('attrType');
                var id = $(this).attr('attrId');
                $.post('/report/delete',{'type': type, 'id': id}, function(data) {
                    if (data) {
                        alert('ลบสำเร็จ');
                        //alert(data);
                        window.location.reload();
                    }
                });
            }

        });
    });
</script>