<script>
    $(document).ready(function() {
        $('#datetimepicker1').datetimepicker({
            language: 'pt-BR'
        });
        $('button.removePushList').click(function() {
            var messageid = $(this).attr('messageid');
            var r = confirm('คุณต้องการข้อความนี้ใช่ไหม');
            if (r) {
                $.post('../site/removePushList/messageid/' + messageid, function(response) {
                    if (response) {
                        alert('ลบสำเร็จ');
                        window.location = '../site/pushList';
                    }
                });
            }
        });
    });
</script>
<h1>รายการข้อความ (push notification)</h1>
<h2><a href="/">กลับหน้าแรก</a></h2>
<form method="post">
    <table class="table">
        <tr>
            <td>#</td>
            <td>ข้อความ</td>
            <td>เวลาที่ต้องการส่ง</td>
            <td>เวลาที่สร้าง</td>
            <td>สถานะการส่ง (ios)</td>
            <td>สถานะการส่ง (android)</td>
            <td>ลบ</td>
        </tr>
        <tr>
            <td></td>
            <td><input type="text" name="message" /></td>
            <td>
                <div class="well">
                    <div id="datetimepicker1" class="input-append date">
                        <input data-format="yyyy-MM-dd hh:mm:ss" type="text" name="senddatetime"></input>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                            </i>
                        </span>
                    </div>
                </div>
            </td>
            <td><input type="submit" name="submit" class="btn btn-primary" value="ส่ง"/></td>
            <td></td>
            <td></td>
        </tr>
        <?php
        foreach ($list as $obj) {
            ?>
            <tr>
                <td><?php echo $obj['messageid'] ?></td>
                <td><?php echo $obj['message'] ?></td>
                <td><?php echo $obj['senddatetime'] ?></td>
                <td><?php echo $obj['createdatetime'] ?></td>
                
                <td><?php echo $obj['ios_sent'] ?></td>
                <td><?php echo $obj['android_sent'] ?></td>
                <td><button type="button" class="btn btn-danger removePushList" messageid="<?php echo $obj['messageid'] ?>">ลบ</button></td>
            </tr>
            <?php
        }
        ?>
    </table>
</form>