
<h1>ประเทศทั้งหมด</h1>
<h2><a href="/">กลับหน้าแรก</a></h2>
<input type="checkbox"  name="all" /> ต้องการข้อมูลทุกประเทศ
<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>ไทย</th>
            <th>อังกฤษ</th>
            <th>จีนดังเดิ่ม</th>
            <th>จีนประยุกษ์</th>
            <th>เกาหลี</th>
            <th>เวียดนาม</th>
            <th>ลาว</th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        //echo count($list);
        foreach ($list as $key => $obj) {
//            var_dump($obj);
//            exit();
            ?>
            <tr>
                <?php
                if ($obj['id7m']) {
                    ?>
                    <td><a href="javascript:void(0)" class="removeCom7m" leagueName="<?php echo $obj['competitionName'] ?>" id7m="<?php echo $obj['id7m'] ?>"><?php echo $obj['competitionName'] ?></a></td>
                    <?php
                } else {
                    ?>
                    <td><?php echo $obj['competitionName'] ?></td> 
                    <?php
                }
                ?>

                <td><input type="text" value="<?php echo $obj['comNameTh'] ?>" id="th-<?php echo $obj['competitionId'] ?>" class="span1"/></td>
                <td><input type="text" value="<?php echo $obj['comNameEn'] ?>" id="en-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['comNameBig'] ?>" id="big-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['comNameGb'] ?>" id="gb-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['comNameKr'] ?>" id="kr-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['comNameVn'] ?>" id="vn-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <td><input type="text" value="<?php echo $obj['comNameLa'] ?>" id="la-<?php echo $obj['competitionId'] ?>" class="span1" /></td>
                <?php
                if (!empty($obj['comNameTh']) && !empty($obj['comNameEn']) && !empty($obj['comNameBig']) && !empty($obj['comNameGb']) && !empty($obj['comNameKr']) && !empty($obj['comNameVn']) && !empty($obj['comNameLa'])) {
                    ?>
                    <td id="edit-<?php echo $obj['competitionId']; ?>"></td>
                <?php } else { ?>
                    <td id="edit-<?php echo $obj['competitionId']; ?>"><a href="javascript:void(0)" data-toggle="tooltip" title="Save"><i class="icon-hdd saveLang" completname="<?php echo $obj['competitionName'] ?>" completid="<?php echo $obj['competitionId'] ?>"></i></a></td>
                <?php } ?>
                <td><a href="javascript:void(0)" class="7m" competitionId="<?php echo $obj['competitionId'] ?>" comName="<?php echo $obj['competitionName'] ?>">7m</a></td>
                <td><a href="/league/index/competitionId/<?php echo $obj['competitionId'] ?>">ดูลีกทั้งหมด</a>
                <td><a href="/team/index/competitionId/<?php echo $obj['competitionId'] ?>">ดูทีมทั้งหมด</a>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div class="modal hide fade" id="comModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>รายชื่อประเทศจาก 7m</h3>
    </div>
    <div class="modal-body">
        <p>ชื่อประเทศจาก futbol24 : <span id="comName"></span></p>
        ชื่อประเทศจาก 7m
        <select id="competitionId" original="">
            <option value="เลือก">เลือก</option>
        </select>
    </div>
    <div class="modal-footer">
        <button  class="btn closeModal">ปิด</button>
        <button id="saveComBt" class="btn btn-primary">บันทึก</button>
    </div>
</div>

<div class="modal hide fade" id="zoneModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>โซนการแข่งขันจาก 7m</h3>
    </div>
    <div class="modal-body">     
        <div>
            โซนการแข่งขันจาก 7m
        </div>
        <select id="zoneselect">
            <option value="13">Euro</option>
            <option value="26">America</option>
            <option value="25">Asia</option>
            <option value="27">Oceania,Africa</option>
            <option value="29">Other</option>
        </select>
    </div>
    <div class="modal-footer">
        <button  class="btn closeModal">ปิด</button>
        <button id="saveZone" class="btn btn-primary">บันทึก</button>
    </div>
</div>