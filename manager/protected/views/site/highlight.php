<h1>ไฮไลท์คู่บอล</h1>
<h2><a href="/">กลับหน้าแรก</a></h2>
<h4><a href="/site/highlight">ผลวันนี้</a>&nbsp;&nbsp;<a href="/site/highlight/date_selected/<?php echo $yesterday_date?>">ผลเมื่อวาน</a></a></h4>
<table class="table table-bordered">
    <thead>
        <tr>
            <td>เวลา</td>
            <td>ชื่อประเทศ</td>
            <td>ชื่อลีก</td>
            <td>ทีมเหย้า</td>
            <td>ผล</td>
            <td>ทีมเยือน</td>
            <td>เพิ่มวิดีโอ</td>

        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($live_league_list as $live_league) {

            for ($i = 0; $i < count($live_match_list); $i++) {
                if ($live_match_list[$i]['subleagueId'] != $live_league['subleagueId']) {
                    continue;
                }
                ?>
                <tr>
                    <td><?php echo $live_match_list[$i]['date'] ?></td>
                    <td><?php echo $live_league['competitionName'] ?></td>
                    <td><?php echo $live_league['ln'] ?></td>
                    <td><?php echo $live_match_list[$i]['hn'] ?></td>
                    <td><?php echo $live_match_list[$i]['s1'] ?></td>
                    <td><?php echo $live_match_list[$i]['gn'] ?></td>
                    <td><button type="button" class="btn addVideo btn-success" hid="<?php echo $live_match_list[$i]['hid'] ?>" gid="<?php echo $live_match_list[$i]['gid'] ?>" leagueId="<?php echo $live_league['leagueId'] ?>" competitionId="<?php echo $live_match_list[$i]['competitionId'] ?>" mid="<?php echo $live_match_list[$i]['mid'] ?>">เพิ่ม</button></td>
                </tr>
                <?php
            }
            ?>

            <?php
        }
        ?>
    </tbody>
</table>
<div class="modal hide fade" id="myModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>เพิ่มวิดีโอ</h3>
    </div>
    <div class="modal-body" id="modal-content-1" style="display:none">
        <form id="video-highlight">
            <input type="text" name="title1" placeholder="หัวข้อ 1" class="video_highlight"/>
            <input type="text" name="content1" placeholder="ลิงค์วิดีโอ 1" class="span5 video_highlight" />
            <input type="hidden" name="id1" class="video_highlight" />
            <input type="text" name="title2" placeholder="หัวข้อ 2" class="video_highlight"/>
            <input type="text" name="content2" placeholder="ลิงค์วิดีโอ 2" class="span5 video_highlight" />
            <input type="hidden" name="id2" class="video_highlight" />
            <input type="text" name="title3" placeholder="หัวข้อ 3" class="video_highlight"/>
            <input type="text" name="content3" placeholder="ลิงค์วิดีโอ 3" class="span5 video_highlight" />
            <input type="hidden" name="id3" class="video_highlight" />
            <input type="hidden" name="mid" class="video_highlight" />
            <input type="hidden" name="hid" class="video_highlight"/>
            <input type="hidden" name="gid" class="video_highlight"/>
            <input type="hidden" name="leagueId" class="video_highlight"/>
            <input type="hidden" name="competitionId" class="video_highlight"/>

        </form>
    </div>
    <div class="modal-body" id="modal-content-2">
        <img src="/images/loading.gif" />
    </div>
    <div class="modal-footer">
        <input type="submit" name="submit" value="ยกเลิก" class="btn" id="closeButton"/>
        <input type="submit" name="submit" value="เพิ่มคลิป" class="btn btn-primary" id="submitButton" />
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('button.addVideo').click(function() {
            $('#myModal').modal('show');
            $('#modal-content-1').hide();
            $('#modal-content-2').show();
            $('input.video_highlight').val("");
            var mid = $(this).attr('mid');
            var hid = $(this).attr('hid');
            var gid = $(this).attr('gid');
            var leagueId = $(this).attr('leagueId');
            var competitionId = $(this).attr('competitionId');
            $('input[name="mid"]').val(mid);
            $('input[name="hid"]').val(hid);
            $('input[name="gid"]').val(gid);
            $('input[name="leagueId"]').val(leagueId);
            $('input[name="competitionId"]').val(competitionId);
            $.get('/site/getHighlight/mid/' + mid, function(response) {
                if (response) {
                    $('#modal-content-1').show();
                    $('#modal-content-2').hide();
                    for (var i = 0; i < response.length; i++) {
                        var index = i + 1;
                        $('input[name="title' + index + '"]').val(response[i]['title']);
                        $('input[name="content' + index + '"]').val(response[i]['content']);
                        $('input[name="id' + index + '"]').val(response[i]['video_id']);
                    }
                }
            }, 'json');
        });
        $('#closeButton').click(function() {
            $('#myModal').modal('hide');
        });
        $('#submitButton').click(function() {

            var r = confirm("คุณต้องการเพิ่ม/แก้ไข วิดีโอใช่ไหม?");
            if (r) {
                var param = $('#video-highlight').serialize();

                $.post('/site/addVideoHighlight', param, function(response) {
                    if(response){
                        alert("success");
                    }else{
                        alert("fail");
                    }
                    $('#myModal').modal('hide');
                });
                
//                $.ajax('/site/addVideoHighlight', param, function(response) {
//                    if(response['success']){
//                        alert("success");
//                    }else{
//                        alert("fail");
//                    }
//                    $('#myModal').modal('hide');
//                });
                
            }
        });
    });
</script>