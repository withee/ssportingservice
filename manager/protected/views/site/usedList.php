<div class="row">
    <script>
        $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
        $(function() {
            $("#datepicker").datepicker({dateFormat: "yy-mm-dd"});
        });

        $("#datepicker2").datepicker({dateFormat: "yy-mm-dd"});
        $(function() {
            $("#datepicker2").datepicker({dateFormat: "yy-mm-dd"});
        });
    </script>
<div class="span9" style="width: 100%;">
    <h3><a href="/">กลับหน้าหลัก</a></h3>
    <form class="form-form-inline">
        <div class="form-group" style="float: left;">
            <div class="controls">
                <label class="control-label">วันที่</label>
                <input type="text" name="date" value="<?php echo $date->format('Y-m-d'); ?>" id="datepicker"  />
            </div>
        </div>
        <div class="form-group" style="float: left;">
            <div class="controls">
                <label class="control-label">ถึง</label>
                <input type="text" name="date2" value="<?php echo $date2->format('Y-m-d'); ?>" id="datepicker2"  />
            </div>
        </div>
        <div class="form-group" style="float: left;">
            <div class="controls" style="padding-top: 25px;">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    <div style="clear: both;"></div>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {

            var id=$('#chart').attr('subProjectId');
            var date=$('input[name="date"]').val();
            var date2=$('input[name="date2"]').val();
            var req = $.ajax({
                type: 'GET',
                url: '/site/getChartNotification?date='+date+'&date2='+date2,
                dataType: 'JSON'
            });
            req.done(function(res) {
                if (res.status == 'success') {
                    obj = [];
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'day');
                    data.addColumn('number', 'ios');
                    data.addColumn('number', 'android');
                    data.addColumn('number', 'web');
                    var id = 0;

                    $.each(res.data.ios, function (i, elem) {
                        var logDate = [];
                        logDate.push(i);
                        if(typeof(res.data.ios[i])!=="undefined") {
                            logDate.push(parseInt(res.data.ios[i]));
                        }else{
                            logDate.push(0);
                        }

                        if(typeof(res.data.android[i])!=="undefined") {
                            logDate.push(parseInt(res.data.android[i]));
                        }else{
                            logDate.push(0);
                        }

                        if(typeof(res.data.web[i])!=="undefined") {
                            logDate.push(parseInt(res.data.web[i]));
                        }else{
                            logDate.push(0);
                        }

                        obj.push(logDate);
                    });
                    data.addRows(obj);
                    var options = {
                        title: 'กราฟจำนวนสมาชิกผู้เข้าใช้งาน',
                        hAxis: {title: 'วัน',  titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        legend: {position: 'top', maxLines: 3},
                        chartArea:{left:50,top:50,width:'100%',height:'75%'}
                    };

                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            });
        }
    </script>
    <div id="chart_div" style="width: 100%; height: 500px;"></div>
    <style>
        td.success{
            background-color: #8be865;
        }
        td.info{
           background-color: #5ec5f8;
        }
        td.warning{
           background-color: #f9e680;
        }
    </style>

    <table class="table table-bordered">
        <thead>
        <tr>
            <td rowspan="2">อุปกรณ์ <??></td>
            <?php
            $dateTitle=new DateTime($date->format('Y-m-d'));
//            $dateTitle->modify("-1 day");
            while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['ios'])-1)){
                echo "<td style='background-color:#cccccc;padding: 0px;text-align: center;'>" . $dateTitle->format("Y-m-d")."</td>";
                $dateTitle->modify("+1 day");
            }?>
            <td rowspan="2">รวม</td>
        </tr>
        <tr>
            <?php
            $dateTitle=new DateTime($date->format('Y-m-d'));;
//            $dateTitle2->modify("-1 day");
            while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['ios'])-1)){
                echo "<td style='background-color:#cccccc;padding: 0px;text-align: center;'>" . ImageUtility::getDateOfWeek($dateTitle) . "</td>";
                $dateTitle->modify("+1 day");
            }?>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border-bottom: solid 2px;" rowspan="3">ios</td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['ios'])-1)){ ?>
                    <?php if(!empty($list['ios'][$dateTitle->format('Y-m-d')]['status']) && $list['ios'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-down'){  ?>
                        <td style="background-color: #ff0000;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['ios'][$dateTitle->format('Y-m-d')]['status'])?$list['ios'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }elseif(!empty($list['ios'][$dateTitle->format('Y-m-d')]['status']) && $list['ios'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-up'){ ?>
                        <td style="background-color: #00ff00;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['ios'][$dateTitle->format('Y-m-d')]['status'])?$list['ios'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }else{ ?>
                        <td style="text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['ios'][$dateTitle->format('Y-m-d')]['status'])?$list['ios'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }
                    $dateTitle->modify("+1 day");
                } ?>
                <td></td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                $sumIos=0;
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['ios'])-1)){ ?>
                    <?php
                    $sumIos+=(!empty($list['ios'][$dateTitle->format('Y-m-d')]['value'])?$list['ios'][$dateTitle->format('Y-m-d')]['value']:0);
                    echo '<td style="border-bottom: solid 2px;" class="'.(!empty($list['ios'][$dateTitle->format('Y-m-d')]['class'])?$list['ios'][$dateTitle->format('Y-m-d')]['class']:'').'"> '.(!empty($list['ios'][$dateTitle->format('Y-m-d')]['value'])?$list['ios'][$dateTitle->format('Y-m-d')]['value']:'0').'</td>';
                    $dateTitle->modify("+1 day"); ?>
                <?php }?>
                <td style="border-bottom: solid 2px;font-weight: bolder;text-align: center;"><?php echo number_format($sumIos) ?></td>
            </tr>

            <tr>
                <td style="border-bottom: solid 2px;" rowspan="3">android</td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['android'])-1)){ ?>
                    <?php if(!empty($list['android'][$dateTitle->format('Y-m-d')]['status']) && $list['android'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-down'){  ?>
                        <td style="background-color: #ff0000;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['android'][$dateTitle->format('Y-m-d')]['status'])?$list['android'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }elseif(!empty($list['android'][$dateTitle->format('Y-m-d')]['status']) && $list['android'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-up'){ ?>
                        <td style="background-color: #00ff00;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['android'][$dateTitle->format('Y-m-d')]['status'])?$list['android'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }else{ ?>
                        <td style="text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['android'][$dateTitle->format('Y-m-d')]['status'])?$list['android'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }
                    $dateTitle->modify("+1 day");
                } ?>
                <td></td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                $sumIos=0;
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['android'])-1)){ ?>
                    <?php
                    $sumIos+=(!empty($list['android'][$dateTitle->format('Y-m-d')]['value'])?$list['android'][$dateTitle->format('Y-m-d')]['value']:0);
                    echo '<td style="border-bottom: solid 2px;" class="'.(!empty($list['android'][$dateTitle->format('Y-m-d')]['class'])?$list['android'][$dateTitle->format('Y-m-d')]['class']:'').'"> '.(!empty($list['android'][$dateTitle->format('Y-m-d')]['value'])?$list['android'][$dateTitle->format('Y-m-d')]['value']:'0').'</td>';
                    $dateTitle->modify("+1 day"); ?>
                <?php }?>
                <td style="border-bottom: solid 2px;font-weight: bolder;text-align: center;"><?php echo number_format($sumIos) ?></td>
            </tr>

            <tr>
                <td style="border-bottom: solid 2px;" rowspan="3">web</td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['web'])-1)){ ?>
                    <?php if(!empty($list['web'][$dateTitle->format('Y-m-d')]['status']) && $list['web'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-down'){  ?>
                        <td style="background-color: #ff0000;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['web'][$dateTitle->format('Y-m-d')]['status'])?$list['web'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }elseif(!empty($list['web'][$dateTitle->format('Y-m-d')]['status']) && $list['web'][$dateTitle->format('Y-m-d')]['status']=='icon-chevron-up'){ ?>
                        <td style="background-color: #00ff00;text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['web'][$dateTitle->format('Y-m-d')]['status'])?$list['web'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }else{ ?>
                        <td style="text-align: center;padding: 0px;"><?php echo '<i class="'.(!empty($list['web'][$dateTitle->format('Y-m-d')]['status'])?$list['web'][$dateTitle->format('Y-m-d')]['status']:'').'"></i>' ?></td>
                    <?php }
                    $dateTitle->modify("+1 day");
                } ?>
                <td></td>
            </tr>
            <tr>
                <?php
                $dateTitle=new DateTime($date->format('Y-m-d'));
                $sumIos=0;
                while(ImageUtility::DateDiff($date->format('Y-m-d'),$dateTitle->format('Y-m-d'))<=(count($list['web'])-1)){ ?>
                    <?php
                    $sumIos+=(!empty($list['web'][$dateTitle->format('Y-m-d')]['value'])?$list['web'][$dateTitle->format('Y-m-d')]['value']:0);
                    echo '<td style="border-bottom: solid 2px;" class="'.(!empty($list['web'][$dateTitle->format('Y-m-d')]['class'])?$list['web'][$dateTitle->format('Y-m-d')]['class']:'').'"> '.(!empty($list['web'][$dateTitle->format('Y-m-d')]['value'])?$list['web'][$dateTitle->format('Y-m-d')]['value']:'0').'</td>';
                    $dateTitle->modify("+1 day"); ?>
                <?php }?>
                <td style="border-bottom: solid 2px;font-weight: bolder;text-align: center;"><?php echo number_format($sumIos) ?></td>
            </tr>

        </tbody>
    </table>
</div>
</div>