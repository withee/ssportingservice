<div class="row">
    <div class="span3 columns" style="margin: 0px;">

<!--        --><?php //echo Yii::app()->controller->id." :: ".Yii::app()->controller->action->id; ?>

        <ul class="nav nav-pills nav-stacked nav-pills-stacked-example" style="font-size: 22px;">
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='site' and Yii::app()->controller->action->id=='index')?'active':''); ?>"><a href="/site/index">หน้าแรก</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='site' and Yii::app()->controller->action->id=='competition')?'active':''); ?>"><a href="/site/competition">ประเทศทั้งหมด</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='site' and Yii::app()->controller->action->id=='usedList')?'active':''); ?>"><a href="/site/usedList">รายงานการใช้งาน</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='site' and Yii::app()->controller->action->id=='pushList')?'active':''); ?>"><a href="/site/pushList">รายการแจ้งเตือน</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='site' and Yii::app()->controller->action->id=='highlight')?'active':''); ?>"><a href="/site/highlight">ไฮไลท์คู่บอล</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='news')?'active':''); ?>"><a href="/news/index">ข่าวฟุตบอล</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='banner')?'active':''); ?>"><a href="/banner/index">แบนเนอร์</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='odds')?'active':''); ?>"><a href="/odds/index">เพิ่มราคา</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='analyze')?'active':''); ?>"><a href="/analyze/index">วิเคราะห์ผลบอล</a></li>
            <li role="presentation" class="dropdown <?php echo ((Yii::app()->controller->id=='lottery')?'active':''); ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    จัดการลอตเตอร์รี่ <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/lottery/lotteryNews">จัดการข่าว</a></li>
                    <li><a href="/lottery/lotteryTded">จัดการทีเด็ด</a></li>
<!--                    <li><a href="#">Something else here</a></li>-->
<!--                    <li role="separator" class="divider"></li>-->
<!--                    <li><a href="#">Separated link</a></li>-->
                </ul>
            </li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='wb' and Yii::app()->controller->action->id=='admin')?'active':''); ?>"><a href="/wb/admin">เพิ่มบอร์ด</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='wb' and Yii::app()->controller->action->id=='index')?'active':''); ?>"><a href="/wb/index">จัดการเว็บบอร์ด</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='instruction')?'active':''); ?>"><a href="/instruction/index">เมนูวิธีใช้</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='instruction')?'report':''); ?>"><a href="/report/index">รายงาน</a></li>
            <li role="presentation" class="dropdown <?php echo ((Yii::app()->controller->id=='usagereport')?'active':''); ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    รายงานการใช้ <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
<!--                    <li><a href="/usagereport/repairscores">แก้ไขสกอร์</a></li>-->
                    <li><a href="/usagereport/matchremain">คู่บอลค้าง</a></li>
                    <li><a href="/usagereport/managematch">จัดการคู่บอล</a></li>
                    <li><a href="/usagereport/register">การสมัคร</a></li>
                    <li><a href="/usagereport/device">อุปกรณ์</a></li>
                    <li><a href="/usagereport/match">คู่บอล</a></li>
                    <li><a href="/usagereport/time">ช่วงเวลา</a></li>
                    <li><a href="/usagereport/diamond">จำนวนเพชร</a></li>
                    <li><a href="/usagereport/win">จำนวนคู่ที่ชนะ</a></li>
                    <li><a href="/usagereport/sgold">จำนวน sgold</a></li>
                    <li><a href="/usagereport/givediamond">แจกเพชร</a></li>
                    <li><a href="/usagereport/givesgold">แจก sgold</a></li>
                </ul>
            </li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='event')?'report':''); ?>"><a href="/event/index">กิจกรรม คู่บอล</a></li>
            <li role="presentation" class="<?php echo ((Yii::app()->controller->id=='server')?'active':''); ?>"><a href="/server/add">สถานะเซริฟเวอร์</a></li>
            <li role="presentation" class="dropdown <?php echo ((Yii::app()->controller->id=='Gallery')?'active':''); ?>">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Gallery <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/Gallery/GallerySexy">Sexy</a></li>
                </ul>
            </li>
            <li role="presentation" class="dropdown <?php echo ((Yii::app()->controller->id=='Video')?'active':''); ?>">
                <a href="/video" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
                    video
                </a>
<!--                <ul class="dropdown-menu">-->
<!--                    <li><a href="/Video/VideoHighlight">Highlight</a></li>-->
<!--                    <li><a href="/Video/VideoGeneral">General</a></li>-->
<!--                </ul>-->
            </li>
        </ul>

<!--        <h2><a href="/site/competition">ประเทศทั้งหมด</a></h2>-->
<!--        <h2><a href="/site/usedList">รายงานการใช้งาน</a></h2>-->
<!--        <h2><a href="/site/pushList">รายการแจ้งเตือน</a></h2>-->
<!--        <h2><a href="/site/highlight">ไฮไลท์คู่บอล</a></h2>-->
<!--        <h2><a href="/news/index">ข่าวฟุตบอล</a></h2>-->
<!--        <h2><a href="/banner/index">แบนเนอร์</a></h2>-->
<!--        <h2><a href="/odds/index">เพิ่มราคา</a></h2>-->
<!--        <h2><a href="/analyze/index">วิเคราะห์ผลบอล</a></h2>-->
<!--        <h2><a href="/lottery/index">จัดการลอตเตอร์รี่</a></h2>-->
<!--        <h2><a href="/wb/admin">เพิ่มบอร์ด</a></h2>-->
<!--        <h2><a href="/wb/index">จัดการเว็บบอร์ด</a></h2>-->
<!--        <h2><a href="/instruction/index">เมนูวิธีใช้</a></h2>-->
<!--        <h2><a href="/report/index">รายงาน</a></h2>-->
<!--        <h2><a href="/usagereport/index">รายงานการใช้</a></h2>-->
<!--        <h2><a href="/event/index">กิจกรรม คู่บอล</a></h2>-->
    </div>
    <div class="span9 columns" style="margin: 0px;">
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>

        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var req = $.ajax({
                    type: 'GET',
                    url: '/site/GetChartUsedList',
                    dataType: 'JSON'
                });
                req.done(function(res) {
                    if (res.status == 'success') {
                        obj = [];
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'day');
                        data.addColumn('number', 'ios');
                        data.addColumn('number', 'android');
                        data.addColumn('number', 'web');
                        var id = 0;

                        $.each(res.data.ios, function (i, elem) {
                            var logDate = [];
                            logDate.push(i);
                            if(typeof(res.data.ios[i])!=="undefined") {
                                logDate.push(parseInt(res.data.ios[i]));
                            }else{
                                logDate.push(0);
                            }

                            if(typeof(res.data.android[i])!=="undefined") {
                                logDate.push(parseInt(res.data.android[i]));
                            }else{
                                logDate.push(0);
                            }

                            if(typeof(res.data.web[i])!=="undefined") {
                                logDate.push(parseInt(res.data.web[i]));
                            }else{
                                logDate.push(0);
                            }

                            obj.push(logDate);
                        });
                        data.addRows(obj);
                        var options = {
                            title: 'กราฟจำนวนสมาชิกผู้เข้าระบบ',
                            hAxis: {title: 'วัน',  titleTextStyle: {color: '#333'}},
                            vAxis: {minValue: 0},
                            legend: {position: 'top', maxLines: 3},
                            chartArea:{left:50,top:50,width:'100%',height:'75%'}
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_used'));
                        chart.draw(data, options);
                    }
                });
            }
        </script>

        <div id="chart_used" style="width: 100%; height: 500px;"></div>

        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawChart);
            function drawChart() {
                var req = $.ajax({
                    type: 'GET',
                    url: '/site/getChartNotification',
                    dataType: 'JSON'
                });
                req.done(function(res) {
                    if (res.status == 'success') {
                        obj = [];
                        var data = new google.visualization.DataTable();
                        data.addColumn('string', 'day');
                        data.addColumn('number', 'ios');
                        data.addColumn('number', 'android');
                        data.addColumn('number', 'web');
                        var id = 0;

                        $.each(res.data.ios, function (i, elem) {
                            var logDate = [];
                            logDate.push(i);
                            if(typeof(res.data.ios[i])!=="undefined") {
                                logDate.push(parseInt(res.data.ios[i]));
                            }else{
                                logDate.push(0);
                            }

                            if(typeof(res.data.android[i])!=="undefined") {
                                logDate.push(parseInt(res.data.android[i]));
                            }else{
                                logDate.push(0);
                            }

                            if(typeof(res.data.web[i])!=="undefined") {
                                logDate.push(parseInt(res.data.web[i]));
                            }else{
                                logDate.push(0);
                            }

                            obj.push(logDate);
                        });
                        data.addRows(obj);
                        var options = {
                            title: 'กราฟจำนวนสมาชิกผู้เข้าใช้งาน (เฉพาะคนเล่น)',
                            hAxis: {title: 'วัน',  titleTextStyle: {color: '#333'}},
                            vAxis: {minValue: 0},
                            legend: {position: 'top', maxLines: 3},
                            chartArea:{left:50,top:50,width:'100%',height:'75%'}
                        };

                        var chart = new google.visualization.AreaChart(document.getElementById('chart_bet'));
                        chart.draw(data, options);
                    }
                });
            }
        </script>

        <div id="chart_bet" style="width: 100%; height: 500px;"></div>
<!--        <div></div>-->
    </div>
</div>
