<?php
$session = new CHttpSession;
$session->open();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="/bt/docs/assets/js/jquery.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-transition.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-alert.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-modal.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-dropdown.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-scrollspy.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-tab.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-tooltip.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-popover.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-button.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-collapse.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-carousel.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-typeahead.js"></script>
        <script src="/bt/docs/assets/js/bootstrap-datetimepicker.min.js"></script>
        <meta charset="utf-8">
        <title>Scores Home</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Le styles -->
        <link href="/bt/docs/assets/css/bootstrap.css" rel="stylesheet">
        <link href="/bt/docs/assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>
        <link href="/bt/docs/assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="/assets/js/html5shiv.js"></script>
        <![endif]-->

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/bt/docs/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/bt/docs/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/bt/docs/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/bt/docs/assets/ico/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="/bt/docs/assets/ico/favicon.png">
<!--uploadify-->
        <script src="/js/jquery.uploadify/jquery.uploadify.v2.1.4.js"></script>
        <script src="/js/jquery.uploadify/swfobject.js"></script>
        <link href="/js/jquery.uploadify/uploadify.css" rel="stylesheet">


    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="brand" href="#">Scores Home</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <li class="active"><a href="/">Home</a></li>

                            <?php
                            if ($session['admin']) {
                                ?>
                                <li><a href="/site/logout">Logout</a></li>
                                <?php
                            } else {
                                ?>
                                <li><a href="/site/login">Login</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>

        <div class="container">

            <?php echo $content; ?>

        </div> <!-- /container -->

        <!-- Le javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

    </body>
</html>
