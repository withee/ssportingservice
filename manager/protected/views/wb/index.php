<h2><a href="/">กลับหน้าแรก</a></h2>
<div class="control-group">
    <label class="control-label">Board</label>
    <div class="controls">
        <select class="type-select">
            <?php
            foreach ($feed as $f) {
                ?>
                <option value="<?php echo $f->fid; ?>" <?php echo ($f->fid == $type) ? "selected" : ""; ?>><?php echo $f->feed_name; ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>
<div class="control-group">
    <label class="control-label">Page</label>
    <div class="controls">
        <select class="page-select">
            <?php
            for($i=20;$i<=$allrecord;$i=$i+20) {
                ?>
                <option value="<?php echo ($i/20); ?>" <?php echo (($i/20) == $page) ? "selected" : ""; ?>><?php echo ($i/20); ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>
<table width="100%" border="1">

    <tr>
        <td>วันที่โพส</td>
        <td>เจ้าของ</td>
        <td>หัวข้อ</td>
        <td width="20%">รายละเอียด</td>
        <td>คนดู</td>
        <td>ปักหมุด</td>
        <td>ลบ</td>

    </tr>
    <?php
    foreach ($timelines as $key => $timeline) {
        // echo $timeline->id . ":" . $webboard[$timeline->id]->id . "<br>";
        ?>
        <tr>
            <td><?php echo $timeline['created_at']; ?></td>
            <?php if ($timeline->fb_uid == "admin") { ?>
                <td>admin</td>
            <?php } else { ?>
                <td><?php echo "{$users[$timeline['id']]['display_name']}"; ?></td>
            <?php } ?>
            <td><?php echo "{$webboard[$timeline['id']]['title']}" ?></td>
            <td width="20%"><?php echo "{$webboard[$timeline['id']]['desc']}" ?></td>
            <td><?php echo $timeline->view; ?></td>
            <td class="pin" tid="<?php echo $timeline->id; ?>"><input <?php echo ($timeline->pin == 'Y') ? 'checked' : ""; ?>  class="pinchk" tid="<?php echo $timeline->id; ?>"  type="checkbox" ></td>
            <td class="del" tid="<?php echo $timeline->id; ?>"><input <?php echo ($timeline->remove == 'Y') ? 'checked' : ""; ?>  class="rmchk" tid="<?php echo $timeline->id; ?>"  type="checkbox" ></td>

        </tr>

        <?php
    }
    ?>

</table>

<script type="text/javascript">

    $(document).ready(function() {
        $(".pinchk").change(function() {
            var chk = $(this).prop("checked");
            var pin = (chk) ? 'Y' : 'N';
            var tid = $(this).attr("tid");
            $.post('/wb/Pin', {id: tid, pin: pin}, function(response) {
                if (response) {
                    console.log(response);
                    alert(response.success);
                }
            });
        });
        $(".rmchk").change(function() {
            var chk = $(this).prop("checked");
            var remove = (chk) ? 'Y' : 'N';
            var tid = $(this).attr("tid");
            $.post('/wb/Remove', {id: tid, remove: remove}, function(response) {
                if (response) {
                    console.log(response);
                    alert(response.success);
                }
            });
        });
        $(".type-select").change(function() {
            var type = $(this).val();
           // alert(type);
           location =  "/wb/index?type="+type;
        });
         $(".page-select").change(function() {
            var type = $(".type-select").val();
            var page = $(this).val();
           // alert(type);
           location =  "/wb/index?type="+type+"&page="+page;
        });
    });
</script>