<h5><a href="/wb/add">เพิ่มข่าวใหม่</a></h5>
<table width="100%">

    <tr>
        <td>ลำดับ</td>

        <td>หัวข้อ</td>
        <td width="20%">รายละเอียด</td>
        <td>คนดู</td>
        <td>ปักหมุด</td>
        <td>ลบ</td>

    </tr>
    <?php
    foreach ($timelines as $key => $timeline) {
        // echo $timeline->id . ":" . $webboard[$timeline->id]->id . "<br>";
        ?>
        <tr>
            <td><?php echo $key + 1; ?></td>
            <td><?php echo "{$webboard[$timeline->id]->title}" ?></td>
            <td width="20%"><?php echo "{$webboard[$timeline->id]->desc}" ?></td>
            <td><?php echo $timeline->view; ?></td>
            <td class="pin" tid="<?php echo $timeline->id; ?>"><input <?php echo ($timeline->pin == 'Y') ? 'checked' : ""; ?>  class="pinchk" tid="<?php echo $timeline->id; ?>"  type="checkbox" ></td>
            <td class="del" tid="<?php echo $timeline->id; ?>"><input <?php echo ($timeline->remove == 'Y') ? 'checked' : ""; ?>  class="rmchk" tid="<?php echo $timeline->id; ?>"  type="checkbox" ></td>

        </tr>

        <?php
    }
    ?>

</table>

<script type="text/javascript">

    $(document).ready(function() {
        $(".pinchk").change(function() {
            var chk = $(this).prop("checked");
            var pin = (chk) ? 'Y' : 'N';
            var tid = $(this).attr("tid");

            $.post('/wb/Pin', {id: tid, pin: pin}, function(response) {
                if (response) {
                    console.log(response);
                    alert(response.success);
                }
            });

        });

        $(".rmchk").change(function() {
            var chk = $(this).prop("checked");
            var remove = (chk) ? 'Y' : 'N';
            var tid = $(this).attr("tid");

            $.post('/wb/Remove', {id: tid, remove: remove}, function(response) {
                if (response) {
                    console.log(response);
                    alert(response.success);
                }
            });

        });
    });
</script>