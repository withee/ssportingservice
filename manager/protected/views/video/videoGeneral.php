<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/7/2016
 * Time: 18:16
 */
?>


<div class="row">
    <div class="span12">
        <a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal" style="float: right">เพิ่ม Video</a>
    </div>
</div>

<div class="modal" id="myModal" style="width: 760px;display: <?php echo ((!empty($VideoObj->video_id))?'block':'none')?>;">
    <div class="modal-header">
        <a href="/Video/VideoHighlight" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
        <h3>จัดการ Video</h3>
    </div>
    <div class="modal-body">
        <form method="post" class="form-horizontal" style="width: 600px;">
            <input type="hidden" class="input-small" style="width: 200px;" name="video[video_id]" value="<?php echo $VideoObj->video_id ?>" placeholder="title">
            <div class="control-group">
                <label class="control-label" for="inputEmail">title</label>
                <div class="controls">
                    <input type="text" class="input-small" style="width: 400px;" name="video[title]" value="<?php echo $VideoObj->title ?>" placeholder="title">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">content</label>
                <div class="controls">
                    <input type="text" class="input-small" style="width: 400px;" name="video[content]" value="<?php echo $VideoObj->content ?>" placeholder="content">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="inputEmail">videosource</label>
                <div class="controls">
                    <select name="video[videosource]" style="width: 400px;">
                        <option <?php echo (($VideoObj->videosource=='facebook')?'selected':'') ?> value="facebook">Facebook</option>
                        <option <?php echo (($VideoObj->videosource=='twitter')?'selected':'') ?> value="twitter">Twitter</option>
                        <option <?php echo (($VideoObj->videosource=='youtube')?'selected':'') ?> value="youtube">Youtube</option>
                        <option <?php echo (($VideoObj->videosource=='dailymotion')?'selected':'') ?> value="dailymotion">Dailymotion</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">videotype</label>
                <div class="controls">
                    <select name="video[videotype]" style="width: 400px;">
                        <!--                <option --><?php //echo (($VideoObj->videotype=='highlight')?'selected':'') ?><!-- value="highlight">highlight</option>-->
                        <option <?php echo (($VideoObj->videotype=='general')?'selected':'') ?> value="general">general</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">video_tag</label>
                <div class="controls">
                    <input type="text" class="input-small" style="width: 400px;" name="video[video_tag]" value="<?php echo $VideoObj->video_tag ?>" placeholder="video_tag">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail">desc</label>
                <div class="controls">
                    <textarea  style="width: 400px;height: 100px;" name="video[desc]"><?php echo $VideoObj->desc ?></textarea>
                </div>
            </div>
            <div class="control-group">
                <?php if(!empty($VideoObj->video_id)){ ?>
                    <button style="float: right;" type="submit" class="btn btn-warning">แก้ไข</button>
                <?php }else{ ?>
                    <button style="float: right;" type="submit" class="btn btn-primary">เพิ่ม</button>
                <?php } ?>
            </div>
        </form>
    </div>
    <div class="modal-footer hide">
        <a href="#" class="btn">Close</a>
        <a href="#" class="btn btn-primary">Save changes</a>
    </div>
</div>

<div class="row">
    <div class="span12">
        <div id="page">
            <?php
            //var_dump($number['allnews']);
            $page = ceil((int) $countPage / (int) $limit);

            $left = 3;
            $right = $page - 2;



            echo "<a href='/Video/VideoGeneral/offset/1/limit/$limit'><<หน้าแรก   </a>";
            for ($i = 1; $i <= $page; $i++) {
                //$shownumber = $i + 1;
                if ($i <= $left) {
                    echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                }

                if ($i > $left && $i < $right) {
                    if ($i == $offset - 1) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>......$i|</a>";
                    }
                    if ($i == $offset) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                    }
                    if ($i == $offset + 1) {
                        echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|......</a>";
                    }
                }

                if ($i >= $right) {
                    echo "<a href='/Video/VideoGeneral/offset/".(($i-1)*$limit)."/limit/$limit'>$i|</a>";
                }
            }
            echo "<a href='/Video/VideoGeneral/offset/".(($page-1)*$limit)."/limit/$limit'>   หน้าสุดท้าย>></a>";
            ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="span12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>title</th>
                <th>content</th>
                <th>desc</th>
                <th>videosource</th>
                <th>videotype</th>
                <th>video_tag</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($dataVideoHighlight as $key=>$value){ ?>
            <tr>
                <td><?php echo $value->video_id; ?></td>
                <td><?php echo $value->title; ?></td>
                <td><?php echo $value->content; ?></td>
                <td><?php echo $value->desc; ?></td>
                <td><?php echo $value->videosource; ?></td>
                <td><?php echo $value->videotype; ?></td>
                <td><?php echo $value->video_tag; ?></td>
                <td>
                    <a href="/Video/VideoGeneral?id=<?php echo $value->video_id ?>" class="btn btn-primary">แก้ไข</a>
                    <a onclick="return confirm('ยืนยันการ ลบ')" href="/Video/DeleteVideoGeneral?id=<?php echo $value->video_id ?>" class="btn btn-danger">ลบ</a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
