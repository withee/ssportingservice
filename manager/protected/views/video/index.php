<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/7/2016
 * Time: 18:16
 */
?>
<div class="row">
    <div class="span12">
        <form method="get" class="form-inline" style="margin: 5px;">
            <input type="text" class="input-medium" placeholder="title" name="search" value="<?php echo $search ?>">
            <input type="text" class="input-medium" placeholder="tag" name="tag" value="<?php echo $tag ?>">
            <select name="videotype">
                <option value="">ไม่เลือก</option>
                <option <?php echo (($videotype=="highlight")?'selected':'')?> value="highlight">highlight</option>
                <option <?php echo (($videotype=="general")?'selected':'')?> value="general">general</option>
            </select>
            <select name="category">
                <option value="">ไม่เลือก</option>
                <option <?php echo (($category=="other")?'selected':'')?> value="other">other</option>
                <option <?php echo (($category=="europa")?'selected':'')?> value="europa">europa</option>
                <option <?php echo (($category=="ucl")?'selected':'')?> value="ucl">ucl</option>
                <option <?php echo (($category=="league 1")?'selected':'')?> value="league 1">league 1</option>
                <option <?php echo (($category=="serie a")?'selected':'')?> value="serie a">serie a</option>
                <option <?php echo (($category=="bundesliga")?'selected':'')?> value="bundesliga">bundesliga</option>
                <option <?php echo (($category=="la liga")?'selected':'')?> value="la liga">la liga</option>
                <option <?php echo (($category=="premier league")?'selected':'')?> value="premier league">premier league</option>
                <option <?php echo (($category=="thai")?'selected':'')?> value="thai">thai</option>
                <option <?php echo (($category=="international")?'selected':'')?> value="international">international</option>
            </select>
            <button type="submit" class="btn"><i class="icon-search"></i></button>
            <a href="/Video/add" role="button" class="btn btn-primary" data-toggle="modal" style="">เพิ่ม Video</a>
        </form>
    </div>
</div>

<div class="row">
    <div class="span12">
        <div class="pagination pagination-mini">
            <ul>
            <?php
            //var_dump($number['allnews']);
            $pageAll = ceil((int) $countPage / (int) $limit);
            $page = ceil((int) $offset / (int) $limit)+1;
//            var_dump("pageAll: ".$pageAll." :: page: ".$page);
            $left = 5;
            $right = $pageAll - 5;

            echo "<li class=''><a href='/Video?search=".$search."&tag=".$tag."&videotype=".$videotype."&category=".$category."&offset=".((1-1)*$limit)."&limit=$limit'><<หน้าแรก   </a></li>";
            for ($i = 1; $i <= $pageAll; $i++) {
                if($page==$i) {
                    echo "<li class='active'><a href='/Video?search=" . $search . "&tag=" . $tag . "&videotype=" . $videotype . "&category=" . $category . "&offset=" . (($i - 1) * $limit) . "&limit=$limit'>$i</a></li>";
                }else{
                    echo "<li class=''><a href='/Video?search=" . $search . "&tag=" . $tag . "&videotype=" . $videotype . "&category=" . $category . "&offset=" . (($i - 1) * $limit) . "&limit=$limit'>$i</a></li>";
                }
            }
            echo "<li class=''><a href='/Video?search=".$search."&tag=".$tag."&videotype=".$videotype."&category=".$category."&offset=".(($pageAll-1)*$limit)."&limit=".$limit."'>   หน้าสุดท้าย>></a></li>";
            ?>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="span12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>รูป</th>
                <th>ลีกส์</th>
                <th>หัวข้อ</th>
                <th>ลิ้ง</th>
                <th>รายละเอียด</th>
                <th>ประเถท</th>
                <th>หมวดหมู่</th>
                <th>เเท็ค</th>
                <th>จัดการ</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($dataVideo as $key=>$value){ ?>
            <tr>
                <td><?php echo $value->create_datetime; ?></td>
                <td><img id="video_thumbnail_image_<?php echo $value->video_id ?>" style="width: 100%;" src="<?php echo (!empty($value->thumbnail)? $value->thumbnail :'http://www.amfmph.com/wp-content/plugins/social-media-builder//img/no-image.png')?>"></td>
                <td><?php echo $value->category; ?></td>
                <td><?php echo $value->title; ?></td>
                <td><?php echo $value->content; ?></td>
                <td><?php echo $value->desc; ?></td>
                <td><?php echo $value->videosource; ?></td>
                <td><?php echo $value->videotype; ?></td>
                <td><?php echo $value->video_tag; ?></td>
                <td>
                    <form video_id="<?php echo $value->video_id; ?>" id="imageUploadWorkSheet-<?php echo $value->video_id; ?>" class="imageUploadWorkSheet" enctype="multipart/form-data" style="margin:0px;" >
                        <label for="upload-file-work-sheet-<?php echo $value->video_id; ?>"  class="btn btn-inverse select-file-work-sheet"><span class="oi oi-image"></span> อัพรูป</label>
                        <input video_id="<?php echo $value->video_id; ?>" type="file" name="Filedata" id="upload-file-work-sheet-<?php echo $value->video_id; ?>" class="upload-file-work-sheet" style="display: none;">
                    </form>
<!--                    <input type="file" class="upLoadFileThumbnailImage" video_id="--><?php //echo $value->video_id; ?><!--" id="upLoadFileThumbnailImage_--><?php //echo $value->video_id ?><!--">-->
                    <a href="/Video/add?id=<?php echo $value->video_id ?>" class="btn btn-primary">แก้ไข</a>
                    <a onclick="return confirm('ยืนยันการ ลบ')" href="/Video/Delete?id=<?php echo $value->video_id ?>" class="btn btn-danger">ลบ</a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>

    $(document).on('change', ".upload-file-work-sheet", function (e) {
        console.log($(this).attr('video_id'));
        $("#imageUploadWorkSheet-"+$(this).attr('video_id')).submit();
    });
    $(document).on('submit', ".imageUploadWorkSheet", function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: "/Video/UploadFile/" + $(this).attr('video_id'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                response=jQuery.parseJSON(response);
//                console.log(response['video_id']+" :: "+(response['thumbnail']+'?'+Math.random()));
                $('#video_thumbnail_image_'+response['video_id']).attr('src',(response['thumbnail']+'?'+Math.random()));
            },
            error: function (data) {
                console.log("error");
                console.log(data);
            }
        });
    });


//    $("input.upLoadFileThumbnailImage").each(function( index ) {
//        $("input#upLoadFileThumbnailImage_"+$(this).attr('video_id')).uploadify({
//            'fileSizeLimit' : '150KB',
//            'uploader': '/js/jquery.uploadify/uploadify.swf',
//            'script': "/Video/UploadFile/" + $(this).attr('video_id'),
//            'cancelImg': '/js/jquery.uploadify/cancel.png',
////            'multi': true,
//            'auto': true,
//            'fileExt': '*.png;*.jpg',
//            'fileDesc': 'Image Files (.jpg,.png)',
//            'onComplete': function (event, queueID, fileObj, response, data) {
//                response=jQuery.parseJSON(response);
//                console.log((response['thumbnail']+'?'+Math.random()));
//                $('#video_thumbnail_image_'+response['video_id']).attr('src',(response['thumbnail']+'?'+Math.random()));
//            },
//            'onError': function (event, queueID, fileObj, response, data) {
//            }
//        });
//    });
</script>
