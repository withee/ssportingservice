<?php
/**
 * Created by PhpStorm.
 * User: ii
 * Date: 12/14/2016
 * Time: 09:39
 */
?>
<style>
.errorMessage{
    color: #ff0000;
}
</style>
<div class="row">
    <div class="span12">
        <a href="/Video" role="button" class="btn btn-primary" data-toggle="modal" style="">กลับ</a>
    </div>
</div>
<div class="row">
    <div class="span12">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'formVideo',
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
            'enableAjaxValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
//        'validateOnChange' => true,
//        'validateOnType' => true,
            ),
            'htmlOptions' => array(
                'class' => 'form-horizontal',
            ),
        ));
        ?>
        <?php echo $form->hiddenField($VideoObj, 'video_id', array('class' => 'form-control')); ?>
<!--        <form method="post" class="form-horizontal" style="width: 600px;">-->
<!--            <input type="hidden" class="input-small" style="width: 200px;" name="video[video_id]" value="--><?php //echo $VideoObj->video_id ?><!--" placeholder="title">-->
<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">หัวข้อ</label>-->
<!--                <div class="controls">-->
<!--                    <input type="text" class="input-small" style="width: 400px;" name="video[title]" value="--><?php //echo $VideoObj->title ?><!--" placeholder="title">-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">ชื่อ</label>
            <div class="controls">
                <?php echo $form->textField($VideoObj, 'title', array('class' => 'input-small','style'=>'width: 400px;')); ?>
                <?php echo $form->error($VideoObj, 'title'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">ลิ้ง</label>-->
<!--                <div class="controls">-->
<!--                    <input type="text" class="input-small" style="width: 400px;" name="video[content]" value="--><?php //echo $VideoObj->content ?><!--" placeholder="content">-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">ตำแหน่งที่ตั้งวิดีโอ</label>
            <div class="controls">
                <?php echo $form->textField($VideoObj, 'pathFile', array('class' => 'input-small','style'=>'width: 400px;')); ?>
                <?php echo $form->error($VideoObj, 'pathFile'); ?>
            </div>
        </div>

        <div class="control-group">
            <label for="inputPassword3" class="control-label">ลิ้ง</label>
            <div class="controls">
                <?php echo $form->textField($VideoObj, 'content', array('class' => 'input-small','style'=>'width: 400px;')); ?>
                <?php echo $form->error($VideoObj, 'content'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">ประเถท</label>-->
<!--                <div class="controls">-->
<!--                    <select name="video[videosource]" style="width: 400px;">-->
<!--                        <option --><?php //echo (($VideoObj->videosource=='facebook')?'selected':'') ?><!-- value="facebook">Facebook</option>-->
<!--                        <option --><?php //echo (($VideoObj->videosource=='twitter')?'selected':'') ?><!-- value="twitter">Twitter</option>-->
<!--                        <option --><?php //echo (($VideoObj->videosource=='youtube')?'selected':'') ?><!-- value="youtube">Youtube</option>-->
<!--                        <option --><?php //echo (($VideoObj->videosource=='dailymotion')?'selected':'') ?><!-- value="dailymotion">Dailymotion</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">ประเถท</label>
            <div class="controls">
                <?php echo $form->dropDownList($VideoObj,'videosource', array('facebook'=>'facebook','twitter'=>'twitter','youtube'=>'youtube','dailymotion'=>'dailymotion','streamable'=>'streamable'), array('class' => 'form-control','style'=>'width: 400px;')) ?>
                <?php echo $form->error($VideoObj, 'videosource'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">หมวดหมู่</label>-->
<!--                <div class="controls">-->
<!--                    <select name="video[videotype]" style="width: 400px;">-->
<!--                        <option --><?php //echo (($VideoObj->videotype=='highlight')?'selected':'') ?><!-- value="highlight">highlight</option>-->
<!--                        <option --><?php //echo (($VideoObj->videotype=='general')?'selected':'') ?><!-- value="general">general</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">หมวดหมู่</label>
            <div class="controls">
                <?php echo $form->dropDownList($VideoObj,'videotype', array('highlight'=>'highlight','general'=>'general'), array('class' => 'form-control','style'=>'width: 400px;')) ?>
                <?php echo $form->error($VideoObj, 'videotype'); ?>
            </div>
        </div>

        <div class="control-group">
            <label for="inputPassword3" class="control-label">สไตล์</label>
            <div class="controls">
                <?php echo $form->dropDownList($VideoObj,'videostyle', array('normal'=>'normal','goal'=>'goal'), array('class' => 'form-control','style'=>'width: 400px;')) ?>
                <?php echo $form->error($VideoObj, 'videostyle'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label">ลีกส์</label>-->
<!--                <div class="controls">-->
<!--                    <select name="video[category]">-->
<!--                        <option value="other">other</option>-->
<!--                        <option value="thai">thai</option>-->
<!--                        <option value="premier league">premier league</option>-->
<!--                        <option value="la liga">la liga</option>-->
<!--                        <option value="bundesliga">bundesliga</option>-->
<!--                        <option value="serie a">serie a</option>-->
<!--                        <option value="league 1">league 1</option>-->
<!--                        <option value="ucl">ucl</option>-->
<!--                        <option value="europa">europa</option>-->
<!--                    </select>-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">ลีกส์</label>
            <div class="controls">
                <?php echo $form->dropDownList($VideoObj,'category', array('other'=>'other','europa'=>'europa','ucl'=>'ucl','league 1'=>'league 1','serie a'=>'serie a','bundesliga'=>'bundesliga','la liga'=>'la liga','premier league'=>'premier league','thai'=>'thai','international'=>'international'), array('class' => 'form-control')) ?>
                <?php echo $form->error($VideoObj, 'category'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">เเท็ค</label>-->
<!--                <div class="controls">-->
<!--                    <input type="text" class="input-small" style="width: 400px;" name="video[video_tag]" value="--><?php //echo $VideoObj->video_tag ?><!--" placeholder="video_tag">-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">เเท็ค</label>
            <div class="controls">
                <?php echo $form->textField($VideoObj, 'video_tag', array('class' => 'form-control','style'=>'width: 400px;')); ?>
                <?php echo $form->error($VideoObj, 'video_tag'); ?>
            </div>
        </div>

<!--            <div class="control-group">-->
<!--                <label class="control-label" for="inputEmail">รายละเอียด</label>-->
<!--                <div class="controls">-->
<!--                    <textarea  style="width: 400px;height: 100px;" name="video[desc]">--><?php //echo $VideoObj->desc ?><!--</textarea>-->
<!--                </div>-->
<!--            </div>-->

        <div class="control-group">
            <label for="inputPassword3" class="control-label">รายละเอียด</label>
            <div class="controls">
                <?php echo $form->textArea($VideoObj, 'desc', array('class' => 'form-control','style'=>'width: 400px;height: 100px;')); ?>
                <?php echo $form->error($VideoObj, 'desc'); ?>
            </div>
        </div>

            <div class="control-group">
                <div class="controls" style="width: 400px;">
                    <?php if(!empty($VideoObj->video_id)){ ?>
                        <?php echo CHtml::submitButton(Yii::t('main', 'แก้ไข'), array('id' => 'addform', 'class' => 'btn btn-warning','style'=>'float: right;')); ?>
    <!--                    <button style="float: right;" type="submit" class="btn btn-warning">แก้ไข</button>-->
                    <?php }else{ ?>
                        <?php echo CHtml::submitButton(Yii::t('main', 'เพิ่ม'), array('id' => 'addform', 'class' => 'btn btn-primary','style'=>'float: right;')); ?>
    <!--                    <button style="float: right;" type="submit" class="btn btn-primary">เพิ่ม</button>-->
                    <?php } ?>
                </div>
            </div>
        <?php $this->endWidget(); ?>
<!--        </form>-->
    </div>
</div>
