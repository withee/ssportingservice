<h2><a href="/event/index">กลับ</a></h2>
<input type="button" value="จบกิจกรรม" id="event-finished" eid="<?php echo $result['id']; ?>" /><input type="button" value="ยกเลิกกิจกรรม" id="event-cancel" eid="<?php echo $result['id']; ?>"/>
<?php
$winarr = array();
foreach ($result['winner'] as $winner) {
    $winarr[$winner['comment_id']] = $winner['prize'];
}
foreach ($result['games'] as $game) {
    ?>
    <table class="table" border="1">
        <thead>
            <tr>           
                <th><?php echo "<img src={$game->h32x32}>" . $game->homeNameTh; ?> </th>                  
                <th width="100px">รางวัล</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($result['bet'][$game['mid']]['home'] as $user) {
                ?>
                <tr <?php echo array_key_exists($user->id, $winarr) ? "bgcolor=#eeeeee" : ""; ?>>           
                    <td><?php echo "<img src=//graph.facebook.com/{$user['fb_uid']}/picture><font color=blue>" . htmlspecialchars($user['user']['display_name']) . "</font> : " . $user['message'] . "({$user->like})"; ?> </td>                  
                    <th>
                        <select id="<?php echo $user->id . $user->fb_uid ?>" uid="<?php echo $user->fb_uid ?>" cid="<?php echo $user->id ?>">
                            <?php
                            for ($i = 0; $i <= 500; $i = $i + 5) {
                                if (array_key_exists($user->id, $winarr)) {
                                    if ($i == $winarr[$user->id]) {
                                        ?>
                                        <option selected="true" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </th>

                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    <table class="table" border="1">
        <thead>
            <tr>                         
                <th><?php echo "<img src={$game->a32x32}>" . $game->awayNameTh; ?></th>            
                <th width="100px">รางวัล</th>

            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($result['bet'][$game['mid']]['away'] as $user) {
                //var_dump($user);exit();
                ?>
                <tr <?php echo array_key_exists($user->id, $winarr) ? "bgcolor=#eeeeee" : ""; ?>>           
                    <td><?php echo "<img src=//graph.facebook.com/{$user['fb_uid']}/picture><font color=blue>" . htmlspecialchars($user['user']['display_name']) . "</font> : " . $user['message'] . "({$user->like})"; ?> </td>                  
                    <th>
                        <select id="<?php echo $user->id . $user->fb_uid ?>" uid="<?php echo $user->fb_uid ?>" cid="<?php echo $user->id ?>">
                            <?php
                            for ($i = 0; $i <= 500; $i = $i + 5) {
                                if (array_key_exists($user->id, $winarr)) {
                                    if ($i == $winarr[$user->id]) {
                                        ?>
                                        <option selected="true" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </th>

                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <hr>
    <?php
}
?>
<script>
    $(document).ready(function () {
        var selected = [];

        $('select').change(function () {
            var uid = $(this).attr('uid');
            var prize = $(this).val();
            var eid = $('#event-finished').attr('eid');
            var cid = $(this).attr('cid');
            var agree = confirm("ยืนนยันให้รางวัล " + prize + " เพชร");
            if (agree) {
                $.post("/event/prize",
                        {
                            fb_uid: uid, prize: prize, eid: eid, cid: cid
                        },
                function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                });
            }
        });

        $('#event-cancel').click(function () {
            var eid = $(this).attr("eid");
            var agree = confirm("ยืนนยันการยกเลิกอีเว้น");
            if (agree) {
                $.post("/event/cancel",
                        {
                            id: eid
                        },
                function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                });
            }
        });

        $('#event-finished').click(function () {
            var eid = $(this).attr("eid");
            var agree = confirm("ยืนนยันการจบอีเว้น");
            if (agree) {
                $.post("/event/end",
                        {
                            id: eid
                        },
                function (res) {
                    var obj = jQuery.parseJSON(res);
                    alert(obj.desc);
                    console.log(res);
                });
            }
        });

    });
</script>