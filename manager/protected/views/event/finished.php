<h2><a href="/event/index">กลับหน้าแรก</a></h2>
<a href="/event/add">เพิ่มกิจกรรม</a>|<a href="/event/finished">กิจกรรมที่จบแล้ว</a>
<table class="table">
    <thead>
        <tr>
            <th>วันที่</th>
            <th>ชื่อ</th>  
            <th>คำอธิบาย</th>
            <th>สถานะ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($eventlist as $event) {
            ?>
            <tr>
                <td><?php echo $event->created_at; ?></td>
                <td><?php echo $event->title; ?></td>
                <td><?php echo $event->desc; ?></td>
                <td><?php echo $event->status; ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('button.removeInstruction').click(function () {
            var r = confirm('คุณต้องการลบวิธีใช้นี้ใช่หรือไม่?');
            if (r) {
                var newsId = $(this).attr('instructionId');
                $.post('/instruction/delete/instructionId/' + newsId, function (data) {
                    if (data) {
                        alert('ลบสำเร็จ');
                        window.location.reload();
                    }
                });
            }

        });
    });
</script>