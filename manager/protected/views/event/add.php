<h2><a href="/event/index">กลับ</a></h2>
<div>
    <input type="text" placeholder="ชื่อกิจกรรม" class="event-name"/>
    <input type="text" placeholder="รายละเอียด" class="event-desc"/>
    <input type="button" id="event-add" value="เพิ่ม"/>    
</div>
<div>
    ดูคู่บอลย้อนหลัง
    <select id="show-date">
        <?php
        for ($i = 0; $i < 7; $i++) {
            $d = date("Y-m-d");
            if ($i > 0) {
                $startdate = date("Y-m-d", strtotime($d . "-$i day"));
            } else {
                $startdate = $d;
            }
            ?>
            <option value="<?php echo $startdate ?>" <?php echo ($startdate == $day) ? "selected" : ""; ?>><?php echo $startdate ?></option>
            <?php
        }
        ?>
    </select>
</div>

<table class="table">
    <thead>
        <tr>
            <th>เลือก</th>
            <th>ลีก</th>  
            <th>คู่บอล</th>
            <th>เวลาเตะ</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($gamelist as $game) {
            ?>
            <tr>
                <td><input type="checkbox" value="<?php echo $game->mid ?>"></td>
                <td><?php echo $game->leagueNameTh; ?></td>
                <td><?php echo "<img src={$game->h32x32}>" . $game->homeNameTh . " - " . $game->awayNameTh . "<img src={$game->a32x32}>"; ?></td>
                <td><?php echo $game->show_date; ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        var selected = [];
        $('input:checkbox').change(function () {
            //alert($(this).val());
            mid = $(this).val();
            index = jQuery.inArray(mid, selected);
            if (index >= 0) {
                selected.pop(index);
            } else {
                selected.push(mid);
            }
            console.log(selected);
        });
        $('#event-add').click(function () {
            var name = $(".event-name").val();
            var desc = $(".event-desc").val();
            if (selected.length > 0) {
//                var res=$.ajax({
//                    type: "POST",
//                    url: "/event/seve",
//                    dataType: "JSON",
//                    data: {name: name, desc: desc, midlist: selected}
//                });
                $.post("/event/save",
                        {
                            name: name, desc: desc, midlist: selected
                        },
                function (res) {
                    alert(res);
                });

            } else {
                alert('ยังไม่มีคู่บอลในกิจกรรม');
            }
        });

        $('#show-date').change(function () {
            var day = $(this).val();
            window.location = "/event/add?day=" + day;
        });
    });
</script>