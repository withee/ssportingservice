
<h2><a href="/lottery/lotteryTded">กลับหน้าทีเด็ด</a></h2>
<h1>เพิ่มทีเด็ด</h1>

<form class="form-horizontal" method="post">

    <input type="hidden" name="img" id="img" />
    <div class="control-group">
        <label class="control-label">ชื่อทีเด็ด</label>
        <div class="controls">
            <input type="text" placeholder="title" id="title" name="title" class="span7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รายละเอียดย่อ</label>
        <div class="controls">
            <textarea id="desc" name="desc" class="span7" rows="10"></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รายละเอียด</label>
        <div class="controls" style="width:670px;">
            <textarea id="content" name="content" class="ckeditor"></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รูปอัพโหลด ขนาด 100x100</label>
        <div class="controls">
            <div id="showImg">

            </div>
            <input type="file" name="file_upload" id="file_upload" />
        </div>
    </div>
    <div class="control-group">
        <div class="controls">

            <input type="submit" class="btn btn-primary"   value="บันทึก"/>
        </div>
    </div>
</form>