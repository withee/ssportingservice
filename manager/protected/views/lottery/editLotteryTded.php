
<h2><a href="/lottery/lotteryTded">กลับหน้าทีเด็ด</a></h2>
<h1>แก้ไขทีเด็ด</h1>
<form class="form-horizontal"  method="post" action="/lottery/editLotterytded/id/<?php echo $obj['lottery_tded_id']?>">

    <input type="hidden" name="img" id="img"  value="<?php echo $obj['lottery_tded_img']?>"/>
    <div class="control-group">
        <label class="control-label">ชื่อข่าว</label>
        <div class="controls">
            <input type="text" placeholder="title" id="title" name="title" value="<?php echo $obj['lottery_tded_title']?>" class="span7">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รายละเอียดย่อ</label>
        <div class="controls">
            <textarea id="desc" name="desc" class="span7" rows="10"><?php echo $obj['lottery_tded_desc']?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รายละเอียด</label>
        <div class="controls" style="width:670px;">
            <textarea id="content" name="content" class="ckeditor"><?php echo $obj['lottery_tded_content']?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">รูปอัพโหลด ขนาด 100x100</label>
        <div class="controls">
            <div id="showImg">
                <img width="100" height="100" src="<?php echo $obj['lottery_tded_img']?>"/>
            </div>
            <input type="file" name="file_upload" id="file_upload" />
        </div>
    </div>
    <div class="control-group">
        <div class="controls">

            <input type="submit" class="btn btn-primary"   value="บันทึก"/>
        </div>
    </div>
</form>