<h2><a href="/lottery/addLotteryNews">เพิ่มข่าว</a></h2>
<h2><a href="/lottery">กลับหน้าจัดการลอตเตอร์รี่</a></h2>
<h2><a href="/">กลับหน้าแรก</a></h2>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>ชื่อข่าว</th>
        <th>เวลาที่สร้าง</th>
        <th>เวลาที่ปรับปรุง</th>
        <th>จำนวนวิว</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($list as $obj){
        ?>
        <tr>
            <td><img width="100" height="100" src="<?php echo $obj['lottery_news_img']?>"/></td>
            <td><?php echo $obj['lottery_news_title']?></td>
            <td><?php echo $obj['lottery_news_createdatetime']?></td>
            <td><?php echo $obj['lottery_news_modatetime']?></td>
            <td><?php echo $obj['lottery_news_views_count']?></td>
            <td><a class="btn btn-info" href="/lottery/editLotteryNews/id/<?php echo $obj['lottery_news_id']?>">แก้ไข</a>&nbsp;<a class="btn btn-danger" href="javascript:remove('<?php echo $obj['lottery_news_id']?>')">ลบ</a></td>

        </tr>
        <?php
    }
    ?>

    </tbody>
</table>


<script>
    $(document).ready(function(){

    });
    function remove(id){
        var r =confirm("คุณต้องการลบข่าวนี้ใช่ไหม");
        if(r){
            $.post('/lottery/removeNews/lottery_news_id/'+id,function(response){
                if(response){
                    window.location.reload();
                }else{
                    alert('ลบไม่สำเร็จ ลองใหม่อีกครั้ง');
                }
            });
        }
    }
</script>