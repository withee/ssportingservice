<?php
/**
 * Created by JetBrains PhpStorm.
 * User: indpendents
 * Date: 9/5/13 AD
 * Time: 2:51 PM
 * To change this template use File | Settings | File Templates.
 */

class UserFavoriteListCommand extends CConsoleCommand{
    public $langList =array("en"=>'En',"th"=>'Th',"big"=>'Big',"gb"=>'Gb',"kr"=>'kr',"vn"=>'Vn');
    public function run($args){
        $sql="select device_id,platform from favorite group by device_id,platform";
        $list=Yii::app()->db->createCommand($sql)->queryAll();
        foreach($list as $user){
            $device_id = $user['device_id'];
            $platform = $user['platform'];
            $league_sql="select favorite.*,if(lang_team.tid is not null ,lang_team.teamNameEn,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
            $favoriteList = Yii::app()->db->createCommand($league_sql)->queryAll();
            file_put_contents('../favoriteTeam/la/'.$platform.'_'.$device_id.'.json',json_encode($favoriteList));
            foreach($this->langList as $key=>$ln){
                $league_sql="select favorite.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
                //echo $league_sql;exit;
                $favoriteList = Yii::app()->db->createCommand($league_sql)->queryAll();
                file_put_contents('../favoriteTeam/'.$key.'/'.$platform.'_'.$device_id.'.json',json_encode($favoriteList));
            }
            $league_sql ="select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
            $favoriteList = Yii::app()->db->createCommand($league_sql)->queryAll();
            file_put_contents('../favoriteLeague/la/'.$platform.'_'.$device_id.'.json',json_encode($favoriteList));
            foreach($this->langList as $key=>$ln){
                $league_sql ="select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueName$ln,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
                $favoriteList = Yii::app()->db->createCommand($league_sql)->queryAll();
                file_put_contents('../favoriteLeague/'.$key.'/'.$platform.'_'.$device_id.'.json',json_encode($favoriteList));
            }

        }

    }
}