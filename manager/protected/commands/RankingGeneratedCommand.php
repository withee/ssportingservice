<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of rankingGenerated
 *
 * @author mrsyrop
 */
class RankingGeneratedCommand extends CConsoleCommand {

    public function run($args) {
        $db = Yii::app()->db;
        $results = array("scores" => array(), "rating" => array(), "friends" => array());
        $mappingscore = array("all" => array(), "14days" => array(), "2weeks" => array());
        $mappingrating = array("rate100" => array(), "rate200" => array());
        $day = date("Y-m-d");
        $twoweekago = date("Y-m-d", strtotime("-14day"));
        $week = date("W");
        $nowyear = date("Y");
        $lasttwoweek = date("Y-m-d");
        if ($week > 2) {
            if ($week % 2 == 0) {
                $week-=2;
            } else {
                $week--;
            }
        }
        $week = (int) $week;
        if ($week < 10) {
            $lasttwoweek = date("Y-m-d", strtotime("$nowyear-W0$week-1"));
        } else {
            $lasttwoweek = date("Y-m-d", strtotime("$nowyear-W$week-1"));
        }
//    echo "$nowyear-W$week-0";
//    echo $lasttwoweek;
//    exit;

        $scoresult = array("all" => array(), "last14day" => array(), "last2week" => array());
        $allsql = "SELECT *,overall_gp focus_gp,user_status as displayname FROM facebook_user ORDER BY overall_gp DESC";
        $scoresult["all"] = $db->createCommand($allsql)->queryAll();
        $tmpall = array();
        $tmpallind = 0;
        foreach ($scoresult["all"] as $key => $user) {
            $scoresult["all"][$key]['bestprize'] = $this->getBestPrize($user['fb_uid'], 3);
            if (is_numeric($user["fb_uid"])) {
                $mappingscore["all"][$user['fb_uid']] = $key;
                if ($tmpall % 10 != 0) {
                    
                }
            }
            //echo $key . "\n";
        }

        $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us 
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE DATE(us.update_at) BETWEEN '$twoweekago' AND DATE(NOW())
GROUP BY us.fb_uid
ORDER BY (CASE WHEN SUM(daily_change)<>0.0 then 0 ELSE 1 END),SUM(daily_change) DESC";
        $scoresult["last14day"] = $db->createCommand($last14daysql)->queryAll();
        foreach ($scoresult["last14day"] as $key => $user) {
            $scoresult["last14day"][$key]['bestprize'] = $this->getBestPrize($user['fb_uid'], 3);
            if (is_numeric($user["fb_uid"]))
                $mappingscore["14days"][$user['fb_uid']] = $key;
        }

        $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us 
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE DATE(us.update_at) ='$lasttwoweek'
ORDER BY us.overall_gp DESC";
        $scoresult["last2week"] = $db->createCommand($last2week)->queryAll();
        foreach ($scoresult["last2week"] as $key => $user) {
            $scoresult["last2week"][$key]['bestprize'] = $this->getBestPrize($user['fb_uid'], 3);
            if (is_numeric($user["fb_uid"]))
                $mappingscore["2weeks"][$user['fb_uid']] = $key;
        }

        $results["scores"] = $scoresult;
        file_put_contents("../ranking/mappingscore.json", json_encode($mappingscore));




        $rateresult = array("rate100" => array(), "rate200" => array());
        $rate100sql = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
          LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
          WHERE DATE(ur.update_at)='$day'
          ORDER BY ur.last_100 DESC,ur.w100 DESC,ur.wh100 DESC, ur.d100 DESC";
        $rateresult["rate100"] = $db->createCommand($rate100sql)->queryAll();
        foreach ($rateresult["rate100"]as $key => $user) {
            $rateresult["rate100"][$key]['bestprize'] = $this->getBestPrize($user['fb_uid'], 3);
            if (is_numeric($user["fb_uid"]))
                $mappingrating["rate100"][$user['fb_uid']] = $key;
        }

        $rate200sql = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
          LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
          WHERE DATE(ur.update_at)='$day'
          ORDER BY ur.last_200 DESC, ur.last_100 DESC";

        $rateresult["rate200"] = $db->createCommand($rate200sql)->queryAll();
        foreach ($rateresult["rate200"]as $key => $user) {
            $rateresult["rate200"][$key]['bestprize'] = $this->getBestPrize($user['fb_uid'], 3);
            if (is_numeric($user["fb_uid"]))
                $mappingrating["rate200"][$user['fb_uid']] = $key;
        }
        file_put_contents("../ranking/mappingrating.json", json_encode($mappingrating));

        $results["rating"] = $rateresult;

        file_put_contents("../ranking/ranking.json", json_encode($results));

//        if ($rank == "all" || $rank == "friends") {
//            $friresult = array("last14day" => array(), "last2week" => array());
//            $knowuser = "";
//            if (is_numeric($fb_uid)) {
//                $friend_sql = "SELECT DISTINCT ff.friend_facebook_id as id
//          FROM  facebook_friends ff
//          WHERE ff.facebook_id = '$fb_uid'";
//                $stmtfriend = $db->query($friend_sql);
//                $userlist = $stmtfriend->fetchAll(5);
//                $knowuser = "('$fb_uid'";
//                foreach ($userlist as $user) {
//                    $knowuser.=",'{$user->id}'";
//                }
//                $knowuser.=")";
//
//
//                $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us
//          LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
//          WHERE DATE(us.update_at) BETWEEN '$twoweekago' AND DATE(NOW())
//          AND us.fb_uid IN $knowuser
//          GROUP BY us.fb_uid
//          ORDER BY (CASE WHEN SUM(daily_change)<>0.0 then 0 ELSE 1 END),SUM(daily_change) DESC LIMIT $start,$range";
//                $stmt = $db->query($last14daysql);
//                $friresult["last14day"] = $stmt->fetchAll(5);
//                foreach ($friresult["last14day"] as $key => $user) {
//                    $friresult["last14day"][$key]->bestprize = getBestPrize($user->fb_uid, 3);
//                }
//
//                $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us
//          LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
//          WHERE DATE(us.update_at) ='$lasttwoweek'
//          AND us.fb_uid IN $knowuser
//          ORDER BY us.overall_gp DESC LIMIT $start,$range";
//                $stmt = $db->query($last2week);
//                $friresult["last2week"] = $stmt->fetchAll(5);
//                foreach ($friresult["last2week"] as $key => $user) {
//                    $friresult["last2week"][$key]->bestprize = getBestPrize($user->fb_uid, 3);
//                }
//            }
//            $results["friends"] = $friresult;
//        }
//
//        return $results;
    }

    function getBestPrize($fb_uid, $top) {
        $db = Yii::app()->db;
        $sql = "SELECT ua.fb_uid, t.type, t.sequence, t.picture, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid = '$fb_uid'
GROUP BY ua.trophy_id
ORDER BY sequence
LIMIT $top 
";
        $results = $db->createCommand($sql)->queryAll();
        return $results;
    }

}
