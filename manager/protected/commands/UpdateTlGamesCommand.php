<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdateTlGamesCommand
 *
 * @author mrsyrop
 */
require "../../Carbon/src/Carbon/Carbon.php";

use Carbon\Carbon;

class UpdateTlGamesCommand extends CConsoleCommand
{

    public function run()
    {
        $functionstart = Carbon::now();
        $db = Yii::app()->db;
//        $resetsql = "UPDATE timelines_game SET `hdp`='',`hdp_home`='',`hdp_away`='' WHERE sid=1";
//        $db->createCommand($resetsql)->execute();

        $day3 = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "-2 day"));
        echo $day3 . "\n";
        $tlsql = "SELECT tg.id,tg.mid,tg.leagueName,lm.lid,lm._lid,lm.hid,lm.gid,lm.sid,lm.c0,lm.c1,lm.c2,lm.cx,lm.date,lm.s2,lm.s1,lm.hn,lm.gn FROM timelines_game  tg
LEFT JOIN live_match lm ON lm.mid=tg.mid
WHERE lm.date>'$day3'
ORDER BY lm.date ASC";
        $tlgames = $db->createCommand($tlsql)->queryAll();
        $file = '../odds/today.json';
        var_dump(file_exists($file));
        $odds = '';
        if (file_exists($file)) {
            $odds = json_decode(file_get_contents($file), true);
        }
        $resultset = 0;
        foreach ($tlgames as $tlgame) {
            $resultset++;
            //$lmsql = "SELECT _lid,hid,gid,sid,c0,c1,c2,cx,date,s2,s1,hn,gn FROM live_match WHERE mid={$tlgame["mid"]} AND new='Y' ORDER BY sid DESC";
            //$lm = $db->createCommand($lmsql)->queryAll();
            $hdp = "";
            $hdphome = "";
            $hdpaway = "";
            $type = "";
            //if (!empty($lm)) {
            $score = $tlgame["s2"];
            if (!empty($tlgame["s1"])) {
                $score = $tlgame["s1"];
            }
            //$updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',c0='{$lm[0]["c0"]}',c1='{$lm[0]["c1"]}',c2='{$lm[0]["c2"]}',cx='{$lm[0]["cx"]}',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`show_date`='{$lm[0]['date']}' WHERE id='{$tlgame["id"]}'";
            if ($odds) {
                $key = "{$tlgame['_lid']}_{$tlgame['hid']}_{$tlgame['gid']}";
                //echo $key;
                if (array_key_exists($key, $odds["data"])) {
                    //echo $key . "\n";
                    if (array_key_exists('SBOBET', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['SBOBET']['hdp'];
                        $hdphome = $odds['data']["$key"]['SBOBET']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['SBOBET']['hdp_away'];
                        $type = 'SBOBET';
                        echo $type . $odds['data']["$key"]['SBOBET']['hdp'] . "\n";
                    } else if (array_key_exists('Bet365', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['Bet365']['hdp'];
                        $hdphome = $odds['data']["$key"]['Bet365']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['Bet365']['hdp_away'];
                        $type = 'Bet365';
                        echo $type . "\n";
                    } else if (array_key_exists('Ladbrokes', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['Ladbrokes']['hdp'];
                        $hdphome = $odds['data']["$key"]['Ladbrokes']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['Ladbrokes']['hdp_away'];
                        $type = 'Ladbrokes';
                        echo $type . "\n";
                    }
                    //$updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`type`='$type',`show_date`='{$lm[0]['date']}' WHERE id='{$tlgame["id"]}'";
                }
            }

            if ($odds) {
                $key = "{$tlgame['lid']}_{$tlgame['hid']}_{$tlgame['gid']}";
                //echo $key;
                if (array_key_exists($key, $odds["data"])) {
                    echo $key . "\n";
                    if (array_key_exists('SBOBET', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['SBOBET']['hdp'];
                        $hdphome = $odds['data']["$key"]['SBOBET']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['SBOBET']['hdp_away'];
                        $type = 'SBOBET';
                        echo $type . $odds['data']["$key"]['SBOBET']['hdp'] . "\n";
                    } else if (array_key_exists('Bet365', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['Bet365']['hdp'];
                        $hdphome = $odds['data']["$key"]['Bet365']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['Bet365']['hdp_away'];
                        $type = 'Bet365';
                        echo $type . "\n";
                    } else if (array_key_exists('Ladbrokes', $odds['data']["$key"])) {
                        $hdp = $odds['data']["$key"]['Ladbrokes']['hdp'];
                        $hdphome = $odds['data']["$key"]['Ladbrokes']['hdp_home'];
                        $hdpaway = $odds['data']["$key"]['Ladbrokes']['hdp_away'];
                        $type = 'Ladbrokes';
                        echo $type . "\n";
                    }
                    //$updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`type`='$type',`show_date`='{$lm[0]['date']}' WHERE id='{$tlgame["id"]}'";
                }
            }
            //echo $updatesql . "\n";
            $updatesql = "UPDATE timelines_game SET sid='{$tlgame["sid"]}' , score='$score',c0='{$tlgame["c0"]}',c1='{$tlgame["c1"]}',c2='{$tlgame["c2"]}',cx='{$tlgame["cx"]}',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`show_date`='{$tlgame['date']}' WHERE mid='{$tlgame["mid"]}'";
            if (!empty($hdp)) {
                echo $updatesql . "\n";
            }
            $updated = $db->createCommand($updatesql)->execute();
            //var_dump($updated);
            //echo $lm[0]['hn'] . "-" . $lm[0]['gn'] . "sid=" . $lm[0]['sid'] . "\n";
            //}
        }
        $functionstop = Carbon::now();

        echo "Found $resultset records\n";
        echo "===========\n";
        echo $functionstop->diffInseconds($functionstart);
        echo "s\n";
        echo "===========\n";
    }

}
