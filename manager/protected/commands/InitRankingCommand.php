<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InitRankingCommand
 *
 * @author withee
 */
class InitRankingCommand extends CConsoleCommand {

    public function run($args) {
        $spirit = isset($args[0]) ? $args[0] : 0;
        $db = Yii::app()->db;
        $transaction = $db->beginTransaction();
        $sql_user_list = "select *from facebook_user";
        $user_list = $db->createCommand($sql_user_list)->queryAll();
        try {
            foreach ($user_list as $user) {
                if (!empty($user["fb_uid"])) {
                    $rankTb_sql = "SELECT * FROM ranking_map WHERE fb_uid='{$user['fb_uid']}'";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking_map` (`fb_uid`, `gp_stamp`,`stamp_at`, `update_at`) VALUES ('{$user['fb_uid']}', {$user['gp']},NOW(), NOW());")->execute();
                        //echo "New " . $user['fb_uid'] . "\n";
                    } else {
                        $db->createCommand("UPDATE `ranking_map` SET `gp_stamp`={$user['gp']}, `gp_change`=0,`update_at`=NOW(),`stamp_at`=NOW() WHERE  fb_uid='{$user['fb_uid']}';")->execute();
                        //echo "Reset " . $user['fb_uid'] . "\n";
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
        }
    }

}

?>
