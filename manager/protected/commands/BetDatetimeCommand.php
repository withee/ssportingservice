<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BetDatetimeCommand
 *
 * @author mrsyrop
 */
class BetDatetimeCommand extends CConsoleCommand {

    public function run() {       
        $round = 0;
        $limit = 1000;
        $listmember = $limit;
        while ($listmember == $limit) {
            echo "round $round\n";
            $criteria = new CDbCriteria();
            $criteria->order = "betId";
            $criteria->limit = $limit;
            $criteria->offset = $round;
            $betlist = Bet::model()->findAll($criteria);
            foreach ($betlist as $bet) {
                $betdate = date("Y-m-d H:i:s", $bet->betDatetime);
                $bet->created_at = '$betdate';
                $bet->save();
            }
            $round++;
            $listmember = count($betlist);
        }
    }

}
