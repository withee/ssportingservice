<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RankingCommand
 *
 * @author withee
 */
class RankingCommand extends CConsoleCommand {

    public function run($args) {
        $spirit = isset($args[0]) ? $args[0] : 0;
        $db = Yii::app()->db;
        $transaction = $db->beginTransaction();
        $sql_user_list = "select *from facebook_user";
        $user_list = $db->createCommand($sql_user_list)->queryAll();
        try {
            foreach ($user_list as $user) {
                if (!empty($user["fb_uid"])) {
                    $rankTb_sql = "SELECT * FROM ranking_map WHERE fb_uid='{$user['fb_uid']}'";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking_map` (`fb_uid`, `gp_stamp`,`stamp_at`, `update_at`) VALUES ('{$user['fb_uid']}', {$user['gp']},NOW(), NOW());")->execute();
                    } else {
                        $change = (float) $user["gp"] - (float) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `ranking_map` SET `gp_change`=$change,`update_at`=NOW() WHERE  fb_uid='{$user['fb_uid']}';")->execute();
                        echo "update " . $user["fb_uid"] . "\n";
                    }
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
        }
    }

}

?>
