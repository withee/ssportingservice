<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Panupong
 * Date: 9/10/2556
 * Time: 11:01 น.
 * To change this template use File | Settings | File Templates.
 */

class ClearPointCommand extends CConsoleCommand{

    public function run($args){
        $sql="
        update facebook_user set gp=5000,pts=0,w=0,l=0,d=0;
        TRUNCATE TABLE bet;
        ";
        $db = Yii::app()->db;
        $transaction=$db->beginTransaction();
        try{
            $db->createCommand($sql)->execute();
            $transaction->commit();
            echo 1;
        }catch(Exception $e){
            $transaction->rollback();
            echo 0;
        }
    }
}