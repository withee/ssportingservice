<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AutoRoundPriceCommand
 *
 * @author Tingly
 */
class PushListMessageCommand extends CConsoleCommand {

    public function run($args) {

        $query = "select *from push_list_message
            where sent='N' and senddatetime<=current_timestamp 
            order by senddatetime DESC";
        $push_list_message = Yii::app()->db->createCommand($query)->queryRow();
        if ($push_list_message) {
            $all_device_command = "select apple_id,platform 
                from used_list 
                where app='ballscore'
                and date(used_at) <= '2013-06-10'
                group by apple_id
                order by id
                ";
            $all_device = Yii::app()->db->createCommand($all_device_command)->queryAll();
            //echo count($all_device);
            $apple_id = array();
            $array = array(
                'message' => $push_list_message['message'],
                'deviceToken' => array(),
            );
            $count = 0;
            foreach ($all_device as $device) {
                if ($device['platform'] == 'ios') {
                    array_push($apple_id, $device['apple_id']);
                    //pushIos($array);
                    if (count($apple_id) == 100) {
                        $count++;
                        echo $count;
                        $array['deviceToken'] = $apple_id;
                        $this->pushIos($array);
                        $apple_id = array();
                    }
                }
            }
            $array['deviceToken'] = $apple_id;
            $this->pushIos($array);


            $sql = "update push_list_message set sent='Y' where messageid=" . $push_list_message['messageid'];
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    public function pushIos($array) {
        // Put your device token here (without spaces):
        //$deviceToken = '4a5bb24082a28d825b8c3e415dcf1018bdf6662bfe4ba5caeab7e6e079ef26e5';
        //$deviceToken = $array['deviceToken'];
// Put your private key's passphrase here:
        $passphrase = 'subancomsci12';

// Put your alert message here:
        $message = $array['message'];

////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'ckPro.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
        $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default',
            'badge' => 1,
        );

// Encode the payload as JSON
// Build the binary notification
        //echo count($array['apple_id']);
        //echo count($array['deviceToken']);
        foreach ($array['deviceToken'] as $deviceToken) {

            $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default',
                'badge' => 1,
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            if (!$result) {
                echo 'Message not delivered' . PHP_EOL;
            } else {
                //echo 'Message successfully delivered' . PHP_EOL;
            }
        }
// Close the connection to the server
        fclose($fp);
    }

    public function pushAndroid($reg_id, $array) {
        define("GOOGLE_API_KEY", "AIzaSyDmNlVG1YUuBIkdUouJWT8ipWwjVLmcuok");
        define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");
        $fields = array(
            'registration_ids' => $reg_id,
            'data' => $array,
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Problem occurred: ' . curl_error($ch));
        }

        curl_close($ch);
        echo $result;
    }

}

?>
