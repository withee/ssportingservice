<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdateTeamLogosCommand
 *
 * @author withee
 */
class UpdateTeamLogosCommand extends CConsoleCommand {

    public function run($args) {
        $db = Yii::app()->db;
        $path = $args[0];
        $teamlogos = $db->createCommand("SELECT * FROM team_logos")->queryAll();
        $root = "../teams_clean/";
        foreach ($teamlogos as $logo) {
//            $s32 = explode("/", $logo["32x32"]);
//            $s64 = explode("/", $logo["64x64"]);
//            $s256 = explode("/", $logo["256x256"]);
            //echo $root . $logo["tid"] . "_32x32.png";

            $s32 = "";
            $s64 = "";
            $s256 = "";
            if (is_file($root . $logo["tid"] . "_32x32.png")) {
                $s32 = $path . $logo["tid"] . "_32x32.png";
            } else {
                $s32 = $path . "team_default_32x32.png";
            }

            if (is_file($root . $logo["tid"] . "_64x64.png")) {
                $s64 = $path . $logo["tid"] . "_64x64.png";
            } else {
                $s64 = $path . "team_default_64x64.png";
            }

            if (is_file($root . $logo["tid"] . "_256x256.png")) {
                $s256 = $path . $logo["tid"] . "_256x256.png";
            } else {
                $s256 = $path . "team_default_256x256.png";
            }




            $db->createCommand("UPDATE team_logos SET `32x32`='$s32',`64x64`='$s64',`256x256`='$s256',`update_at`=NOW() WHERE id={$logo["id"]}")->execute();
            //echo "update:".$logo["id"];
        }
    }

}

?>
