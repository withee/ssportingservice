<?php
require '../TotalStatement.php';
#require '../DBConfig.php';


/**
 * Created by JetBrains PhpStorm.
 * User: admin
 * Date: 1/8/2556
 * Time: 16:16 น.
 * To change this template use File | Settings | File Templates.
 */
class ReClearBillCommand extends CConsoleCommand
{

    private $pointMul = 1;
    private $blist = array();

    public function run($args)
    {
        //exit;
        $t = new TotalStatement();
        $mulTable = array(
            'win' => 1,
            'lose' => -1,
            'draw' => 0,
            'win_half' => 0.5,
            'lose_half' => -0.5
        );
        $all_match_success = "select *from live_match where sid>=5 and showDate>=current_date - interval 10 day and FROM_UNIXTIME(c0) + interval 180 minute < current_timestamp";
        $all_match_success = Yii::app()->db->createCommand($all_match_success)->queryAll();
        $all_match = array();
        $status_array = array();
        $bet_list = array();
        foreach ($all_match_success as $match) {
            $status_array[$match['_lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['sid'];
            $status_array[$match['lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['sid'];
            $result_win = null;
            $hasResult = false;
            if ($match['sid'] == 5 || $match['sid'] == 8 || $match['sid'] == 9 || $match['sid'] == 15) {

                $scoreArray = array();
                if ($match['sid'] == 5 || $match['sid'] == 9) {
                    $scoreArray = explode('-', $match['s1']);
                } else if ($match['sid'] == 8) {
                    $s2Array = explode(",", $match['s2']);
                    if (count($s2Array) < 2) {
                        $all_match[$match['_lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['s1'];
                        $all_match[$match['lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['s1'];
                        continue;
                    } else {
                        $scoreArray = explode('-', $s2Array[1]);
                    }
                } else if ($match['sid'] == 15) {
                    $scoreArray = explode('-', $match['s1']);
                } else {
                    echo '1score:' . $match['s1'] . "\n";
                    echo '2score:' . $match['s2'] . "\n";
                    echo "sid:" . $match['sid'] . "\n";
                    //exit;
                }
                if (count($scoreArray) < 2) {
                    echo "mid:" . $match['mid'] . "\n";
                    echo '1score:' . $match['s1'] . "\n";
                    echo '2score:' . $match['s2'] . "\n";
                    echo "sid:" . $match['sid'] . "\n";
                    //exit;
                }

                $result_diff = $scoreArray[0] - $scoreArray[1];
                if ($scoreArray[0] == $scoreArray[1]) {
                    $result_win = 'draw';
                } else if ($scoreArray[0] > $scoreArray[1]) {
                    $result_win = 'home';
                } else {
                    $result_win = 'away';
                }
                $match['result_win'] = $result_win;
                $match['result_diff'] = $result_diff;
                $match['scoreHome'] = $scoreArray[0];
                $match['scoreAway'] = $scoreArray[1];
                $hasResult = true;
                //echo 1;exit;
            } else if ($match['sid'] > 5) {
                $all_match[$match['_lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['s1'];
                $all_match[$match['lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match['s1'];
                //echo "Match :".$match['_lid'].'_'.$match['hid'].'_'.$match['gid']."\n";
                //echo  $match['sid']."\n";
            }
            if ($hasResult) {
                $all_match[$match['_lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match;
                $all_match[$match['lid'] . '_' . $match['hid'] . '_' . $match['gid']] = $match;
                //echo 1;exit;
            }
        }
        echo "finish game:" . count($all_match) . "\n";
        $all_bet_sql = "select *from bet where rescore='Y'";
        $all_bet = Yii::app()->db->createCommand($all_bet_sql)->queryAll();
        echo "wait game:" . count($all_bet) . "\n";
        $all_user = array();

        foreach ($all_bet as $bet) {
            //echo $bet['leagueId'].'_'.$bet['hid'].'_'.$bet['gid']."\n";
            if (isset($all_match[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']])) {
                array_push($all_user, $bet['fb_uid']);
                //$match=null;
                if (!is_array($all_match[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']])) {
                    $score = $all_match[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']];
                    $sid = $status_array[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']];
                    $update_bet_sql = "update bet set  status_id=$sid,result='draw',betResult='draw',FT_score='$score',mul=0,betAmount=0 where betId=" . $bet['betId'];
                    $fb_uid = $bet['fb_uid'];
                    $result = $bet['result'];
                    if ($result == 'win' || $result == 'lose' || $result == 'draw') {
                        $result_arr = array('win' => 'w', 'draw' => 'd', 'lose' => 'l');
                        $_betAmount = -1 * $bet['betAmount'];
                        $_sql_add = "$result_arr[$result] = $result_arr[$result] -1";
                        $sql_update_user = "update facebook_user set gp=gp+( $_betAmount ), $_sql_add where fb_uid='$fb_uid'";
                        //echo $sql_update_user;
                        try {
                            Yii::app()->db->createCommand($sql_update_user)->execute();
                            Yii::app()->db->createCommand($update_bet_sql)->execute();
                        } catch (Exception $e) {
                            echo $e->getMessage() . "\n";
                            echo $sql_update_user . "\n";
                            echo $update_bet_sql . "\n";

                        }
                        $bet_id = $bet['betId'];
                        $sql_statement = "select *from total_statement where bet_id=$bet_id order by id DESC";
                        $statement = Yii::app()->db->createCommand($sql_statement)->queryRow();
                        $bet_info = array(
                            'fb_uid' => $fb_uid,
                            'result' => 'draw',
                            're_scoin_income' => $statement['scoin_outcome'],
                            're_sgold_income' => $statement['sgold_outcome'],
                            're_scoin_outcome' => $statement['scoin_income'],
                            're_sgold_outcome' => $statement['sgold_income'],
                            're_diamond_income' => $statement['diamond_outcome'],
                            're_diamond_outcome' => $statement['diamond_income'],
                            'scoin_income' => 0,
                            'sgold_income' => 0,
                            'sgold_outcome' => 0,
                            'scoin_outcome' => 0,
                            'mul' => 0,
                            'bet_id' => $bet['betId'],
                        );
                        array_push($bet_list, $bet_info);
                    } else {

                    }


                } else {
                    $match = $all_match[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']];
                    if ($bet['oddsType'] == '1x2') {
                        $mul = $bet['choose'] == $match['result_win'] ? $bet['betValue'] : 0;
                        $betResult = $mul == 0 ? 'lose' : 'win';
                    } else if ($bet['oddsType'] == 'hdp') {
                        $favorite = 0;
                        if ($bet['betHdp'] > 0) {
                            $favorite = 2;
                        } else if ($bet['betHdp'] < 0) {
                            $favorite = 1;
                        }
                        $betResult = $this->hdp($favorite, $bet['betHdp'], $bet['choose'], $match['scoreHome'], $match['scoreAway']);
                    } else {
                        continue;
                    }
                    //echo $betResult;
                    if ($betResult == 'win' || $betResult == 'win_half') {
                        $mul = $mulTable[$betResult] * ($bet['betValue'] - 1);
                    } else {
                        $mul = $mulTable[$betResult];
                    }

                    $betAmount = $mul * ($bet['bet_sgold'] > 0 ? $bet['bet_sgold'] : $bet['amount']) * $this->pointMul;
                    $transaction = Yii::app()->getDb()->beginTransaction();
                    try {

                        $bResult = $betResult;
                        $sql_add = '';
                        if ($betResult == 'win_half') {
                            $bResult = 'win';
                        } else if ($betResult == 'lose_half') {
                            $bResult = 'lose';
                        }
                        if ($bResult == 'win') {
                            $sql_add = "w=w+1";
                            if ($bet['result'] == 'win') {
                            } else if ($bet['result'] == 'lose') {
                                $sql_add .= ",l=l-1 ";
                            } else if ($bet['result'] == 'draw') {
                                $sql_add .= ",d=d-1 ";
                            }
                        } else if ($bResult == 'lose') {
                            $sql_add = "l=l+1";
                            if ($bet['result'] == 'win') {
                                $sql_add .= ",w=w-1 ";
                            } else if ($bet['result'] == 'lose') {
                            } else if ($bet['result'] == 'draw') {
                                $sql_add .= ",d=d-1 ";
                            }
                        } else {
                            $sql_add = "d=d+1";
                            if ($bet['result'] == 'win') {
                                $sql_add .= ",w=w-1 ";
                            } else if ($bet['result'] == 'lose') {
                                $sql_add .= ",l=l-1 ";
                            } else if ($bet['result'] == 'draw') {

                            }
                        }
                        $betAmount += -1 * ($bet['betAmount']);
                        $fb_uid = $bet['fb_uid'];
                        $update_bet_sql = null;
                        $sql_update_user = null;
                        $sid = $status_array[$bet['leagueId'] . '_' . $bet['hid'] . '_' . $bet['gid']];
                        $result_sgold = number_format($mul * $bet['bet_sgold'], 3, '.', '');
                        $sgold_income = 0;
                        $sgold_outcome = 0;
                        $scoin_income = 0;
                        $scoin_outcome = 0;
                        if ($result_sgold > 0) {
                            $scoin_income = $bet['bet_sgold'];
                            $sgold_income = $result_sgold;
                        } else if ($result_sgold < 0) {
                            //$scoin_income=$bet['bet_sgold']+$result_sgold;
                            $sgold_outcome = abs($result_sgold);
                        } else {
                            $scoin_income = $bet['bet_sgold'];
                        }

                        $update_bet_sql = "update bet   set status_id=$sid, betResult='$betResult',result='$bResult',FT_score='" . $match['scoreHome'] . '-' . $match['scoreAway'] . "',mul=$mul,betAmount=$betAmount,result_sgold=$result_sgold where betId=" . $bet['betId'];
                        $sql_update_user = "update facebook_user set gp=gp+($betAmount),$sql_add where fb_uid='$fb_uid'";
                        Yii::app()->db->createCommand($update_bet_sql)->execute();
                        Yii::app()->db->createCommand($sql_update_user)->execute();
                        $this->blist[] = $bet['betId'];
                        $bet_id = $bet['betId'];
                        $sql_statement = "select *from total_statement where bet_id=$bet_id order by id DESC";
                        $statement = Yii::app()->db->createCommand($sql_statement)->queryRow();
                        $bet_info = array(
                            'fb_uid' => $fb_uid,
                            'result' => $bResult,
                            're_scoin_income' => $statement['scoin_outcome'],
                            're_sgold_income' => $statement['sgold_outcome'],
                            're_scoin_outcome' => $statement['scoin_income'],
                            're_sgold_outcome' => $statement['sgold_income'],
                            're_diamond_income' => $statement['diamond_outcome'],
                            're_diamond_outcome' => $statement['diamond_income'],
                            'scoin_income' => $scoin_income,
                            'sgold_income' => $sgold_income,
                            'sgold_outcome' => $sgold_outcome,
                            'scoin_outcome' => $scoin_outcome,
                            'mul' => $mul,
                            'bet_id' => $bet['betId'],
                        );
                        array_push($bet_list, $bet_info);
                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollBack();
                        echo $e->getMessage();
                    }
                }
            }
            // $this->blist[] = $bet['betId'];
        }
        $all_user_unique = array_unique($all_user);
        $fb_list = array();
        foreach ($all_user_unique as $fb_uid) {
//            $sql="select *from bet where fb_uid='$fb_uid'
//                        order by betId DESC
//                        limit 100";
            $sql = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = '$fb_uid'
ORDER BY b.betId DESC
LIMIT 100";
            $list = Yii::app()->db->createCommand($sql)->queryAll();
            file_put_contents('../bet_List/' . $fb_uid . '.json', json_encode($list));
            //chmod('../bet_List/'.$fb_uid.'.json',0777);
            $lastResultArray = array();
            $sql = "select betResult from bet where fb_uid='$fb_uid' and (result='win' or result='lose' or result='draw') order by betId DESC limit 5";
            $resultList = Yii::app()->db->createCommand($sql)->queryColumn();
            $resultStr = implode(',', $resultList);
            $resultStr = str_replace('lose', 'l', str_replace('win', 'w', str_replace('draw', 'd', str_replace('lose_half', 'l_h', str_replace('win_half', 'w_h', $resultStr)))));
            $sql_update = "update facebook_user set lastResult='$resultStr' where  fb_uid='$fb_uid'";
            Yii::app()->db->createCommand($sql_update)->execute();
            $sql = "select *from facebook_user where fb_uid='$fb_uid'";

            $obj = Yii::app()->db->createCommand($sql)->queryRow();
            $fb_list[$obj['fb_uid']] = array(
                'sgold' => $obj['sgold'],
                'scoin' => $obj['scoin'],
                'uid' => $obj['uid'],
                'diamond' => $obj['diamond']
            );
            //new TotalStatement();
            file_put_contents('../facebook_info/' . $fb_uid . '.json', json_encode($obj));
            //chmod('../facebook_info/'.$fb_uid.'.json',0775);
        }
        //$this->sendNoti();
        print_r($bet_list);
        $t->reClearBill($bet_list, $fb_list);
        //var_dump($this->blist);
    }

    public function hdp($favorite, $mode_hdp_ou, $choose, $score_home, $score_away)
    {
        echo ' HDP ';
        // ::::::::::: cal :::::::::::
        if ($favorite == 1) {
            // 1 == home favorite
            $chk_favoirte = 'home';
            $score_favorite = $score_home;
            $score_not_favorite = $score_away;
        } elseif ($favorite == 2) {
            // 2 == away favorite
            $chk_favoirte = 'away';
            $score_favorite = $score_away;
            $score_not_favorite = $score_home;
        } else {
            // 0 == not favorite
            if ($choose == 'home') {
                $chk_favoirte = 'not_favorite';
                $score_favorite = $score_away;
                $score_not_favorite = $score_home;
            } else {
                $chk_favoirte = 'not_favorite';
                $score_favorite = $score_home;
                $score_not_favorite = $score_away;
            }
        }

        $hdp = abs($mode_hdp_ou);
        echo ":::::::::::: " . $hdp . "\n";
        // $choose == favorite !!
        if ($choose == $chk_favoirte) {

            $score = ($score_favorite - $score_not_favorite);
            $result = $score - $hdp;

            if ($result == 0) {
                $bet_result = "draw";
            } else if ($result >= 0.5) {
                $bet_result = "win";
            } else if ($result == 0.25) {
                $bet_result = "win_half";
            } else if ($result == -0.25) {
                $bet_result = "lose_half";
            } else {
                $bet_result = "lose";
            }
        } else {
            // $choose != favorite !!
            $score = ($score_not_favorite - $score_favorite);
            $result = $score + $hdp;

            if ($result == 0) {
                $bet_result = "draw";
            } else if ($result >= 0.5) {
                $bet_result = "win";
            } else if ($result == 0.25) {
                $bet_result = "win_half";
            } else if ($result == -0.25) {
                $bet_result = "lose_half";
            } else {
                $bet_result = "lose";
            }
        }

        return $bet_result;
    }

    public function facebookFriendMul($facebook_id, $mid, $choose)
    {

        $sql = "select sum(if(bet.choose = '$choose',1,0))as sum_same,sum(if(bet.choose!='$choose',1,0)) as sum_diff from facebook_friends fb
left join request_notification_count as device on device.fb_uid = fb.friend_facebook_id
left join bet on bet.deviceId = device.device_id and bet.platform = device.platform
where  fb.facebook_id='$facebook_id' and device.fb_uid is not null and bet.mid=$mid";
        $sum = Yii::app()->db->createCommand($sql)->queryRow();
        return $sum;
    }

    public function sendNoti()
    {
        $db = DBConfig::getConnection();
        foreach ($this->blist as $bet) {
            $sql = "SELECT *
FROM `bet` b
LEFT JOIN `timelines_game` tg ON b.mid = tg.mid
WHERE b.betId ='$bet'
";
            // $betdata = Yii::app()->db->createCommand($sql)->queryAll();
            $stmt = $db->query($sql);
            $betdata = $stmt->fetch(5);

            if ($betdata) {
                $this->regEvent('reporter', 'report', "ผลการเล่นเกมส์", "", 'game', "{$betdata->mid}", "{$betdata->fb_uid}", "{$betdata->homeNameTh}", "{$betdata->awayNameTh}", "{$betdata->score}", "{$betdata->choose}", "{$betdata->betHdp}", "{$betdata->betResult}", "{$betdata->betAmount}");
            }
//            var_dump($betdata);
//            exit();
        }
    }

    function regEvent($fb_uid, $type, $sys_smg, $smg, $linktype, $key, $target = null, $home, $away, $score, $choose, $bethdp, $betresult, $gp)
    {
        $db = DBConfig::getConnection();

        $day = date("Y-m-d H:i:s");
        $stamp = strtotime($day);

        $sql = "INSERT INTO `event_list` (`fb_uid`, `type`, `sys_msg`, `message`,`link_type`,`link_key`, `event_at`, `event_stamp`,`home`,`away`,`score`,`choose`,`bet_hdp`,`bet_result`,`gp`) VALUES ('$fb_uid', '$type', '$sys_smg', '$smg','$linktype','$key', '$day', '$stamp','$home','$away','$score','$choose','$bethdp','$betresult','$gp');";
        $success = $db->exec($sql);
        $idsql = "SELECT LAST_INSERT_ID();";
        $stmt = $db->query($idsql);
        $lastid = $stmt->fetchColumn();
        if ($target) {
            $rsql = "INSERT INTO `event_list_related` (`rfb_uid`, `related_to`) VALUES ('$target', $lastid);";
            $db->exec($rsql);
        } else {
            $friend_sql = "SELECT DISTINCT ff.friend_facebook_id as id 
FROM  facebook_friends ff
WHERE ff.facebook_id = '$fb_uid'";
            $stmtfriend = $db->query($friend_sql);
            $userlist = $stmtfriend->fetchAll(5);
            foreach ($userlist as $user) {
                $rsql = "INSERT INTO `event_list_related` (`rfb_uid`, `related_to`) VALUES ('{$user->id}', $lastid);";
                $db->exec($rsql);
            }
        }
    }

}
