<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InitGpAndGoldCommand
 *
 * @author mrsyrop
 */
class InitGpAndGoldCommand extends CConsoleCommand {

    public function run() {
        $db = Yii::app()->db;
        $userlist = $db->createCommand("SELECT `fb_uid` FROM `facebook_user`")->queryAll();
        foreach ($userlist as $user) {
            if (is_numeric($user['fb_uid'])) {

                $betsql = "SELECT b.fb_uid, SUM( b.amount*b.mul) AS gp, COUNT( b.betId ) AS play, fb.pts,SUM(b.result_sgold) as pgold,fb.sgold
FROM bet b
LEFT JOIN facebook_user fb ON b.fb_uid = fb.fb_uid
WHERE b.fb_uid = '{$user['fb_uid']}'  
GROUP BY b.fb_uid
ORDER BY gp DESC";
                $userbet = $db->createCommand($betsql)->queryAll();

                //var_dump($userbet);
                if ($userbet) {
                    $usergold = $userbet[0]['pgold'];
                    $realgp = $userbet[0]['gp'];
                    $updatesql = "UPDATE `sgold_play_statistic` SET `user_sgold`='$usergold' WHERE  fb_uid='{$userbet[0]['fb_uid']}' AND stamp_at=DATE(NOW())";
                    $db->createCommand($updatesql)->execute();
                    $updatesql = "UPDATE `gp_statistic` SET `real_gp`='$realgp' WHERE fb_uid='{$userbet[0]['fb_uid']}' AND stamp_at=DATE(NOW())";
                    $db->createCommand($updatesql)->execute();
                }
            }
        }
    }

}
