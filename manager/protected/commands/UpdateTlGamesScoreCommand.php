<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdateTlGamesCommand
 *
 * @author mrsyrop
 */
require "../../Carbon/src/Carbon/Carbon.php";

use Carbon\Carbon;

class UpdateTlGamesScoreCommand extends CConsoleCommand {

    public function run() {
        $functionstart = Carbon::now();
        $db = Yii::app()->db;
        $tlsql = "SELECT tg.id,tg.mid,lm.sid,lm.s1,lm.s2,lm.c0,lm.c1,lm.c2,lm.cx FROM timelines_game tg
LEFT JOIN live_match lm ON lm.mid=tg.mid
WHERE tg.sid > 1
AND tg.sid < 5
AND DATE(tg.show_date)=CURRENT_DATE";
        $tlgames = $db->createCommand($tlsql)->queryAll();
        foreach ($tlgames as $tlgame) {
                $score = $tlgame["s2"];
                if (!empty($tlgame["s1"])) {
                    $score = $tlgame["s1"];
                }
                //echo $updatesql . "\n";
                $updatesql = "UPDATE timelines_game SET score='$score',sid='{$tlgame['sid']}',c0='{$tlgame['c0']}',c1='{$tlgame['c1']}',c2='{$tlgame['c2']}',cx='{$tlgame['cx']}' WHERE id='{$tlgame["id"]}'";
                $db->createCommand($updatesql)->execute();
        }
    }

}
