<?php
/**
 * Created by JetBrains PhpStorm.
 * User: indpendents
 * Date: 8/8/13 AD
 * Time: 1:56 AM
 * To change this template use File | Settings | File Templates.
 */

class ClearDataCommand extends CConsoleCommand {
    public function run($args){
        $this->clearLeagueMatchEvent();
    }
    public function clearLeagueMatchEvent(){
        $live_match_sql="select mid from live_match where showDate  < current_date - interval 10 day";
        $live_match_list = Yii::app()->db->createCommand($live_match_sql)->queryColumn();
        echo "total data :".count($live_match_list)." .\n";
        foreach($live_match_list as $key=>$mid){
            $delete_event_sql ="delete from live_match_event where mid =".$mid;
            Yii::app()->db->createCommand($delete_event_sql)->execute();
            $filename ='../gen_file_events/'.$mid.'.json';
            if(file_exists($filename)){
                unlink($filename);
            }
            $key++;
            echo "process $key of ".count($live_match_list)."\n";

        }
        $sql ="delete from live_match where showDate  < current_date - interval 10 day;
               delete from live_league where date < current_date - interval 10 day;
               delete from live_match_checked where date < current_date - interval 10 day;

        ";
        Yii::app()->db->createCommand($sql)->execute();
    }
}