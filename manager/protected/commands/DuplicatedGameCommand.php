<?php
/**
 * Created by PhpStorm.
 * User: Panupong
 * Date: 21/11/2556
 * Time: 17:04 น.
 */
require "../../Carbon/src/Carbon/Carbon.php";
use Carbon\Carbon;

class DuplicatedGameCommand extends CConsoleCommand
{
    public function run($args)
    {
        $dt = Carbon::now();
        $yesterday = $dt->yesterday()->toDateString();
        $sql = "SELECT * FROM live_match lm
WHERE DATE(lm.date)=DATE(NOW())
AND lm.showDate='$yesterday'
ORDER BY lm.date";
        //echo $sql . "\n";
        $livematch = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($livematch as $lm) {
            echo $lm['mid'] . "\n";
            $attr = "";
            $value = "";
            foreach ($lm as $key => $val) {
                //echo $key . " ";
                if ($key == 'mid') {
                    $attr .= "`$key`";
                    $value .= "'$val'";

                }else{
                    $attr .= ","."`$key`";
                    if($key=='showDate'){
                        $value .= "," . "'{$dt->toDateString()}'";
                    }else {
                        $value .= "," . "'$val'";
                    }
                }
            }
            $insert = "INSERT IGNORE live_match ($attr) VALUES ($value)";
            Yii::app()->db->createCommand($insert)->execute();
            //echo "$insert\n";

        }
    }

}