<?php
/**
 * Created by PhpStorm.
 * User: Panupong
 * Date: 19/11/2556
 * Time: 16:00 น.
 */

class UpdateExpCommand extends CConsoleCommand{
    public function run($args){
        $this->updateDataV1();
    }
    public function updateDataV1(){
        $db = Yii::app()->db;
        $sql="select  round((sum(if(result='win',1,0))/sum(if(result='win' or result='lose',1,0)))*sum(if(result='win',1,0))) as exp,fb_uid from bet
where date(from_unixtime(betDatetime)) = current_date -interval 1 day and result!='wait' group by fb_uid ";
        $list =$db->createCommand($sql)->queryAll();
        $transaction=$db->beginTransaction();
        try{
            foreach($list as $obj){
                $fb_uid =  $obj['fb_uid'];
                $exp = $obj['exp'];
                $sql_update ="update facebook_user set exp=exp+".$exp." where fb_uid='$fb_uid'";
                $db->createCommand($sql_update)->execute();
            }
            $transaction->commit();
        }catch(Exception $e){
            $transaction->rollback();
        }
        $transaction=$db->beginTransaction();
        try{
            $sql_update =
                "
                update facebook_user set level= FLOOR(exp/10)+1 where exp<=100;
                update facebook_user set level= 11+FLOOR((exp-100)/20) where exp>100;
                ";
            $db->createCommand($sql_update)->execute();
            $transaction->commit();
        }catch(Exception $e){
            $transaction->rollback();
        }
        $transaction=$db->beginTransaction();
        try{
            foreach($list as $obj){
                $fb_uid =  $obj['fb_uid'];
                $sql ="select *from facebook_user where fb_uid='$fb_uid'";
                $obj=$db->createCommand($sql)->queryRow();
                file_put_contents('../facebook_info/'.$fb_uid.'.json',json_encode($obj));

            }
            $transaction->commit();
        }catch(Exception $e){
            $transaction->rollback();
        }

    }
} 