<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DailyUserGpCommand
 *
 * @author mrsyrop
 */
class DailyUserGpCommand extends CConsoleCommand {

    public function run($args) {
        $db = Yii::app()->db;
        $userlist = $db->createCommand("SELECT * FROM facebook_user")->queryAll();
        $day = date("Y-m-d h:i:s");
        $yesterday = date("Y-m-d", strtotime($day . "-1day"));
        $week = date("W");
        $firstmonday = date("Y-m-d", strtotime('Monday this week'));
        $nowday = date("Y-m-d");
        //$transaction = Yii::app()->getDb()->beginTransaction();
        try {
            foreach ($userlist as $user) {
                if (is_numeric($user["fb_uid"])) {
                    $today_stamp = $db->createCommand("SELECT * FROM user_statistic us WHERE us.fb_uid='{$user["fb_uid"]}' AND us.stamp_at='$nowday'")->queryAll();
                    if (empty($today_stamp)) {
                        $chk_stamp = "SELECT * FROM user_statistic us WHERE us.fb_uid='{$user["fb_uid"]}' AND us.stamp_at='$yesterday'";
                        $yesterdaystamp = $db->createCommand($chk_stamp)->queryAll();

                        //var_dump($yesterdaystamp);exit;

                        $gp_change = 0.0;
                        $stamp_gp = (double) $user["gp"];


                        if ($week % 2 != 0 && $firstmonday == $nowday) {
                            $stamp_gp = 1500.00;
                        }

                        if ((int) $user["overall_gp"] == 0) {
                            $updateuser = "UPDATE facebook_user SET `overall_gp`='{$user["gp"]}' WHERE `fb_uid`='{$user["fb_uid"]}'";
                            $db->createCommand($updateuser)->execute();
                        }

                        if (!empty($yesterdaystamp)) {
                            $gp_change = (double) $user["gp"] - (double) $yesterdaystamp[0]["daily_stamp"];
                            $overall = (double) $user["overall_gp"] + $gp_change;
                            $updatestat = "UPDATE user_statistic SET `daily_change`='$gp_change',`overall_gp`='$overall',`update_at`=DATE('$day') WHERE id='{$yesterdaystamp[0]["id"]}'";
                            $db->createCommand($updatestat)->execute();
                        }
//                        if (empty($today_stamp)) {
                        $addnew = "INSERT INTO user_statistic (`fb_uid`,`daily_stamp`,`daily_change`,`stamp_at`,`update_at`,`week`) VALUES ('{$user["fb_uid"]}','$stamp_gp',0.0,DATE('$day'),DATE('$day'),'$week')";
                        $db->createCommand($addnew)->execute();
                        $updateuser = "UPDATE facebook_user SET `gp`=$stamp_gp,`overall_gp`=`overall_gp`+$gp_change WHERE `fb_uid`='{$user["fb_uid"]}'";
                        $db->createCommand($updateuser)->execute();
//                        } else {
//                            $minor_change = (double) $user["gp"] - (double) $today_stamp[0]["daily_stamp"];
//                            $overall = (double) $user["overall_gp"] + $minor_change;
//
//                            $updatestat = "UPDATE user_statistic SET `daily_stamp`={$user["gp"]},`overall_gp`=$overall,`stamp_at`='$day' WHERE id={$today_stamp[0]["id"]}";
//                            $db->createCommand($updatestat)->execute();
//                            $updateuser = "UPDATE facebook_user SET `gp`=$stamp_gp,`overall_gp`=`overall_gp`+$minor_change WHERE `fb_uid`={$user["fb_uid"]}";
//                            $db->createCommand($updateuser)->execute();
//                        }
                    }
                }
            }
            //$transaction->commit();
        } catch (Exception $e) {
            //$transaction->rollBack();
            echo $e->getMessage();
        }
    }

}
