<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UpdateTlGamesCommand
 *
 * @author mrsyrop
 */
require "../../Carbon/src/Carbon/Carbon.php";

use Carbon\Carbon;

class SubleagueStatCommand extends CConsoleCommand {

    public function run($args) {
        $functionstart = Carbon::now();
        $dt = Carbon::now();
        $db = Yii::app()->db;
        $tlsql = "SELECT ll.leagueId,ll.subleagueId FROM live_league ll
WHERE ll.date BETWEEN DATE(NOW()) AND DATE_ADD(current_date,INTERVAL 3 DAY)
ORDER BY ll.leagueId";
        $subleaguelist = $db->createCommand($tlsql)->queryAll();
        foreach ($subleaguelist as $league) {
            echo $league['subleagueId']." ";
            $insertsql ="INSERT IGNORE INTO live_league_checked (`date`, `leagueId`) VALUES ('{$dt->toDateString()}', '{$league['subleagueId']}');";
            echo " ".$insertsql." ";
            $success=$db->createCommand($insertsql)->execute();
            if($success){
                echo "success";
            }else{
                echo "fail";
            }
            echo "\n";

            echo $league['leagueId']." ";
            $insertsql ="INSERT IGNORE INTO live_league_checked (`date`, `leagueId`) VALUES ('{$dt->toDateString()}', '{$league['leagueId']}');";
            echo " ".$insertsql." ";
            $success=$db->createCommand($insertsql)->execute();
            if($success){
                echo "success";
            }else{
                echo "fail";
            }
            echo "\n";
        }
    }

}
