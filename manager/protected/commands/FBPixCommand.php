<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 12/1/2559
 * Time: 10:48 น.
 */
class FBPixCommand extends CConsoleCommand
{

    public function run()
    {
        $db = Yii::app()->db;
        header("content-type: image/jpeg");
        $dir = "../cache";
        $small = $dir . "/small";
        $large = $dir . "/large";
        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }

        if (!is_dir($small)) {
            mkdir($small, 0777);
        }

        if (!is_dir($large)) {
            mkdir($large, 0777);
        }

        $allsql = "SELECT * FROM facebook_user fu
WHERE fu.last_login_date  >= DATE(NOW()) - INTERVAL 3 DAY";
        $userlist = $db->createCommand($allsql)->queryAll();
        foreach ($userlist as $key => $user) {
            echo $key . ":" . $user['fb_uid'], "\n";
            if (is_numeric($user['fb_uid'])) {
                if (!is_file($small . "/" . $user['fb_uid'] . ".jpg")) {
                    $url = "https://graph.facebook.com/" . $user['fb_uid'] . "/picture?width=50&height=50";
                    $hd = get_headers($url);
                    echo $hd[2] . "\n";
                    $data = file_get_contents($url);
                    $file = $small . "/" . $user['fb_uid'] . ".jpg";
                    file_put_contents($file, $data);
                }
                if (!is_file($large . "/" . $user['fb_uid'] . ".jpg")) {
                    $url = "https://graph.facebook.com/" . $user['fb_uid'] . "/picture?width=200&height=200";
                    $data = file_get_contents($url);
                    $file = $large . "/" . $user['fb_uid'] . ".jpg";
                    file_put_contents($file, $data);
                }
                //exit;
            }
        }

    }

}