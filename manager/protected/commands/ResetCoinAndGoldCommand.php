<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ResetCionAndGoldCommand
 *
 * @author mrsyrop
 */
require('../DBConfig.php');

class ResetCoinAndGoldCommand extends CConsoleCommand {

    public function run() {
        $db = DBConfig::getConnection();

        $clearmini = $db->exec("TRUNCATE TABLE mini_game_statement");
        if ($clearmini) {
            echo "mini game clear\n";
        } else {
            echo "mini game clear failed\n";
        }

        $clearmini = $db->exec("TRUNCATE TABLE total_statement");
        if ($clearmini) {
            echo "total statement clear\n";
        } else {
            echo "total statement clear failed\n";
        }

        $sql = "SELECT * FROM facebook_user";
        $stmt = $db->query($sql);
        $users = $stmt->fetchAll(5);
        foreach ($users as $user) {
            if (is_numeric($user->fb_uid)) {
                $success = $db->exec("UPDATE `facebook_user` SET `sgold`=0, `scoin`=5000, `last_login_date`='000 -00-00' WHERE  `uid`='{$user->uid}';");
                if ($success) {
                    echo "{$user->uid}:{$user->display_name} was reset\n";
                } else {
                    echo "{$user->uid}:{$user->display_name} reset failed\n";
                }
            }
        }
    }

}
