<?php
/**
 * Created by PhpStorm.
 * User: Tingly
 * Date: 27/6/2557
 * Time: 17:40 น.
 */

class SystemConfigInitCommand extends CConsoleCommand {
    public function run($args){
        $config =array(
            'sgold_init'=>0,
            'scoin_init'=>5000,
            'diamond_init'=>0,
            'ball_game_sgold'=>array(0,0,0,0,0,0,0,0,0,0,0),
            'ball_game_scoin'=>array( 50,100,150,200,300,400,500,750,1000,1200),
            'mini_game_sgold'=>array(0,0,0),
            'mini_game_scoin'=>array(100,200,500),
            'login_sgold'=>array(0,0,0,0,0,0,0),
            'login_scoin'=>array(1000,1100,1200,1300,1400,1500,1700),
            'free_treasure_scoin1'=>500,
            'free_treasure_scoin2'=>1000,
            'free_treasure_scoin_percent1'=>50,
            'free_treasure_scoin_percent2'=>25,
            'treasure_low_between'=>array(0,200),
            'treasure_low_scoin'=>100,
            'treasure_limit_per_day'=>10,
            'treasure_medium_between'=>array(0,500),
            'treasure_medium_scoin'=>250,
            'treasure_high_between'=>array(0,1000),
            'treasure_high_scoin'=>500,
            'exchange_rate'=>array(
                array(1,100),
                array(1,125)
            ),
            'play_sgold_limit'=>-3000,
            'reset_sgold_day'=>7,
            'sgold_to_diamond'=>1000,
            'step_combo_per_day'=>array(
                3=>3,
                4=>5,
                5=>6,
                6=>7,
                7=>8,
                8=>9,
                9=>10,
                10=>11,
                11=>12,
                12=>13,
                13=>14,
                14=>15,
                15=>16,
                16=>17
            ),
            'step_combo_continues'=>array(
                3=>3,
                4=>5,
                5=>6,
                6=>7,
                7=>8,
                8=>9,
                9=>10,
                10=>11,
                11=>12,
                12=>13,
                13=>14,
                14=>15,
                15=>16,
                16=>17
            ),
            'diamond_income'=>1,
            'diamond_outcome'=>0.5,
            'award_top_sgold'=>array(
                array('sgold'=>0,'diamond'=>600,'scoin'=>0),//1
                array('sgold'=>0,'diamond'=>400,'scoin'=>0),//2
                array('sgold'=>0,'diamond'=>300,'scoin'=>0),//3
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//4
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//5
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//6
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//7
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//8
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//9
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//10


            ),
            'award_top_ranking'=>array(
                array('sgold'=>0,'diamond'=>600,'scoin'=>0),//1
                array('sgold'=>0,'diamond'=>400,'scoin'=>0),//2
                array('sgold'=>0,'diamond'=>300,'scoin'=>0),//3
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//4
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//5
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//6
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//7
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//8
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//9
                array('sgold'=>0,'diamond'=>100,'scoin'=>0),//10
            ),
            'award_top_league'=>array(
                array('sgold'=>0,'diamond'=>3000,'scoin'=>0),//1
                array('sgold'=>0,'diamond'=>2000,'scoin'=>0),//2
                array('sgold'=>0,'diamond'=>1000,'scoin'=>0),//3
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//4
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//5
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//6
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//7
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//8
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//9
                array('sgold'=>0,'diamond'=>500,'scoin'=>0),//10
            ),

        );
        file_put_contents('../system_config/file.json',json_encode($config));
        echo "success!!!\n";
    }
} 