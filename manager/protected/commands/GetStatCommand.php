<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetStatCommand
 *
 * @author withee
 */
class GetStatCommand extends CConsoleCommand {

    public function run($args) {
        $spirit = isset($args[0]) ? $args[0] : 0;
        $db = Yii::app()->db;
        $transaction = $db->beginTransaction();
        $sql_user_list = "SELECT lm._lid,ll.leagueNameTh league,lm.hid,ht.teamNameTh home,lm.gid,at.teamNameTh away,lm.s1 score
 FROM live_match lm
LEFT JOIN lang_league ll ON ll.leagueId=lm._lid
LEFT JOIN lang_team ht ON ht.tid=lm.hid
LEFT JOIN lang_team at ON at.tid=lm.gid

WHERE
ht.teamNameTh IS NOT NULL 
AND at.teamNameTh IS NOT NULL
ORDER BY lm._lid";
        $matchlist = $db->createCommand($sql_user_list)->queryAll();
        try {
            $content = "";
            foreach ($matchlist as $match) {
                $home = file_get_contents("http://ws.1ivescore.com/teamOnlys/th/{$match["hid"]}");
                $away = file_get_contents("http://ws.1ivescore.com/teamOnlys/th/{$match["gid"]}");
                $h2h = file_get_contents("http://ws.1ivescore.com/compareOnly?hid={$match["hid"]}&gid={$match["gid"]}&lang=th");
                $home = json_decode($home);
                $away = json_decode($away);
                $h2h = json_decode($h2h);
                //echo $home;
                $content.=$match["league"] . "\n";
                $content.=$match["home"] . " VS " . $match["away"] . "\n";
                $arrHome = array();
                $arrAway = array();
                $content.="Last Result\n";
                $content.=$match["home"] . ",H1,H2,H3,H4,H5,A1,A2,A3,A4,A5\n";
                $content.=" ,";
                for ($i = 0; $i < 5; $i++) {
                    if (!empty($home->result)) {
                        if (array_key_exists($i, $home->result)) {
                            $scores = $home->result[$i]->score;
                            $score = explode("-", $scores);
                            if ((int) $match["hid"] == (int) $home->result[$i]->tid1) {
                                $arrHome[] = $score[0];
                                $arrAway[] = $score[1];
                            } else {
                                $arrHome[] = $score[1];
                                $arrAway[] = $score[0];
                            }
                        } else {
                            $arrHome[] = "-";
                            $arrAway[] = "-";
                        }
                    } else {
                        $arrHome[] = "-";
                        $arrAway[] = "-";
                    }
                }
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrHome[$i] . ",";
                }
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrAway[$i] . ";";
                }
                $content.="\n";
                $arrHome = array();
                $arrAway = array();
                $content.=$match["away"] . ",H1,H2,H3,H4,H5,A1,A2,A3,A4,A5\n";
                $content.=" ,";
                for ($i = 0; $i < 5; $i++) {
                    if (!empty($away->result)) {
                        if (array_key_exists($i, $away->result)) {
                            $scores = $away->result[$i]->score;
                            $score = explode("-", $scores);
                            if ((int) $match["gid"] == (int) $away->result[$i]->tid1) {
                                $arrHome[] = $score[0];
                                $arrAway[] = $score[1];
                            } else {
                                $arrHome[] = $score[1];
                                $arrAway[] = $score[0];
                            }
                        } else {
                            $arrHome[] = "-";
                            $arrAway[] = "-";
                        }
                    } else {
                        $arrHome[] = "-";
                        $arrAway[] = "-";
                    }
                }
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrHome[$i] . ",";
                }
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrAway[$i] . ";";
                }
                $content.="\n";


                $arrHome = array();
                $arrAway = array();
                $content.="H2H\n";
                for ($i = 0; $i < 5; $i++) {
                    if (!empty($h2h->result_vs)) {
                        if (array_key_exists($i, $h2h->result_vs)) {
                            $scores = $h2h->result_vs[$i]->score;
                            $score = explode("-", $scores);
                            if ((int) $match["hid"] == (int) $h2h->result_vs[$i]->tid1) {
                                $arrHome[] = $score[0];
                                $arrAway[] = $score[1];
                            } else {
                                $arrHome[] = $score[1];
                                $arrAway[] = $score[0];
                            }
                        } else {
                            $arrHome[] = "-";
                            $arrAway[] = "-";
                        }
                    } else {
                        $arrHome[] = "-";
                        $arrAway[] = "-";
                    }
                }
                $content.=$match["home"] . ",";
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrHome[$i] . ",";
                }
                $content.="\n";
                $content.=$match["away"] . ",";
                for ($i = 0; $i < 5; $i++) {
                    $content.=$arrAway[$i] . ";";
                }
                $content.="\n\n\n";
                echo $content;
                //exit();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
        }
    }

}

?>
