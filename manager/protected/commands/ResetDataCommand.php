<?php
/**
 * Created by PhpStorm.
 * User: Panupong
 * Date: 26/11/2556
 * Time: 10:45 น.
 */

class ResetDataCommand extends CConsoleCommand{
    private $pointMul=1;
    public function run($args){
        $mulTable =array(
            'win'=>1,
            'lose'=>-1,
            'draw'=>0,
            'win_half'=>0.5,
            'lose_half'=>-0.5
        );
        $db =Yii::app()->db;
        $transaction = $db->beginTransaction();
        try{
            $sql ="select *from facebook_user";
            $all_user = $db->createCommand($sql)->queryAll();
            foreach($all_user as $user){
                $fb_uid = $user['fb_uid'];
                //echo ($fb_uid)."\n";
                $sql_all_bet ="select *from bet where fb_uid='$fb_uid' and result!='wait'";
                $all_bet=$db->createCommand($sql_all_bet)->queryAll();
                $draw =0;$win=0;$lose=0;
                $betAmountTotal=1500;
                foreach($all_bet as $bet){
                    if($bet['FT_score']=='P-P'){
                        $draw++;
                    }else{
                        $scoreArray =  explode('-',$bet['FT_score']);
                        $result_diff=$scoreArray[0]-$scoreArray[1];
                        if($scoreArray[0]==$scoreArray[1]){
                            $result_win='draw';
                        }else if($scoreArray[0]>$scoreArray[1]){
                            $result_win ='home';
                        }else{
                            $result_win ='away';
                        }
                        $match['result_win']=$result_win;
                        $match['result_diff']=$result_diff;
                        $match['scoreHome']=$scoreArray[0];
                        $match['scoreAway']=$scoreArray[1];
                        if($bet['oddsType']=='1x2'){
                            $mul = $bet['choose']==$match['result_win']?$bet['betValue']:0;
                            $betResult  =$mul==0?'lose':'win';

                        }else if($bet['oddsType']=='hdp'){
                            $favorite=0;
                            if($bet['betHdp']>0){
                                $favorite=2;
                            }else if($bet['betHdp']<0){
                                $favorite=1;
                            }
                            $betResult=$this->hdp($favorite,$bet['betHdp'],$bet['choose'],$match['scoreHome'],$match['scoreAway']);
                        }
                        //echo $betResult;
                        if($betResult=='win' || $betResult=='win_half'){
                            $mul = $mulTable[$betResult]*($bet['betValue']-1);
                        }else{
                            $mul = $mulTable[$betResult];
                        }

                        $betAmount = $mul*$bet['amount']*$this->pointMul;
                        $bResult = $betResult;
                        if($betResult=='win_half'){
                            $bResult='win';
                        }else if($betResult=='lose_half'){
                            $bResult='lose';
                        }
                        if($bResult=='win'){
                            $win++;
                        }else if($bResult=='lose'){
                            $lose++;
                        }else{
                            $draw++;
                        }

                        $betId = $bet['betId'];
                        $sql_update_bet=
                            "
                            update bet set
                            betResult='$betResult',
                            result='$bResult',
                            mul =$mul,
                            betAmount = $betAmount
                            where betId=$betId
                            ";
                        $db->createCommand($sql_update_bet)->execute();
                        $betAmountTotal+=$betAmount;
                    }
                }
                $sql_update_user = "update facebook_user set gp=$betAmountTotal,w=$win,l=$lose,d=$draw where fb_uid='$fb_uid'";
                $db->createCommand($sql_update_user)->execute();
                echo "win=$win,lose=$lose,draw=$draw\n";
            }
            $transaction->commit();
        }catch(Exception $e){
            $transaction->rollback();
            echo $e->getMessage();
        }
    }
    public function hdp($favorite, $mode_hdp_ou, $choose, $score_home, $score_away) {
        //echo ' HDP ';
        // ::::::::::: cal :::::::::::
        if ($favorite == 1) {
            // 1 == home favorite
            $chk_favoirte = 'home';
            $score_favorite = $score_home;
            $score_not_favorite = $score_away;
        } elseif ($favorite == 2) {
            // 2 == away favorite
            $chk_favoirte = 'away';
            $score_favorite = $score_away;
            $score_not_favorite = $score_home;
        } else {
            // 0 == not favorite
            if ($choose == 'home') {
                $chk_favoirte = 'not_favorite';
                $score_favorite = $score_away;
                $score_not_favorite = $score_home;
            } else {
                $chk_favoirte = 'not_favorite';
                $score_favorite = $score_home;
                $score_not_favorite = $score_away;
            }
        }

        $hdp = abs($mode_hdp_ou);
        //echo ":::::::::::: " . $hdp . "\n";
        // $choose == favorite !!
        if ($choose == $chk_favoirte) {

            $score = ($score_favorite - $score_not_favorite);
            $result = $score - $hdp;

            if ($result == 0) {
                $bet_result = "draw";
            } else if ($result >= 0.5) {
                $bet_result = "win";
            } else if ($result == 0.25) {
                $bet_result = "win_half";
            } else if ($result == -0.25) {
                $bet_result = "lose_half";
            } else {
                $bet_result = "lose";
            }
        } else {
            // $choose != favorite !!
            $score = ($score_not_favorite - $score_favorite);
            $result = $score + $hdp;

            if ($result == 0) {
                $bet_result = "draw";
            } else if ($result >= 0.5) {
                $bet_result = "win";
            } else if ($result == 0.25) {
                $bet_result = "win_half";
            } else if ($result == -0.25) {
                $bet_result = "lose_half";
            } else {
                $bet_result = "lose";
            }
        }

        return $bet_result;
    }
} 