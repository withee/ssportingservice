<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GameEventTrigerCommand
 *
 * @author mrsyrop
 */
require '../../analog/lib/Analog.php';
require "../../Carbon/src/Carbon/Carbon.php";

use Carbon\Carbon;

class GameEventTrigerCommand extends CConsoleCommand {

    public function run($args) {
//        $logger = new Logger;
//        $logger->handler(Analog\Handler\File::init('../logs/GameEventTriger.log'));
//        $logger->info("logsssss");

        $ge = new GameEvent();


        Analog::handler(\Analog\Handler\File::init('../logs/GameEventTriger.log'));
        Analog::log("in public function run()", 6);
        //..................Print log here...............................
        // log "in public function run($args) {"
        //.......................................................................

        $db = Yii::app()->db;
        $file = '../odds/today.json';
        var_dump(file_exists($file));

        $odds = '';
        if (array_key_exists(0, $args)) {
            $now = Carbon::now();
            $lastaccess = $ge->lastAccess($args[0]);

            $filetime = Carbon::create(date("Y", $lastaccess), date("m", $lastaccess), date("d", $lastaccess), date("H", $lastaccess), date("i", $lastaccess), date("s", $lastaccess));
            //echo $filetime;

            if (!$ge->getDup($args[0], $now, $filetime)) {
                if (file_exists($file)) {
                    $odds = json_decode(file_get_contents($file), true);
                }
                $lmsql = "SELECT _lid,hid,gid,sid,c0,c1,c2,cx,date,s2,s1 FROM live_match WHERE mid='{$args[0]}'";
                //..................Print log here...............................
                // log $lmsql
                //.......................................................................
                Analog::log("$lmsql", 6);
                $lm = $db->createCommand($lmsql)->queryAll();
                $hdp = "";
                $hdphome = "";
                $hdpaway = "";
                $type = "";
                if (!empty($lm)) {
                    $score = $lm[0]["s2"];
                    if (!empty($lm[0]["s1"])) {
                        $score = $lm[0]["s1"];
                    }
                    //$updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',c0='{$lm[0]["c0"]}',c1='{$lm[0]["c1"]}',c2='{$lm[0]["c2"]}',cx='{$lm[0]["cx"]}',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`show_date`='{$lm[0]['date']}' WHERE mid='{$args[0]}'";
                    if ($odds) {
                        $key = "{$lm[0]['_lid']}_{$lm[0]['hid']}_{$lm[0]['gid']}";
                        //echo $key;
                        if (array_key_exists($key, $odds["data"])) {
                            if (array_key_exists('SBOBET', $odds['data']["$key"])) {
                                $hdp = $odds['data']["$key"]['SBOBET']['hdp'];
                                $hdphome = $odds['data']["$key"]['SBOBET']['hdp_home'];
                                $hdpaway = $odds['data']["$key"]['SBOBET']['hdp_away'];
                                $type = 'SBOBET';
                            } else if (array_key_exists('Bet365', $odds['data']["$key"])) {
                                $hdp = $odds['data']["$key"]['Bet365']['hdp'];
                                $hdphome = $odds['data']["$key"]['Bet365']['hdp_home'];
                                $hdpaway = $odds['data']["$key"]['Bet365']['hdp_away'];
                                $type = 'Bet365';
                            } else if (array_key_exists('Ladbrokes', $odds['data']["$key"])) {
                                $hdp = $odds['data']["$key"]['Ladbrokes']['hdp'];
                                $hdphome = $odds['data']["$key"]['Ladbrokes']['hdp_home'];
                                $hdpaway = $odds['data']["$key"]['Ladbrokes']['hdp_away'];
                                $type = 'Ladbrokes';
                            }
                            //$updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`type`='$type',`show_date`='{$lm[0]['date']}' WHERE mid='{$args[0]}'";
                        }
                    }
                    $updatesql = "UPDATE timelines_game SET sid='{$lm[0]["sid"]}' , score='$score',c0='{$lm[0]["c0"]}',c1='{$lm[0]["c1"]}',c2='{$lm[0]["c2"]}',cx='{$lm[0]["cx"]}',`hdp`='$hdp',`hdp_home`='$hdphome',`hdp_away`='$hdpaway',`show_date`='{$lm[0]['date']}' WHERE mid='{$args[0]}'";
                    //echo $updatesql . "\n";
                    //..................Print log here...............................
                    // log $updatesql
                    //.......................................................................
                    Analog::log("$updatesql", 6);
                    $db->createCommand($updatesql)->execute();
                }
            } else {
                Analog::log("Triger on same match $args[0]", 6);
            }
        }
        //..................Print log here...............................
        // log "out public function run($args) {"
        //.......................................................................
        Analog::log("out public function run()", 6);
    }

}
