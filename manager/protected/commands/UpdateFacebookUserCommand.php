<?php

/**
 * Created by PhpStorm.
 * User: Panupong
 * Date: 18/11/2556
 * Time: 11:45 น.
 */
class UpdateFacebookUserCommand extends CConsoleCommand {

    public function run($args) {
        $sql = "select *from facebook_user ";
        $list = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($list as $obj) {
            $fb_uid = $obj['fb_uid'];
            $sql = "select betResult from bet where fb_uid='$fb_uid' and (result='win' or result='lose' or result='draw') order by betId DESC limit 5";
            $resultList = Yii::app()->db->createCommand($sql)->queryColumn();
            $resultStr = implode(',', $resultList);
            $resultStr = str_replace('lose', 'l', str_replace('win', 'w', str_replace('draw', 'd', str_replace('lose_half', 'l_h', str_replace('win_half', 'w_h', $resultStr)))));
            $sql_update = "update facebook_user set lastResult='$resultStr' where  fb_uid='$fb_uid'";
            Yii::app()->db->createCommand($sql_update)->execute();
//            $sql = "select *from bet where fb_uid='$fb_uid'
//                        order by betId DESC
//                        limit 100";
            $sql = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = '$fb_uid'
ORDER BY b.betId DESC
LIMIT 100";
            $list = Yii::app()->db->createCommand($sql)->queryAll();

            file_put_contents('../bet_List/' . $fb_uid . '.json', json_encode($list));
            //chmod('../bet_List/'.$fb_uid.'.json',0775);

            $sql = "select *from facebook_user where fb_uid='$fb_uid'";
            $_obj = Yii::app()->db->createCommand($sql)->queryRow();
            $tpsql = "SELECT ua.fb_uid, t.type, t.sequence, t.picture, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
GROUP BY ua.trophy_id
";
            $trophy = Yii::app()->db->createCommand($tpsql)->queryAll();
            $_obj["achieve"] = $trophy;
            if (($_obj['pts'] - $_obj['d']) > 0) {
                $_obj['accurate'] = number_format(floatval($_obj['w'] / ($_obj['pts'] - $_obj['d']) * 100), 2);
            } else {
                $_obj['accurate'] = number_format(0, 2);
            }

            file_put_contents('../facebook_info/' . $fb_uid . '.json', json_encode($_obj));
            //chmod('../facebook_info/'.$fb_uid.'.json',0775);
        }
    }

}
