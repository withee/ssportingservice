<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TimelinesGameCommand
 *
 * @author mrsyrop
 */
class TimelinesGameCommand extends CConsoleCommand
{

    public function run()
    {
//        $json = json_decode(file_get_contents("http://api.ssporting.com/oddsToday"), true);
        $json = json_decode(file_get_contents("../odds/today.json"), true);

        $db = Yii::app()->db;
        /* case live game has update
          $livesql = "SELECT * FROM live_match WHERE new='Y'";
          $lives = $db->createCommand($livesql)->queryAll();
          foreach ($lives as $live) {
          $score = $live['s2'];
          if (!empty($live['s1'])) {
          $score = $live['s1'];
          }
          $updatetl = "UPDATE `timelines_game` SET `_lid`={$live['_lid']}, `hid`={$live['hid']}, `gid`={$live['gid']}, `score`='$score' WHERE  `mid`={$live['mid']};";
          $db->createCommand($updatetl)->execute();
          }
         */
        $updatefieldsql = "SELECT * FROM timelines_game WHERE mid IN (SELECT mid FROM live_match WHERE sid=1 OR sid=11)";
        $rows = $db->createCommand($updatefieldsql)->queryAll();
        foreach ($rows as $row) {

            $lmsql = "SELECT sid,hid,gid,date FROM live_match WHERE mid={$row['mid']}";
            $livematch = $db->createCommand($lmsql)->queryRow();
            $tlsql = "SELECT sid,hid,gid FROM timelines_game WHERE mid={$row['mid']}";
            $timelinetemp = $db->createCommand($lmsql)->queryRow();
            if ($livematch) {

                if ((int)$livematch['hid'] != (int)$timelinetemp['hid']) {
                    $swapdate = date("Y-m-d H:i:s");
                    //echo $swapdate;
                    $db->createCommand("INSERT INTO `team_swap` (`mid`, `bf_hid`, `bf_gid`, `af_hid`, `af_gid`, `created_at`) VALUES ({$row['mid']}, {$timelinetemp['hid']}, {$timelinetemp['gid']}, {$livematch['hid']}, {$livematch['gid']}, '{$swapdate}');")->execute();
                }
                $db->createCommand("UPDATE timelines_game SET sid='{$livematch['sid']}',hid='{$livematch['hid']}',gid='{$livematch['gid']}',show_date='{$livematch['date']}' WHERE id={$row['id']}")->execute();
            }

            //echo $row["id"] . "\n";
            if (array_key_exists((int)$row["_lid"], $json["lang_league"])) {
                $sql = "SELECT competitionId,leagueName FROM lang_league WHERE leagueId='{$row["_lid"]}'";
                $langleague = $db->createCommand($sql)->queryRow();

                $league = $this->removeSpecial($json["lang_league"][$row["_lid"]]);
                $updatetl = "UPDATE `timelines_game` SET `leagueNameEn`='{$league["en"]}', `leagueNameTh`='{$league["th"]}' WHERE  `id`={$row["id"]};";
                if ($langleague) {
                    $ll = $this->removeSpecial($langleague);
                    $updatetl = "UPDATE `timelines_game` SET `competitionId`='{$ll['competitionId']}',`leagueName`='{$ll['leagueName']}', `leagueNameEn`='{$league["en"]}', `leagueNameTh`='{$league["th"]}' WHERE  `id`={$row["id"]};";
                }

                //echo "update league from file\n";
                $db->createCommand($updatetl)->execute();
            } else {
                $leaguesql = "SELECT 
		ll.competitionId,
                    ll.leagueId,
      ll.leagueName as LeagueName,
      ll.leagueNameEn as LeagueNameEn,
      ll.leagueNameTh as LeagueNameTh,
      ll.leagueNameBig LeagueNameBig,
      ll.leagueNameGb LeagueNameGb,
      ll.leagueNameKr LeagueNameKr,
      ll.leagueNameVn LeagueNameVn,
      ll.leagueNameLa LeagueNameLa,
      ll.id7m
FROM lang_league ll
WHERE ll.leagueId='{$row["_lid"]}'";
                $leagues = $db->createCommand($leaguesql)->queryRow();
                if (!empty($leagues)) {
                    //echo "update league from db\n";
                    $league = $this->removeSpecial($leagues);
                    $updatetl = "UPDATE `timelines_game` SET `competitionId`='{$league["competitionId"]}',`leagueName`='{$league["LeagueName"]}', `leagueNameEn`='{$league["LeagueNameEn"]}', `leagueNameTh`='{$league["LeagueNameTh"]}', `leagueNameBig`='{$league["LeagueNameBig"]}', `leagueNameGb`='{$league["LeagueNameGb"]}', `leagueNameKr`='{$league["LeagueNameKr"]}', `leagueNameVn`='{$league["LeagueNameVn"]}', `leagueNameLa`='{$league["LeagueNameLa"]}' WHERE  `id`='{$row["id"]}';";
                    $db->createCommand($updatetl)->execute();
                } else {
                    $leaguesql = "SELECT * FROM `league`
WHERE leagueId='{$row["_lid"]}'";
                    $leagues = $db->createCommand($leaguesql)->queryRow();
                    if (!empty($leagues)) {
                        $league = $this->removeSpecial($leagues);
                        $updatetl = "UPDATE `timelines_game` SET `competitionId`='{$league["competitionId"]}',`leagueName`='{$league["leagueName"]}', `leagueNameEn`='{$league["leagueName"]}', `leagueNameTh`='{$league["leagueName"]}', `leagueNameBig`='{$league["leagueName"]}', `leagueNameGb`='{$league["leagueName"]}', `leagueNameKr`='{$league["leagueName"]}', `leagueNameVn`='{$league["leagueName"]}', `leagueNameLa`='{$league["leagueName"]}' WHERE  `id`={$row["id"]};";
                        $db->createCommand($updatetl)->execute();
                        $leagues = null;
                    }
                }
            }

            if (array_key_exists((int)$row["hid"], $json["lang_team1"])) {
                $home = $this->removeSpecial($json["lang_team1"][$row["hid"]]);
                $updatetl = "UPDATE `timelines_game` SET `homeNameEn`='{$home["en"]}', `homeNameTh`='{$home["th"]}' WHERE  `id`={$row["id"]};";
                //echo "home from file\n";
                $db->createCommand($updatetl)->execute();
            } else {
                $htsq = "SELECT 
		ht.teamName HomeName,
      ht.teamNameEn HomeNameEn,
      ht.teamNameTh HomeNameTh,
      ht.teamNameBig HomeNameBig,
      ht.teamNameGb HomeNameGb,
      ht.teamNameKr HomeNameKr,
      ht.teamNameVn HomeNameVn,
      ht.teamNameLa HomeNameLa,
      ht.id7m      
FROM lang_team ht
WHERE ht.tid={$row["hid"]}";
                $homes = $db->createCommand($htsq)->queryRow();
                if (!empty($homes)) {
                    //echo "home from db\n";
                    $home = $this->removeSpecial($homes);
                    $updatetl = "UPDATE `timelines_game` SET `homeName`='{$home["HomeName"]}', `homeNameEn`='{$home["HomeNameEn"]}', `homeNameTh`='{$home["HomeNameTh"]}' WHERE  `id`={$row["id"]};";
                    $db->createCommand($updatetl)->execute();
                }
            }


            if (array_key_exists((int)$row["gid"], $json["lang_team2"])) {
                $away = $this->removeSpecial($json["lang_team2"][$row["gid"]]);
                $updatetl = "UPDATE `timelines_game` SET `awayNameEn`='{$away["en"]}', `awayNameTh`='{$away["th"]}' WHERE  `id`={$row["id"]};";
                //echo "away from file\n";
                $db->createCommand($updatetl)->execute();
            } else {
                $atsq = "SELECT 
		at.teamName AwayName,
      at.teamNameEn AwayNameEn,
      at.teamNameTh AwayNameTh,
      at.teamNameBig AwayNameBig,
      at.teamNameGb AwayNameGb,
      at.teamNameKr AwayNameKr,
      at.teamNameVn AwayNameVn,
      at.teamNameLa AwayNameLa,
      at.id7m
FROM lang_team at
WHERE at.tid={$row["gid"]}";
                $aways = $db->createCommand($atsq)->queryRow();
                if (!empty($aways)) {
                    //echo "away from db\n";
                    $away = $this->removeSpecial($aways);
                    $updatetl = "UPDATE `timelines_game` SET `awayName`='{$away["AwayName"]}', `awayNameEn`='{$away["AwayNameEn"]}', `awayNameTh`='{$away["AwayNameTh"]}' WHERE  `id`={$row["id"]};";
                    $db->createCommand($updatetl)->execute();
                }
            }

            $key = $row["_lid"] . "_" . $row["hid"] . "_" . $row["gid"];
            if (array_key_exists($key, $json["data"])) {
                //echo "odds from file\n";
                if (array_key_exists("SBOBET", $json["data"][$key])) {
                    $updatetl = "UPDATE `timelines_game` SET `hdp`='{$json["data"][$key]["SBOBET"]["hdp"]}', `hdp_home`='{$json["data"][$key]["SBOBET"]["hdp_home"]}', `hdp_away`='{$json["data"][$key]["SBOBET"]["hdp_away"]}', `type`='{$json["data"][$key]["SBOBET"]["type"]}', `match_id`='{$json["data"][$key]["SBOBET"]["match_id"]}' WHERE  `id`={$row["id"]};";
                    $db->createCommand($updatetl)->execute();
                } else if (array_key_exists("Bet365", $json["data"][$key])) {
                    $updatetl = "UPDATE `timelines_game` SET `hdp`='{$json["data"][$key]["Bet365"]["hdp"]}', `hdp_home`='{$json["data"][$key]["Bet365"]["hdp_home"]}', `hdp_away`='{$json["data"][$key]["Bet365"]["hdp_away"]}', `type`='{$json["data"][$key]["Bet365"]["type"]}', `match_id`='{$json["data"][$key]["Bet365"]["match_id"]}' WHERE  `id`={$row["id"]};";
                    $db->createCommand($updatetl)->execute();
                } else if (array_key_exists("Ladbrokes", $json["data"][$key])) {
                    $updatetl = "UPDATE `timelines_game` SET `hdp`='{$json["data"][$key]["Ladbrokes"]["hdp"]}', `hdp_home`='{$json["data"][$key]["Ladbrokes"]["hdp_home"]}', `hdp_away`='{$json["data"][$key]["Ladbrokes"]["hdp_away"]}', `type`='{$json["data"][$key]["Ladbrokes"]["type"]}', `match_id`='{$json["data"][$key]["Ladbrokes"]["match_id"]}' WHERE  `id`={$row["id"]};";
                    $db->createCommand($updatetl)->execute();
                }
            } else {
                if (!empty($leagues) && !empty($homes) && !empty($aways)) {
                    $oddssql = "
      SELECT 
      o7.hdp,
      o7.hdp_home,
      o7.hdp_away,
      o7.type,
      o7.match_id
FROM odds_7m o7
WHERE o7.league_id ='{$leagues["id7m"]}'
      AND
      o7.home_id ='{$homes["id7m"]}'
      AND
      o7.away_id ='{$aways["id7m"]}'"
                        . " ORDER BY FIELD(o7.type, 'SBOBET', 'BET365', 'Ladbrokes')
LIMIT 1";
                    $odds = $db->createCommand($oddssql)->queryRow();
                    ////echo $oddssql;
                    if (!empty($odds)) {
                        //echo "odds from db\n";
                        $updatetl = "UPDATE `timelines_game` SET `hdp`='{$odds["hdp"]}', `hdp_home`='{$odds["hdp_home"]}', `hdp_away`='{$odds["hdp_away"]}', `type`='{$odds["type"]}', `match_id`='{$odds["match_id"]}' WHERE  `id`={$row["id"]};";
                        $db->createCommand($updatetl)->execute();
                    }
                }
            }

            $hlogosql = "SELECT 
	   hlogos.32x32 h32x32,
      hlogos.64x64 h64x64,
      hlogos.256x256 h256x256
FROM team_logos hlogos
WHERE 
		hlogos.tid={$row["hid"]}";
            $hlogo = $db->createCommand($hlogosql)->queryRow();
            if (!empty($hlogo)) {
                $updatetl = "UPDATE `timelines_game` SET `h32x32`='{$hlogo["h32x32"]}', `h64x64`='{$hlogo["h64x64"]}', `h256x256`='{$hlogo["h256x256"]}' WHERE  `id`={$row["id"]};";
                $db->createCommand($updatetl)->execute();
            } else {
                $inslogo = "INSERT INTO `team_logos` (`tid`, `32x32`, `64x64`, `256x256`, `update_at`)  VALUES ('{$row["hid"]}', 'http://api.ssporting.com/teams_clean/team_default_32x32.png', 'http://api.ssporting.com/teams_clean/team_default_64x64.png', 'http://api.ssporting.com/teams_clean/team_default_256x256.png', '2014-04-04 06:55:07')";
                $db->createCommand($inslogo)->execute();
            }

            $alogosql = "SELECT 
	   alogos.32x32 a32x32,
      alogos.64x64 a64x64,
      alogos.256x256 a256x256
FROM team_logos alogos
WHERE 
		alogos.tid={$row["gid"]}";
            $alogo = $db->createCommand($alogosql)->queryRow();
            if (!empty($alogo)) {
                $updatetl = "UPDATE `timelines_game` SET `a32x32`='{$alogo["a32x32"]}', `a64x64`='{$alogo["a64x64"]}', `a256x256`='{$alogo["a256x256"]}' WHERE  `id`={$row["id"]};";
                $db->createCommand($updatetl)->execute();
            } else {
                $inslogo = "INSERT INTO `team_logos` (`tid`, `32x32`, `64x64`, `256x256`, `update_at`)  VALUES ('{$row["gid"]}', 'http://api.ssporting.com/teams_clean/team_default_32x32.png', 'http://api.ssporting.com/teams_clean/team_default_64x64.png', 'http://api.ssporting.com/teams_clean/team_default_256x256.png', '2014-04-04 06:55:07')";
                $db->createCommand($inslogo)->execute();
            }
        }
    }

    function removeSpecial($rs)
    {
        foreach ($rs as $key => $r) {
            $rs[$key] = mysql_escape_string($r);
        }
        return $rs;
    }

    /*
      public function run() {
      $db = Yii::app()->db;
      /*
      $sql = "INSERT INTO `timelines_game` (`mid`, `_lid`, `hid`, `gid`, `sid`, `competitionId`, `show_date`, `score`, `leagueName`, `leagueNameEn`, `leagueNameTh`, `leagueNameBig`, `leagueNameGb`, `leagueNameKr`, `leagueNameVn`, `leagueNameLa`, `homeName`, `homeNameEn`, `homeNameTh`, `homeNameBig`, `homeNameGb`, `homeNameKr`, `homeNameVn`, `homeNameLa`, `awayName`, `awayNameEn`, `awayNameTh`, `awayNameBig`, `awayNameGb`, `awayNameKr`, `awayNameVn`, `awayNameLa`, `hdp`, `hdp_home`, `hdp_away`, `type`, `match_id`, `h32x32`, `h64x64`, `h256x256`, `a32x32`, `a64x64`, `a256x256`)
      SELECT
      lm.mid,
      lm._lid,
      lm.hid,
      lm.gid,
      lm.sid,
      ll.competitionId,
      lm.date,
      lm.s1 score,
      ll.leagueName as LeagueName,
      ll.leagueNameEn as LeagueNameEn,
      ll.leagueNameTh as LeagueNameTh,
      ll.leagueNameBig LeagueNameBig,
      ll.leagueNameGb LeagueNameGb,
      ll.leagueNameKr LeagueNameKr,
      ll.leagueNameVn LeagueNameVn,
      ll.leagueNameLa LeagueNameLa,
      ht.teamName HomeName,
      ht.teamNameEn HomeNameEn,
      ht.teamNameTh HomeNameTh,
      ht.teamNameBig HomeNameBig,
      ht.teamNameGb HomeNameGb,
      ht.teamNameKr HomeNameKr,
      ht.teamNameVn HomeNameVn,
      ht.teamNameLa HomeNameLa,
      at.teamName AwayName,
      at.teamNameEn AwayNameEn,
      at.teamNameTh AwayNameTh,
      at.teamNameBig AwayNameBig,
      at.teamNameGb AwayNameGb,
      at.teamNameKr AwayNameKr,
      at.teamNameVn AwayNameVn,
      at.teamNameLa AwayNameLa,
      o7.hdp,
      o7.hdp_home,
      o7.hdp_away,
      o7.type,
      o7.match_id,
      hlogos.32x32 h32x32,
      hlogos.64x64 h64x64,
      hlogos.256x256 h256x256,
      alogos.32x32 a32x32,
      alogos.64x64 a64x64,
      alogos.256x256 a256x256
      FROM
      live_match lm
      LEFT JOIN
      lang_league ll
      ON lm._lid = ll.leagueId
      LEFT JOIN
      lang_team ht
      ON lm.hid = ht.tid
      LEFT JOIN
      lang_team at
      ON lm.gid = at.tid
      LEFT JOIN
      team_logos hlogos
      ON hlogos.tid=ht.tid
      LEFT JOIN
      team_logos alogos
      ON alogos.tid=at.tid
      LEFT JOIN
      odds_7m o7
      ON o7.league_id = ll.id7m
      AND
      o7.home_id = ht.id7m
      AND
      o7.away_id = at.id7m
      WHERE
      lm.mid NOT IN (
      SELECT
      mid
      FROM
      timelines_game
      )
      AND
      lm.sid <5
      AND
      o7.type = (
      CASE
      WHEN o7.type = 'SBOBET' IS NOT NULL
      THEN 'SBOBET'
      WHEN o7.type = 'Bet365' IS NOT NULL
      THEN 'Bet365'
      ELSE 'Ladbrokes'
      END
      )";

      $db->createCommand($sql)->execute();

      $sql = "SELECT
     * 
      FROM
      live_match
      WHERE
      sid <5";
      $livematches = $db->createCommand($sql)->queryAll();
      ////echo count($livematches) . "\n";
      foreach ($livematches as $key => $livematch) {
      //            $gamesql = "
      //                SELECT
      //                    lm.mid,
      //                    lm._lid,
      //                    lm.hid,
      //                    lm.gid,
      //                    lm.sid,
      //                    ll.competitionId,
      //                    lm.date,
      //                    lm.s1 score,
      //                    ll.leagueName as LeagueName,
      //                    ll.leagueNameEn as LeagueNameEn,
      //                    ll.leagueNameTh as LeagueNameTh,
      //                    ll.leagueNameBig LeagueNameBig,
      //                    ll.leagueNameGb LeagueNameGb,
      //                    ll.leagueNameKr LeagueNameKr,
      //                    ll.leagueNameVn LeagueNameVn,
      //                    ll.leagueNameLa LeagueNameLa,
      //                    ht.teamName HomeName,
      //                    ht.teamNameEn HomeNameEn,
      //                    ht.teamNameTh HomeNameTh,
      //                    ht.teamNameBig HomeNameBig,
      //                    ht.teamNameGb HomeNameGb,
      //                    ht.teamNameKr HomeNameKr,
      //                    ht.teamNameVn HomeNameVn,
      //                    ht.teamNameLa HomeNameLa,
      //                    at.teamName AwayName,
      //                    at.teamNameEn AwayNameEn,
      //                    at.teamNameTh AwayNameTh,
      //                    at.teamNameBig AwayNameBig,
      //                    at.teamNameGb AwayNameGb,
      //                    at.teamNameKr AwayNameKr,
      //                    at.teamNameVn AwayNameVn,
      //                    at.teamNameLa AwayNameLa,
      //                    IfNull(o7.hdp,0) hdp,
      //                    IfNull(o7.hdp_home,0) hdp_home,
      //                    IfNull(o7.hdp_away,0) hdp_away,
      //                    IfNull(o7.type,0) type,
      //                    IfNull(o7.match_id,0) match_id,
      //                    hlogos.32x32 h32x32,
      //                    hlogos.64x64 h64x64,
      //                    hlogos.256x256 h256x256,
      //                    alogos.32x32 a32x32,
      //                    alogos.64x64 a64x64,
      //                    alogos.256x256 a256x256
      //                FROM
      //                    live_match lm
      //                        LEFT JOIN
      //                            lang_league ll
      //                            ON lm._lid = ll.leagueId
      //                        LEFT JOIN
      //                            lang_team ht
      //                            ON lm.hid = ht.tid
      //                        LEFT JOIN
      //                            lang_team at
      //                            ON lm.gid = at.tid
      //                        LEFT JOIN
      //                            team_logos hlogos
      //                            ON hlogos.tid=ht.tid
      //                        LEFT JOIN
      //                            team_logos alogos
      //                            ON alogos.tid=at.tid
      //                        LEFT JOIN
      //                            odds_7m o7
      //                            ON o7.league_id = ll.id7m
      //                            AND
      //                                o7.home_id = ht.id7m
      //                            AND
      //                                o7.away_id = at.id7m
      //                WHERE
      //                    lm.mid ='{$livematch["mid"]}'
      //                    AND
      //                        o7.type = (
      //                            CASE
      //                                WHEN o7.type = 'SBOBET' IS NOT NULL
      //                                    THEN 'SBOBET'
      //                                WHEN o7.type = 'Bet365' IS NOT NULL
      //                                    THEN 'Bet365'
      //                                ELSE 'Ladbrokes'
      //                            END
      //                            )";

      $gamesql = "INSERT IGNORE INTO `timelines_game` (`mid`, `_lid`, `hid`, `gid`, `sid`, `competitionId`, `show_date`, `score`, `leagueName`, `leagueNameEn`, `leagueNameTh`, `leagueNameBig`, `leagueNameGb`, `leagueNameKr`, `leagueNameVn`, `leagueNameLa`, `homeName`, `homeNameEn`, `homeNameTh`, `homeNameBig`, `homeNameGb`, `homeNameKr`, `homeNameVn`, `homeNameLa`, `awayName`, `awayNameEn`, `awayNameTh`, `awayNameBig`, `awayNameGb`, `awayNameKr`, `awayNameVn`, `awayNameLa`, `hdp`, `hdp_home`, `hdp_away`, `type`, `match_id`, `h32x32`, `h64x64`, `h256x256`, `a32x32`, `a64x64`, `a256x256`)
      SELECT
      lm.mid,
      lm._lid,
      lm.hid,
      lm.gid,
      lm.sid,
      ll.competitionId,
      lm.date,
      lm.s1 score,
      ll.leagueName as LeagueName,
      ll.leagueNameEn as LeagueNameEn,
      ll.leagueNameTh as LeagueNameTh,
      ll.leagueNameBig LeagueNameBig,
      ll.leagueNameGb LeagueNameGb,
      ll.leagueNameKr LeagueNameKr,
      ll.leagueNameVn LeagueNameVn,
      ll.leagueNameLa LeagueNameLa,
      ht.teamName HomeName,
      ht.teamNameEn HomeNameEn,
      ht.teamNameTh HomeNameTh,
      ht.teamNameBig HomeNameBig,
      ht.teamNameGb HomeNameGb,
      ht.teamNameKr HomeNameKr,
      ht.teamNameVn HomeNameVn,
      ht.teamNameLa HomeNameLa,
      at.teamName AwayName,
      at.teamNameEn AwayNameEn,
      at.teamNameTh AwayNameTh,
      at.teamNameBig AwayNameBig,
      at.teamNameGb AwayNameGb,
      at.teamNameKr AwayNameKr,
      at.teamNameVn AwayNameVn,
      at.teamNameLa AwayNameLa,
      o7.hdp,
      o7.hdp_home,
      o7.hdp_away,
      o7.type,
      o7.match_id,
      hlogos.32x32 h32x32,
      hlogos.64x64 h64x64,
      hlogos.256x256 h256x256,
      alogos.32x32 a32x32,
      alogos.64x64 a64x64,
      alogos.256x256 a256x256
      FROM
      live_match lm
      LEFT JOIN
      lang_league ll
      ON lm._lid = ll.leagueId
      LEFT JOIN
      lang_team ht
      ON lm.hid = ht.tid
      LEFT JOIN
      lang_team at
      ON lm.gid = at.tid
      LEFT JOIN
      team_logos hlogos
      ON hlogos.tid=ht.tid
      LEFT JOIN
      team_logos alogos
      ON alogos.tid=at.tid
      LEFT JOIN
      odds_7m o7
      ON o7.league_id = ll.id7m
      AND
      o7.home_id = ht.id7m
      AND
      o7.away_id = at.id7m
      WHERE
      lm.mid ={$livematch["mid"]}";
      // //echo $gamesql;

      $db->createCommand($gamesql)->execute();
      //$tmpgames = $db->createCommand($gamesql)->queryAll();
      ////echo $key;
      //var_dump($tmpgames);
      //            if (!empty($tmpgames[0])) {
      //                $insertsql = "INSERT INTO `timelines_game` (`mid`, `_lid`, `hid`, `gid`, `sid`, `competitionId`, `show_date`, `score`, `leagueName`, `leagueNameEn`, `leagueNameTh`, `leagueNameBig`, `leagueNameGb`, `leagueNameKr`, `leagueNameVn`, `leagueNameLa`, `homeName`, `homeNameEn`, `homeNameTh`, `homeNameBig`, `homeNameGb`, `homeNameKr`, `homeNameVn`, `homeNameLa`, `awayName`, `awayNameEn`, `awayNameTh`, `awayNameBig`, `awayNameGb`, `awayNameKr`, `awayNameVn`, `awayNameLa`, `hdp`, `hdp_home`, `hdp_away`, `type`, `match_id`, `h32x32`, `h64x64`, `h256x256`, `a32x32`, `a64x64`, `a256x256`) VALUES ('{$tmpgames[0]["mid"]}', '{$tmpgames[0]["_lid"]}', '{$tmpgames[0]["hid"]}', '{$tmpgames[0]["gid"]}', '{$tmpgames[0]["sid"]}', '{$tmpgames[0]["competitionId"]}', '{$tmpgames[0]["date"]}', '{$tmpgames[0]["score"]}', '{$tmpgames[0]["LeagueName"]}', '{$tmpgames[0]["LeagueNameEn"]}', '{$tmpgames[0]["LeagueNameTh"]}', '{$tmpgames[0]["LeagueNameBig"]}', '{$tmpgames[0]["LeagueNameGb"]}', '{$tmpgames[0]["LeagueNameKr"]}', '{$tmpgames[0]["LeagueNameVn"]}', '{$tmpgames[0]["LeagueNameLa"]}', '{$tmpgames[0]["HomeName"]}', '{$tmpgames[0]["HomeNameEn"]}', '{$tmpgames[0]["HomeNameTh"]}', '{$tmpgames[0]["HomeNameBig"]}', '{$tmpgames[0]["HomeNameGb"]}', '{$tmpgames[0]["HomeNameKr"]}', '{$tmpgames[0]["HomeNameVn"]}', '{$tmpgames[0]["HomeNameLa"]}', '{$tmpgames[0]["AwayName"]}', '{$tmpgames[0]["AwayNameEn"]}', '{$tmpgames[0]["AwayNameTh"]}', '{$tmpgames[0]["AwayNameBig"]}', '{$tmpgames[0]["AwayNameGb"]}', '{$tmpgames[0]["AwayNameKr"]}', '{$tmpgames[0]["AwayNameVn"]}', '{$tmpgames[0]["AwayNameLa"]}', '{$tmpgames[0]["hdp"]}', '{$tmpgames[0]["hdp_home"]}', '{$tmpgames[0]["hdp_away"]}', '{$tmpgames[0]["type"]}', '{$tmpgames[0]["match_id"]}', '{$tmpgames[0]["h32x32"]}', '{$tmpgames[0]["h64x64"]}', '{$tmpgames[0]["h256x256"]}', '{$tmpgames[0]["a32x32"]}', '{$tmpgames[0]["a64x64"]}', '{$tmpgames[0]["a256x256"]}') ON DUPLICATE KEY UPDATE `hdp`='{$tmpgames[0]["hdp"]}', `hdp_home`='{$tmpgames[0]["hdp_home"]}', `hdp_away`='{$tmpgames[0]["hdp_away"]}'";
      //                $db->createCommand($insertsql)->execute();
      //                ////echo "Got data\n";
      //            }
      }
      }
     */
}
