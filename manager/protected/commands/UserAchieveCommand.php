<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserAchieveCommand
 *
 * @author mrsyrop
 */
class UserAchieveCommand extends CConsoleCommand {

    public function run() {
        $db = Yii::app()->db;
        $week = date("W");
        $firstmonday = date("Y-m-d", strtotime('Monday this week'));
        $nowday = date("Y-m-d");
        $nowdate = date("Y-m-d H:i:s");
        $twoweekago = date("Y-m-d", strtotime("-14day"));
        $lasttwoweek = date('Y-m-d', strtotime($nowday . 'last sunday'));
        $nowyear = date("Y");
        $week = (int) $week;
        if ($week % 2 != 0 && $firstmonday == $nowday) {
            $allsql = "SELECT *,overall_gp focus_gp,user_status as displayname FROM facebook_user ORDER BY overall_gp DESC LIMIT 10";
            $rankall = $db->createCommand($allsql)->queryAll();
            foreach ($rankall as $key => $ra) {
                $sequence = $key + 1;
                $trophysql = "SELECT * FROM trophy WHERE type='all' AND sequence=$sequence";
                $trophy = $db->createCommand($trophysql)->queryAll();
                $allinsert = "INSERT INTO `user_achieve` (`fb_uid`, `trophy_id`, `earn_at`) VALUES ('{$ra["fb_uid"]}', {$trophy[0]["id"]}, '$nowdate');";
                $db->createCommand($allinsert)->execute();
            }


            $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us 
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at BETWEEN '$twoweekago' AND DATE(NOW())
GROUP BY us.fb_uid
ORDER BY (CASE WHEN SUM(daily_change)<>0.0 then 0 ELSE 1 END),SUM(daily_change) DESC LIMIT 10";
            $rank14 = $db->createCommand($last14daysql)->queryAll();
            foreach ($rank14 as $key => $ra) {
                $sequence = $key + 1;
                $trophysql = "SELECT * FROM trophy WHERE type='14d' AND sequence=$sequence";
                $trophy = $db->createCommand($trophysql)->queryAll();
                $insert14 = "INSERT INTO `user_achieve` (`fb_uid`, `trophy_id`, `earn_at`) VALUES ('{$ra["fb_uid"]}', {$trophy[0]["id"]}, '$nowdate');";
                $db->createCommand($insert14)->execute();
            }

            $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us 
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at ='$lasttwoweek'
ORDER BY us.overall_gp DESC LIMIT 10";
            $rank2w = $db->createCommand($allsql)->queryAll();
            foreach ($rank2w as $key => $ra) {
                $sequence = $key + 1;
                $trophysql = "SELECT * FROM trophy WHERE type='2w' AND sequence=$sequence";
                $trophy = $db->createCommand($trophysql)->queryAll();
                $insert2w = "INSERT INTO `user_achieve` (`fb_uid`, `trophy_id`, `earn_at`) VALUES ('{$ra["fb_uid"]}', {$trophy[0]["id"]}, '$nowdate');";
                $db->createCommand($insert2w)->execute();
            }

            $r100sql = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
ORDER BY DATE(ur.update_at) DESC, ur.last_100 DESC,ur.w100 DESC,ur.wh100 DESC LIMIT 10";
            $r100 = $db->createCommand($r100sql)->queryAll();
            foreach ($r100 as $key => $ra) {
                $sequence = $key + 1;
                $trophysql = "SELECT * FROM trophy WHERE type='r100' AND sequence=$sequence";
                $trophy = $db->createCommand($trophysql)->queryAll();
                $insertr100 = "INSERT INTO `user_achieve` (`fb_uid`, `trophy_id`, `earn_at`) VALUES ('{$ra["fb_uid"]}', {$trophy[0]["id"]}, '$nowdate');";
                $db->createCommand($insertr100)->execute();
            }

            $r200sql = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
ORDER BY DATE(ur.update_at) DESC, ur.last_200 DESC, ur.last_100 DESC LIMIT 10";
            $r200 = $db->createCommand($r200sql)->queryAll();
            foreach ($r200 as $key => $ra) {
                $sequence = $key + 1;
                $trophysql = "SELECT * FROM trophy WHERE type='r200' AND sequence=$sequence";
                $trophy = $db->createCommand($trophysql)->queryAll();
                $insertr200 = "INSERT INTO `user_achieve` (`fb_uid`, `trophy_id`, `earn_at`) VALUES ('{$ra["fb_uid"]}', {$trophy[0]["id"]}, '$nowdate');";
                $db->createCommand($insertr200)->execute();
            }
        }
    }

}
