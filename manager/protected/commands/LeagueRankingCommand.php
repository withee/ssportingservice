<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LeagueRankingCommand
 *
 * @author mrsyrop
 */
class LeagueRankingCommand extends CConsoleCommand {

    public function run() {
        $db = Yii::app()->db;
        $league = array('34885', '35350', '35351', '35030', '34991');
        $json = json_decode(file_get_contents("../system_config/league.json"), true);
        $league = $json['lid'];
        foreach ($league as $lid) {

            $betsql = "SELECT b.fb_uid, SUM( b.amount*b.mul) AS gp, COUNT( b.betId ) AS play, fb.pts
FROM bet b
LEFT JOIN facebook_user fb ON b.fb_uid = fb.fb_uid
WHERE b.leagueId = '$lid'
GROUP BY b.fb_uid
ORDER BY gp DESC";
            $betlist = $db->createCommand($betsql)->queryAll();
            $user = array();
            $daynow = date("Y-m-d");
            $yesterday = date("Y-m-d", strtotime($daynow . "-1 day"));

            foreach ($betlist as $bet) {
                echo $bet["fb_uid"] . ":" . $bet["gp"] . "\n";
                $dupsql = "SELECT * FROM `league_ranking` WHERE `fb_uid`='{$bet['fb_uid']}' AND `_lid`='$lid' AND stamp_at='$daynow'";
                $todaystmp = $db->createCommand($dupsql)->queryAll();

                if ($todaystmp) {
                    
                } else {
                    $inssql = "INSERT INTO `league_ranking` (`_lid`,`fb_uid`,`pts`, `stamp_gp`, `changed_gp`, `overall_gp`,`play`, `stamp_at`, `update_at`, `season`) VALUES ('$lid','{$bet['fb_uid']}','{$bet['pts']}', '{$bet['gp']}', 0, 0,'{$bet['play']}',DATE(NOW()),'0000-00-00',YEAR(NOW()));";
                    $db->createCommand($inssql)->execute();
                }

                //$ntime = date('H');
                // if ($ntime == 5) {
                $ytdsql = "SELECT * FROM `league_ranking` WHERE fb_uid='{$bet['fb_uid']}' AND `_lid`='$lid' AND stamp_at='$yesterday'";
                $yesterdaystmp = $db->createCommand($ytdsql)->queryAll();
                if ($yesterdaystmp) {
                    $gpchange = floatval($bet['gp']) - floatval($yesterdaystmp[0]['stamp_gp']);
                    $updatesql = "UPDATE `league_ranking` SET `changed_gp`=$gpchange, `overall_gp`={$bet['gp']}, `update_at`=DATE(NOW()),`play`='{$bet['play']}',`pts`='{$bet['pts']}' WHERE  `id`='{$yesterdaystmp[0]['id']}'";
                    $db->createCommand($updatesql)->execute();
                }
                //  }
            }

//        $nHour = date("H");
//        if ($nHour == 5) {
            $stsql = "SELECT * FROM league_ranking WHERE update_at='$daynow' AND `_lid`='$lid' ORDER BY overall_gp DESC, pts DESC, play DESC";
            $stat = $db->createCommand($stsql)->queryAll();
            foreach ($stat as $key => $st) {
                $oldsql = "SELECT * FROM league_ranking WHERE fb_uid='{$st['fb_uid']}' AND `_lid`='$lid' AND update_at='$yesterday'";
                $oldstat = $db->createCommand($oldsql)->queryAll();
                $oldrank = 0;
                $crank = $key + 1;
                $rdiff = 0;
                if ($oldstat) {
                    $oldrank = (int) $oldstat[0]['rank'];
                }
                if ($oldrank == 0) {
                    $rdiff = $crank;
                } else {
                    $rdiff = $oldrank - $crank;
                }
                $updaterank = "UPDATE `league_ranking` SET `rank`=$crank, `rank_change`=$rdiff WHERE  `id`={$st['id']};";
                $db->createCommand($updaterank)->execute();
            }
            // }
        }
    }

}
