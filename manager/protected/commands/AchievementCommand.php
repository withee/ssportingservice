<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AchievementCommand
 *
 * @author mrsyrop
 */
class AchievementCommand extends CConsoleCommand {

    public function run() {
        $nowday = date("Y-m-d");
        //$nowday = "2014-08-16";
        $criteria = new CDbCriteria();
        $criteria->condition = "update_at='$nowday'";
        $criteria->order = "all_rank";
        $criteria->limit = 10;
        $allranks = Gpstatistic::model()->findAll($criteria);
        foreach ($allranks as $rank) {
            echo $rank->rank . ":" . $rank->fb_uid . "\n";
            $tropy = Trophy::model()->find("type='All Ranking' AND sequence='{$rank->all_rank}'");
            $arc = new Achieve();
            $arc->fb_uid = $rank->fb_uid;
            $arc->trophy_id = $tropy->id;
            $arc->earn_at = date("Y-m-d");
            $arc->save();
        }

        $criteria = new CDbCriteria();
        $criteria->condition = "update_at='$nowday'";
        $criteria->order = "rank";
        $criteria->limit = 10;
        $allranks = Gpstatistic::model()->findAll($criteria);
        foreach ($allranks as $rank) {
            echo $rank->rank . ":" . $rank->fb_uid . "\n";
            $tropy = Trophy::model()->find("type='Last Champ Ranking' AND sequence='{$rank->rank}'");
            $arc = new Achieve();
            $arc->fb_uid = $rank->fb_uid;
            $arc->trophy_id = $tropy->id;
            $arc->earn_at = date("Y-m-d");
            $arc->save();
        }


        $criteria = new CDbCriteria();
        $criteria->condition = "update_at='$nowday'";
        $criteria->order = "all_sgold_rank";
        $criteria->limit = 10;
        $allranks = Sgoldstatistic::model()->findAll($criteria);
        foreach ($allranks as $rank) {
            echo $rank->rank . ":" . $rank->fb_uid . "\n";
            $tropy = Trophy::model()->find("type='All Sgold' AND sequence='{$rank->all_sgold_rank}'");
            $arc = new Achieve();
            $arc->fb_uid = $rank->fb_uid;
            $arc->trophy_id = $tropy->id;
            $arc->earn_at = date("Y-m-d");
            $arc->save();
        }


        $criteria = new CDbCriteria();
        $criteria->condition = "update_at='$nowday'";
        $criteria->order = "rank";
        $criteria->limit = 10;
        $allranks = Sgoldstatistic::model()->findAll($criteria);
        foreach ($allranks as $rank) {
            echo $rank->rank . ":" . $rank->fb_uid . "\n";
            $tropy = Trophy::model()->find("type='Last Champ Sgold' AND sequence='{$rank->rank}'");
            $arc = new Achieve();
            $arc->fb_uid = $rank->fb_uid;
            $arc->trophy_id = $tropy->id;
            $arc->earn_at = date("Y-m-d");
            $arc->save();
        }
    }

}
