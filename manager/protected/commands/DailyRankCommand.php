<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DailyRankSetCommand
 *
 * @author withee
 */
class DailyRankCommand extends CConsoleCommand {

    public function run($args) {
        $this->Setscore();
        $this->Setlike();
        $this->SetsNewbies();
    }

    function Setscore() {
        $db = Yii::app()->db;
        $week = date("W");
        $week4 = date("W");
        $year = date("Y");
        if ($week % 2 != 0) {
            $week++;
        }
        if ($week4 % 4 != 0) {
            $week4 = $week4 + (4 - ($week4 % 4));
        }
        $user_sql = "
SELECT r14s.* FROM ranking14_score r14s
LEFT JOIN facebook_user fb ON fb.fb_uid=r14s.fb_uid
WHERE week=$week 
ORDER BY (CASE WHEN r14s.gp_change =0 then 1 ELSE 0 END),r14s.gp_change DESC, fb.gp DESC
        ";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking14_score` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }


        $user_sql = "SELECT r28s.* FROM ranking28_score r28s
LEFT JOIN facebook_user fb  ON fb.fb_uid=r28s.fb_uid
WHERE week=$week4
ORDER BY (CASE WHEN r28s.gp_change =0 then 1 ELSE 0 END), r28s.gp_change DESC, fb.gp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking28_score` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }

        $user_sql = "SELECT rs.* FROM rankingall_score rs
LEFT JOIN facebook_user fb ON fb.fb_uid=rs.fb_uid
WHERE year=$year
ORDER BY fb.gp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `rankingall_score` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }
    }

    function SetsNewbies() {
        $db = Yii::app()->db;
        $week = date("W");
        $week4 = date("W");
        $year = date("Y");
        if ($week % 2 != 0) {
            $week++;
        }
        if ($week4 % 4 != 0) {
            $week4 = $week4 + (4 - ($week4 % 4));
        }
        $user_sql = "
SELECT r14s.* FROM ranking14_newbies r14s
LEFT JOIN facebook_user fb ON fb.fb_uid=r14s.fb_uid
WHERE week=$week 
ORDER BY (CASE WHEN r14s.gp_change =0 then 1 ELSE 0 END),r14s.gp_change DESC, fb.gp DESC
        ";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking14_newbies` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }


        $user_sql = "SELECT r28s.* FROM ranking28_newbies r28s
LEFT JOIN facebook_user fb  ON fb.fb_uid=r28s.fb_uid
WHERE week=$week4
ORDER BY (CASE WHEN r28s.gp_change =0 then 1 ELSE 0 END), r28s.gp_change DESC, fb.gp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking28_newbies` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }

        $user_sql = "SELECT rs.* FROM rankingall_newbies rs
LEFT JOIN facebook_user fb ON fb.fb_uid=rs.fb_uid
WHERE year=$year
ORDER BY fb.gp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `rankingall_newbies` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }
    }

    function Setlike() {
        $db = Yii::app()->db;
        $week = date("W");
        $week4 = date("W");
        $year = date("Y");
        if ($week % 2 != 0) {
            $week++;
        }
        if ($week4 % 4 != 0) {
            $week4 = $week4 + (4 - ($week4 % 4));
        }
        $user_sql = "
SELECT r14s.* FROM ranking14_like r14s
LEFT JOIN facebook_user fb ON fb.fb_uid=r14s.fb_uid
WHERE week=$week 
ORDER BY (CASE WHEN r14s.like_change =0 then 1 ELSE 0 END),r14s.like_change DESC, r14s.like_stamp DESC
        ";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking14_like` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }


        $user_sql = "SELECT r28s.* FROM ranking28_like r28s
LEFT JOIN facebook_user fb  ON fb.fb_uid=r28s.fb_uid
WHERE week=$week4
ORDER BY (CASE WHEN r28s.like_change =0 then 1 ELSE 0 END), r28s.like_change DESC, r28s.like_stamp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `ranking28_like` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }

        $user_sql = "SELECT rs.* FROM rankingall_like rs
LEFT JOIN facebook_user fb ON fb.fb_uid=rs.fb_uid
WHERE year=$year
ORDER BY (CASE WHEN rs.like_change =0 then 1 ELSE 0 END), rs.like_change DESC, rs.like_stamp DESC";
        $user_list = $db->createCommand($user_sql)->queryAll();
        foreach ($user_list as $key => $user) {
            $rank = $key + 1;
            $sql_set = "UPDATE `rankingall_like` SET `rank`=$rank WHERE  `id`={$user['id']};";
            $pass = $db->createCommand($sql_set)->execute();
        }
    }

}

?>
