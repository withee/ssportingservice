<?php

/**
 * Created by PhpStorm.
 * User: Withee
 * Date: 07-Sep-15
 * Time: 16:59
 */
require "../../Carbon/src/Carbon/Carbon.php";
use Carbon\Carbon;
class WriteLiveFileCommand extends CConsoleCommand
{
    public function run($args)
    {
        date_default_timezone_set('Asia/Bangkok');
        $db = Yii::app()->db;
        $nation = ["Th", "En"];
        $json = array();
        $liveMatchArray = array();
        $jsonArrLm = array();
        $liveLeagueArray = array();
        $jsonArrLl = array();
        $teamlogos = array();


        $countLiveMatchOnly = 0;
        $countLiveMatchWaitOnly = 0;
        $countLiveMatchResultOnly = 0;
        $countYesterdayResultOnly = 0;


        $dtstart = Carbon::now();
        $functionstartsql="INSERT INTO connection_log (funcname,name,status,source) VALUES ('writelivefile','CConsoleCommand','connect','php')";
        $db->createCommand($functionstartsql)->execute();

        $countLiveMatchWaitOnly = file_get_contents("../swcp/dist/file/sumWait.txt");
        $countLiveMatchOnly = file_get_contents("../swcp/dist/file/sumLive.txt");
        $countLiveMatchResultOnly = file_get_contents("../swcp/dist/file/sumResult.txt");
        $countYesterdayResultOnly = file_get_contents("../swcp/dist/file/sumYesterday.txt");

        echo $countLiveMatchOnly . "\n";
        echo $countLiveMatchWaitOnly . "\n";
        echo $countLiveMatchResultOnly . "\n";
        echo $countYesterdayResultOnly . "\n";

        $count = array();
        $count["live"] = (int)$countLiveMatchOnly;
        $count["wait"] = (int)$countLiveMatchWaitOnly;
        $count["result"] = (int)$countLiveMatchResultOnly;
        $count["yesterday"] = (int)$countYesterdayResultOnly;

        // var_dump($count);

        $query1 = "select  live_match.*,gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,ht.teamNameTh as hnTh,ht.teamNameEn as hnEn
FROM live_match
left join lang_team  as gt on gt.tid= live_match.gid
left join lang_team  as ht on ht.tid = live_match.hid
where showDate=current_date and new='Y' and (sid=1) group by mid order by date ASC,mid ASC";
        $rslm = $db->createCommand($query1)->queryAll();
//echo count($rslm);
        foreach ($rslm as $key => $val) {
            $rslm[$key]['gn'] = (empty($val['gnTh'])) ? $rslm[$key]['gn'] : $rslm[$key]['gnTh'];
            $rslm[$key]['hn'] = (empty($val['hnTh'])) ? $rslm[$key]['hn'] : $rslm[$key]['hnTh'];
            $jsonArrLm[0][] = $rslm[$key];
            $rslm[$key]['gn'] = (empty($val['gnEn'])) ? $rslm[$key]['gn'] : $rslm[$key]['gnEn'];
            $rslm[$key]['hn'] = (empty($val['hnEn'])) ? $rslm[$key]['hn'] : $rslm[$key]['hnEn'];
            $jsonArrLm[1][] = $rslm[$key];


            $logohomesql = "SELECT * FROM team_logos WHERE tid=" . $val['hid'];
            $rshomelogos = $db->createCommand($logohomesql)->queryRow();
            if (empty($rshomelogos)) {
                $rshomelogos["id"] = 0;
                $rshomelogos["tid"] = $val['hid'];
                $rshomelogos["32x32"] = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $rshomelogos["64x64"] = "http://api.ssporting.com/teams_clean/team_default_64x64.png";
                $rshomelogos["256x256"] = "http://api.ssporting.com/teams_clean/team_default_256x256.png";
                $rshomelogos["update_at"] = date('Y-m-d H:i:s');
            }
            $teamlogos[$rshomelogos['tid']] = $rshomelogos;


            $logoawaysql = "SELECT * FROM team_logos WHERE tid=" . $val['gid'];
            $rsawaylogos = $db->createCommand($logoawaysql)->queryRow();
            if (empty($rsawaylogos)) {
                $rsawaylogos["id"] = 0;
                $rsawaylogos["tid"] = $val['hid'];
                $rsawaylogos["32x32"] = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $rsawaylogos["64x64"] = "http://api.ssporting.com/teams_clean/team_default_64x64.png";
                $rsawaylogos["256x256"] = "http://api.ssporting.com/teams_clean/team_default_256x256.png";
                $rsawaylogos["update_at"] = date('Y-m-d H:i:s');
            }
            $teamlogos[$rsawaylogos['tid']] = $rsawaylogos;
        }
        $liveMatchArray = $rslm;
        //echo count($liveMatchArray); exit;
        //var_dump($teamlogos);

        $query2 = "select live_league.competitionId,live_league.subleagueId,live_league.subleagueName,live_league.ln,live_league.lnk,live_league.fg,live_league.bg,live_league.kn,live_league.kkod,
lang_league.leagueName,lang_league.leagueNameTh,lang_league.leagueNameEn,
lang_competition.comName,lang_competition.comNameTh,lang_competition.comNameEn,
league.priority from live_league
left join lang_league on lang_league.leagueId = live_league.leagueId
left join lang_competition on  lang_competition.comName = live_league.kn
left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId
left join league on league.leagueId = live_league.leagueId
where live_league.date=current_date and live_league.new='Y' and (live_match.sid=1)
group by live_league.subleagueId
order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC
   ";
        $rsll = $db->createCommand($query2)->queryAll();
        foreach ($rsll as $key => $ll) {
            $rsll[$key]["ln"] = empty($ll["leagueNameTh"]) ? $ll["ln"] : $ll["leagueNameTh"];
            $rsll[$key]['kn'] = empty($ll["comNameTh"]) ? $ll['kn'] : $ll["comNameTh"];
            $jsonArrLl[0][] = $rsll[$key];
            $rsll[$key]["ln"] = empty($ll["leagueNameEn"]) ? $ll["ln"] : $ll["leagueNameEn"];
            $rsll[$key]['kn'] = empty($ll["comNameEn"]) ? $ll['kn'] : $ll["comNameEn"];
            $jsonArrLl[1][] = $rsll[$key];
        }
        $liveLeagueArray = $rsll;

        $nowday = Carbon::now();
        $json['Teamlogos']=$teamlogos;
        $json['live_match']=$liveMatchArray;
        $json['live_league']=$liveLeagueArray;
        $json['count']=$count;
        $json['c3']=(int)$nowday->timestamp;
        $json['writer']='php';

        for ( $i = 0; $i < count($nation); $i++) {
        //json.put("live_match", jsonArrLm[i]);
            $json['live_match']=$jsonArrLm[$i];
        //        json.put("live_league", jsonArrLl[i]);
            $json['live_league']=$jsonArrLl[$i];
                if ((count($jsonArrLm[$i]) > 0 && count($jsonArrLl[$i]) > 0) || (empty($jsonArrLm[$i]) && empty($jsonArrLl[$i]))) {
            //System.out.println(nation[i]);
           // FileController.writeFile("../../cronjob_gen_file/files/" + nation[i].toLowerCase() + "/liveMatchWaitOnly.json", json.toString());
                    $filepath = "../cronjob_gen_file/files/" . strtolower($nation[$i]) . "/liveMatchWaitOnly.json";
                    file_put_contents($filepath,json_encode($json));
                }
            }

/////////===========New File=========
        $json = array();
        $liveMatchArray = array();
        $jsonArrLm = array();
        $liveLeagueArray = array();
        $jsonArrLl = array();
        $teamlogos = array();

        $query1 = "select  live_match.*,
gt.teamNameTh as gnTh,gt.teamNameEn as gnEn,
ht.teamNameTh as hnTh,ht.teamNameEn as hnEn
FROM live_match
left join lang_team  as gt on gt.tid = live_match.gid
left join lang_team  as ht on ht.tid = live_match.hid
where showDate=current_date and new='Y' and (sid>=5) and lnr=2 group by mid order by date ASC,mid ASC";
        $rslm = $db->createCommand($query1)->queryAll();
        foreach ($rslm as $key => $val) {
            $rslm[$key]['gn'] = (empty($val['gnTh'])) ? $rslm[$key]['gn'] : $rslm[$key]['gnTh'];
            $rslm[$key]['hn'] = (empty($val['hnTh'])) ? $rslm[$key]['hn'] : $rslm[$key]['hnTh'];
            $jsonArrLm[0][] = $rslm[$key];
            $rslm[$key]['gn'] = (empty($val['gnEn'])) ? $rslm[$key]['gn'] : $rslm[$key]['gnEn'];
            $rslm[$key]['hn'] = (empty($val['hnEn'])) ? $rslm[$key]['hn'] : $rslm[$key]['hnEn'];
            $jsonArrLm[1][] = $rslm[$key];


            $logohomesql = "SELECT * FROM team_logos WHERE tid=" . $val['hid'];
            $rshomelogos = $db->createCommand($logohomesql)->queryRow();
            if (empty($rshomelogos)) {
                $rshomelogos["id"] = 0;
                $rshomelogos["tid"] = $val['hid'];
                $rshomelogos["32x32"] = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $rshomelogos["64x64"] = "http://api.ssporting.com/teams_clean/team_default_64x64.png";
                $rshomelogos["256x256"] = "http://api.ssporting.com/teams_clean/team_default_256x256.png";
                $rshomelogos["update_at"] = date('Y-m-d H:i:s');
            }
            $teamlogos[$rshomelogos['tid']] = $rshomelogos;


            $logoawaysql = "SELECT * FROM team_logos WHERE tid=" . $val['gid'];
            $rsawaylogos = $db->createCommand($logoawaysql)->queryRow();
            if (empty($rsawaylogos)) {
                $rsawaylogos["id"] = 0;
                $rsawaylogos["tid"] = $val['hid'];
                $rsawaylogos["32x32"] = "http://api.ssporting.com/teams_clean/team_default_32x32.png";
                $rsawaylogos["64x64"] = "http://api.ssporting.com/teams_clean/team_default_64x64.png";
                $rsawaylogos["256x256"] = "http://api.ssporting.com/teams_clean/team_default_256x256.png";
                $rsawaylogos["update_at"] = date('Y-m-d H:i:s');
            }
            $teamlogos[$rsawaylogos['tid']] = $rsawaylogos;
        }
        $liveMatchArray = $rslm;

        //var_dump($teamlogos);

        $query2 = "select live_league.competitionId,live_league.subleagueId,live_league.subleagueName,live_league.ln,live_league.lnk,live_league.fg,live_league.bg,live_league.kn,live_league.kkod,
lang_league.leagueName,lang_league.leagueNameTh,lang_league.leagueNameEn,
lang_competition.comName,lang_competition.comNameTh,lang_competition.comNameEn,
league.priority from live_league
left join lang_league on lang_league.leagueId = live_league.leagueId
left join lang_competition on  lang_competition.comName = live_league.kn
left join live_match on live_match.showDate = live_league.date and live_match._lid = live_league.leagueId
left join league on league.leagueId = live_league.leagueId
where live_league.date=current_date and live_league.new='Y' and (live_match.sid>=5) and lnr=2
group by live_league.subleagueId
order by (CASE WHEN league.priority IS NULL then 1 ELSE 0 END),league.priority ASC,live_league.competitionId ASC,live_league.leagueId ASC,live_league.subleagueId ASC";
        $rsll = $db->createCommand($query2)->queryAll();
        foreach ($rsll as $key => $ll) {
            $rsll[$key]["ln"] = empty($ll["leagueNameTh"]) ? $ll["ln"] : $ll["leagueNameTh"];
            $rsll[$key]['kn'] = empty($ll["comNameTh"]) ? $ll['kn'] : $ll["comNameTh"];
            $jsonArrLl[0][] = $rsll[$key];
            $rsll[$key]["ln"] = empty($ll["leagueNameEn"]) ? $ll["ln"] : $ll["leagueNameEn"];
            $rsll[$key]['kn'] = empty($ll["comNameEn"]) ? $ll['kn'] : $ll["comNameEn"];
            $jsonArrLl[1][] = $rsll[$key];
        }
        $liveLeagueArray = $rsll;

        $nowday = Carbon::now();
        $json['Teamlogos']=$teamlogos;
        $json['live_match']=$liveMatchArray;
        $json['live_league']=$liveLeagueArray;
        $json['count']=$count;
        $json['c3']=$nowday->timestamp;
        $json['writer']='php';
        foreach($nation as $key=>$nt){
            $json['live_match']=$jsonArrLm[$key];
            $json['live_league']=$jsonArrLl[$key];
            $filepath = "../cronjob_gen_file/files/" . strtolower($nt) . "/liveMatchResultOnly.json";
            file_put_contents($filepath,json_encode($json));
        }



        $dtstop=Carbon::now();
        $lf = $dtstop->timestamp-$dtstart->timestamp;
        $functionstartsql="INSERT INTO connection_log (funcname,name,status,source,life_time) VALUES ('writelivefile','CConsoleCommand','close','php',$lf)";
        $db->createCommand($functionstartsql)->execute();

    }

}