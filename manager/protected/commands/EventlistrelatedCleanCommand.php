<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AchievementCommand
 *
 * @author mrsyrop
 */
class EventlistrelatedCleanCommand extends CConsoleCommand {

    public function run() {
        $db = Yii::app()->db;

        try {
            $db->beginTransaction();
            $basedir = 'backup';
            $storedir = 'event_list_related';
            if (!is_dir($basedir)) {
                mkdir($basedir);
                chmod($basedir, 0777);
            }
            $usedir = $basedir . "/" . $storedir;
            if (!is_dir($usedir)) {
                mkdir($usedir);
                chmod($usedir, 0777);
            }
            

            //$db->commit();
        } catch (Exception $ex) {
       //     $db->rollBack();
            echo $ex->getMessage();
        }
    }

}
