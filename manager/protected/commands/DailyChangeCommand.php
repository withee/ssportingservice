<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DailyRankingCommand
 *
 * @author withee
 */
class DailyChangeCommand extends CConsoleCommand {

    public function run($args) {
        $spirit = isset($args[0]) ? $args[0] : 0;
        $this->rankScores();
        $this->rankLike();
        $this->rankNewbies();
    }

    function rankScores() {
        $db = Yii::app()->db;
        //$transaction = $db->beginTransaction();
        $user_sql = "SELECT * FROM facebook_user";
        $user_list = $db->createCommand($user_sql)->queryAll();
        try {
            $week = date("W");
            $week4 = date("W");
            $year = date("Y");
            if ($week % 2 != 0) {
                $week++;
            }
            if ($week4 % 4 != 0) {
                $week4 = $week4 + (4 - ($week4 % 4));
            }


            foreach ($user_list as $user) {
                if (!empty($user["fb_uid"])) {

                    $rankTb_sql = "SELECT * FROM ranking14_score WHERE fb_uid='{$user['fb_uid']}' AND week=$week";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking14_score` (`rank`,`week`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$week, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `ranking14_score` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week ;")->execute();
                    }


                    $rankTb_sql = "SELECT * FROM ranking28_score WHERE fb_uid='{$user['fb_uid']}' AND week=$week4";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking28_score` (`rank`,`week`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$week4, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `ranking28_score` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week4 ;")->execute();
                    }


                    $rankTb_sql = "SELECT * FROM rankingall_score WHERE fb_uid='{$user['fb_uid']}' AND year=$year";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `rankingall_score` (`rank`,`year`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$year, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `rankingall_score` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `year`=$year ;")->execute();
                    }



//                    exit();
                }
            }
            // $transaction->commit();
        } catch (Exception $e) {
            //  $transaction->rollback();
            echo $e->getMessage();
        }
    }

    function rankNewbies() {
        $db = Yii::app()->db;
        $startweek = date("W");
        $startweek4 = date("W");
        $startyear = date("Y");
        if ($startweek % 2 != 1) {
            $startweek--;
        }
        if ($startweek4 % 4 != 1) {
            $startweek4 = $startweek4 - (($startweek4 % 4) - 1);
        }
        //$transaction = $db->beginTransaction();
        $startdate14 = date("Y-m-d", strtotime("$startyear-W$startweek-1"));
        $startdate28 = date("Y-m-d", strtotime("$startyear-W$startweek4-1"));
        $firstdateofyear = date("Y-m-d", strtotime("01-01-$startyear"));

        try {
            $week = date("W");
            $week4 = date("W");
            $year = date("Y");
            if ($week % 2 != 0) {
                $week++;
            }
            if ($week4 % 4 != 0) {
                $week4 = $week4 + (4 - ($week4 % 4));
            }

            $user14_sql = "SELECT * FROM facebook_user WHERE DATE(reg_at) BETWEEN $startdate14 AND DATE(NOW())";
            $user14_list = $db->createCommand($user14_sql)->queryAll();
            foreach ($user14_list as $user) {
                if (!empty($user["fb_uid"])) {

                    $rankTb_sql = "SELECT * FROM ranking14_newbies WHERE fb_uid='{$user['fb_uid']}' AND week=$week";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking14_newbies` (`rank`,`week`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$week, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `ranking14_newbies` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week ;")->execute();
                    }
                }
            }

            $user28_sql = "SELECT * FROM facebook_user WHERE DATE(reg_at) BETWEEN $startdate28 AND DATE(NOW())";
            $user28_list = $db->createCommand($user28_sql)->queryAll();
            foreach ($user28_list as $user) {
                if (!empty($user["fb_uid"])) {

                    $rankTb_sql = "SELECT * FROM ranking28_newbies WHERE fb_uid='{$user['fb_uid']}' AND week=$week4";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `ranking28_newbies` (`rank`,`week`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$week4, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `ranking28_newbies` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week4 ;")->execute();
                    }
                }
            }
            $userall_sql = "SELECT * FROM facebook_user WHERE DATE(reg_at) BETWEEN $startyear AND DATE(NOW())";
            $userall_list = $db->createCommand($userall_sql)->queryAll();
            foreach ($userall_list as $user) {
                if (!empty($user["fb_uid"])) {

                    $rankTb_sql = "SELECT * FROM rankingall_newbies WHERE fb_uid='{$user['fb_uid']}' AND year=$year";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $db->createCommand("INSERT INTO `rankingall_newbies` (`rank`,`year`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$year, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
                    } else {
                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
                        $db->createCommand("UPDATE `rankingall_newbies` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `year`=$year ;")->execute();
                    }
                }
            }
//
//
//                    $rankTb_sql = "SELECT * FROM ranking28_score WHERE fb_uid='{$user['fb_uid']}' AND week=$week";
//                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
//                    if (empty($rankTb)) {
//                        $db->createCommand("INSERT INTO `ranking28_score` (`rank`,`week`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$week4, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
//                    } else {
//                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
//                        $db->createCommand("UPDATE `ranking28_score` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week4 ;")->execute();
//                    }
//
//
//                    $rankTb_sql = "SELECT * FROM rankingall_score WHERE fb_uid='{$user['fb_uid']}' AND year=$year";
//                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
//                    if (empty($rankTb)) {
//                        $db->createCommand("INSERT INTO `rankingall_score` (`rank`,`year`, `fb_uid`, `gp_stamp`, `gp_change`, `stamp_at`, `update_at`) VALUES (0,$year, '{$user['fb_uid']}', {$user['gp']}, 0.0, NOW(), NOW());")->execute();
//                    } else {
//                        $change = (double) $user["gp"] - (double) $rankTb[0]['gp_stamp'];
//                        $db->createCommand("UPDATE `rankingall_score` SET `gp_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `year`=$year ;")->execute();
//                    }
//
//
//
////                    exit();
//                }
//            }
            // $transaction->commit();
        } catch (Exception $e) {
            //  $transaction->rollback();
            echo $e->getMessage();
        }
    }

    function rankLike() {
        $db = Yii::app()->db;
        $startweek = date("W");
        $startweek4 = date("W");
        $startyear = date("Y");
        if ($startweek % 2 != 1) {
            $startweek--;
        }
        if ($startweek4 % 4 != 1) {
            $startweek4 = $startweek4 - (($startweek4 % 4) - 1);
        }

        $startdate14 = strtotime("$startyear-W$startweek-1");
        $startdate28 = strtotime("$startyear-W$startweek4-1");
        $firstdateofyear = strtotime("01-01-$startyear");
        $nowday = date("Y-m-d");

        // echo $startdate14 . "\n" . $startdate28 . "\n" . $firstdateofyear;

        $user_sql = "SELECT * FROM facebook_user";
        $user_list = $db->createCommand($user_sql)->queryAll();
        try {
            $week = date("W");
            $week4 = date("W");
            $year = date("Y");
            if ($week % 2 != 0) {
                $week++;
            }
            if ($week4 % 4 != 0) {
                $week4 = $week4 + (4 - ($week4 % 4));
            }


            foreach ($user_list as $user) {
                if (!empty($user["fb_uid"])) {

                    $rankTb_sql = "SELECT * FROM ranking14_like WHERE fb_uid='{$user['fb_uid']}' AND week=$week";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $db->createCommand("INSERT INTO `ranking14_like` (`rank`,`week`, `fb_uid`, `like_stamp`, `like_change`, `stamp_at`, `update_at`) VALUES (0,$week, '{$user['fb_uid']}', {$sumlike[0]["sumlike"]}, 0, NOW(), NOW());")->execute();
                    } else {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $change = (int) $sumlike[0]['sumlike'] - (int) $rankTb[0]['like_stamp'];
                        if ($change != 0) {
                            //echo "update ".$user["fb_uid"]." : ".$change;
                            $db->createCommand("UPDATE `ranking14_like` SET `like_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week ;")->execute();
                        }
                    }

                    $rankTb_sql = "SELECT * FROM ranking28_like WHERE fb_uid='{$user['fb_uid']}' AND week=$week4";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $db->createCommand("INSERT INTO `ranking28_like` (`rank`,`week`, `fb_uid`, `like_stamp`, `like_change`, `stamp_at`, `update_at`) VALUES (0,$week4, '{$user['fb_uid']}', {$sumlike[0]["sumlike"]}, 0, NOW(), NOW());")->execute();
                    } else {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $change = (int) $sumlike[0]['sumlike'] - (int) $rankTb[0]['like_stamp'];
                        if ($change != 0) {
                            //echo "update ".$user["fb_uid"]." : ".$change;
                            $db->createCommand("UPDATE `ranking28_like` SET `like_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `week`=$week4 ;")->execute();
                        }
                    }

                    $rankTb_sql = "SELECT * FROM rankingall_like WHERE fb_uid='{$user['fb_uid']}' AND year=$year";
                    $rankTb = $db->createCommand($rankTb_sql)->queryAll();
                    if (empty($rankTb)) {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $db->createCommand("INSERT INTO `rankingall_like` (`rank`,`year`, `fb_uid`, `like_stamp`, `like_change`, `stamp_at`, `update_at`) VALUES (0,$year, '{$user['fb_uid']}', {$sumlike[0]["sumlike"]}, 0, NOW(), NOW());")->execute();
                    } else {
                        $sumlike = $db->createCommand("SELECT (CASE WHEN SUM(`like`) IS NULL then 0 ELSE SUM(`like`) END) sumlike FROM `comment_on_match` WHERE fb_uid='{$user["fb_uid"]}'")->queryAll();
                        $change = (int) $sumlike[0]['sumlike'] - (int) $rankTb[0]['like_stamp'];
                        if ($change != 0) {
                            //echo "update ".$user["fb_uid"]." : ".$change;
                            $db->createCommand("UPDATE `rankingall_like` SET `like_change`=$change, `update_at`=NOW() WHERE  `fb_uid`='{$user['fb_uid']}' AND `year`=$year ;")->execute();
                        }
                    }
                }
            }
        } catch (Exception $e) {
            //  $transaction->rollback();
            echo $e->getMessage();
        }
    }

}

?>
