<?php
/**
 * Created by JetBrains PhpStorm.
 * User: indpendents
 * Date: 9/13/13 AD
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
require('../DBConfig.php');
class GenSubLeagueDataCommand extends CConsoleCommand{
    public function run($args){
        $content =file_get_contents('../league_list.txt');
        $list=explode(',',$content);
        foreach($list as $leagueId){
            if($leagueId){
                echo $leagueId."\n";
                $this->gen($leagueId);
            }

        }
    }
    public function gen($leagueId){
        $db = DBConfig::getConnection();
        $sql_subLeague = "select sub_league.* from sub_league where leagueId=$leagueId order by seq ASC";
        $stmt = $db->query($sql_subLeague);
        $subLeagueList = $stmt->fetchAll();
        $json = json_encode(array(
            'sub_league' => array(),
            'latest_results' => array(),
            'next_matches' => array(),
        ));
        if (count($subLeagueList) > 0) {
            $sub_league_array = array();
            $sql_league = "
                select league.leagueId,league.competitionId,league.leagueNamePk,league.leagueName,
                if(lang_league.leagueNameEn is null ,league.leagueName,lang_league.leagueNameEn) as leagueNameEn,
                if(lang_league.leagueNameBig is null ,league.leagueName,lang_league.leagueNameBig) as leagueNameBig,
                if(lang_league.leagueNameGb is null ,league.leagueName,lang_league.leagueNameGb) as leagueNameGb,
                if(lang_league.leagueNameKr is null ,league.leagueName,lang_league.leagueNameKr) as leagueNameKr,
                if(lang_league.leagueNameTh is null ,league.leagueName,lang_league.leagueNameTh) as leagueNameTh,
                if(lang_league.leagueNameLa is null ,league.leagueName,lang_league.leagueNameLa) as leagueNameLa,
                if(lang_league.leagueNameVn is null ,league.leagueName,lang_league.leagueNameVn) as leagueNameVn
                from league left join lang_league on league.leagueId = lang_league.leagueId where league.leagueId=$leagueId
                    ";
            //echo $sql_league;
            //exit;
            $stmt_leauge = $db->query($sql_league);
            $league = $stmt_leauge->fetch(PDO::FETCH_OBJ);
            $team_list = array();
            $lang_league = array(
                'en' => $league->leagueNameEn,
                'big' => $league->leagueNameBig,
                'gb' => $league->leagueNameGb,
                'kr' => $league->leagueNameKr,
                'th' => $league->leagueNameTh,
                'la' => $league->leagueNameLa,
                'vn' => $league->leagueNameVn,
            );
            $sub_league_array[0] = array(
                'subLeagueNamePk' => $league->leagueNamePk,
                'subLeagueName' => $league->leagueName,
                'leagueId' => $leagueId,
                'competitionId' => $league->competitionId,
                'seq' => "0",
                'latest_results_league' => array(),
                'next_matches_league' => array(),
                'stat_table' => array(),
            );
            foreach ($subLeagueList as $subLeague) {
                unset($subLeague[0]);
                unset($subLeague[1]);
                unset($subLeague[2]);
                unset($subLeague[3]);
                unset($subLeague[4]);
                $subLeague['latest_results_league'] = array();
                $subLeague['next_matches_league'] = array();
                $subLeague['stat_table'] = array();
                $sub_league_array[$subLeague['seq']] = $subLeague;
            }

            $sql_latest_results = "
                select s.seq,r.* from latest_results_league r
                left join
                sub_league as s  on s.leagueId = r.leagueId and s.subLeagueNamePk = r.subLeagueNamePk

                where r.leagueId=$leagueId
                order by s.seq ASC,r.match_timestamp ASC
                ";
            //echo $sql_latest_results;exit;
            $sql_next_matches = "
                select s.seq,n.* from next_matches_league n
                left join
                sub_league as s  on s.leagueId = n.leagueId and s.subLeagueNamePk = n.subLeagueNamePk
                where n.leagueId=$leagueId
                order by s.seq ASC,n.match_timestamp ASC
                ";
            //echo $sql_next_matches;exit;
            $sql_stat_table = "
                select s.seq,stat.* from stat_table stat
                left join
                sub_league as s on s.leagueId = stat.leagueId and s.subLeagueNamePk = stat.subLeagueNamePk
                where stat.leagueId=$leagueId
                order by s.seq ASC,stat.no ASC
                ";
            $stmt_stat_table = $db->query($sql_stat_table);
            $stat_table = $stmt_stat_table->fetchAll(PDO::FETCH_OBJ);
            $stat_table_size = count($stat_table);
            //echo $stat_table_size;exit;
            if ($stat_table_size > 0) {

                for ($i = 0; $i < $stat_table_size; $i++) {
                    array_push($team_list, $stat_table[$i]->tid);
                    $seq = $stat_table[$i]->seq ? $stat_table[$i]->seq : 0;
                    unset($stat_table[$i]->seq);
                    array_push($sub_league_array[$seq]['stat_table'], $stat_table[$i]);
                }
            }
            $stmt_latest_results = $db->query($sql_latest_results);
            $latest_results = $stmt_latest_results->fetchAll(PDO::FETCH_OBJ);
            $latest_results_size = count($latest_results);
            if ($latest_results_size > 0) {
                for ($i = 0; $i < $latest_results_size; $i++) {

                    array_push($team_list, $latest_results[$i]->tid1);
                    array_push($team_list, $latest_results[$i]->tid2);
                    $seq = $latest_results[$i]->seq ? $latest_results[$i]->seq : 0;
                    unset($latest_results[$i]->seq);
                    array_push($sub_league_array[$seq]['latest_results_league'], $latest_results[$i]);
                }
            }
            $stmt_next_matches = $db->query($sql_next_matches);
            $next_matches = $stmt_next_matches->fetchAll(PDO::FETCH_OBJ);
            $next_matches_size = count($next_matches);
            if ($next_matches_size > 0) {
                for ($i = 0; $i < $next_matches_size; $i++) {
                    array_push($team_list, $next_matches[$i]->tid1);
                    array_push($team_list, $next_matches[$i]->tid2);
                    $seq = $next_matches[$i]->seq ? $next_matches[$i]->seq : 0;
                    unset($next_matches[$i]->seq);
                    //echo json_encode($sub_league_array[$seq]['next_matches_league']);exit;
                    array_push($sub_league_array[$seq]['next_matches_league'], $next_matches[$i]);
                }
            }

            $team_list_unique = array_unique($team_list);
            $sql_team = "
                select team.tid,team.tn as teamName,team.tnPk as teamNamePk,
                if(lang_team.teamNameEn is null,team.tn,lang_team.teamNameEn) as teamNameEn,
                if(lang_team.teamNameBig is null,team.tn,lang_team.teamNameBig) as teamNameBig,
                if(lang_team.teamNameGb is null,team.tn,lang_team.teamNameGb) as teamNameGb,
                if(lang_team.teamNameKr is null,team.tn,lang_team.teamNameKr) as teamNameKr,
                if(lang_team.teamNameTh is null,team.tn,lang_team.teamNameTh) as teamNameTh,
                if(lang_team.teamNameLa is null,team.tn,lang_team.teamNameLa) as teamNameLa,
                if(lang_team.teamNameVn is null,team.tn,lang_team.teamNameVn) as teamNameVn
                from team
                left join lang_team on lang_team.tid = team.tid
                where  team.tid=" . implode(" or  team.tid=", $team_list_unique) . "
                ";
            //echo $sql_team;exit;
            $stmt_team = $db->query($sql_team);
            $teamList = $stmt_team->fetchAll(PDO::FETCH_OBJ);
            $lang_team = array();
            foreach ($teamList as $t) {
                $lang_team[$t->tid] = array(
                    'en' => $t->teamNameEn,
                    'big' => $t->teamNameBig,
                    'gb' => $t->teamNameGb,
                    'kr' => $t->teamNameKr,
                    'th' => $t->teamNameTh,
                    'la' => $t->teamNameLa,
                    'vn'=> $t->teamNameVn,
                );
            }
            //echo json_encode($lang_team);exit;
            $obj = array(
                'lang_team' => $lang_team,
                'lang_league' => $lang_league,
                'data' => $sub_league_array,
            );
            $json = json_encode($obj);
            file_put_contents('../sub_league_data/' . $leagueId . '.json', $json);
        }else{
            file_put_contents('../sub_league_data/'.$leagueId.'.json',$json);
        }
    }

}