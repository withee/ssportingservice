<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PlayCountCommand
 *
 * @author mrsyrop
 */
require('../DBConfig.php');

class PlayCountCommand extends CConsoleCommand {

    public function run() {
        $db = DBConfig::getConnection();
        $usersql = "SELECT fb_uid FROM facebook_user";
        $stmt = $db->query($usersql);
        $userlist = $stmt->fetchAll(5);
        foreach ($userlist as $user) {
            //echo $user->fb_uid . "\n";
            if (is_numeric($user->fb_uid)) {
                $betsql = "SELECT `mid`,`result` FROM `bet` WHERE `fb_uid`='{$user->fb_uid}'";
                $bstmt = $db->query($betsql);
                $betlist = $bstmt->fetchAll(5);
                $pts = 0;
                $win = 0;
                $lose = 0;
                $draw = 0;
                $wait = 0;
                foreach ($betlist as $bet) {
                    $pts++;
                    if ($bet->result == 'win') {
                        $win++;
                    } else if ($bet->result == 'lose') {
                        $lose++;
                    } else if ($bet->result == 'draw') {
                        $draw++;
                    } else {
                        $wait++;
                    }
                }
                $db->exec("UPDATE facebook_user SET `pts`=$pts,`w`=$win,`l`=$lose,`d`=$draw WHERE `fb_uid`='{$user->fb_uid}'");
            }
        }
    }

}
