<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RankPublisherCommand
 *
 * @author mrsyrop
 */
require "../DBConfig.php";
require '../../facebook-php-sdk/src/facebook.php';

class RankPublisherCommand extends CConsoleCommand {

    public function run() {
        $db = DBConfig::getConnection();

        $day = date("Y-m-d");
        $shareday = date("d-m-Y");
        $config = json_decode(file_get_contents("../system_config/file.json"), true);
        $gplimit = count($config['award_top_ranking']);
        $sglimit = count($config['award_top_sgold']);
        $sql = "SELECT *  FROM gp_statistic gs LEFT JOIN facebook_user fb ON gs.fb_uid =fb.fb_uid WHERE gs.update_at='$day' ORDER BY rank LIMIT $gplimit";
        //AND fb.uid IN (1663,1688,10370,1306,874,7784,1320) 
        $stmt = $db->query($sql);
        $gprank = $stmt->fetchAll(5);
        //echo "Ranking\n";
        foreach ($gprank as $mkey => $gp) {
            //echo $gp->rank . ":" . $gp->display_name . "\n";
            $ttr = new TotalStatement();
            $ttr->uid = $gp->uid;
            $ttr->statement_type = "award";
            $ttr->type_addition = "top_ranking";
            $ttr->before_diamond = $gp->diamond;
            $ttr->before_scoin = $gp->scoin;
            $ttr->before_sgold = $gp->sgold;
            $ttr->scoin_income = $config['award_top_ranking'][$mkey]['scoin'];
            $ttr->sgold_income = $config['award_top_ranking'][$mkey]['sgold'];
            $ttr->diamond_income = $config['award_top_ranking'][$mkey]['diamond'];
            $ttr->balance_scoin = $ttr->before_scoin + $ttr->scoin_income;
            $ttr->balance_sgold = $ttr->before_sgold + $ttr->sgold_income;
            $ttr->balance_diamond = $ttr->before_diamond + $ttr->diamond_income;
            $ttr->result = "success";
            $ttr->statement_timestamp = date("Y-m-d H:i:s");
            $saved = $ttr->save();
            if ($saved) {
                $fbuser = FacebookUser::model()->find("uid={$gp->uid}");
                $fbuser->scoin = $ttr->balance_scoin;
                $fbuser->sgold = $ttr->balance_sgold;
                $fbuser->diamond = $ttr->balance_diamond;
                $fbuser->save();
            }

            if ($gp->fb_access_token) {
                //echo $gp->fb_uid . "\n";

                $smg = "เกมทายผลฟุตบอลสดทุกแมตช์ ลุ้นรับรางวัลมากมายทุก 15 วัน";
                $desc = "หากคุณคือเซียนบอลตัวจริง เราขอท้าพิสูจน์ความแม่น ไปกับเกมการแข่งขันสดๆจากทุกลีกทั่วโลกได้แล้วที่นี่";
                $caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
                $name = "ยินดีด้วย คุณได้รับรางวัล Top Ranking อันดับ {$gp->rank} จำนวน ";
                foreach ($config['award_top_ranking'][$mkey] as $key => $val) {
                    //echo "earn " . $key . ":" . $val . "\n";
                    if ($val > 0) {
                        $name.= $val . " " . $key . " ";
                    }
                }
                $name.=" รอบวันที่ $shareday";
                // echo $smg;
                $result = $this->fbPoster($gp->fb_access_token, $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/exchange-coin.php?" . date('U'), "http://api.ssporting.com/fb_shared/share-diamond.png?" . date('U'));
                //  var_dump($result);


                $smg = "เกมทายผลฟุตบอลสดทุกแมตช์บนเว็บและมือถือที่เดียวในโลก มาพิสูจน์ความแม่นกันเลย";
                $desc = "เกมส์ทายผลบอลที่มีการเก็บคะแนนเป็นลีก เพื่อหาผู้เล่นที่มีความแม่นยำสูงสุด";
                $caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
                $name = "ยินดีด้วย คุณได้รับรางวัลอันดับ {$gp->rank} ประเภท Top Ranking";
                $name.=" รอบวันที่ $shareday";
                // echo $smg;
                $result = $this->fbPoster($gp->fb_access_token, $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/ranking.php?" . date('U'), "http://api.ssporting.com/fb_shared/topranking-1.png?" . date('U'));
                //  var_dump($result);
            }
        }


        $sql = "SELECT * FROM sgold_play_statistic gs LEFT JOIN facebook_user fb ON gs.fb_uid =fb.fb_uid WHERE gs.update_at='$day' ORDER BY rank LIMIT $sglimit";
        $stmt = $db->query($sql);
        $gprank = $stmt->fetchAll(5);
        //echo "SGOLD\n";
        foreach ($gprank as $mkey => $gp) {
            //echo $gp->rank . ":" . $gp->display_name . "\n";
            $ttr = new TotalStatement();
            $ttr->uid = $gp->uid;
            $ttr->statement_type = "award";
            $ttr->type_addition = "top_sgold";
            $ttr->before_diamond = $gp->diamond;
            $ttr->before_scoin = $gp->scoin;
            $ttr->before_sgold = $gp->sgold;
            $ttr->scoin_income = $config['award_top_sgold'][$mkey]['scoin'];
            $ttr->sgold_income = $config['award_top_sgold'][$mkey]['sgold'];
            $ttr->diamond_income = $config['award_top_sgold'][$mkey]['diamond'];
            $ttr->balance_scoin = $ttr->before_scoin + $ttr->scoin_income;
            $ttr->balance_sgold = $ttr->before_sgold + $ttr->sgold_income;
            $ttr->balance_diamond = $ttr->before_diamond + $ttr->diamond_income;
            $ttr->result = "success";
            $ttr->statement_timestamp = date("Y-m-d H:i:s");
            $saved = $ttr->save();
            if ($saved) {
                $fbuser = FacebookUser::model()->find("uid={$gp->uid}");
                $fbuser->scoin = $ttr->balance_scoin;
                $fbuser->sgold = $ttr->balance_sgold;
                $fbuser->diamond = $ttr->balance_diamond;
                $fbuser->save();
            }


            if ($gp->fb_access_token) {
                //echo $gp->fb_uid . "\n";

                $smg = "เกมทายผลฟุตบอลสดทุกแมตช์ ลุ้นรับรางวัลมากมายทุก 15 วัน";
                $desc = "หากคุณคือเซียนบอลตัวจริง เราขอท้าพิสูจน์ความแม่น ไปกับเกมการแข่งขันสดๆจากทุกลีกทั่วโลกได้แล้วที่นี่";
                $caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
                $name = "ยินดีด้วย คุณได้รับรางวัล Top S-Gold อันดับ {$gp->rank} จำนวน ";
                foreach ($config['award_top_sgold'][$mkey] as $key => $val) {
                    //echo "earn " . $key . ":" . $val . "\n";
                    if ($val > 0) {
                        $name.= $val . " " . $key . " ";
                    }
                }
                $name.=" รอบวันที่ $shareday";
                // echo $smg;
                $result = $this->fbPoster($gp->fb_access_token, $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/exchange-coin.php?" . date('U'), "http://api.ssporting.com/fb_shared/share-diamond.png?" . date('U'));
                //  var_dump($result);


                $smg = "เกมทายผลฟุตบอลสดทุกแมตช์บนเว็บและมือถือที่เดียวในโลก มาพิสูจน์ความแม่นกันเลย";
                $desc = "เกมส์ทายผลบอลที่มีการเก็บคะแนนเป็นลีก เพื่อหาผู้เล่นที่มีความแม่นยำสูงสุด";
                $caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
                $name = "ยินดีด้วย คุณได้รับรางวัลอันดับ {$gp->rank} ประเภท Top Sgold";
                $name.=" รอบวันที่ $shareday";
                // echo $smg;
                $result = $this->fbPoster($gp->fb_access_token, $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/ranking.php?" . date('U'), "http://api.ssporting.com/fb_shared/topranking-1.png?" . date('U'));
                //  var_dump($result);
            }
        }


        //$desc = "$showdate หากคุณคือเซียนบอลตัวจริง เราขอท้าพิสูจน์ความแม่น ไปกับเกมการแข่งขันสดๆจากทุกลีกทั่วโลกได้แล้วที่นี่";
        //$caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
        //$smg = "{$teams['home']['teamNameTh']} vs {$teams['away']['teamNameTh']} ใครจะชนะมาดูกัน: " . $postmsg;
        //$response_array["postable"] = fbPoster($token["fb_access_token"], $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/game.php?mid=$mid", "http://ssporting.com/WebObject/images/s{$mid}.jpg?" . date('u'));
    }

    function fbPoster($token, $msg, $name, $desc, $caption, $link, $picture) {
        $postable = FALSE;
        $db = DBConfig::getConnection();
        $results = array("success" => $postable, "msg" => "");
        $facebook = new Facebook(array(
            'appId' => '400809029976308',
            'secret' => '93e21cf1551a4536d20843fcdf236402',
        ));
        if (!empty($token)) {
            $facebook->setAccessToken($token);
            $user_id = $facebook->getUser();
            $tmplink = "fb400809029976308://authorize#expires_in=0&access_token=$token&target_url=$link";
            if (!empty($user_id)) {
                $sql = "SELECT * FROM fb_post WHERE fb_uid='$user_id' ORDER BY id DESC";
                $stmt = $db->query($sql);
                $lastpost = $stmt->fetch(5);
                $allow = TRUE;
                if (!empty($lastpost)) {
                    $now = strtotime(date("Y-m-d H:i:s"));
                    $lasttime = strtotime($lastpost->last_post . "+3hours");
//                    if ($now >= $lasttime) {
//                        $allow = TRUE;
//                    } else {
//                        $allow = FALSE;
//                    }
                }
                if ($allow) {
                    try {
                        $ret_obj = $facebook->api('/me/feed', 'POST', array(
                            'link' => $link,
                            'message' => $msg,
                            'application' => '400809029976308',
                            'is_hidden' => TRUE,
                            'picture' => $picture,
                            "name" => "$name",
                            "caption" => "$caption",
                            "description" => "$desc"
                        ));
                        $postable = TRUE;
                        $addsql = "INSERT INTO `fb_post` (`fb_uid`, `last_post`) VALUES ('$user_id', NOW())";
                        $db->exec($addsql);
                        //echo "post";
                    } catch (FacebookApiException $e) {
//            $login_url = $facebook->getLoginUrl(array(
//                'scope' => 'publish_actions'
//            ));
                        //echo "fail";
                        $results["msg"] = $e->getType() . ":" . $e->getMessage();
                        error_log($e->getType());
                        error_log($e->getMessage());
                    }
                }
            }
        } else {
            $results["msg"] = "empty token";
        }
        $results["success"] = $postable;
        return $results;
    }

}
