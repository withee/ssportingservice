<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AwardsCommand
 *
 * @author mrsyrop
 */
//require '/home/dev/domains/ssporting.com/api/public_html/Award.php';
require '../Award.php';

//require '../DBConfig.php';
class AwardsCommand extends CConsoleCommand {

    public function run() {
        $db = $this->getOwnConnectionServer();
        $league = array('34885', '35350', '35351', '35030', '34991');
        $config = json_decode(file_get_contents('../system_config/file.json'), true);
        $day = date("d");
        $nowday = date("Y-m-d");
        $startday = date("Y-m-d");
        if ($day > 15) {
            $startday = date("Y") . "-" . date("m") . "-16";
        } else {
            $startday = date("Y") . "-" . date("m") . "-01";
        }
        if ($day == 16 || $day == 1) {
            $limit = count($config['award_top_ranking']);
            $sql = "SELECT fb.uid FROM gp_statistic gs LEFT JOIN facebook_user fb ON gs.fb_uid=fb.fb_uid WHERE gs.update_at='$startday' ORDER BY gs.rank LIMIT $limit";
            echo $sql . "\n";
            $stmt = $db->query($sql);
            $toprank = $stmt->fetchAll(5);
            $topranklist = array();
            foreach ($toprank as $rank) {
                $topranklist[] = $rank->uid;
            }
            award($topranklist, 'top_ranking');


            $limit = count($config['award_top_sgold']);
            $sql = "SELECT fb.uid FROM sgold_play_statistic gs LEFT JOIN facebook_user fb ON gs.fb_uid=fb.fb_uid WHERE gs.update_at='$startday' ORDER BY gs.rank LIMIT $limit";
            echo $sql . "\n";
            $stmt = $db->query($sql);
            $toprank = $stmt->fetchAll(5);
            $topranklist = array();
            foreach ($toprank as $rank) {
                $topranklist[] = $rank->uid;
            }
            award($topranklist, 'top_sgold');


//            $limit = count($config['award_top_league']);
//            foreach ($league as $lid) {
//                $sql = "SELECT fb.uid FROM league_ranking gs LEFT JOIN facebook_user fb ON gs.fb_uid=fb.fb_uid WHERE gs.update_at='$startday' AND _lid=$lid ORDER BY gs.rank LIMIT $limit";
//                echo $sql . "\n";
//                $stmt = $db->query($sql);
//                $toprank = $stmt->fetchAll(5);
//                $topranklist = array();
//                foreach ($toprank as $rank) {
//                    $topranklist[] = $rank->uid;
//                }
//                award($topranklist, 'top_league');
//            }
        }
    }

    public static function getOwnConnectionServer() {
        $dbhost = "54.179.179.202";
        $dbuser = "dev";
        $dbpass = "@x1234";
        $dbname = "dev_ssporting_swcp";
        $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->query("SET NAMES 'utf8'");
        return $dbh;
    }

    public static function getOwnConnection() {
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = "1234";
        $dbname = "1ivescore_swcp";
        $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh->query("SET NAMES 'utf8'");
        return $dbh;
    }

}
