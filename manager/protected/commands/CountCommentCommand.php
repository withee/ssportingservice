<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AchievementCommand
 *
 * @author mrsyrop
 */
class CountCommentCommand extends CConsoleCommand {

    public function run() {

        try {
            $chunk = 50000;
            $rows = $chunk;
            $round = 0;
            while ($rows == $chunk) {
                $start = $round * $chunk;
                $criteria = new CDbCriteria();
//                $criteria->condition = "update_at='$nowday'";
//                $criteria->order = "all_rank";
                $criteria->offset = $start;
                $criteria->limit = $chunk;
                $timelines = Timelines::model()->findAll($criteria);
                $countrow=0;
                foreach ($timelines as $key=>$timeline) {
                    $commentcount = 0;
                    if ((int) $timeline->type == 1) {
                        $criteria = new CDbCriteria();
                        $criteria->condition = "match_id={$timeline->key_list} AND parent_id=0";
                        $commentcount = CommentOnMatch::model()->count($criteria);
                    } else {
                        $criteria = new CDbCriteria();
                        $criteria->condition = "reply_to={$timeline->id} AND parent_id=0";
                        $commentcount = TimelinesReply::model()->count($criteria);
                    }
                    $timeline->comment_count = $commentcount;
                    $timeline->save();
                    echo ($key+1)."|".$timeline->id . ":" . $commentcount . "\n";
                    $countrow++;
                }
                $round++;
                $rows = $countrow;
                echo $rows."\n";
            }
        } catch (Exception $ex) {

            echo $ex->getMessage();
        }
    }

}
