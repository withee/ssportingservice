<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserRatingCommand
 *
 * @author mrsyrop
 */
class UserRatingCommand extends CConsoleCommand {

    public function run($args) {
        $db = Yii::app()->db;
        $userlist = $db->createCommand("SELECT * FROM facebook_user")->queryAll();
        foreach ($userlist as $user) {
            if (is_numeric($user["fb_uid"])) {
                $lastplay = $db->createCommand("SELECT MAX(betdatetime) as lastplay FROM bet WHERE fb_uid={$user["fb_uid"]}")->queryAll();
                $lastplaydate = date("Y-m-d h:i:s", $lastplay[0]["lastplay"]);
                $day = date("Y-m-d h:i:s");
                $week = date("W");
                $lastplaymonth = date("Y-m", $lastplay[0]["lastplay"]);
                $month = date("Y-m");
                $rate100 = 0.0;
                $rate200 = 0.0;
                $w100 = 0;
                $wh100 = 0;
                $d100 = 0;
                $l100 = 0;
                $lh100 = 0;
                $w200 = 0;
                $wh200 = 0;
                $d200 = 0;
                $l200 = 0;
                $lh200 = 0;
                if ($lastplaymonth == $month) {
//                    $betlistsql = "SELECT * FROM bet WHERE fb_uid={$user["fb_uid"]} ORDER BY betId DESC LIMIT 200";
//                    $betlist = $db->createCommand($betlistsql)->queryAll();

                    $betlistsql = "SELECT COUNT(betId) play,SUM( IF( betResult = 'win', 1, 0 ) ) win, SUM( IF( betResult = 'win_half', 1, 0 ) ) win_half, SUM( IF( betResult = 'draw', 1, 0 ) ) draw, SUM( IF( betResult = 'lose', 1, 0 ) ) lose, SUM( IF( betResult = 'lose_half', 1, 0 ) ) lose_half
FROM (
SELECT *
FROM `bet`
WHERE fb_uid = '{$user['fb_uid']}'
AND betResult <> 'wait'
ORDER BY betId DESC
LIMIT 100
) tmp";
                    $betlist100 = $db->createCommand($betlistsql)->queryAll();
                    if ((int) $betlist100[0]['play'] == 100) {
                        $w100 = $betlist100[0]['win'];
                        $wh100 = $betlist100[0]['win_half'];
                        $d100 = $betlist100[0]['draw'];
                        $l100 = $betlist100[0]['lose'];
                        $lh100 = $betlist100[0]['lose_half'];
                        $rate100 = ($w100 + $wh100);
                        echo $user['fb_uid'] . ":100|" . $rate100 . "\n";
                    }

                    $betlistsql = "SELECT COUNT(betId) play,SUM( IF( betResult = 'win', 1, 0 ) ) win, SUM( IF( betResult = 'win_half', 1, 0 ) ) win_half, SUM( IF( betResult = 'draw', 1, 0 ) ) draw, SUM( IF( betResult = 'lose', 1, 0 ) ) lose, SUM( IF( betResult = 'lose_half', 1, 0 ) ) lose_half
FROM (
SELECT *
FROM `bet`
WHERE fb_uid = '{$user['fb_uid']}'
AND betResult <> 'wait'
ORDER BY betId DESC
LIMIT 200
) tmp";
                    $betlist200 = $db->createCommand($betlistsql)->queryAll();
                    if ((int) $betlist200[0]['play'] == 200) {
                        $w200 = $betlist200[0]['win'];
                        $wh200 = $betlist200[0]['win_half'];
                        $d200 = $betlist200[0]['draw'];
                        $l200 = $betlist200[0]['lose'];
                        $lh200 = $betlist200[0]['lose_half'];
                        $rate200 = ($w200 + $wh200) / 2;
                        echo $user['fb_uid'] . ":200|" . $rate200 . "\n";
                    }

                    $insert = "INSERT INTO `user_rating` (
`fb_uid` ,
`last_100` ,
`last_200` ,
`w100` ,
`wh100` ,
`d100` ,
`l100` ,
`lh100` ,
`w200` ,
`wh200` ,
`d200` ,
`l200` ,
`lh200` ,
`last_play` ,
`update_at` ,
`week`
)
VALUES (
'{$user["fb_uid"]}', '$rate100', '$rate200', '{$w100}', '{$wh100}', '{$d100}', '{$l100}', '{$lh100}', '{$w200}', '{$wh200}', '{$d200}', '{$l200}', '{$lh200}', '$lastplaydate','$day', '$week'
);";
                    $db->createCommand($insert)->execute();
                }
            }
        }
    }

}
