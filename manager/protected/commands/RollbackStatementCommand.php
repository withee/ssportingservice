<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RollbackStatementCommand
 *
 * @author mrsyrop
 */
require('../DBConfig.php');

class RollbackStatementCommand extends CConsoleCommand {

    public function run($args) {
        $stype = $args[0];
        $day = $args[1];
        $db = DBConfig::getConnection();
        $statement = TotalStatement::model()->findAll("statement_type='$stype' AND DATE(statement_timestamp)='$day'");
        foreach ($statement as $st) {
            $fb = FacebookUser::model()->find("uid={$st->uid}");
//            $fb = new FacebookUser();
//            $st = new TotalStatement();

            echo $fb->display_name . "\n";
            $rb = new TotalStatement();
            $rb->uid = $fb->uid;
            $rb->statement_type = 'rollback';
            $rb->type_addition = $st->type_addition;
            $rb->before_sgold = $fb->sgold;
            $rb->before_scoin = $fb->scoin;
            $rb->before_diamond = $fb->diamond;
            $rb->sgold_outcome = $st->sgold_income;
            $rb->scoin_outcome = $st->scoin_income;
            $rb->diamond_outcome = $st->diamond_income;
            $rb->statement_timestamp = date("Y-m-d H:i:s");
            $rb->balance_sgold = $fb->sgold - $st->sgold_income;
            $rb->balance_scoin = $fb->scoin - $st->scoin_income;
            $rb->balance_diamond = $fb->diamond - $st->diamond_income;
            $rb->result = 'success';

            if ($rb->save()) {
                echo "save";
                $fb->sgold -= $rb->sgold_outcome;
                $fb->scoin -= $rb->scoin_outcome;
                $fb->diamond -= $rb->diamond_outcome;
                $fb->save();
            }
            //   print_r($rb->getErrors());
        }
    }

}
