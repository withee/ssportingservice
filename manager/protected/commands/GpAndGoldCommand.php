<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GpAndGoldCommand
 *
 * @author mrsyrop
 */
class GpAndGoldCommand extends CConsoleCommand {

    public function run() {
        $db = Yii::app()->db;
        $league = array('34885', '35350', '35351', '35030', '34991');
        $day = date("d");
        $nowday = date("Y-m-d");
        $startday = date("Y-m-d");
        if ((int) $day > 15) {
            $startday = date("Y-m") . "-16";
        } else {
            $startday = date("Y-m") . "-01";
        }

        if ((int) $day == 1) {
            $startday = date("Y-m", strtotime("-1 months")) . "-16";
        }
//
        if ((int) $day == 16) {
            $startday = date("Y-m") . "-01";
        }


        $ststart = strtotime($startday);
        $ststop = strtotime($nowday);

        $userlist = $db->createCommand("SELECT `fb_uid` FROM `facebook_user`")->queryAll();
        foreach ($userlist as $user) {
            if (is_numeric($user['fb_uid'])) {
                $betsql = "SELECT b.fb_uid, SUM( b.amount*b.mul) AS gp, COUNT( b.betId ) AS play, fb.pts,SUM(b.result_sgold) as pgold,fb.sgold
FROM bet b
LEFT JOIN facebook_user fb ON b.fb_uid = fb.fb_uid
WHERE b.fb_uid = '{$user['fb_uid']}'
AND b.betDatetime BETWEEN $ststart AND $ststop    
GROUP BY b.fb_uid
ORDER BY gp DESC";

//               echo $betsql;
//               exit();
                $betlist = $db->createCommand($betsql)->queryAll();
                $daynow = date("Y-m-d");
                $yesterday = date("Y-m-d", strtotime($daynow . "-1 day"));

                foreach ($betlist as $bet) {
                    //echo $bet["fb_uid"] . ":" . $bet["gp"] . "\n";
                    $dupsql = "SELECT * FROM `gp_statistic` WHERE `fb_uid`='{$bet['fb_uid']}'  AND stamp_at='$daynow'";
                    $todaystmp = $db->createCommand($dupsql)->queryAll();



                    $ytdsql = "SELECT * FROM `gp_statistic` WHERE fb_uid='{$bet['fb_uid']}'  AND stamp_at='$yesterday'";
                    $yesterdaystmp = $db->createCommand($ytdsql)->queryAll();

                    if ($yesterdaystmp) {
                        $gpchange = floatval($bet['gp']) - floatval($yesterdaystmp[0]['stamp_gp']);
                        $updatesql = "UPDATE `gp_statistic` SET `changed_gp`=$gpchange, `overall_gp`={$bet['gp']}, `update_at`=DATE(NOW()),`play`='{$bet['play']}',`pts`='{$bet['pts']}' WHERE  `id`='{$yesterdaystmp[0]['id']}'";
                        $db->createCommand($updatesql)->execute();
                    }

                    if ($todaystmp) {
                        
                    } else {
                        $realgp = 0;
//                        if ($yesterdaystmp) {
//                            $realgp = $yesterdaystmp[0]['real_gp'];
//                        } else {
                        $betsql = "SELECT b.fb_uid, SUM( b.amount*b.mul) AS gp
FROM bet b
WHERE b.fb_uid = '{$user['fb_uid']}'  
";
                        $userbet = $db->createCommand($betsql)->queryAll();
                        $realgp = $userbet[0]['gp'];
//                        }

                        $inssql = "INSERT INTO `gp_statistic` (`fb_uid`,`pts`, `stamp_gp`, `changed_gp`, `overall_gp`,`real_gp`,`play`, `stamp_at`, `update_at`, `season`) VALUES ('{$bet['fb_uid']}','{$bet['pts']}', '{$bet['gp']}', 0, 0,'$realgp','{$bet['play']}',DATE(NOW()),'0000-00-00',YEAR(NOW()));";
                        $db->createCommand($inssql)->execute();
                    }

                    //$ntime = date('H');
                    // if ($ntime == 5) {
                    //  }





                    $ytdsql = "SELECT * FROM `sgold_play_statistic` WHERE fb_uid='{$bet['fb_uid']}'  AND stamp_at='$yesterday'";
                    $yesterdaystmp = $db->createCommand($ytdsql)->queryAll();
                    if ($yesterdaystmp) {
                        $gpchange = floatval($bet['pgold']) - floatval($yesterdaystmp[0]['stamp_sgold']);
                        $updatesql = "UPDATE `sgold_play_statistic` SET `changed_sgold`=$gpchange, `overall_sgold`={$bet['pgold']}, `update_at`=DATE(NOW()),`play`='{$bet['play']}' WHERE  `id`='{$yesterdaystmp[0]['id']}'";
                        $db->createCommand($updatesql)->execute();
                    }

                    $dupsql = "SELECT * FROM `sgold_play_statistic` WHERE `fb_uid`='{$bet['fb_uid']}'  AND stamp_at='$daynow'";
                    $todaystmp = $db->createCommand($dupsql)->queryAll();

                    if ($todaystmp) {
                        
                    } else {
//                        $usersgold = 0;
//                        if ($yesterdaystmp) {
//                            $usersgold = $yesterdaystmp[0]['user_sgold'];
//                        } else {
                        $betsql = "SELECT SUM(b.result_sgold) as pgold
FROM bet b
WHERE b.fb_uid = '{$user['fb_uid']}'  ";
                        $userbet = $db->createCommand($betsql)->queryAll();
                        $usersgold = $userbet[0]['pgold'];
//                        }
//
                        $inssql = "INSERT INTO `sgold_play_statistic` (`fb_uid`,`user_sgold`, `stamp_sgold`, `changed_sgold`, `overall_sgold`,`play`, `stamp_at`, `update_at`, `season`) VALUES ('{$bet['fb_uid']}','$usersgold', '{$bet['pgold']}', 0, 0,'{$bet['play']}',DATE(NOW()),'0000-00-00',YEAR(NOW()));";
                        $db->createCommand($inssql)->execute();
                    }
                }
            }
        }
        $nHour = date("H");
        //      if ((int) $nHour % 4 == 0) {
        $stsql = "SELECT * FROM gp_statistic WHERE update_at='$daynow'  ORDER BY overall_gp DESC, pts DESC, play DESC";
        $stat = $db->createCommand($stsql)->queryAll();
        foreach ($stat as $key => $st) {
            $oldsql = "SELECT * FROM gp_statistic WHERE fb_uid='{$st['fb_uid']}'  AND update_at='$yesterday'";
            $oldstat = $db->createCommand($oldsql)->queryAll();
            $oldrank = 0;
            $crank = $key + 1;
            $rdiff = 0;
            if ($oldstat) {
                $oldrank = (int) $oldstat[0]['rank'];
            }
            if ($oldrank == 0) {
                $rdiff = $crank;
            } else {
                $rdiff = $oldrank - $crank;
            }
            $updaterank = "UPDATE `gp_statistic` SET `rank`=$crank, `rank_change`=$rdiff WHERE  `id`={$st['id']};";
            $db->createCommand($updaterank)->execute();
        }

        $stsql = "SELECT * FROM gp_statistic WHERE update_at='$daynow'  ORDER BY real_gp DESC, pts DESC, play DESC";
        $stat = $db->createCommand($stsql)->queryAll();
        foreach ($stat as $key => $st) {
            $oldsql = "SELECT * FROM gp_statistic WHERE fb_uid='{$st['fb_uid']}'  AND update_at='$yesterday'";
            $oldstat = $db->createCommand($oldsql)->queryAll();
            $oldrank = 0;
            $crank = $key + 1;
            $rdiff = 0;
            if ($oldstat) {
                $oldrank = (int) $oldstat[0]['rank'];
            }
            if ($oldrank == 0) {
                $rdiff = $crank;
            } else {
                $rdiff = $oldrank - $crank;
            }
            $updaterank = "UPDATE `gp_statistic` SET `all_rank`=$crank, `all_rank_change`=$rdiff WHERE  `id`={$st['id']};";
            $db->createCommand($updaterank)->execute();
        }



        $stsql = "SELECT * FROM sgold_play_statistic WHERE update_at='$daynow'  ORDER BY overall_sgold DESC, user_sgold DESC";
        $stat = $db->createCommand($stsql)->queryAll();
        foreach ($stat as $key => $st) {
            $oldsql = "SELECT * FROM sgold_play_statistic WHERE fb_uid='{$st['fb_uid']}'  AND update_at='$yesterday'";
            $oldstat = $db->createCommand($oldsql)->queryAll();
            $oldrank = 0;
            $crank = $key + 1;
            $rdiff = 0;
            if ($oldstat) {
                $oldrank = (int) $oldstat[0]['rank'];
            }
            if ($oldrank == 0) {
                $rdiff = $crank;
            } else {
                $rdiff = $oldrank - $crank;
            }
            $updaterank = "UPDATE `sgold_play_statistic` SET `rank`=$crank, `rank_change`=$rdiff WHERE  `id`={$st['id']};";
            $db->createCommand($updaterank)->execute();
        }

        $stsql = "SELECT * FROM sgold_play_statistic WHERE update_at='$daynow'  ORDER BY user_sgold DESC";
        $stat = $db->createCommand($stsql)->queryAll();
        foreach ($stat as $key => $st) {
            $oldsql = "SELECT * FROM sgold_play_statistic WHERE fb_uid='{$st['fb_uid']}'  AND update_at='$yesterday'";
            $oldstat = $db->createCommand($oldsql)->queryAll();
            $oldrank = 0;
            $crank = $key + 1;
            $rdiff = 0;
            if ($oldstat) {
                $oldrank = (int) $oldstat[0]['all_sgold_rank'];
            }
            if ($oldrank == 0) {
                $rdiff = $crank;
            } else {
                $rdiff = $oldrank - $crank;
            }
            $updaterank = "UPDATE `sgold_play_statistic` SET `all_sgold_rank`=$crank, `all_sgold_rchange`=$rdiff WHERE  `id`={$st['id']};";
            $db->createCommand($updaterank)->execute();
        }

        //      }
    }

}
