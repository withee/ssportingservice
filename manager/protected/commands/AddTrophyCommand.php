<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AddTrophyCommand
 *
 * @author mrsyrop
 */
class AddTrophyCommand extends CConsoleCommand {

    public function run($args) {
        $folder = $args[0];
        $type = $args[1];
        $url = $args[2];
        $desc = $args[3];

        $files = scandir("../trophy/$folder");
        foreach ($files as $file) {
            if ($file != "." && $file != "..") {
                $sq = explode(".", $file);
                echo $sq[0];
                $trophy = new Trophy();
                $trophy->sequence = $sq[0];
                $trophy->desc = $desc . $sq[0];
                $trophy->type = $type;
                $trophy->picture = $url . $folder . "/" . $file;
                $trophy->save();
                echo $trophy->id;
            }
        }
    }

}
