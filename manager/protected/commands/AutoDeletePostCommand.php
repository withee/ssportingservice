<?php

/**
 * Created by PhpStorm.
 * User: mrsyrop
 * Date: 21/1/2559
 * Time: 11:00 น.
 */
class AutoDeletePostCommand extends CConsoleCommand
{

    public function run()
    {

        $db = Yii::app()->db;
        //header("content-type: application/json");
        $opts = array('http' =>
            array(
                'method' => 'GET',
                'header' => 'Content-type: application/json'
            )
        );

        $context = stream_context_create($opts);

        try {
            $allsql = "SELECT * FROM `timelines`  tl
LEFT JOIN `video_highlight` vh ON tl.key_list=vh.video_id
WHERE tl.`content_type`='highlight'
AND tl.`remove`='N'
AND tl.`created_at` > NOW() - INTERVAL 1 DAY
AND vh.content IS NOT NULL";
            $urllists = $db->createCommand($allsql)->queryAll();

            foreach ($urllists as $url) {
                if (preg_match('/dailymotion/', $url['content'])) {
                    echo $url['content'] . "==> YES\n";

                    $urlflage = explode('/video/', $url['content']);
                    if (array_key_exists(1, $urlflage)) {
                        //echo $urlflage[1] . "\n";
                        $response = json_decode($this->getSslPage("https://api.dailymotion.com/video/" . $urlflage[1]), true);
                        //var_dump($response);
                        if (array_key_exists('error', $response)) {
                            echo "not_found\n";
                            $success = $db->createCommand("UPDATE `timelines` SET `remove`='Y' WHERE  `id`={$url['id']}")->execute();;
                            if ($success) {
                                echo "deleted\n";
                            }
                        } else {
                            echo "found\n";
                        }
                    }
                } else {
                    echo $url['content'] . "==> NO\n";
                }

                //$response = json_decode($this->getSslPage("https://api.dailymotion.com/video/x1t6wbs"), true);
                //var_dump($response);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function getSslPage($url)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}