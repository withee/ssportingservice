<?php
/**
 * Created by PhpStorm.
 * User: Tingly
 * Date: 7/22/14
 * Time: 1:35 PM
 */
require '../TotalStatement.php';
class ClearComboStepCommand extends CConsoleCommand{
    public function run($args){
        $config = json_decode(file_get_contents('../system_config/file.json'));
        $step_bonus =get_object_vars($config->step_combo_per_day);
        //print_r($step_bonus);exit;
        //echo $step_bonus["3"];exit;
        //var_dump($step_bonus);exit;
        $t = new TotalStatement();
        $db = Yii::app()->db;
        $sql =
            "
            select
sum(if(result='win',1,0)) as sum_win,
sum(if(result='lose',1,0)) as sum_lose,
sum(if(result='draw',1,0)) as sum_draw,
sum(if(result='wait',1,0)) as sum_wait,
sum(if(result='cancel',1,0)) as sum_cancel,
fb_uid from bet where
date(FROM_UNIXTIME(kickOff)- interval 4 hour) = current_date - interval 1 day group by fb_uid
            ";
        $list=Yii::app()->db->createCommand($sql)->queryAll();
        Yii::app()->db->createCommand("update facebook_user set step_combo_today_count=0")->execute();
        $trans=$db->beginTransaction();
        try{
            foreach($list as $obj){
                $fb_uid = $obj['fb_uid'];
                $facebook_user = $db->createCommand("select *from facebook_user where fb_uid='$fb_uid'")->queryRow();
                if($facebook_user['step_combo_type']=='neutral'){
                    if($obj['sum_win']>=1 && $obj['sum_lose']==0){
                        if($obj['sum_win']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            $diamond_income=$step_bonus["".$obj['sum_win'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set diamond=$balance_diamond,step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=step_combo_count+".$obj['sum_win']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'positive',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=step_combo_count+".$obj['sum_win']." where fb_uid='$fb_uid'";
                            //echo $sql_fb."\n";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>=1 && $obj['sum_win']==0){
                        if($obj['sum_lose']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            //echo $obj['sum_win']."\n";

                            $diamond_income=$step_bonus["".$obj['sum_lose'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set diamond=$balance_diamond,step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'negative',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>0 || $obj['sum_win']>0){
                        $sql_fb ="update facebook_user set step_combo_type='neutral',step_combo_count=0,step_combo_today_count=0 where fb_uid='$fb_uid'";
                        //echo $sql_fb."\n";
                        $db->createCommand($sql_fb)->execute();

                    }else{
                        //draw
                    }

                }else if($facebook_user['step_combo_type']=='negative'){
                    if($obj['sum_win']>=1 && $obj['sum_lose']==0){
                        if($obj['sum_win']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            $diamond_income=$step_bonus["".$obj['sum_win'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set  diamond=$balance_diamond,step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=".$obj['sum_win']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'positive',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=".$obj['sum_win']." where fb_uid='$fb_uid'";
                            //echo $sql_fb."\n";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>=1 && $obj['sum_win']==0){
                        if($obj['sum_lose']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            $diamond_income=$step_bonus["".$obj['sum_lose'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set diamond=$balance_diamond,step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=step_combo_count+".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'negative',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=step_combo_count+".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>0 || $obj['sum_win']>0){
                        $sql_fb ="update facebook_user set step_combo_type='neutral',step_combo_count=0,step_combo_today_count=0 where fb_uid='$fb_uid'";
                        //echo $sql_fb."\n";
                        $db->createCommand($sql_fb)->execute();

                    }else{
                        //draw
                    }
                }else{//positive
                    if($obj['sum_win']>=1 && $obj['sum_lose']==0){
                        if($obj['sum_win']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            $diamond_income=$step_bonus["".$obj['sum_win'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set diamond=$balance_diamond,step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=step_combo_count+".$obj['sum_win']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'positive',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='positive',step_combo_today_count=".$obj['sum_win'].",step_combo_count=step_combo_count+".$obj['sum_win']." where fb_uid='$fb_uid'";
                            //echo $sql_fb."\n";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>=1 && $obj['sum_win']==0){
                        if($obj['sum_lose']>=3){
                            $before_sgold =$facebook_user['sgold'];
                            $before_scoin =$facebook_user['scoin'];
                            $before_diamond = $facebook_user['diamond'];
                            $sgold_income=0;
                            $scoin_income=0;
                            $sgold_outcome=0;
                            $scoin_outcome=0;
                            $diamond_income=$step_bonus["".$obj['sum_lose'].""];
                            $diamond_outcome=0;
                            $balance_diamond = $before_diamond+$diamond_income;
                            $balance_sgold = $before_sgold;
                            $balance_scoin = $before_scoin;
                            $result='success';
                            $sql_fb ="update facebook_user set diamond=$balance_diamond,step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                            $sql=$t->getTotalStatementQuery($facebook_user['uid'],'step_bonus',"'step_bonus_today'",$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,"NULL","NULL",'negative',$before_diamond,$diamond_income,$diamond_outcome,$balance_diamond);
                            $db->createCommand($sql)->execute();
                        }else{
                            $sql_fb ="update facebook_user set step_combo_type='negative',step_combo_today_count=".$obj['sum_lose'].",step_combo_count=".$obj['sum_lose']." where fb_uid='$fb_uid'";
                            $db->createCommand($sql_fb)->execute();
                        }
                    }else if($obj['sum_lose']>0 || $obj['sum_win']>0){
                        $sql_fb ="update facebook_user set step_combo_type='neutral',step_combo_count=0,step_combo_today_count=0 where fb_uid='$fb_uid'";
                        $db->createCommand($sql_fb)->execute();

                    }else{
                        //draw
                    }
                }


            }
            $trans->commit();

        }catch(Exception $e){
            $trans->rollback();
            echo $e->getMessage();
        }


    }
} 