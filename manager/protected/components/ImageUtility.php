<?php
/**
 * Created by PhpStorm.
 * User: Panupong
 * Date: 30/1/2557
 * Time: 15:16 น.
 */

class ImageUtility {

    public static function DateDiff($strDate1,$strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
    }

    public static function getDateOfWeek($date){
//        $date = new DateTime($date, new DateTimeZone('Asia/Bangkok'));
//        $date->setDate($year,$month,$day);
        if($date->format('w')=='0'){
            return 'อา.';
        }elseif($date->format('w')=='1'){
            return 'จ.';
        }elseif($date->format('w')=='2'){
            return 'อ.';
        }elseif($date->format('w')=='3'){
            return 'พ.';
        }elseif($date->format('w')=='4'){
            return 'พฤ.';
        }elseif($date->format('w')=='5'){
            return 'ศ.';
        }elseif($date->format('w')=='6'){
            return 'ส.';
        }
    }

    public static function getDayOfWeek($year,$month,$day){
        $date = new DateTime("now", new DateTimeZone('Asia/Bangkok'));
        $date->setDate($year,$month,$day);
        if($date->format('w')=='0'){
            return 'อา.';
        }elseif($date->format('w')=='1'){
            return 'จ.';
        }elseif($date->format('w')=='2'){
            return 'อ.';
        }elseif($date->format('w')=='3'){
            return 'พ.';
        }elseif($date->format('w')=='4'){
            return 'พฤ.';
        }elseif($date->format('w')=='5'){
            return 'ศ.';
        }elseif($date->format('w')=='6'){
            return 'ส.';
        }
    }

    public static function resizeLimit($path, $newPath, $max) {
        //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
        $size = GetimageSize($path);
        $_width = $size[0];
        $_height = $size[1];
        $mime = $size['mime'];
        $width = null;
        $height = null;
        if ($_width > $_height) {
            $width = $max;
            $height = round($_height * ($max / $_width));
        } else {
            $height = $max;
            $width = round($_width * ($max / $_height));
        }
        //$height=round($width*$size[1]/$size[0]);
        $new_image_ext = null;
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }
        if ($new_image_ext == 'jpg') {
            $images_orig = ImageCreateFromJPEG($path);
        } else if ($new_image_ext == 'png') {
            $images_orig = ImageCreateFromPNG($path);
        } else if ($new_image_ext == 'gif') {
            $images_orig = ImageCreateFromGIF($path);
        }

        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);

        //ImageJPEG($images_fin, $newPath);
        if ($new_image_ext == 'jpg') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageJPEG($images_fin, $newPath);
        } else if ($new_image_ext == 'png') {
            imagealphablending($images_fin, false);
            imagesavealpha($images_fin, true);
            $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
            imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
            imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
            ImagePNG($images_fin, $newPath);
        } else if ($new_image_ext == 'gif') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageGIF($images_fin, $newPath);
        }
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }
    public static function resize($path, $newPath, $type,$max) {
        //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
        $size = GetimageSize($path);
        $_width = $size[0];
        $_height = $size[1];
        $mime = $size['mime'];
        $width = null;
        $height = null;
        if($type=='height'){
            $height=$max;
            $width = round($_width * ($max / $_height));
        }else{
            $width =$max;
            $height=round($_height * ($max/$_width));
        }
        //$height=round($width*$size[1]/$size[0]);
        $new_image_ext = null;
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }
        if ($new_image_ext == 'jpg') {
            $images_orig = ImageCreateFromJPEG($path);
        } else if ($new_image_ext == 'png') {
            $images_orig = ImageCreateFromPNG($path);
        } else if ($new_image_ext == 'gif') {
            $images_orig = ImageCreateFromGIF($path);
        }

        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);

        //ImageJPEG($images_fin, $newPath);
        if ($new_image_ext == 'jpg') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageJPEG($images_fin, $newPath);
        } else if ($new_image_ext == 'png') {
            imagealphablending($images_fin, false);
            imagesavealpha($images_fin, true);
            $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
            imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
            imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
            ImagePNG($images_fin, $newPath);
        } else if ($new_image_ext == 'gif') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageGIF($images_fin, $newPath);
        }
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }
    public static function resizeFixed($path, $newPath, $w,$h) {
        //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
        $size = GetimageSize($path);
        $mime = $size['mime'];
        $width = $w;
        $height = $h;

        //$height=round($width*$size[1]/$size[0]);
        $new_image_ext = null;
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }
        if ($new_image_ext == 'jpg') {
            $images_orig = ImageCreateFromJPEG($path);
        } else if ($new_image_ext == 'png') {
            $images_orig = ImageCreateFromPNG($path);
        } else if ($new_image_ext == 'gif') {
            $images_orig = ImageCreateFromGIF($path);
        }

        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);

        //ImageJPEG($images_fin, $newPath);
        if ($new_image_ext == 'jpg') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageJPEG($images_fin, $newPath);
        } else if ($new_image_ext == 'png') {
            imagealphablending($images_fin, false);
            imagesavealpha($images_fin, true);
            $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
            imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
            imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
            ImagePNG($images_fin, $newPath);
        } else if ($new_image_ext == 'gif') {
            ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
            ImageGIF($images_fin, $newPath);
        }
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }
    public static function crop($path, $newPath, $maxWidth,$maxHeight) {
        //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
        $size = GetimageSize($path);
        $width = $size[0];
        $height = $size[1];
        $w1 = $width;
        $h1 = $height;
        $mime = $size['mime'];
        if ($width > $maxWidth) {
            $ratio = $maxWidth / $width;

            $height = ($height * $ratio);
            $width = ($width * $ratio);
        }
        if ($height > $maxHeight) {
            $ratio = $maxHeight / $height;
            //height=maxHeight;
            $width = ($width * $ratio);
            $height = ($height * $ratio);
        }
        if ($width < $maxWidth) {
            $ratio = $maxWidth / $width;
            $width = $width * $ratio;
            $height = $height * $ratio;
        }
        if ($height < $maxHeight) {
            $ratio = $maxHeight / $height;
            $height = $height * $ratio;
            $width = $width * $ratio;
        }
        $y = (($height / 2) - ($maxHeight / 2));
        $x = (($width / 2) - ($maxWidth / 2));
        //$height=round($width*$size[1]/$size[0]);
        $new_image_ext = null;
        switch ($mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }
        if ($new_image_ext == 'jpg') {
            $images_orig = ImageCreateFromJPEG($path);
        } else if ($new_image_ext == 'png') {
            $images_orig = ImageCreateFromPNG($path);
        } else if ($new_image_ext == 'gif') {
            $images_orig = ImageCreateFromGIF($path);
        }

        $newImage = ImageCreateTrueColor($width, $height);
        $newImage2 = ImageCreateTrueColor($maxWidth, $maxHeight);

        //ImageJPEG($images_fin, $newPath);
        if ($new_image_ext == 'jpg') {
            imagecopyresampled($newImage, $images_orig, 0, 0, 0, 0, $width, $height, $w1, $h1);
            imagecopyresampled($newImage2, $newImage, 0, 0, $x, $y, $maxWidth, $maxHeight, $maxWidth, $maxHeight);
            ImageJPEG($newImage2, $newPath);
        } else if ($new_image_ext == 'png') {
            imagealphablending($newImage, false);
            imagesavealpha($newImage, true);
            $transparent = imagecolorallocatealpha($newImage, 255, 255, 255, 127);
            imagefilledrectangle($newImage, 0, 0, $width, $height, $transparent);
            imagecopyresampled($newImage, $images_orig, 0, 0, 0, 0, $width, $height, $w1, $h1);

            imagealphablending($newImage2, false);
            imagesavealpha($newImage2, true);
            $transparent = imagecolorallocatealpha($newImage2, 255, 255, 255, 127);
            imagefilledrectangle($newImage2, 0, 0, $maxWidth, $maxWidth, $transparent);
            imagecopyresampled($newImage2, $newImage, 0, 0, 0, 0, $maxWidth, $maxWidth, $maxWidth, $maxHeight);
            ImagePNG($newImage2, $newPath);
        } else if ($new_image_ext == 'gif') {
            imagecopyresampled($newImage, $images_orig, 0, 0, 0, 0, $width, $height, $w1, $h1);
            imagecopyresampled($newImage2, $newImage, 0, 0, $x, $y, $maxWidth, $maxHeight, $maxWidth, $maxHeight);
            ImageGIF($newImage2, $newPath);
        }
        ImageDestroy($images_orig);
        ImageDestroy($newImage);
        ImageDestroy($newImage2);
    }
} 