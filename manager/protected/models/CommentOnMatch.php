<?php

/**
 * This is the model class for table "comment_on_match".
 *
 * The followings are the available columns in table 'comment_on_match':
 * @property string $id
 * @property string $parent_id
 * @property string $match_id
 * @property string $fb_uid
 * @property string $message
 * @property string $picture
 * @property string $thumbnail
 * @property string $choose
 * @property string $type
 * @property string $comment_at
 * @property string $last_update
 * @property string $like
 * @property string $report
 * @property string $country_id
 * @property string $remove
 */
class CommentOnMatch extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'comment_on_match';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fb_uid, choose', 'required'),
            array('parent_id, match_id, comment_at, last_update', 'length', 'max' => 11),
            array('fb_uid', 'length', 'max' => 255),
            array('choose', 'length', 'max' => 4),
            array('type', 'length', 'max' => 7),
            array('like, report, country_id', 'length', 'max' => 10),
            array('remove', 'length', 'max' => 1),
            array('message, picture, thumbnail', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, parent_id, match_id, fb_uid, message, picture, thumbnail, choose, type, comment_at, last_update, like, report, country_id, remove', 'safe', 'on' => 'search'),
        );
    }

    public function getTableSchema() {
        $table = parent::getTableSchema();
        $table->columns['fb_uid']->isForeignKey = true;
        $table->foreignKeys['fb_uid'] = array('User', 'fb_uid');
        return $table;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', '', 'foreignKey' => array('fb_uid'=>'fb_uid'))
           //'user' => array(self::BELONGS_TO, 'User', '','on'=>'t.fb_uid=user.fb_uid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parent_id' => 'Parent',
            'match_id' => 'Match',
            'fb_uid' => 'Fb Uid',
            'message' => 'Message',
            'picture' => 'Picture',
            'thumbnail' => 'Thumbnail',
            'choose' => 'Choose',
            'type' => 'Type',
            'comment_at' => 'Comment At',
            'last_update' => 'Last Update',
            'like' => 'Like',
            'report' => 'Report',
            'country_id' => 'Country',
            'remove' => 'Remove',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id, true);
        $criteria->compare('match_id', $this->match_id, true);
        $criteria->compare('fb_uid', $this->fb_uid, true);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('picture', $this->picture, true);
        $criteria->compare('thumbnail', $this->thumbnail, true);
        $criteria->compare('choose', $this->choose, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('comment_at', $this->comment_at, true);
        $criteria->compare('last_update', $this->last_update, true);
        $criteria->compare('like', $this->like, true);
        $criteria->compare('report', $this->report, true);
        $criteria->compare('country_id', $this->country_id, true);
        $criteria->compare('remove', $this->remove, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CommentOnMatch the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
