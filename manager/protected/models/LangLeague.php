<?php

/**
 * This is the model class for table "lang_league".
 *
 * The followings are the available columns in table 'lang_league':
 * @property string $leagueName
 * @property string $leagueNameTh
 * @property integer $competitionId
 * @property string $leagueId
 * @property string $leagueNameBig
 * @property string $leagueNameGb
 * @property string $leagueNameKr
 * @property string $leagueNameVn
 * @property string $leagueNameLa
 * @property string $leagueNameEn
 * @property string $id7m
 */
class LangLeague extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LangLeague the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lang_league';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('leagueName, competitionId, leagueId, id7m', 'required'),
			array('competitionId', 'numerical', 'integerOnly'=>true),
			array('leagueName', 'length', 'max'=>200),
			array('leagueNameTh', 'length', 'max'=>1000),
			array('leagueId', 'length', 'max'=>10),
			array('leagueNameBig, leagueNameGb, leagueNameKr, leagueNameVn, leagueNameLa, leagueNameEn', 'length', 'max'=>255),
			array('id7m', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('leagueName, leagueNameTh, competitionId, leagueId, leagueNameBig, leagueNameGb, leagueNameKr, leagueNameVn, leagueNameLa, leagueNameEn, id7m', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'leagueName' => 'League Name',
			'leagueNameTh' => 'League Name Th',
			'competitionId' => 'Competition',
			'leagueId' => 'League',
			'leagueNameBig' => 'League Name Big',
			'leagueNameGb' => 'League Name Gb',
			'leagueNameKr' => 'League Name Kr',
			'leagueNameVn' => 'League Name Vn',
			'leagueNameLa' => 'League Name La',
			'leagueNameEn' => 'League Name En',
			'id7m' => 'Id7m',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('leagueName',$this->leagueName,true);
		$criteria->compare('leagueNameTh',$this->leagueNameTh,true);
		$criteria->compare('competitionId',$this->competitionId);
		$criteria->compare('leagueId',$this->leagueId,true);
		$criteria->compare('leagueNameBig',$this->leagueNameBig,true);
		$criteria->compare('leagueNameGb',$this->leagueNameGb,true);
		$criteria->compare('leagueNameKr',$this->leagueNameKr,true);
		$criteria->compare('leagueNameVn',$this->leagueNameVn,true);
		$criteria->compare('leagueNameLa',$this->leagueNameLa,true);
		$criteria->compare('leagueNameEn',$this->leagueNameEn,true);
		$criteria->compare('id7m',$this->id7m,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}