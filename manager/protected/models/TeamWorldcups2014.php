<?php

/**
 * This is the model class for table "team".
 *
 * The followings are the available columns in table 'team':
 * @property integer $tid
 * @property string $tn
 * @property string $tnPk
 * @property string $tnTh
 * @property integer $cid
 * @property string $comPk
 * @property string $comType
 */
class TeamWorldcups2014 extends EMongoDocument
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TeamWorldcups2014 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'team';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tid, tn, tnPk, tnTh, cid, comPk, comType', 'required'),
			array('tid, cid', 'numerical', 'integerOnly'=>true),
			array('tn, tnPk, comPk', 'length', 'max'=>100),
			array('tnTh', 'length', 'max'=>200),
			array('comType', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tid, tn, tnPk, tnTh, cid, comPk, comType', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tid' => 'Tid',
			'tn' => 'Tn',
			'tnPk' => 'Tn Pk',
			'tnTh' => 'Tn Th',
			'cid' => 'Cid',
			'comPk' => 'Com Pk',
			'comType' => 'Com Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tid',$this->tid);
		$criteria->compare('tn',$this->tn,true);
		$criteria->compare('tnPk',$this->tnPk,true);
		$criteria->compare('tnTh',$this->tnTh,true);
		$criteria->compare('cid',$this->cid);
		$criteria->compare('comPk',$this->comPk,true);
		$criteria->compare('comType',$this->comType,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}