<?php

/**
 * This is the model class for table "competitions".
 *
 * The followings are the available columns in table 'competitions':
 * @property integer $competitionId
 * @property string $competitionType
 * @property string $competitionName
 * @property string $competitionNamePk
 * @property string $leagueNamePk
 * @property string $season
 * @property string $competitionNameTh
 */
class Competitions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Competitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'competitions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('competitionId, competitionType, competitionName, competitionNamePk, leagueNamePk, season', 'required'),
			array('competitionId', 'numerical', 'integerOnly'=>true),
			array('competitionType', 'length', 'max'=>13),
			array('competitionName, competitionNamePk, season', 'length', 'max'=>50),
			array('leagueNamePk', 'length', 'max'=>100),
			array('competitionNameTh', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('competitionId, competitionType, competitionName, competitionNamePk, leagueNamePk, season, competitionNameTh', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'competitionId' => 'Competition',
			'competitionType' => 'Competition Type',
			'competitionName' => 'Competition Name',
			'competitionNamePk' => 'Competition Name Pk',
			'leagueNamePk' => 'League Name Pk',
			'season' => 'Season',
			'competitionNameTh' => 'Competition Name Th',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('competitionId',$this->competitionId);
		$criteria->compare('competitionType',$this->competitionType,true);
		$criteria->compare('competitionName',$this->competitionName,true);
		$criteria->compare('competitionNamePk',$this->competitionNamePk,true);
		$criteria->compare('leagueNamePk',$this->leagueNamePk,true);
		$criteria->compare('season',$this->season,true);
		$criteria->compare('competitionNameTh',$this->competitionNameTh,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}