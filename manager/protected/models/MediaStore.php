<?php

/**
 * This is the model class for table "media_store".
 *
 * The followings are the available columns in table 'media_store':
 * @property string $id
 * @property string $gall_id
 * @property string $path
 * @property string $thumbnail
 * @property string $created_at
 * @property string $remove
 * @property string $desc
 */
class MediaStore extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MediaStore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_store';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gall_id, path', 'required'),
			array('gall_id', 'length', 'max'=>10),
			array('remove', 'length', 'max'=>1),
			array('desc', 'length', 'max'=>256),
			array('thumbnail, created_at', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, gall_id, path, thumbnail, created_at, remove, desc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gall_id' => 'Gall',
			'path' => 'Path',
			'thumbnail' => 'Thumbnail',
			'created_at' => 'Created At',
			'remove' => 'Remove',
			'desc' => 'Desc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('gall_id',$this->gall_id,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('remove',$this->remove,true);
		$criteria->compare('desc',$this->desc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}