<?php

/**
 * This is the model class for table "push_list_message".
 *
 * The followings are the available columns in table 'push_list_message':
 * @property string $messageid
 * @property string $message
 * @property string $createdatetime
 * @property string $senddatetime
 * @property string $ios_sent
 * @property string $android_sent
 */
class PushListMessage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'push_list_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message, createdatetime', 'required'),
			array('message', 'length', 'max'=>255),
			array('ios_sent, android_sent', 'length', 'max'=>1),
			array('senddatetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('messageid, message, createdatetime, senddatetime, ios_sent, android_sent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'messageid' => 'Messageid',
			'message' => 'Message',
			'createdatetime' => 'Createdatetime',
			'senddatetime' => 'Senddatetime',
			'ios_sent' => 'Ios Sent',
			'android_sent' => 'Android Sent',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('messageid',$this->messageid,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('createdatetime',$this->createdatetime,true);
		$criteria->compare('senddatetime',$this->senddatetime,true);
		$criteria->compare('ios_sent',$this->ios_sent,true);
		$criteria->compare('android_sent',$this->android_sent,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PushListMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
