<?php

/**
 * This is the model class for table "live_match".
 *
 * The followings are the available columns in table 'live_match':
 * @property integer $mid
 * @property integer $lid
 * @property integer $_lid
 * @property integer $oid
 * @property integer $sid
 * @property integer $kid
 * @property integer $lnr
 * @property integer $c0
 * @property integer $c1
 * @property integer $c2
 * @property integer $ml
 * @property integer $hid
 * @property integer $gid
 * @property string $hn
 * @property string $gn
 * @property integer $hrci
 * @property integer $grci
 * @property string $grc
 * @property string $hrc
 * @property string $s1
 * @property string $s2
 * @property integer $mstan
 * @property string $otv
 * @property integer $hstan
 * @property integer $gstan
 * @property integer $golI
 * @property integer $l
 * @property integer $a
 * @property integer $ao
 * @property integer $cx
 * @property string $info
 * @property integer $rek_h
 * @property string $date
 * @property string $showDate
 * @property string $hide
 * @property string $m
 * @property string $new
 */
class LiveMatch extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LiveMatch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'live_match';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mid, lid, _lid, oid, sid, kid, lnr, c0, c1, c2, ml, hid, gid, hn, gn, hrci, grci, grc, hrc, s1, s2, mstan, otv, hstan, gstan, golI, l, a, ao, cx, info, rek_h, date, showDate, m', 'required'),
			array('mid, lid, _lid, oid, sid, kid, lnr, c0, c1, c2, ml, hid, gid, hrci, grci, mstan, hstan, gstan, golI, l, a, ao, cx, rek_h', 'numerical', 'integerOnly'=>true),
			array('hn, gn', 'length', 'max'=>200),
			array('grc, hrc, info', 'length', 'max'=>100),
			array('s1, s2, otv, m', 'length', 'max'=>50),
			array('hide, new', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mid, lid, _lid, oid, sid, kid, lnr, c0, c1, c2, ml, hid, gid, hn, gn, hrci, grci, grc, hrc, s1, s2, mstan, otv, hstan, gstan, golI, l, a, ao, cx, info, rek_h, date, showDate, hide, m, new', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mid' => 'Mid',
			'lid' => 'Lid',
			'_lid' => 'Lid',
			'oid' => 'Oid',
			'sid' => 'Sid',
			'kid' => 'Kid',
			'lnr' => 'Lnr',
			'c0' => 'C0',
			'c1' => 'C1',
			'c2' => 'C2',
			'ml' => 'Ml',
			'hid' => 'Hid',
			'gid' => 'Gid',
			'hn' => 'Hn',
			'gn' => 'Gn',
			'hrci' => 'Hrci',
			'grci' => 'Grci',
			'grc' => 'Grc',
			'hrc' => 'Hrc',
			's1' => 'S1',
			's2' => 'S2',
			'mstan' => 'Mstan',
			'otv' => 'Otv',
			'hstan' => 'Hstan',
			'gstan' => 'Gstan',
			'golI' => 'Gol I',
			'l' => 'L',
			'a' => 'A',
			'ao' => 'Ao',
			'cx' => 'Cx',
			'info' => 'Info',
			'rek_h' => 'Rek H',
			'date' => 'Date',
			'showDate' => 'Show Date',
			'hide' => 'Hide',
			'm' => 'M',
			'new' => 'New',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mid',$this->mid);
		$criteria->compare('lid',$this->lid);
		$criteria->compare('_lid',$this->_lid);
		$criteria->compare('oid',$this->oid);
		$criteria->compare('sid',$this->sid);
		$criteria->compare('kid',$this->kid);
		$criteria->compare('lnr',$this->lnr);
		$criteria->compare('c0',$this->c0);
		$criteria->compare('c1',$this->c1);
		$criteria->compare('c2',$this->c2);
		$criteria->compare('ml',$this->ml);
		$criteria->compare('hid',$this->hid);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('hn',$this->hn,true);
		$criteria->compare('gn',$this->gn,true);
		$criteria->compare('hrci',$this->hrci);
		$criteria->compare('grci',$this->grci);
		$criteria->compare('grc',$this->grc,true);
		$criteria->compare('hrc',$this->hrc,true);
		$criteria->compare('s1',$this->s1,true);
		$criteria->compare('s2',$this->s2,true);
		$criteria->compare('mstan',$this->mstan);
		$criteria->compare('otv',$this->otv,true);
		$criteria->compare('hstan',$this->hstan);
		$criteria->compare('gstan',$this->gstan);
		$criteria->compare('golI',$this->golI);
		$criteria->compare('l',$this->l);
		$criteria->compare('a',$this->a);
		$criteria->compare('ao',$this->ao);
		$criteria->compare('cx',$this->cx);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('rek_h',$this->rek_h);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('showDate',$this->showDate,true);
		$criteria->compare('hide',$this->hide,true);
		$criteria->compare('m',$this->m,true);
		$criteria->compare('new',$this->new,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}