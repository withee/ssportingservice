<?php

/**
 * This is the model class for table "league_s".
 *
 * The followings are the available columns in table 'league_s':
 * @property integer $id
 * @property integer $areaId
 * @property string $name
 * @property integer $targetId
 * @property integer $type
 * @property integer $count
 * @property string $url
 * @property integer $gameType
 * @property string $ln
 * @property string $name_1
 * @property string $name_2
 */
class LeagueS extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LeagueS the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'league_s';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, areaId, name, targetId, type, count, gameType, ln', 'required'),
			array('id, areaId, targetId, type, count, gameType', 'numerical', 'integerOnly'=>true),
			array('name, url, ln, name_1, name_2', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, areaId, name, targetId, type, count, url, gameType, ln, name_1, name_2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'areaId' => 'Area',
			'name' => 'Name',
			'targetId' => 'Target',
			'type' => 'Type',
			'count' => 'Count',
			'url' => 'Url',
			'gameType' => 'Game Type',
			'ln' => 'Ln',
			'name_1' => 'Name 1',
			'name_2' => 'Name 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('areaId',$this->areaId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('targetId',$this->targetId);
		$criteria->compare('type',$this->type);
		$criteria->compare('count',$this->count);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('gameType',$this->gameType);
		$criteria->compare('ln',$this->ln,true);
		$criteria->compare('name_1',$this->name_1,true);
		$criteria->compare('name_2',$this->name_2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}