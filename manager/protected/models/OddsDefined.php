<?php

/**
 * This is the model class for table "odds_defined".
 *
 * The followings are the available columns in table 'odds_defined':
 * @property string $match_id
 * @property integer $league_id
 * @property integer $home_id
 * @property integer $away_id
 * @property string $type
 * @property string $kick_off
 * @property string $hdp_home
 * @property string $hdp_away
 * @property string $hdp
 * @property string $hdp_home_e
 * @property string $hdp_away_e
 * @property string $hdp_e
 * @property string $hpd_datetime_update
 * @property string $ou_over_e
 * @property string $ou_under_e
 * @property string $ou_e
 * @property string $ou_over
 * @property string $ou_under
 * @property string $ou
 * @property string $ou_datetime_update
 * @property string $odds1x2_home_e
 * @property string $odds1x2_away_e
 * @property string $odds1x2_draw_e
 * @property string $odds1x2_home
 * @property string $odds1x2_away
 * @property string $odds1x2_draw
 * @property string $odds1x2_datetime_update
 * @property string $odds_date
 */
class OddsDefined extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OddsDefined the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'odds_defined';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('league_id, home_id, away_id, type, kick_off, odds_date', 'required'),
			array('league_id, home_id, away_id', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>9),
			array('kick_off', 'length', 'max'=>10),
			array('hdp_home, hdp_away, hdp, hdp_home_e, hdp_away_e, hdp_e, ou_over_e, ou_under_e, ou_e, ou_over, ou_under, ou, odds1x2_home_e, odds1x2_away_e, odds1x2_draw_e, odds1x2_home, odds1x2_away, odds1x2_draw', 'length', 'max'=>255),
			array('hpd_datetime_update, ou_datetime_update, odds1x2_datetime_update', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('match_id, league_id, home_id, away_id, type, kick_off, hdp_home, hdp_away, hdp, hdp_home_e, hdp_away_e, hdp_e, hpd_datetime_update, ou_over_e, ou_under_e, ou_e, ou_over, ou_under, ou, ou_datetime_update, odds1x2_home_e, odds1x2_away_e, odds1x2_draw_e, odds1x2_home, odds1x2_away, odds1x2_draw, odds1x2_datetime_update, odds_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'match_id' => 'Match',
			'league_id' => 'League',
			'home_id' => 'Home',
			'away_id' => 'Away',
			'type' => 'Type',
			'kick_off' => 'Kick Off',
			'hdp_home' => 'Hdp Home',
			'hdp_away' => 'Hdp Away',
			'hdp' => 'Hdp',
			'hdp_home_e' => 'Hdp Home E',
			'hdp_away_e' => 'Hdp Away E',
			'hdp_e' => 'Hdp E',
			'hpd_datetime_update' => 'Hpd Datetime Update',
			'ou_over_e' => 'Ou Over E',
			'ou_under_e' => 'Ou Under E',
			'ou_e' => 'Ou E',
			'ou_over' => 'Ou Over',
			'ou_under' => 'Ou Under',
			'ou' => 'Ou',
			'ou_datetime_update' => 'Ou Datetime Update',
			'odds1x2_home_e' => 'Odds1x2 Home E',
			'odds1x2_away_e' => 'Odds1x2 Away E',
			'odds1x2_draw_e' => 'Odds1x2 Draw E',
			'odds1x2_home' => 'Odds1x2 Home',
			'odds1x2_away' => 'Odds1x2 Away',
			'odds1x2_draw' => 'Odds1x2 Draw',
			'odds1x2_datetime_update' => 'Odds1x2 Datetime Update',
			'odds_date' => 'Odds Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('match_id',$this->match_id,true);
		$criteria->compare('league_id',$this->league_id);
		$criteria->compare('home_id',$this->home_id);
		$criteria->compare('away_id',$this->away_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('kick_off',$this->kick_off,true);
		$criteria->compare('hdp_home',$this->hdp_home,true);
		$criteria->compare('hdp_away',$this->hdp_away,true);
		$criteria->compare('hdp',$this->hdp,true);
		$criteria->compare('hdp_home_e',$this->hdp_home_e,true);
		$criteria->compare('hdp_away_e',$this->hdp_away_e,true);
		$criteria->compare('hdp_e',$this->hdp_e,true);
		$criteria->compare('hpd_datetime_update',$this->hpd_datetime_update,true);
		$criteria->compare('ou_over_e',$this->ou_over_e,true);
		$criteria->compare('ou_under_e',$this->ou_under_e,true);
		$criteria->compare('ou_e',$this->ou_e,true);
		$criteria->compare('ou_over',$this->ou_over,true);
		$criteria->compare('ou_under',$this->ou_under,true);
		$criteria->compare('ou',$this->ou,true);
		$criteria->compare('ou_datetime_update',$this->ou_datetime_update,true);
		$criteria->compare('odds1x2_home_e',$this->odds1x2_home_e,true);
		$criteria->compare('odds1x2_away_e',$this->odds1x2_away_e,true);
		$criteria->compare('odds1x2_draw_e',$this->odds1x2_draw_e,true);
		$criteria->compare('odds1x2_home',$this->odds1x2_home,true);
		$criteria->compare('odds1x2_away',$this->odds1x2_away,true);
		$criteria->compare('odds1x2_draw',$this->odds1x2_draw,true);
		$criteria->compare('odds1x2_datetime_update',$this->odds1x2_datetime_update,true);
		$criteria->compare('odds_date',$this->odds_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}