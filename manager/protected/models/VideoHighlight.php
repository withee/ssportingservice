<?php

/**
 * This is the model class for table "video_highlight".
 *
 * The followings are the available columns in table 'video_highlight':
 * @property string $video_id
 * @property string $mid
 * @property integer $gid
 * @property integer $hid
 * @property integer $leagueId
 * @property integer $competitionId
 * @property string $content
 * @property string $title
 * @property string $create_datetime
 * @property string $videosource
 * @property string $videotype
 * @property string $videostyle
 * @property string $video_tag
 * @property string $like
 * @property string $report
 * @property string $desc
 * @property string $thumbnail
 * @property string $category
 * @property string $pathFile
 */
class VideoHighlight extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'video_highlight';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content,title,video_tag', 'required'),
			array('gid, hid, leagueId, competitionId', 'numerical', 'integerOnly'=>true),
			array('mid, like, report', 'length', 'max'=>10),
			array('title, videosource, videotype, videostyle', 'length', 'max'=>255),
			array('thumbnail, category', 'length', 'max'=>250),
			array('video_tag, pathFile', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('video_id, mid, gid, hid, leagueId, competitionId, content, title, create_datetime, videosource, videotype, videostyle, video_tag, like, report, desc, thumbnail, category, pathFile', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'video_id' => 'Video',
			'mid' => 'Mid',
			'gid' => 'Gid',
			'hid' => 'Hid',
			'leagueId' => 'League',
			'competitionId' => 'Competition',
			'content' => 'Content',
			'title' => 'Title',
			'create_datetime' => 'Create Datetime',
			'videosource' => 'Videosource',
			'videotype' => 'Videotype',
			'videostyle' => 'Videostyle',
			'video_tag' => 'Video Tag',
			'like' => 'Like',
			'report' => 'Report',
			'desc' => 'Desc',
			'thumbnail' => 'Thumbnail',
			'category' => 'Category',
			'pathFile' => 'Path File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('video_id',$this->video_id,true);
		$criteria->compare('mid',$this->mid,true);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('hid',$this->hid);
		$criteria->compare('leagueId',$this->leagueId);
		$criteria->compare('competitionId',$this->competitionId);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('create_datetime',$this->create_datetime,true);
		$criteria->compare('videosource',$this->videosource,true);
		$criteria->compare('videotype',$this->videotype,true);
		$criteria->compare('videostyle',$this->videostyle,true);
		$criteria->compare('video_tag',$this->video_tag,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('pathFile',$this->pathFile,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VideoHighlight the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

//    function afterValidate() {
//        if ($this->videotype == 'highlight' and $this->category=='other') {
//            $this->addError('category', 'กรุณาเลือก ลีกส์');
//        }
//        return parent::afterValidate();
//    }
}
