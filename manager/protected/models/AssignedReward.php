<?php

/**
 * This is the model class for table "assigned_reward".
 *
 * The followings are the available columns in table 'assigned_reward':
 * @property string $id
 * @property string $uid
 * @property string $event
 * @property double $prize
 * @property string $unit
 * @property string $day
 * @property string $created_at
 * @property string $updated_at
 */
class AssignedReward extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'assigned_reward';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('day, created_at, updated_at', 'required'),
			array('prize', 'numerical'),
			array('uid', 'length', 'max'=>10),
			array('event', 'length', 'max'=>50),
			array('unit', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uid, event, prize, unit, day, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'event' => 'Event',
			'prize' => 'Prize',
			'unit' => 'Unit',
			'day' => 'Day',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('event',$this->event,true);
		$criteria->compare('prize',$this->prize);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AssignedReward the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
