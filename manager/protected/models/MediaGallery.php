<?php

/**
 * This is the model class for table "media_gallery".
 *
 * The followings are the available columns in table 'media_gallery':
 * @property string $id
 * @property string $fb_uid
 * @property string $gall_name
 * @property string $gall_type
 * @property string $group
 * @property string $created_at
 * @property string $update_at
 * @property string $stamp_at
 * @property string $like
 * @property string $report
 * @property string $remove
 * @property string $desc
 * @property string $title
 * @property string $tag
 * @property integer $view
 */
class MediaGallery extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'media_gallery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid', 'required'),
			array('view', 'numerical', 'integerOnly'=>true),
			array('fb_uid, gall_name', 'length', 'max'=>255),
			array('gall_type', 'length', 'max'=>7),
			array('group', 'length', 'max'=>50),
			array('stamp_at, like, report', 'length', 'max'=>10),
			array('remove', 'length', 'max'=>1),
			array('desc, title, tag', 'length', 'max'=>256),
			array('created_at, update_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fb_uid, gall_name, gall_type, group, created_at, update_at, stamp_at, like, report, remove, desc, title, tag, view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'Images' => array(self::HAS_MANY, 'MediaStore', 'gall_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fb_uid' => 'Fb Uid',
			'gall_name' => 'Gall Name',
			'gall_type' => 'Gall Type',
			'group' => 'Group',
			'created_at' => 'Created At',
			'update_at' => 'Update At',
			'stamp_at' => 'Stamp At',
			'like' => 'Like',
			'report' => 'Report',
			'remove' => 'Remove',
			'desc' => 'Desc',
			'title' => 'Title',
			'tag' => 'Tag',
			'view' => 'View',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('gall_name',$this->gall_name,true);
		$criteria->compare('gall_type',$this->gall_type,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('stamp_at',$this->stamp_at,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('remove',$this->remove,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('view',$this->view);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MediaGallery the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
