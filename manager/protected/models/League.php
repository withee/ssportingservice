<?php

/**
 * This is the model class for table "league".
 *
 * The followings are the available columns in table 'league':
 * @property integer $leagueId
 * @property integer $rid
 * @property integer $competitionId
 * @property string $leagueNamePk
 * @property string $leagueName
 * @property string $leagueNameTh
 * @property string $leagueSeason
 * @property integer $priority
 */
class League extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return League the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'league';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('leagueId, rid, competitionId, leagueNamePk, leagueName, leagueNameTh, leagueSeason', 'required'),
			array('leagueId, rid, competitionId, priority', 'numerical', 'integerOnly'=>true),
			array('leagueNamePk, leagueName, leagueSeason', 'length', 'max'=>50),
			array('leagueNameTh', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('leagueId, rid, competitionId, leagueNamePk, leagueName, leagueNameTh, leagueSeason, priority', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'leagueId' => 'League',
			'rid' => 'Rid',
			'competitionId' => 'Competition',
			'leagueNamePk' => 'League Name Pk',
			'leagueName' => 'League Name',
			'leagueNameTh' => 'League Name Th',
			'leagueSeason' => 'League Season',
			'priority' => 'Priority',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('leagueId',$this->leagueId);
		$criteria->compare('rid',$this->rid);
		$criteria->compare('competitionId',$this->competitionId);
		$criteria->compare('leagueNamePk',$this->leagueNamePk,true);
		$criteria->compare('leagueName',$this->leagueName,true);
		$criteria->compare('leagueNameTh',$this->leagueNameTh,true);
		$criteria->compare('leagueSeason',$this->leagueSeason,true);
		$criteria->compare('priority',$this->priority);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}