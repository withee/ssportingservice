<?php

/**
 * This is the model class for table "team".
 *
 * The followings are the available columns in table 'team':
 * @property integer $tid
 * @property string $tn
 * @property string $tnPk
 * @property string $tnTh
 * @property integer $cid
 * @property string $comPk
 * @property string $comType
 * @property string $team_bh
 * @property string $team_name
 * @property string $team_mainshirt
 * @property string $team_awayshirt
 * @property string $team_logo
 * @property string $team_edate
 * @property string $team_country_bh
 * @property string $team_country
 * @property string $team_city
 * @property string $team_stadium
 * @property string $team_addr
 * @property string $team_capacity
 * @property string $team_website
 * @property string $team_email
 * @property string $team_avgAge
 * @property string $team_gmurl
 * @property string $coach_bh
 * @property string $coach_name
 * @property string $coach_photo
 * @property string $coach_birthday
 * @property string $coach_club
 * @property string $coach_pre_club
 * @property string $coach_his_club
 * @property string $coach_gj
 * @property string $plist_num
 * @property string $plist_i
 * @property string $plist_n
 * @property string $plist_pos
 * @property string $plist_l
 * @property string $team_resume
 * @property string $team_golry
 * @property string $team_best
 * @property string $team_photo
 * @property string $coach_join
 * @property string $coach_height
 * @property string $coach_weight
 * @property string $coach_note
 * @property string $coach_golry
 */
class Team extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Team the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'team';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tid, tn, tnPk, tnTh, cid, comPk, comType', 'required'),
			array('tid, cid', 'numerical', 'integerOnly'=>true),
			array('tn, tnPk, comPk', 'length', 'max'=>100),
			array('tnTh', 'length', 'max'=>200),
			array('comType', 'length', 'max'=>50),
			array('team_bh, team_name, team_mainshirt, team_awayshirt, team_logo, team_edate, team_country_bh, team_country, team_city, team_stadium, team_addr, team_capacity, team_website, team_email, team_avgAge, team_gmurl, coach_bh, coach_name, coach_photo, coach_birthday, coach_club, coach_pre_club, coach_his_club, coach_gj, plist_num, plist_i, plist_n, plist_pos, plist_l, team_resume, team_golry, team_best, team_photo, coach_join, coach_height, coach_weight, coach_note, coach_golry', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tid, tn, tnPk, tnTh, cid, comPk, comType, team_bh, team_name, team_mainshirt, team_awayshirt, team_logo, team_edate, team_country_bh, team_country, team_city, team_stadium, team_addr, team_capacity, team_website, team_email, team_avgAge, team_gmurl, coach_bh, coach_name, coach_photo, coach_birthday, coach_club, coach_pre_club, coach_his_club, coach_gj, plist_num, plist_i, plist_n, plist_pos, plist_l, team_resume, team_golry, team_best, team_photo, coach_join, coach_height, coach_weight, coach_note, coach_golry', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tid' => 'Tid',
			'tn' => 'Tn',
			'tnPk' => 'Tn Pk',
			'tnTh' => 'Tn Th',
			'cid' => 'Cid',
			'comPk' => 'Com Pk',
			'comType' => 'Com Type',
			'team_bh' => 'Team Bh',
			'team_name' => 'Team Name',
			'team_mainshirt' => 'Team Mainshirt',
			'team_awayshirt' => 'Team Awayshirt',
			'team_logo' => 'Team Logo',
			'team_edate' => 'Team Edate',
			'team_country_bh' => 'Team Country Bh',
			'team_country' => 'Team Country',
			'team_city' => 'Team City',
			'team_stadium' => 'Team Stadium',
			'team_addr' => 'Team Addr',
			'team_capacity' => 'Team Capacity',
			'team_website' => 'Team Website',
			'team_email' => 'Team Email',
			'team_avgAge' => 'Team Avg Age',
			'team_gmurl' => 'Team Gmurl',
			'coach_bh' => 'Coach Bh',
			'coach_name' => 'Coach Name',
			'coach_photo' => 'Coach Photo',
			'coach_birthday' => 'Coach Birthday',
			'coach_club' => 'Coach Club',
			'coach_pre_club' => 'Coach Pre Club',
			'coach_his_club' => 'Coach His Club',
			'coach_gj' => 'Coach Gj',
			'plist_num' => 'Plist Num',
			'plist_i' => 'Plist I',
			'plist_n' => 'Plist N',
			'plist_pos' => 'Plist Pos',
			'plist_l' => 'Plist L',
			'team_resume' => 'Team Resume',
			'team_golry' => 'Team Golry',
			'team_best' => 'Team Best',
			'team_photo' => 'Team Photo',
			'coach_join' => 'Coach Join',
			'coach_height' => 'Coach Height',
			'coach_weight' => 'Coach Weight',
			'coach_note' => 'Coach Note',
			'coach_golry' => 'Coach Golry',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tid',$this->tid);
		$criteria->compare('tn',$this->tn,true);
		$criteria->compare('tnPk',$this->tnPk,true);
		$criteria->compare('tnTh',$this->tnTh,true);
		$criteria->compare('cid',$this->cid);
		$criteria->compare('comPk',$this->comPk,true);
		$criteria->compare('comType',$this->comType,true);
		$criteria->compare('team_bh',$this->team_bh,true);
		$criteria->compare('team_name',$this->team_name,true);
		$criteria->compare('team_mainshirt',$this->team_mainshirt,true);
		$criteria->compare('team_awayshirt',$this->team_awayshirt,true);
		$criteria->compare('team_logo',$this->team_logo,true);
		$criteria->compare('team_edate',$this->team_edate,true);
		$criteria->compare('team_country_bh',$this->team_country_bh,true);
		$criteria->compare('team_country',$this->team_country,true);
		$criteria->compare('team_city',$this->team_city,true);
		$criteria->compare('team_stadium',$this->team_stadium,true);
		$criteria->compare('team_addr',$this->team_addr,true);
		$criteria->compare('team_capacity',$this->team_capacity,true);
		$criteria->compare('team_website',$this->team_website,true);
		$criteria->compare('team_email',$this->team_email,true);
		$criteria->compare('team_avgAge',$this->team_avgAge,true);
		$criteria->compare('team_gmurl',$this->team_gmurl,true);
		$criteria->compare('coach_bh',$this->coach_bh,true);
		$criteria->compare('coach_name',$this->coach_name,true);
		$criteria->compare('coach_photo',$this->coach_photo,true);
		$criteria->compare('coach_birthday',$this->coach_birthday,true);
		$criteria->compare('coach_club',$this->coach_club,true);
		$criteria->compare('coach_pre_club',$this->coach_pre_club,true);
		$criteria->compare('coach_his_club',$this->coach_his_club,true);
		$criteria->compare('coach_gj',$this->coach_gj,true);
		$criteria->compare('plist_num',$this->plist_num,true);
		$criteria->compare('plist_i',$this->plist_i,true);
		$criteria->compare('plist_n',$this->plist_n,true);
		$criteria->compare('plist_pos',$this->plist_pos,true);
		$criteria->compare('plist_l',$this->plist_l,true);
		$criteria->compare('team_resume',$this->team_resume,true);
		$criteria->compare('team_golry',$this->team_golry,true);
		$criteria->compare('team_best',$this->team_best,true);
		$criteria->compare('team_photo',$this->team_photo,true);
		$criteria->compare('coach_join',$this->coach_join,true);
		$criteria->compare('coach_height',$this->coach_height,true);
		$criteria->compare('coach_weight',$this->coach_weight,true);
		$criteria->compare('coach_note',$this->coach_note,true);
		$criteria->compare('coach_golry',$this->coach_golry,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}