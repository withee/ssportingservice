<?php

/**
 * This is the model class for table "timelines_reply".
 *
 * The followings are the available columns in table 'timelines_reply':
 * @property string $id
 * @property string $parent_id
 * @property string $fb_uid
 * @property string $message
 * @property string $picture
 * @property string $thumbnail
 * @property string $like
 * @property string $report
 * @property string $created_at
 * @property string $comment_at
 * @property string $remove
 * @property string $reply_to
 * @property string $reply_type
 */
class TimelinesReply extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'timelines_reply';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid, created_at', 'required'),
			array('parent_id, reply_to', 'length', 'max'=>15),
			array('fb_uid', 'length', 'max'=>255),
			array('like, report', 'length', 'max'=>11),
			array('comment_at', 'length', 'max'=>20),
			array('remove', 'length', 'max'=>1),
			array('reply_type', 'length', 'max'=>3),
			array('message, picture, thumbnail', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, fb_uid, message, picture, thumbnail, like, report, created_at, comment_at, remove, reply_to, reply_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'fb_uid' => 'Fb Uid',
			'message' => 'Message',
			'picture' => 'Picture',
			'thumbnail' => 'Thumbnail',
			'like' => 'Like',
			'report' => 'Report',
			'created_at' => 'Created At',
			'comment_at' => 'Comment At',
			'remove' => 'Remove',
			'reply_to' => 'Reply To',
			'reply_type' => 'Reply Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('comment_at',$this->comment_at,true);
		$criteria->compare('remove',$this->remove,true);
		$criteria->compare('reply_to',$this->reply_to,true);
		$criteria->compare('reply_type',$this->reply_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TimelinesReply the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
