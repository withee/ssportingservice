<?php

/**
 * This is the model class for table "timelines".
 *
 * The followings are the available columns in table 'timelines':
 * @property string $id
 * @property string $fb_uid
 * @property string $keygen
 * @property string $content_type
 * @property string $key_list
 * @property string $created_at
 * @property string $updated_at
 * @property string $stamped_at
 * @property string $remove
 * @property string $pin
 * @property string $view
 * @property string $comment_count
 * @property integer $type
 * @property string $last_event
 */
class Timelines extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'timelines';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid', 'required'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('fb_uid, keygen', 'length', 'max'=>255),
			array('content_type', 'length', 'max'=>9),
			array('key_list', 'length', 'max'=>15),
			array('stamped_at', 'length', 'max'=>20),
			array('remove, pin, last_event', 'length', 'max'=>1),
			array('view, comment_count', 'length', 'max'=>10),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fb_uid, keygen, content_type, key_list, created_at, updated_at, stamped_at, remove, pin, view, comment_count, type, last_event', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fb_uid' => 'Fb Uid',
			'keygen' => 'Keygen',
			'content_type' => 'Content Type',
			'key_list' => 'Key List',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'stamped_at' => 'Stamped At',
			'remove' => 'Remove',
			'pin' => 'Pin',
			'view' => 'View',
			'comment_count' => 'Comment Count',
			'type' => 'Type',
			'last_event' => 'Last Event',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('keygen',$this->keygen,true);
		$criteria->compare('content_type',$this->content_type,true);
		$criteria->compare('key_list',$this->key_list,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('stamped_at',$this->stamped_at,true);
		$criteria->compare('remove',$this->remove,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('comment_count',$this->comment_count,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('last_event',$this->last_event,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Timelines the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
