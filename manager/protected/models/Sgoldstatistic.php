<?php

/**
 * This is the model class for table "sgold_play_statistic".
 *
 * The followings are the available columns in table 'sgold_play_statistic':
 * @property string $id
 * @property string $fb_uid
 * @property double $user_sgold
 * @property double $stamp_sgold
 * @property double $changed_sgold
 * @property double $overall_sgold
 * @property integer $rank
 * @property integer $rank_change
 * @property integer $all_sgold_rank
 * @property integer $all_sgold_rchange
 * @property integer $play
 * @property string $stamp_at
 * @property string $update_at
 * @property string $season
 */
class Sgoldstatistic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sgoldstatistic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sgold_play_statistic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid, stamp_at, update_at, season', 'required'),
			array('rank, rank_change, all_sgold_rank, all_sgold_rchange, play', 'numerical', 'integerOnly'=>true),
			array('user_sgold, stamp_sgold, changed_sgold, overall_sgold', 'numerical'),
			array('fb_uid', 'length', 'max'=>255),
			array('season', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fb_uid, user_sgold, stamp_sgold, changed_sgold, overall_sgold, rank, rank_change, all_sgold_rank, all_sgold_rchange, play, stamp_at, update_at, season', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fb_uid' => 'Fb Uid',
			'user_sgold' => 'User Sgold',
			'stamp_sgold' => 'Stamp Sgold',
			'changed_sgold' => 'Changed Sgold',
			'overall_sgold' => 'Overall Sgold',
			'rank' => 'Rank',
			'rank_change' => 'Rank Change',
			'all_sgold_rank' => 'All Sgold Rank',
			'all_sgold_rchange' => 'All Sgold Rchange',
			'play' => 'Play',
			'stamp_at' => 'Stamp At',
			'update_at' => 'Update At',
			'season' => 'Season',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('user_sgold',$this->user_sgold);
		$criteria->compare('stamp_sgold',$this->stamp_sgold);
		$criteria->compare('changed_sgold',$this->changed_sgold);
		$criteria->compare('overall_sgold',$this->overall_sgold);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('rank_change',$this->rank_change);
		$criteria->compare('all_sgold_rank',$this->all_sgold_rank);
		$criteria->compare('all_sgold_rchange',$this->all_sgold_rchange);
		$criteria->compare('play',$this->play);
		$criteria->compare('stamp_at',$this->stamp_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('season',$this->season,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}