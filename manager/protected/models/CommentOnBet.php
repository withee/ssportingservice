<?php

/**
 * This is the model class for table "comment_on_bet".
 *
 * The followings are the available columns in table 'comment_on_bet':
 * @property string $id
 * @property string $bet_id
 * @property string $fb_uid
 * @property string $message
 * @property string $choose
 * @property string $comment_at
 * @property string $last_update
 * @property string $like
 */
class CommentOnBet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CommentOnBet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment_on_bet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid, message, choose', 'required'),
			array('bet_id, like', 'length', 'max'=>10),
			array('fb_uid, message', 'length', 'max'=>255),
			array('choose', 'length', 'max'=>4),
			array('comment_at, last_update', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, bet_id, fb_uid, message, choose, comment_at, last_update, like', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bet_id' => 'Bet',
			'fb_uid' => 'Fb Uid',
			'message' => 'Message',
			'choose' => 'Choose',
			'comment_at' => 'Comment At',
			'last_update' => 'Last Update',
			'like' => 'Like',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('bet_id',$this->bet_id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('choose',$this->choose,true);
		$criteria->compare('comment_at',$this->comment_at,true);
		$criteria->compare('last_update',$this->last_update,true);
		$criteria->compare('like',$this->like,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}