<?php

/**
 * This is the model class for table "board".
 *
 * The followings are the available columns in table 'board':
 * @property string $id
 * @property string $uid
 * @property string $title
 * @property string $desc
 * @property string $media
 * @property string $media_type
 * @property integer $categoty
 * @property string $view
 * @property string $like
 * @property string $report
 * @property string $pin
 * @property string $remove
 */
class Board extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Board the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'board';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('desc', 'required'),
			array('categoty', 'numerical', 'integerOnly'=>true),
			array('uid', 'length', 'max'=>15),
			array('title, media', 'length', 'max'=>255),
			array('media_type', 'length', 'max'=>5),
			array('view, like, report', 'length', 'max'=>10),
			array('pin, remove', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, title, desc, media, media_type, categoty, view, like, report, pin, remove', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'title' => 'Title',
			'desc' => 'Desc',
			'media' => 'Media',
			'media_type' => 'Media Type',
			'categoty' => 'Categoty',
			'view' => 'View',
			'like' => 'Like',
			'report' => 'Report',
			'pin' => 'Pin',
			'remove' => 'Remove',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('media',$this->media,true);
		$criteria->compare('media_type',$this->media_type,true);
		$criteria->compare('categoty',$this->categoty);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('remove',$this->remove,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}