<?php

/**
 * This is the model class for table "facebook_user".
 *
 * The followings are the available columns in table 'facebook_user':
 * @property string $uid
 * @property string $fb_uid
 * @property string $fb_email
 * @property string $fb_firstname
 * @property string $fb_middle_name
 * @property string $fb_lastname
 * @property string $fb_languague
 * @property string $fb_name
 * @property string $fb_username
 * @property string $fb_timezone
 * @property string $fb_birthday_date
 * @property string $fb_sex
 * @property string $fb_pic
 * @property string $fb_profile_url
 * @property string $fb_access_token
 * @property string $fb_location
 * @property double $gp
 * @property integer $pts
 * @property integer $w
 * @property integer $d
 * @property integer $l
 * @property string $follow_count
 * @property string $following_count
 * @property integer $level
 * @property string $lastResult
 * @property string $exp
 * @property string $country_id
 * @property string $user_status
 * @property double $spirit
 * @property double $overall_gp
 * @property string $reg_at
 * @property string $mind
 * @property string $cover
 * @property string $site
 * @property string $display_name
 */
class User extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'facebook_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fb_uid, lastResult', 'required'),
            array('pts, w, d, l, level', 'numerical', 'integerOnly' => true),
            array('gp, spirit, overall_gp', 'numerical'),
            array('fb_uid, fb_email, fb_firstname, fb_middle_name, fb_lastname, fb_languague, fb_name, fb_username, fb_timezone, fb_birthday_date, fb_sex, fb_pic, fb_profile_url, fb_access_token, fb_location, lastResult, user_status, display_name', 'length', 'max' => 255),
            array('follow_count, following_count, exp, country_id', 'length', 'max' => 10),
            array('mind', 'length', 'max' => 512),
            array('reg_at, cover, site', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('uid, fb_uid, fb_email, fb_firstname, fb_middle_name, fb_lastname, fb_languague, fb_name, fb_username, fb_timezone, fb_birthday_date, fb_sex, fb_pic, fb_profile_url, fb_access_token, fb_location, gp, pts, w, d, l, follow_count, following_count, level, lastResult, exp, country_id, user_status, spirit, overall_gp, reg_at, mind, cover, site, display_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comment' => array(self::HAS_MANY, 'CommentOnMatch', '',"on" => "comment.fb_uid=t.fb_uid"),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'uid' => 'Uid',
            'fb_uid' => 'Fb Uid',
            'fb_email' => 'Fb Email',
            'fb_firstname' => 'Fb Firstname',
            'fb_middle_name' => 'Fb Middle Name',
            'fb_lastname' => 'Fb Lastname',
            'fb_languague' => 'Fb Languague',
            'fb_name' => 'Fb Name',
            'fb_username' => 'Fb Username',
            'fb_timezone' => 'Fb Timezone',
            'fb_birthday_date' => 'Fb Birthday Date',
            'fb_sex' => 'Fb Sex',
            'fb_pic' => 'Fb Pic',
            'fb_profile_url' => 'Fb Profile Url',
            'fb_access_token' => 'Fb Access Token',
            'fb_location' => 'Fb Location',
            'gp' => 'Gp',
            'pts' => 'Pts',
            'w' => 'W',
            'd' => 'D',
            'l' => 'L',
            'follow_count' => 'Follow Count',
            'following_count' => 'Following Count',
            'level' => 'Level',
            'lastResult' => 'Last Result',
            'exp' => 'Exp',
            'country_id' => 'Country',
            'user_status' => 'User Status',
            'spirit' => 'Spirit',
            'overall_gp' => 'Overall Gp',
            'reg_at' => 'Reg At',
            'mind' => 'Mind',
            'cover' => 'Cover',
            'site' => 'Site',
            'display_name' => 'Display Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('uid', $this->uid, true);
        $criteria->compare('fb_uid', $this->fb_uid, true);
        $criteria->compare('fb_email', $this->fb_email, true);
        $criteria->compare('fb_firstname', $this->fb_firstname, true);
        $criteria->compare('fb_middle_name', $this->fb_middle_name, true);
        $criteria->compare('fb_lastname', $this->fb_lastname, true);
        $criteria->compare('fb_languague', $this->fb_languague, true);
        $criteria->compare('fb_name', $this->fb_name, true);
        $criteria->compare('fb_username', $this->fb_username, true);
        $criteria->compare('fb_timezone', $this->fb_timezone, true);
        $criteria->compare('fb_birthday_date', $this->fb_birthday_date, true);
        $criteria->compare('fb_sex', $this->fb_sex, true);
        $criteria->compare('fb_pic', $this->fb_pic, true);
        $criteria->compare('fb_profile_url', $this->fb_profile_url, true);
        $criteria->compare('fb_access_token', $this->fb_access_token, true);
        $criteria->compare('fb_location', $this->fb_location, true);
        $criteria->compare('gp', $this->gp);
        $criteria->compare('pts', $this->pts);
        $criteria->compare('w', $this->w);
        $criteria->compare('d', $this->d);
        $criteria->compare('l', $this->l);
        $criteria->compare('follow_count', $this->follow_count, true);
        $criteria->compare('following_count', $this->following_count, true);
        $criteria->compare('level', $this->level);
        $criteria->compare('lastResult', $this->lastResult, true);
        $criteria->compare('exp', $this->exp, true);
        $criteria->compare('country_id', $this->country_id, true);
        $criteria->compare('user_status', $this->user_status, true);
        $criteria->compare('spirit', $this->spirit);
        $criteria->compare('overall_gp', $this->overall_gp);
        $criteria->compare('reg_at', $this->reg_at, true);
        $criteria->compare('mind', $this->mind, true);
        $criteria->compare('cover', $this->cover, true);
        $criteria->compare('site', $this->site, true);
        $criteria->compare('display_name', $this->display_name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getAdmin() {
        return self::model()->find("fb_uid=100007730416796");
    }

}
