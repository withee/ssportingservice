<?php

/**
 * This is the model class for table "lang_competition".
 *
 * The followings are the available columns in table 'lang_competition':
 * @property string $comName
 * @property string $comNameTh
 * @property integer $cid
 * @property string $comNamePk
 * @property string $comNameBig
 * @property string $comNameGb
 * @property string $comNameKr
 * @property string $comNameVn
 * @property string $comNameLa
 * @property string $comNameEn
 * @property string $id7m
 */
class LangCompetition extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LangCompetition the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lang_competition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comName, cid, comNamePk, id7m', 'required'),
			array('cid', 'numerical', 'integerOnly'=>true),
			array('comName', 'length', 'max'=>200),
			array('comNameTh', 'length', 'max'=>1000),
			array('comNamePk, comNameBig, comNameGb, comNameKr, comNameVn, comNameLa, comNameEn', 'length', 'max'=>255),
			array('id7m', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('comName, comNameTh, cid, comNamePk, comNameBig, comNameGb, comNameKr, comNameVn, comNameLa, comNameEn, id7m', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comName' => 'Com Name',
			'comNameTh' => 'Com Name Th',
			'cid' => 'Cid',
			'comNamePk' => 'Com Name Pk',
			'comNameBig' => 'Com Name Big',
			'comNameGb' => 'Com Name Gb',
			'comNameKr' => 'Com Name Kr',
			'comNameVn' => 'Com Name Vn',
			'comNameLa' => 'Com Name La',
			'comNameEn' => 'Com Name En',
			'id7m' => 'Id7m',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('comName',$this->comName,true);
		$criteria->compare('comNameTh',$this->comNameTh,true);
		$criteria->compare('cid',$this->cid);
		$criteria->compare('comNamePk',$this->comNamePk,true);
		$criteria->compare('comNameBig',$this->comNameBig,true);
		$criteria->compare('comNameGb',$this->comNameGb,true);
		$criteria->compare('comNameKr',$this->comNameKr,true);
		$criteria->compare('comNameVn',$this->comNameVn,true);
		$criteria->compare('comNameLa',$this->comNameLa,true);
		$criteria->compare('comNameEn',$this->comNameEn,true);
		$criteria->compare('id7m',$this->id7m,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}