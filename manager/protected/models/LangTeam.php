<?php

/**
 * This is the model class for table "lang_team".
 *
 * The followings are the available columns in table 'lang_team':
 * @property string $teamName
 * @property string $teamNameTh
 * @property string $tnPk
 * @property string $tid
 * @property string $teamNameBig
 * @property string $teamNameGb
 * @property string $teamNameKr
 * @property string $teamNameVn
 * @property string $teamNameLa
 * @property string $teamNameEn
 * @property string $id7m
 */
class LangTeam extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LangTeam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lang_team';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('teamName, tnPk, tid', 'required'),
			array('teamName', 'length', 'max'=>200),
			array('teamNameTh', 'length', 'max'=>500),
			array('tnPk, teamNameBig, teamNameGb, teamNameKr, teamNameVn, teamNameLa, teamNameEn', 'length', 'max'=>255),
			array('tid, id7m', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('teamName, teamNameTh, tnPk, tid, teamNameBig, teamNameGb, teamNameKr, teamNameVn, teamNameLa, teamNameEn, id7m', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'teamName' => 'Team Name',
			'teamNameTh' => 'Team Name Th',
			'tnPk' => 'Tn Pk',
			'tid' => 'Tid',
			'teamNameBig' => 'Team Name Big',
			'teamNameGb' => 'Team Name Gb',
			'teamNameKr' => 'Team Name Kr',
			'teamNameVn' => 'Team Name Vn',
			'teamNameLa' => 'Team Name La',
			'teamNameEn' => 'Team Name En',
			'id7m' => 'Id7m',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('teamName',$this->teamName,true);
		$criteria->compare('teamNameTh',$this->teamNameTh,true);
		$criteria->compare('tnPk',$this->tnPk,true);
		$criteria->compare('tid',$this->tid,true);
		$criteria->compare('teamNameBig',$this->teamNameBig,true);
		$criteria->compare('teamNameGb',$this->teamNameGb,true);
		$criteria->compare('teamNameKr',$this->teamNameKr,true);
		$criteria->compare('teamNameVn',$this->teamNameVn,true);
		$criteria->compare('teamNameLa',$this->teamNameLa,true);
		$criteria->compare('teamNameEn',$this->teamNameEn,true);
		$criteria->compare('id7m',$this->id7m,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}