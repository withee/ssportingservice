<?php

/**
 * This is the model class for table "total_statement".
 *
 * The followings are the available columns in table 'total_statement':
 * @property string $id
 * @property string $uid
 * @property string $statement_type
 * @property string $type_addition
 * @property double $before_sgold
 * @property double $before_scoin
 * @property double $sgold_income
 * @property double $scoin_income
 * @property double $sgold_outcome
 * @property double $scoin_outcome
 * @property double $balance_sgold
 * @property double $balance_scoin
 * @property string $result
 * @property string $bet_id
 * @property string $game_id
 * @property string $note
 * @property string $statement_timestamp
 * @property double $before_diamond
 * @property double $diamond_income
 * @property double $diamond_outcome
 * @property double $balance_diamond
 */
class TotalStatement extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TotalStatement the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'total_statement';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, statement_type, result, statement_timestamp', 'required'),
			array('before_sgold, before_scoin, sgold_income, scoin_income, sgold_outcome, scoin_outcome, balance_sgold, balance_scoin, before_diamond, diamond_income, diamond_outcome, balance_diamond', 'numerical'),
			array('uid, bet_id, game_id', 'length', 'max'=>10),
			array('statement_type', 'length', 'max'=>11),
			array('type_addition', 'length', 'max'=>19),
			array('result', 'length', 'max'=>7),
			array('note', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, uid, statement_type, type_addition, before_sgold, before_scoin, sgold_income, scoin_income, sgold_outcome, scoin_outcome, balance_sgold, balance_scoin, result, bet_id, game_id, note, statement_timestamp, before_diamond, diamond_income, diamond_outcome, balance_diamond', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'statement_type' => 'Statement Type',
			'type_addition' => 'Type Addition',
			'before_sgold' => 'Before Sgold',
			'before_scoin' => 'Before Scoin',
			'sgold_income' => 'Sgold Income',
			'scoin_income' => 'Scoin Income',
			'sgold_outcome' => 'Sgold Outcome',
			'scoin_outcome' => 'Scoin Outcome',
			'balance_sgold' => 'Balance Sgold',
			'balance_scoin' => 'Balance Scoin',
			'result' => 'Result',
			'bet_id' => 'Bet',
			'game_id' => 'Game',
			'note' => 'Note',
			'statement_timestamp' => 'Statement Timestamp',
			'before_diamond' => 'Before Diamond',
			'diamond_income' => 'Diamond Income',
			'diamond_outcome' => 'Diamond Outcome',
			'balance_diamond' => 'Balance Diamond',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('statement_type',$this->statement_type,true);
		$criteria->compare('type_addition',$this->type_addition,true);
		$criteria->compare('before_sgold',$this->before_sgold);
		$criteria->compare('before_scoin',$this->before_scoin);
		$criteria->compare('sgold_income',$this->sgold_income);
		$criteria->compare('scoin_income',$this->scoin_income);
		$criteria->compare('sgold_outcome',$this->sgold_outcome);
		$criteria->compare('scoin_outcome',$this->scoin_outcome);
		$criteria->compare('balance_sgold',$this->balance_sgold);
		$criteria->compare('balance_scoin',$this->balance_scoin);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('bet_id',$this->bet_id,true);
		$criteria->compare('game_id',$this->game_id,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('statement_timestamp',$this->statement_timestamp,true);
		$criteria->compare('before_diamond',$this->before_diamond);
		$criteria->compare('diamond_income',$this->diamond_income);
		$criteria->compare('diamond_outcome',$this->diamond_outcome);
		$criteria->compare('balance_diamond',$this->balance_diamond);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}