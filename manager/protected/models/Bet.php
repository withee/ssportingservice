<?php

/**
 * This is the model class for table "bet".
 *
 * The followings are the available columns in table 'bet':
 * @property string $betId
 * @property string $betType
 * @property string $deviceId
 * @property string $platform
 * @property integer $hid
 * @property integer $gid
 * @property integer $leagueId
 * @property string $kickOff
 * @property string $betDatetime
 * @property string $oddsType
 * @property string $betValue
 * @property string $choose
 * @property string $betHdp
 * @property string $result
 * @property string $betResult
 * @property string $oddsTimestamp
 * @property integer $amount
 * @property string $message
 * @property double $mul
 * @property double $betAmount
 * @property string $FT_score
 * @property integer $mid
 * @property string $fb_uid
 * @property string $like_count
 * @property string $country_id
 * @property integer $status_id
 * @property string $bet_sgold
 * @property double $result_sgold
 * @property string $created_at
 */
class Bet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bet the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bet';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('betType, deviceId, platform, hid, gid, leagueId, kickOff, betDatetime, oddsType, betValue, choose, oddsTimestamp, message, mid, fb_uid, created_at', 'required'),
			array('hid, gid, leagueId, amount, mid, status_id', 'numerical', 'integerOnly'=>true),
			array('mul, betAmount, result_sgold', 'numerical'),
			array('betType, betResult', 'length', 'max'=>9),
			array('deviceId, betValue, betHdp, FT_score, fb_uid', 'length', 'max'=>255),
			array('platform', 'length', 'max'=>7),
			array('kickOff, betDatetime', 'length', 'max'=>11),
			array('oddsType', 'length', 'max'=>3),
			array('choose', 'length', 'max'=>4),
			array('result', 'length', 'max'=>6),
			array('like_count, country_id, bet_sgold', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('betId, betType, deviceId, platform, hid, gid, leagueId, kickOff, betDatetime, oddsType, betValue, choose, betHdp, result, betResult, oddsTimestamp, amount, message, mul, betAmount, FT_score, mid, fb_uid, like_count, country_id, status_id, bet_sgold, result_sgold, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                      'author'=>array(self::BELONGS_TO, 'User', 'fb_uid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'betId' => 'Bet',
			'betType' => 'Bet Type',
			'deviceId' => 'Device',
			'platform' => 'Platform',
			'hid' => 'Hid',
			'gid' => 'Gid',
			'leagueId' => 'League',
			'kickOff' => 'Kick Off',
			'betDatetime' => 'Bet Datetime',
			'oddsType' => 'Odds Type',
			'betValue' => 'Bet Value',
			'choose' => 'Choose',
			'betHdp' => 'Bet Hdp',
			'result' => 'Result',
			'betResult' => 'Bet Result',
			'oddsTimestamp' => 'Odds Timestamp',
			'amount' => 'Amount',
			'message' => 'Message',
			'mul' => 'Mul',
			'betAmount' => 'Bet Amount',
			'FT_score' => 'Ft Score',
			'mid' => 'Mid',
			'fb_uid' => 'Fb Uid',
			'like_count' => 'Like Count',
			'country_id' => 'Country',
			'status_id' => 'Status',
			'bet_sgold' => 'Bet Sgold',
			'result_sgold' => 'Result Sgold',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('betId',$this->betId,true);
		$criteria->compare('betType',$this->betType,true);
		$criteria->compare('deviceId',$this->deviceId,true);
		$criteria->compare('platform',$this->platform,true);
		$criteria->compare('hid',$this->hid);
		$criteria->compare('gid',$this->gid);
		$criteria->compare('leagueId',$this->leagueId);
		$criteria->compare('kickOff',$this->kickOff,true);
		$criteria->compare('betDatetime',$this->betDatetime,true);
		$criteria->compare('oddsType',$this->oddsType,true);
		$criteria->compare('betValue',$this->betValue,true);
		$criteria->compare('choose',$this->choose,true);
		$criteria->compare('betHdp',$this->betHdp,true);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('betResult',$this->betResult,true);
		$criteria->compare('oddsTimestamp',$this->oddsTimestamp,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('mul',$this->mul);
		$criteria->compare('betAmount',$this->betAmount);
		$criteria->compare('FT_score',$this->FT_score,true);
		$criteria->compare('mid',$this->mid);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('like_count',$this->like_count,true);
		$criteria->compare('country_id',$this->country_id,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('bet_sgold',$this->bet_sgold,true);
		$criteria->compare('result_sgold',$this->result_sgold);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function SumBetAllByMatch($mid){
        return intval(Yii::app()->db->createCommand("select count(*) from bet where bet.mid=".$mid)->queryScalar());
    }
}