<?php

/**
 * This is the model class for table "timelines_game".
 *
 * The followings are the available columns in table 'timelines_game':
 * @property string $id
 * @property string $mid
 * @property string $_lid
 * @property string $hid
 * @property string $gid
 * @property string $sid
 * @property string $c0
 * @property string $c1
 * @property string $c2
 * @property string $cx
 * @property string $competitionId
 * @property string $show_date
 * @property string $score
 * @property string $leagueName
 * @property string $leagueNameEn
 * @property string $leagueNameTh
 * @property string $leagueNameBig
 * @property string $leagueNameGb
 * @property string $leagueNameKr
 * @property string $leagueNameVn
 * @property string $leagueNameLa
 * @property string $homeName
 * @property string $homeNameEn
 * @property string $homeNameTh
 * @property string $homeNameBig
 * @property string $homeNameGb
 * @property string $homeNameKr
 * @property string $homeNameVn
 * @property string $homeNameLa
 * @property string $awayName
 * @property string $awayNameEn
 * @property string $awayNameTh
 * @property string $awayNameBig
 * @property string $awayNameGb
 * @property string $awayNameKr
 * @property string $awayNameVn
 * @property string $awayNameLa
 * @property string $hdp
 * @property string $hdp_home
 * @property string $hdp_away
 * @property string $type
 * @property string $match_id
 * @property string $h32x32
 * @property string $h64x64
 * @property string $h256x256
 * @property string $a32x32
 * @property string $a64x64
 * @property string $a256x256
 * @property string $like
 * @property string $report
 * @property string $remove
 */
class TimelinesGame extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TimelinesGame the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'timelines_game';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('show_date', 'required'),
			array('mid, _lid, hid, gid, sid, c0, c1, c2, cx, competitionId', 'length', 'max'=>15),
			array('score', 'length', 'max'=>50),
			array('leagueName, leagueNameEn, leagueNameTh, leagueNameBig, leagueNameGb, leagueNameKr, leagueNameVn, leagueNameLa, homeName, homeNameEn, homeNameTh, homeNameBig, homeNameGb, homeNameKr, homeNameVn, homeNameLa, awayName, awayNameEn, awayNameTh, awayNameBig, awayNameGb, awayNameKr, awayNameVn, awayNameLa, hdp, hdp_home, hdp_away, h32x32, h64x64, h256x256, a32x32, a64x64, a256x256', 'length', 'max'=>255),
			array('type', 'length', 'max'=>9),
			array('match_id, like, report', 'length', 'max'=>10),
			array('remove', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, mid, _lid, hid, gid, sid, c0, c1, c2, cx, competitionId, show_date, score, leagueName, leagueNameEn, leagueNameTh, leagueNameBig, leagueNameGb, leagueNameKr, leagueNameVn, leagueNameLa, homeName, homeNameEn, homeNameTh, homeNameBig, homeNameGb, homeNameKr, homeNameVn, homeNameLa, awayName, awayNameEn, awayNameTh, awayNameBig, awayNameGb, awayNameKr, awayNameVn, awayNameLa, hdp, hdp_home, hdp_away, type, match_id, h32x32, h64x64, h256x256, a32x32, a64x64, a256x256, like, report, remove', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mid' => 'Mid',
			'_lid' => 'Lid',
			'hid' => 'Hid',
			'gid' => 'Gid',
			'sid' => 'Sid',
			'c0' => 'C0',
			'c1' => 'C1',
			'c2' => 'C2',
			'cx' => 'Cx',
			'competitionId' => 'Competition',
			'show_date' => 'Show Date',
			'score' => 'Score',
			'leagueName' => 'League Name',
			'leagueNameEn' => 'League Name En',
			'leagueNameTh' => 'League Name Th',
			'leagueNameBig' => 'League Name Big',
			'leagueNameGb' => 'League Name Gb',
			'leagueNameKr' => 'League Name Kr',
			'leagueNameVn' => 'League Name Vn',
			'leagueNameLa' => 'League Name La',
			'homeName' => 'Home Name',
			'homeNameEn' => 'Home Name En',
			'homeNameTh' => 'Home Name Th',
			'homeNameBig' => 'Home Name Big',
			'homeNameGb' => 'Home Name Gb',
			'homeNameKr' => 'Home Name Kr',
			'homeNameVn' => 'Home Name Vn',
			'homeNameLa' => 'Home Name La',
			'awayName' => 'Away Name',
			'awayNameEn' => 'Away Name En',
			'awayNameTh' => 'Away Name Th',
			'awayNameBig' => 'Away Name Big',
			'awayNameGb' => 'Away Name Gb',
			'awayNameKr' => 'Away Name Kr',
			'awayNameVn' => 'Away Name Vn',
			'awayNameLa' => 'Away Name La',
			'hdp' => 'Hdp',
			'hdp_home' => 'Hdp Home',
			'hdp_away' => 'Hdp Away',
			'type' => 'Type',
			'match_id' => 'Match',
			'h32x32' => 'H32x32',
			'h64x64' => 'H64x64',
			'h256x256' => 'H256x256',
			'a32x32' => 'A32x32',
			'a64x64' => 'A64x64',
			'a256x256' => 'A256x256',
			'like' => 'Like',
			'report' => 'Report',
			'remove' => 'Remove',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('mid',$this->mid,true);
		$criteria->compare('_lid',$this->_lid,true);
		$criteria->compare('hid',$this->hid,true);
		$criteria->compare('gid',$this->gid,true);
		$criteria->compare('sid',$this->sid,true);
		$criteria->compare('c0',$this->c0,true);
		$criteria->compare('c1',$this->c1,true);
		$criteria->compare('c2',$this->c2,true);
		$criteria->compare('cx',$this->cx,true);
		$criteria->compare('competitionId',$this->competitionId,true);
		$criteria->compare('show_date',$this->show_date,true);
		$criteria->compare('score',$this->score,true);
		$criteria->compare('leagueName',$this->leagueName,true);
		$criteria->compare('leagueNameEn',$this->leagueNameEn,true);
		$criteria->compare('leagueNameTh',$this->leagueNameTh,true);
		$criteria->compare('leagueNameBig',$this->leagueNameBig,true);
		$criteria->compare('leagueNameGb',$this->leagueNameGb,true);
		$criteria->compare('leagueNameKr',$this->leagueNameKr,true);
		$criteria->compare('leagueNameVn',$this->leagueNameVn,true);
		$criteria->compare('leagueNameLa',$this->leagueNameLa,true);
		$criteria->compare('homeName',$this->homeName,true);
		$criteria->compare('homeNameEn',$this->homeNameEn,true);
		$criteria->compare('homeNameTh',$this->homeNameTh,true);
		$criteria->compare('homeNameBig',$this->homeNameBig,true);
		$criteria->compare('homeNameGb',$this->homeNameGb,true);
		$criteria->compare('homeNameKr',$this->homeNameKr,true);
		$criteria->compare('homeNameVn',$this->homeNameVn,true);
		$criteria->compare('homeNameLa',$this->homeNameLa,true);
		$criteria->compare('awayName',$this->awayName,true);
		$criteria->compare('awayNameEn',$this->awayNameEn,true);
		$criteria->compare('awayNameTh',$this->awayNameTh,true);
		$criteria->compare('awayNameBig',$this->awayNameBig,true);
		$criteria->compare('awayNameGb',$this->awayNameGb,true);
		$criteria->compare('awayNameKr',$this->awayNameKr,true);
		$criteria->compare('awayNameVn',$this->awayNameVn,true);
		$criteria->compare('awayNameLa',$this->awayNameLa,true);
		$criteria->compare('hdp',$this->hdp,true);
		$criteria->compare('hdp_home',$this->hdp_home,true);
		$criteria->compare('hdp_away',$this->hdp_away,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('match_id',$this->match_id,true);
		$criteria->compare('h32x32',$this->h32x32,true);
		$criteria->compare('h64x64',$this->h64x64,true);
		$criteria->compare('h256x256',$this->h256x256,true);
		$criteria->compare('a32x32',$this->a32x32,true);
		$criteria->compare('a64x64',$this->a64x64,true);
		$criteria->compare('a256x256',$this->a256x256,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('remove',$this->remove,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}