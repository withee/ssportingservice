<?php

/**
 * This is the model class for table "gp_statistic".
 *
 * The followings are the available columns in table 'gp_statistic':
 * @property string $id
 * @property string $fb_uid
 * @property string $pts
 * @property double $stamp_gp
 * @property double $changed_gp
 * @property double $overall_gp
 * @property double $real_gp
 * @property integer $rank
 * @property integer $rank_change
 * @property integer $all_rank
 * @property integer $all_rank_change
 * @property integer $play
 * @property string $stamp_at
 * @property string $update_at
 * @property string $season
 */
class Gpstatistic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gpstatistic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gp_statistic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fb_uid, stamp_at, update_at, season', 'required'),
			array('rank, rank_change, all_rank, all_rank_change, play', 'numerical', 'integerOnly'=>true),
			array('stamp_gp, changed_gp, overall_gp, real_gp', 'numerical'),
			array('fb_uid', 'length', 'max'=>255),
			array('pts', 'length', 'max'=>11),
			array('season', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fb_uid, pts, stamp_gp, changed_gp, overall_gp, real_gp, rank, rank_change, all_rank, all_rank_change, play, stamp_at, update_at, season', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fb_uid' => 'Fb Uid',
			'pts' => 'Pts',
			'stamp_gp' => 'Stamp Gp',
			'changed_gp' => 'Changed Gp',
			'overall_gp' => 'Overall Gp',
			'real_gp' => 'Real Gp',
			'rank' => 'Rank',
			'rank_change' => 'Rank Change',
			'all_rank' => 'All Rank',
			'all_rank_change' => 'All Rank Change',
			'play' => 'Play',
			'stamp_at' => 'Stamp At',
			'update_at' => 'Update At',
			'season' => 'Season',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('fb_uid',$this->fb_uid,true);
		$criteria->compare('pts',$this->pts,true);
		$criteria->compare('stamp_gp',$this->stamp_gp);
		$criteria->compare('changed_gp',$this->changed_gp);
		$criteria->compare('overall_gp',$this->overall_gp);
		$criteria->compare('real_gp',$this->real_gp);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('rank_change',$this->rank_change);
		$criteria->compare('all_rank',$this->all_rank);
		$criteria->compare('all_rank_change',$this->all_rank_change);
		$criteria->compare('play',$this->play);
		$criteria->compare('stamp_at',$this->stamp_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('season',$this->season,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}