<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property string $newsId
 * @property string $titleEn
 * @property string $titleBig
 * @property string $titleGb
 * @property string $titleTh
 * @property string $titleKr
 * @property string $titleVn
 * @property string $titleLa
 * @property string $shortDescriptionEn
 * @property string $shortDescriptionBig
 * @property string $shortDescriptionGb
 * @property string $shortDescriptionTh
 * @property string $shortDescriptionKr
 * @property string $shortDescriptionVn
 * @property string $shortDescriptionLa
 * @property string $contentEn
 * @property string $contentBig
 * @property string $contentGb
 * @property string $contentTh
 * @property string $contentKr
 * @property string $contentVn
 * @property string $contentLa
 * @property integer $leagueId1
 * @property integer $leagueId2
 * @property integer $leagueId3
 * @property integer $leagueId4
 * @property integer $teamId1
 * @property integer $teamId2
 * @property integer $teamId3
 * @property integer $teamId4
 * @property string $createDatetime
 * @property string $updateDatetime
 * @property string $imageLink
 * @property string $like
 * @property string $report
 * @property string $worldcups
 */
class News extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('createDatetime, updateDatetime, imageLink', 'required'),
			array('leagueId1, leagueId2, leagueId3, leagueId4, teamId1, teamId2, teamId3, teamId4', 'numerical', 'integerOnly'=>true),
			array('titleEn, titleBig, titleGb, titleTh, titleKr, titleVn, titleLa', 'length', 'max'=>255),
			//array('createDatetime, updateDatetime', 'length', 'max'=>11),
			array('like, report', 'length', 'max'=>10),
			array('worldcups', 'length', 'max'=>1),
			array('shortDescriptionEn, shortDescriptionBig, shortDescriptionGb, shortDescriptionTh, shortDescriptionKr, shortDescriptionVn, shortDescriptionLa, contentEn, contentBig, contentGb, contentTh, contentKr, contentVn, contentLa', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('newsId, titleEn, titleBig, titleGb, titleTh, titleKr, titleVn, titleLa, shortDescriptionEn, shortDescriptionBig, shortDescriptionGb, shortDescriptionTh, shortDescriptionKr, shortDescriptionVn, shortDescriptionLa, contentEn, contentBig, contentGb, contentTh, contentKr, contentVn, contentLa, leagueId1, leagueId2, leagueId3, leagueId4, teamId1, teamId2, teamId3, teamId4, createDatetime, updateDatetime, imageLink, like, report, worldcups', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'newsId' => 'News',
			'titleEn' => 'Title En',
			'titleBig' => 'Title Big',
			'titleGb' => 'Title Gb',
			'titleTh' => 'Title Th',
			'titleKr' => 'Title Kr',
			'titleVn' => 'Title Vn',
			'titleLa' => 'Title La',
			'shortDescriptionEn' => 'Short Description En',
			'shortDescriptionBig' => 'Short Description Big',
			'shortDescriptionGb' => 'Short Description Gb',
			'shortDescriptionTh' => 'Short Description Th',
			'shortDescriptionKr' => 'Short Description Kr',
			'shortDescriptionVn' => 'Short Description Vn',
			'shortDescriptionLa' => 'Short Description La',
			'contentEn' => 'Content En',
			'contentBig' => 'Content Big',
			'contentGb' => 'Content Gb',
			'contentTh' => 'Content Th',
			'contentKr' => 'Content Kr',
			'contentVn' => 'Content Vn',
			'contentLa' => 'Content La',
			'leagueId1' => 'League Id1',
			'leagueId2' => 'League Id2',
			'leagueId3' => 'League Id3',
			'leagueId4' => 'League Id4',
			'teamId1' => 'Team Id1',
			'teamId2' => 'Team Id2',
			'teamId3' => 'Team Id3',
			'teamId4' => 'Team Id4',
			'createDatetime' => 'Create Datetime',
			'updateDatetime' => 'Update Datetime',
			'imageLink' => 'Image Link',
			'like' => 'Like',
			'report' => 'Report',
			'worldcups' => 'Worldcups',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('newsId',$this->newsId,true);
		$criteria->compare('titleEn',$this->titleEn,true);
		$criteria->compare('titleBig',$this->titleBig,true);
		$criteria->compare('titleGb',$this->titleGb,true);
		$criteria->compare('titleTh',$this->titleTh,true);
		$criteria->compare('titleKr',$this->titleKr,true);
		$criteria->compare('titleVn',$this->titleVn,true);
		$criteria->compare('titleLa',$this->titleLa,true);
		$criteria->compare('shortDescriptionEn',$this->shortDescriptionEn,true);
		$criteria->compare('shortDescriptionBig',$this->shortDescriptionBig,true);
		$criteria->compare('shortDescriptionGb',$this->shortDescriptionGb,true);
		$criteria->compare('shortDescriptionTh',$this->shortDescriptionTh,true);
		$criteria->compare('shortDescriptionKr',$this->shortDescriptionKr,true);
		$criteria->compare('shortDescriptionVn',$this->shortDescriptionVn,true);
		$criteria->compare('shortDescriptionLa',$this->shortDescriptionLa,true);
		$criteria->compare('contentEn',$this->contentEn,true);
		$criteria->compare('contentBig',$this->contentBig,true);
		$criteria->compare('contentGb',$this->contentGb,true);
		$criteria->compare('contentTh',$this->contentTh,true);
		$criteria->compare('contentKr',$this->contentKr,true);
		$criteria->compare('contentVn',$this->contentVn,true);
		$criteria->compare('contentLa',$this->contentLa,true);
		$criteria->compare('leagueId1',$this->leagueId1);
		$criteria->compare('leagueId2',$this->leagueId2);
		$criteria->compare('leagueId3',$this->leagueId3);
		$criteria->compare('leagueId4',$this->leagueId4);
		$criteria->compare('teamId1',$this->teamId1);
		$criteria->compare('teamId2',$this->teamId2);
		$criteria->compare('teamId3',$this->teamId3);
		$criteria->compare('teamId4',$this->teamId4);
		$criteria->compare('createDatetime',$this->createDatetime,true);
		$criteria->compare('updateDatetime',$this->updateDatetime,true);
		$criteria->compare('imageLink',$this->imageLink,true);
		$criteria->compare('like',$this->like,true);
		$criteria->compare('report',$this->report,true);
		$criteria->compare('worldcups',$this->worldcups,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}