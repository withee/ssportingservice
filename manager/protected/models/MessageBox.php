<?php

/**
 * This is the model class for table "message_box".
 *
 * The followings are the available columns in table 'message_box':
 * @property string $id
 * @property string $sender
 * @property string $receiver
 * @property string $key
 * @property string $message
 * @property string $media
 * @property string $send_at
 * @property string $stamp_at
 * @property string $seen
 * @property string $remove
 */
class MessageBox extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message_box';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('message, send_at', 'required'),
			array('sender, receiver, stamp_at', 'length', 'max'=>15),
			array('key', 'length', 'max'=>256),
			array('message, media', 'length', 'max'=>512),
			array('seen, remove', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sender, receiver, key, message, media, send_at, stamp_at, seen, remove', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender' => 'Sender',
			'receiver' => 'Receiver',
			'key' => 'Key',
			'message' => 'Message',
			'media' => 'Media',
			'send_at' => 'Send At',
			'stamp_at' => 'Stamp At',
			'seen' => 'Seen',
			'remove' => 'Remove',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sender',$this->sender,true);
		$criteria->compare('receiver',$this->receiver,true);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('media',$this->media,true);
		$criteria->compare('send_at',$this->send_at,true);
		$criteria->compare('stamp_at',$this->stamp_at,true);
		$criteria->compare('seen',$this->seen,true);
		$criteria->compare('remove',$this->remove,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MessageBox the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
