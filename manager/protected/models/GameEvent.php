<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GameEvent
 *
 * @author mrsyrop
 */
//require "../../Carbon/src/Carbon/Carbon.php";
//use Carbon\Carbon;

class GameEvent {

    protected $file = '../system_config/gameevent.json';

    protected function create($arr = array()) {
        file_put_contents('../system_config/gameevent.json', json_encode($arr));
        chmod('../system_config/gameevent.json', 0777);
    }

    public function lastAccess($mid) {
        if (!file_exists($this->file)) {
            $this->create();
        }
        $midlist = json_decode(file_get_contents($this->file), TRUE);
        //$lastaccess = filemtime($this->file);
        $lastaccess = strtotime(date("Y-m-d"));
        if (array_key_exists($mid, $midlist)) {
            $lastaccess = $midlist[$mid];
        }
        return $lastaccess;
    }

    public function set($mid) {
        if (!file_exists($this->file)) {
            $this->create();
        }
        $midlist = json_decode(file_get_contents($this->file), TRUE);
        if (count($mid) < 5) {
            $midlist[$mid] = strtotime(date("Y-m-d H:i:s"));
        } else {
            $midlist = array();
            $midlist[$mid] = strtotime(date("Y-m-d H:i:s"));
        }
        $this->create($midlist);
    }

    public function getDup($mid, $now, $lastaccess) {
        $duplicate = FALSE;
        if (!file_exists($this->file)) {
            $this->create();
        }
        //$lastaccess = fileatime($this->file);
//        $now = Canbon::now();

        $diffmin = $now->diffInMinutes($lastaccess);

        $midlist = json_decode(file_get_contents($this->file), TRUE);
        if (array_key_exists($mid, $midlist)) {
            //echo "exist\n";
            if ($diffmin >= 1) {
                $duplicate = FALSE;
                //$this->create();
                $this->set($mid);
            } else {
                $duplicate = TRUE;
            }
            //  var_dump($duplicate);
        } else {
            $this->set($mid);
        }
        return $duplicate;
    }

}
