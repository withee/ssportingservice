<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Web Application',
    'timeZone' => 'Asia/Bangkok',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.YiiMongoDbSuite.*',
        'ext.helpers.*',
    ),
    'homeUrl' => '/',
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123456',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
//        'gii' => array(
//            'class' => 'system.gii.GiiModule',
//            'password' => '123',
//            // add additional generator path
//            'generatorPaths' => array(
//                'ext.YiiMongoDbSuite.gii'
//            ),
//        ),
    ),
    // application components
    'components' => array(
        'image'=>array(
            'class'=>'ext.image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
            'params'=>array('directory'=>'/opt/local/bin'),
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/testdrive.db',
        ),
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=api.ssporting.com;dbname=dev_ssporting_swcp',
            'emulatePrepare' => true,
            'username' => 'dev',
            'password' => '@x1234',
            'charset' => 'utf8',
        ),


//        'test_db' => array(
//            'connectionString' => 'mysql:host=localhost;dbname=lottery_service',
//            'emulatePrepare' => true,
//            'username' => 'ws',
//            'password' => 'x3@x1234',
//            'charset' => 'utf8',
//            'class'  => 'CDbConnection',
//        'mongodb' => array(
//            'class' => 'EMongoDB',
//            'connectionString' => 'mongodb://localhost',
//            'dbName' => '1ivescore_swcp',
//            'fsyncFlag' => true,
//            'safeFlag' => true,
//            'useCursor' => false
//        ),

      'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
