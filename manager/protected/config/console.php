<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    // application components
    'components' => array(
        /*
          'db' => array(
          'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/testdrive.db',
          ),
         */
        // uncomment the following to use a MySQL database
//        'db' => array(
//            'connectionString' => 'mysql:host=127.0.0.1;dbname=dev_ssporting_swcp',
//            'emulatePrepare' => true,
//            'username' => 'dev',
//            'password' => '@x1234',
//            'charset' => 'utf8',
//            'tablePrefix' => '',
//        ),

        'db' => array(
            'connectionString' => 'mysql:host=127.0.0.1;dbname=1ivescore_swcp',
            'emulatePrepare' => true,
            'username' => '1ivescore',
            'password' => '1ivescore',
            'charset' => 'utf8',
            'tablePrefix' => '',
        ),
    ),
);
