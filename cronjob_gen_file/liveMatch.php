<?php

require 'DBConfig.php';

$sql = "select live_match.*,gt.teamNameTh as gnTh,ht.teamNameTh as hnTh FROM live_match 
        left join lang_team  as gt on gt.teamName = live_match.gn
        left join lang_team  as ht on ht.teamName = live_match.hn
        where showDate=current_date and new='Y' group by mid order by date ASC";


$db = DBConfig::getConnection();
$stmt = $db->query($sql);
$list = $stmt->fetchAll(PDO::FETCH_OBJ);
$sql = "select live_league.*,lang_league.leagueNameTh,lang_competition.comNameTh from live_league 
            left join lang_league on lang_league.leagueName = live_league.ln
            left join lang_competition on  lang_competition.comName = live_league.kn
            where date=current_date and new='Y' order by competitionId ASC,leagueId ASC,subleagueId ASC";
$stmt = $db->query($sql);
$leagueList = $stmt->fetchAll(PDO::FETCH_OBJ);
$db = null;
$c3 = time();
$obj = array(
    'live_match' => $list,
    'live_league' => $leagueList,
    'c3' => $c3,
);
echo file_put_contents("cronjob_gen_file/files/liveMatch.json", json_encode($obj));

?>
