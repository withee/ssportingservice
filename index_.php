<?php

require 'Slim/Slim.php';
require 'DBConfig.php';
$app = new Slim();
$app->contentType('application/json; charset=utf-8');
//$app->get('/wines', 'getWines');
//$app->get('/wines/:id',  'getWine');
//$app->get('/wines/search/:query', 'findByName');
//$app->post('/wines', 'addWine');
//$app->put('/wines/:id', 'updateWine');
//$app->delete('/wines/:id',   'deleteWine');
$app->get('/liveMatch', 'getLiveMatch');
$app->get('/liveMatchUpdate', 'getLiveMatchUpdate');
$app->get('/statTable/:id', 'getStatTable');
$app->get('/liveMatchEvent/:id', 'getLiveMatchEvent');
$app->run();

function getLiveMatch() {
    $sql = "select * FROM live_match where showDate=current_date order by date ASC";
    try {
        $db = DBConfig::getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select *from live_league where date=current_date order by competitionId ASC,leagueId ASC,subleagueId ASC";
        $stmt = $db->query($sql);
        $leagueList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        echo '{"live_match": ' . json_encode($list) . '}{"live_league":' . json_encode($leagueList) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getLiveMatchEvent($mid) {
    $sql = "select * FROM live_match_event where mid=$mid";
    try {
        $db = DBConfig::getConnection();
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"live_match_event": ' . json_encode($list) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getLiveMatchUpdate() {

    $dateSql = "select date FROM live_match_update order by date";
    try {
        $db = DBConfig::getConnection();
        $stmt = $db->query($dateSql);
        $date = $stmt->fetch();
        $sql = "select * FROM live_match_update where date='" . $date['date'] . "'";
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"live_match_update": ' . json_encode($list) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getStatTable($leagueId) {
//    $request = Slim::getInstance()->request();
//    $tnPk =$request->params("tnPk");
//    $leagueId= $request->params("leagueId");        
    $query = "select *from stat_table where leagueId=$leagueId";
    try {
        $db = DBConfig::getConnection();
        $stmt = $db->query($query);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo '{"stat_table": ' . json_encode($list) . '}';
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

?>
