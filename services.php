<?php

require 'DBConfig.php';

$cmd = isset($_REQUEST['cmd']) ? $_REQUEST['cmd'] : "";
$db = DBConfig::getConnection();
switch ($cmd) {
    case 'openapp':
        $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
        $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
        $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
        $country = isset($_REQUEST['country']) ? $_REQUEST['country'] : '';
        $device_timezone = isset($_REQUEST['device_timezone']) ? $_REQUEST['device_timezone'] : '';
        $lang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'en';
        $rs = $db->exec("INSERT INTO `used_list` (`apple_id`, `used_at`, `app`) VALUES ('$apple_id', NOW(), '$app');");
        $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
        if ($rs > 0) {
            $result['success'] = true;
            $result['desc'] = "Counted";
            $id = $db->query("SELECT LAST_INSERT_ID()")->fetch();
            $id = (int) $id[0];
            $result['result'] = $db->query("SELECT * FROM used_list WHERE `id`=$id")->fetchAll(PDO::FETCH_OBJ);
        }
        $sql = "select *from request_notification_count where device_id ='$apple_id' and app='$app' and platform='$platform'";
        $stmt = $db->query($sql);
        $obj = $stmt->fetch(PDO::FETCH_OBJ);
        //print_r($obj);exit;
        if ($obj == null) {
            $sql = "insert into request_notification_count (device_id,app,platform,qty,country,device_timezone,lang) values('" . $apple_id . "','$app','$platform',0,'$country','$device_timezone','$lang')";
            $db->exec($sql);
        } else {
            $sql = "update request_notification_count set qty = 0,country='$country',device_timezone='$device_timezone',lang='$lang' where device_id='$apple_id' and  app='$app' and platform='$platform'";
            $db->exec($sql);
        }
        echo json_encode($result);
        break;
    case 'RequestNotification':
        $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
        $number =strlen($apple_id);

        $match_id = isset($_REQUEST['match_id']) ? $_REQUEST['match_id'] : '';
        $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
        if($number<=20){
            $platform='web';
        }
        $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
        $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "Y";
        $rs = $db->query("SELECT * FROM request_notification WHERE match_id=$match_id AND apple_id='$apple_id' AND app='$app'")->fetch();
        //var_dump($rs);
        //echo 1;exit;
        $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
        if (empty($rs)) {
            $rs = $db->exec("INSERT INTO `request_notification` (`match_id`, `apple_id`, `create_at`, `notification`, `app`,`platform`) VALUES ($match_id, '$apple_id', NOW(), '$status', '$app','$platform')");


            $result['success'] = true;
            $result['desc'] = "New insert";
        } else {
            $res = $db->exec("UPDATE `request_notification` SET `notification`='$status' WHERE  `id`={$rs['id']}");
            if ($res > 0) {
                $result['success'] = true;
                $result['desc'] = "Status updated";
            }
        }
        $r = $db->query("SELECT * FROM `request_notification` WHERE apple_id='$apple_id' AND app='$app' AND notification='Y'")->fetchAll();
        if (!empty($r)) {
            $result['result'] = $r;
        }

        echo json_encode($result);
        break;
    case 'NotificationList':
        $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
        $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
        $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
        $rs = $db->query("SELECT * FROM `request_notification` WHERE apple_id='$apple_id' AND app='$app' AND notification='Y'")->fetchAll();
        $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
        if (!empty($rs)) {
            $result['success'] = true;
            $result['desc'] = "get list";
            $result['result'] = $rs;
        }
        echo json_encode($result);
        break;
    default:
        $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
        echo json_encode($result);
        break;
}
?>