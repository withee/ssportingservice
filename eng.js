var LANGUAGE_INDEX = 2;
var SOUND_CT = "Sound Off";
var SOUND_OT = "Sound on";
var SOUND_CSRC = "http://img.7m.cn/img2/lb_a3.gif";
var SOUND_OSRC = "http://img.7m.cn/img2/lb_a4.gif"
var WIN_CT = "Close Prompt Window";
var WIN_OT = "Open Prompt Window";
var WIN_CSRC = "http://img.7m.cn/img2/twin1.gif";
var WIN_OSRC = "http://img.7m.cn/img2/twin0.gif"
var RQ_ARR = ["0", "0/0.5", "0.5", "0.5/1", "1", "1/1.5", "1.5", "1.5/2", "2", "2/2.5", "2.5", "2.5/3", "3", "3/3.5", "3.5", "3.5/4", "4", "4/4.5", "4.5", "4.5/5", "5", "5/5.5", "5.5", "5.5/6", "6", "6/6.5", "6.5", "6.5/7", "7", "7/7.5", "7.5", "7.5/8", "8", "8/8.5", "8.5", "8.5/9", "9", "9/9.5", "9.5", "9.5/10", "10", "10/10.5", "10.5", "10.5/11", "11", "11/11.5", "11.5", "11.5/12", "12", "12/12.5", "12.5", "12.5/13", "13", "13/13.5", "13.5", "13.5/14", "14", "14/14.5", "14.5", "14.5/15", "15", "15/15.5", "15.5", "15.5/16", "16", "16/16.5", "16.5", "16.5/17", "17", "17/17.5", "17.5", "17.5/18", "18", "18/18.5", "18.5", "18.5/19", "19", "19/19.5", "19.5", "19.5/20", "20"];
var STATE_ARR = ["", "1st", "HT", "2nd", "FT", "Pause", "Cancel", "Extra", "Extra", "Extra", "120 Minutes", "Pen", "Finished", "Postponed", "Cut", "Undecided", "Gold", ""];
var RESUME_ARR = ["90 minutes", "120 minutes", "Penalty kicks", "100 minutes", "end of a show", "Total scores", "Double bouts", "Score during play-off", "Live"];
var WEEK_STR = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".split(",");
var RESULE_STR = "Result";
var FIXTURE_LINK = "<a href=\"http://data.7m.cn/fixture_data/sc_en1.shtml\" target=\"_top\">View Fixture</a>";
var LIVE_TABLE_HEAD = ["&nbsp;", "Contest", "Time", "Status", "Home", "Score", "Away", "HT", "&nbsp;", '', "TOP"];
var PK_LINK_FUN_NAME = "ShowPk_en";
var RPK_LINK_FUN_NAME = "ShowCPk_en";
var LBPK_LINK_FUN_NAME = "ShowLBPk_en";
var TEAM_LINK_FUN_NAME = "Team_en";
var DETAILS_LINK_FUN_NAME = "ShowDetails_en";
var ANALYSE_LINK_FUN_NAME = "ShowAnalyse_en";
var EURO1X2_LINK_FUN_NAME = "Show1x2_en";
var PLAYER_LINK = "Player_en";
var ZLK_LINK_FUN_NAME = "zlk_en";
var MC_LINK_FUN_NAME = "MC_en";
var ANALYSE_STR = '<img src="http://img.7m.cn/icon/Analyse_1.gif" border="0">';
var EURO1X2_STR = '<img src="http://img.7m.cn/icon/o1.gif" border="0">';
var ASSIGN_BEFORE_STR = "Assign before this";
var BEFORE_STR = "before";
var CURRENT_WIN_STR = "W";
var GOAL_TIPS_WIDTH = 560;
var SPEAK_TITLE = "";
var WEATHER_ARR = ["", "Sunny", "Scattered Cloudy", "Mostly Cloudy", "Overcast", "Light Rain", "Moderate Rain-->Heavy Rain", "Thundershowers", "Thunderstorms", "Light Snow", "Pour", "Fine", "Partly Cloudy", "Scattered Cloudy", "Mostly Cloudy", "Sleet", "", "", "Partly Cloudy", "Light Thunderstorms", "Light Showers", "Mist", "Freezing Fog", "Occasional Light Rain", "Moderate Rain", "Light Snow Showers", "Light Rain", "Snow Showers", "Windy and Dusty", "Low Blowing Snow", "Heavy Snow Showers", "Moderate Snow"];
var OPENMATCHLIST = "open match list";
var CLOSEMATCHLIST = "close match list";
var GOALDETAIL = "Click to view detail";
var BASE_HANDICAP = "Early Reference: ";
var HOME_HANDICAP_STR = "-?";
var AWAY_HANDICAP_STR = "+?";
var NORECORED = 'No matched record found';
var MATCHCENTER_STR = "Match center";
var NODATA_IMG = '<img src="http://img.7m.cn/v2/nodata_en.jpg" alt="No data at the moment" title="No data at the moment" />';
var NODATA_STR = 'No data currently. If you have logged in, please check <a href="javascript:selectMatch()">Selected Events</a>; if not, please try <a href="javascript:live_f.location.reload()">Refresh the page</a>.';
var DOMAIN_STR = "www.7msport.com";
var STR_TOP = ["Favourite","Restore Defaults","Top","Cancel Top"];
/*member*/
var USERPASS_ERROR = "User name or password is wrong, please try later.";
var LOGIN_FAILED = "Login fails, please try again later.";
var USERNAME_NULL = "Please type in user name.";
var PASSWORD_NULL = "Enter password please";
var SIGN_STR = "Login";
var LOGIN_STR = "Loading...";
/*end member*/

function MAKEDTROW(sj, ssj) {
	return ssj[2] + '/' + ssj[1] + '/' + ssj[0] + ' (' + WEEK_STR[sj.getDay()] + ')';
}