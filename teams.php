<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/js/update_lang.js?timestamp=<?php echo time()?>"></script>
    </head>
    <body>
    <body>
        <h2><a href="/com.php">กลับหน้าประเทศทั้งหมด</a></h2>
        <h1>ทีมทั้งหมด</h1>
        <table>
            <thead>
                <tr>
                    <td>ชื่อภาษาอังกฤษ</td>
                    <td>ชื่อภาษาไทย</td>

                </tr>
            </thead>
            <tbody>
                <?php
                $competitionId = $_GET['competitionId'];
                require 'DBConfig.php';
                $db = DBConfig::getConnection();
                $sql = "select team.*,lang_team.teamNameTh from team
                left join lang_team on lang_team.tid=team.tid
                where team.cid=$competitionId and team.tid >0
                order by tn ASC";
                //echo $sql;
                $stmt = $db->query($sql);
                $list = $stmt->fetchAll(PDO::FETCH_OBJ);
                //echo count($list);
                foreach ($list as $obj) {
                    //print_r($obj);
                    ?>
                    <tr>
                        <td><?php echo $obj->tn ?></td>
                        <td><input type="text" value="<?php echo $obj->teamNameTh ?>"  class="input-update" table="lang_team" attr="teamName" attrValue="<?php echo $obj->tn ?>" attrTh="teamNameTh"  tnPk ="<?php echo $obj->tnPk?>" tid="<?php echo $obj->tid?>"/></td>

                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </body>
</html>