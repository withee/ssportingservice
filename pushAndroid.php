<?php

define("GOOGLE_API_KEY", "AIzaSyDmNlVG1YUuBIkdUouJWT8ipWwjVLmcuok");
define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");

function send_gcm_notify($reg_id, $message) {

    $fields = array(
        'registration_ids' => array($reg_id),
        'data' => array("message" => $message),
    );

    $headers = array(
        'Authorization: key=' . GOOGLE_API_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Problem occurred: ' . curl_error($ch));
    }

    curl_close($ch);
    echo $result;
}
//$reg_id = "APA91bEK5_HoGK4i8KjH5W9rl1vrPKpnrzQCuhDDrFsAVhFbDBqQdZh7VvmemyqIMxMYps0C1P9SqRtRe7uXGWDgNhnyz_lBaHGEeHtVgE0DWGcYthFR4iao_pMR3RYH2c9B0JpC58z8DpF5XrC6o2ypsgBUSuxyvg";
//$msg = "Google Cloud Messaging working well";
//send_gcm_notify($reg_id, $msg);
?>