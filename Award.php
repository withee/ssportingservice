<?php

require 'TotalStatement.php';

//$uidList = array(1,2,3,4,5,6,7,8,9,10);
//$type = 'top_sgold';
//
//award($uidList,$type);

function award($uidList, $type) {
    $t = new TotalStatement();
    //$config = json_decode(file_get_contents('/media/DATA/devPro/swcp-web/system_config/file.json'), true);
    $config = json_decode(file_get_contents('/home/dev/domains/ssporting.com/api/public_html/system_config/file.json'), true);
    $dataList = array();

    foreach ($uidList as $key => $uid) {
        $user = $t->getUser($uid);
        $data = array();

        $before_sgold = $user->sgold;
        $before_scoin = $user->scoin;
        $before_diamond = $user->diamond;

        $sgold_outcome = 0;
        $scoin_outcome = 0;
        $diamond_outcome = 0;

        $balance_sgold = $before_sgold;
        $balance_scoin = $before_scoin;
        $balance_diamond = $before_diamond;


        $db = DBConfig::getConnection();

        if ($type === "top_sgold") {
            $award = $config["award_top_sgold"][$key];
        } else if ($type === "top_ranking") {
            $award = $config["award_top_ranking"][$key];
        } else {
            $award = $config["award_top_league"][$key];
        }

        $sgold_income = $award["sgold"];
        $scoin_income = $award["scoin"];
        $diamond_income = $award["diamond"];

        $balance_sgold += floatval($sgold_income);
        $balance_scoin += floatval($scoin_income);
        $balance_diamond += floatval($diamond_income);

        $sql = $t->getTotalStatementQuery($uid, 'award', "'" . $type . "'", $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, 'success', "NULL", "NULL", '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
        $sql_update_facebook = "update facebook_user set sgold=$balance_sgold, scoin=$balance_scoin, diamond=$balance_diamond where uid = $uid";
        $db->exec($sql);
        $db->exec($sql_update_facebook);
        $data['status'] = 'success';
        $data['description'] = '';
        $data['balance_sgold'] = $before_sgold;
        $data['balance_scoin'] = $before_scoin;
        $data['balance_diamond'] = $balance_diamond;
        $dataList[] = $data;
    }

    //echo json_encode($dataList);
}
