<?php

header('Content-Type: application/json; charset=utf-8');
$data = array(
    'status' => 1,
    'message' => '',
    'fullPath' => '',
    'smallPath' => '',
);
$date = new DateTime();
$allowedExts = array("gif", "jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
//var_dump($temp);
//exit();
$extension = end($temp);
if ((($_FILES["file"]["type"] == "image/gif")
        || ($_FILES["file"]["type"] == "image/jpeg")
        || ($_FILES["file"]["type"] == "image/jpg")
        || ($_FILES["file"]["type"] == "image/pjpeg")
        || ($_FILES["file"]["type"] == "image/x-png")
        || ($_FILES["file"]["type"] == "image/png"))
        && ($_FILES["file"]["size"] < 3000000)
        && in_array($extension, $allowedExts)) {
    if ($_FILES["file"]["error"] > 0) {
        //echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
        $data['status'] = 2;
        $data['message'] = $_FILES["file"]["error"];
    } else {
//        echo "Upload: " . $_FILES["file"]["name"] . "<br>";
//        echo "Type: " . $_FILES["file"]["type"] . "<br>";
//        echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
//        echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";

        if (file_exists("upload/" . $date->getTimestamp() . '_' . $_FILES["file"]["name"])) {
            //echo $date->getTimestamp().'_'.$_FILES["file"]["name"] . " already exists. ";
            $data['status'] = 2;
            $data['message'] = 'Please try again.';
        } else {
            $timestamp = $date->getTimestamp();
            move_uploaded_file($_FILES["file"]["tmp_name"], "upload/" . $timestamp . '_' . $_FILES["file"]["name"]);
            $data['message'] = 'Upload success.';
            $data['fullPath'] = "upload/" . $timestamp . '_' . $_FILES["file"]["name"];
            $data['smallPath'] = 'upload/small_' . $timestamp . '_' . $_FILES['file']['name'];
            resize($data['fullPath'], $data['smallPath'], 200);
        }
    }
} else {
    //echo "Invalid file";
    $data['status'] == 2;
    $data['message'] = 'Invalid file. ' . $_FILES["file"]["name"];
}
echo json_encode($data);

function resize($path, $newPath, $max) {
    //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
    $size = GetimageSize($path);
    $_width = $size[0];
    $_height = $size[1];
    $mime = $size['mime'];
    $width = null;
    $height = null;
    if ($_width > $_height) {
        $width = $max;
        $height = round($_height * ($max / $_width));
    } else {
        $height = $max;
        $width = round($_width * ($max / $_height));
    }
    //$height=round($width*$size[1]/$size[0]);
    $new_image_ext = null;
    switch ($mime) {
        case 'image/jpeg':
            $image_create_func = 'imagecreatefromjpeg';
            $image_save_func = 'imagejpeg';
            $new_image_ext = 'jpg';
            break;

        case 'image/png':
            $image_create_func = 'imagecreatefrompng';
            $image_save_func = 'imagepng';
            $new_image_ext = 'png';
            break;

        case 'image/gif':
            $image_create_func = 'imagecreatefromgif';
            $image_save_func = 'imagegif';
            $new_image_ext = 'gif';
            break;

        default:
            throw Exception('Unknown image type.');
    }
    if ($new_image_ext == 'jpg') {
        $images_orig = ImageCreateFromJPEG($path);
    } else if ($new_image_ext == 'png') {
        $images_orig = ImageCreateFromPNG($path);
    } else if ($new_image_ext == 'gif') {
        $images_orig = ImageCreateFromGIF($path);
    }

    $photoX = ImagesX($images_orig);
    $photoY = ImagesY($images_orig);
    $images_fin = ImageCreateTrueColor($width, $height);

    //ImageJPEG($images_fin, $newPath);
    if ($new_image_ext == 'jpg') {
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageJPEG($images_fin, $newPath);
    } else if ($new_image_ext == 'png') {
        imagealphablending($images_fin, false);
        imagesavealpha($images_fin, true);
        $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
        imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
        imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
        ImagePNG($images_fin, $newPath);
    } else if ($new_image_ext == 'gif') {
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageGIF($images_fin, $newPath);
    }
    ImageDestroy($images_orig);
    ImageDestroy($images_fin);
}

