$(document).ready(function() {

    $('input.input-update').blur(function() {

        var value = $(this).val();
        if (value) {
            var table = $(this).attr('table');
            var attr = $(this).attr('attr');
            var attrValue = $(this).attr('attrValue');
            var attrTh = $(this).attr('attrTh');
            var leagueId = $(this).attr('leagueId');
            var competitionId = $(this).attr('competitionId');
            var tnPk = $(this).attr('tnPk');
            var tid = $(this).attr('tid');
            var comName = $(this).attr('comName');
            var comNamePk = $(this).attr('comNamePk');
            $.post('/updateLang.php', {
                table: table,
                attr: attr,
                attrValue: attrValue,
                attrTh: attrTh,
                attrThValue: value,
                leagueId: leagueId,
                competitionId: competitionId,
                tnPk: tnPk,
                tid: tid,
                comName: comName,
                comNamePk: comNamePk

            }, function(response) {

            });
        }
    });
//    $('input.input-update').keypress(function(e){
//        if(e.which == 13) {
//            var value = $(this).val();
//            if(value){
//                var table = $(this).attr('table');
//                var attr = $(this).attr('attr');
//                var attrValue = $(this).attr('attrValue');
//                var attrTh = $(this).attr('attrTh');
//                $.post('/updateLang',{
//                    table:table,
//                    attr:attr,
//                    attrValue:attrValue,
//                    attrTh:attrTh,
//                    attrThValue:value
//            
//                },function(reponse){
//            
//                    });
//                
//            }
//        }
//    });
});