<?php
/**
 * Created by PhpStorm.
 * User: Withee
 * Date: 11-Nov-15
 * Time: 13:31
 */
require 'DBConfig.php';

$dbs = DBConfig::getServerConnection();
$dbl = DBConfig::getConnection();

$alltable = $dbs->query("show tables");
foreach ($alltable as $key => $table) {
    echo $key . ':' . $table[0] . "\n";
    $limit = 1000;
    $resultsize = $limit;
    $round = 0;
    while ($resultsize == $limit) {
        $start = $round * $limit;
        $sql = "SELECT * FROM " . $table[0] . " LIMIT $start,$limit";
        $resultset = $dbs->query($sql);
        $resultsize = 0;
        foreach ($resultset as $rs) {
            $resultsize++;
            $columnlist = "";
            $valuelist = "";
            //print_r($rs);
            $colindex = 0;
            foreach ($rs as $column => $value) {

                if ($colindex % 2 == 0) {
                    if (empty($columnlist)) {
                        $columnlist .= "`$column`";
                    } else {
                        $columnlist .= ",`$column`";
                    }

                    if (empty($valuelist)) {
                        $valuelist .= "'" . addslashes($value) . "'";
                    } else {
                        $valuelist .= ",'" . addslashes($value) . "'";
                    }
                }
                $colindex++;
            }
            $insql = "INSERT IGNORE INTO `{$table[0]}` ($columnlist) VALUES ($valuelist);";
            //echo $insql;
            $success = $dbl->exec($insql);
            if ($success) {
                //echo "====> success\n";
            } else {
                //echo "====> fail\n";
            }
        }
        $round++;


    }

}