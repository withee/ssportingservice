ALTER TABLE `timelines`
	ADD COLUMN `stamped_at` BIGINT UNSIGNED NOT NULL DEFAULT '0' AFTER `updated_at`;
ALTER TABLE `timelines_reply`
	ADD COLUMN `stamped_at` BIGINT UNSIGNED NOT NULL DEFAULT '0' AFTER `created_at`;

