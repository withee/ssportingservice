-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.35-0ubuntu0.12.04.2 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table 1ivescore_swcp.timelines_related
DROP TABLE IF EXISTS `timelines_related`;
CREATE TABLE IF NOT EXISTS `timelines_related` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tl_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_type` enum('status','image','gallery','video','game') DEFAULT NULL,
  `key_list` text,
  `fb_uid` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `related_type` enum('post','comment','like','report') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `stamp_at` int(10) unsigned NOT NULL DEFAULT '0',
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
