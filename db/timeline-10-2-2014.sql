-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.35-0ubuntu0.12.04.2 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table 1ivescore_swcp.media_gallery
DROP TABLE IF EXISTS `media_gallery`;
CREATE TABLE IF NOT EXISTS `media_gallery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fb_uid` varchar(255) NOT NULL,
  `gall_name` varchar(255) NOT NULL DEFAULT 'untitled',
  `gall_type` enum('picture','video','audio') NOT NULL DEFAULT 'picture',
  `created_at` datetime DEFAULT NULL,
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  `desc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table 1ivescore_swcp.media_store
DROP TABLE IF EXISTS `media_store`;
CREATE TABLE IF NOT EXISTS `media_store` (
  `id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `gall_id` int(10) unsigned NOT NULL,
  `path` text CHARACTER SET utf8 NOT NULL,
  `thumbnail` text,
  `created_at` datetime DEFAULT NULL,
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  `desc` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table 1ivescore_swcp.timelines
DROP TABLE IF EXISTS `timelines`;
CREATE TABLE IF NOT EXISTS `timelines` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `fb_uid` varchar(255) NOT NULL,
  `content_type` enum('status','image','gallery','video','game') NOT NULL DEFAULT 'status',
  `key_list` text,
  `content` text NOT NULL,
  `like` int(10) unsigned NOT NULL DEFAULT '0',
  `report` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table 1ivescore_swcp.timelines_reply
DROP TABLE IF EXISTS `timelines_reply`;
CREATE TABLE IF NOT EXISTS `timelines_reply` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(15) unsigned NOT NULL DEFAULT '0',
  `fb_uid` varchar(255) NOT NULL,
  `message` text,
  `image` text,
  `thumbnail` text,
  `like` int(11) unsigned NOT NULL DEFAULT '0',
  `report` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  `reply_to` int(15) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table 1ivescore_swcp.wall_status
DROP TABLE IF EXISTS `wall_status`;
CREATE TABLE IF NOT EXISTS `wall_status` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `fb_uid` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `remove` enum('Y','N') NOT NULL DEFAULT 'N',
  `comment_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
