<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="/bt/docs/assets/css/bootstrap.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }
        </style>
        <link href="/bt/docs/assets/css/bootstrap-responsive.css" rel="stylesheet">
        <script type="text/javascript" src="/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="/js/update_lang.js?timestamp=<?php echo time() ?>"></script>
    </head>
    <body>
        <div class="container">
            <h1>ประเทศทั้งหมด</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ชื่อภาษาไทย</th>
                        <th>ชื่อภาษาอังกฤษ</th>
                        <th>ชื่อภาษาจีนดังเดิ่ม</th>
                        <th>ชื่อภาษาจีนประยุกษ์</th>
                        <th>ชื่อภาษาเกาหลี</th>
                        <th>ชื่อภาษาเวียดนาม</th>
                        <th>ชื่อภาษาลาว</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    require 'DBConfig.php';
                    $db = DBConfig::getConnection();
                    $sql = "select *from competitions 
left join lang_competition on lang_competition.cid = competitions.competitionId";
                    $stmt = $db->query($sql);
                    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
                    //echo count($list);
                    foreach ($list as $obj) {
                        ?>
                        <tr>
                            <td><?php echo $obj->competitionName ?></td> 
                            <td><input type="text" value="<?php echo $obj->comNameTh ?>" id="th-<?php echo $obj->competitionId ?>" class="span2"/></td>
                            <td><input type="text" value="<?php echo $obj->comNameEn ?>" id="en-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><input type="text" value="<?php echo $obj->comNameBig ?>" id="big-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><input type="text" value="<?php echo $obj->comNameGb ?>" id="gb-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><input type="text" value="<?php echo $obj->comNameKr ?>" id="kr-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><input type="text" value="<?php echo $obj->comNameVn ?>" id="vn-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><input type="text" value="<?php echo $obj->comNameLa ?>" id="la-<?php echo $obj->competitionId ?>" class="span2" /></td>
                            <td><a href="/league.php?competitionId=<?php echo $obj->competitionId ?>">ดูลีกทั้งหมด</a>
                            <td><a href="/teams.php?competitionId=<?php echo $obj->competitionId ?>">ดูทีมทั้งหมด</a>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </body>

</html>
