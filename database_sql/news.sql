-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2013 at 03:21 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `1ivescore_swcp`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `newsId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titleEn` varchar(255) DEFAULT NULL,
  `titleBig` varchar(255) DEFAULT NULL,
  `titleGb` varchar(255) DEFAULT NULL,
  `titleTh` varchar(255) DEFAULT NULL,
  `titleKr` varchar(255) DEFAULT NULL,
  `titleVn` varchar(255) DEFAULT NULL,
  `titleLa` varchar(255) DEFAULT NULL,
  `shortDescriptionEn` text,
  `shortDescriptionBig` text,
  `shortDescriptionGb` text,
  `shortDescriptionTh` text,
  `shortDescriptionKr` text,
  `shortDescriptionVn` text,
  `shortDescriptionLa` text,
  `contentEn` text,
  `contentBig` text,
  `contentGb` text,
  `contentTh` text,
  `contentKr` text,
  `contentVn` text,
  `contentLa` text,
  `leagueId1` mediumint(8) unsigned DEFAULT NULL,
  `leagueId2` mediumint(9) DEFAULT NULL,
  `leagueId3` smallint(6) DEFAULT NULL,
  `leagueId4` smallint(6) DEFAULT NULL,
  `teamId1` mediumint(9) DEFAULT NULL,
  `teamId2` mediumint(9) DEFAULT NULL,
  `teamId3` mediumint(9) DEFAULT NULL,
  `teamId4` mediumint(9) DEFAULT NULL,
  `createDatetime` int(11) unsigned NOT NULL,
  `updateDatetime` int(11) unsigned NOT NULL,
  `imageLink` text NOT NULL,
  PRIMARY KEY (`newsId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
