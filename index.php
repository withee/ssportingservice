<?php

require '../facebook-php-sdk/src/facebook.php';
require 'Slim/Slim.php';
//require 'DBConfig.php';
require 'TotalStatement.php';
require "../Carbon/src/Carbon/Carbon.php";
require '../analog/lib/Analog.php';

use Carbon\Carbon;

$app = new Slim();
$app->contentType('application/json; charset=utf-8');

#$app->get('/liveMatchs', 'getLiveMatch');
#$app->get('/competitionOnly/:id', 'getCompetitionOnlys');
#$app->get('/competitionOnlyWithSubLeague/:id', 'getCompetitionOnlysWithSubLeague');
#$app->get('/liveMatchOnly', 'getLiveMatchOnly');
#$app->get('/liveMatchOnly/:id', 'getLiveMatchOnly');
#$app->get('/liveMatchWaitOnly', 'getLiveMatchWaitOnly');
#$app->get('/liveMatchWaitOnly/:id', 'getLiveMatchWaitOnly');
#$app->get('/liveMatchResultOnly', 'getLiveMatchResultOnly');
#$app->get('/liveMatchResultOnly/:id', 'getLiveMatchResultOnly');
#$app->get('/yesterdayResultOnly', 'getLiveMatchYesterdayResultOnly');
#$app->get('/yesterdayResultOnly/:id', 'getLiveMatchYesterdayResultOnly');
#$app->get('/liveMatchUpdate', 'getLiveMatchUpdate');
#$app->get('/liveMatchUpdateOnly', 'getLiveMatchUpdateOnly');
#$app->get('/statTable/:id', 'getStatTable');
$app->get('/statTableOnly/:id', 'getStatTableOnly');
$app->get('/statTableOnlys', 'getStatTableOnlys'); // complete
#$app->get('/liveMatchEvent/:id', 'getLiveMatchEvent');
#$app->get('/competition', 'getcompetitions');
#$app->get('/competitionOnly', 'getcompetitionsOnly');
#$app->get('/compare', 'compare');
$app->get('/compareOnly', 'compareOnly'); //complete
$app->get('/compareOnlys', 'compareOnlys'); //complete
#$app->get('/team', 'team');
$app->get('/teamOnly/:id', 'teamOnly');
$app->get('/teamOnlys', 'teamOnlys'); //complete
$app->get('/currentTime', 'currentTime');
#$app->get('/competitionTable/:id', 'getCompetitionTable');
#$app->get('/getEarlyDateList', 'getEarlyDateList');
#$app->get('/getOdd/:date', 'getOdd');
#$app->get('/getOdd', 'getOdd');
#$app->get('/getLastEvent', 'getLastEvent');
#$app->get('/getLastEvent/:limit', 'getLastEvent');
#$app->get('/leagueMain/:id', 'leagueMain');
$app->get('/leagueMainOnly/:id', 'leagueMainOnly');
$app->get('/leagueMainOnlys', 'leagueMainOnlys'); //complete
#$app->get('/leagueResult/', 'leagueResult');
#$app->get('/leagueFixture/', 'leagueFixture');
#$app->get('/leagueStat/:id', 'leagueStat');
#$app->get('/getLeague/:id', 'getLeague');
#$app->get('/getAllTeam', 'getAllTeam');
#$app->get('/getResult', 'getResult');
#$app->get('/getResult/:id', 'getResult');
#$app->get('/getLeagueByCid/:id', 'getLeagueByCid');
#$app->get('/getLeagueByCidOnly/:id', 'getLeagueByCidOnly');
#$app->get('/getLeagueByCidOnlys', 'getLeagueByCidOnlys');
$app->get('/resetNotification', 'resetNotification'); //ignore
$app->get('/favorite', "favorite"); //ignore
$app->get('/favoriteWithUID', "favoriteWithUID");
$app->get('/unfavorite', "unfavorite"); //ignore
$app->get('/unfavoriteWithUID', "unfavoriteWithUID"); //ignore
$app->get('/favoriteList', 'favoriteList');
$app->get('/favoriteListWithUID', 'favoriteListWithUID');
#$app->get('/videoHighlight', 'videoHighlight');
$app->get('/device_info', 'device_info');
#$app->get('/oddsToday', 'oddsToday');
$app->get('/subLeagueData/:id', 'subLeagueData'); //complete
#$app->get('/topPlayerScore/:id', 'topPlayerScore');
#$app->get('/allTransferLeague', 'allTransferLeague');
#$app->get('/playerTransferLeague/:id', 'playerTransferLeague');
#$app->get('/playerTransferTeam/:id', 'playerTransferTeam');
$app->get('/allNews', 'allNews');
$app->get('/newsDetail/:id', 'newsDetail');
$app->get('/leagueNews/:id', 'leagueNews');
$app->get('/teamNews/:id', 'teamNews');
$app->get('/dailyNews', 'dailyNews');
$app->get('/bet', 'bet'); //ignore
$app->post('/bet', 'bet');
$app->get('/saveFacebookInfo', 'saveFacebookInfo'); //ignore
$app->get('/allTeamId', 'allTeamId'); //ignore
$app->get('/betInfo/:id', 'betInfo');
$app->get('/voteBet/:id', 'voteBet'); //ingure
#$app->get('/betLists','betList');
$app->get('/voteList/:id', 'voteList');
$app->get('/voteList', 'voteListWithoutRewrite');
//$app->get('/betComment','betComment');
$app->get('/commentFacebookFriends/:id', 'commentFacebookFriends');
$app->get('/commentFacebookAll', 'commentFacebookAll');
$app->get('/saveFacebookFriends', 'saveFacebookFriends');
$app->post('/saveFacebookFriends', 'saveFacebookFriends');
$app->get('/betFriendList', 'betFriendList');
$app->get('/follow', 'follow');
$app->get('/checkFollow', 'checkFollow');
$app->get('/likeBetComment', 'likeBetComment');
$app->get('/unlikeBetComment', 'unlikeBetComment');
$app->get('/betComment', 'betComment');
$app->get('/followers', 'followers');
$app->get('/following', 'following');
$app->get('/getAllTeam', 'getAllTeam');
$app->get('/getRanking', 'getRanking');
$app->get('/getRankingCountry', 'getRankingCountry');

$app->get('/getFriendRanking', 'getFriendRanking');
$app->get('/commentOnbet', 'commentOnbet');
$app->get('/editCommentOnbet', 'editCommentOnbet');
$app->get('/removeCommentOnbet', 'removeCommentOnbet');
$app->get('/getCommentOnbet', 'getCommentOnbet');
$app->get('/commentOnmatch', 'commentOnmatch');
$app->post('/commentOnmatch', 'commentOnmatch');
$app->get('/editCommentOnmatch', 'editCommentOnmatch');
$app->get('/removeCommentOnmatch', 'removeCommentOnmatch');
$app->get('/getCommentOnmatch', 'getCommentOnmatch');
$app->get('/mCommentLike', 'mCommentLike');
$app->get('/mCommentUnlike', 'mCommentUnlike');
$app->get('/mCommentReport', 'mCommentReport');
$app->get('/swcp_ios_version3', 'swcp_ios_version3');
$app->get('/getCountryId', 'getCountryId');
$app->get('/updateUserStatus', 'updateUserStatus');
$app->get('/getAllTeams', 'getAllTeams');
$app->get('/getAllLeagues', 'getAllLeagues');
$app->get('/getSubleagues', 'getSubleagues');
$app->get('/getBetInfo', 'getBetInfo');
$app->get('/getMatchCommentList', 'getMatchCommentList');
$app->get('/getBetOnMatchInfo', 'getBetOnMatchInfo');
$app->get('/timeTest', 'timeTest');
$app->get('/getMoreMatchCommentList', 'getMoreMatchCommentList');
$app->get('/writeMatchComment', 'writeMatchComment');
$app->get('/matchRewrite', 'matchRewrite');
$app->get('/setMind', 'setMind');
$app->get('/setSite', 'setSite');
$app->get('/setCover', 'setCover');
$app->get('/setDisplayName', 'setDisplayName');
$app->get('/UpdateWallStatus', 'UpdateWallStatus');
$app->post('/UpdateWallStatus', 'UpdateWallStatus');
$app->get('/matchonfile/:id', 'matchonfile');
$app->get('/timelineReply', 'timelineReply');
$app->post('/timelineReply', 'timelineReply');
$app->get('/timelineLike', 'timelineLike');
$app->get('/timelineReport', 'timelineReport');
$app->get('/getTimelines', 'getTimelines');
$app->get('/UpdateTimelineContent', 'UpdateTimelineContent');
$app->get('/timelineReplyLike', 'timelineReplyLike');
$app->get('/timelineReplyReport', 'timelineReplyReport');
$app->get('/tlGamePlayer', 'tlGamePlayer');
$app->get('/getAchieve', 'getAchieve');
$app->get('/getAchieve2', 'getAchieve2');
$app->get('/timelineSingleContent', 'timelineSingleContent');
$app->get('/getFriendTimelines', 'getFriendTimelines');
$app->get('/nextReset', 'nextReset');
$app->get('/getGameData', 'getGameData');
$app->get('/sortedliveMatchOnly/:id(/:subleague)', 'sortedliveMatchOnly');
$app->get('/sortedliveMatchOnly', 'sortedliveMatchOnly');
$app->get('/sortedliveMatchWaitOnly', 'sortedliveMatchWaitOnly');
$app->get('/sortedliveMatchWaitOnly/:id(/:subleague)', 'sortedliveMatchWaitOnly');
$app->get('/sortedliveMatchResultOnly(/:id)(/:subleague)', 'sortedliveMatchResultOnly');
$app->get('/sortedyesterdayResultOnly(/:id)(/:subleague)', 'sortedyesterdayResultOnly');
$app->get('/removeTimeline', 'removeTimeline');
$app->get('/removeTimelineReply', 'removeTimelineReply');
$app->get('/initTrophydesc', 'initTrophydesc');
$app->get('/soundSetting', 'soundSetting');
$app->get('/worldcupsRanking', 'worldcupsRanking');
$app->get('/initDisplayName', 'initDisplayName');
$app->get('/regEvent(/:fb_uid)(/:type)(/:sys)(/:smg)(/:linktype)(/:key)(/:target)', 'regEvent');
$app->get('/getNotifications/:fb_uid', 'getNotifications');
$app->get('/notiSeen/:fb_uid(/:id)', 'notiSeen');
$app->get('/notiRemove/:fb_uid(/:id)', 'notiRemove');
$app->get('/dailyAlert/:fb_uid', 'dailyAlert');
$app->get('/getTopictype', 'getTopictype');
$app->get('/initTlkey', 'initTlkey');
$app->post('/newTopic', 'newTopic');
$app->get('/setCatview/:uid(/:list)', 'setCatview');
$app->get('/getCatview/:uid', 'getCatview');
$app->get('/getReccommend(/:cat)', 'getReccommend');
$app->get('/updateUserfile/:fb_uid', 'updateUserfile');
$app->get('/dailyBonus/:uid', 'dailyBonus');
$app->post('/sendMsg', 'sendMsg');
$app->get('/getMsg', 'getMsg');
$app->get('/delMsg/:id', 'delMsg');
$app->get('/getInbox', 'getInbox');
$app->get('/inboxBySender', 'inboxBySender');
$app->get('/sendboxBySender', 'sendboxBySender');
$app->get('/getSendbox', 'getSendbox');
$app->get('/getMobileFeed', 'getMobileFeed');
$app->get('/getUserlist', 'getUserlist');
$app->get('/getInstruction', 'getInstruction');
$app->get('/openapp', 'openapp');
$app->get('/RequestNotification', 'RequestNotification');
$app->get('/RequestNotificationWithUID', 'RequestNotificationWithUID');
$app->get('/NotificationList', 'NotificationList');
$app->get('/NotificationListWithUID', 'NotificationListWithUID');
$app->get('/FBtofile/:fb_uid', 'FBtofile');
$app->get('/playValue', 'playValue');
$app->get('/rateSelect', 'rateSelect');
$app->get('/logSet/:id(/:status)', 'logSet');
$app->get('/initProfile', 'initProfile');
//$app->get('/fbPoster', 'fbPoster');
$app->get('/updateBetinfo/:mid', 'updateBetinfo');
$app->get('/getsu', 'getSu');
$app->get('/getphpfile', 'phpfile');
$app->get('/serverstatus', 'ServerStatus');
$app->get('/sortedliveMatchUpdateOnly', 'sortedliveMatchUpdateOnly');
$app->get('/testnoti', 'sendPushNotificationToGCM');
$app->post('/FileMan', 'FileMan');
$app->run();
date_default_timezone_set('UTC');

function getDomainName()
{
    return "http://newapi.ssporting.com";
}

function phpfile()
{
    $file = file_get_contents("cronjob_gen_file/file/en/liveMatchWaitOnly.json");
    echo $file;
}

function getSu()
{
    return "100007730416796";
}

function followers()
{
    $startf = Carbon::now();
    $ap = new Slim();
    $fb_uid = $ap->request()->params('fb_uid');
    $offset = $ap->request()->params('offset');
    if (!$offset) {
        $offset = 0;
    }
    $limit = 100;
    $list = array();
    $newquery = true;
    $cachefile = "cache/follower/" . $fb_uid . ".json";
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $dt = date('H');
        $filedt = date('H', $filem);
        if ($dt == $filedt) {
            $newquery = false;
            $list = json_decode(file_get_contents($cachefile), true);
        }
    }
    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select  f.fb_uid,f.fb_email,f.fb_firstname,f.fb_middle_name,f.fb_lastname,f.fb_pic,f.follow_count,f.display_name from follow
left join facebook_user as f on f.fb_uid = follow.fb_uid
where follow.fb_follow_uid='$fb_uid'
order by follow_timestamp DESC
limit $offset,$limit";
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        //querylog(__FUNCTION__, $db);
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
        file_put_contents($cachefile, json_encode($list));
    }
    echo json_encode($list);
}

function getfollowers($fb_uid, $offset = 0)
{
    $ap = new Slim();
//    $fb_uid = $ap->request()->params('fb_uid');
//    $offset = $ap->request()->params('offset');
    $startf = Carbon::now();
    $limit = 100;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select  f.fb_uid,f.fb_email,f.fb_firstname,f.fb_middle_name,f.fb_lastname,f.fb_pic,f.follow_count from follow
left join facebook_user as f on f.fb_uid = follow.fb_uid
where follow.fb_follow_uid='$fb_uid'
order by follow_timestamp DESC
limit $offset,$limit";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    //echo json_encode($list);
    return $list;
}

function following()
{
    $startf = Carbon::now();
    $ap = new Slim();
    $fb_uid = $_REQUEST['fb_uid'];
    $offset = $_REQUEST['offset'];
    $limit = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 100;
    $start = (int)$offset * $limit;
    $cachefile = "cache/following/" . $fb_uid . '.json';
    $newquery = true;
    $result = array("user" => array(), "bet" => array());
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $filedt = date('H', $filem);
        $dt = date('H');
        if ($filedt == $dt) {
            $newquery = false;
            $result = json_decode(file_get_contents($cachefile), true);
        }
    }

    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select  f.fb_uid,f.fb_email,f.fb_firstname,f.fb_middle_name,f.fb_lastname,f.display_name,f.fb_pic,f.follow_count,f.gp,f.lastResult, f.spirit,f.level from follow
left join facebook_user as f on f.fb_uid = follow.fb_follow_uid
where (follow.fb_uid='$fb_uid' AND f.fb_uid <> '' AND f.fb_uid <> '(null)' AND f.fb_uid IS NOT NULL)
order by f.fb_uid
limit $start,$limit";

        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($list as $l) {
            if (!empty($l->fb_uid)) {
                // $betsql = "select * from bet where fb_uid={$l->fb_uid} ORDER BY betId DESC LIMIT 1";

                $sql_betList = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = '{$l->fb_uid}'
ORDER BY b.betId DESC
LIMIT 1";
                $stmt = $db->query($sql_betList);
                $res = $stmt->fetch(5);
                $result["user"][$l->fb_uid] = $l;
                if (!empty($res)) {
                    $result["bet"][$l->fb_uid] = $res;
                } else {
                    $result["bet"][$l->fb_uid] = null;
                }
            }
        }
        //querylog(__FUNCTION__, $db);
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
        file_put_contents($cachefile, json_encode($result));
    }
    echo json_encode($result);
}

function getfollowing($fb_uid, $offset = 0, $limit = 100)
{
    $ap = new Slim();
    $startf = Carbon::now();
//    $fb_uid = $_REQUEST['fb_uid'];
//    $offset = $_REQUEST['offset'];
//    $limit = isset($_REQUEST["limit"]) ? (int) $_REQUEST["limit"] : 100;
    $start = (int)$offset * $limit;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select  f.fb_uid,f.fb_email,f.fb_firstname,f.fb_middle_name,f.fb_lastname,f.fb_pic,f.follow_count,f.gp,f.lastResult, f.spirit,f.level from follow
left join facebook_user as f on f.fb_uid = follow.fb_follow_uid
where (follow.fb_uid='$fb_uid' AND f.fb_uid <> '')
order by f.fb_uid
limit $start,$limit";

    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result = array("user" => array(), "bet" => array());
    foreach ($list as $l) {
        if (!empty($l->fb_uid)) {
            // $betsql = "select * from bet where fb_uid={$l->fb_uid} ORDER BY betId DESC LIMIT 1";

            $sql_betList = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = {$l->fb_uid}
ORDER BY b.betId DESC
LIMIT 1";
            $stmt = $db->query($sql_betList);
            $res = $stmt->fetch(5);
            $result["user"][$l->fb_uid] = $l;
            if (!empty($res)) {
                $result["bet"][$l->fb_uid] = $res;
            } else {
                $result["bet"][$l->fb_uid] = null;
            }
        }
    }
    //echo json_encode($result);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    return $result;
}

function betComment()
{
    $startf = Carbon::now();
    $ap = new Slim();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $fb_uid = $ap->request()->params('fb_uid');
    $mid = $ap->request()->params('mid');
    $page = $ap->request()->params("offset");
    $pagelimit = $ap->request()->params("limit");

    if (empty($page)) {
        $page = 0;
    }
    if (empty($pagelimit)) {
        $pagelimit = 15;
    }
    $start = $page * $pagelimit;
    $stop = $start + $pagelimit;
    $sql = "SELECT * FROM live_match WHERE mid=$mid";
    $stmt = $stmt = $db->query($sql);
    $lm = $stmt->fetch(PDO::FETCH_OBJ);
    $commentdate = strtotime($lm->date . "-3 day");

    $sql = "select betId,choose,message,betDatetime,bet.fb_uid,bet.oddsTimestamp,like_count,if(bclike.bet_id is null,0,1) as checked,fb.user_status as displayname from bet
left join bet_comment_like as bclike  on bclike.bet_id = bet.betId and bclike.fb_uid = $fb_uid
left join facebook_friends as ff on ff.friend_facebook_id = bet.fb_uid and ff.facebook_id=$fb_uid
LEFT JOIN facebook_user fb ON bet.fb_uid=fb.fb_uid    
where mid=$mid and message <> '' and bet.betdatetime>$commentdate
order by bet.like_count DESC,length(bet.message) DESC,(CASE WHEN ff.friend_facebook_id IS NULL then 1 ELSE 0 END),bet.betDatetime LIMIT $start,$pagelimit";

    //   echo $sql;
    //   exit();    


    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);


    $sql_choose_home = "select bet.fb_uid from bet
left join facebook_friends as ff on ff.friend_facebook_id = bet.fb_uid and ff.facebook_id=$fb_uid
where mid=$mid and bet.choose='home'
order by (CASE WHEN ff.friend_facebook_id IS NULL then 1 ELSE 0 END)
limit 5";
    $sql_choose_away = "select bet.fb_uid from bet
left join facebook_friends as ff on ff.friend_facebook_id = bet.fb_uid and ff.facebook_id=$fb_uid
where mid=$mid and bet.choose='away'
order by (CASE WHEN ff.friend_facebook_id IS NULL then 1 ELSE 0 END)
limit 5";
    $sql_choose_draw = "select bet.fb_uid from bet
left join facebook_friends as ff on ff.friend_facebook_id = bet.fb_uid and ff.facebook_id=$fb_uid
where mid=$mid and bet.choose='draw'
order by (CASE WHEN ff.friend_facebook_id IS NULL then 1 ELSE 0 END)
limit 5";
    $stmt = $db->query($sql_choose_home);
    $home_list = $stmt->fetchAll(PDO::FETCH_COLUMN);
    $stmt = $db->query($sql_choose_away);
    $away_list = $stmt->fetchAll(PDO::FETCH_COLUMN);
    $stmt = $db->query($sql_choose_draw);
    $draw_list = $stmt->fetchAll(PDO::FETCH_COLUMN);
    $obj = array(
        'comment_list' => $list,
        'home' => $home_list,
        'away' => $away_list,
        'draw' => $draw_list,
    );

    $json = json_encode($obj);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo $json;
}

function likeBetComment()
{

    $startf = Carbon::now();
    $ap = new Slim();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $fb_uid = $ap->request()->params('fb_uid');
    $bet_id = $ap->request()->params('bet_id');
    $stmt = $db->query("select *from bet_comment_like where fb_uid='$fb_uid' and bet_id=$bet_id");
    if ($stmt->fetch(PDO::FETCH_OBJ)) {
        echo 0;
    } else {
        $db->beginTransaction();
        try {
            $sql_insert = "insert into bet_comment_like (fb_uid,bet_id) values('$fb_uid',$bet_id)";
            $db->exec($sql_insert);
            $sql_update = "update bet set like_count=like_count+1 where betId=$bet_id";
            $db->exec($sql_update);


            $user = $db->query("select b.fb_uid from bet_comment_like 
            join bet as b
            where b.betId = $bet_id");
            $uid = $user->fetch(PDO::FETCH_OBJ);
            $sql_update_spirit = "UPDATE `facebook_user` SET `spirit`=`spirit`+0.1 WHERE  `fb_uid`='{$uid->fb_uid}';";
            $db->exec($sql_update_spirit);
            $db->commit();

//            $sql_json = "select *from facebook_user where fb_uid='{$uid->fb_uid}'";
//            $stmt = $db->query($sql_json);
//            $obj = $stmt->fetch(PDO::FETCH_OBJ);
//            foreach ($obj as $key => $value) {
//                if ($value == NULL) {
//                    $obj->$key = "";
//                }
//            }
//            file_put_contents('facebook_info/' . $uid->fb_uid . '.json', json_encode($obj));
//            chmod('facebook_info/' . $uid->fb_uid . '.json', 0777);
            FBtofile($fb_uid);
            //querylog(__FUNCTION__, $db);
            $stop = Carbon::now();
            $duration = $stop->micro - $startf->micro;
            DBConfig::closeConnection($db, __FUNCTION__, $duration);
            echo 1;
        } catch (Exception $e) {
            $db->rollBack();
            //echo $e->getTraceAsString();
            echo $e->getMessage();
            echo 0;
        }
    }
}

function unlikeBetComment()
{
    $startf = Carbon::now();
    $ap = new Slim();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $fb_uid = $ap->request()->params('fb_uid');
    $bet_id = $ap->request()->params('bet_id');
    $stmt = $db->query("select *from bet_comment_like where fb_uid='$fb_uid' and bet_id=$bet_id");
    if ($stmt->fetch(PDO::FETCH_OBJ)) {
        $db->beginTransaction();
        try {
            $sql_delete = "delete from bet_comment_like where fb_uid='$fb_uid' and bet_id=$bet_id";
            $db->exec($sql_delete);
            $sql_update = "update bet set like_count=like_count-1 where betId=$bet_id";
            $db->exec($sql_update);
            $db->commit();
            //querylog(__FUNCTION__, $db);
            $stop = Carbon::now();
            $duration = $stop->micro - $startf->micro;
            DBConfig::closeConnection($db, __FUNCTION__, $duration);
            echo 1;
        } catch (Exception $e) {
            $db->rollBack();
            echo 0;
        }
    } else {
        echo 0;
    }
}

function checkFollow()
{
    $startf = Carbon::now();
    $ap = new Slim();
    $fb_uid = $ap->request()->params('fb_uid');
    $fb_follow_uid = $ap->request()->params('fb_follow_uid');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select *from follow where fb_uid ='$fb_uid' and fb_follow_uid='$fb_follow_uid'";
    $stmt = $db->query($sql);
    $obj = $stmt->fetch(PDO::FETCH_OBJ);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    if ($obj) {
        echo 1;
    } else {
        echo 0;
    }
}

function follow()
{
    $ap = new Slim();
    $fb_uid = $ap->request()->params('fb_uid');
    $fb_follow_uid = $ap->request()->params('fb_follow_uid');
    $checked = $ap->request()->params('checked');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select *from follow where  fb_uid='$fb_uid' and fb_follow_uid='$fb_follow_uid'";

    $stmt = $db->query($sql);
    $obj = $stmt->fetch(PDO::FETCH_OBJ);
    if ($obj) {
        if ($checked == 'Y') {
            echo 0;
        } else {
            $db->beginTransaction();
            try {
                $sql_delete = "delete from follow where fb_uid='$fb_uid' and fb_follow_uid='$fb_follow_uid'";
                $db->exec($sql_delete);
                //echo $sql_delete;
                $sql_update = "update facebook_user set follow_count=follow_count-1 where fb_uid='$fb_follow_uid'";
                $db->exec($sql_update);
                $sql_update = "update facebook_user set following_count=following_count-1 where fb_uid='$fb_uid'";
                $db->exec($sql_update);
                $db->commit();
                //querylog(__FUNCTION__, $db);
                echo 1;
            } catch (Exception $e) {
                echo $e->getTraceAsString();
                $db->rollBack();
            }
//            $sql = "select *from facebook_user where fb_uid='$fb_follow_uid'";
//            $stmt = $db->query($sql);
//            $obj = $stmt->fetch(PDO::FETCH_OBJ);
//            foreach ($obj as $key => $value) {
//                if ($value == NULL) {
//                    $obj->$key = "";
//                }
//            }
//            file_put_contents('facebook_info/' . $fb_follow_uid . '.json', json_encode($obj));
//            chmod('facebook_info/' . $fb_follow_uid . '.json', 0777);
            FBtofile($fb_follow_uid);
//            $sql = "select *from facebook_user where fb_uid='$fb_uid'";
//            $stmt = $db->query($sql);
//            $obj = $stmt->fetch(PDO::FETCH_OBJ);
//            foreach ($obj as $key => $value) {
//                if ($value == NULL) {
//                    $obj->$key = "";
//                }
//            }
//            file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//            chmod('facebook_info/' . $fb_uid . '.json', 0777);
            FBtofile($fb_uid);
        }
    } else {

        if ($checked == 'Y') {
            $db->beginTransaction();
            try {
                $sql_insert = "insert into follow (fb_uid,fb_follow_uid,follow_timestamp) values('$fb_uid','$fb_follow_uid',UNIX_TIMESTAMP(now()))";
                $db->exec($sql_insert);
                $sql_update = "update facebook_user set follow_count=follow_count+1 where fb_uid='$fb_follow_uid'";
                $db->exec($sql_update);
                $sql_update = "update facebook_user set following_count=following_count+1 where fb_uid='$fb_uid'";
                $db->exec($sql_update);
                regEvent($db, $fb_uid, 'follow', 'กำลังติดตามคุณ', '', 'follow', '0', $fb_follow_uid);
                $db->commit();
                //querylog(__FUNCTION__, $db);
                echo 1;
            } catch (Exception $e) {
                $db->rollBack();
                //echo $sql_insert;
            }
//            $sql = "select *from facebook_user where fb_uid='$fb_follow_uid'";
//            $stmt = $db->query($sql);
//            $obj = $stmt->fetch(PDO::FETCH_OBJ);
//            foreach ($obj as $key => $value) {
//                if ($value == NULL) {
//                    $obj->$key = "";
//                }
//            }
//            file_put_contents('facebook_info/' . $fb_follow_uid . '.json', json_encode($obj));
//            chmod('facebook_info/' . $fb_follow_uid . '.json', 0777);
            FBtofile($fb_follow_uid);
//            $sql = "select *from facebook_user where fb_uid='$fb_uid'";
//            $stmt = $db->query($sql);
//            $obj = $stmt->fetch(PDO::FETCH_OBJ);
//            foreach ($obj as $key => $value) {
//                if ($value == NULL) {
//                    $obj->$key = "";
//                }
//            }
//            file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//            chmod('facebook_info/' . $fb_uid . '.json', 0777);
            FBtofile($fb_uid);
        } else {
            echo 0;
        }
    }
}

function commentFacebookFriends($id)
{
    $ap = new Slim();
    $page = $ap->request()->params("offset");
    $pagelimit = $ap->request()->params("limit");

    if (empty($page)) {
        $page = 0;
    }
    if (empty($pagelimit)) {
        $pagelimit = 15;
    }
    $start = $page * $pagelimit;
    $stop = $start + $pagelimit;

    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
//    $sql = " (select  bet.*, device.fb_firstname,device.fb_middle_name,device.fb_lastname,device.fb_uid from facebook_friends fb
//left join facebook_user as device on device.fb_uid = fb.friend_facebook_id
//left join follow fl on device.fb_uid = fl.fb_follow_uid
//left join bet on bet.fb_uid = device.fb_uid
//where fb.facebook_id='$id' and device.fb_uid is not null and bet.message is not null and bet.message<>''
//)
//union
//(select bet.*,device.fb_firstname,device.fb_middle_name,device.fb_lastname,device.fb_uid from bet
//left join facebook_user as device on device.fb_uid = bet.fb_uid
//where device.fb_uid ='$id' and device.fb_uid is not null and bet.message is not null and bet.message<>''
//)
//
//order by betId DESC
//limit $start,$pagelimit
//";

    $sql = "(select bet.*, device.fb_firstname,device.fb_middle_name,device.fb_lastname,device.display_name from
((
select device.* from facebook_user as device
left join facebook_friends fb   on device.fb_uid = fb.friend_facebook_id
where fb.facebook_id='$id'
)
union
(
select device.* from facebook_user as device
left join follow fl on device.fb_uid = fl.fb_follow_uid
where fl.fb_uid='$id'
)) device
left join bet on bet.fb_uid = device.fb_uid
where device.fb_uid is not null
and bet.message is not null and bet.message<>'' )
 union
 (select bet.*,device.fb_firstname,device.fb_middle_name,device.fb_lastname,device.display_name from bet
left join facebook_user as device on device.fb_uid = bet.fb_uid
where device.fb_uid ='$id'
and device.fb_uid is not null
and bet.message is not null and bet.message<>'' )
order by betId DESC
limit $start,$pagelimit";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    //querylog(__FUNCTION__, $db);
    echo json_encode($list);
}

function commentFacebookAll()
{
    $ap = new Slim();
    $page = $ap->request()->params("offset");
    $pagelimit = $ap->request()->params("limit");

    if (empty($page)) {
        $page = 0;
    }
    if (empty($pagelimit)) {
        $pagelimit = 15;
    }
    $start = $page * $pagelimit;
    $stop = $start + $pagelimit;

    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = " select  bet.*, device.fb_firstname,device.fb_middle_name,device.fb_lastname,device.fb_uid from facebook_user as device
left join bet on bet.fb_uid = device.fb_uid
where (device.fb_uid is not null and bet.message is not null and bet.message <> '')
order by betId DESC
limit $start,$pagelimit
";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    //querylog(__FUNCTION__, $db);
    echo json_encode($list);
}

function betFriendList()
{
    $ap = new Slim();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $facebook_id = $ap->request()->params('facebook_id');
    $mid = $ap->request()->params('mid');
    $sql = "select bet.choose,device.fb_uid as facebook_id,bet.message,bet.betDatetime from facebook_friends fb
left join request_notification_count as device on device.fb_uid = fb.friend_facebook_id
left join bet on bet.deviceId = device.device_id and bet.platform = device.platform
where  fb.facebook_id='$facebook_id' and device.fb_uid is not null and bet.mid=$mid";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    $data = array(
        'home' => array(),
        'away' => array(),
        'draw' => array(),
    );
    foreach ($list as $obj) {
        array_push($data[$obj->choose], $obj);
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($data);
}

function saveFacebookFriends()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $facebook_id = $ap->request()->params('facebook_id');
    $facebook_friend_list = $ap->request()->params('facebook_friend_list');

    // Comment by mr.siwakorn
    // 10/03/2014 14:40
    // Delete space and , that we don't used from the string
    //-------------
    //echo $facebook_friend_list;
    $fb_list = explode(",", $facebook_friend_list);
//    $sql = "select *from facebook_friends where facebook_id='$facebook_id'";
//    $stmt = $db->query($sql);
//    if ($stmt->fetch(PDO::FETCH_OBJ)) {
    $sql_delete = "delete from facebook_friends where facebook_id='$facebook_id'";
    $db->exec($sql_delete);
//    }
    $sqlinsert = "INSERT IGNORE INTO `facebook_friends` (`facebook_id`,`friend_facebook_id`) VALUES";
    $havefriend = FALSE;
    foreach ($fb_list as $fb_id) {
        if (is_numeric($fb_id)) {
            if ($havefriend) {
                $sqlinsert .= ", ('$facebook_id','$fb_id')";
            } else {
                $sqlinsert .= " ('$facebook_id','$fb_id')";
                $havefriend = TRUE;
            }
//            $sql_insert = "insert into facebook_friends values('$facebook_id','$fb_id')";
//            $db->exec($sql_insert);
        }
    }
    //echo $sqlinsert;
    $db->exec($sqlinsert);

    file_put_contents('facebook_friend_list/' . $facebook_id . '.json', json_encode($fb_list));
    //chmod('facebook_friend_list/' . $facebook_id . '.json', 0777);
    //querylog(__FUNCTION__, $db);
    echo 1;
}

/*
  function betComment(){
  $ap = new Slim();
  $mid  = $ap->request()->params('mid');
  $device_id = $ap->request()->params('device_id');
  $platform = $ap->request()->params('platform');
  $content  = mysql_escape_string($ap->request()->params('content'));
  $datetime = strtotime(date('Y-m-d H:i:s'));
  $db = DBConfig::getConnection();
  $sql_insert ="insert into bet_comment (device_id,platform,mid,content,comment_datetime) values('$device_id','$platform',$mid,'$content',$datetime)";
  $json=$db->exec($sql_insert);
  if (isset($_GET['jsoncallback'])) {
  echo $_GET['jsoncallback'] . '(' . $json . ')';
  } else {
  echo $json;
  }
  }
 */

function voteBet($mid)
{

    $filename = 'vote_bet/' . $mid . '.json';
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array(
            'home' => "0",
            'away' => "0",
            'draw' => "0",
        ));
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function betInfo($mid)
{
    //echo "use function";
    $filename = 'bet_info/' . $mid . '.json';
    if (file_exists($filename)) {
        //    echo $filename;
        $json = json_decode(file_get_contents($filename));
        //var_dump($json);
        foreach ($json as $key => $val) {
            if (is_numeric($val)) {
                //  echo $val;
            } else {
                updateBetinfo($mid);
                break;
            }
        }
    } else {
        //    echo "file not found";
        $json = json_encode(array(
            'hdp_home' => "0",
            'hdp_away' => "0",
            'odds1x2_home' => "0",
            'odds1x2_away' => "0",
            'odds1x2_draw' => "0",
        ));
        updateBetinfo($mid);
    }
    $json = file_get_contents($filename);
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function voteList($id)
{
    $ap = new Slim();
    // $device_id = $ap->request()->params('device_id');
    // $platform = $ap->request()->params('platform');
//    if (!empty($platform)) {
//        $filename = 'vote_List/' . $device_id . '_' . $platform . '.json';
//    } else {
//        $filename = 'vote_List/' . $device_id . '.json';
//    }
    $filename = 'vote_List/' . $id . '.json';

    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array());
    }

    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function voteListWithoutRewrite()
{
    $ap = new Slim();
    $uid = $ap->request()->params('uid', 0);
    // $platform = $ap->request()->params('platform');
//    if (!empty($platform)) {
//        $filename = 'vote_List/' . $device_id . '_' . $platform . '.json';
//    } else {
//        $filename = 'vote_List/' . $device_id . '.json';
//    }
    $filename = 'vote_List/' . $uid . '.json';

    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array());
    }

    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}


function betList()
{
    $ap = new Slim();
    $device_id = $ap->request()->params('device_id');
    $platform = $ap->request()->params('platform');
    $filename = 'bet_List/' . $platform . '_' . $device_id . '.json';
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array());
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function allTeamId()
{
    $json = null;
    $cachefile = "cache/allteamid.json";
    $list = array();
    $newquery = true;
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $filedt = date('H', $filem);
        $dt = date('H');
        if ($filedt == $dt) {
            $newquery = false;
            $list = json_decode(file_get_contents($cachefile), true);
        }
    }
    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = 'SELECT tid FROM `team` ';
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_COLUMN);
        file_put_contents($cachefile, json_encode($list));
    }
    $json = json_encode(array('list' => $list));
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function bet()
{
    $startf = Carbon::now();
    //$config = json_encode(file_get_contents('system_config/file.json'));
    $ap = new Slim();
    //dev.swcp.com/bet?device_id=1234&platform=ios&match_id=915605&betType=Bet365&hid=4356&gid=1012&leagueId=152&odds1x2_home=2.5&amount=0&message=
    $fb_uid = $ap->request()->params('fb_uid');
    $uid = $ap->request()->params('uid', 0);
    $device_id = $ap->request()->params('device_id');
    $platform = $ap->request()->params('platform');
    $match_id = $ap->request()->params('match_id');
    $mid = $ap->request()->params('mid');
    $betType = $ap->request()->params('betType');
    $hid = $ap->request()->params('hid');
    $gid = $ap->request()->params('gid');
    $leagueId = $ap->request()->params('leagueId');
    $hdp_home = $ap->request()->params('hdp_home');
    $hdp_away = $ap->request()->params('hdp_away');
    $betHdp = $ap->request()->params('betHdp');
    $odds1x2_home = $ap->request()->params('odds1x2_home');
    $odds1x2_away = $ap->request()->params('odds1x2_away');
    $odds1x2_draw = $ap->request()->params('odds1x2_draw');
    $amount = $ap->request()->params('amount');
    $user_defined = $ap->request()->params('user_defined', 0);
    $country = $ap->request()->params("country");
    $country = $country ? $country : 0;
    $amount = $amount ? $amount : 1;
    $amount *= 10;
    $scoin = $ap->request()->params('scoin');
    $scoin = $scoin ? $scoin : 100;
    $message = $ap->request()->params('message');
    $postmsg = $message;
    $message = addslashes($postmsg);
    ///////////temp////////////////
    $gamesql = "";
    $trackstring = "";

    //print_r($_SERVER['REQUEST_METHOD']);
    //print_r(file_get_contents('php://input'));

//    $logdir = "logs/bet";
//    if (!is_dir($logdir)) {
//        mkdir($logdir, 0777);
//    }
//    $logdate = date('Y-m-d');
//    $file = $logdir . "/" . $logdate . ".log";
//    if (!is_file($file)) {
//        $ourFileName = $file;
//        $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
//        fclose($ourFileHandle);
//        chmod($file, 0777);
//    }

    //Analog::handler(\Analog\Handler\File::init($file));
    //$logset = logGet(1);
    //Analog::log("============$mid==============", 6);
    //Analog::log("user $fb_uid", 6);
    //if ($logset == 'on') {
    //Analog::log("bet in platform $platform value $scoin", 6);
    //}

    if ($platform == "ios" && $message == "Comment") {
        ////Analog::log("ios with message : Comment", 6);
        $message = "";
    }
    $vote_choose = $ap->request()->params('vote_choose');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if ($vote_choose && $mid) {
        //if ($logset == 'on') {
        ////Analog::log("vote $vote_choose", 6);
        // }
        $response_array = array(
            'status' => 1,
            'value_change' => array(),
        );
        $sql = "";
        if (!empty($fb_uid)) {
            $sql = "select *from vote_bet where mid='$mid' and fb_uid='$fb_uid'";
        } else {
            $sql = "select *from vote_bet where mid='$mid' and device_id='$device_id' and platform='$platform'";
        }
        $stmt = $db->query($sql);
        $vote_bet = $stmt->fetch(PDO::FETCH_OBJ);
        if (empty($vote_bet)) {
            $sql = "select *from live_match where mid='$mid' and new ='Y' and sid=1 and live_match.date -interval 5 minute > current_timestamp";
            $stmt = $db->query($sql);
            $match = $stmt->fetch(PDO::FETCH_OBJ);
            if ($match) {
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $sql_insert = "insert into vote_bet (uid,device_id,platform,mid,vote_choose,vote_datetime,fb_uid) values(
                '$uid','$device_id','$platform','$mid','$vote_choose',$datetime,'$fb_uid'
                )";  //ignored duplicate key
                $db->exec($sql_insert);
                $sql_count = "select sum(if(vote_choose='home',1,0)) as home ,sum(if(vote_choose='away',1,0)) as away ,sum(if(vote_choose='draw',1,0)) as draw
from vote_bet where mid='$mid'";
                $stmt = $db->query($sql_count);
                $change = $stmt->fetch(PDO::FETCH_OBJ);
                $response_array['value_change'] = $change;
                file_put_contents('vote_bet/' . $mid . '.json', json_encode($change));
                //chmod('vote_bet/' . $mid . '.json', 0777);


                if (!empty($fb_uid)) {
                    if (!empty($message)) {
                        ////Analog::log("bet with message : $message", 6);
                        msgFromBet($db, $mid, $vote_choose, $fb_uid, $message, $country);
                    }
                    $sql_vote_list = "select *from vote_bet where fb_uid='$fb_uid' order by vote_datetime DESC limit 10";
                    $stmt = $db->query($sql_vote_list);
                    $vote_list = $stmt->fetchAll(PDO::FETCH_OBJ);
                    file_put_contents('vote_List/' . $uid . '.json', json_encode($vote_list));
                    //chmod('vote_List/' . $uid . '.json', 0777);
                } else {
                    $sql_vote_list = "select *from vote_bet where platform='$platform' and device_id='$device_id' order by vote_datetime DESC limit 10";
                    $stmt = $db->query($sql_vote_list);
                    $vote_list = $stmt->fetchAll(PDO::FETCH_OBJ);
                    file_put_contents('vote_List/' . $platform . '_' . $device_id . '.json', json_encode($vote_list));
                    //chmod('vote_List/' . $platform . '_' . $device_id . '.json', 0777);
                }
            } else {
                $response_array['status'] = 3;
            }
        } else {
            $sql = "select *from live_match where mid='$mid' and new ='Y' and sid=1 and live_match.date -interval 5 minute > current_timestamp";
            $stmt = $db->query($sql);
            $match = $stmt->fetch(PDO::FETCH_OBJ);
            if ($match) {
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $sql_update = "";
                if (!empty($fb_uid)) {
                    $sql_update = "update vote_bet set vote_datetime='$datetime',vote_choose='$vote_choose' where mid='$mid' and fb_uid='$fb_uid'";
                } else {
                    $sql_update = "update vote_bet set vote_datetime='$datetime',vote_choose='$vote_choose' where mid='$mid' and platform='$platform' and device_id='$device_id'";
                }
                //   $sql_update = "update vote_bet set vote_datetime='$datetime',vote_choose='$vote_choose' where mid=$mid and platform='$platform' and device_id='$device_id'";

                $db->exec($sql_update);
                $sql_count = "select sum(if(vote_choose='home',1,0)) as home ,sum(if(vote_choose='away',1,0)) as away ,sum(if(vote_choose='draw',1,0)) as draw
from vote_bet where mid='$mid'";
                $stmt = $db->query($sql_count);
                $change = $stmt->fetch(PDO::FETCH_OBJ);
                $response_array['value_change'] = $change;
                file_put_contents('vote_bet/' . $mid . '.json', json_encode($change));
                //chmod('vote_bet/' . $mid . '.json', 0777);
                if (!empty($fb_uid)) {
                    $sql_vote_list = "select *from vote_bet where fb_uid='$fb_uid' order by vote_datetime DESC limit 10";
                    $stmt = $db->query($sql_vote_list);
                    $vote_list = $stmt->fetchAll(PDO::FETCH_OBJ);
                    file_put_contents('vote_List/' . $uid . '.json', json_encode($vote_list));
                    //chmod('vote_List/' . $uid . '.json', 0777);
                } else {
                    $sql_vote_list = "select *from vote_bet where platform='$platform' and device_id='$device_id' order by vote_datetime DESC limit 10";
                    $stmt = $db->query($sql_vote_list);
                    $vote_list = $stmt->fetchAll(PDO::FETCH_OBJ);
                    file_put_contents('vote_List/' . $platform . '_' . $device_id . '.json', json_encode($vote_list));
                    //chmod('vote_List/' . $platform . '_' . $device_id . '.json', 0777);
                }
            } else {
                $response_array['status'] = 3;
            }
        }
    } else {

        $usersql = "SELECT scoin FROM facebook_user WHERE fb_uid='$fb_uid'";
        $usersqtm = $db->query($usersql);
        $current_scoin = $usersqtm->fetchColumn();
        //Analog::log("User scoin: $current_scoin", 6);
        if ((int)$scoin <= (int)$current_scoin) {


            $sql_dup = "select *from bet where fb_uid='$fb_uid' and mid='$mid'";
            $stmt = $db->query($sql_dup);
            $bet_dup = $stmt->fetch(PDO::FETCH_OBJ);

            //$choose  = $ap->request()->params('choose');
            //$sql = "select *from live_match where hid='$hid' and gid='$gid' and _lid='$leagueId' and new='Y'  and  sid =1  and date -interval 0 minute > current_timestamp ";
            //echo $sql;
            $sql = "select * from timelines_game where hid='$hid' and gid='$gid' and _lid='$leagueId' and  sid =1 and remove='N' AND show_date > NOW() #and date -interval 0 minute > current_timestamp ";
            $sql = "select * from timelines_game where hid='$hid' and gid='$gid' and  sid =1 and remove='N' AND show_date > NOW()";
            $stmt = $db->query($sql);
            $gamesql = $sql;
            //////Analog::log("livematch: $sql", 6);
            $obj = $stmt->fetch(PDO::FETCH_OBJ);
            //status 1 = success,2 = value_change , 3 = match not found
            $response_array = array(
                'status' => 1,
                'value_change' => array(),
                'message' => "just init"
            );
            $trackstring .= "->init $hid-$gid-$leagueId";
            if (!empty($obj)) {
                $trackstring .= "->has game";
                $showdate = date('H:i d-m-Y', strtotime($obj->show_date . " +7hours"));
                $leagueId = $obj->_lid;
                $oddsType = null;
                $sql_odds = '';
                $choose = '';
                if ($hdp_home || $hdp_away) {
                    $trackstring .= "->choose side";
                    //$sql_odds = "select *from odds_defined where match_id = $match_id and odds_date=current_date and type='$betType' and hdp_home is not null and hdp_away is not null and hdp is not null UNION select *from odds_7m where match_id = $match_id and odds_date=current_date and type='$betType' and hdp_home is not null and hdp_away is not null and hdp is not null";
                    $sql_odds = "SELECT odds.* FROM
(
select *from odds_defined
where match_id = '$match_id'

and type='$betType'
and hdp_home is not null
and hdp_away is not null
and hdp is not null
UNION
select *from odds_7m
where match_id = '$match_id'

and type='$betType'
and hdp_home is not null
and hdp_away is not null
and hdp is not null
) odds
ORDER BY MAX(odds.odds_date) DESC
LIMIT 1";
                    $oddsType = 'hdp';
                    if ($hdp_home) {
                        $choose = 'home';
                    } else {
                        $choose = 'away';
                    }
                } else if ($odds1x2_home || $odds1x2_away || $odds1x2_draw) {
                    if ($user_defined) {
                        $sql_odds = "select *from odds_defined where match_id = '$match_id'  and type='$betType' and  odds1x2_home is not null and odds1x2_away is not null and odds1x2_draw is not null";
                    } else {
                        $sql_odds = "select *from odds_7m where match_id = '$match_id'  and type='$betType' and  odds1x2_home is not null and odds1x2_away is not null and odds1x2_draw is not null";
                    }
                    $oddsType = '1x2';
                    if ($odds1x2_home) {
                        $choose = 'home';
                    } else if ($odds1x2_away) {
                        $choose = 'away';
                    } else {
                        $choose = 'draw';
                    }
                }

                $stmt = $db->query($sql_odds);
                //echo $sql_odds;
                $odds_obj = $stmt->fetch(PDO::FETCH_OBJ);
                if ($odds_obj) {
                    $trackstring .= "->has odds";
                    //echo $oddsType.','.$choose;
                    $bet_value = '';
                    $bet_value_change = array();
                    if ($choose == 'home' && $oddsType == 'hdp') {
                        $bet_value = $odds_obj->hdp_home == $hdp_home ? $hdp_home : $hdp_home;
                    } else if ($choose == 'away' && $oddsType == 'hdp') {
                        $bet_value = $odds_obj->hdp_away == $hdp_away ? $hdp_away : $hdp_away;
                    } else if ($choose == 'home' && $oddsType == '1x2') {
                        //echo $odds_obj->odds1x2_home.','.$odds1x2_home;
                        $bet_value = $odds_obj->odds1x2_home == $odds1x2_home ? $odds1x2_home : '';
                    } else if ($choose == 'away' && $oddsType == '1x2') {
                        $bet_value = $odds_obj->odds1x2_away == $odds1x2_away ? $odds1x2_away : '';
                    } else if ($choose == 'draw' && $oddsType == '1x2') {
                        $bet_value = $odds_obj->odds1x2_draw == $odds1x2_draw ? $odds1x2_draw : '';
                    }
                    $samehdp = ($odds_obj->hdp == $betHdp) ? true : true;
                    $db->beginTransaction();

                    try {
                        $trackstring .= "->try";
                        if ($bet_value && $samehdp) {
                            //insert
                            $mid = $obj->mid;
                            $oddsTimestamp = json_encode($odds_obj);
                            $datetime = strtotime(date('Y-m-d H:i:s'));
//                        var_dump($bet_dup);
//                        exit();
                            if ($bet_dup) {
                                //Analog::log("update duplicate bet from $platform", 6);
                                //Analog::log("choose $choose", 6);
                                $sql_insert = "update bet set betDatetime='$datetime',betType='$betType',betValue='$bet_value',choose='$choose',betHdp='$betHdp',oddsTimestamp='$oddsTimestamp',oddsType='$oddsType',message='$message' where mid='$mid' and fb_uid='$fb_uid'";
                                //$sql_insert = "update bet set betDatetime='$datetime',oddsTimestamp='$oddsTimestamp',oddsType='$oddsType' where mid='$mid' and fb_uid='$fb_uid'";
                            } else {
                                //Analog::log("new bet", 6);
                                //Analog::log("choose $choose", 6);
                                $device_id = empty($device_id) ? $fb_uid : $device_id;
                                $sql_insert = "insert ignore into bet (deviceId,platform,hid,gid,leagueId,betDatetime,kickOff,betType,betValue,choose,betHdp,result,oddsTimestamp,oddsType,amount,message,mid,fb_uid,country_id) value('$device_id','$platform',$hid,$gid,$leagueId,$datetime,$odds_obj->kick_off,'$betType','$bet_value','$choose','$betHdp','wait','$oddsTimestamp','$oddsType',$amount,'$message',$mid,'$fb_uid',$country);";
                                $sql_json = "select *from facebook_user where fb_uid='$fb_uid'";
                                $stmt = $db->query($sql_json);
                                $obj = $stmt->fetch(PDO::FETCH_OBJ);
                                if (!empty($message)) {
                                    $sql_update_spirit = "UPDATE `facebook_user` SET `spirit`=`spirit`+0.1 WHERE  `fb_uid`='$fb_uid';";
                                    $db->exec($sql_update_spirit);
                                    foreach ($obj as $key => $value) {
                                        if ($value == NULL) {
                                            $obj->$key = "";
                                        }
                                    }
                                    file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
                                    //chmod('facebook_info/' . $fb_uid . '.json', 0777);
                                    msgFromBet($db, $mid, $choose, $fb_uid, $message, $country);
                                    //writeMatchComment($mid);
                                    //Analog::log("bet with message : $message", 6);
                                }
                                //writeMatchComment($mid);
                                $rs = TimelineTrigger($fb_uid, "game", $mid, 1);

                                $user = "SELECT fb_name,fb_access_token,uid FROM facebook_user WHERE fb_uid='$fb_uid'";
                                $stmt = $db->query($user);
                                $token = $stmt->fetch();
                                //var_dump($token[0]);
                                $teamhsql = "SELECT teamNameTh,teamNameEN FROM lang_team WHERE tid='$hid'";
                                $teamhstmt = $db->query($teamhsql);
                                $teamh = $teamhstmt->fetch();

                                $teamasql = "SELECT teamNameTh,teamNameEN FROM lang_team WHERE tid='$gid'";
                                $teamastmt = $db->query($teamasql);
                                $teama = $teamastmt->fetch();

                                $teams = array('home' => array(), 'away' => array());
                                $teams['home'] = $teamh;
                                $teams['away'] = $teama;
                                $name = $teams['home']['teamNameTh'] . '-' . $teams['away']['teamNameTh'];
                                $side = "เสมอ";
                                $stamhdp = 0.0;
                                if (floatval($betHdp) != 0) {
                                    $side = "ชนะ";
                                }
//                            if ($choose == 'home') {
//                                if (floatval($betHdp) < 0) {
//                                    $side = "ต่อ";
//                                    $stamhdp = floatval($betHdp);
//                                } else if (floatval($betHdp) > 0) {
//                                    $side = "รอง";
//                                    $stamhdp = floatval($betHdp);
//                                }
//                            } else {
//                                if (floatval($betHdp) < 0) {
//                                    $side = "รอง";
//                                    $stamhdp = floatval($betHdp) * (-1);
//                                } else if (floatval($betHdp) > 0) {
//                                    $side = "ต่อ";
//                                    $stamhdp = floatval($betHdp) * (-1);
//                                }
//                            }
                                //$desc = "$showdate หากคุณคือเซียนบอลตัวจริง เราขอท้าพิสูจน์ความแม่น ไปกับเกมการแข่งขันสดๆจากทุกลีกทั่วโลกได้แล้วที่นี่";
                                //$caption = "ssporting.com เกมทายผลฟุตบอลสดทุกแมตช์";
                                //$smg = "{$teams['home']['teamNameTh']} vs {$teams['away']['teamNameTh']} ใครจะชนะมาดูกัน: " . $postmsg;
                                //$response_array["postable"] = fbPoster($token["fb_access_token"], $smg, $name, $desc, $caption, "https://apps.facebook.com/ssporting/game.php?mid=$mid", "http://ssporting.com/WebObject/images/s{$mid}.jpg?" . date('u'));
                                //Analog::log("Post FB with $smg", 6);
                                regEvent($db, $fb_uid, 'play', "ได้ทายผลคู่:", "{$teams['home']['teamNameTh']} - {$teams['away']['teamNameTh']}", 'game', $mid);
                            }
                            $trackstring .= "->$sql_insert";
                            $sqlreturn = $db->exec($sql_insert);

                            //////Analog::log("1142:$sql_insert", 6);
                            //////Analog::log("1142:$sqlreturn", 6);
                            //regEvent($db, $fb_uid, 'play', "ได้ทายผลคู่:", "{$teams['home']['teamNameTh']} - {$teams['away']['teamNameTh']}", 'game', $mid);
                            //writeMatchComment($mid);
                            //all country
//                        SELECT b.*,hlt.teamName homeName,alt.teamName awayName,ll.leagueName
//FROM bet b
//LEFT JOIN lang_team hlt ON hlt.tid=b.hid
//LEFT JOIN lang_team alt ON alt.tid=b.gid
//LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
//WHERE fb_uid = '100001188216619'
//ORDER BY b.betId DESC
//LIMIT 100
//                        $sql_betList = "select *from bet where fb_uid='$fb_uid'
//                        order by betId DESC
//                        limit 100";
                            updateBetinfo($mid);
                            $langlistT = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
                            $langlistL = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
                            $sql_betList = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = '$fb_uid'
ORDER BY b.betId DESC
LIMIT 100";
                            $stmt = $db->query($sql_betList);
                            //////Analog::log("1168:$sql_betList", 6);
                            $betList = $stmt->fetchAll(PDO::FETCH_OBJ);
                            file_put_contents('bet_List/' . $fb_uid . '.json', json_encode($betList));
                            //chmod('bet_List/' . $fb_uid . '.json', 0777);

                            //specific country
                            if ($country != 0) {
//                            $sql_betList = "select *from bet where fb_uid='$fb_uid' AND country_id=$country
//                        order by betId DESC
//                        limit 100";
                                $sql_betList = "SELECT b.*,hlt.teamNameEn hen,alt.teamNameEn aen,ll.leagueNameEn len,hlt.teamNameTh hth,alt.teamNameTh ath,ll.leagueNameTh lth,hlt.teamNameBig hbig,alt.teamNameBig abig,ll.leagueNameBig lbig,hlt.teamNameGb hgb,alt.teamNameGb agb,ll.leagueNameGb lgb,hlt.teamNameKr hkr,alt.teamNameKr akr,ll.leagueName lkr,hlt.teamNameVn hvn,alt.teamNameVn avn,ll.leagueNameVn lvn,hlt.teamNameLa hla,alt.teamNameLa ala,ll.leagueNameLa lla
FROM bet b
LEFT JOIN lang_team hlt ON hlt.tid=b.hid
LEFT JOIN lang_team alt ON alt.tid=b.gid
LEFT JOIN lang_league ll ON ll.leagueId=b.leagueId
WHERE fb_uid = '$fb_uid' AND country_id=$country
ORDER BY b.betId DESC
LIMIT 100";
                                $stmt = $db->query($sql_betList);
                                //////Analog::log("1187:$sql_betList", 6);
                                $betList = $stmt->fetchAll(PDO::FETCH_OBJ);
                                file_put_contents('bet_List/' . $fb_uid . '_' . $country . '.json', json_encode($betList));
                                //chmod('bet_List/' . $fb_uid . '_' . $country . '.json', 0777);
                            }

//has function handle it already.
//                            $sql_bet = "select
//sum(if(oddsType='hdp' and choose='home',1,0)) as hdp_home,
//sum(if(oddsType='hdp' and choose='away',1,0)) as hdp_away,
//sum(if(oddsType='1x2' and choose='home',1,0)) as odds1x2_home,
//sum(if(oddsType='1x2' and choose='away',1,0)) as odds1x2_away,
//sum(if(oddsType='1x2' and choose='draw',1,0)) as odds1x2_draw
//from bet
//where mid=$mid";
//                            $stmt = $db->query($sql_bet);
//                            //////Analog::log("1202:$sql_bet", 6);
//                            $betInfo = $stmt->fetch(PDO::FETCH_OBJ);
//                            if (!empty($betInfo)) {
//                                file_put_contents('bet_info/' . $mid . '.json', json_encode($betInfo));
//                                chmod('bet_info/' . $mid . '.json', 0777);
//                            }
                            $sql_update_user = "update facebook_user set pts=pts+1 where fb_uid='$fb_uid'";
                            $db->exec($sql_update_user);
                            //////Analog::log("1209:$sql_update_user", 6);
                            $db->commit();
//                        if ($fb_uid == '1022053875') {

                            $t = new TotalStatement();
                            $betObj = $betList[0];
                            $bet_id = $betObj->betId;
                            $uid = $token['uid'];
                            $t->bet($uid, $bet_id, $scoin);
//                        }
//                        writeMatchComment($mid);
                            /*
                              $sql_bet_comment = "select choose,message,betDatetime,fb_uid from bet where mid=$mid and message <>''";
                              $stmt = $db->query($sql_bet_comment);
                              $betCommentList = $stmt->fetchAll(PDO::FETCH_OBJ);
                              file_put_contents('bet_comment/'.$mid.'.json',json_encode($betCommentList));
                             */
                        } else {
                            $bet_value_change['hdp'] = $odds_obj->hdp;
                            $bet_value_change['hdp_home'] = $odds_obj->hdp_home;
                            $bet_value_change['hdp_away'] = $odds_obj->hdp_away;
                            $bet_value_change['odds1x2_home'] = $odds_obj->odds1x2_home;
                            $bet_value_change['odds1x2_away'] = $odds_obj->odds1x2_away;
                            $bet_value_change['odds1x2_draw'] = $odds_obj->odds1x2_draw;
                            $bet_value_change['ou'] = $odds_obj->ou;
                            $bet_value_change['ou_over'] = $odds_obj->ou_over;
                            $bet_value_change['ou_under'] = $odds_obj->ou_under;
                            $response_array['status'] = 2;
                            $response_array['value_change'] = $bet_value_change;
                            $response_array['same_hdp'] = $samehdp;
                            ////Analog::log("hdp changed", 6);
                        }
                    } catch (Exception $e) {
                        $db->rollBack();
                        //echo $e->message();
                        //Analog::log("Exception:{$e->getMessage()}", 6);
                        $trackstring .= "->Exception:" . $e->getMessage();
                        file_put_contents('error.txt', $e->getMessage());
                    }
                    ////Analog::log("Success:Pass through try catch", 6);
                } else {
                    $response_array['status'] = 3;
                    $response_array['message'] = '7' . " odds not found";
                    //Analog::log("error message 7", 6);
                }
            } else {
                $response_array['status'] = 3;
                $response_array['message'] = 24 . " game not found: " . $gamesql;
                //Analog::log("error {$response_array['message']}", 6);
            }
        } else {
            $response_array['status'] = 3;
            $response_array['message'] = "scoin ของคุณมีไม่พอ";
            //Analog::log("error {$response_array['message']}", 6);
        }
    }
    //FBtofile($fb_uid);
    if (!empty($mid)) {
        writeMatchComment($mid);
        ////Analog::log("update match comment $mid", 6);
    }

    $usersql = "SELECT scoin,sgold,display_name FROM facebook_user WHERE fb_uid='$fb_uid'";
    $usersqtm = $db->query($usersql);
    $current_scoin = $usersqtm->fetch(5);
    $response_array['scoin'] = $current_scoin->scoin;
    $response_array['sgold'] = $current_scoin->sgold;
    $response_array['message'] .= "|" . $trackstring;
    //Analog::log("User:: {$current_scoin->display_name}", 6);
    $json = json_encode($response_array);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function leagueNews($leagueId)
{

    $lang = array(
        'en', 'th', 'big', 'gb', 'kr', 'vn', 'la'
    );
    $limit = 60 * 60 * 2;
    $file_name = 'league_news/' . $leagueId . '.json';
    $in_present = false;
    if (file_exists($file_name)) {
        $file_timestamp = filemtime($file_name);
        if (time() - $file_timestamp < $limit) {
            $in_present = true;
        }
    }

    if ($in_present) {
        $json = file_get_contents($file_name);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $list_sql = "select news.*,
l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
from news
left join lang_league as l1 on l1.leagueId = news.leagueId1
left join lang_league as l2 on l2.leagueId = news.leagueId2
left join lang_league as l3 on l3.leagueId = news.leagueId3
left join lang_league as l4 on l4.leagueId = news.leagueId4
left join lang_team as t1 on t1.tid = news.teamId1
left join lang_team as t2 on t2.tid = news.teamId2
left join lang_team as t3 on t3.tid = news.teamId3
left join lang_team as t4 on t4.tid = news.teamId4
where leagueId1 = $leagueId or leagueId2 =$leagueId or leagueId3= $leagueId or leagueId4 = $leagueId
order by newsId DESC
limit 10";
        $stmt = $db->query($list_sql);
        $list_all = $stmt->fetchAll(PDO::FETCH_OBJ);
        $lang_leauge = array();
        $lang_team = array();
        $lang_content = array();
        $lang_title = array();
        $lang_shortDescription = array();
        $data = array();
        foreach ($list_all as $object) {
            $newsId = $object->newsId;
            $lang_content_obj = array();
            $lang_content_obj['en'] = $object->contentEn;
            $lang_content_obj['th'] = $object->contentTh;
            $lang_content_obj['big'] = $object->contentBig;
            $lang_content_obj['gb'] = $object->contentGb;
            $lang_content_obj['kr'] = $object->contentKr;
            $lang_content_obj['vn'] = $object->contentVn;
            $lang_content_obj['la'] = $object->contentLa;
            $lang_content[$newsId] = $lang_content_obj;

            unset($object->contentEn);
            unset($object->contentTh);
            unset($object->contentBig);
            unset($object->contentGb);
            unset($object->contentKr);
            unset($object->contentVn);
            unset($object->contentLa);

            $lang_title_obj = array();
            $lang_title_obj['en'] = $object->titleEn;
            $lang_title_obj['th'] = $object->titleTh;
            $lang_title_obj['big'] = $object->titleBig;
            $lang_title_obj['gb'] = $object->titleGb;
            $lang_title_obj['kr'] = $object->titleKr;
            $lang_title_obj['vn'] = $object->titleVn;
            $lang_title_obj['la'] = $object->titleLa;
            $lang_title[$newsId] = $lang_title_obj;

            unset($object->titleEn);
            unset($object->titleTh);
            unset($object->titleBig);
            unset($object->titleGb);
            unset($object->titleKr);
            unset($object->titleVn);
            unset($object->titleLa);

            $lang_shortDescription_obj = array();
            $lang_shortDescription_obj['en'] = $object->shortDescriptionEn;
            $lang_shortDescription_obj['th'] = $object->shortDescriptionTh;
            $lang_shortDescription_obj['big'] = $object->shortDescriptionBig;
            $lang_shortDescription_obj['gb'] = $object->shortDescriptionGb;
            $lang_shortDescription_obj['kr'] = $object->shortDescriptionKr;
            $lang_shortDescription_obj['vn'] = $object->shortDescriptionVn;
            $lang_shortDescription_obj['la'] = $object->shortDescriptionLa;
            $lang_shortDescription[$newsId] = $lang_shortDescription_obj;

            unset($object->shortDescriptionEn);
            unset($object->shortDescriptionTh);
            unset($object->shortDescriptionBig);
            unset($object->shortDescriptionGb);
            unset($object->shortDescriptionKr);
            unset($object->shortDescriptionVn);
            unset($object->shortDescriptionLa);


            if ($object->leagueId1) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn1;
                $leagueObj['th'] = $object->leagueNameTh1;
                $leagueObj['big'] = $object->leagueNameBig1;
                $leagueObj['gb'] = $object->leagueNameGb1;
                $leagueObj['kr'] = $object->leagueNameKr1;
                $leagueObj['vn'] = $object->leagueNameVn1;
                $leagueObj['la'] = $object->leagueNameLa1;
                $lang_leauge[$object->leagueId1] = $leagueObj;
            }

            unset($object->leagueNameEn1);
            unset($object->leagueNameTh1);
            unset($object->leagueNameBig1);
            unset($object->leagueNameGb1);
            unset($object->leagueNameKr1);
            unset($object->leagueNameVn1);
            unset($object->leagueNameLa1);

            if ($object->leagueId2) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn2;
                $leagueObj['th'] = $object->leagueNameTh2;
                $leagueObj['big'] = $object->leagueNameBig2;
                $leagueObj['gb'] = $object->leagueNameGb2;
                $leagueObj['kr'] = $object->leagueNameKr2;
                $leagueObj['vn'] = $object->leagueNameVn2;
                $leagueObj['la'] = $object->leagueNameLa2;
                $lang_leauge[$object->leagueId2] = $leagueObj;
            }

            unset($object->leagueNameEn2);
            unset($object->leagueNameTh2);
            unset($object->leagueNameBig2);
            unset($object->leagueNameGb2);
            unset($object->leagueNameKr2);
            unset($object->leagueNameVn2);
            unset($object->leagueNameLa2);

            if ($object->leagueId3) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn2 = 3;
                $leagueObj['th'] = $object->leagueNameTh3;
                $leagueObj['big'] = $object->leagueNameBig3;
                $leagueObj['gb'] = $object->leagueNameGb3;
                $leagueObj['kr'] = $object->leagueNameKr3;
                $leagueObj['vn'] = $object->leagueNameVn3;
                $leagueObj['la'] = $object->leagueNameLa3;
                $lang_leauge[$object->leagueId3] = $leagueObj;
            }

            unset($object->leagueNameEn3);
            unset($object->leagueNameTh3);
            unset($object->leagueNameBig3);
            unset($object->leagueNameGb3);
            unset($object->leagueNameKr3);
            unset($object->leagueNameVn3);
            unset($object->leagueNameLa3);

            if ($object->leagueId4) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn4;
                $leagueObj['th'] = $object->leagueNameTh4;
                $leagueObj['big'] = $object->leagueNameBig4;
                $leagueObj['gb'] = $object->leagueNameGb4;
                $leagueObj['kr'] = $object->leagueNameKr4;
                $leagueObj['vn'] = $object->leagueNameVn4;
                $leagueObj['la'] = $object->leagueNameLa4;
                $lang_leauge[$object->leagueId4] = $leagueObj;
            }

            unset($object->leagueNameEn4);
            unset($object->leagueNameTh4);
            unset($object->leagueNameBig4);
            unset($object->leagueNameGb4);
            unset($object->leagueNameKr4);
            unset($object->leagueNameVn4);
            unset($object->leagueNameLa4);

            if ($object->teamId1) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn1;
                $teamObj['th'] = $object->teamNameTh1;
                $teamObj['big'] = $object->teamNameBig1;
                $teamObj['gb'] = $object->teamNameGb1;
                $teamObj['kr'] = $object->teamNameKr1;
                $teamObj['vn'] = $object->teamNameVn1;
                $teamObj['la'] = $object->teamNameLa1;
                $lang_team[$object->teamId1] = $teamObj;
            }

            unset($object->teamNameEn1);
            unset($object->teamNameTh1);
            unset($object->teamNameBig1);
            unset($object->teamNameGb1);
            unset($object->teamNameKr1);
            unset($object->teamNameVn1);
            unset($object->teamNameLa1);

            if ($object->teamId2) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn2;
                $teamObj['th'] = $object->teamNameTh2;
                $teamObj['big'] = $object->teamNameBig2;
                $teamObj['gb'] = $object->teamNameGb2;
                $teamObj['kr'] = $object->teamNameKr2;
                $teamObj['vn'] = $object->teamNameVn2;
                $teamObj['la'] = $object->teamNameLa2;
                $lang_team[$object->teamId2] = $teamObj;
            }

            unset($object->teamNameEn2);
            unset($object->teamNameTh2);
            unset($object->teamNameBig2);
            unset($object->teamNameGb2);
            unset($object->teamNameKr2);
            unset($object->teamNameVn2);
            unset($object->teamNameLa2);

            if ($object->teamId3) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn3;
                $teamObj['th'] = $object->teamNameTh3;
                $teamObj['big'] = $object->teamNameBig3;
                $teamObj['gb'] = $object->teamNameGb3;
                $teamObj['kr'] = $object->teamNameKr3;
                $teamObj['vn'] = $object->teamNameVn3;
                $teamObj['la'] = $object->teamNameLa3;
                $lang_team[$object->teamId3] = $teamObj;
            }

            unset($object->teamNameEn3);
            unset($object->teamNameTh3);
            unset($object->teamNameBig3);
            unset($object->teamNameGb3);
            unset($object->teamNameKr3);
            unset($object->teamNameVn3);
            unset($object->teamNameLa3);

            if ($object->teamId4) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn4;
                $teamObj['th'] = $object->teamNameTh4;
                $teamObj['big'] = $object->teamNameBig4;
                $teamObj['gb'] = $object->teamNameGb4;
                $teamObj['kr'] = $object->teamNameKr4;
                $teamObj['vn'] = $object->teamNameVn4;
                $teamObj['la'] = $object->teamNameLa4;
                $lang_team[$object->teamId4] = $teamObj;
            }

            unset($object->teamNameEn4);
            unset($object->teamNameTh4);
            unset($object->teamNameBig4);
            unset($object->teamNameGb4);
            unset($object->teamNameKr4);
            unset($object->teamNameVn4);
            unset($object->teamNameLa4);
            array_push($data, $object);
        }
        $jsonObj = array(
            'data' => $data,
            'lang_title' => $lang_title,
            'lang_content' => $lang_content,
            'lang_shortDescription' => $lang_shortDescription,
            'lang_league' => $lang_leauge,
            'lang_team' => $lang_team,
        );
        $json = json_encode($jsonObj);
        file_put_contents('league_news/' . $leagueId . '.json', $json);
        //chmod('league_news/' . $leagueId . '.json', 0777);
        //querylog(__FUNCTION__, $db);
    }

    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function teamNews($teamId)
{

    $lang = array(
        'en', 'th', 'big', 'gb', 'kr', 'vn', 'la'
    );
    $limit = 60 * 60 * 2;
    $file_name = 'team_news/' . $teamId . '.json';
    $in_present = false;
    if (file_exists($file_name)) {
        $file_timestamp = filemtime($file_name);
        if (time() - $file_timestamp < $limit) {
            $in_present = true;
        }
    }

    if ($in_present) {
        $json = file_get_contents($file_name);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $list_sql = "select news.*,
l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
from news
left join lang_league as l1 on l1.leagueId = news.leagueId1
left join lang_league as l2 on l2.leagueId = news.leagueId2
left join lang_league as l3 on l3.leagueId = news.leagueId3
left join lang_league as l4 on l4.leagueId = news.leagueId4
left join lang_team as t1 on t1.tid = news.teamId1
left join lang_team as t2 on t2.tid = news.teamId2
left join lang_team as t3 on t3.tid = news.teamId3
left join lang_team as t4 on t4.tid = news.teamId4
where teamId1 = $teamId or teamId2 =$teamId or teamId3= $teamId or teamId4 = $teamId
order by newsId DESC
limit 10";
        $stmt = $db->query($list_sql);
        $list_all = $stmt->fetchAll(PDO::FETCH_OBJ);
        $lang_leauge = array();
        $lang_team = array();
        $lang_content = array();
        $lang_title = array();
        $lang_shortDescription = array();
        $data = array();
        foreach ($list_all as $object) {
            $newsId = $object->newsId;
            $lang_content_obj = array();
            $lang_content_obj['en'] = $object->contentEn;
            $lang_content_obj['th'] = $object->contentTh;
            $lang_content_obj['big'] = $object->contentBig;
            $lang_content_obj['gb'] = $object->contentGb;
            $lang_content_obj['kr'] = $object->contentKr;
            $lang_content_obj['vn'] = $object->contentVn;
            $lang_content_obj['la'] = $object->contentLa;
            $lang_content[$newsId] = $lang_content_obj;

            unset($object->contentEn);
            unset($object->contentTh);
            unset($object->contentBig);
            unset($object->contentGb);
            unset($object->contentKr);
            unset($object->contentVn);
            unset($object->contentLa);

            $lang_title_obj = array();
            $lang_title_obj['en'] = $object->titleEn;
            $lang_title_obj['th'] = $object->titleTh;
            $lang_title_obj['big'] = $object->titleBig;
            $lang_title_obj['gb'] = $object->titleGb;
            $lang_title_obj['kr'] = $object->titleKr;
            $lang_title_obj['vn'] = $object->titleVn;
            $lang_title_obj['la'] = $object->titleLa;
            $lang_title[$newsId] = $lang_title_obj;

            unset($object->titleEn);
            unset($object->titleTh);
            unset($object->titleBig);
            unset($object->titleGb);
            unset($object->titleKr);
            unset($object->titleVn);
            unset($object->titleLa);

            $lang_shortDescription_obj = array();
            $lang_shortDescription_obj['en'] = $object->shortDescriptionEn;
            $lang_shortDescription_obj['th'] = $object->shortDescriptionTh;
            $lang_shortDescription_obj['big'] = $object->shortDescriptionBig;
            $lang_shortDescription_obj['gb'] = $object->shortDescriptionGb;
            $lang_shortDescription_obj['kr'] = $object->shortDescriptionKr;
            $lang_shortDescription_obj['vn'] = $object->shortDescriptionVn;
            $lang_shortDescription_obj['la'] = $object->shortDescriptionLa;
            $lang_shortDescription[$newsId] = $lang_shortDescription_obj;

            unset($object->shortDescriptionEn);
            unset($object->shortDescriptionTh);
            unset($object->shortDescriptionBig);
            unset($object->shortDescriptionGb);
            unset($object->shortDescriptionKr);
            unset($object->shortDescriptionVn);
            unset($object->shortDescriptionLa);


            if ($object->leagueId1) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn1;
                $leagueObj['th'] = $object->leagueNameTh1;
                $leagueObj['big'] = $object->leagueNameBig1;
                $leagueObj['gb'] = $object->leagueNameGb1;
                $leagueObj['kr'] = $object->leagueNameKr1;
                $leagueObj['vn'] = $object->leagueNameVn1;
                $leagueObj['la'] = $object->leagueNameLa1;
                $lang_leauge[$object->leagueId1] = $leagueObj;
            }

            unset($object->leagueNameEn1);
            unset($object->leagueNameTh1);
            unset($object->leagueNameBig1);
            unset($object->leagueNameGb1);
            unset($object->leagueNameKr1);
            unset($object->leagueNameVn1);
            unset($object->leagueNameLa1);

            if ($object->leagueId2) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn2;
                $leagueObj['th'] = $object->leagueNameTh2;
                $leagueObj['big'] = $object->leagueNameBig2;
                $leagueObj['gb'] = $object->leagueNameGb2;
                $leagueObj['kr'] = $object->leagueNameKr2;
                $leagueObj['vn'] = $object->leagueNameVn2;
                $leagueObj['la'] = $object->leagueNameLa2;
                $lang_leauge[$object->leagueId2] = $leagueObj;
            }

            unset($object->leagueNameEn2);
            unset($object->leagueNameTh2);
            unset($object->leagueNameBig2);
            unset($object->leagueNameGb2);
            unset($object->leagueNameKr2);
            unset($object->leagueNameVn2);
            unset($object->leagueNameLa2);

            if ($object->leagueId3) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn2 = 3;
                $leagueObj['th'] = $object->leagueNameTh3;
                $leagueObj['big'] = $object->leagueNameBig3;
                $leagueObj['gb'] = $object->leagueNameGb3;
                $leagueObj['kr'] = $object->leagueNameKr3;
                $leagueObj['vn'] = $object->leagueNameVn3;
                $leagueObj['la'] = $object->leagueNameLa3;
                $lang_leauge[$object->leagueId3] = $leagueObj;
            }

            unset($object->leagueNameEn3);
            unset($object->leagueNameTh3);
            unset($object->leagueNameBig3);
            unset($object->leagueNameGb3);
            unset($object->leagueNameKr3);
            unset($object->leagueNameVn3);
            unset($object->leagueNameLa3);

            if ($object->leagueId4) {
                $leagueObj = array();
                $leagueObj['en'] = $object->leagueNameEn4;
                $leagueObj['th'] = $object->leagueNameTh4;
                $leagueObj['big'] = $object->leagueNameBig4;
                $leagueObj['gb'] = $object->leagueNameGb4;
                $leagueObj['kr'] = $object->leagueNameKr4;
                $leagueObj['vn'] = $object->leagueNameVn4;
                $leagueObj['la'] = $object->leagueNameLa4;
                $lang_leauge[$object->leagueId4] = $leagueObj;
            }

            unset($object->leagueNameEn4);
            unset($object->leagueNameTh4);
            unset($object->leagueNameBig4);
            unset($object->leagueNameGb4);
            unset($object->leagueNameKr4);
            unset($object->leagueNameVn4);
            unset($object->leagueNameLa4);

            if ($object->teamId1) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn1;
                $teamObj['th'] = $object->teamNameTh1;
                $teamObj['big'] = $object->teamNameBig1;
                $teamObj['gb'] = $object->teamNameGb1;
                $teamObj['kr'] = $object->teamNameKr1;
                $teamObj['vn'] = $object->teamNameVn1;
                $teamObj['la'] = $object->teamNameLa1;
                $lang_team[$object->teamId1] = $teamObj;
            }

            unset($object->teamNameEn1);
            unset($object->teamNameTh1);
            unset($object->teamNameBig1);
            unset($object->teamNameGb1);
            unset($object->teamNameKr1);
            unset($object->teamNameVn1);
            unset($object->teamNameLa1);

            if ($object->teamId2) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn2;
                $teamObj['th'] = $object->teamNameTh2;
                $teamObj['big'] = $object->teamNameBig2;
                $teamObj['gb'] = $object->teamNameGb2;
                $teamObj['kr'] = $object->teamNameKr2;
                $teamObj['vn'] = $object->teamNameVn2;
                $teamObj['la'] = $object->teamNameLa2;
                $lang_team[$object->teamId2] = $teamObj;
            }

            unset($object->teamNameEn2);
            unset($object->teamNameTh2);
            unset($object->teamNameBig2);
            unset($object->teamNameGb2);
            unset($object->teamNameKr2);
            unset($object->teamNameVn2);
            unset($object->teamNameLa2);

            if ($object->teamId3) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn3;
                $teamObj['th'] = $object->teamNameTh3;
                $teamObj['big'] = $object->teamNameBig3;
                $teamObj['gb'] = $object->teamNameGb3;
                $teamObj['kr'] = $object->teamNameKr3;
                $teamObj['vn'] = $object->teamNameVn3;
                $teamObj['la'] = $object->teamNameLa3;
                $lang_team[$object->teamId3] = $teamObj;
            }

            unset($object->teamNameEn3);
            unset($object->teamNameTh3);
            unset($object->teamNameBig3);
            unset($object->teamNameGb3);
            unset($object->teamNameKr3);
            unset($object->teamNameVn3);
            unset($object->teamNameLa3);

            if ($object->teamId4) {
                $teamObj = array();
                $teamObj['en'] = $object->teamNameEn4;
                $teamObj['th'] = $object->teamNameTh4;
                $teamObj['big'] = $object->teamNameBig4;
                $teamObj['gb'] = $object->teamNameGb4;
                $teamObj['kr'] = $object->teamNameKr4;
                $teamObj['vn'] = $object->teamNameVn4;
                $teamObj['la'] = $object->teamNameLa4;
                $lang_team[$object->teamId4] = $teamObj;
            }

            unset($object->teamNameEn4);
            unset($object->teamNameTh4);
            unset($object->teamNameBig4);
            unset($object->teamNameGb4);
            unset($object->teamNameKr4);
            unset($object->teamNameVn4);
            unset($object->teamNameLa4);
            array_push($data, $object);
        }
        $jsonObj = array(
            'data' => $data,
            'lang_title' => $lang_title,
            'lang_content' => $lang_content,
            'lang_shortDescription' => $lang_shortDescription,
            'lang_league' => $lang_leauge,
            'lang_team' => $lang_team,
        );
        $json = json_encode($jsonObj);
        file_put_contents('team_news/' . $teamId . '.json', $json);
        //chmod('team_news/' . $teamId . '.json', 0777);
        //querylog(__FUNCTION__, $db);
    }

    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function newsDetail($newsId)
{
    $lang = array(
        'en', 'th', 'big', 'gb', 'kr', 'vn', 'la'
    );
    $limit = 60 * 60 * 2;
    $file_name = 'news/' . $newsId . '.json';
    $in_present = false;
    if (file_exists($file_name)) {
        $file_timestamp = filemtime($file_name);
        if (time() - $file_timestamp < $limit) {
            $in_present = true;
        }
    }

    if ($in_present) {
        $json = file_get_contents($file_name);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $list_sql = "select news.*,
l1.leagueNameEn as leagueNameEn1,l1.leagueNameTh as leagueNameTh1,l1.leagueNameBig as leagueNameBig1,l1.leagueNameGb as leagueNameGb1,l1.leagueNameKr as leagueNameKr1,l1.leagueNameVn as leagueNameVn1,l1.leagueNameLa as leagueNameLa1,
l2.leagueNameEn as leagueNameEn2,l2.leagueNameTh as leagueNameTh2,l2.leagueNameBig as leagueNameBig2,l2.leagueNameGb as leagueNameGb2,l2.leagueNameKr as leagueNameKr2,l2.leagueNameVn as leagueNameVn2,l2.leagueNameLa as leagueNameLa2,
l3.leagueNameEn as leagueNameEn3,l3.leagueNameTh as leagueNameTh3,l3.leagueNameBig as leagueNameBig3,l3.leagueNameGb as leagueNameGb3,l3.leagueNameKr as leagueNameKr3,l3.leagueNameVn as leagueNameVn3,l3.leagueNameLa as leagueNameLa3,
l4.leagueNameEn as leagueNameEn4,l4.leagueNameTh as leagueNameTh4,l4.leagueNameBig as leagueNameBig4,l4.leagueNameGb as leagueNameGb4,l4.leagueNameKr as leagueNameKr4,l4.leagueNameVn as leagueNameVn4,l4.leagueNameLa as leagueNameLa4,
t1.teamNameEn as teamNameEn1,t1.teamNameTh as teamNameTh1,t1.teamNameBig as teamNameBig1,t1.teamNameGb as teamNameGb1,t1.teamNameKr as teamNameKr1,t1.teamNameVn as teamNameVn1,t1.teamNameLa as teamNameLa1,
t2.teamNameEn as teamNameEn2,t2.teamNameTh as teamNameTh2,t2.teamNameBig as teamNameBig2,t2.teamNameGb as teamNameGb2,t2.teamNameKr as teamNameKr2,t2.teamNameVn as teamNameVn2,t2.teamNameLa as teamNameLa2,
t3.teamNameEn as teamNameEn3,t3.teamNameTh as teamNameTh3,t3.teamNameBig as teamNameBig3,t3.teamNameGb as teamNameGb3,t3.teamNameKr as teamNameKr3,t3.teamNameVn as teamNameVn3,t3.teamNameLa as teamNameLa3,
t4.teamNameEn as teamNameEn4,t4.teamNameTh as teamNameTh4,t4.teamNameBig as teamNameBig4,t4.teamNameGb as teamNameGb4,t4.teamNameKr as teamNameKr4,t4.teamNameVn as teamNameVn4,t4.teamNameLa as teamNameLa4
from news
left join lang_league as l1 on l1.leagueId = news.leagueId1
left join lang_league as l2 on l2.leagueId = news.leagueId2
left join lang_league as l3 on l3.leagueId = news.leagueId3
left join lang_league as l4 on l4.leagueId = news.leagueId4
left join lang_team as t1 on t1.tid = news.teamId1
left join lang_team as t2 on t2.tid = news.teamId2
left join lang_team as t3 on t3.tid = news.teamId3
left join lang_team as t4 on t4.tid = news.teamId4
where newsId=$newsId";
        $stmt = $db->query($list_sql);
        $object = $stmt->fetch(PDO::FETCH_OBJ);
        $lang_leauge = array();
        $lang_team = array();
        $lang_content = array();
        $lang_title = array();
        $lang_shortDescription = array();

        $lang_content_obj = array();
        $lang_content_obj['en'] = $object->contentEn;
        $lang_content_obj['th'] = $object->contentTh;
        $lang_content_obj['big'] = $object->contentBig;
        $lang_content_obj['gb'] = $object->contentGb;
        $lang_content_obj['kr'] = $object->contentKr;
        $lang_content_obj['vn'] = $object->contentVn;
        $lang_content_obj['la'] = $object->contentLa;
        $lang_content[$newsId] = $lang_content_obj;

        unset($object->contentEn);
        unset($object->contentTh);
        unset($object->contentBig);
        unset($object->contentGb);
        unset($object->contentKr);
        unset($object->contentVn);
        unset($object->contentLa);

        $lang_title_obj = array();
        $lang_title_obj['en'] = $object->titleEn;
        $lang_title_obj['th'] = $object->titleTh;
        $lang_title_obj['big'] = $object->titleBig;
        $lang_title_obj['gb'] = $object->titleGb;
        $lang_title_obj['kr'] = $object->titleKr;
        $lang_title_obj['vn'] = $object->titleVn;
        $lang_title_obj['la'] = $object->titleLa;
        $lang_title[$newsId] = $lang_title_obj;

        unset($object->titleEn);
        unset($object->titleTh);
        unset($object->titleBig);
        unset($object->titleGb);
        unset($object->titleKr);
        unset($object->titleVn);
        unset($object->titleLa);

        $lang_shortDescription_obj = array();
        $lang_shortDescription_obj['en'] = $object->shortDescriptionEn;
        $lang_shortDescription_obj['th'] = $object->shortDescriptionTh;
        $lang_shortDescription_obj['big'] = $object->shortDescriptionBig;
        $lang_shortDescription_obj['gb'] = $object->shortDescriptionGb;
        $lang_shortDescription_obj['kr'] = $object->shortDescriptionKr;
        $lang_shortDescription_obj['vn'] = $object->shortDescriptionVn;
        $lang_shortDescription_obj['la'] = $object->shortDescriptionLa;
        $lang_shortDescription[$newsId] = $lang_shortDescription_obj;

        unset($object->shortDescriptionEn);
        unset($object->shortDescriptionTh);
        unset($object->shortDescriptionBig);
        unset($object->shortDescriptionGb);
        unset($object->shortDescriptionKr);
        unset($object->shortDescriptionVn);
        unset($object->shortDescriptionLa);


        if ($object->leagueId1) {
            $leagueObj = array();
            $leagueObj['en'] = $object->leagueNameEn1;
            $leagueObj['th'] = $object->leagueNameTh1;
            $leagueObj['big'] = $object->leagueNameBig1;
            $leagueObj['gb'] = $object->leagueNameGb1;
            $leagueObj['kr'] = $object->leagueNameKr1;
            $leagueObj['vn'] = $object->leagueNameVn1;
            $leagueObj['la'] = $object->leagueNameLa1;
            $lang_leauge[$object->leagueId1] = $leagueObj;
        }

        unset($object->leagueNameEn1);
        unset($object->leagueNameTh1);
        unset($object->leagueNameBig1);
        unset($object->leagueNameGb1);
        unset($object->leagueNameKr1);
        unset($object->leagueNameVn1);
        unset($object->leagueNameLa1);

        if ($object->leagueId2) {
            $leagueObj = array();
            $leagueObj['en'] = $object->leagueNameEn2;
            $leagueObj['th'] = $object->leagueNameTh2;
            $leagueObj['big'] = $object->leagueNameBig2;
            $leagueObj['gb'] = $object->leagueNameGb2;
            $leagueObj['kr'] = $object->leagueNameKr2;
            $leagueObj['vn'] = $object->leagueNameVn2;
            $leagueObj['la'] = $object->leagueNameLa2;
            $lang_leauge[$object->leagueId2] = $leagueObj;
        }

        unset($object->leagueNameEn2);
        unset($object->leagueNameTh2);
        unset($object->leagueNameBig2);
        unset($object->leagueNameGb2);
        unset($object->leagueNameKr2);
        unset($object->leagueNameVn2);
        unset($object->leagueNameLa2);

        if ($object->leagueId3) {
            $leagueObj = array();
            $leagueObj['en'] = $object->leagueNameEn2 = 3;
            $leagueObj['th'] = $object->leagueNameTh3;
            $leagueObj['big'] = $object->leagueNameBig3;
            $leagueObj['gb'] = $object->leagueNameGb3;
            $leagueObj['kr'] = $object->leagueNameKr3;
            $leagueObj['vn'] = $object->leagueNameVn3;
            $leagueObj['la'] = $object->leagueNameLa3;
            $lang_leauge[$object->leagueId3] = $leagueObj;
        }

        unset($object->leagueNameEn3);
        unset($object->leagueNameTh3);
        unset($object->leagueNameBig3);
        unset($object->leagueNameGb3);
        unset($object->leagueNameKr3);
        unset($object->leagueNameVn3);
        unset($object->leagueNameLa3);

        if ($object->leagueId4) {
            $leagueObj = array();
            $leagueObj['en'] = $object->leagueNameEn4;
            $leagueObj['th'] = $object->leagueNameTh4;
            $leagueObj['big'] = $object->leagueNameBig4;
            $leagueObj['gb'] = $object->leagueNameGb4;
            $leagueObj['kr'] = $object->leagueNameKr4;
            $leagueObj['vn'] = $object->leagueNameVn4;
            $leagueObj['la'] = $object->leagueNameLa4;
            $lang_leauge[$object->leagueId4] = $leagueObj;
        }

        unset($object->leagueNameEn4);
        unset($object->leagueNameTh4);
        unset($object->leagueNameBig4);
        unset($object->leagueNameGb4);
        unset($object->leagueNameKr4);
        unset($object->leagueNameVn4);
        unset($object->leagueNameLa4);

        if ($object->teamId1) {
            $teamObj = array();
            $teamObj['en'] = $object->teamNameEn1;
            $teamObj['th'] = $object->teamNameTh1;
            $teamObj['big'] = $object->teamNameBig1;
            $teamObj['gb'] = $object->teamNameGb1;
            $teamObj['kr'] = $object->teamNameKr1;
            $teamObj['vn'] = $object->teamNameVn1;
            $teamObj['la'] = $object->teamNameLa1;
            $lang_team[$object->teamId1] = $teamObj;
        }

        unset($object->teamNameEn1);
        unset($object->teamNameTh1);
        unset($object->teamNameBig1);
        unset($object->teamNameGb1);
        unset($object->teamNameKr1);
        unset($object->teamNameVn1);
        unset($object->teamNameLa1);

        if ($object->teamId2) {
            $teamObj = array();
            $teamObj['en'] = $object->teamNameEn2;
            $teamObj['th'] = $object->teamNameTh2;
            $teamObj['big'] = $object->teamNameBig2;
            $teamObj['gb'] = $object->teamNameGb2;
            $teamObj['kr'] = $object->teamNameKr2;
            $teamObj['vn'] = $object->teamNameVn2;
            $teamObj['la'] = $object->teamNameLa2;
            $lang_team[$object->teamId2] = $teamObj;
        }

        unset($object->teamNameEn2);
        unset($object->teamNameTh2);
        unset($object->teamNameBig2);
        unset($object->teamNameGb2);
        unset($object->teamNameKr2);
        unset($object->teamNameVn2);
        unset($object->teamNameLa2);

        if ($object->teamId3) {
            $teamObj = array();
            $teamObj['en'] = $object->teamNameEn3;
            $teamObj['th'] = $object->teamNameTh3;
            $teamObj['big'] = $object->teamNameBig3;
            $teamObj['gb'] = $object->teamNameGb3;
            $teamObj['kr'] = $object->teamNameKr3;
            $teamObj['vn'] = $object->teamNameVn3;
            $teamObj['la'] = $object->teamNameLa3;
            $lang_team[$object->teamId3] = $teamObj;
        }

        unset($object->teamNameEn3);
        unset($object->teamNameTh3);
        unset($object->teamNameBig3);
        unset($object->teamNameGb3);
        unset($object->teamNameKr3);
        unset($object->teamNameVn3);
        unset($object->teamNameLa3);

        if ($object->teamId4) {
            $teamObj = array();
            $teamObj['en'] = $object->teamNameEn4;
            $teamObj['th'] = $object->teamNameTh4;
            $teamObj['big'] = $object->teamNameBig4;
            $teamObj['gb'] = $object->teamNameGb4;
            $teamObj['kr'] = $object->teamNameKr4;
            $teamObj['vn'] = $object->teamNameVn4;
            $teamObj['la'] = $object->teamNameLa4;
            $lang_team[$object->teamId4] = $teamObj;
        }

        unset($object->teamNameEn4);
        unset($object->teamNameTh4);
        unset($object->teamNameBig4);
        unset($object->teamNameGb4);
        unset($object->teamNameKr4);
        unset($object->teamNameVn4);
        unset($object->teamNameLa4);

        $jsonObj = array(
            'data' => array($object),
            'lang_title' => $lang_title,
            'lang_content' => $lang_content,
            'lang_shortDescription' => $lang_shortDescription,
            'lang_league' => $lang_leauge,
            'lang_team' => $lang_team,
        );
        $json = json_encode($jsonObj);
        file_put_contents('news/' . $newsId . '.json', $json);
        //chmod('news/' . $newsId . '.json', 0777);
        //querylog(__FUNCTION__, $db);
    }

    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function allNews()
{
    $filename = 'news/allNews.json';
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = null;
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function dailyNews()
{
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "th";
    $limit = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 8;
    $results = array("lang" => $lang, "data" => array(), "titles" => array(), "desc" => array(), "fullstory" => array(), "ontimelines" => array());
    $filename = 'news/allNews.json';
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
        $newslist = json_decode($json, true);
        foreach ($newslist["data"] as $key => $data) {
            if ($key < $limit) {
                $tlsql = "SELECT id FROM timelines WHERE content_type='news' AND key_list='{$data["newsId"]}'";
                $stmt = $db->query($tlsql);
                $timeline = $stmt->fetch(5);
                if (!empty($timeline)) {
                    $results["ontimelines"][$data["newsId"]] = $timeline->id;
                }
                $results["data"][$key] = $data;
                $results["titles"][$data["newsId"]] = $newslist["lang_title"][$data['newsId']][$lang];
                $results["desc"][$data["newsId"]] = $newslist["lang_shortDescription"][$data['newsId']][$lang];
                $results["fullstory"][$data["newsId"]] = $newslist["lang_content"][$data['newsId']][$lang];
            } else {
                break;
            }
        }
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function allTransferLeague()
{
    $filename = 'cronjob_gen_file/files/allTransferLeague.json';
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = null;
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function playerTransferLeague($leagueId)
{
    $filename = 'player_transfer/' . $leagueId . '.json';
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {


        $lang_player = array();
        $player = array(
            'en' => 'Antolin Viveros Alcaraz',
            'big' => 'Antolin Viveros Alcaraz',
            'gb' => 'Antolin Viveros Alcaraz',
            'la' => 'Antolin Viveros Alcaraz',
            'th' => 'Antolin Viveros Alcaraz',
            'kr' => 'Antolin Viveros Alcaraz',
            'vn' => 'Antolin Viveros Alcaraz',
        );
        $lang_player[70996] = $player;
        $lang_player[70997] = $player;

        $team = array(
            'en' => 'Arsenal',
            'big' => 'Arsenal',
            'gb' => 'Arsenal',
            'la' => 'Arsenal',
            'th' => 'Arsenal',
            'kr' => 'Arsenal',
            'vn' => 'Arsenal',
        );
        $lang_team = array();
        $lang_team[569] = $team;
        $lang_team[193] = $team;
        $transfer = array(
            'transfer_date' => '01-07-2013',
            'playerId' => 70996,
            'position' => 'Striker',
            'toTid' => 569,
            'fromTid' => 193,
            'transfer_state' => 'Confirmed',
            'transfer_type' => 'Owned Wholly',
        );
        $transfer_list = array(
            $transfer,
            $transfer,
        );
        $data = array(
            'join_in' => $transfer_list,
            'departure' => $transfer_list,
        );
        $obj = array(
            'data' => $data,
            'lang_player' => $lang_player,
            'lang_team' => $lang_team,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function playerTransferTeam($tid)
{

    $lang_player = array();
    $player = array(
        'en' => 'Antolin Viveros Alcaraz',
        'big' => 'Antolin Viveros Alcaraz',
        'gb' => 'Antolin Viveros Alcaraz',
        'la' => 'Antolin Viveros Alcaraz',
        'th' => 'Antolin Viveros Alcaraz',
        'kr' => 'Antolin Viveros Alcaraz',
        'vn' => 'Antolin Viveros Alcaraz',
    );
    $lang_player[70996] = $player;
    $lang_player[70997] = $player;

    $team = array(
        'en' => 'Arsenal',
        'big' => 'Arsenal',
        'gb' => 'Arsenal',
        'la' => 'Arsenal',
        'th' => 'Arsenal',
        'kr' => 'Arsenal',
        'vn' => 'Arsenal',
    );
    $lang_team = array();
    $lang_team[569] = $team;
    $lang_team[193] = $team;
    $transfer = array(
        'transfer_date' => '01-07-2013',
        'playerId' => 70996,
        'position' => 'Striker',
        'toTid' => 569,
        'fromTid' => 193,
        'transfer_state' => 'Confirmed',
        'transfer_type' => 'Owned Wholly',
    );
    $transfer_list = array(
        $transfer,
        $transfer,
    );
    $data = array(
        'join_in' => $transfer_list,
        'departure' => $transfer_list,
    );
    $obj = array(
        'data' => $data,
        'lang_player' => $lang_player,
        'lang_team' => $lang_team,
    );
    $json = json_encode($obj);
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function topPlayerScore($leagueId)
{
    $filename = "top_player_score/$leagueId.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {

        $obj = new stdClass();
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function subLeagueData($leagueId)
{
    $limit = 60 * 60 * 2;

    $file_name = 'sub_league_data/' . $leagueId . '.json';
    $in_present = false;
    if (file_exists($file_name)) {
        $file_timestamp = filemtime($file_name);
        if (time() - $file_timestamp < $limit) {
            $in_present = true;
        }
    }

    if ($in_present) {
        $json = file_get_contents($file_name);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql_subLeague = "select sub_league.* from sub_league where leagueId=$leagueId order by seq ASC";
        $stmt = $db->query($sql_subLeague);
        $subLeagueList = $stmt->fetchAll();
        $json = json_encode(array(
            'sub_league' => array(),
            'latest_results' => array(),
            'next_matches' => array(),
        ));
        if (count($subLeagueList) > 0) {
            $sub_league_array = array();
            $sql_league = "
                select league.leagueId,league.competitionId,league.leagueNamePk,league.leagueName,
                if(lang_league.leagueNameEn is null ,league.leagueName,lang_league.leagueNameEn) as leagueNameEn,
                if(lang_league.leagueNameBig is null ,league.leagueName,lang_league.leagueNameBig) as leagueNameBig,
                if(lang_league.leagueNameGb is null ,league.leagueName,lang_league.leagueNameGb) as leagueNameGb,
                if(lang_league.leagueNameKr is null ,league.leagueName,lang_league.leagueNameKr) as leagueNameKr,
                if(lang_league.leagueNameTh is null ,league.leagueName,lang_league.leagueNameTh) as leagueNameTh,
                if(lang_league.leagueNameLa is null ,league.leagueName,lang_league.leagueNameLa) as leagueNameLa,
                if(lang_league.leagueNameVn is null ,league.leagueName,lang_league.leagueNameVn) as leagueNameVn
                from league left join lang_league on league.leagueId = lang_league.leagueId where league.leagueId=$leagueId
                    ";
            //echo $sql_league;
            //exit;
            $stmt_leauge = $db->query($sql_league);
            $league = $stmt_leauge->fetch(PDO::FETCH_OBJ);
            $team_list = array();
            $lang_league = array(
                'en' => $league->leagueNameEn,
                'big' => $league->leagueNameBig,
                'gb' => $league->leagueNameGb,
                'kr' => $league->leagueNameKr,
                'th' => $league->leagueNameTh,
                'la' => $league->leagueNameLa,
                'vn' => $league->leagueNameVn,
            );
            $sub_league_array[0] = array(
                'subLeagueNamePk' => $league->leagueNamePk,
                'subLeagueName' => $league->leagueName,
                'leagueId' => $leagueId,
                'competitionId' => $league->competitionId,
                'seq' => "0",
                'latest_results_league' => array(),
                'next_matches_league' => array(),
                'stat_table' => array(),
            );
            foreach ($subLeagueList as $subLeague) {
                unset($subLeague[0]);
                unset($subLeague[1]);
                unset($subLeague[2]);
                unset($subLeague[3]);
                unset($subLeague[4]);
                $subLeague['latest_results_league'] = array();
                $subLeague['next_matches_league'] = array();
                $subLeague['stat_table'] = array();
                $sub_league_array[$subLeague['seq']] = $subLeague;
            }

            $sql_latest_results = "
                select s.seq,r.* from latest_results_league r
                left join
                sub_league as s  on s.leagueId = r.leagueId and s.subLeagueNamePk = r.subLeagueNamePk

                where r.leagueId=$leagueId
                order by s.seq ASC,r.match_timestamp ASC
                ";

            $sql_next_matches = "
                select s.seq,n.* from next_matches_league n
                left join
                sub_league as s  on s.leagueId = n.leagueId and s.subLeagueNamePk = n.subLeagueNamePk
                where n.leagueId=$leagueId
                order by s.seq ASC,n.match_timestamp ASC
                ";

            $sql_stat_table = "
                select s.seq,stat.* from stat_table stat
                left join
                sub_league as s on s.leagueId = stat.leagueId and s.subLeagueNamePk = stat.subLeagueNamePk
                where stat.leagueId=$leagueId
                order by s.seq ASC,stat.no ASC
                ";
            $stmt_stat_table = $db->query($sql_stat_table);
            $stat_table = $stmt_stat_table->fetchAll(PDO::FETCH_OBJ);
            $stat_table_size = count($stat_table);

            if ($stat_table_size > 0) {

                for ($i = 0; $i < $stat_table_size; $i++) {
                    array_push($team_list, $stat_table[$i]->tid);
                    $seq = $stat_table[$i]->seq ? $stat_table[$i]->seq : 0;
                    unset($stat_table[$i]->seq);
                    array_push($sub_league_array[$seq]['stat_table'], $stat_table[$i]);
                }
            }
            $stmt_latest_results = $db->query($sql_latest_results);
            $latest_results = $stmt_latest_results->fetchAll(PDO::FETCH_OBJ);
            $latest_results_size = count($latest_results);
            if ($latest_results_size > 0) {
                for ($i = 0; $i < $latest_results_size; $i++) {

                    array_push($team_list, $latest_results[$i]->tid1);
                    array_push($team_list, $latest_results[$i]->tid2);
                    $seq = $latest_results[$i]->seq ? $latest_results[$i]->seq : 0;
                    unset($latest_results[$i]->seq);
                    array_push($sub_league_array[$seq]['latest_results_league'], $latest_results[$i]);
                }
            }
            $stmt_next_matches = $db->query($sql_next_matches);
            $next_matches = $stmt_next_matches->fetchAll(PDO::FETCH_OBJ);
            $next_matches_size = count($next_matches);
            if ($next_matches_size > 0) {
                for ($i = 0; $i < $next_matches_size; $i++) {
                    array_push($team_list, $next_matches[$i]->tid1);
                    array_push($team_list, $next_matches[$i]->tid2);
                    $seq = $next_matches[$i]->seq ? $next_matches[$i]->seq : 0;
                    unset($next_matches[$i]->seq);

                    array_push($sub_league_array[$seq]['next_matches_league'], $next_matches[$i]);
                }
            }

            $team_list_unique = array_unique($team_list);
            $sql_team = "
                select team.tid,team.tn as teamName,team.tnPk as teamNamePk,
                if(lang_team.teamNameEn is null,team.tn,lang_team.teamNameEn) as teamNameEn,
                if(lang_team.teamNameBig is null,team.tn,lang_team.teamNameBig) as teamNameBig,
                if(lang_team.teamNameGb is null,team.tn,lang_team.teamNameGb) as teamNameGb,
                if(lang_team.teamNameKr is null,team.tn,lang_team.teamNameKr) as teamNameKr,
                if(lang_team.teamNameTh is null,team.tn,lang_team.teamNameTh) as teamNameTh,
                if(lang_team.teamNameLa is null,team.tn,lang_team.teamNameLa) as teamNameLa,
                if(lang_team.teamNameVn is null,team.tn,lang_team.teamNameVn) as teamNameVn
                from team
                left join lang_team on lang_team.tid = team.tid
                where  team.tid=" . implode(" or  team.tid=", $team_list_unique) . "
                ";

            $stmt_team = $db->query($sql_team);
            $teamList = $stmt_team->fetchAll(PDO::FETCH_OBJ);
            $lang_team = array();
            foreach ($teamList as $t) {
                $lang_team[$t->tid] = array(
                    'en' => $t->teamNameEn,
                    'big' => $t->teamNameBig,
                    'gb' => $t->teamNameGb,
                    'kr' => $t->teamNameKr,
                    'th' => $t->teamNameTh,
                    'la' => $t->teamNameLa,
                    'vn' => $t->teamNameVn,
                );
            }

            $obj = array(
                'lang_team' => $lang_team,
                'lang_league' => $lang_league,
                'data' => $sub_league_array,
            );
            $json = json_encode($obj);
            file_put_contents('sub_league_data/' . $leagueId . '.json', $json);
            //chmod('sub_league_data/' . $leagueId . '.json', 0777);
        }
        //querylog(__FUNCTION__, $db);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function saveFacebookInfo()
{
    $ap = new Slim();
    $fb_email = $ap->request()->params('fb_email');
    $fb_firstname = $ap->request()->params('fb_firstname');
    $fb_lastname = $ap->request()->params('fb_lastname');
    $fb_middle_name = $ap->request()->params('fb_middle_name');
    $fb_languague = $ap->request()->params('fb_languague');
    $fb_name = $ap->request()->params('fb_name');
    $fb_uid = $ap->request()->params('fb_uid');
    $fb_username = $ap->request()->params('fb_username');
    $fb_timezone = $ap->request()->params('fb_timezone');
    $fb_birthday_date = $ap->request()->params('fb_birthday_date');
    $fb_sex = $ap->request()->params('fb_sex');
    $fb_pic = $ap->request()->params('fb_pic');
    $fb_profile_url = $ap->request()->params('fb_profile_url');
    $fb_access_token = $ap->request()->params('fb_access_token');
    $fb_location = $ap->request()->params('location');
    $country = $ap->request()->params('country');
    $country = $country ? $country : 0;
    if (empty($fb_location)) {
        $fb_location = "unknow";
    }
    $hastoken = "";
    if (!empty($fb_access_token)) {
        $hastoken = "fb_access_token = \"$fb_access_token\",";
    }
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select *from facebook_user where fb_uid='$fb_uid'";
    $stmt = $db->query($sql);
    if ($stmt->fetch(PDO::FETCH_OBJ)) {
        $sql = "update facebook_user set
        fb_email=\"$fb_email\",
        fb_firstname = \"$fb_firstname\",
        fb_middle_name = \"$fb_middle_name\",
        fb_lastname = \"$fb_lastname\",
        fb_languague=\"$fb_languague\",
        fb_name = \"$fb_name\",
        fb_username = \"$fb_username\",
        fb_timezone = \"$fb_timezone\",
        fb_birthday_date = \"$fb_birthday_date\",
        fb_sex = \"$fb_sex\",
        fb_pic  = \"$fb_pic\",
        fb_profile_url = \"$fb_profile_url\",
        {$hastoken}
        fb_location = \"$fb_location\",
        country_id = $country
         where fb_uid='$fb_uid'

    ";
        $db->exec($sql);
    } else {
        $sql = "insert into facebook_user (fb_uid,fb_email,fb_firstname,fb_middle_name,fb_lastname,fb_languague,fb_name,fb_username,
        fb_timezone,fb_birthday_date,fb_sex,fb_pic,fb_profile_url,fb_access_token,gp,pts,w,d,l,reg_at,user_status,display_name) values(
        \"$fb_uid\",\"$fb_email\",\"$fb_firstname\",\"$fb_middle_name\",\"$fb_lastname\",\"$fb_languague\",\"$fb_name\",
        \"$fb_username\",\"$fb_timezone\",\"$fb_birthday_date\",\"$fb_sex\",\"$fb_pic\",\"$fb_profile_url\",\"$fb_access_token\",1500,0,0,0,0,NOW(),\"$fb_firstname\",\"$fb_firstname\"
        )";
        $db->exec($sql);
        file_put_contents('bet_List/' . $fb_uid . '.json', json_encode(array()));
        //chmod('bet_List/' . $fb_uid . '.json', 0777);
    }
//    $sql = "select *from facebook_user where fb_uid='$fb_uid'";
//    $stmt = $db->query($sql);
//    $obj = $stmt->fetch(PDO::FETCH_OBJ);
//    foreach ($obj as $key => $value) {
//        if ($value == NULL) {
//            $obj->$key = "";
//        }
//    }
//
//    $tpsql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
//FROM `user_achieve` ua
//LEFT JOIN `trophy` t ON ua.trophy_id = t.id
//WHERE ua.fb_uid ='$fb_uid'
//GROUP BY ua.trophy_id
//ORDER BY ua.earn_at DESC
//";
//    $tpstmt = $db->query($tpsql);
//    $trophy = $tpstmt->fetchAll(5);
//    $obj->achieve = $trophy;
//    $obj->bestprize = getBestPrize($fb_uid, 5);
//    $obj->writeby = "savefacebookinfo";
//
//    file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//    chmod('facebook_info/' . $fb_uid . '.json', 0777);
//    $sql = "select * from facebook_user where fb_uid='$fb_uid'";
//    $stmt = $db->query($sql);
//    $obj = $stmt->fetch(PDO::FETCH_OBJ);
//    $tt = new TotalStatement();
//    $newlogin = $tt->continueLoginBonus($obj->uid);
//    if ($newlogin) {
//        $tt->todayBonus($obj->uid);
//    }
    //querylog(__FUNCTION__, $db);
    FBtofile($fb_uid);
    echo 1;
}

function FBtofile($fb_uid)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select * from facebook_user where fb_uid='$fb_uid'";
    $stmt = $db->query($sql);
    $obj = $stmt->fetch(PDO::FETCH_OBJ);

    foreach ($obj as $key => $value) {
        if ($value == NULL) {
            $obj->$key = "";
        }
    }

    $tpsql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
GROUP BY ua.trophy_id
ORDER BY ua.earn_at DESC
";
    $tpstmt = $db->query($tpsql);
    $trophy = $tpstmt->fetchAll(5);
    $obj->achieve = $trophy;

    $tpsql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
FROM `achieve` ua
LEFT JOIN `trophy2` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
GROUP BY ua.trophy_id
ORDER BY ua.earn_at DESC
";
    $tpstmt = $db->query($tpsql);
    $trophy = $tpstmt->fetchAll(5);
    $obj->achieve2 = $trophy;

    $obj->bestprize = getBestPrize($db, $fb_uid, 5);
    $obj->bestprize2 = getBestPrize2($db, $fb_uid, 5);
    $obj->writeby = "FBtofile";
    if (($obj->pts - $obj->d) > 0) {
        $obj->accurate = number_format(floatval($obj->w / ($obj->w + $obj->l) * 100), 2);
    } else {
        $obj->accurate = number_format(0, 2);
    }
    $obj->diamond = number_format($obj->diamond, 2);

//    $sssql = "SELECT *
//FROM sound_setting ss
//WHERE fb_uid ='$fb_uid'";
//    $ssstmt = $db->query($sssql);
//    $sound = $ssstmt->fetch(5);
//    $obj->sound = $sound;
    file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
    //chmod('facebook_info/' . $fb_uid . '.json', 0777);
    //querylog(__FUNCTION__, $db);
}

function updateUserfile($fb_uid)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select * from facebook_user where fb_uid='$fb_uid'";
    $stmt = $db->query($sql);
    $obj = $stmt->fetch(PDO::FETCH_OBJ);
    foreach ($obj as $key => $value) {
        if ($value == NULL) {
            $obj->$key = "";
        }
    }

    $tpsql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
GROUP BY ua.trophy_id
ORDER BY ua.earn_at DESC
";
    $tpstmt = $db->query($tpsql);
    $trophy = $tpstmt->fetchAll(5);
    $obj->achieve = $trophy;
    $obj->bestprize = getBestPrize($db, $fb_uid, 5);
    $obj->writeby = "FBtofile";
    $obj->accurate = number_format(0, 2);
    if (($obj->pts - $obj->d) > 0) {
        $obj->accurate = number_format(floatval($obj->w / ($obj->pts - $obj->d) * 100), 2);
    }

//    $sssql = "SELECT *
//FROM sound_setting ss
//WHERE fb_uid ='$fb_uid'";
//    $ssstmt = $db->query($sssql);
//    $sound = $ssstmt->fetch(5);
//    $obj->sound = $sound;
    file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
    //@chmod('facebook_info/' . $fb_uid . '.json', 0777);
    //querylog(__FUNCTION__, $db);
    echo json_encode($obj);
}

function soundSetting()
{
    $fb_uid = $_REQUEST["fb_uid"];
    $sound1 = isset($_REQUEST["sound1"]) ? $_REQUEST['sound1'] : "N";
    $sound2 = isset($_REQUEST["sound2"]) ? $_REQUEST['sound2'] : "N";
    $sound3 = isset($_REQUEST["sound3"]) ? $_REQUEST['sound3'] : "N";
    $sound4 = isset($_REQUEST["sound4"]) ? $_REQUEST['sound4'] : "N";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "INSERT INTO sound_setting (fb_uid,sound1,sound2,sound3,sound4) VALUES ('$fb_uid','$sound1','$sound2','$sound3','$sound4') ON DUPLICATE KEY UPDATE sound1='$sound1',sound2='$sound2',sound3='$sound3',sound4='$sound4'";
    $db->exec($sql);
    FBtofile($fb_uid);
    echo file_get_contents('facebook_info/' . $fb_uid . '.json');
}

function device_info()
{
    $ap = new Slim();
    $device_id = $ap->request()->params('device_id');
    $app = $ap->request()->params('app');
    $platform = $ap->request()->params('platform');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "select *from request_notification_count where device_id='$device_id' and app='$app' and platform='$platform'";
    $stmt = $db->query($sql);
    $info = $stmt->fetch(PDO::FETCH_OBJ);
    $json = json_encode($info);
    //querylog(__FUNCTION__, $db);
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function videoHighlight()
{
    $ap = new Slim();
    $mid = $ap->request()->params('mid');
    $tid = $ap->request()->params('tid');
    $leagueId = $ap->request()->params('leagueId');
    $competitionId = $ap->request()->params('competitionId');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if ($mid) {
        $filename = "video_highlight/$mid.json";
        //echo $filename;
        $json = null;
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
        } else {

            $sql = "select *from video_highlight where mid=$mid";
            $stmt = $db->query($sql);
            $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
            $json = json_encode($obj);
        }
    } else if ($tid) {
        $filename = "video_highlight/team/$tid.json";
        //echo $filename;
        $json = null;
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
        } else {

            $sql = "select *from video_highlight where gid =$tid or hid = $tid order by video_id DESC limit 50";
            $stmt = $db->query($sql);
            $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
            $json = json_encode($obj);
        }
    } else if ($leagueId) {
        $filename = "video_highlight/league/$leagueId.json";
        $json = null;
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
        } else {

            $sql = "select *from video_highlight where leagueId=$leagueId order by video_id DESC limit 50";
            $stmt = $db->query($sql);
            $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
            $json = json_encode($obj);
        }
    } else if ($competitionId) {
        $filename = "video_highlight/competition/$competitionId.json";
        $json = null;
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
        } else {
            $sql = "select *from video_highlight where competitionId=$competitionId order by video_id DESC limit 50";
            $stmt = $db->query($sql);
            $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
            $json = json_encode($obj);
        }
    }
    //querylog(__FUNCTION__, $db);
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function resetNotification()
{
    $ap = new Slim();
    $device_id = $ap->request()->params("device_id");
    $app = $ap->request()->params("app", 'ballscore');
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "update request_notification_count set qty=0 where device_id='$device_id' and app='$app'";
    echo $db->exec($sql);
}

function getLeagueByCidOnly($id)
{
    $filename = "leagueByCid/$id.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {

        $obj = array(
            'list' => array(),
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLeagueByCidOnlys()
{
    $ap = new Slim();
    $cid = $ap->request()->params('cid');
    $lang = $ap->request()->params('lang');
    $ln = $lang ? "$cid/" : "";
    $filename = "leagueByCid/$ln.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {

        $obj = array(
            'list' => array(),
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function leagueMainOnly($id)
{
    $filename = "gen_file_league/$id.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {

        try {

            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $sql = "select league.*,lang_league.leagueNameTh as leagueNameTh2 from league
                              left join lang_league on lang_league.leagueName = league.leagueName
                              where league.leagueId=$id";
            $stmt = $db->query($sql);
            $info = $stmt->fetch(PDO::FETCH_OBJ);
            $sql = "SELECT  home.tid as hid ,away.tid as aid, `date`,  `teamHomeNamePk`,  `teamAwayNamePk`,  `competitionType`,  `competitionNamePk`,  `leagueSeason`,  `subLeagueNamePk`,  `leagueNamePk`,  `hn`,  `an`,  `hcomPk`,  `acomPk`,  `r`,`score`,result_league.subLeagueName,result_league.leagueName,t1.teamNameTh as teamNameTh1,t2.teamNameTh as teamNameTh2
                    FROM result_league
                    left join team as home on (result_league.teamHomeNamePk  = home.tnPk)
                    left join team as away on (result_league.teamAwayNamePk  = away.tnPk)
                    left join lang_team as t1 on (t1.teamName = home.tnPk)
                    left join lang_team as t2 on (t2.teamName = away.tnPk)
                    where result_league.leagueId=$id
                    order by result_league.date DESC limit 20";
            //echo $sql;
            $stmt = $db->query($sql);
            $resultList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
                            left join team on (team.tnPk=stat_table.tnPk)
                            left join lang_team t on t.teamName = team.tnPk
                            where stat_table.leagueId=$id
                           group by stat_table.tnPk order by stat_table.no ASC";
            $stmt = $db->query($sql);
            $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
            $sql = "select home.tid as hid,away.tid as aid,fixture_league.*,t1.teamNameTh as teamNameTh1,t2.teamNameTh as teamNameTh2
                   from  fixture_league
                    left join team as home on (home.tnPk=fixture_league.teamHomeNamePk)
                    left join team as away on (away.tnPk=fixture_league.teamAwayNamePk)
                    left join lang_team as t1 on (t1.teamName = home.tnPk)
                    left join lang_team as t2 on (t2.teamName = away.tnPk)
                    where fixture_league.leagueId=$id order by datetime ASC  limit 20";
            $stmt = $db->query($sql);
            $fixtureList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $tidlist = "0";
            foreach ($resultList as $rlist) {
                $tidlist .= ',' . $rlist->hid . ',' . $rlist->aid;
            }
            foreach ($stat_table as $stable) {
                $tidlist .= ',' . $stable->tid;
            }
            foreach ($fixtureList as $flist) {
                $tidlist .= ',' . $flist->hid . ',' . $flist->aid;
            }
            // echo $tidlist;
            $logossql = "SELECT * FROM team_logos WHERE tid IN (" . $tidlist . ") GROUP BY tid";
            $logostmt = $db->query($logossql);
            $tlogos = $logostmt->fetchAll(5);
            $logos = array();
            foreach ($tlogos as $logo) {
                $logos[$logo->tid] = $logo;
            }
            $c3 = time();
            $obj = array(
                'c3' => $c3,
                'info' => array($info),
                'resultList' => $resultList,
                'stat_table' => $stat_table,
                'fixtureList' => $fixtureList,
                'logos' => $logos,
                'source' => 'DB'
            );
            //querylog(__FUNCTION__, $db);
            //$obj =null;
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function leagueMainOnlys()
{
    $ap = new Slim();
    $id = $ap->request()->params("leagueId");
    $lang = $ap->request()->params("lang");
    $ln = $lang == 'en' ? '' : $lang . '/';
    $filename = "gen_file_league/" . $ln . $id . ".json";

    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {

        try {
            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $sql = "select league.*,lang_league.leagueNameTh as leagueNameTh2 from league
                              left join lang_league on lang_league.leagueName = league.leagueName
                              where league.leagueId=$id";
            $stmt = $db->query($sql);
            $info = $stmt->fetch(PDO::FETCH_OBJ);
            $sql = "SELECT  home.tid as hid ,away.tid as aid, `date`,  `teamHomeNamePk`,  `teamAwayNamePk`,  `competitionType`,  `competitionNamePk`,  `leagueSeason`,  `subLeagueNamePk`,  `leagueNamePk`,  `hn`,  `an`,  `hcomPk`,  `acomPk`,  `r`,`score`,result_league.subLeagueName,result_league.leagueName,t1.teamNameTh as teamNameTh1,t2.teamNameTh as teamNameTh2
                    FROM result_league
                    left join team as home on (result_league.teamHomeNamePk  = home.tnPk)
                    left join team as away on (result_league.teamAwayNamePk  = away.tnPk)
                    left join lang_team as t1 on (t1.teamName = home.tnPk)
                    left join lang_team as t2 on (t2.teamName = away.tnPk)
                    where result_league.leagueId=$id
                    order by result_league.date DESC limit 20";
            //echo $sql;
            $stmt = $db->query($sql);
            $resultList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
                            left join team on (team.tnPk=stat_table.tnPk)
                            left join lang_team t on t.teamName = team.tnPk
                            where stat_table.leagueId=$id
                           group by stat_table.tnPk order by stat_table.no ASC";
            $stmt = $db->query($sql);
            $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
            $sql = "select home.tid as hid,away.tid as aid,fixture_league.*,t1.teamNameTh as teamNameTh1,t2.teamNameTh as teamNameTh2
                   from  fixture_league
                    left join team as home on (home.tnPk=fixture_league.teamHomeNamePk)
                    left join team as away on (away.tnPk=fixture_league.teamAwayNamePk)
                    left join lang_team as t1 on (t1.teamName = home.tnPk)
                    left join lang_team as t2 on (t2.teamName = away.tnPk)
                    where fixture_league.leagueId=$id order by datetime ASC  limit 20";
            $stmt = $db->query($sql);
            $fixtureList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $c3 = time();
            $obj = array(
                'c3' => $c3,
                'info' => $info,
                'resultList' => $resultList,
                'stat_table' => $stat_table,
                'fixtureList' => $fixtureList,
            );
            //querylog(__FUNCTION__, $db);
            //$obj =null;
            $db = null;
        } catch (PDOException $e) {
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLeagueByCid($id)
{
    $sql = "select league.*, lang_league.leagueNameTh from league
            left join stat_table on stat_table.leagueId = league.leagueId
            left join lang_league on lang_league.leagueName = league.leagueName
            where league.competitionId = $id and stat_table.leagueId is not null
            group by league.leagueId";
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);

        $obj = array(
            'list' => $list,
        );
        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getResult($id = null)
{
    try {
        if ($id) {
            $date = "'$id'";
        } else {
            $date = 'current_date';
        }
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select live_match.mid, live_match_event.h, live_match_event.g
            FROM live_match
            left join live_match_event on (live_match_event.mid = live_match.mid)
            where showDate = $date and live_match_event.m = 'FT'
            group by mid
            order by date ASC";
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'result' => $result,
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getAllTeam()
{
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select tid, tnPk, tn from team where tid>0 order by tid ASC";
        $stmt = $db->query($sql);
        $teamList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'list' => $teamList,
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function leagueStat($id)
{
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select *from league where leagueId = " . $id;

        $stmt = $db->query($sql);
        $league = $stmt->fetch(PDO::FETCH_OBJ);
        $stat = array();
        if ($league) {
            $sql = "select *from league_stat where subLeagueId = $id and leagueSeason = '" . $league->leagueSeason . "'";
            $stmt = $db->query($sql);
            $league_stat = $stmt->fetch(PDO::FETCH_OBJ);

            if ($league_stat) {
                $stat['HT_FT'] = json_decode($league_stat->HT_FT);
                $stat['ou2_5'] = json_decode($league_stat->ou2_5);
                $stat['hwdaw'] = json_decode($league_stat->hwdaw);
                $stat['goals_scored'] = json_decode($league_stat->goals_scored);
                $stat['correct_scoresd'] = json_decode($league_stat->correct_scores);
                $stat['top_5_offensive_teams'] = json_decode($league_stat->top_5_offensive_teams);
                $stat['top_5_defensive_teams'] = json_decode($league_stat->top_5_defensive_teams);
            }
        }
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'stat' => $stat,
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function leagueFixture()
{
    try {

        $ap = new Slim();
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $id = $ap->request()->params("leagueId");
        $limit = $ap->request()->params("limit");
        $page = $ap->request()->params("page");
        $ofset = 0;
        if (!$limit) {
            $limit = 50;
        }
        if (!$page) {
            $page = 1;
        } else {
            $ofset = ($page - 1) * $limit;
        }
        $sql = "select count(*) as count from fixture_league where leagueId = $id";
        $stmt = $db->query($sql);
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $num = ceil($result->count / $limit);
        $currentPage = (int)$page;
        if ($page > $num) {
            $currentPage = 1;
            $ofset = 0;
        } else if ($page == 1) {
            $currentPage = 1;
            $ofset = ($currentPage - 1) * $limit;
        }
        if ($currentPage != $num && $currentPage != 1) {
            $previousPage = $currentPage - 1;
            $nextPage = $currentPage + 1;
        } else if ($currentPage == $num && $currentPage == 1) {
            $previousPage = $currentPage;
            $nextPage = $currentPage;
        } else if ($currentPage == $num) {
            $previousPage = $currentPage - 1;
            $nextPage = 1;
        } else if ($currentPage == 1) {
            $previousPage = $num;
            $nextPage = $currentPage + 1;
        }

        $sql = "select *from fixture_league where leagueId = $id order by datetime ASC limit $ofset, $limit";
        $stmt = $db->query($sql);
        $fixtureList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'fixtureList' => $fixtureList,
            'numPage' => $num,
            'nextPage' => $nextPage,
            'previousPage' => $previousPage,
            'currentPage' => $currentPage,
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function leagueResult()
{
    try {

        $ap = new Slim();
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $id = $ap->request()->params("leagueId");
        $limit = $ap->request()->params("limit");
        $page = $ap->request()->params("page");
        $ofset = 0;
        if (!$limit) {
            $limit = 50;
        }
        if (!$page) {
            $page = 1;
        } else {
            $ofset = ($page - 1) * $limit;
        }
        $sql = "select *from league where leagueId = " . $id;

//        $stmt = $db->query($sql);
//        $league = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select count(*) as count from result_league where leagueId = $id";
        $stmt = $db->query($sql);
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        $num = ceil($result->count / $limit);
        $currentPage = (int)$page;
        if ($page > $num) {
            $currentPage = 1;
            $ofset = 0;
        } else if ($page == 1) {
            $currentPage = 1;
            $ofset = ($currentPage - 1) * $limit;
        }
        if ($currentPage != $num && $currentPage != 1) {
            $previousPage = $currentPage - 1;
            $nextPage = $currentPage + 1;
        } else if ($currentPage == $num && $currentPage == 1) {
            $previousPage = $currentPage;
            $nextPage = $currentPage;
        } else if ($currentPage == $num) {
            $previousPage = $currentPage - 1;
            $nextPage = 1;
        } else if ($currentPage == 1) {
            $previousPage = $num;
            $nextPage = $currentPage + 1;
        }
        $sql = "SELECT `date`, `teamHomeNamePk`, `teamAwayNamePk`, `competitionType`, `competitionNamePk`, `leagueSeason`, `subLeagueNamePk`, `leagueNamePk`, `hn`, `an`, `hcomPk`, `acomPk`, `r`, `score`, subLeagueName, leagueName FROM result_league
            where leagueId = $id
            order by date DESC limit $ofset, $limit";
        //echo $sql;
        $stmt = $db->query($sql);
        $resultList = $stmt->fetchAll(PDO::FETCH_OBJ);


        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'resultList' => $resultList,
            'numPage' => $num,
            'nextPage' => $nextPage,
            'previousPage' => $previousPage,
            'currentPage' => $currentPage,
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function leagueMain($id)
{
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT home.tid as hid, away.tid as aid, `date`, `teamHomeNamePk`, `teamAwayNamePk`, `competitionType`, `competitionNamePk`, `leagueSeason`, `subLeagueNamePk`, `leagueNamePk`, `hn`, `an`, `hcomPk`, `acomPk`, `r`, `score`, subLeagueName, leagueName FROM result_league
            left join team as home on (result_league.teamHomeNamePk = home.tnPk)
            left join team as away on (result_league.teamAwayNamePk = away.tnPk)
            where result_league.leagueId = $id
            order by result_league.date DESC limit 12";
        //echo $sql;
        $stmt = $db->query($sql);
        $resultList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select stat_table.*, team.tid from stat_table left join team on (stat_table.tnPk = team.tnPk) where stat_table.subLeagueId = $id group by stat_table.tnPk order by stat_table.no ASC";
        $stmt = $db->query($sql);
        $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select home.tid as hid, away.tid as aid, fixture_league.* from fixture_league
            left join team as home on (home.tnPk = fixture_league.teamHomeNamePk)
            left join team as away on (away.tnPk = fixture_league.teamAwayNamePk)
            where fixture_league.leagueId = $id order by datetime DESC limit 12";
        $stmt = $db->query($sql);
        $fixtureList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'resultList' => $resultList,
            'stat_table' => $stat_table,
            'fixtureList' => array_reverse($fixtureList),
        );

        //querylog(__FUNCTION__, $db);
        $db = null;
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getLastEvent($limit = 10)
{
    $filename = 'cronjob_gen_file/files/liveMatchLastEvent.json';
    $json = null;
    $c3 = time();
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {

        $obj = array(
            'live' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
//    try {
//
//        $db = DBConfig::getConnection();
//        $sql = "select *from live_match_last_event order by id desc limit 1";
//        $stmt = $db->query($sql);
//        $last = $stmt->fetch(PDO::FETCH_OBJ);
//        $c3 = time();
//        $obj = array(
//            'c3' => $c3,
//            'list' => array(),
//        );
//        //var_dump($last);
//        if ($last) {
//            //echo 5;
//            $sql = "select *from live_match_last_event where datetime = '" . $last->datetime . "' order by id desc limit " . $limit;
//            $stmt = $db->query($sql);
//            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//            $obj['list'] = $list;
//        }
//        $db = null;
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function getOdd($date = null)
{
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        if ($date) {

        } else {
            $date = date('Y-m-d');
            $time = date('H:i:s');
            $startDate = time();
            //echo $time;

            if ($time > "11:00:00" && $time <= "23:59:59") {

            } else {
                $date = date('Y-m-d', strtotime('-1 day', $startDate));
            }
        }
        $sql = "select *from localodd where date = '" . $date . "' order by start ASC";
        $stmt = $db->query($sql);
        $oddList = $stmt->fetchAll(PDO::FETCH_OBJ);
        //querylog(__FUNCTION__, $db);
        $db = null;
        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'oddList' => $oddList,
        );
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getEarlyDateList()
{
    try {
        $dateList = array();

        $date = date('Y-m-d');
        $time = date('H:i:s');
        $startDate = time();
        if ($time > "11:00:00" && $time <= "23:59:59") {
            $inc = 0;
        } else {
            $date = date('Y-m-d', strtotime('-1 day', $startDate));
            $inc = 1;
        }
        for ($i = 1; $i <= 5; $i++) {
            $newDate = date('Y-m-d', strtotime('+' . ($i - $inc) . ' day', $startDate));
            array_push($dateList, $newDate);
        }


        $c3 = time();
        $obj = array(
            'c3' => $c3,
            'dateList' => $dateList,
        );
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getCompetitionTable($id)
{

    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");

        $sql = "select league.leagueId, league.leagueNamePk, league.leagueName, league.competitionId, league.leagueSeason from league left join stat_table on (league.leagueId = stat_table.subLeagueId)
            where stat_table.cid = $id
            group by league.leagueId
            order by league.leagueSeason DESC, league.leagueId ASC";
        $stmt = $db->query($sql);
        $leagueList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $c3 = time();
        $obj = array(
            'competitionTable' => $leagueList,
            'c3' => $c3,
        );

        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function currentTime()
{

    //$datetime = new DateTime("America/Denver");
    $datetime = new DateTime('now');
    //$datetime->setTimezone('America/Denver');
    echo $datetime->getTimestamp();
}

function team()
{

    try {
        $ap = new Slim();
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $tid = $ap->request()->params("tid");
        if ($tid) {
            $sql = "select *from team where tid = $tid";
            $stmt = $db->query($sql);
            $team = $stmt->fetch(PDO::FETCH_OBJ);
            if ($team) {
                $tnPk = $team->tnPk;
            } else {
                echo "";
                exit;
            }
        } else {
            $tnPk = $ap->request()->params("tnPk");
        }

        $sql = "select * from team where tnPk = \"$tnPk\"";
        $stmt = $db->query($sql);
        $team = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from stat_table where tnPk=\"$tnPk\" ";
        $stmt = $db->query($sql);
        $stat_list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stat_table = array();
        foreach ($stat_list as $stat) {
            $sql = "select *from stat_table where leagueId=$stat->leagueId  and subLeagueId " . ($stat->subLeagueId > 0 ? "=$stat->subLeagueId" : "is null") . "  group by tnPk order by stat_table.no ASC";
            $stmt = $db->query($sql);
            $st = $stmt->fetchAll(PDO::FETCH_OBJ);
            $sql = "select *from league where leagueId=$stat->leagueId";
            $stmt = $db->query($sql);
            $league = $stmt->fetch(PDO::FETCH_OBJ);
            for ($i = 0; $i < count($st); $i++) {
                $st[$i]->ml = json_decode($st[$i]->ml);
            }
            array_push($stat_table, array(
                'league' => $league,
                'list' => $st,
            ));
        }


        $sql = "select league.leagueId ,`date`,  `teamHomeNamePk`,  `teamAwayNamePk`,  competitions.`competitionType`,  competitions.`competitionNamePk`,  league.`leagueSeason`,  `subLeagueNamePk`,  result.`leagueNamePk`,  `hn`,  `an`,  `hcomPk`,  `acomPk`,  `r`, `score`, result.leagueName,teamNamePk,lnk
from result
left join competitions on competitions.competitionNamePk = result.competitionNamePk
left join league on league.leagueNamePk = result.leagueNamePk and league.competitionId = competitions.competitionId
where teamHomeNamePk=\"$team->tnPk\"  or teamAwayNamePk=\"$team->tnPk\"order by date DESC limit 12";
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select *from fixture where teamNamePk=\"$tnPk\"  order by date ASC limit 12";
        $stmt = $db->query($sql);
        $fixture = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select *from stat_ou where tnPk=\"$tnPk\" ";
        $stmt = $db->query($sql);
        $stat_ou = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from stat_chean_sheet where tnPk=\"$tnPk\" ";
        $stmt = $db->query($sql);
        $stat_chean_sheet = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from stat_gold_after65 where tnPk=\"$tnPk\" ";
        $stmt = $db->query($sql);
        $stat_gold_after65 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from stat_gold_in_minutes where tnPk=\"$tnPk\" ";
        $stmt = $db->query($sql);
        $stat_gold_in_minutes = $stmt->fetch(PDO::FETCH_OBJ);
        if ($stat_gold_in_minutes) {
            $stat_gold_in_minutes->g1 = $stat_gold_in_minutes->g1 == "null" ? null : json_decode($stat_gold_in_minutes->g1);
            $stat_gold_in_minutes->g2 = $stat_gold_in_minutes->g2 == "null" ? null : json_decode($stat_gold_in_minutes->g2);
            $stat_gold_in_minutes->g3 = $stat_gold_in_minutes->g3 == "null" ? null : json_decode($stat_gold_in_minutes->g3);
            $stat_gold_in_minutes->g4 = $stat_gold_in_minutes->g4 == "null" ? null : json_decode($stat_gold_in_minutes->g4);
            $stat_gold_in_minutes->g5 = $stat_gold_in_minutes->g5 == "null" ? null : json_decode($stat_gold_in_minutes->g5);
            $stat_gold_in_minutes->g6 = $stat_gold_in_minutes->g6 == "null" ? null : json_decode($stat_gold_in_minutes->g6);
        }
        $sql = "select *from team_stat where tid=" . $team->tid;
        $stmt = $db->query($sql);
        $team_stat = $stmt->fetch(PDO::FETCH_OBJ);
        if ($team_stat) {
            $team_stat->HT_FT = json_decode($team_stat->HT_FT);
            $team_stat->series = json_decode($team_stat->series);
            $team_stat->correct_scores = json_decode($team_stat->correct_scores);
            $team_stat->additional_stat = json_decode($team_stat->additional_stat);
        }
        $obj = array(
            'team' => $team,
            'result' => $result,
            'fixture' => $fixture,
            'stat_table' => $stat_table,
            'stat_ou' => $stat_ou,
            'stat_chean_sheet' => $stat_chean_sheet,
            'stat_after_gold' => $stat_gold_after65,
            'stat_gold_in_minutes' => $stat_gold_in_minutes,
            'team_stat' => $team_stat,
        );
        //echo '{"live_match_event": ' . json_encode($list) . ':"c3":"'.$c3.'"}';
        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function compareOnly()
{
    $ap = new Slim();
    $hid = $ap->request()->params("hid");
    $gid = $ap->request()->params("gid");
    $lang = $ap->request()->params('lang');
    $ln = $lang ? $lang . '/' : 'en/';
    $filename = 'compare_team/' . $ln . $hid . '_' . $gid . '.json';
    $filename2 = 'compare_team/' . $hid . '_' . $gid . '.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else if (file_exists($filename2)) {
        $json = file_get_contents($filename2);
    } else {

        $obj = array(
            'result_vs' => array(),
            'result1' => array(),
            'result2' => array(),
            'team1' => array(),
            'team2' => array(),
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function compareOnlys()
{
    $ap = new Slim();
    $hid = $ap->request()->params("hid");
    $gid = $ap->request()->params("gid");
    $lang = $ap->request()->params('lang');
    $ln = $lang ? $lang . '/' : 'en/';
    $filename = 'compare_team/' . $ln . $hid . '_' . $gid . '.json';
    $filename2 = 'compare_team/' . $hid . '_' . $gid . '.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else if (file_exists($filename2)) {
        $json = file_get_contents($filename2);
    } else {

        $obj = array(
            'result_vs' => array(),
            'result1' => array(),
            'result2' => array(),
            'team1' => array(),
            'team2' => array(),
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function compare()
{
    try {

//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $ap = new Slim();
        $hid = $ap->request()->params("hid");
        $gid = $ap->request()->params("gid");
        if ($hid && $gid) {
            $sql = "select *from team where tid=$hid";
            $stmt = $db->query($sql);
            $home = $stmt->fetch(PDO::FETCH_OBJ);
            $sql = "select *from team where tid=$gid";
            $stmt = $db->query($sql);
            $away = $stmt->fetch(PDO::FETCH_OBJ);
            if ($home && $away) {
                $tnPk1 = $home->tnPk;
                $tnPk2 = $away->tnPk;
            } else {
                echo "";
                exit;
            }
        } else {
            $tnPk1 = $ap->request()->params("tnPk1");
            $tnPk2 = $ap->request()->params("tnPk2");
            $sql = "select *from team where tnPk=\"$tnPk1\"";
            $stmt = $db->query($sql);
            $home = $stmt->fetch(PDO::FETCH_OBJ);
            $sql = "select *from team where tnPk=\"$tnPk2\"";
            $stmt = $db->query($sql);
            $away = $stmt->fetch(PDO::FETCH_OBJ);
        }
        $sql = "select *from form_table where tnPk=\"$tnPk1\"";
        $stmt = $db->query($sql);
        $stat_form1 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from form_table where tnPk=\"$tnPk2\"";
        $stmt = $db->query($sql);
        $stat_form2 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from head2head where (tnPk1=\"$tnPk1\" and tnPk2=\"$tnPk2\")";
        $stmt = $db->query($sql);
        $h2h1 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from head2head where (tnPk1=\"$tnPk2\" and tnPk2=\"$tnPk1\")";
        $stmt = $db->query($sql);
        $h2h2 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from result where teamHomeNamePk=\"$tnPk1\"  or  teamAwayNamePk=\"$tnPk1\" group by  date order by date DESC limit 12";
        $stmt = $db->query($sql);
        $result1 = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select *from result where teamHomeNamePk=\"$tnPk2\" or teamAwayNamePk=\"$tnPk2\" group by date order by date DESC limit 12";
        $stmt = $db->query($sql);
        $result2 = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select *from stat_ou where tnPk=\"$tnPk1\"";
        $stmt = $db->query($sql);
        $stat_ou1 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from stat_ou where tnPk=\"$tnPk2\"";
        $stmt = $db->query($sql);
        $stat_ou2 = $stmt->fetch(PDO::FETCH_OBJ);
        $sql = "select *from result_vs where (tnPk1=\"$tnPk1\" and tnPk2=\"$tnPk2\") or (tnPk1=\"$tnPk2\" and tnPk2=\"$tnPk1\") order by date DESC";
        $stmt = $db->query($sql);
        $result_vs = $stmt->fetchAll(PDO::FETCH_OBJ);
        $obj = array(
            'team1' => $home,
            'team2' => $away,
            'result_vs' => $result_vs,
            'stat_form1' => $stat_form1,
            'stat_form2' => $stat_form2,
            'h2h1' => $h2h1,
            'h2h2' => $h2h2,
            'result1' => $result1,
            'result2' => $result2,
            'stat_ou1' => $stat_ou1,
            'stat_ou2' => $stat_ou2,
            'result_vs' => $result_vs,
        );

        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {
            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getLeague($id)
{
    $sql = "select * FROM league  where competitionId =$id";
    try {

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);

        $obj = array(
            'list' => $list,
        );
        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getCompetitionOnlys($lang = NULL)
{
    $ln = $lang ? "$lang/" : "";
    $filename = 'cronjob_gen_file/files/' . $ln . 'competitions.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array(
            'competition' => array(),
        ));
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getCompetitionOnlysWithSubLeague($lang = NULL)
{
    $ln = $lang ? "$lang/" : "";
    $filename = 'cronjob_gen_file/files/' . $ln . 'competitions_sub.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $json = json_encode(array(
            'competition' => array(),
        ));
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getCompetitions()
{
    $filename = 'cronjob_gen_file/files/competitions.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $sql = "select * FROM competitions order by competitionType DESC,competitionName ASC";
        try {

            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $obj = array(
                'competitions' => $list,
            );
            $json = json_encode($obj);
            //querylog(__FUNCTION__, $db);
        } catch (PDOException $e) {

        }
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
//    $sql = "select * FROM competitions order by competitionType DESC,competitionName ASC";
//    try {
//
//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//
//        $obj = array(
//            'competitions' => $list,
//        );
//
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function getCompetitionsOnly()
{
    $filename = 'cronjob_gen_file/files/competitions.json';
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $sql = "select * FROM competitions order by competitionType DESC,competitionName ASC";
        try {

            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $obj = array(
                'competitions' => $list,
            );
            $json = json_encode($obj);
            //querylog(__FUNCTION__, $db);
        } catch (PDOException $e) {

        }
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
//    $sql = "select * FROM competitions order by competitionType DESC,competitionName ASC";
//    try {
//
//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//
//        $obj = array(
//            'competitions' => $list,
//        );
//
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function getLiveMatchOnly($lang = null)
{
    $ln = $lang ? $lang . '/' : '';
    $filename = 'cronjob_gen_file/files/' . $ln . 'liveMatchOnly.json';
    $json = null;
    if (file_exists($filename)) {

        $c3 = time();
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {
        $c3 = time();
        $obj = array(
            'live_match' => array(),
            'live_league' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLiveMatchWaitOnly($lang = null)
{
    $ln = $lang ? $lang . '/' : '';
    $filename = 'cronjob_gen_file/files/' . $ln . 'liveMatchWaitOnly.json';
    $json = null;
    if (file_exists($filename)) {

        $c3 = time();
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {
        $c3 = time();
        $obj = array(
            'live_match' => array(),
            'live_league' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLiveMatchResultOnly($lang = null)
{
    $ln = $lang ? $lang . '/' : '';
    $filename = 'cronjob_gen_file/files/' . $ln . 'liveMatchResultOnly.json';
    $json = null;
    if (file_exists($filename)) {

        $c3 = time();
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {
        $c3 = time();
        $obj = array(
            'live_match' => array(),
            'live_league' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLiveMatchYesterdayResultOnly($lang = null)
{
    $ln = $lang ? $lang . '/' : '';
    $filename = 'cronjob_gen_file/files/' . $ln . 'liveMatchYesterdayResultOnly.json';
    $json = null;
    if (file_exists($filename)) {
        $c3 = time();
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {
        $c3 = time();
        $obj = array(
            'live_match' => array(),
            'live_league' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getLiveMatch()
{
    //$ln = $lang?$lang.'/':'';
    $filename = 'cronjob_gen_file/files/liveMatch.json';
    $json = null;
    if (file_exists($filename)) {

        $c3 = time();
        $json = file_get_contents($filename);
        $json = preg_replace('/"c3":\d+/', '"c3":' . $c3, $json);
    } else {
        $c3 = time();
        $obj = array(
            'live_match' => array(),
            'live_league' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
//    $json = file_get_contents('cronjob_gen_file/files/liveMatch.json');
//
//    if (isset($_GET['jsoncallback'])) {
//        echo $_GET['jsoncallback'] . '(' . $json . ')';
//    } else {
//        echo $json;
//    }
//    $sql = "select live_match.*,gt.teamNameTh as gnTh,ht.teamNameTh as hnTh FROM live_match
//        left join lang_team  as gt on gt.teamName = live_match.gn
//        left join lang_team  as ht on ht.teamName = live_match.hn
//        where showDate=current_date and new='Y' group by mid order by date ASC";
//    try {
//
//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//        $sql = "select live_league.*,lang_league.leagueNameTh,lang_competition.comNameTh from live_league
//            left join lang_league on lang_league.leagueName = live_league.ln
//            left join lang_competition on  lang_competition.comName = live_league.kn
//            where date=current_date and new='Y' order by competitionId ASC,leagueId ASC,subleagueId ASC";
//        $stmt = $db->query($sql);
//        $leagueList = $stmt->fetchAll(PDO::FETCH_OBJ);
//        $db = null;
//        $c3 = time();
//        $obj = array(
//            'live_match' => $list,
//            'live_league' => $leagueList,
//            'c3' => $c3,
//        );
//
//
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function v($lm)
{
    $str = '';
    if ($lm->sid == 2) {
        //echo $lm->cx;
        $ap = y($lm->cx);
        //echo $ap;
        $ao = (int)($c3 - $lm->c0 - $lm->c1) / 60;
        if ($ao < 1) {
            $str = 1 . "'";
        } else if ($ao > $ap) {
            $str = number_format($ap) . "+'";
        } else {
            $str = number_format($ao) . "'";
        }
        //echo $str;
    } else if ($lm->sid == 4) {
        $ap = y($lm->cx);
        $ao = (int)($c3 - $lm->c0 - $lm->c2) / 60 + $ap;
        if ($ao <= $ap) {
            $str = number_format($ap + 1) + "'";
        } else if ($ao > $ap) {
            $ap = $ap * 2;
            if ($ao > $ap) {
                $str = number_format($ap) . "+'";
            } else {
                $str = number_format($ao) . "'";
            }
        }
    }
    return $str;
}

function y($cx)
{
    if ($cx > 0) {
        return (int)$cx;
    } else {
        return 45;
    }
}

function getLiveMatch2()
{
    try {
        $now = date('Y-m-d H:i:s');
        echo date_default_timezone_get();
        echo $now;
        echo "[" . time() . "]";
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getLiveMatchEvent($mid)
{
    $filename = 'gen_file_events/' . $mid . '.json';
    $json = null;
    if (file_exists($filename)) {


        $json = file_get_contents($filename);
    } else {
        $c3 = time();
        $obj = array(
            'live_match_event' => array(),
            'c3' => $c3,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
//    $sql = "select * FROM live_match_event where mid=$mid";
//    try {
//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//        $db = null;
//        $c3 = time();
//        $obj = array(
//            'live_match_event' => $list,
//            'c3' => $c3,
//        );
//        //echo '{"live_match_event": ' . json_encode($list) . ':"c3":"'.$c3.'"}';
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function getLiveMatchUpdateOnly()
{
    $filename = 'cronjob_gen_file/files/liveMatchUpdate.json';
    $json = null;
    if (file_exists($filename)) {


        $json = file_get_contents($filename);
    } else {
        $c3 = time();
        $obj = array(
            'live_match_update' => array(),
            'c3' => $c3,
            'update_id' => 0,
        );
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }

//    $json = file_get_contents('cronjob_gen_file/files/liveMatchUpdate.json');
//    if (isset($_GET['jsoncallback'])) {
//        echo $_GET['jsoncallback'] . '(' . $json . ')';
//    } else {
//        echo $json;
//    }
//    $dateSql = "select date FROM live_match_update where date(date) = current_date order by date DESC limit 1 ";
//    try {
//        $db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
//        $stmt = $db->query($dateSql);
//        $date = $stmt->fetch();
//
//        $sql = "select * FROM live_match_update where date=('" . $date['date'] . "') order by zid ASC";
//        //echo $sql;
//        $stmt = $db->query($sql);
//        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
//        $db = null;
//        $c3 = time();
//        $obj = array(
//            'live_match_update' => $list,
//            'c3' => $c3,
//        );
//
//        if (isset($_GET['jsoncallback'])) {
//            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
//        } else {
//
//            echo json_encode($obj);
//        }
//    } catch (PDOException $e) {
//        echo '{"error":{"text":' . $e->getMessage() . '}}';
//    }
}

function getLiveMatchUpdate()
{

    $dateSql = "select date FROM live_match_update where date(date) = current_date order by date DESC limit 1 ";
    try {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($dateSql);
        $date = $stmt->fetch();

        $sql = "select * FROM live_match_update where date=('" . $date['date'] . "') order by zid ASC";
        //echo $sql;
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        $c3 = time();
        $obj = array(
            'live_match_update' => $list,
            'c3' => $c3,
        );
        //querylog(__FUNCTION__, $db);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . json_encode($obj) . ')';
        } else {

            echo json_encode($obj);
        }
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getStatTable($leagueId)
{
//    $request = Slim::getInstance()->request();
//    $tnPk =$request->params("tnPk");
//    $leagueId= $request->params("leagueId");
    try {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
            left join team on (team.tnPk=stat_table.tnPk)
            left join lang_team t on t.teamName = team.tn
            where stat_table.leagueId=$leagueId
            group by stat_table.tnPk order by stat_table.no ASC";
        $stmt = $db->query($sql);
        $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select league.*,lang_league.leagueNameTh from league
            left join lang_league on lang_league.leagueName = league.leagueName
            where league.leagueId=$leagueId";
        $stmt = $db->query($sql);
        $info = $stmt->fetch(PDO::FETCH_OBJ);
        $obj = array(
            'stat_table' => $stat_table,
            'info' => $info
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        echo json_encode($obj);
    } catch (PDOException $e) {
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function getStatTableOnly($leagueId)
{
    $filename = "gen_file_stat_table/$leagueId.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
            left join team on (team.tnPk=stat_table.tnPk)
            left join lang_team t on t.teamName = team.tn
            where stat_table.leagueId=$leagueId  and subLeagueNamePk=''
            group by stat_table.tnPk order by stat_table.no ASC";
        $stmt = $db->query($sql);
        $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select league.*,lang_league.leagueNameTh from league
            left join lang_league on lang_league.leagueName = league.leagueName
            where league.leagueId=$leagueId";
        $stmt = $db->query($sql);
        $info = $stmt->fetch(PDO::FETCH_OBJ);
        $obj = array(
            'stat_table' => $stat_table,
            'info' => $info
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getStatTableOnlys()
{

    $ap = new Slim();
    //$id = $ap->request()->params("leagueId");
    $leagueId = $ap->request()->params("leagueId");
    $lang = $ap->request()->params('lang');
    $ln = $lang == 'en' ? '' : $lang . '/';
    $filename = "gen_file_stat_table/$ln" . "$leagueId.json";
    $json = null;
    if (file_exists($filename)) {
        $json = file_get_contents($filename);
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
            left join team on (team.tnPk=stat_table.tnPk)
            left join lang_team t on t.teamName = team.tn
            where stat_table.leagueId=$leagueId and subLeagueNamePk=''
            group by stat_table.tnPk order by stat_table.no ASC";
        $stmt = $db->query($sql);
        $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
        $sql = "select league.*,lang_league.leagueNameTh from league
            left join lang_league on lang_league.leagueName = league.leagueName
            where league.leagueId=$leagueId";
        $stmt = $db->query($sql);
        $info = $stmt->fetch(PDO::FETCH_OBJ);
        $obj = array(
            'stat_table' => $stat_table,
            'info' => $info
        );
        //querylog(__FUNCTION__, $db);
        $db = null;
        $json = json_encode($obj);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function teamOnly($id)
{
    //echo $id;
    $filename = "team_data/$id.json";
    $json = null;
    $tidlist = '0';
    if (file_exists($filename)) {
        //echo 1;
        $json = file_get_contents($filename);
    } else {
        $team_sql = "select lang_team.teamNameTh as tnTh,team.*,lang_competition.comNameTh,lang_competition.comName  as comName from team "
            . " left join lang_team on lang_team.tid = team.tid"
            . " left join lang_competition on lang_competition.cid = team.cid"
            . " where team.tid='$id'";
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($team_sql);
        $team = $stmt->fetch(PDO::FETCH_OBJ);

        $result_sql = "select result.*,league.leagueId,t1.teamNameTh as teamNameTh1,t2.teamNameTh as  teamNameTh2,team1.tid as tid1,team2.tid as tid2 from result "
            . "left join competitions on competitions.competitionNamePk = result.competitionNamePk "
            . "left join league on league.leagueNamePk = result.leagueNamePk and league.competitionId = competitions.competitionId "
            . " left join team as team1 on team1.tnPk =result.teamHomeNamePk"
            . " left join team as team2 on team2.tnPk = result.teamAwayNamepk"
            . " left join lang_team as t1 on t1.tid =team1.tid "
            . " left join lang_team as t2 on t2.tid =team2.tid "
            . "where result.teamHomeNamePk=\"" . $team->tnPk . "\" or result.teamAwayNamePk=\"" . $team->tnPk . "\" group by result.date order by result.date DESC limit 10";

        $stmt = $db->query($result_sql);
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        $fixture_sql = "select fixture.*,team1.tid as tid1,team2.tid as tid2,t1.teamNameTh as teamNameTh1,t2.teamNameTh as teamNameTh2 from fixture "
            . " left join team as team1 on team1.tnPk=fixture.teamHomeNamePk"
            . " left join team as team2 on team2.tnPk=fixture.teamAwayNamePk"
            . " left join lang_team as t1 on t1.tid = team1.tid"
            . " left join lang_team as t2 on t2.tid = team2.tid"
            . " where teamNamePk=\"" . $team->tnPk . "\" and date>=current_date order by date ASC limit 10";
        $stmt = $db->query($fixture_sql);
        $fixture = $stmt->fetchAll(PDO::FETCH_OBJ);

        $stat_table_sql = "select *from stat_table where tnPk=\"" . $team->tnPk . "\" group by leagueId";
        $stmt = $db->query($stat_table_sql);
        $leagueList = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stat_table = array();
        foreach ($leagueList as $league) {
            $leagueId = $league->leagueId;
            $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table "
                . "            left join team on (team.tnPk=stat_table.tnPk) "
                . "            left join lang_team t on t.tid = team.tid "
                . "            where stat_table.leagueId=" . $leagueId
                . "            group by stat_table.tnPk order by stat_table.no ASC";
            $stmt = $db->query($sql);
            $stat_list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $new_stat_list = array();
            foreach ($stat_list as $stat) {
                $stat->ml = json_decode($stat->ml);
                $stat->ml_g = json_decode($stat->ml_g);
                $stat->ml_h = json_decode($stat->ml_h);
                if (is_numeric($stat->tid)) {
                    $tidlist .= ',' . $stat->tid;
                }
                array_push($new_stat_list, $stat);
            }
            $league_sql = "select *from league where leagueId =$leagueId";
            $stmt = $db->query($league_sql);
            $l = $stmt->fetch(PDO::FETCH_OBJ);
            $array = array(
                'league' => $l,
                'list' => $new_stat_list,
            );
            array_push($stat_table, $array);
        }


        if (!empty($team)) {
            if (is_numeric($team->tid)) {
                $tidlist .= ',' . $team->tid;
            }
        }
        foreach ($result as $rlist) {
            if (is_numeric($rlist->tid1)) {
                $tidlist .= ',' . $rlist->tid1;
            }
            if (is_numeric($rlist->tid2)) {
                $tidlist .= ',' . $rlist->tid2;
            }
        }
        foreach ($fixture as $flist) {
            if (is_numeric($flist->tid1)) {
                $tidlist .= ',' . $flist->tid1;
            }
            if (is_numeric($flist->tid2)) {
                $tidlist .= ',' . $flist->tid2;
            }
        }
        // echo $tidlist;
        $logossql = "SELECT * FROM team_logos WHERE tid IN (" . $tidlist . ") GROUP BY tid";
        // echo $logossql . "\n";
        $logostmt = $db->query($logossql);
        $tlogos = $logostmt->fetchAll(5);
        $logos = array();
        foreach ($tlogos as $logo) {
            $logos[$logo->tid] = $logo;
        }

        $obj = array(
            'team' => $team,
            'result' => $result,
            'fixture' => $fixture,
            'stat_table' => $stat_table,
            'logos' => $logos,
            'source' => "DB"
        );
        $json = json_encode($obj);
        //querylog(__FUNCTION__, $db);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function teamOnlys()
{
    //$db = DBConfig::getConnection();
    $ap = new Slim();
    $tid = $ap->request()->params('tid');
    $lang = $ap->request()->params('lang');
    $ln = $lang ? $lang . '/' : 'en/';
    $filename = "team_data/" . $ln . $tid . ".json";
    $json = null;
    if (file_exists($filename)) {
        //echo 1;
        $json = file_get_contents($filename);
        if (isset($_GET['jsoncallback'])) {
            echo $_GET['jsoncallback'] . '(' . $json . ')';
        } else {
            echo $json;
        }
    } else {
        teamOnly($tid);
    }
}

function favorite()
{
    $langList = array("en" => "En", "th" => "Th", "big" => "Big", "gb" => "Gb", "kr" => "Kr", "vn" => "Vn");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $id = $ap->request()->params("id");
    $device_id = $ap->request()->params("device_id");
    $platform = $ap->request()->params("platform");
    $competition_id = $ap->request()->params("competition_id");
    $league_id = $ap->request()->params('league_id');
    $favorite_type = $ap->request()->params("favorite_type");
    $sql = "select *from favorite where id=$id and device_id='$device_id' and platform='$platform' and favorite_type='$favorite_type'";
    $stmt = $db->query($sql);
    $favorite = $stmt->fetch(PDO::FETCH_OBJ);
    $success = 0;
    if ($favorite) {
        $success = 0;
    } else if ($device_id) {
        $sql = "insert ignore into favorite values($id,'$device_id','$platform','$favorite_type',$competition_id,$league_id)";
        $success = $db->exec($sql);

//        if ($favorite_type == 'team') {
//            $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamNameEn,team.tn) as name from favorite
//            left join team on favorite.id = team.tid
//            left join lang_team on favorite.id = lang_team.tid
//            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
//            $stmt = $db->query($team_sql);
//            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//            file_put_contents('favoriteTeam/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
//            foreach ($langList as $key => $ln) {
//                $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite
//            left join team on favorite.id = team.tid
//            left join lang_team on favorite.id = lang_team.tid
//            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
//                $stmt = $db->query($team_sql);
//                $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//                file_put_contents('favoriteTeam/' . $key . '/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
//            }
//        } else {
//            $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite
//            left join league on favorite.id = league.leagueId
//            left join lang_league on favorite.id = lang_league.leagueId
//            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
//
//            $stmt = $db->query($league_sql);
//            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//            file_put_contents('favoriteLeague/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
//            foreach ($langList as $key => $ln) {
//                $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueName$ln,league.leagueName) as name from favorite
//            left join league on favorite.id = league.leagueId
//            left join lang_league on favorite.id = lang_league.leagueId
//            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
//                $stmt = $db->query($league_sql);
//                $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//                file_put_contents('favoriteLeague/' . $key . '/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
//            }
//        }
    }

    if ($favorite_type == 'team') {
        $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamNameEn,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
        $stmt = $db->query($team_sql);
        $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
        file_put_contents('favoriteTeam/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        foreach ($langList as $key => $ln) {
            $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
            $stmt = $db->query($team_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            file_put_contents('favoriteTeam/' . $key . '/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
            //chmod('favoriteTeam/' . $key . '/' . $platform . '_' . addslashes($device_id) . '.json', 0777);
        }
    } else {
        $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";

        $stmt = $db->query($league_sql);
        $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
        file_put_contents('favoriteLeague/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        foreach ($langList as $key => $ln) {
            $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueName$ln,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";

            $stmt = $db->query($league_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $lfile = "favoriteLeague/$key/$platform" . "_" . addslashes($device_id) . ".json";
            //echo $lfile;
            file_put_contents($lfile, json_encode($favoriteList));
            //chmod($lfile, 0777);
        }
    }
    //querylog(__FUNCTION__, $db);
    echo $success;
}

function favoriteWithUID()
{
    $langList = array("en" => "En", "th" => "Th", "big" => "Big", "gb" => "Gb", "kr" => "Kr", "vn" => "Vn");
    $langList = array("en" => "En", "th" => "Th");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $id = $ap->request()->params("id");
    $device_id = $ap->request()->params("device_id");
    $uid = $ap->request()->params("uid");
    if (empty($uid)) {
        $uid = 0;
    }
    $platform = $ap->request()->params("platform");
    $competition_id = $ap->request()->params("competition_id");
    $league_id = $ap->request()->params('league_id');
    $favorite_type = $ap->request()->params("favorite_type");
    $sql = "select *from favorite_uid where id=$id and uid='$uid' and favorite_type='$favorite_type' and competition_id='$competition_id' and league_id='$league_id'";
    $stmt = $db->query($sql);
    $favorite = $stmt->fetch(PDO::FETCH_OBJ);
    $success = 0;
    if ($favorite) {
        $success = 0;
    } else if ($uid) {
        $sql = "insert ignore into favorite_uid (`id`, `uid`, `device_id`, `platform`, `favorite_type`, `competition_id`, `league_id`, `created_at`) values($id,$uid,'$device_id','$platform','$favorite_type',$competition_id,$league_id,NOW())";
        //echo $sql;
        $success = $db->exec($sql);

        // echo 'success :'.$success;
    }
    updateFavorite($db, $uid, $favorite_type);
//    if ($favorite_type == 'team') {
//        foreach ($langList as $key => $ln) {
//            $team_sql = "select favorite_uid.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite_uid
//left join team on favorite_uid.id = team.tid
//left join lang_team on favorite_uid.id = lang_team.tid
//where favorite_type='team'
//and uid ='$uid'
//group by team.tid";
//            $stmt = $db->query($team_sql);
//            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//            file_put_contents('favoriteTeam/' . $key . '/'  . $uid . '.json', json_encode($favoriteList));
//            chmod('favoriteTeam/' . $key . '/'  . $uid . '.json', 0777);
//        }
//    } else {
//        foreach ($langList as $key => $ln) {
//            $league_sql="select favorite_uid.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite_uid
//left join league on favorite_uid.id = league.leagueId
//left join lang_league on favorite_uid.id = lang_league.leagueId
//where favorite_type='league'
//and uid ='$uid'
//group by league.leagueId";
//                //echo $league_sql;
//            $stmt = $db->query($league_sql);
//            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
//            $lfile = "favoriteLeague/$key/$uid".".json";
//            //echo $lfile;
//            file_put_contents($lfile, json_encode($favoriteList));
//            chmod($lfile, 0777);
//        }
//    }
    //querylog(__FUNCTION__, $db);
    echo $success;
}

function updateFavorite($db, $uid, $favorite_type)
{
    $langList = array("en" => "En", "th" => "Th");
    if ($favorite_type == 'team') {
        foreach ($langList as $key => $ln) {
            $team_sql = "select favorite_uid.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite_uid
left join team on favorite_uid.id = team.tid
left join lang_team on favorite_uid.id = lang_team.tid
where favorite_type='team'
and uid ='$uid'
group by team.tid";
            $stmt = $db->query($team_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            file_put_contents('favoriteTeam/' . $key . '/' . $uid . '.json', json_encode($favoriteList));
            //chmod('favoriteTeam/' . $key . '/' . $uid . '.json', 0777);
        }
    } else {
        foreach ($langList as $key => $ln) {
            $league_sql = "select favorite_uid.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite_uid
left join league on favorite_uid.id = league.leagueId
left join lang_league on favorite_uid.id = lang_league.leagueId
where favorite_type='league'
and uid ='$uid'
group by league.leagueId";
            //echo $league_sql;
            $stmt = $db->query($league_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            $lfile = "favoriteLeague/$key/$uid" . ".json";
            //echo $lfile;
            file_put_contents($lfile, json_encode($favoriteList));
            //chmod($lfile, 0777);
        }
    }
}

function favoriteList()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $device_id = $ap->request()->params("device_id");
    $platform = $ap->request()->params("platform");
    $favorite_type = $ap->request()->params("favorite_type");

    $lang = $ap->request()->params("lang", 'en');
    $first = substr($lang, 0, 1);
    $ln = str_replace($first, strtoupper($first), $lang);
    //$sql = "select *from favorite where device_id='$device_id' and platform='$platform' and favorite_type='$favorite_type'";

    if ($favorite_type == 'team') {
        $sql = "
            select favorite.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='$favorite_type' and device_id ='$device_id' and platform='$platform' group by team.tid

            ";
        $stmt = $db->query($sql);
        //echo $sql;
    } else if ($favorite_type == 'league') {
        $sql = "
            select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueName$ln,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='$favorite_type' and device_id ='$device_id' and platform='$platform' group by league.leagueId

            ";
        $stmt = $db->query($sql);
    }
    $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
    //querylog(__FUNCTION__, $db);
    echo json_encode($favoriteList);
}

function favoriteListWithUID()
{
    $type = array('team' => "favoriteTeam", 'league' => "favoriteLeague");
    $ap = new Slim();
    $uid = $ap->request()->params("uid");
    $favorite_type = $ap->request()->params("favorite_type");
    $lang = $ap->request()->params("lang", 'en');
    $file = "{$type[$favorite_type]}/$lang/$uid.json";
    $favoriteList = array();
    if (!file_exists($file)) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        updateFavorite($db, $uid, $favorite_type);
    }
    $favoriteList = json_decode(file_get_contents($file), true);
    //querylog(__FUNCTION__, $db);
    echo json_encode($favoriteList);
}

function unfavorite()
{
    $langList = array("en" => "En", "th" => "Th", "big" => "Big", "gb" => "Gb", "kr" => "Kr", "vn" => "Vn");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $id = $ap->request()->params("id");
    $device_id = $ap->request()->params("device_id");
    $platform = $ap->request()->params("platform");
    $favorite_type = $ap->request()->params("favorite_type");
    if ($id) {
        $sql = "delete from favorite where id=$id and device_id='$device_id' and platform='$platform' and favorite_type='$favorite_type'";

        $success = $db->exec($sql);
    } else {
        $sql = "delete from favorite where device_id='$device_id' and platform='$platform' and favorite_type='$favorite_type'";
        $success = $db->exec($sql);
    }
    if ($favorite_type == 'team') {
        $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamNameEn,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
        $stmt = $db->query($team_sql);
        $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
        file_put_contents('favoriteTeam/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        foreach ($langList as $key => $ln) {
            $team_sql = "select favorite.*,if(lang_team.tid is not null ,lang_team.teamName$ln,team.tn) as name from favorite
            left join team on favorite.id = team.tid
            left join lang_team on favorite.id = lang_team.tid
            where favorite_type='team' and device_id ='$device_id' and platform='$platform' group by team.tid";
            $stmt = $db->query($team_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            file_put_contents('favoriteTeam/' . $key . '/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        }
    } else {
        $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueNameEn,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
        $stmt = $db->query($league_sql);
        $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
        file_put_contents('favoriteLeague/la/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        foreach ($langList as $key => $ln) {
            $league_sql = "select favorite.*,if(lang_league.leagueId is not null ,lang_league.leagueName$ln,league.leagueName) as name from favorite
            left join league on favorite.id = league.leagueId
            left join lang_league on favorite.id = lang_league.leagueId
            where favorite_type='league' and device_id ='$device_id' and platform='$platform' group by league.leagueId";
            $stmt = $db->query($league_sql);
            $favoriteList = $stmt->fetchAll(PDO::FETCH_OBJ);
            file_put_contents('favoriteLeague/' . $key . '/' . $platform . '_' . $device_id . '.json', json_encode($favoriteList));
        }
    }
    //querylog(__FUNCTION__, $db);
    echo $success;
}

function unfavoriteWithUID()
{
    $langList = array("en" => "En", "th" => "Th", "big" => "Big", "gb" => "Gb", "kr" => "Kr", "vn" => "Vn");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    //$fid = $ap->request()->params("fid", 0);
    $uid = $ap->request()->params("uid", 0);
    $id = $ap->request()->params("id");
    $league_id = $ap->request()->params("league_id");
    //$device_id = $ap->request()->params("device_id");
    //$platform = $ap->request()->params("platform");
    $favorite_type = $ap->request()->params("favorite_type");
    $favorite_type = $ap->request()->params("favorite_type", 'team');
    $sql = "delete from favorite_uid where id='$id' and uid='$uid' and favorite_type='$favorite_type' and league_id='$league_id'";
    $success = $db->exec($sql);
    updateFavorite($db, $uid, $favorite_type);
    echo $success;
}

function oddsToday()
{
    $file_name = 'odds/today.json';
    $json = '';
    if (file_exists($file_name)) {
        $json = file_get_contents($file_name);
    }
    if (isset($_GET['jsoncallback'])) {
        echo $_GET['jsoncallback'] . '(' . $json . ')';
    } else {
        echo $json;
    }
}

function getRanking()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $range = isset($_REQUEST["range"]) ? (int)$_REQUEST["range"] : 10;
    $rank = isset($_REQUEST["rank"]) ? $_REQUEST["rank"] : "all";
    $need = isset($_REQUEST["need"]) ? $_REQUEST["need"] : "all";
    $version = isset($_REQUEST["v"]) ? $_REQUEST["v"] : 0;
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "none";
    $start = $offset * 100;
    $result = array();
    switch ($version) {
        case 0:
            $sql = "select * from facebook_user AS fb WHERE fb.fb_uid IS NOT NULL ORDER BY gp DESC, w DESC, d DESC, pts DESC LIMIT $start,100";
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);

            //$foundself = false;
            foreach ($list as $key => $l) {
                if (count($result) < 100) {
                    foreach ($l as $attrkey => $attr) {
                        //  echo $attrkey . ":" . $attr . "\n";
                        if ($l->$attrkey == NULL) {
                            $l->$attrkey = "";
                        }
                    }
                    $result[$start + $key + 1] = $l;
                }
            }
            break;
        case 1:
            $result = getRankingV2($offset);
            break;
        case 3:
            $result = getRankingV3($offset, $range, $rank, $need);
            break;
        case 4:
            $result = getRankingV4($offset, $range, $rank, $need, $fb_uid);
            break;
        case 5:
            $result = getRankingV5($fb_uid, $offset, $range);
            break;
    }
    $json = json_encode(array('ranks' => $result));
    echo $json;
}

function getRankingV2($offset)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $start = $offset * 100;
    $sql = "SELECT fb.*,rm.gp_stamp,rm.gp_change FROM `ranking_map` rm
LEFT JOIN facebook_user fb ON rm.fb_uid=fb.fb_uid
WHERE fb.fb_uid IS NOT NULL
AND rm.gp_change<>0
ORDER BY rm.gp_change DESC, fb.w DESC, fb.d DESC, fb.pts DESC
LIMIT $start,100";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result = array("monthly" => array(), "alltime" => array());
    //$foundself = false;
    foreach ($list as $key => $l) {
        if (count($result) < 100) {
            foreach ($l as $attrkey => $attr) {
                //  echo $attrkey . ":" . $attr . "\n";
                if ($l->$attrkey == NULL) {
                    $l->$attrkey = "";
                }
            }
            $l->rank = $start + $key + 1;
            $result["monthly"][] = $l;
        }
    }

    $sql = "SELECT fb.*,rm.gp_stamp,rm.gp_change FROM `ranking_map` rm
LEFT JOIN facebook_user fb ON rm.fb_uid=fb.fb_uid
WHERE fb.fb_uid IS NOT NULL
ORDER BY fb.gp DESC, fb.w DESC, fb.d DESC, fb.pts DESC
LIMIT $start,100";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach ($list as $key => $l) {
        if (count($result) < 100) {
            foreach ($l as $attrkey => $attr) {
                //  echo $attrkey . ":" . $attr . "\n";
                if ($l->$attrkey == NULL) {
                    $l->$attrkey = "";
                }
            }
            $l->rank = $start + $key + 1;
            $result["alltime"][] = $l;
        }
    }

    return $result;
    //  $json = json_encode(array('ranks' => $result));
    //   echo $json;
}

function getRankingV3($offset, $range, $rank, $need)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $start = $offset * $range;
    $week = date("W");
    $week4 = date("W");
    $year = date("Y");
    if ($week % 2 != 0) {
        $week++;
    }
    if ($week4 % 4 != 0) {
        $week4 = $week4 + (4 - ($week4 % 4));
    }
    $results = array("score" => array(), "rating" => array(), "like" => array(), "newbies" => array());

    if ($rank == "score" || $rank == "all") {
        $result = array("rank14" => array(), "rank28" => array(), "rankall" => array());
        if ($need == "rank14" || $need == "all") {
            $sql14 = "SELECT r14s.rank,fb.fb_uid,fb.user_status as displayname,r14s.week,r14s.gp_stamp,r14s.gp_change,fb.gp FROM ranking14_score r14s
LEFT JOIN facebook_user fb ON r14s.fb_uid=fb.fb_uid
WHERE week=$week
ORDER BY rank
LIMIT $start,$range";
            $stmt14 = $db->query($sql14);
            $list14 = $stmt14->fetchAll(PDO::FETCH_OBJ);
            $result["rank14"] = $list14;
        }

        if ($need == "rank28" || $need == "all") {
            $sql28 = "SELECT r28s.rank,fb.fb_uid,fb.user_status as displayname,r28s.week,r28s.gp_stamp,r28s.gp_change,fb.gp FROM ranking28_score r28s
LEFT JOIN facebook_user fb ON r28s.fb_uid=fb.fb_uid
WHERE week=$week4
ORDER BY rank
LIMIT $start,$range";
            $stmt28 = $db->query($sql28);
            $list28 = $stmt28->fetchAll(PDO::FETCH_OBJ);
            $result["rank28"] = $list28;
        }

        if ($need == "rankall" || $need == "all") {
            $sql = "SELECT rs.rank,fb.fb_uid,fb.user_status as displayname,rs.year,rs.gp_stamp,rs.gp_change,fb.gp FROM rankingall_score rs
LEFT JOIN facebook_user fb ON rs.fb_uid=fb.fb_uid
WHERE year=$year
ORDER BY rank
LIMIT $start,$range";
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["rankall"] = $list;
        }
        $results["score"] = $result;
    }


    if ($rank == "like" || $rank == "all") {
        $result = array("rank14" => array(), "rank28" => array(), "rankall" => array());
        if ($need == "rank14" || $need == "all") {
            $sql14 = "SELECT r14s.rank,fb.fb_uid,fb.user_status as displayname,r14s.week,r14s.like_stamp,r14s.like_change,fb.gp FROM ranking14_like r14s
LEFT JOIN facebook_user fb ON r14s.fb_uid=fb.fb_uid
WHERE week=$week
ORDER BY rank
LIMIT $start,$range";
            $stmt14 = $db->query($sql14);
            $list14 = $stmt14->fetchAll(PDO::FETCH_OBJ);
            $result["rank14"] = $list14;
        }

        if ($need == "rank28" || $need == "all") {
            $sql28 = "SELECT r28s.rank,fb.fb_uid,fb.user_status as displayname,r28s.week,r28s.like_stamp,r28s.like_change,fb.gp FROM ranking28_like r28s
LEFT JOIN facebook_user fb ON r28s.fb_uid=fb.fb_uid
WHERE week=$week4
ORDER BY rank
LIMIT $start,$range";
            $stmt28 = $db->query($sql28);
            $list28 = $stmt28->fetchAll(PDO::FETCH_OBJ);
            $result["rank28"] = $list28;
        }

        if ($need == "rankall" || $need == "all") {
            $sql = "SELECT rs.rank,fb.fb_uid,fb.user_status as displayname,rs.year,rs.like_stamp,rs.like_change,fb.gp FROM rankingall_like rs
LEFT JOIN facebook_user fb ON rs.fb_uid=fb.fb_uid
WHERE year=$year
ORDER BY rank
LIMIT $start,$range";
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["rankall"] = $list;
        }
        $results["like"] = $result;
    }


    if ($rank == "newbies" || $rank == "all") {
        $result = array("rank14" => array(), "rank28" => array(), "rankall" => array());
        if ($need == "rank14" || $need == "all") {
            $sql14 = "SELECT r14s.rank,fb.fb_uid,fb.user_status as displayname,r14s.week,r14s.gp_stamp,r14s.gp_change,fb.gp FROM ranking14_newbies r14s
LEFT JOIN facebook_user fb ON r14s.fb_uid=fb.fb_uid
WHERE week=$week
ORDER BY rank
LIMIT $start,$range";
            $stmt14 = $db->query($sql14);
            $list14 = $stmt14->fetchAll(PDO::FETCH_OBJ);
            $result["rank14"] = $list14;
        }

        if ($need == "rank28" || $need == "all") {
            $sql28 = "SELECT r28s.rank,fb.fb_uid,fb.user_status as displayname,r28s.week,r28s.gp_stamp,r28s.gp_change,fb.gp FROM ranking28_newbies r28s
LEFT JOIN facebook_user fb ON r28s.fb_uid=fb.fb_uid
WHERE week=$week4
ORDER BY rank
LIMIT $start,$range";
            $stmt28 = $db->query($sql28);
            $list28 = $stmt28->fetchAll(PDO::FETCH_OBJ);
            $result["rank28"] = $list28;
        }

        if ($need == "rankall" || $need == "all") {
            $sql = "SELECT rs.rank,fb.fb_uid,fb.user_status as displayname,rs.year,rs.gp_stamp,rs.gp_change,fb.gp FROM rankingall_newbies rs
LEFT JOIN facebook_user fb ON rs.fb_uid=fb.fb_uid
WHERE year=$year
ORDER BY rank
LIMIT $start,$range";
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["rankall"] = $list;
        }
        $results["newbies"] = $result;
    }


    return $results;
    //  $json = json_encode(array('ranks' => $result));
    //   echo $json;
}

function getRankingV4($offset, $range, $rank, $need, $fb_uid)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("scores" => array(), "rating" => array(), "friends" => array(), "own" => array());
    $day = date("Y-m-d");
    $yesterday = date("Y-m-d", strtotime($day . "-1 day"));
    $twoweekago = date("Y-m-d", strtotime("-14day"));
    $week = date("W");
    $nowyear = date("Y");
    $lasttwoweek = date("Y-m-d");
    $lastdaysql = "SELECT update_at FROM user_rating ORDER BY update_at DESC";
    $stmt = $db->query($lastdaysql);
    $lastday = $stmt->fetchColumn();
    //echo $lastday;
    if ($week > 2) {
        if ($week % 2 == 0) {
            $mon = date("Y-m-d", strtotime('last monday'));
            if ($day === $mon) {
                $week -= 2;
            }
        } else {
            $week--;
        }
    }
    $week = (int)$week;
    if ($week < 10) {
        $lasttwoweek = date("Y-m-d", strtotime("$nowyear-W0$week-0"));
    } else {
        $lasttwoweek = date("Y-m-d", strtotime("$nowyear-W$week-0"));
    }
//    echo "$nowyear-W$week-0";
//    echo $week;
//    echo $lasttwoweek;
//    exit;
    $start = (int)$offset * (int)$range;
    $nowhour = date("H");
    $alllist = array();
    $list14 = array();
    $w2list = array();
    $r100list = array();
    $r200list = array();
    $owner = array("all" => array(), "last14day" => array(), "last2week" => array(), "rate100" => array(), "rate200" => array(), "friend14" => array(), "friend2w" => array());
    if ($rank == "all" || $rank == "score") {
        $scoresult = array("all" => array(), "last14day" => array(), "last2week" => array());
        $fileall = "ranking/all.json";
        $updateall = TRUE;

        if (file_exists($fileall)) {
            $json = json_decode(file_get_contents($fileall), true);
            if ($nowhour == $json["hour"]) {
                $scoresult["all"] = $json["data"];
                $updateall = FALSE;
            } else {
                $updateall = TRUE;
            }
        }

        if ($updateall) {
            $allsql = "SELECT *,overall_gp focus_gp,user_status as displayname FROM facebook_user WHERE pts<>0 ORDER BY overall_gp DESC";
            $stmt = $db->query($allsql);
            $scores = $stmt->fetchAll(5);
            foreach ($scores as $key => $user) {
                $alllist[$user->fb_uid] = $key + 1;
                if ($key < 10) {
                    $scoresult["all"][$key] = $user;
                    $scoresult["all"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $scoresult["all"][$key]->rank = $key + 1;
                }
            }
            $arrayall = array("hour" => date("H"), "data" => $scoresult["all"]);
            file_put_contents("ranking/all.json", json_encode($arrayall));
            file_put_contents("ranking/alllist.json", json_encode($alllist));
        }


        $file14 = "ranking/14.json";
        $update14 = TRUE;
        if (file_exists($file14)) {
            $json = json_decode(file_get_contents($file14), true);
            if ($nowhour == $json["hour"]) {
                $scoresult["last14day"] = $json["data"];
                $update14 = FALSE;
            } else {
                $update14 = TRUE;
            }
        }
        if ($update14) {
            $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at BETWEEN '$twoweekago' AND '$day'
AND fb.pts<>0
GROUP BY us.fb_uid
ORDER BY (CASE WHEN SUM(daily_change)<>0.0 then 0 ELSE 1 END),SUM(daily_change) DESC ";


            //echo $last14daysql;exit();
            $stmt = $db->query($last14daysql);
            $scores = $stmt->fetchAll(5);
            foreach ($scores as $key => $user) {
                $list14[$user->fb_uid] = $key + 1;
                if ($key < 10) {
                    $scoresult["last14day"][$key] = $user;
                    $scoresult["last14day"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $scoresult["last14day"][$key]->rank = $key + 1;
                }
            }
            $array14 = array("hour" => date("H"), "data" => $scoresult["last14day"]);
            file_put_contents("ranking/14.json", json_encode($array14));
            file_put_contents("ranking/14list.json", json_encode($list14));
        }


        $file2w = "ranking/2w.json";
        $update2w = TRUE;
        if (file_exists($file2w)) {
            $json = json_decode(file_get_contents($file2w), true);
            if ($nowhour == $json["hour"]) {
                $scoresult["last2week"] = $json["data"];
                $update2w = FALSE;
            } else {
                $update2w = TRUE;
            }
        }
        if ($update2w) {
            $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at ='$lasttwoweek'
AND fb.pts<>0
ORDER BY us.overall_gp DESC";

            $stmt = $db->query($last2week);
            $scores = $stmt->fetchAll(5);
            foreach ($scores as $key => $user) {
                $w2list[$user->fb_uid] = $key + 1;
                if ($key < 10) {
                    $scoresult["last2week"][$key] = $user;
                    $scoresult["last2week"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $scoresult["last2week"][$key]->rank = $key + 1;
                }
            }
            $array2w = array("hour" => date("H"), "data" => $scoresult["last2week"]);
            file_put_contents("ranking/2w.json", json_encode($array2w));
            file_put_contents("ranking/2wlist.json", json_encode($w2list));
        }

        $results["scores"] = $scoresult;
    }


    if ($rank == "all" || $rank == "rating") {
        $rateresult = array("rate100" => array(), "rate200" => array());


        $filer100 = "ranking/r100.json";
        $updater100 = TRUE;
        if (file_exists($filer100)) {
            $json = json_decode(file_get_contents($filer100), true);
            if ($nowhour == $json["hour"]) {
                $rateresult["rate100"] = $json["data"];
                $updater100 = FALSE;
            } else {
                $updater100 = TRUE;
            }
        }
        if ($updater100) {
            $last14daysql = "SELECT   fb.fb_name,fb.display_name,fb.lastResult,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
WHERE ur.last_100<>0
AND ur.update_at='$lastday'
ORDER BY  ur.last_100 DESC,ur.w100 DESC, ur.wh100 DESC,ur.d100 DESC";
            $stmt = $db->query($last14daysql);
            $rateres = $stmt->fetchAll(5);
            foreach ($rateres as $key => $user) {
                $r100list[$user->fb_uid] = $key + 1;
                if ($key < 10) {
                    $rateresult["rate100"][$key] = $user;
                    $rateresult["rate100"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $rateresult["rate100"][$key]->rank = $key + 1;
                }
            }
            $arrayr100 = array("hour" => date("H"), "data" => $rateresult["rate100"]);
            file_put_contents("ranking/r100.json", json_encode($arrayr100));
            file_put_contents("ranking/r100list.json", json_encode($r100list));
        }

        $filer200 = "ranking/r200.json";
        $updater200 = TRUE;
        if (file_exists($filer200)) {
            $json = json_decode(file_get_contents($filer200), true);
            if ($nowhour == $json["hour"]) {
                $rateresult["rate200"] = $json["data"];
                $updater200 = FALSE;
            } else {
                $updater200 = TRUE;
            }
        }
        if ($updater200) {
            $last14daysql = "SELECT   fb.fb_name,fb.display_name,fb.lastResult,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
WHERE ur.last_100<>0
AND ur.update_at='$lastday'
ORDER BY ur.last_200 DESC, ur.last_100 DESC";
            $stmt = $db->query($last14daysql);
            $rateres = $stmt->fetchAll(5);
            foreach ($rateres as $key => $user) {
                $r200list[$user->fb_uid] = $key + 1;
                if ($key < 10) {
                    $rateresult["rate200"][$key] = $user;
                    $rateresult["rate200"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $rateresult["rate200"][$key]->rank = $key + 1;
                }
            }
            $arrayr200 = array("hour" => date("H"), "data" => $rateresult["rate200"]);
            file_put_contents("ranking/r200.json", json_encode($arrayr200));
            file_put_contents("ranking/r200list.json", json_encode($r200list));
        }

        $results["rating"] = $rateresult;
    }

    if ($rank == "all" || $rank == "friends") {
        $friresult = array("last14day" => array(), "last2week" => array());
        $knowuser = "";
        if (is_numeric($fb_uid)) {
            $friend_sql = "SELECT DISTINCT ff.friend_facebook_id as id
FROM  facebook_friends ff
WHERE ff.facebook_id = '$fb_uid'";
            $stmtfriend = $db->query($friend_sql);
            $userlist = $stmtfriend->fetchAll(5);
            $knowuser = "('$fb_uid'";
            foreach ($userlist as $user) {
                $knowuser .= ",'{$user->id}'";
            }
            $knowuser .= ")";


            $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at BETWEEN '$twoweekago' AND DATE(NOW())
AND us.fb_uid IN $knowuser
GROUP BY us.fb_uid
ORDER BY (CASE WHEN SUM(daily_change)<>0.0 then 0 ELSE 1 END),SUM(daily_change) DESC";

            $stmt = $db->query($last14daysql);
            $frires = $stmt->fetchAll(5);
            foreach ($frires as $key => $user) {
                if ($key < 10) {
                    $friresult["last14day"][$key] = $user;
                    $friresult["last14day"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $friresult["last14day"][$key]->rank = $key + 1;
                }

                if ($user->fb_uid == $fb_uid) {
                    $owner["friend14"] = $user;
                    $owner["friend14"]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $owner["friend14"]->rank = $key + 1;
                }
                //$friresult["last14day"][$key]->bestprize = getBestPrize($user->fb_uid, 3);
            }

            $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at ='$lasttwoweek'
AND us.fb_uid IN $knowuser
ORDER BY us.overall_gp DESC LIMIT $start,$range";
            $stmt = $db->query($last2week);
            $frires = $stmt->fetchAll(5);
            foreach ($frires as $key => $user) {
                //$friresult["last2week"][$key]->bestprize = getBestPrize($user->fb_uid, 3);

                if ($key < 10) {
                    $friresult["last2week"][$key] = $user;
                    $friresult["last2week"][$key]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $friresult["last2week"][$key]->rank = $key + 1;
                }
                if ($user->fb_uid == $fb_uid) {
                    $owner["friend2w"] = $user;
                    $owner["friend2w"]->bestprize = getBestPrize($db, $user->fb_uid, 3);
                    $owner["friend2w"]->rank = $key + 1;
                }
            }
        }
        $results["friends"] = $friresult;
    }

    if (!empty($fb_uid)) {
        $file = "ranking/alllist.json";
        $jsn = json_decode(file_get_contents($file), true);
        if (array_key_exists($fb_uid, $jsn)) {
            $allsql = "SELECT *,overall_gp focus_gp,user_status as displayname FROM facebook_user WHERE fb_uid='$fb_uid'";
            $stmt = $db->query($allsql);
            $scoresall = $stmt->fetch(5);
            $scoresall->bestprize = getBestPrize($db, $fb_uid, 3);
            $scoresall->rank = $jsn[$fb_uid];
            $owner["all"] = $scoresall;
        }

        $file = "ranking/14list.json";
        $jsn = json_decode(file_get_contents($file), true);
        if (array_key_exists($fb_uid, $jsn)) {
            $last14daysql = "SELECT   us.fb_uid,SUM(daily_change) as focus_gp,fb.*,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at BETWEEN '$twoweekago' AND DATE(NOW())
AND fb.fb_uid='$fb_uid'";
            $stmt = $db->query($last14daysql);
            $scores14 = $stmt->fetch(5);
            $scores14->bestprize = getBestPrize($db, $fb_uid, 3);
            $scores14->rank = $jsn[$fb_uid];
            $owner["last14day"] = $scores14;
        }

        $file = "ranking/2wlist.json";
        $jsn = json_decode(file_get_contents($file), true);
        if (array_key_exists($fb_uid, $jsn)) {
            $last2week = "SELECT  fb.*,us.overall_gp focus_gp,fb.user_status as displayname  FROM user_statistic us
LEFT JOIN facebook_user fb ON us.fb_uid=fb.fb_uid
WHERE us.update_at ='$lasttwoweek'
AND fb.fb_uid='$fb_uid'";
            $stmt = $db->query($last2week);
            $scores2w = $stmt->fetch(5);
            $scores2w->bestprize = getBestPrize($db, $fb_uid, 3);
            $scores2w->rank = $jsn[$fb_uid];
            $owner["last2week"] = $scores2w;
        }

        $file = "ranking/r100list.json";
        $jsn = json_decode(file_get_contents($file), true);
        if (array_key_exists($fb_uid, $jsn)) {
            $last100 = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
WHERE fb.fb_uid='$fb_uid'
ORDER BY DATE(ur.update_at) DESC";
            $stmt = $db->query($last100);
            $scores2w = $stmt->fetch(5);
            $scores2w->bestprize = getBestPrize($db, $fb_uid, 3);
            $scores2w->rank = $jsn[$fb_uid];
            $owner["rate100"] = $scores2w;
        }

        $file = "ranking/r200list.json";
        $jsn = json_decode(file_get_contents($file), true);
        if (array_key_exists($fb_uid, $jsn)) {
            $last200 = "SELECT   fb.*,ur.*,fb.user_status as displayname FROM user_rating ur
LEFT JOIN   facebook_user fb ON  ur.fb_uid=fb.fb_uid
WHERE fb.fb_uid='$fb_uid'
ORDER BY DATE(ur.update_at) DESC";
            $stmt = $db->query($last200);
            $scores2w = $stmt->fetch(5);
            $scores2w->bestprize = getBestPrize($db, $fb_uid, 3);
            $scores2w->rank = $jsn[$fb_uid];
            $owner["rate200"] = $scores2w;
        }
    }
    $results["own"] = $owner;
    return $results;
}

function getRankingV5($fb_uid = null, $offset = 0, $limit = 10)
{
    $functionstart = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $result = array('success' => true, 'desc' => 'nothing', 'top_ranking' => array('alltime' => array(), 'now' => array(), '2week' => array()), 'top_sgold' => array('alltime' => array(), 'now' => array(), '2week' => array()), 'league' => array(), 'friend' => array('now' => array(), '2week' => array()));
    $nowday = date('Y-m-d');
    $startday = date("Y-m-d");
    $day = date('d');
    if ($day > 15) {
        $startday = date("Y") . "-" . date("m") . "-16";
    } else {
        $startday = date("Y") . "-" . date("m") . "-01";
    }

    $own = array('top_ranking' => array(), 'top_sgold' => array(), 'league' => array());
    $yesterday = date('Y-m-d', strtotime($nowday . "-1 day"));
    $start = $offset * $limit;


    $top_ranking = array('alltime' => array(), 'now' => array(), '2week' => array());
    $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$nowday' ORDER BY `rank` limit $start,$limit";
    $stmt = $db->query($sql);
    $top_ranking['now'] = $stmt->fetchAll(5);
    foreach ($top_ranking['now'] as $key => $tr) {
        $top_ranking['now'][$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$nowday' AND gr.`fb_uid`='$fb_uid'";
        $stmt = $db->query($sql);
        // $top_ranking['own'] = $stmt->fetch(5);
        $own['top_ranking']['now'] = $stmt->fetch(5);
        if ($own['top_ranking']['now']) {
            $own['top_ranking']['now']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_ranking']['now'] = array();
        }
    }

    $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$startday' ORDER BY `rank` limit $start,$limit";
    $stmt = $db->query($sql);
    $top_ranking['2week'] = $stmt->fetchAll(5);
    foreach ($top_ranking['2week'] as $key => $tr) {
        $top_ranking['2week'][$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$startday' AND gr.`fb_uid`='$fb_uid'";
        $stmt = $db->query($sql);
        // $top_ranking['own'] = $stmt->fetch(5);
        $own['top_ranking']['2week'] = $stmt->fetch(5);
        if ($own['top_ranking']['2week']) {
            $own['top_ranking']['2week']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_ranking']['2week'] = array();
        }
    }

    $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$nowday' ORDER BY `all_rank` limit $start,$limit";
    $stmt = $db->query($sql);
    $top_ranking['alltime'] = $stmt->fetchAll(5);
    foreach ($top_ranking['alltime'] as $key => $tr) {
        $top_ranking['alltime'][$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT gr.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM `gp_statistic` gr  LEFT JOIN `facebook_user` fb ON gr.fb_uid=fb.fb_uid  WHERE  gr.`update_at`='$nowday' AND gr.`fb_uid`='$fb_uid'";
        $stmt = $db->query($sql);
        // $top_ranking['own'] = $stmt->fetch(5);
        $own['top_ranking']['alltime'] = $stmt->fetch(5);
        if ($own['top_ranking']['alltime']) {
            $own['top_ranking']['alltime']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_ranking']['alltime'] = array();
        }
    }


    $result['top_ranking'] = $top_ranking;


    $top_sgold = array('alltime' => array(), 'now' => array(), '2week' => array());
    $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$nowday' ORDER BY rank  LIMIT $start,$limit";
    $stmt = $db->query($sql);
    $top_sgold['now'] = $stmt->fetchAll(5);
    foreach ($top_sgold['now'] as $key => $tr) {
        $top_sgold['now'] [$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$nowday' AND ss.fb_uid='$fb_uid'";
        $stmt = $db->query($sql);
        //$top_sgold['now']['own'] = $stmt->fetch(5);
        $own['top_sgold']['now'] = $stmt->fetch(5);
        if ($own['top_sgold']['now']) {
            $own['top_sgold']['now']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_sgold']['now'] = array();
        }
    }

    $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$nowday' ORDER BY all_sgold_rank  LIMIT $start,$limit";
    $stmt = $db->query($sql);
    $top_sgold['alltime'] = $stmt->fetchAll(5);
    foreach ($top_sgold['alltime'] as $key => $tr) {
        $top_sgold['alltime'] [$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$nowday' AND ss.fb_uid='$fb_uid'";
        $stmt = $db->query($sql);
        //$top_sgold['alltime']['own'] = $stmt->fetch(5);
        $own['top_sgold']['alltime'] = $stmt->fetch(5);
        if ($own['top_sgold']['alltime']) {
            $own['top_sgold']['alltime']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_sgold']['alltime'] = array();
        }
    }

    $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$startday' ORDER BY rank  LIMIT $start,$limit";
    $stmt = $db->query($sql);
    $top_sgold['2week'] = $stmt->fetchAll(5);
    foreach ($top_sgold['2week'] as $key => $tr) {
        $top_sgold['2week'] [$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
    }
    if (is_numeric($fb_uid)) {
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult,@@hostname FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$startday' AND ss.fb_uid='$fb_uid'";
        $stmt = $db->query($sql);
        //$top_sgold['2week']['own'] = $stmt->fetch(5);
        $own['top_sgold']['2week'] = $stmt->fetch(5);
        if ($own['top_sgold']['2week']) {
            $own['top_sgold']['2week']->bestprize2 = getBestPrize2($db, $fb_uid, 5);
        } else {
            $own['top_sgold']['2week'] = array();
        }
    }
    $result['top_sgold'] = $top_sgold;


    $league = array('34885', '35350', '35351', '35030', '34991');
    $json = json_decode(file_get_contents("system_config/league.json"), true);
    $league = $json['lid'];
    $leaguerank = array();
    foreach ($league as $lid) {
        $sql = "SELECT lr.*,fb.display_name,fb.pts,fb.w,lastResult,ll.leagueNameTh as league_name,@@hostname FROM league_ranking lr LEFT JOIN facebook_user fb ON lr.fb_uid=fb.fb_uid LEFT JOIN lang_league ll ON ll.leagueId=$lid WHERE lr.update_at='$nowday' AND _lid='$lid' ORDER BY rank  LIMIT $start,$limit";
        $stmt = $db->query($sql);
        $leaguerank[$lid] = $stmt->fetchAll(5);
        foreach ($leaguerank[$lid] as $key => $tr) {
            $leaguerank[$lid] [$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
        }
        if (is_numeric($fb_uid)) {
            $sql = "SELECT lr.*,fb.display_name,fb.pts,fb.w,lastResult,ll.leagueNameTh as league_name,@@hostname FROM league_ranking lr LEFT JOIN facebook_user fb ON lr.fb_uid=fb.fb_uid LEFT JOIN lang_league ll ON ll.leagueId=$lid WHERE lr.update_at='$nowday' AND _lid='$lid' AND lr.fb_uid='$fb_uid'";
            $stmt = $db->query($sql);
            //$leaguerank[$lid]['own'] = $stmt->fetch(5);
            $own['league'][$lid] = $stmt->fetch(5);
            if ($own['league'][$lid]) {
                $own['league'][$lid]->bestprize2 = getBestPrize2($db, $fb_uid, 5);
            } else {
                $own['league'][$lid] = array();
            }
        }
    }
    $result['league'] = $leaguerank;
    $result['own'] = $own;

    if (is_numeric($fb_uid)) {
        $sql = "SELECT friend_facebook_id FROM facebook_friends WHERE facebook_id='$fb_uid' AND friend_facebook_id <>'' AND friend_facebook_id IS NOT NULL";
        $stmt = $db->query($sql);
        $userlist = $stmt->fetchAll(5);
        $knowser = "$fb_uid";
        $friends = array();
        foreach ($userlist as $user) {
            $knowser .= ",{$user->friend_facebook_id}";
        }


        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$nowday' AND ss.fb_uid IN ($knowser) ORDER BY rank  LIMIT $start,$limit";
        $stmt = $db->query($sql);
        $friends['now'] = $stmt->fetchAll(5);
        foreach ($friends['now'] as $key => $tr) {
            $friends['now'][$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
        }
        $sql = "SELECT ss.*,fb.display_name,fb.pts,fb.w,lastResult FROM sgold_play_statistic ss LEFT JOIN facebook_user fb ON ss.fb_uid=fb.fb_uid WHERE ss.update_at='$startday' AND ss.fb_uid IN ($knowser) ORDER BY rank  LIMIT $start,$limit";
        $stmt = $db->query($sql);
        $friends['2week'] = $stmt->fetchAll(5);
        foreach ($friends['2week'] as $key => $tr) {
            $friends['2week'][$key]->bestprize2 = getBestPrize2($db, $tr->fb_uid, 5);
        }
        $result['friend'] = $friends;
    }

    $functionstop = Carbon::now();
    $result['function_time'] = $functionstop->diffInseconds($functionstart);
    //querylog(__FUNCTION__, $db);
    return $result;
}

function getRankingCountry()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $ap = new Slim();
    $uid = $ap->request()->params("uid");
    $country = $ap->request()->params("country");
    $sql = "select * from facebook_user AS fb WHERE fb.fb_uid IS NOT NULL AND country_id=$country ORDER BY gp DESC, w DESC, d DESC, pts DESC";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result = array();
    $foundself = false;
    foreach ($list as $key => $l) {
        if (count($result) < 50) {
            foreach ($l as $attrkey => $attr) {
                //  echo $attrkey . ":" . $attr . "\n";
                if ($l->$attrkey == NULL) {
                    $l->$attrkey = "";
                }
            }
            $result[$key + 1] = $l;
        }

//        if ($l->fb_uid == $uid) {
//            $foundself = true;
//            foreach ($l as $attrkey => $attr) {
//                //  echo $attrkey . ":" . $attr . "\n";
//                if ($l->$attrkey == NULL) {
//                    $l->$attrkey = "";
//                }
//            }
//            $result[$key + 1] = $l;
//        }
    }
    $json = json_encode(array('ranks' => $result));
    echo $json;
}

function getFriendRanking()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $uid = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : "";
    $version = isset($_REQUEST["v"]) ? (int)$_REQUEST["v"] : 0;
    $sql = "select friend_facebook_id as friends  from facebook_friends where facebook_id = $uid";
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(PDO::FETCH_COLUMN);
    $result = null;
    $results = array();

    $friendlist = "('$uid'";
    foreach ($list as $key => $l) {
        $friendlist .= "," . "'$l'";
    }
    $friendlist .= ")";
    //echo $friendlist;
    switch ($version) {
        case 0:
            $rsql = "select * from facebook_user WHERE fb_uid IN $friendlist ORDER BY gp DESC, w DESC, d DESC, pts DESC";
            $stmt = $db->query($rsql);
            $result = $stmt->fetchAll(PDO::FETCH_OBJ);
            foreach ($result as $key => $r) {
                foreach ($r as $attrkey => $attr) {
                    //  echo $attrkey . ":" . $attr . "\n";
                    if ($r->$attrkey == NULL) {
                        $r->$attrkey = "";
                    }
                }
                $results[$key + 1] = $r;
            }
            break;
        case 1:
            $results = getFriendRankingV2($friendlist);
            break;
    }
    $json = json_encode(array("ranks" => $results));
    echo $json;
}

function getFriendRankingV2($friendlist)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("monthly" => array(), "alltime" => array());


    $rsql = "SELECT fb.*,rm.gp_stamp,rm.gp_change FROM `ranking_map` rm
LEFT JOIN facebook_user fb ON rm.fb_uid=fb.fb_uid
WHERE fb.fb_uid IN $friendlist
AND rm.gp_change<>0
ORDER BY rm.gp_change DESC, fb.w DESC, fb.d DESC, fb.pts DESC";
    $stmt = $db->query($rsql);
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach ($result as $key => $r) {
        foreach ($r as $attrkey => $attr) {
            //  echo $attrkey . ":" . $attr . "\n";
            if ($r->$attrkey == NULL) {
                $r->$attrkey = "";
            }
        }
        $r->rank = $key + 1;
        $results["monthly"][] = $r;
    }


    $rsql = "SELECT fb.*,rm.gp_stamp,rm.gp_change FROM `ranking_map` rm
LEFT JOIN facebook_user fb ON rm.fb_uid=fb.fb_uid
WHERE fb.fb_uid IN $friendlist
ORDER BY fb.gp DESC, fb.w DESC, fb.d DESC, fb.pts DESC";
    $stmt = $db->query($rsql);
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    foreach ($result as $key => $r) {
        foreach ($r as $attrkey => $attr) {
            //  echo $attrkey . ":" . $attr . "\n";
            if ($r->$attrkey == NULL) {
                $r->$attrkey = "";
            }
        }
        $r->rank = $key + 1;
        $results["alltime"][] = $r;
    }


    return $results;
}

function commentOnbet()
{
    $betid = isset($_REQUEST["betid"]) ? $_REQUEST["betid"] : "";
    $choose = isset($_REQUEST["choose"]) ? $_REQUEST["choose"] : "draw";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $picture = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    $thumbnail = isset($_REQUEST["thumbnail"]) ? $_REQUEST["thumbnail"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($betid)) {
        $result["desc"] = "empty bet id";
    } else if (empty($message) || (empty($picture) && empty($thumbnail))) {
        $result["desc"] = "empty comment need message or picture";
    } else if (empty($fb_uid)) {
        $result["desc"] = "empty facebook id";
    } else {
        $datetime = strtotime(date('Y-m-d H:i:s'));
        $sql = "INSERT INTO `comment_on_bet` (`bet_id`, `fb_uid`, `message`,`picture`,`thumbnail`, `choose`, `comment_at`, `last_update`) VALUES ($betid, '$fb_uid', '$message','$picture','$thumbnail', '$choose', $datetime, $datetime);";
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $db->exec($sql);
//        $lastid = $db->lastInsertId();
//
//        $sql = "SELECT * FROM comment_on_bet WHERE id=$lastid";
//        $stmt = $db->query($sql);
//        $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);
//
//        $result["success"] = true;
//        $result["desc"] = "saved";
//        $result["data"] = $commentlist[0];
    }
    // echo json_encode($result);
}

function commentOnmatch()
{
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    $choose = isset($_REQUEST["choose"]) ? $_REQUEST["choose"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $picture = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    $thumbnail = isset($_REQUEST["thumbnail"]) ? $_REQUEST["thumbnail"] : "";
    $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "message";
    $country = isset($_REQUEST["country"]) ? $_REQUEST["country"] : 0;
    $parent_id = isset($_REQUEST["parent_id"]) ? $_REQUEST["parent_id"] : 0;
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $bet_sql = "SELECT * FROM bet WHERE mid='$mid' AND fb_uid='$fb_uid'";
    $betstmt = $db->query($bet_sql);
    $bet = $betstmt->fetch(5);
    $vote_sql = "SELECT * FROM vote_bet WHERE mid='$mid'AND fb_uid='$fb_uid'";
    $votstmt = $db->query($vote_sql);
    $vote = $votstmt->fetch(5);
    $message = addslashes($message);
    $match_sql = "SELECT * FROM live_match WHERE mid='$mid' ORDER BY showDate DESC";
    $matchstmt = $db->query($match_sql);
    $livematch = $matchstmt->fetch(5);
    $nowday = date("Y-m-d");
    $limitday = date("Y-m-d", strtotime($livematch->showDate . '+3 day'));
    if ($nowday <= $limitday) {
        if ((empty($bet) && empty($vote)) && $nowday <= $livematch->showDate) {
            $result["desc"] = "คุณยังไม่ได้เข้าร่วมเกมส์ กรุณาทายผลก่อน";
        } else if (empty($message) && empty($picture)) {
            $result["desc"] = "empty message or picture";
        } else if (empty($fb_uid)) {
            $result["desc"] = "empty facebook id";
        } else {
            if (empty($choose)) {
                if (!empty($bet)) {
                    $choose = $bet->choose;
                }
            }
            $datetime = strtotime(date('Y-m-d H:i:s'));
            $sql = "INSERT INTO `comment_on_match` (`match_id`, `fb_uid`, `message`,`picture`,`thumbnail`, `choose`,`type`, `comment_at`, `last_update`,`country_id`,`parent_id`) VALUES ($mid, '$fb_uid', '$message', '$picture', '$thumbnail', '$choose', '$type', $datetime, $datetime,$country,$parent_id);";
            $db->exec($sql);
            $lastid = $db->lastInsertId();

            $sql = "SELECT cm.*,fb.user_status displayname FROM comment_on_match cm
        LEFT JOIN facebook_user fb ON cm.fb_uid=fb.fb_uid
        WHERE id=$lastid";
            $stmt = $db->query($sql);
            $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);
            writeMatchComment($mid);
            if ($parent_id) {
                $sql = "SELECT * FROM  `comment_on_match` WHERE id='$parent_id'";
                $stmt = $db->query($sql);
                $cm = $stmt->fetch(5);
                if ($fb_uid != $cm->fb_uid) {
                    regEvent($db, $fb_uid, 'comment', ' ได้ตอบกลับความคิดเห็นบนคู่บอลที่คุณเล่น', "$message", 'game', "{$cm->match_id}", "{$cm->fb_uid}");
                }
            }

            $result["success"] = true;
            $result["desc"] = "saved";
            $result["data"] = $commentlist[0];
        }
    } else {
        $result["success"] = FALSE;
        $result["desc"] = "Comment on too long finished game.";
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function msgFromBet($db, $mid, $choose, $fb_uid, $message, $country)
{
    //$db = DBConfig::getConnection();
    $picture = "";
    $thumbnail = "";
    $type = "message";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($mid)) {
        echo "empty mid";
    } else if (empty($fb_uid)) {
        echo "empty facebook id";
    } else {
        $datetime = strtotime(date('Y-m-d H:i:s'));
        $sql = "INSERT INTO `comment_on_match` (`match_id`, `fb_uid`, `message`,`picture`,`thumbnail`, `choose`,`type`, `comment_at`, `last_update`,`country_id`) VALUES ($mid, '$fb_uid', '$message', '$picture', '$thumbnail', '$choose', '$type', $datetime, $datetime,$country);";
        $db->exec($sql);
    }
}

function editCommentOnbet()
{
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
    $choose = isset($_REQUEST["choose"]) ? $_REQUEST["choose"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($id)) {
        $result["desc"] = "empty id";
    } else {
        if (!empty($choose) || !empty($message)) {
            $condition = "";
            if (!empty($choose)) {
                $condition .= "`choose`='$choose'";
            }

            if (!empty($message)) {
                if (!empty($condition))
                    $condition .= ",";
                $condition .= "`message`='$message'";
            }
            $datetime = strtotime(date('Y-m-d H:i:s'));
            $condition .= ",`last_update`=$datetime";
            $sql = "UPDATE `comment_on_bet` SET $condition WHERE  `id`=$id;";
            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $db->exec($sql);
            $sql = "SELECT * FROM comment_on_bet WHERE id=$id";
            $stmt = $db->query($sql);
            $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["success"] = true;
            $result["desc"] = "updated";
            $result["data"] = $commentlist[0];
        } else {
            $result["desc"] = "invalid parameter";
        }
    }
    echo json_encode($result);
}

function editCommentOnmatch()
{
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
    $choose = isset($_REQUEST["choose"]) ? $_REQUEST["choose"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($id)) {
        $result["desc"] = "empty id";
    } else {
        if (!empty($choose) || !empty($message)) {
            $condition = "";
            if (!empty($choose)) {
                $condition .= "`choose`='$choose'";
            }

            if (!empty($message)) {
                if (!empty($condition))
                    $condition .= ",";
                $condition .= "`message`='$message'";
            }
            $datetime = strtotime(date('Y-m-d H:i:s'));
            $condition .= ",`last_update`=$datetime";
            $sql = "UPDATE `comment_on_match` SET $condition WHERE  `id`=$id;";
            $db = DBConfig::getConnection();
            $db->exec("SET profiling = 1;");
            $db->exec($sql);
            $sql = "SELECT * FROM comment_on_match WHERE id=$id";
            $stmt = $db->query($sql);
            $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["success"] = true;
            $result["desc"] = "updated";
            $result["data"] = $commentlist[0];
        } else {
            $result["desc"] = "invalid parameter";
        }
    }
    echo json_encode($result);
}

function removeCommentOnbet()
{
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($id)) {
        $result["desc"] = "empty id";
    } else {
        $sql = "UPDATE `comment_on_bet` SET `remove`='Y' WHERE  `id`=$id;";
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $db->exec($sql);
        //$lastid = $db->lastInsertId();
        $result["success"] = true;
        $result["desc"] = "remove";
        //$result["data"] = $lastid;
    }
    echo json_encode($result);
}

function removeCommentOnmatch()
{
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");

    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($id)) {
        $result["desc"] = "empty id";
    } else {
        $updatesql = "SELECT match_id FROM comment_on_match WHERE `id`='$id'";
        $updatestmt = $db->query($updatesql);
        $mid = $updatestmt->fetch();
        $super = getSu();
        $sql = "UPDATE `comment_on_match` SET `remove`='Y' WHERE  `id`=$id AND fb_uid='$fb_uid'";
        if ($fb_uid == $super) {
            $sql = "UPDATE `comment_on_match` SET `remove`='Y' WHERE  `id`=$id";
        }
        $success = $db->exec($sql);
        if ($success) {
            $result["success"] = true;
            $result["desc"] = "remove";
            writeMatchComment($mid[0]);
        } else {
            $result["desc"] = "permission denied.";
        }
    }
    echo json_encode($result);
}

function getCommentOnBet()
{
    $betid = isset($_REQUEST["betid"]) ? $_REQUEST["betid"] : "";
    $sort = isset($_REQUEST["order"]) ? $_REQUEST["order"] : "squence";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($betid)) {
        $result["desc"] = "empty id";
    } else {
        $order = "ASC";
        if ($sort == "recent") {
            $order = "DESC";
        }

        $sql = "SELECT * FROM comment_on_bet WHERE bet_id=$betid AND remove='N' ORDER BY id $order";
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($sql);
        $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);


        $result["success"] = true;
        $result["desc"] = "remove";
        $result["list"] = $commentlist;
    }

    echo json_encode($result);
}

function getCommentOnmatch()
{
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    $sort = isset($_REQUEST["order"]) ? $_REQUEST["order"] : "squence";
    $country = isset($_REQUEST["country"]) ? $_REQUEST["country"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "list" => array(), "vote" => array());
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (empty($mid)) {
        $result["desc"] = "empty id";
    } else {
        $order = "ASC";
        if ($sort == "recent") {
            $order = "DESC";
        }

        $countryselect = "";
        if (!empty($country)) {
            $countryselect = " AND country_id='$country'";
        }
//        $sql = "SELECT * FROM live_match WHERE mid=$mid";
//        $stmt = $db->query($sql);
//        $match = $stmt->fetch(PDO::FETCH_OBJ);
        //$commentdate = strtotime($match->date . "-3 day");
        $sql = "SELECT cm.*,fb.display_name FROM comment_on_match cm LEFT JOIN facebook_user fb ON  cm.fb_uid=fb.fb_uid  WHERE cm.match_id='$mid' AND cm.remove='N' $countryselect ORDER BY cm.id $order";
        $stmt = $db->query($sql);
        $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);

        $result["success"] = true;
        $result["desc"] = "success";
        $result["list"] = $commentlist;

        foreach ($commentlist as $comment) {
            $sql = "SELECT mcl.fb_uid,fb.fb_name,fb.display_name FROM match_comment_like mcl
                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
                WHERE comment_id='{$comment->id}' AND status='like'";
            $stmt = $db->query($sql);
            $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);
            $result["like"][$comment->id] = $likelist;
        }
        $vote = array("home" => array("count" => 0, "people" => array()), "draw" => array("count" => 0, "people" => array()), "away" => array("count" => 0, "people" => array()));
        $votesql = "SELECT * FROM vote_bet WHERE mid='$mid'";
        $votestmt = $db->query($votesql);
        $votelist = $votestmt->fetchAll(5);
        foreach ($votelist as $v) {
            $vote[$v->vote_choose]['count']++;
            if (!empty($v->fb_uid)) {
                $vote[$v->vote_choose]['people'][] = $v->fb_uid;
            }
        }
        $result["vote"] = $vote;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function mCommentLike()
{
    $commentid = isset($_REQUEST["commentid"]) ? $_REQUEST["commentid"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";

    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($commentid)) {
        $result["desc"] = "empty comment id";
    } else if (empty($fb_uid)) {
        $result["desc"] = "empty fb uid";
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT * FROM match_comment_like WHERE comment_id=$commentid AND fb_uid=$fb_uid";
        $stmt = $db->query($sql);
        $liked = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (empty($liked)) {
            $db->beginTransaction();
            try {
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $sql = "INSERT INTO `match_comment_like` (`comment_id`, `fb_uid`,`like_at`) VALUES ($commentid, '$fb_uid',$datetime);";
                $db->exec($sql);

                $sql = "UPDATE `comment_on_match` SET `like`=`like`+1 WHERE  `id`=$commentid;";
                $db->exec($sql);


                $user = $db->query("select fb_uid from comment_on_match
            where id = $commentid");
                $uid = $user->fetch(PDO::FETCH_OBJ);
                $sql_update_spirit = "UPDATE `facebook_user` SET `spirit`=`spirit`+0.1 WHERE  `fb_uid`='{$uid->fb_uid}';";
                $db->exec($sql_update_spirit);
                $db->commit();

//                $sql_json = "select *from facebook_user where fb_uid='{$uid->fb_uid}'";
//                $stmt = $db->query($sql_json);
//                $obj = $stmt->fetch(PDO::FETCH_OBJ);
//                foreach ($obj as $key => $value) {
//                    if ($value == NULL) {
//                        $obj->$key = "";
//                    }
//                }
//                file_put_contents('facebook_info/' . $uid->fb_uid . '.json', json_encode($obj));
//                chmod('facebook_info/' . $uid->fb_uid . '.json', 0777);
                FBtofile($uid->fb_uid);
                if ($commentid) {
                    $sql = "SELECT * FROM  `comment_on_match` WHERE id='$commentid'";
                    $stmt = $db->query($sql);
                    $cm = $stmt->fetch(5);
                    if ($fb_uid != $cm->fb_uid) {
                        regEvent($db, $fb_uid, 'like', 'ถูกใจความคิดเห็นของคุณ', "{$cm->message}", 'game', "{$cm->match_id}", "{$cm->fb_uid}");
                    }
                }
            } catch (Exception $e) {
                $db->rollBack();
            }


            $lastid = $db->lastInsertId();
            $sql = "SELECT mcl.fb_uid,fb.fb_name FROM match_comment_like mcl
                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
                WHERE comment_id=$commentid AND status='like'";
            $stmt = $db->query($sql);
            $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);

            $result["success"] = true;
            $result["desc"] = "updated";
            $result["likelist"] = $likelist;
        } else {
            if ($liked[0]->status == "unlike") {
                $datetime = strtotime(date('Y-m-d H:i:s'));
                $sql = "UPDATE `match_comment_like` SET `status`='like' WHERE  comment_id=$commentid AND fb_uid=$fb_uid";
                $db = DBConfig::getConnection();
                $db->exec("SET profiling = 1;");
                $db->exec($sql);


                $db->beginTransaction();
                try {
                    $sql = "UPDATE `comment_on_match` SET `like`=`like`+1 WHERE  `id`=$commentid;";
                    $db->exec($sql);
                    $db->commit();
                } catch (Exception $e) {
                    $db->rollBack();
                }


                $lastid = $db->lastInsertId();
                $sql = "SELECT mcl.fb_uid,fb.fb_name FROM match_comment_like mcl
                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
                WHERE comment_id=$commentid AND status='like'";
                $stmt = $db->query($sql);
                $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);

                $result["success"] = true;
                $result["desc"] = "updated";
                $result["likelist"] = $likelist;
            } else {
                $result["desc"] = "like already";
            }
        }
    }
    if (!empty($mid)) {
        writeMatchComment($mid);
    }
    echo json_encode($result);
}

function mCommentUnlike()
{
    $commentid = isset($_REQUEST["commentid"]) ? $_REQUEST["commentid"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($commentid)) {
        $result["desc"] = "empty comment id";
    } else if (empty($fb_uid)) {
        $result["desc"] = "empty fb uid";
    } else {
        $datetime = strtotime(date('Y-m-d H:i:s'));
        $sql = "UPDATE `match_comment_like` SET `status`='unlike' WHERE  comment_id=$commentid AND fb_uid=$fb_uid";
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $db->exec($sql);


        $db->beginTransaction();
        try {
            $sql = "UPDATE `comment_on_match` SET `like`=`like`-1 WHERE  `id`=$commentid;";
            $db->exec($sql);
            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
        }


        $lastid = $db->lastInsertId();
        $sql = "SELECT mcl.fb_uid,fb.fb_name FROM match_comment_like mcl
                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
                WHERE comment_id=$commentid AND status='like'";
        $stmt = $db->query($sql);
        $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);

        $result["success"] = true;
        $result["desc"] = "updated";
        $result["likelist"] = $likelist;
    }
    if (!empty($mid)) {
        writeMatchComment($mid);
    }
    echo json_encode($result);
}

function mCommentReport()
{
    $commentid = isset($_REQUEST["commentid"]) ? $_REQUEST["commentid"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $reason = isset($_REQUEST["reason"]) ? $_REQUEST["reason"] : "none";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($commentid)) {
        $result["desc"] = "empty comment id";
    } else if (empty($fb_uid)) {
        $result["desc"] = "empty fb uid";
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT * FROM match_comment_report WHERE comment_id=$commentid AND fb_uid=$fb_uid";
        $stmt = $db->query($sql);
        $reported = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (empty($reported)) {


            $datetime = strtotime(date('Y-m-d H:i:s'));
            $sql = "INSERT INTO `match_comment_report` (`comment_id`, `fb_uid`, `reason`, `report_at`) VALUES ($commentid, '$fb_uid', '$reason', $datetime);";
            $db->exec($sql);


            $db->beginTransaction();
            try {
                $sql = "UPDATE `comment_on_match` SET `report`=`report`+1 WHERE  `id`=$commentid;";
                $db->exec($sql);
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
            }


//            $lastid = $db->lastInsertId();
//            $sql = "SELECT mcl.fb_uid,fb.fb_name FROM match_comment_like mcl
//                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
//                WHERE comment_id=$commentid AND status='like'";
//            $stmt = $db->query($sql);
//            $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);

            $result["success"] = true;
            $result["desc"] = "updated";
            // $result["likelist"] = $likelist;
        } else {
            $result["desc"] = "already report";
        }
    }
    echo json_encode($result);
}

function getReportedcomment()
{
    $show = isset($_REQUEST["show"]) ? $_REQUEST["show"] : "all";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");

    $optional = "";
    if ($show == "unremove") {
        $optional .= " AND remove='N'";
    } else if ($show == "remove") {
        $optional .= " AND remove='Y'";
    }
}

function swcp_ios_version3()
{
    // $list = array('1.1.6', 'free', 'itms://itunes.apple.com/app/id640924601');
    echo "1.2.0,free,itms://itunes.apple.com/app/id640924601";
}

function getCountryId()
{
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : "";
    $platform = isset($_REQUEST["platform"]) ? $_REQUEST["platform"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $straight = array("ios" => "ios", "android" => "android");
    $reverse = array("ios" => "android", "android" => "ios");
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($name) || empty($platform)) {
        $result["desc"] = "incomplete parameter";
    } else {
        $platform_con = $straight[$platform];
        $sql = "SELECT * FROM country WHERE $platform_con='$name'";
        $stmt = $db->query($sql);
        $namelist = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (!empty($namelist)) {
            $result["success"] = true;
            $result["desc"] = "success";
            $result["data"] = $namelist[0];
            if (!empty($fb_uid)) {
                $sql = "UPDATE `facebook_user` SET `country_id`={$namelist[0]->id} WHERE  `fb_uid`='$fb_uid';";
                $db->exec($sql);
            }
        } else {
            $platform_con = $reverse[$platform];
            $sql = "SELECT * FROM country WHERE $platform_con='$name'";
            $stmt = $db->query($sql);
            $country = $stmt->fetch(PDO::FETCH_OBJ);
            $id = 0;
            if (!empty($country)) {
                $id = $country->id;
                $sql = "UPDATE country SET {$straight[$platform]}='$name' WHERE id=$id";
                $db->exec($sql);
            } else {
                $sql = "INSERT INTO country (`{$straight[$platform]}`) VALUES ('$name')";
                $db->exec($sql);
                $id = $db->lastInsertId();
            }

            $sql = "SELECT * FROM country WHERE id=$id";
            $stmt = $db->query($sql);
            $country = $stmt->fetch(PDO::FETCH_OBJ);
            $result["desc"] = "New country";
            $result["data"] = $country;
        }
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function updateUserStatus()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $status = isset($_REQUEST["status"]) ? $_REQUEST["status"] : "";
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (empty($fb_uid)) {
        $result["desc"] = "empty fb_uid";
    } else if (empty($status)) {
        $result["desc"] = "empty status";
    } else if (strlen($status) > 255) {
        $result["desc"] = "status too long";
    } else {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "UPDATE `facebook_user` SET `user_status`='$status' WHERE  `fb_uid`='$fb_uid';";
        $db->exec($sql);
//        $sql = "SELECT * FROM `facebook_user` WHERE  `fb_uid`='$fb_uid';";
//        $stmt = $db->query($sql);
//        $user = $stmt->fetch(PDO::FETCH_OBJ);
//        if (empty($user->user_status)) {
//            $user->user_status = "";
//        }
//        foreach ($user as $key => $value) {
//            if ($value == NULL) {
//                $user->$key = "";
//            }
//        }
//        file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($user));
//        chmod('facebook_info/' . $fb_uid . '.json', 0777);
        FBtofile($fb_uid);

        $result["success"] = true;
        $result["desc"] = "success";
        $result["data"] = $user;
    }
    echo json_encode($result);
}

function getAllTeams()
{
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $langlist = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");

    $filename = "cronjob_gen_file/files/{$lang}/liveMatchOnly.json";
    $filename2 = "cronjob_gen_file/files/{$lang}/liveMatchWaitOnly.json";
    $filename3 = "cronjob_gen_file/files/{$lang}/liveMatchResultOnly.json";
    $filename4 = "cronjob_gen_file/files/{$lang}/liveMatchYesterdayResultOnly.json";
    $teamfile = "system_config/tid.json";

//    $oldtid = array();
//    if (file_exists($teamfile)) {
//        $oldtid = json_decode(file_get_contents($teamfile), true);
//    } else {
//        file_put_contents($teamfile, json_encode(array()));
//    }

    $filelist = array($filename, $filename2, $filename3, $filename4);
    //$tidlist = array();
    $keytid = "0";
//    foreach ($filelist as $f) {
//        $jsn = json_decode(file_get_contents($f), true);
//        foreach ($jsn['Teamlogos'] as $key => $team) {
//            // $tidlist[$key] = 1;
//            $keytid .= ",$key";
//        }
//    }

//    if ($tidlist != $oldtid) {
//        file_put_contents($teamfile, json_encode($tidlist));
//    }
    //$sql = "SELECT tid,$langlist[$lang] as name FROM lang_team WHERE tid IN ($keytid) ORDER BY tid";
    $sql = "SELECT tid,$langlist[$lang] as name FROM lang_team ORDER BY tid";
    $stmt = $db->query($sql);
    $teamlist = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result = array();
    foreach ($teamlist as $tl) {
        $result[$tl->tid] = $tl;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function getAllLeagues()
{
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $langlist = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "SELECT leagueId,$langlist[$lang] as name FROM lang_league ORDER BY leagueId";
    $stmt = $db->query($sql);
    $teamlist = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result = array();
    foreach ($teamlist as $tl) {
        $result[$tl->leagueId] = $tl;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function getSubleagues()
{
    //29724
    $lid = isset($_REQUEST["lid"]) ? $_REQUEST["lid"] : "";
    $resul = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (!empty($lid)) {
        $sql_str = "select *from (\n"
            . "(\n"
            . "select league.leaguePriority,league.leagueId,league.rid,league.competitionId,league.leagueNamePk,league.leagueName,league.leagueSeason,league.priority,\n"
            . "lang_league.leagueNameBig,lang_league.leagueNameTh,lang_league.leagueNameGb,lang_league.leagueNameKr,lang_league.leagueNameVn,\n"
            . "lang_league.leagueNameLa,lang_league.leagueNameEn,0 as subLeague\n"
            . "from league\n"
            . "left join sub_league on sub_league.leagueId = league.leagueId\n"
            . "left join lang_league on league.leagueId = lang_league.leagueId\n"
            . "left join stat_table as s on s.leagueId = league.leagueId\n"
            . "where league.leagueId=$lid AND s.subLeagueNamePk =''\n"
            . "group by league.leagueId\n"
            . ") \n"
            . "union\n"
            . "(\n"
            . "select league.leaguePriority,league.leagueId,league.rid,league.competitionId,league.leagueNamePk,league.leagueName,league.leagueSeason,league.priority,\n"
            . "lang_league.leagueNameBig,lang_league.leagueNameTh,lang_league.leagueNameGb,lang_league.leagueNameKr,lang_league.leagueNameVn,\n"
            . "lang_league.leagueNameLa,lang_league.leagueNameEn,1 as subLeague\n"
            . "from league\n"
            . "left join sub_league on sub_league.leagueId = league.leagueId\n"
            . "left join lang_league on league.leagueId = lang_league.leagueId\n"
            . "left join stat_table as s on s.leagueId = league.leagueId\n"
            . "where league.leagueId=$lid AND s.subLeagueNamePk !=''\n"
            . "group by league.leagueId\n"
            . "having count(s.subLeagueNamePk)>0\n"
            . ")\n"
            . "union\n"
            . "(\n"
            . "select league.leaguePriority,league.leagueId,league.rid,league.competitionId,league.leagueNamePk,league.leagueName,league.leagueSeason,league.priority,\n"
            . "lang_league.leagueNameBig,lang_league.leagueNameTh,lang_league.leagueNameGb,lang_league.leagueNameKr,lang_league.leagueNameVn,\n"
            . "lang_league.leagueNameLa,lang_league.leagueNameEn,2 as subLeague\n"
            . "from league\n"
            . "left join sub_league on sub_league.leagueId = league.leagueId\n"
            . "left join lang_league on league.leagueId = lang_league.leagueId\n"
            . "left join stat_table as s on s.leagueId = league.leagueId\n"
            . "where league.leagueId=$lid  \n"
            . "group by league.leagueId\n"
            . "having count(s.subLeagueNamePk)=0\n"
            . ") \n"
            . ") league\n"
            . "order by (CASE WHEN league.leaguePriority IS NULL then 1 ELSE 0 END),league.leaguePriority ASC,league.leagueId\n"
            . "";
        //echo $sql_str;

        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $stmt = $db->query($sql_str);
        $league = $stmt->fetchAll(PDO::FETCH_OBJ);
        //querylog(__FUNCTION__, $db);
        if (!empty($league)) {
            $resul["success"] = true;
            $resul["desc"] = "success";
            $resul["data"] = $league[0];
        } else {
            $resul["desc"] = "league not found";
        }
    }
    echo json_encode($resul);
}

function getBetInfo()
{
    $betid = isset($_REQUEST["betid"]) ? $_REQUEST["betid"] : "";
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $leaguelist = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
    $teamlist = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $result = array("success" => false, "desc" => "nothing happen", "data" => NULL);
    if (!empty($betid)) {
        $own_sql = "SELECT * FROM bet b
        LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
        WHERE betId=$betid";
        $stmtown = $db->query($own_sql);
        $own = $stmtown->fetch(5);
        if (!empty($own)) {
            $result["data"]["own"] = $own;
        } else {
            $result["desc"] = "bet not found";
        }
        $league_sql = "SELECT leagueId,id7m,{$leaguelist[$lang]} name FROM lang_league WHERE leagueId={$own->leagueId}";
        $stmtleague = $db->query($league_sql);
        $league = $stmtleague->fetch(5);
        if (!empty($league)) {
            $result["data"]["league"] = $league;
        } else {
            $result["desc"] = "league not found";
        }
        $home_sql = "SELECT tid,id7m,{$teamlist[$lang]} name FROM lang_team WHERE tid={$own->hid}";
        $stmthome = $db->query($home_sql);
        $home = $stmthome->fetch(5);
        if (!empty($home)) {
            $result["data"]["team"]["home"] = $home;
        } else {
            $result["desc"] = "Home team not found";
        }
        $away_sql = "SELECT tid,id7m,{$teamlist[$lang]} name FROM lang_team WHERE tid={$own->gid}";
        $stmtaway = $db->query($away_sql);
        $away = $stmtaway->fetch(5);
        if (!empty($away)) {
            $result["data"]["team"]["away"] = $away;
        } else {
            $result["desc"] = "Away team not found";
        }
        $sql = "SELECT * FROM bet b
        LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
        WHERE mid={$own->mid}";
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(5);
        if (!empty($list)) {
            $result["data"]["list"] = $list;
        } else {
            $result["desc"] = "bet list not found";
        }
    } else {
        $result["desc"] = "empty bet id";
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function getBetOnMatchInfo()
{
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $leaguelist = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
    $teamlist = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $result = array("success" => false, "desc" => "nothing happen", "data" => NULL);
    if (!empty($mid)) {
        $live_query = "SELECT * FROM live_match WHERE mid=$mid";
        $livestmt = $db->query($live_query);
        $live = $livestmt->fetch(5);
        if (!empty($live)) {
            $league_sql = "SELECT leagueId,id7m,{$leaguelist[$lang]} name FROM lang_league WHERE leagueId={$live->_lid}";
            $stmtleague = $db->query($league_sql);
            $league = $stmtleague->fetch(5);
            if (!empty($league)) {
                $result["data"]["league"] = $league;
            } else {
                $result["desc"] = "league not found";
            }
            $home_sql = "SELECT tid,id7m,{$teamlist[$lang]} name FROM lang_team WHERE tid={$live->hid}";
            $stmthome = $db->query($home_sql);
            $home = $stmthome->fetch(5);
            if (!empty($home)) {
                $result["data"]["team"]["home"] = $home;
            } else {
                $result["desc"] = "Home team not found";
            }
            $away_sql = "SELECT tid,id7m,{$teamlist[$lang]} name FROM lang_team WHERE tid={$live->gid}";
            $stmtaway = $db->query($away_sql);
            $away = $stmtaway->fetch(5);
            if (!empty($away)) {
                $result["data"]["team"]["away"] = $away;
            } else {
                $result["desc"] = "Away team not found";
            }

            $file = 'compare_team/' . $live->hid . '_' . $live->gid . '.json';
            $result["data"]["team"]["result"]["home"] = array();
            $result["data"]["team"]["result"]["away"] = array();
            if (file_exists($file)) {
                $homr = array();
                $awayr = array();
                $json = json_decode(file_get_contents($file), true);
                foreach ($json["result1"] as $r1) {
                    $homr[] = $r1->r;
                }
                $result["data"]["team"]["result"]["home"] = $homr;
                foreach ($json["result2"] as $r2) {
                    $awayr[] = $r2->r;
                }
                $result["data"]["team"]["result"]["away"] = $awayr;
            }

            $sql = "SELECT * FROM bet b
        LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
        WHERE mid=$mid";
            $stmt = $db->query($sql);
            $list = $stmt->fetchAll(5);
            if (!empty($list)) {
                $result["data"]["list"] = $list;
                $result["success"] = TRUE;
            } else {
                $result["data"]["list"] = array();
                $result["desc"] = "bet list not found";
            }
            $type = array("SBOBET", "Bet365", "Ladbrokes");
            $result["data"]["odds"] = array();
            foreach ($type as $t) {
                $type_sql = "SELECT * FROM odds_7m WHERE match_id=$mid AND type='$t'";
                $o7mstmt = $db->query($type_sql);
                $o7m = $o7mstmt->fetch(5);
                if (!empty($o7m)) {
                    $result["data"]["odds"] = $o7m;
                    break;
                }
            }
            if (empty($result["data"]["odds"])) {
                foreach ($type as $t) {
                    $type_sql = "SELECT * FROM odds_defined WHERE match_id=$mid AND type='$t'";
                    $o7mstmt = $db->query($type_sql);
                    $o7m = $o7mstmt->fetch(5);
                    if (!empty($o7m)) {
                        $result["data"]["odds"] = $o7m;
                        break;
                    }
                }
            }
        } else {

        }
    } else {
        $result["desc"] = "empty bet id";
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function getMatchCommentList()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $limit = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 10;
    $start = $offset * $limit;
    $leaguelist = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
    $teamlist = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
    $homelist = array("en" => "homeNameEn", "big" => "homeNameBig", "gb" => "homeNameGb", "th" => "homeNameTh", "kr" => "homeNameKr", "vn" => "homeNameVn", "la" => "homeNameLa");
    $awaylist = array("en" => "awayNameEn", "big" => "awayNameBig", "gb" => "awayNameGb", "th" => "awayNameTh", "kr" => "awayNameKr", "vn" => "awayNameVn", "la" => "awayNameLa");
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (!empty($fb_uid)) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        //get all user that know;
//        $friend_sql = "SELECT DISTINCT COALESCE(fl.fb_follow_uid,ff.friend_facebook_id) as id
//FROM  facebook_friends ff
//LEFT JOIN follow fl  ON fl.fb_uid = ff.facebook_id
//WHERE ff.facebook_id = '$fb_uid'";

        $friend_sql = "SELECT DISTINCT COALESCE(fl.fb_follow_uid,ff.friend_facebook_id) as id
FROM  facebook_friends ff
LEFT JOIN follow fl  ON fl.fb_uid = ff.facebook_id
WHERE ff.facebook_id = '$fb_uid' GROUP BY id";
        $stmtfriend = $db->query($friend_sql);
        $userlist = $stmtfriend->fetchAll(5);
        $knowuser = "('$fb_uid'";
        foreach ($userlist as $user) {
            $knowuser .= ",'{$user->id}'";
        }
        $knowuser .= ")";
        //echo $knowuser;
        // end get user
        //start  collect Match
//        $match_sql = "SELECT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,ll.competitionId,lm.date,lm.s1 score, ll.{$leaguelist[$lang]} LeagueName,ht.{$teamlist[$lang]} HomeName,at.{$teamlist[$lang]} AwayName,ll.id7m,ht.id7m,at.id7m,o7.hdp,o7.hdp_home,o7.hdp_away
//FROM bet b
//LEFT JOIN live_match lm ON b.mid = lm.mid
//LEFT JOIN lang_league ll ON lm._lid=ll.leagueId
//LEFT JOIN lang_team ht ON lm.hid=ht.tid
//LEFT JOIN lang_team at ON lm.gid=at.tid
//LEFT JOIN odds_7m o7 ON o7.league_id=ll.id7m  AND o7.home_id=ht.id7m AND o7.away_id=at.id7m
//WHERE b.fb_uid IN $knowuser
//AND o7.type=(CASE  WHEN o7.type='SBOBET' IS NOT NULL THEN 'SBOBET' WHEN o7.type= 'Bet365' IS NOT NULL THEN  'Bet365'  ELSE 'Ladbrokes' END)
//GROUP BY lm.mid
//ORDER BY b.betDatetime DESC
//LIMIT $start,$limit
//";

        $hdptype = array('SBOBET', 'Bet365', 'Ladbrokes');
        $matches = null;
        foreach ($hdptype as $htype) {
            $match_sql = "SELECT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,ll.competitionId,lm.date,lm.s1 score, ll.{$leaguelist[$lang]} LeagueName,ht.{$teamlist[$lang]} HomeName,at.{$teamlist[$lang]} AwayName,ll.id7m,ht.id7m,at.id7m,o7.hdp,o7.hdp_home,o7.hdp_away
FROM bet b
LEFT JOIN live_match lm ON b.mid = lm.mid
LEFT JOIN lang_league ll ON lm._lid=ll.leagueId
LEFT JOIN lang_team ht ON lm.hid=ht.tid
LEFT JOIN lang_team at ON lm.gid=at.tid
LEFT JOIN odds_7m o7 ON o7.league_id=ll.id7m  AND o7.home_id=ht.id7m AND o7.away_id=at.id7m
WHERE b.fb_uid IN $knowuser
AND o7.type='$htype'
GROUP BY lm.mid
ORDER BY b.betDatetime DESC
LIMIT $start,$limit
";
            $match_sql = "SELECT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,lm.competitionId,lm.show_date date,lm.score, lm.{$leaguelist[$lang]} LeagueName,lm.{$homelist[$lang]} HomeName,lm.{$awaylist[$lang]} AwayName,lm.hdp,lm.hdp_home,lm.hdp_away
FROM timelines_game lm
LEFT JOIN bet b  ON b.mid = lm.mid
WHERE b.fb_uid IN $knowuser
AND lm.type='$htype'
GROUP BY lm.mid
ORDER BY b.betDatetime DESC
LIMIT $start,$limit";
            $stmtmatch = $db->query($match_sql);
            $matches = $stmtmatch->fetchAll(5);
            if ($matches) {
                $result['matchsql'] = $match_sql;
                break;
            }
        }
        // var_dump($matches);
        //end get match
        //start collect comment
        $mlist = array();
        foreach ($matches as $key => $match) {
            //collect from know
//            $know_sql = "
//SELECT cm.* FROM (
//(
//SELECT fb.user_status displayname, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid IN  $knowuser
//AND b.match_id={$match->mid}
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.like DESC,b.id
//)
//UNION ALL
//(
//SELECT fb.user_status displayname, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid NOT IN  $knowuser
//AND b.match_id={$match->mid}
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.id
//)
//) AS cm
//
//LIMIT 5";

            $commentdate = strtotime($match->date . "-3 day");
            $know_sql = "SELECT fb.user_status displayname, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$match->mid}
AND b.message IS NOT NULL
AND b.message <>''
AND b.comment_at > $commentdate
ORDER BY  (CASE WHEN b.fb_uid IN  $knowuser  then 0 ELSE 1 END),b.id
LIMIT 5
";


            $cmtstmt = $db->query($know_sql);
            $commentlist = $cmtstmt->fetchAll(5);
            $result["data"][$key]["match"] = $match;
            $result["data"][$key]["betlist"] = $commentlist;

            $count_sql = "SELECT b.fb_uid,fb.user_status displayname,b.choose FROM bet b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE b.mid={$match->mid}";
            $countstmt = $db->query($count_sql);
            $betList = $countstmt->fetchAll(5);
            $beton = array("home" => array("list" => array(), "count" => 0), "draw" => array("list" => array(), "count" => 0), "away" => array("list" => array(), "count" => 0));
            foreach ($betList as $bet) {
                $beton[$bet->choose]["count"]++;
                $beton[$bet->choose]["list"][] = $bet;
            }
            $result["data"][$key]["beton"] = $beton;

            $result["data"][$key]["logos"] = array();
            $homelogo_sql = "SELECT * FROM team_logos WHERE tid={$match->hid}";
            $hlstmt = $db->query($homelogo_sql);
            $homelogo = $hlstmt->fetch(5);
            if (empty($homelogo)) {
                $dmn = getDomainName();
                $newinsertsql = "INSERT INTO team_logos (`tid`,`32x32`,`64x64`,`256x256`,`update_at`) VALUES ({$match->hid},'$dmn/teams_clean/team_default_32x32.png','$dmn/teams_clean/team_default_64x64.png','$dmn/teams_clean/team_default_256x256.png',NOW())";
                $db->exec($newinsertsql);
                $homelogo_sql = "SELECT * FROM team_logos WHERE tid={$match->hid}";
                $hlstmt = $db->query($homelogo_sql);
                $homelogo = $hlstmt->fetch(5);
            }
            $result["data"][$key]["logos"][$homelogo->tid] = $homelogo;

            $awaylogo_sql = "SELECT * FROM team_logos WHERE tid={$match->gid}";
            $alstmt = $db->query($awaylogo_sql);
            $awaylogo = $alstmt->fetch(5);
            if (empty($awaylogo)) {
                $dmn = getDomainName();
                $newinsertsql = "INSERT INTO team_logos (`tid`,`32x32`,`64x64`,`256x256`,`update_at`) VALUES ({$match->gid},'$dmn/teams_clean/team_default_32x32.png','$dmn/teams_clean/team_default_64x64.png','$dmn/teams_clean/team_default_256x256.png',NOW())";
                $db->exec($newinsertsql);
                $awaylogo_sql = "SELECT * FROM team_logos WHERE tid={$match->gid}";
                $alstmt = $db->query($awaylogo_sql);
                $awaylogo = $hlstmt->fetch(5);
            }
            $result["data"][$key]["logos"][$awaylogo->tid] = $awaylogo;
        }
        $result["success"] = true;
        //querylog(__FUNCTION__, $db);
    }

    echo json_encode($result);
}

function matchCommentList($fb_uid)
{
    $lang = "en";
    $offset = 0;
    $limit = 10;
    $start = $offset * $limit;
    $leaguelist = array("en" => "leagueNameEn", "big" => "leagueNameBig", "gb" => "leagueNameGb", "th" => "leagueNameTh", "kr" => "leagueNameKr", "vn" => "leagueNameVn", "la" => "leagueNameLa");
    $teamlist = array("en" => "teamNameEn", "big" => "teamNameBig", "gb" => "teamNameGb", "th" => "teamNameTh", "kr" => "teamNameKr", "vn" => "teamNameVn", "la" => "teamNameLa");
    $homelist = array("en" => "homeNameEn", "big" => "homeNameBig", "gb" => "homeNameGb", "th" => "homeNameTh", "kr" => "homeNameKr", "vn" => "homeNameVn", "la" => "homeNameLa");
    $awaylist = array("en" => "awayNameEn", "big" => "awayNameBig", "gb" => "awayNameGb", "th" => "awayNameTh", "kr" => "awayNameKr", "vn" => "awayNameVn", "la" => "awayNameLa");
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (!empty($fb_uid)) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        //get all user that know;
//        $friend_sql = "SELECT DISTINCT COALESCE(fl.fb_follow_uid,ff.friend_facebook_id) as id
//FROM  facebook_friends ff
//LEFT JOIN follow fl  ON fl.fb_uid = ff.facebook_id
//WHERE ff.facebook_id = '$fb_uid'";

        $friend_sql = "SELECT DISTINCT COALESCE(fl.fb_follow_uid,ff.friend_facebook_id) as id
FROM  facebook_friends ff
LEFT JOIN follow fl  ON fl.fb_uid = ff.facebook_id
WHERE ff.facebook_id = '$fb_uid' GROUP BY id";
        $stmtfriend = $db->query($friend_sql);
        $userlist = $stmtfriend->fetchAll(5);
        $knowuser = "('$fb_uid'";
        foreach ($userlist as $user) {
            $knowuser .= ",'{$user->id}'";
        }
        $knowuser .= ")";
        //echo $knowuser;
        // end get user
        //start  collect Match
        $match_sql = "SELECT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,ll.competitionId,lm.date,lm.s1 score, ll.{$leaguelist[$lang]} LeagueName,ht.{$teamlist[$lang]} HomeName,at.{$teamlist[$lang]} AwayName,ll.id7m,ht.id7m,at.id7m,o7.hdp,o7.hdp_home,o7.hdp_away
FROM bet b
LEFT JOIN live_match lm ON b.mid = lm.mid
LEFT JOIN lang_league ll ON lm._lid=ll.leagueId
LEFT JOIN lang_team ht ON lm.hid=ht.tid
LEFT JOIN lang_team at ON lm.gid=at.tid
LEFT JOIN odds_7m o7 ON o7.league_id=ll.id7m  AND o7.home_id=ht.id7m AND o7.away_id=at.id7m
WHERE b.fb_uid IN $knowuser
AND o7.type=(CASE  WHEN o7.type='SBOBET' IS NOT NULL THEN 'SBOBET' WHEN o7.type= 'Bet365' IS NOT NULL THEN  'Bet365'  ELSE 'Ladbrokes' END)
GROUP BY lm.mid
ORDER BY b.betDatetime DESC
LIMIT $start,$limit
";

        $match_sql = "SELECT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,lm.competitionId,lm.show_date date,lm.score, lm.{$leaguelist[$lang]} LeagueName,lm.{$homelist[$lang]} HomeName,lm.{$awaylist[$lang]} AwayName,lm.hdp,lm.hdp_home,lm.hdp_away
FROM timelines_game lm
LEFT JOIN bet b  ON b.mid = lm.mid
WHERE b.fb_uid IN $knowuser
GROUP BY lm.mid
ORDER BY b.betDatetime DESC
LIMIT $start,$limit";

        $stmtmatch = $db->query($match_sql);
        $matches = $stmtmatch->fetchAll(5);
        // var_dump($matches);
        //end get match
        //start collect comment
        $mlist = array();
        foreach ($matches as $key => $match) {
            //collect from know
//            $know_sql = "
//SELECT cm.* FROM (
//(
//SELECT fb.user_status displayname, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid IN  $knowuser
//AND b.match_id={$match->mid}
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.like DESC,b.id
//)
//UNION ALL
//(
//SELECT fb.user_status displayname, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid NOT IN  $knowuser
//AND b.match_id={$match->mid}
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.id
//)
//) AS cm
//
//LIMIT 5";

            $commentdate = strtotime($match->date . "-3 day");
            $know_sql = "SELECT fb.user_status displayname, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$match->mid}
AND b.message IS NOT NULL
AND b.message <>''
AND b.comment_at > $commentdate
ORDER BY  (CASE WHEN b.fb_uid IN  $knowuser  then 0 ELSE 1 END),b.id
LIMIT 5
";

            $cmtstmt = $db->query($know_sql);
            $commentlist = $cmtstmt->fetchAll(5);
            $result["data"][$key]["match"] = $match;
            $result["data"][$key]["betlist"] = $commentlist;

            $count_sql = "SELECT b.fb_uid,fb.user_status displayname,b.choose FROM bet b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE b.mid={$match->mid}";
            $countstmt = $db->query($count_sql);
            $betList = $countstmt->fetchAll(5);
            $beton = array("home" => array("list" => array(), "count" => 0), "draw" => array("list" => array(), "count" => 0), "away" => array("list" => array(), "count" => 0));
            foreach ($betList as $bet) {
                $beton[$bet->choose]["count"]++;
                $beton[$bet->choose]["list"][] = $bet;
            }
            $result["data"][$key]["beton"] = $beton;

            $result["data"][$key]["logos"] = array();
            $homelogo_sql = "SELECT * FROM team_logos WHERE tid={$match->hid}";
            $hlstmt = $db->query($homelogo_sql);
            $homelogo = $hlstmt->fetch(5);
            if (empty($homelogo)) {
                $dmn = getDomainName();
                $newinsertsql = "INSERT INTO team_logos (`tid`,`32x32`,`64x64`,`256x256`,`update_at`) VALUES ({$match->hid},'$dmn/teams_clean/team_default_32x32.png','$dmn/teams_clean/team_default_64x64.png','$dmn/teams_clean/team_default_256x256.png',NOW())";
                $db->exec($newinsertsql);
                $homelogo_sql = "SELECT * FROM team_logos WHERE tid={$match->hid}";
                $hlstmt = $db->query($homelogo_sql);
                $homelogo = $hlstmt->fetch(5);
            }
            $result["data"][$key]["logos"][$homelogo->tid] = $homelogo;

            $awaylogo_sql = "SELECT * FROM team_logos WHERE tid={$match->gid}";
            $alstmt = $db->query($awaylogo_sql);
            $awaylogo = $alstmt->fetch(5);
            if (empty($awaylogo)) {
                $dmn = getDomainName();
                $newinsertsql = "INSERT INTO team_logos (`tid`,`32x32`,`64x64`,`256x256`,`update_at`) VALUES ({$match->gid},'$dmn/teams_clean/team_default_32x32.png','$dmn/teams_clean/team_default_64x64.png','$dmn/teams_clean/team_default_256x256.png',NOW())";
                $db->exec($newinsertsql);
                $awaylogo_sql = "SELECT * FROM team_logos WHERE tid={$match->gid}";
                $alstmt = $db->query($awaylogo_sql);
                $awaylogo = $hlstmt->fetch(5);
            }
            $result["data"][$key]["logos"][$awaylogo->tid] = $awaylogo;
        }
        $result["success"] = true;
        //querylog(__FUNCTION__, $db);
    }

    //echo json_encode($result);
    return $result;
}

function getMoreMatchCommentList()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    $parent = isset($_REQUEST["parent"]) ? $_REQUEST["parent"] : "";
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $range = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 5;
    $stop = ($offset * $range) + $range;
    $result = array("success" => false, "desc" => "nothing happen", "data" => array());
    if (!empty($fb_uid)) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        //get all user that know;
        $friend_sql = "SELECT DISTINCT COALESCE(fl.fb_follow_uid,ff.friend_facebook_id) as id
FROM  facebook_friends ff
LEFT JOIN follow fl  ON fl.fb_uid = ff.facebook_id
WHERE ff.facebook_id = '$fb_uid'";
        $stmtfriend = $db->query($friend_sql);
        $userlist = $stmtfriend->fetchAll(5);
        $knowuser = "('$fb_uid'";
        foreach ($userlist as $user) {
            $knowuser .= ",'{$user->id}'";
        }
        $knowuser .= ")";

        //collect from know
//        $know_sql = "SELECT * FROM
//(
//(
//SELECT fb.user_status displayname, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid IN  $knowuser
//AND b.match_id=$mid
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.like DESC,b.id DESC
//)
//UNION
//(
//SELECT fb.user_status displayname,b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE b.fb_uid NOT IN  $knowuser
//AND b.match_id=$mid
//AND b.message IS NOT NULL
//AND b.message <>''
//ORDER BY b.id DESC
//)
//) as cm
//LIMIT 0,$stop";
        $optionalcon = "";
        if (!empty($parent)) {
            $optionalcon = "AND parent_id='$parent'";
        }

        $know_sql = "SELECT fb.user_status displayname, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id=$mid
AND b.remove='N'
AND (b.message IS NOT NULL OR b.picture IS NOT NULL)
AND (b.message <>'' OR b.picture<>'')
$optionalcon
ORDER BY  (CASE WHEN b.fb_uid IN  $knowuser  then 0 ELSE 1 END),b.id
LIMIT 0,$stop
";

        $cmtstmt = $db->query($know_sql);
        $commentlist = $cmtstmt->fetchAll(5);
        $result["data"]["betlist"] = $commentlist;
        $count_sql = "SELECT b.fb_uid,fb.user_status displayname,b.choose FROM bet b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE b.mid=$mid";
        $countstmt = $db->query($count_sql);
        $betList = $countstmt->fetchAll(5);
        $beton = array("home" => array("list" => array(), "count" => 0), "draw" => array("list" => array(), "count" => 0), "away" => array("list" => array(), "count" => 0));
        foreach ($betList as $bet) {
            $beton[$bet->choose]["count"]++;
            $beton[$bet->choose]["list"][] = $bet;
        }
        $result["data"]["beton"] = $beton;
        $result["success"] = true;
        //querylog(__FUNCTION__, $db);
    }

    echo json_encode($result);
}

function timeTest()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $param = isset($_REQUEST["param"]) ? $_REQUEST["param"] : "";

    $sql = "SELECT fb_access_token FROM facebook_user WHERE fb_uid='$param'";
    $stmt = $db->query($sql);
    $token = $stmt->fetch();
    $facebook = new Facebook(array(
        'appId' => '400809029976308',
        'secret' => '93e21cf1551a4536d20843fcdf236402',
    ));
    if (!empty($token[0])) {
        $facebook->setAccessToken($token[0]);
        try {
            $permissions = $facebook->api("/me/permissions");
            var_dump($permissions);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
}

function matchRewrite()
{
    $mid = isset($_REQUEST["mid"]) ? $_REQUEST["mid"] : "";
    writeMatchComment($mid);
    echo file_get_contents('matches_comment/' . $mid . '.json');
}

function matchonfile($id)
{
    writeMatchComment($id);
    $i = rand(1, 10);
    if (file_exists('matches_comment/' . $id . '_' . $i . '.json')) {
        echo file_get_contents('matches_comment/' . $id . '_' . $i . '.json');
    } else {
        echo file_get_contents('matches_comment/' . $id . '.json');
    }
}

function writeMatchComment($mid)
{
    //$mid = $_REQUEST["mid"];
    $result = array("betlist" => array(), "like" => array(), "reply" => array(), "beton" => array(), 'file' => 0);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "SELECT cm.*,fb.user_status as displayname,fb.display_name,fb.fb_firstname FROM comment_on_match cm LEFT JOIN facebook_user fb ON fb.fb_uid=cm.fb_uid WHERE match_id='$mid' AND remove='N' ORDER BY id";

    $stmt = $db->query($sql);
    $commentlist = $stmt->fetchAll(PDO::FETCH_OBJ);
    $result["reply"] = array();
    foreach ($commentlist as $comment) {
        if ((int)$comment->parent_id == 0) {
            $result["betlist"][] = $comment;
        } else {
            $result["reply"][$comment->parent_id][] = $comment;
        }
    }

    foreach ($commentlist as $comment) {
        $sql = "SELECT mcl.fb_uid,fb.fb_name,fb.user_status as displayname,fb.display_name,fb.fb_firstname FROM match_comment_like mcl
                JOIN facebook_user fb on mcl.fb_uid = fb.fb_uid
                WHERE comment_id={$comment->id} AND status='like'";
        $stmt = $db->query($sql);
        $likelist = $stmt->fetchAll(PDO::FETCH_OBJ);
        $result["like"][$comment->id] = $likelist;
    }

    $count_sql = "SELECT b.fb_uid,fb.user_status displayname,fb.display_name,fb.fb_firstname,b.choose FROM bet b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE b.mid=$mid";
    $countstmt = $db->query($count_sql);
    $betList = $countstmt->fetchAll(5);
    $beton = array("home" => array("list" => array(), "count" => 0), "draw" => array("list" => array(), "count" => 0), "away" => array("list" => array(), "count" => 0));
    foreach ($betList as $bet) {
        $beton[$bet->choose]["count"]++;
        $beton[$bet->choose]["list"][] = $bet;
    }
    $result["beton"] = $beton;

    //echo json_encode($result);
    //querylog(__FUNCTION__, $db);
    file_put_contents('matches_comment/' . $mid . '.json', json_encode($result));
    //chmod('matches_comment/' . $mid . '.json', 0777);
    for ($i = 1; $i < 10; $i++) {
        $result['file'] = $i;
        file_put_contents('matches_comment/' . $mid . "_" . $i . '.json', json_encode($result));
        //chmod('matches_comment/' . $mid . "_" . $i . '.json', 0777);
    }
}

function setMind()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid) && !empty($message)) {
        $db->exec("UPDATE facebook_user SET `mind`='$message' WHERE fb_uid=$fb_uid");
//        $sql_json = "select *from facebook_user where fb_uid='$fb_uid'";
//        $stmt = $db->query($sql_json);
//        $obj = $stmt->fetch(PDO::FETCH_OBJ);
//        foreach ($obj as $key => $value) {
//            if ($value == NULL) {
//                $obj->$key = "";
//            }
//        }
//        file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//        chmod('facebook_info/' . $fb_uid . '.json', 0777);
        FBtofile($fb_uid);
    }
    echo file_get_contents('facebook_info/' . $fb_uid . '.json');
}

function setSite()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $site = isset($_REQUEST["site"]) ? $_REQUEST["site"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid) && !empty($site)) {
        $db->exec("UPDATE facebook_user SET `site`='$site' WHERE fb_uid=$fb_uid");
//        $sql_json = "select *from facebook_user where fb_uid='$fb_uid'";
//        $stmt = $db->query($sql_json);
//        $obj = $stmt->fetch(PDO::FETCH_OBJ);
//        foreach ($obj as $key => $value) {
//            if ($value == NULL) {
//                $obj->$key = "";
//            }
//        }
//        file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//        chmod('facebook_info/' . $fb_uid . '.json', 0777);
        FBtofile($fb_uid);
    }
    echo file_get_contents('facebook_info/' . $fb_uid . '.json');
}

function setCover()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $picture = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid) && !empty($picture)) {
        $db->exec("UPDATE facebook_user SET `cover`='$picture' WHERE fb_uid=$fb_uid");
//        $sql_json = "select *from facebook_user where fb_uid='$fb_uid'";
//        $stmt = $db->query($sql_json);
//        $obj = $stmt->fetch(PDO::FETCH_OBJ);
//        foreach ($obj as $key => $value) {
//            if ($value == NULL) {
//                $obj->$key = "";
//            }
//        }
//        file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//        chmod('facebook_info/' . $fb_uid . '.json', 0777);
        FBtofile($fb_uid);
    }

    echo file_get_contents('facebook_info/' . $fb_uid . '.json');
}

function setDisplayName()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $name = isset($_REQUEST["name"]) ? $_REQUEST["name"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");

    if (!empty($fb_uid) && !empty($name)) {
        if (preg_match("|\bADMIN\b|i", $name)) {

        } else {
            $db->exec("UPDATE facebook_user SET `display_name`='$name',`user_status`='$name' WHERE fb_uid=$fb_uid");
//        $sql_json = "select *from facebook_user where fb_uid='$fb_uid'";
//        $stmt = $db->query($sql_json);
//        $obj = $stmt->fetch(PDO::FETCH_OBJ);
//        foreach ($obj as $key => $value) {
//            if ($value == NULL) {
//                $obj->$key = "";
//            }
//        }
//        file_put_contents('facebook_info/' . $fb_uid . '.json', json_encode($obj));
//        chmod('facebook_info/' . $fb_uid . '.json', 0777);
            FBtofile($fb_uid);
        }
    }
    echo file_get_contents('facebook_info/' . $fb_uid . '.json');
}

function FileMan()
{
    $file = @$_FILES["file"];
    //var_dump($file);
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "unknow";
    $rootpath = "http://api.ssporting.com";
    $path = "uploaded/$fb_uid";
    $results = array("success" => false, "desc" => "nothing happen", "path" => "", "thumbnail" => "");
//echo $file["name"] . ":" . $fb_uid;


    if (is_dir($path)) {

    } else {
        mkdir($path);
        chmod($path, 0777);
    }


    //$allowedExts = array("gif", "jpeg", "jpg", "png");
    //$temp = explode(".", $file["name"]);
    //$extension = end($temp);
    $x = rand(0, 99);
    if ($file["size"] < 5000000) {
        if ($_FILES["file"]["error"] > 0) {
            //echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
            $results["desc"] .= "=>" . $file["error"];
        } else {
            $day = date("Y-m-d h:i:s");
            $now = strtotime($day);
            $tmppath = $path . "/" . $fb_uid . $now . "tmp" . $x . ".png";
            $fullpath = $path . "/" . $fb_uid . $now . "x" . $x . ".png";
            $thumbnailpath = $path . "/thumbnail_" . $fb_uid . $now . "x" . $x . ".png";
            if (file_exists($fullpath)) {
                //echo $file["name"] . " already exists. ";
                $results["desc"] .= "=>already exists";
                $results["path"] = "$rootpath/" . $fullpath;
            } else {
                move_uploaded_file($file["tmp_name"], $fullpath);
                //chown($fullpath, "dev");
                //chmod($tmppath, 0777);
                try {
                    $fimg = new Imagick();
                    $fimg->readimage($fullpath);
                    $fimg->scaleimage(500, 0);
                    $im->setImageFormat("png");
                    $fimg->writeimage($fullpath);
                    //$fimg->destroy();
                } catch (Exception $e) {
                    // echo $e->getMessage();
                    $results["desc"] .= "=>" . $e->getMessage();
                }
                //exec("pwd");
                //shell_exec("/usr/bin/mogrify -resize 500x $fullpath");

                $imgdetails = getimagesize($fullpath);
                if ($imgdetails) {
                    copy($fullpath, $thumbnailpath);
                    try {
                        $timg = new Imagick();
                        $timg->readimage($thumbnailpath);
                        $timg->scaleimage(200, 0);
                        $im->setImageFormat("png");
                        $timg->writeimage($thumbnailpath);
                        //$timg->destroy();
                    } catch (Exception $e) {
                        // echo $e->getMessage();
                        $results["desc"] .= "=>" . $e->getMessage();
                    }
                    //shell_exec("cp $fullpath $thumbnailpath");
                    //chown($thumbnailpath, "dev");
                    //chmod($thumbnailpath, 0777);
                    //shell_exec("/usr/bin/mogrify -resize 200x $thumbnailpath");
                    //chown($fullpath, "www-data");
                    //chown($thumbnailpath, "www-data");
                    //resize($fullpath, $thumbnailpath, 200);
                    // echo "Stored in: " . "$path/" . $file["name"];
                    $results["success"] = true;
                    $results["desc"] = "completed";
                    $results["path"] = "$rootpath/" . $fullpath;
                    $results["thumbnail"] = "$rootpath/" . $thumbnailpath;
                } else {
                    unlink($fullpath);
                    $results["desc"] .= "=>file not image.";
                }
            }
        }
    } else {
        //echo "Invalid file";
        $results["desc"] .= "=>Invalid file :" . $file["type"] . "|" . $file["size"];
    }
    echo json_encode($results);
}

function resize($path, $newPath, $max)
{
    //$width=200; //*** Fix Width & Heigh (Autu caculate) ***//
    $size = GetimageSize($path);
    $_width = $size[0];
    $_height = $size[1];
    $mime = $size['mime'];
    $width = null;
    $height = null;
    if ($_width > $_height) {
        $width = $max;
        $height = round($_height * ($max / $_width));
    } else {
        $height = $max;
        $width = round($_width * ($max / $_height));
    }
    //$height=round($width*$size[1]/$size[0]);
    $new_image_ext = null;
    switch ($mime) {
        case 'image/jpeg':
            $image_create_func = 'imagecreatefromjpeg';
            $image_save_func = 'imagejpeg';
            $new_image_ext = 'jpg';
            break;

        case 'image/png':
            $image_create_func = 'imagecreatefrompng';
            $image_save_func = 'imagepng';
            $new_image_ext = 'png';
            break;

        case 'image/gif':
            $image_create_func = 'imagecreatefromgif';
            $image_save_func = 'imagegif';
            $new_image_ext = 'gif';
            break;

        default:
            throw Exception('Unknown image type.');
    }
    if ($new_image_ext == 'jpg') {
        $images_orig = ImageCreateFromJPEG($path);
    } else if ($new_image_ext == 'png') {
        $images_orig = ImageCreateFromPNG($path);
    } else if ($new_image_ext == 'gif') {
        $images_orig = ImageCreateFromGIF($path);
    }

    $photoX = ImagesX($images_orig);
    $photoY = ImagesY($images_orig);
    $images_fin = ImageCreateTrueColor($width, $height);

    //ImageJPEG($images_fin, $newPath);
    if ($new_image_ext == 'jpg') {
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageJPEG($images_fin, $newPath);
    } else if ($new_image_ext == 'png') {
        imagealphablending($images_fin, false);
        imagesavealpha($images_fin, true);
        $transparent = imagecolorallocatealpha($images_fin, 255, 255, 255, 127);
        imagefilledrectangle($images_fin, 0, 0, $width, $height, $transparent);
        imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width, $height, $photoX, $photoY);
        ImagePNG($images_fin, $newPath);
    } else if ($new_image_ext == 'gif') {
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width + 1, $height + 1, $photoX, $photoY);
        ImageGIF($images_fin, $newPath);
    }
    ImageDestroy($images_orig);
    ImageDestroy($images_fin);
}

function UpdateWallStatus()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
    $pix = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    $video = isset($_REQUEST['video']) ? $_REQUEST['video'] : "";
    $gallname = isset($_REQUEST["gall_name"]) ? $_REQUEST["gall_name"] : "";
    $galltype = isset($_REQUEST["gall_type"]) ? $_REQUEST["gall_type"] : "picture";
    $title = isset($_REQUEST['title']) ? $_REQUEST['title'] : "";
    $topic = isset($_REQUEST['category']) ? $_REQUEST['category'] : 9;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $day = date("Y-m-d H:i:s");
    $stamp = strtotime($day);
    $videos = array();
    $images = array();
    $message = addslashes($message);
    if (empty($gallname))
        $gallname = $stamp;
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    if (!empty($fb_uid)) {
        if (!empty($pix) || !empty($video)) {
            //   echo "pic";
            $gallsql = "SELECT * FROM media_gallery WHERE fb_uid='$fb_uid' AND gall_name='$gallname' AND gall_type='$galltype' LIMIT 1";
            $gstmt = $db->query($gallsql);
            $gallery = $gstmt->fetch(5);
            if (empty($gallery)) {
                $ginsertsql = "INSERT INTO media_gallery (`fb_uid`,`gall_name`,`gall_type`,`created_at`,`desc`,`title`) VALUES ('$fb_uid','$gallname','$galltype',NOW(),'$message','$title')";
                $db->exec($ginsertsql);
                $gallsql = "SELECT * FROM media_gallery WHERE fb_uid='$fb_uid' AND gall_name='$gallname' AND gall_type='$galltype' LIMIT 1";
                $gstmt = $db->query($gallsql);
                $gallery = $gstmt->fetch(5);
            }


            if (!empty($video)) {
                $pixinsert = "INSERT INTO media_store (`gall_id`,`path`,`thumbnail`,`created_at`,`desc`) VALUES ('{$gallery->id}','$video','$video',NOW(),'$message')";
                $db->exec($pixinsert);
                $lastid = $db->lastInsertId();
                $lastimg = "SELECT * FROM media_store WHERE id=$lastid";
                $limgstmt = $db->query($lastimg);
                $videos = $limgstmt->fetch(5);
            }


            if (!empty($pix)) {
                $pixlist = explode(",", $pix);

                $keylist = "";
                //var_dump($pixlist);
                foreach ($pixlist as $img) {

                    $imgs = explode("|", $img);
                    //var_dump($imgs);
                    $pixinsert = "";
                    if (count($imgs) == 2) {
                        $pixinsert = "INSERT INTO media_store (`gall_id`,`path`,`thumbnail`,`created_at`,`desc`) VALUES ('{$gallery->id}','{$imgs[0]}','{$imgs[1]}',NOW(),'$message')";
                        $db->exec($pixinsert);
                        $lastid = $db->lastInsertId();
                        // echo $lastid;
                        if (!empty($keylist)) {
                            $keylist .= "," . $lastid;
                        } else {
                            $keylist .= $lastid;
                        }
                        //echo $keylist . "\n";
                        $lastimg = "SELECT * FROM media_store WHERE id=$lastid";
                        $limgstmt = $db->query($lastimg);
                        $images[] = $limgstmt->fetch(5);
                    }
                }
            }
            $updategall = "UPDATE `media_gallery` SET `update_at`='$day', `stamp_at`='$stamp' WHERE  `id`='{$gallery->id}';";
            $db->exec($updategall);
            $content = json_encode(array("gallery" => $gallery, "images" => $images, "videos" => $videos));
            if ($galltype == 'picture') {
                $galltype = 'image';
            }
            $results = TimelineTrigger($fb_uid, "$galltype", $gallery->id, $topic);
        } else if (!empty($message)) {
            //  echo "msg";
            $newstatus = "INSERT INTO wall_status (`fb_uid`,`message`,`comment_at`,`updated_at`,`stamp_at`) VALUES ('$fb_uid','$message','$day','$day','$stamp')";
            $db->exec($newstatus);
            $key = $db->lastInsertId();
            $lastmsg = "SELECT * FROM wall_status WHERE id=$key";
            $msgstmt = $db->query($lastmsg);
            $msg = $msgstmt->fetch(5);
            $content = json_encode($msg);
            // echo $content;
            $results = TimelineTrigger($fb_uid, "status", $key, $topic);
        }
        $user = "SELECT fb_name,fb_access_token FROM facebook_user WHERE fb_uid=$fb_uid";
        $stmt = $db->query($user);
        $token = $stmt->fetch();
        //var_dump($token[0]);
        //$results["postable"] = fbPoster($token["fb_access_token"], "{$token["fb_name"]} has activity on scorspot.com", "http://scorspot.com/", "http://ws.1ivescore.com/teams_clean/team_default_256x256.png");
//        regEvent($db,$fb_uid, 'post', 'ได้โพสต์ข้อความบนไทม์ไลน์', "$message", 'timeline', $key);
    }

    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function UpdateTimelineContent()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "all";
    if ($type == "all" || $type == "status") {
        $sql = "SELECT *
FROM timelines tl
WHERE tl.content_type = 'status'";
        $stmt = $db->query($sql);
        $timelines = $stmt->fetchAll(5);
        foreach ($timelines as $wall) {
            $subsql = "SELECT *
FROM wall_status ws
WHERE ws.id ={$wall->key_list}";
            $substmt = $db->query($subsql);
            $msg = $substmt->fetch(5);
            $content = json_encode($msg);
            $upsql = "UPDATE timelines SET content='$content' WHERE id={$wall->id}";
            $db->exec($upsql);
            echo $wall->id . ":" . $wall->content_type . "\n";
        }
    }

    if ($type == 'all' || $type == 'game') {
        $sql = "SELECT *
FROM timelines tl
WHERE tl.content_type = 'game'";
        $stmt = $db->query($sql);
        $timelines = $stmt->fetchAll(5);
        foreach ($timelines as $wall) {
            $subsql = "SELECT *
FROM wall_status ws
WHERE ws.id ={$wall->key_list}";
            $substmt = $db->query($subsql);
            $msg = $substmt->fetch(5);
            $content = json_encode($msg);
            $upsql = "UPDATE timelines SET content='$content' WHERE id={$wall->id}";
            $db->exec($upsql);
            echo $wall->id . ":" . $wall->content_type . "\n";
        }

        $gamesql = "SELECT DISTINCT lm.mid,lm._lid,lm.hid,lm.gid,lm.sid,ll.competitionId,lm.date,lm.s1 score, ll.leagueNameEn LeagueName,ht.teamNameEn HomeName,at.teamNameEn AwayName,ll.id7m,ht.id7m,at.id7m,o7.hdp,o7.hdp_home,o7.hdp_away,o7.type,o7.match_id
FROM live_match lm
LEFT JOIN lang_league ll ON lm._lid=ll.leagueId
LEFT JOIN lang_team ht ON lm.hid=ht.tid
LEFT JOIN lang_team at ON lm.gid=at.tid
LEFT JOIN odds_7m o7 ON o7.league_id=ll.id7m  AND o7.home_id=ht.id7m AND o7.away_id=at.id7m
WHERE lm.mid=$mid
AND o7.type=(CASE  WHEN o7.type='SBOBET' IS NOT NULL THEN 'SBOBET' WHEN o7.type= 'Bet365' IS NOT NULL THEN  'Bet365'  ELSE 'Ladbrokes' END)
";
        $gamestmt = $db->query($gamesql);
        $game = $gamestmt->fetch(5);
        //querylog(__FUNCTION__, $db);
        $content = json_encode($game);
        if (!empty($game))
            $rs = TimelineTrigger($fb_uid, "game", $mid);
    }
}

function gameToTimeline($mid)
{
    $mid = (int)$mid;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "INSERT INTO `timelines_game` (`mid`, `_lid`, `hid`, `gid`, `sid`, `competitionId`, `show_date`, `score`, `leagueName`, `leagueNameEn`, `leagueNameTh`, `leagueNameBig`, `leagueNameGb`, `leagueNameKr`, `leagueNameVn`, `leagueNameLa`, `homeName`, `homeNameEn`, `homeNameTh`, `homeNameBig`, `homeNameGb`, `homeNameKr`, `homeNameVn`, `homeNameLa`, `awayName`, `awayNameEn`, `awayNameTh`, `awayNameBig`, `awayNameGb`, `awayNameKr`, `awayNameVn`, `awayNameLa`, `hdp`, `hdp_home`, `hdp_away`, `type`, `match_id`, `h32x32`, `h64x64`, `h256x256`, `a32x32`, `a64x64`, `a256x256`)
SELECT  lm.mid, lm._lid, lm.hid, lm.gid, lm.sid, ll.competitionId, lm.date, lm.s1 score, ll.leagueName LeagueName,ll.leagueNameEn LeagueNameEn, ll.leagueNameTh LeagueNameTh,ll.leagueNameBig LeagueNameBig,ll.leagueNameGb LeagueNameGb,ll.leagueNameKr LeagueNameKr,ll.leagueNameVn LeagueNameVn,ll.leagueNameLa LeagueNameLa, ht.teamName HomeName,ht.teamNameEn HomeNameEn,ht.teamNameTh HomeNameTh,ht.teamNameBig HomeNameBig,ht.teamNameGb HomeNameGb,ht.teamNameKr HomeNameKr,ht.teamNameVn HomeNameVn,ht.teamNameLa HomeNameLa, at.teamName AwayName,at.teamNameEn AwayNameEn,at.teamNameTh AwayNameTh,at.teamNameBig AwayNameBig,at.teamNameGb AwayNameGb,at.teamNameKr AwayNameKr,at.teamNameVn AwayNameVn,at.teamNameLa AwayNameLa, o7.hdp, o7.hdp_home, o7.hdp_away, o7.type, o7.match_id,hlogos.32x32 h32x32,hlogos.64x64 h64x64,hlogos.256x256 h256x256,alogos.32x32 a32x32,alogos.64x64 a64x64,alogos.256x256 a256x256
FROM live_match lm
LEFT JOIN lang_league ll ON lm._lid = ll.leagueId
LEFT JOIN lang_team ht ON lm.hid = ht.tid
LEFT JOIN lang_team at ON lm.gid = at.tid
LEFT JOIN team_logos hlogos ON hlogos.tid=ht.tid
LEFT JOIN team_logos alogos ON alogos.tid=at.tid
LEFT JOIN odds_7m o7 ON o7.league_id = ll.id7m
AND o7.home_id = ht.id7m
AND o7.away_id = at.id7m
WHERE
lm.mid =$mid
AND
lm.mid NOT IN (SELECT mid FROM timelines_game)
AND o7.type = (
CASE WHEN o7.type = 'SBOBET' IS NOT NULL
THEN 'SBOBET'
WHEN o7.type = 'Bet365' IS NOT NULL
THEN 'Bet365'
ELSE 'Ladbrokes'
END )";
    $db->exec($sql);
    //querylog(__FUNCTION__, $db);
//    $sqldup = "SELECT * FROM timelines_game WHERE mid=$mid";
//    $dupstmt = $db->query($sqldup);
//    $duplicated = $dupstmt->fetch(5);
//    if (empty($duplicated)) {
//        $sql = "
//SELECT  lm.mid, lm._lid, lm.hid, lm.gid, lm.sid, ll.competitionId, lm.date, lm.s1 score, ll.leagueName LeagueName,ll.leagueNameEn LeagueNameEn, ll.leagueNameTh LeagueNameTh,ll.leagueNameBig LeagueNameBig,ll.leagueNameGb LeagueNameGb,ll.leagueNameKr LeagueNameKr,ll.leagueNameVn LeagueNameVn,ll.leagueNameLa LeagueNameLa, ht.teamName HomeName,ht.teamNameEn HomeNameEn,ht.teamNameTh HomeNameTh,ht.teamNameBig HomeNameBig,ht.teamNameGb HomeNameGb,ht.teamNameKr HomeNameKr,ht.teamNameVn HomeNameVn,ht.teamNameLa HomeNameLa, at.teamName AwayName,at.teamNameEn AwayNameEn,at.teamNameTh AwayNameTh,at.teamNameBig AwayNameBig,at.teamNameGb AwayNameGb,at.teamNameKr AwayNameKr,at.teamNameVn AwayNameVn,at.teamNameLa AwayNameLa, o7.hdp, o7.hdp_home, o7.hdp_away, o7.type, o7.match_id,hlogos.32x32 h32x32,hlogos.64x64 h64x64,hlogos.256x256 h256x256,alogos.32x32 a32x32,alogos.64x64 a64x64,alogos.256x256 a256x256
//FROM live_match lm
//LEFT JOIN lang_league ll ON lm._lid = ll.leagueId
//LEFT JOIN lang_team ht ON lm.hid = ht.tid
//LEFT JOIN lang_team at ON lm.gid = at.tid
//LEFT JOIN team_logos hlogos ON hlogos.tid=ht.tid
//LEFT JOIN team_logos alogos ON alogos.tid=at.tid
//LEFT JOIN odds_7m o7 ON o7.league_id = ll.id7m
//AND o7.home_id = ht.id7m
//AND o7.away_id = at.id7m
//WHERE lm.mid =$mid
//AND o7.type = (
//CASE WHEN o7.type = 'SBOBET' IS NOT NULL
//THEN 'SBOBET'
//WHEN o7.type = 'Bet365' IS NOT NULL
//THEN 'Bet365'
//ELSE 'Ladbrokes'
//END )";
//        $gamestmt = $db->query($sql);
//        $tmpgames = $gamestmt->fetch(5);
//        if (!empty($tmpgames)) {
//            $insertsql = "INSERT INTO `timelines_game` (`mid`, `_lid`, `hid`, `gid`, `sid`, `competitionId`, `show_date`, `score`, `leagueName`, `leagueNameEn`, `leagueNameTh`, `leagueNameBig`, `leagueNameGb`, `leagueNameKr`, `leagueNameVn`, `leagueNameLa`, `homeName`, `homeNameEn`, `homeNameTh`, `homeNameBig`, `homeNameGb`, `homeNameKr`, `homeNameVn`, `homeNameLa`, `awayName`, `awayNameEn`, `awayNameTh`, `awayNameBig`, `awayNameGb`, `awayNameKr`, `awayNameVn`, `awayNameLa`, `hdp`, `hdp_home`, `hdp_away`, `type`, `match_id`, `h32x32`, `h64x64`, `h256x256`, `a32x32`, `a64x64`, `a256x256`) VALUES ('{$tmpgames->mid}', '{$tmpgames->_lid}', '{$tmpgames->hid}', '{$tmpgames->gid}', '{$tmpgames->sid}', '{$tmpgames->competitionId}', '{$tmpgames->date}', '{$tmpgames->score}', '{$tmpgames->LeagueName}', '{$tmpgames->LeagueNameEn}', '{$tmpgames->LeagueNameTh}', '{$tmpgames->LeagueNameBig}', '{$tmpgames->LeagueNameGb}', '{$tmpgames->LeagueNameKr}', '{$tmpgames->LeagueNameVn}', '{$tmpgames->LeagueNameLa}', '{$tmpgames->HomeName}', '{$tmpgames->HomeNameEn}', '{$tmpgames->HomeNameTh}', '{$tmpgames->HomeNameBig}', '{$tmpgames->HomeNameGb}', '{$tmpgames->HomeNameKr}', '{$tmpgames->HomeNameVn}', '{$tmpgames->HomeNameLa}', '{$tmpgames->AwayName}', '{$tmpgames->AwayNameEn}', '{$tmpgames->AwayNameTh}', '{$tmpgames->AwayNameBig}', '{$tmpgames->AwayNameGb}', '{$tmpgames->AwayNameKr}', '{$tmpgames->AwayNameVn}', '{$tmpgames->AwayNameLa}', '{$tmpgames->hdp}', '{$tmpgames->hdp_home}', '{$tmpgames->hdp_away}', '{$tmpgames->type}', '{$tmpgames->match_id}', '{$tmpgames->h32x32}', '{$tmpgames->h64x64}', '{$tmpgames->h256x256}', '{$tmpgames->a32x32}', '{$tmpgames->a64x64}', '$tmpgames->a256x256')";
//            $db->exec($insertsql);
//        }
//    }
}

function TimelineTrigger($fb_uid, $type, $key, $topic = 9)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $feed = $topic;
//    $knowuser = "";
//    $knowuser = "('$fb_uid'";
//    $friend_sql = "SELECT DISTINCT ff.friend_facebook_id as id
//FROM  facebook_friends ff
//WHERE ff.facebook_id = '$fb_uid'";
//    $stmtfriend = $db->query($friend_sql);
//    $userlist = $stmtfriend->fetchAll(5);
//    foreach ($userlist as $user) {
//        if (is_numeric($user->id))
//            $knowuser.=",'{$user->id}'";
//    }
//    $knowuser.=")";

    if (!empty($fb_uid) && !empty($type)) {
//        $dupsql = "SELECT * FROM timelines
//WHERE fb_uid IN $knowuser
//AND content_type='$type'
//AND key_list='$key' LIMIT 1";
//        $dupstmt = $db->query($dupsql);
//        $duplicated = $dupstmt->fetch(5);
//        if ($type == "game") {
//            gameToTimeline($key);
//        }

        if ($type == 'game') {
            $feed = 1;
        } else if ($type == 'news') {
            $feed = 2;
        } else if ($type == 'highlight') {
            $feed = 6;
        }

        $keygen = $type . $key;
        $db->exec("UPDATE `timelines` SET `last_event`=IF(pin='Y','Y','N') WHERE `keygen`='$keygen'");
        $sql = "INSERT INTO timelines (`fb_uid`,`content_type`,`key_list`,`created_at`,`updated_at`,`stamped_at`,`type`,`keygen`) VALUES ('$fb_uid','$type','$key',NOW(),NOW(),'$stmp',$feed,'$keygen')";
        $db->exec($sql);
        $lastid = $db->lastInsertId();
        $desc = "post new " . $type;
        if ($type == 'status') {
            regEvent($db, $fb_uid, 'post', 'ได้โพสต์ข้อความบนไทม์ไลน์', "", 'timeline', $lastid);
        } else if ($type == 'image') {
            regEvent($db, $fb_uid, 'post', 'ได้โพสต์รูปภาพบนไทม์ไลน์', "", 'timeline', $lastid);
        } else if ($type == 'video') {
            regEvent($db, $fb_uid, 'post', 'ได้โพสต์วิดีโอบนไทม์ไลน์', "", 'timeline', $lastid);
        } else if ($type == 'board') {
            regEvent($db, $fb_uid, 'post', 'ได้โพสต์กระทู้บนไทม์ไลน์', "", 'timeline', $lastid);
        }
        $rs = relatedTrigger($db, $type, $key, $fb_uid, $desc, "post");
        $results = getSeparatePost($lastid);

        $pintime = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+20 seconds"));
        $db->exec("UPDATE `timelines` SET `updated_at`='$pintime' WHERE `id`=192043;");
    }
    //querylog(__FUNCTION__, $db);
    return $results;
}

function relatedTrigger($db, $content_type, $key, $fb_uid, $desc, $type)
{
    //$db = DBConfig::getConnection();$db->exec("SET profiling = 1;");
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $sql = "INSERT INTO timelines_related (`content_type`,`key_list`,`fb_uid`,`description`,`related_type`,`created_at`,`stamp_at`) VALUES ('$content_type','$key','$fb_uid','$desc','$type',NOW(),'$stmp')";
    $rs = $db->exec($sql);
    return $rs;
}

function timelineReply()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $timeline_id = isset($_REQUEST["timeline_id"]) ? $_REQUEST["timeline_id"] : "";
    $parent = isset($_REQUEST["parent"]) ? (int)$_REQUEST["parent"] : 0;
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : "";
    $picture = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    //$results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $message = addslashes($message);
    $results = null;
    $day = date("Y-m-d H:i:s");
    $rtype = "msg";
    $stmp = strtotime($day);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid)) {
        if (!empty($message) || !empty($picture) || is_numeric($message)) {
            $pix = "";
            $thumpnail = "";
            if (!empty($picture)) {
                $imgs = explode("|", $picture);
                // var_dump($imgs);
                if (count($imgs) == 2) {
                    $pix = $imgs[0];
                    $thumpnail = $imgs[1];
                } else {
                    $pix = $imgs[0];
                }
            }
            if (!empty($message) && !empty($picture)) {
                $rtype = "mix";
            } else if (!empty($picture)) {
                $rtype = "img";
            }
            $insertreply = "INSERT INTO timelines_reply (`parent_id`,`fb_uid`,`message`,`picture`,`thumbnail`,`reply_to`,`reply_type`,`created_at`,`comment_at`) VALUES ('$parent','$fb_uid','$message','$pix','$thumpnail','$timeline_id','$rtype','$day','$stmp')";
            //echo $insertreply;
            $db->exec($insertreply);
            $lastid = $db->lastInsertId();
            $replysql = "SELECT tlr.*,fb.fb_firstname,fb.display_name FROM `timelines_reply` tlr LEFT JOIN facebook_user fb ON tlr.fb_uid=fb.fb_uid WHERE tlr.id='$lastid'";
            $rpstmt = $db->query($replysql);
            $results = $rpstmt->fetch(5);
            $tlsql = "SELECT * FROM timelines  WHERE id='$timeline_id'";
            $tlstmt = $db->query($tlsql);
            $timelines = $tlstmt->fetch(5);

            if (!empty($timelines)) {
                $desc = "comment on {$timelines->content_type}";
                $rs = relatedTrigger($db, $timelines->content_type, $timelines->key_list, $fb_uid, $desc, "comment");

                if ($parent) {
                    $sql = "SELECT fb_uid FROM comment_on_match WHERE id={$parent}";
                    $stmt = $db->query($sql);
                    $postfbuid = $stmt->fetchColumn();
                    if ($fb_uid != $postfbuid) {
                        regEvent($db, $fb_uid, 'comment', 'ได้แสดงความคิดเห็นในโพสต์ของคุณ ', "$message", 'timeline', "$timelines->id", "$postfbuid");
                    }
                } else {
                    if ($fb_uid != $timelines->fb_uid) {
                        regEvent($db, $fb_uid, 'comment', 'ได้แสดงความคิดเห็นในโพสต์ของคุณ ', "$message", 'timeline', "$timelines->id", "$timelines->fb_uid");
                    }
                }
                if ($parent == 0) {
                    $db->exec("UPDATE timelines SET updated_at=NOW(),comment_count=comment_count+1 WHERE id='$timeline_id'");
                } else {
                    $db->exec("UPDATE timelines SET updated_at=NOW() WHERE id='$timeline_id'");
                }
            }
        }
    }
    // $results = getSeparatePost($timeline_id);
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function timelineReplyLike()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $reply_id = isset($_REQUEST["reply_id"]) ? $_REQUEST["reply_id"] : "";
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "success" => false);
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid)) {
        $dup = "SELECT * FROM `tl_reply_related` WHERE `related_to`='$reply_id' AND `fb_uid`='$fb_uid' AND `related_type`='like'";
        $dstmt = $db->query($dup);
        $duplike = $dstmt->fetch(5);
        $success = 0;
        if (empty($duplike)) {
            $sql = "INSERT INTO `tl_reply_related` (`related_to`, `fb_uid`, `related_type`, `created_at`, `stamp_at`) VALUES ('$reply_id', '$fb_uid', 'like', '$day', '$stmp')";
            $db->exec($sql);
            $insertreply = "UPDATE `timelines_reply` SET `like`=`like`+1 WHERE  `id`=$reply_id";
            $db->exec($insertreply);
            $success = 1;
        }


        $tlsql = "SELECT reply_to,fb_uid FROM timelines_reply  WHERE id='$reply_id'";
        $tlstmt = $db->query($tlsql);
        $timeline_id = $tlstmt->fetch(5);

        $tlsql = "SELECT * FROM timelines  WHERE id='{$timeline_id->reply_to}'";
        $tlstmt = $db->query($tlsql);
        $timelines = $tlstmt->fetch(5);
        if (!empty($timelines)) {
            if ($fb_uid != $timeline_id->fb_uid) {
                regEvent($db, $fb_uid, 'like', 'ได้ถูกใจโพสต์ของคุณ ', "", 'timeline', "$timelines->id", "$timeline_id->fb_uid");
            }
        }
    }
    $tlsql = "SELECT tlr.*,fb.fb_firstname,fb.display_name, IF(1=$success,TRUE,FALSE)  as `success`, IF(1=$success,'nothing happen','already liked') as `desc` FROM `timelines_reply` tlr LEFT JOIN facebook_user fb ON tlr.fb_uid=fb.fb_uid WHERE id='$reply_id'";
    //echo $tlsql;
    $tlstmt = $db->query($tlsql);
    $results = $tlstmt->fetch(5);
    //$results = getSeparatePost($timeline_id);
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function timelineReplyReport()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $reply_id = isset($_REQUEST["reply_id"]) ? $_REQUEST["reply_id"] : "";
    $desc = isset($_REQUEST["desc"]) ? $_REQUEST["desc"] : "";
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid)) {
        $sql = "INSERT INTO `tl_reply_related` (`related_to`, `fb_uid`, `related_type`,`description`, `created_at`, `stamp_at`) VALUES ('$reply_id', '$fb_uid', 'report','$desc', '$day', '$stmp')";
        $db->exec($sql);
        $insertreply = "UPDATE `timelines_reply` SET `report`=`report`+1 WHERE  `id`=$reply_id";
        $db->exec($insertreply);
        $tlsql = "SELECT tlr.*,fb.fb_firstname,fb.display_name FROM `timelines_reply` tlr LEFT JOIN facebook_user fb ON tlr.fb_uid=fb.fb_uid WHERE id='$reply_id'";
        $tlstmt = $db->query($tlsql);
        $results = $tlstmt->fetch(5);
    }

    //$results = getSeparatePost($timeline_id);
    echo json_encode($results);
}

function timelineLike()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $timeline_id = isset($_REQUEST["timeline_id"]) ? $_REQUEST["timeline_id"] : "";
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "success" => FALSE, "desc" => 'Nothing happen');
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");


    if (!empty($fb_uid)) {
        $tlsql = "SELECT * FROM timelines WHERE id='$timeline_id'";
        $tlstmt = $db->query($tlsql);
        $timelines = $tlstmt->fetch(5);
        $dupsql = "SELECT * FROM `timelines_related` WHERE `content_type`='{$timelines->content_type}' AND `key_list`='{$timelines->key_list}' AND `related_type`='like' AND `fb_uid`='$fb_uid'";
        //$results['query'] = $dupsql;
        $dstmt = $db->query($dupsql);
        $liked = $dstmt->fetch(5);
        if (empty($liked)) {
            $updatesql = "";
            if ($timelines->content_type == "game") {
                $updatesql = "UPDATE timelines_game SET `like`=`like`+1 WHERE mid='{$timelines->key_list}'";
            } else if ($timelines->content_type == "image") {
                $updatesql = "UPDATE media_gallery SET `like`=`like`+1 WHERE id='{$timelines->key_list}'";
            } else if ($timelines->content_type == "status") {
                $updatesql = "UPDATE wall_status SET `like`=`like`+1 WHERE id='{$timelines->key_list}'";
            } else if ($timelines->content_type == "news") {
                $updatesql = "UPDATE news SET `like`=`like`+1 WHERE newsId='{$timelines->key_list}'";
            } else if ($timelines->content_type == "highlight") {
                $updatesql = "UPDATE video_highlight SET `like`=`like`+1 WHERE video_id='{$timelines->key_list}'";
            } else if ($timelines->content_type == "board") {
                $updatesql = "UPDATE `board` SET `like`=`like`+1  WHERE  `id`='{$timelines->key_list}'";
            }
            $success = $db->exec($updatesql);
            if ($success) {
                $results["success"] = TRUE;
                if ($fb_uid != $timelines->fb_uid) {
                    regEvent($db, $fb_uid, 'like', 'ได้ถูกใจโพสต์ของคุณ ', "", 'timeline', "$timelines->id", "$timelines->fb_uid");
                }
            }
            if (!empty($timelines)) {
                $desc = "like {$timelines->content_type}";
                $rs = relatedTrigger($db, $timelines->content_type, $timelines->key_list, $fb_uid, $desc, "like");
                $db->exec("UPDATE timelines SET updated_at=NOW() WHERE id='$timeline_id'");
            }
        } else {
            $results["desc"] = "already liked";
        }

        $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.id='$timeline_id'
";
        $stmt = $db->query($sql);
        $timeline = $stmt->fetch(5);


        $tl = array("owner" => array(), "content" => array());
        $tl["owner"] = $timeline;
        $rs = getTlContent($db, $timeline->content_type, $timeline->key_list);
        if (!empty($rs[0])) {
            foreach ($rs[0] as $key => $value) {
                $newkey = strtolower($key);
                // echo $newkey.":".$rs[0]->$key."\n";
                $rs[0]->$newkey = $value;
                // unset($rs[0]->$key);
            }
        }

        $tl["content"] = $rs;
        if ($timelines->content_type == "image") {
            $tl["owner"]->desc = $rs[0]->desc;
            $tl["owner"]->like = $rs[0]->like;
            $tl["owner"]->report = $rs[0]->report;
        }


        $results["timelines"][] = $tl;
    }
    //$results = getSeparatePost($timeline_id);
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function timelineReport()
{
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $timeline_id = isset($_REQUEST["timeline_id"]) ? $_REQUEST["timeline_id"] : "";
    $desc = isset($_REQUEST["desc"]) ? $_REQUEST["desc"] : "";
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $day = date("Y-m-d H:i:s");
    $stmp = strtotime($day);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    if (!empty($fb_uid)) {
        $tlsql = "SELECT * FROM timelines WHERE id='$timeline_id'";
        $tlstmt = $db->query($tlsql);
        $timelines = $tlstmt->fetch(5);

        $updatesql = "";
        if ($timelines->content_type == "game") {
            $updatesql = "UPDATE timelines_game SET `report`=`report`+1 WHERE mid='{$timelines->key_list}'";
        } else if ($timelines->content_type == "image") {
            $updatesql = "UPDATE media_gallery SET `report`=`report`+1 WHERE id='{$timelines->key_list}'";
        } else if ($timelines->content_type == "status") {
            $updatesql = "UPDATE wall_status SET `report`=`report`+1 WHERE id='{$timelines->key_list}'";
        } else if ($timelines->content_type == "news") {
            $updatesql = "UPDATE news SET `report`=`report`+1 WHERE newsId='{$timelines->key_list}'";
        } else if ($timelines->content_type == "highlight") {
            $updatesql = "UPDATE video_highlight SET `report`=`report`+1 WHERE video_id='{$timelines->key_list}'";
        } else if ($timelines->content_type == "board") {
            $updatesql = "UPDATE board SET `report`=`report`+1 WHERE id='{$timelines->key_list}'";
        }
        $db->exec($updatesql);

        //$tlsql = "SELECT * FROM timelines WHERE id='$timeline_id'";
        $tlsql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.id='$timeline_id'
";
        $tlstmt = $db->query($tlsql);
        $timelines = $tlstmt->fetch(5);


        $tl = array("owner" => array(), "content" => array());
        $tl["owner"] = $timelines;
        $rs = getTlContent($db, $timelines->content_type, $timelines->key_list);
        $tl["content"] = $rs;

        if ($timelines->content_type == "image") {
            $tl["owner"]->desc = $rs[0]->desc;
            $tl["owner"]->like = $rs[0]->like;
            $tl["owner"]->report = $rs[0]->report;
        }
        $results["timelines"][] = $tl;

        if (!empty($timelines)) {
            if (empty($desc))
                $desc = "report {$timelines->content_type}";
            $rs = relatedTrigger($db, $timelines->content_type, $timelines->key_list, $fb_uid, $desc, "report");
        }
    }
    //$results = getSeparatePost($timeline_id);
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function getSeparatePost($id)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.id=$id
AND tl.remove='N'
";
    $tlstmt = $db->query($sql);
    $timeline = $tlstmt->fetch(5);


    $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
    $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$timeline->content_type}'
AND tr.key_list='{$timeline->key_list}'
";

    $relatestmt = $db->query($relatesql);
    $allrelate = $relatestmt->fetchAll(5);
    foreach ($allrelate as $rel) {
        if ($rel->related_type == "post") {
            $related["post"][] = $rel;
        } else if ($rel->related_type == "comment") {
            $related["comment"][] = $rel;
        } else if ($rel->related_type == "like") {
            $related["like"][] = $rel;
        } else if ($rel->related_type == "report") {
            $related["report"][] = $rel;
        }
    }
    $results["related"][$timeline->id] = $related;


    $tl = array("owner" => array(), "content" => array());
    $tl["owner"] = $timeline;
    $rs = getTlContent($db, $timeline->content_type, $timeline->key_list);
    if (!empty($rs[0])) {
        foreach ($rs[0] as $key => $value) {
            $newkey = strtolower($key);
            // echo $newkey.":".$rs[0]->$key."\n";
            $rs[0]->$newkey = $value;
            // unset($rs[0]->$key);
        }
    }
//    if ($wall->content_type == "game") {
//        $player = array("home" => array(), "away" => array());
//        $homesql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='home' ORDER BY betId DESC LIMIT 5";
//        $homestmt = $db->query($homesql);
//        $home = $homestmt->fetchAll(5);
//        $player["home"] = $home;
//        $awaysql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='away' ORDER BY betId DESC LIMIT 5";
//        $awaystmt = $db->query($homesql);
//        $away = $awaystmt->fetchAll(5);
//        $player["away"] = $away;
//        $rs[0]->player = $player;
//    }
    $tl["content"] = $rs;
    $results["timelines"][] = $tl;
    $replysql = "";
    if ($timeline->content_type == "game") {
        $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$timeline->key_list}
AND b.message IS NOT NULL
AND b.message <>''
AND b.parent_id=0
AND b.remove='N'
ORDER BY  b.id DESC
LIMIT 5
";
    } else {
        $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
FROM timelines_reply tr
LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
WHERE tr.reply_to ={$timeline->id}
AND tr.remove='N'
ORDER BY tr.created_at
";
    }
    $srstmt = $db->query($replysql);
    $allreply = $srstmt->fetchAll(5);
    foreach ($allreply as $indreply) {
        if ((int)$indreply->parent_id == 0) {
            $results["main_reply"][(int)$id][] = $indreply;
        } else {
            $results["sub_reply"][(int)$indreply->parent_id][] = $indreply;
        }
    }
    //querylog(__FUNCTION__, $db);
    return $results;
}

function timelineSingleContent()
{
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array('success' => true, 'desc' => 'nothing', "timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.id=$id
";
    $tlstmt = $db->query($sql);
    $timeline = $tlstmt->fetch(5);


    if ($timeline->remove == 'Y') {
        $results['success'] = FALSE;
        $results['desc'] = 'This content has been deleted.';
    }


    $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
    $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$timeline->content_type}'
AND tr.key_list='{$timeline->key_list}'
";

    $relatestmt = $db->query($relatesql);
    $allrelate = $relatestmt->fetchAll(5);
    foreach ($allrelate as $rel) {
        if ($rel->related_type == "post") {
            $related["post"][] = $rel;
        } else if ($rel->related_type == "comment") {
            $related["comment"][] = $rel;
        } else if ($rel->related_type == "like") {
            $related["like"][] = $rel;
        } else if ($rel->related_type == "report") {
            $related["report"][] = $rel;
        }
    }
    $results["related"][$timeline->id] = $related;


    $tl = array("owner" => array(), "content" => array());
    $tl["owner"] = $timeline;
    $rs = getTlContent($db, $timeline->content_type, $timeline->key_list);
    if (!empty($rs[0])) {
        foreach ($rs[0] as $key => $value) {
            $newkey = strtolower($key);
            // echo $newkey.":".$rs[0]->$key."\n";
            $rs[0]->$newkey = $value;
            // unset($rs[0]->$key);
        }
    }
//    if ($wall->content_type == "game") {
//        $player = array("home" => array(), "away" => array());
//        $homesql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='home' ORDER BY betId DESC LIMIT 5";
//        $homestmt = $db->query($homesql);
//        $home = $homestmt->fetchAll(5);
//        $player["home"] = $home;
//        $awaysql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='away' ORDER BY betId DESC LIMIT 5";
//        $awaystmt = $db->query($homesql);
//        $away = $awaystmt->fetchAll(5);
//        $player["away"] = $away;
//        $rs[0]->player = $player;
//    }
    $tl["content"] = $rs;
    if ($timeline->content_type == "image") {
        $tl["owner"]->desc = $rs[0]->desc;
        $tl["owner"]->like = $rs[0]->like;
        $tl["owner"]->report = $rs[0]->report;
    }

    $results["timelines"][] = $tl;
    $replysql = "";
    if ($timeline->content_type == "game") {
        $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$timeline->key_list}
AND b.message IS NOT NULL
AND b.message <>''
#AND b.parent_id=0
AND b.remove='N'
ORDER BY  b.id DESC
";
    } else {
        $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
FROM timelines_reply tr
LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
WHERE tr.reply_to ='{$timeline->id}'
AND remove='N'
ORDER BY tr.created_at
";
    }
    $srstmt = $db->query($replysql);
    $allreply = $srstmt->fetchAll(5);
    foreach ($allreply as $indreply) {
        if ((int)$indreply->parent_id == 0) {
            $results["main_reply"][(int)$id][] = $indreply;
        } else {
            $results["sub_reply"][(int)$indreply->parent_id][] = $indreply;
        }
    }

    $viewsql = "UPDATE `timelines` SET `view`=`view`+1 WHERE  `id`='$id'";
    $db->exec($viewsql);
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function getTimelines()
{
    $startf = Carbon::now();
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : 0;
    $viewtype = isset($_REQUEST["view_type"]) ? $_REQUEST["view_type"] : "all";
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $range = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 40;
    $topics = isset($_REQUEST['category']) ? $_REQUEST['category'] : "1,2,3,4,5,6,7,8,9,10";
    $start = $offset * $range;
    $functiostart = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "related" => array(), 'recommend' => array(), 'most_view' => array());

    $logOn = logGet(2);
    ////Analog::handler(\//Analog\Handler\File::init('logs/Timeline.log'));
    if ($logOn == 'on') {
        ////Analog::log("===$fb_uid===", 6);
    }
    $cachefile = "cache/timeline/" . $viewtype . "_" . $topics . "_" . $offset . "_" . $range . ".json";
    $newquery = false;
    if (($viewtype != "own") && ($offset == 0)) {
        if (file_exists($cachefile)) {
            $filem = filemtime($cachefile);
            $fileminute = date('i', $filem);
            $dt = date('i');
            if ($dt != $fileminute) {
                $newquery = true;
            } else {
                $results = json_decode(file_get_contents($cachefile), true);
            }
        } else {
            $newquery = true;
        }
    } else {
        $newquery = true;
    }


    if ($newquery) {
        if ($viewtype != 'board') {

            if ($uid == 0) {
                $uidsql = "SELECT uid FROM facebook_user WHERE fb_uid='$fb_uid'";
                $ustmt = $db->query($uidsql);
                $fuid = $ustmt->fetchColumn();
                $uid = $fuid;
            }

            if (!file_exists("wb_setting/$uid.json")) {
                initCatview($uid);
            }

            $json = json_decode(file_get_contents("wb_setting/$uid.json"), true);
            $topics = "";
            foreach ($json as $jsn) {
                if (empty($topics)) {
                    $topics = $jsn['fid'];
                } else {
                    $topics .= ',' . $jsn['fid'];
                }
            }
        }

        if (!empty($fb_uid)) {

            $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name,@@hostname FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND last_event='Y'
AND tl.type IN ($topics,0,9)
ORDER BY pin,updated_at DESC
LIMIT $start,$range";

            if ($viewtype == 'own') {
                $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name,@@hostname FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.fb_uid ='$fb_uid'
AND tl.remove='N'
ORDER BY pin,updated_at DESC
LIMIT $start,$range";
            } else if ($viewtype == 'board') {
                $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name,@@hostname FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND last_event='Y'
AND tl.type IN ($topics)
ORDER BY pin,updated_at DESC
LIMIT $start,$range";
            }

//file sort with group by
//        if ($logOn == 'on') {
//            ////Analog::log("$sql", 6);
//        }
            $tlstmt = $db->query($sql);
            $timelines = $tlstmt->fetchAll(5);
//        if ($logOn == 'on') {
//            ////Analog::log("$timelines[0]['@@hostname']", 6);
//        }
            foreach ($timelines as $wall) {
                $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());

                $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$wall->content_type}'
AND tr.key_list='{$wall->key_list}'
";
                //using key
                $relatestmt = $db->query($relatesql);
                $allrelate = $relatestmt->fetchAll(5);
                foreach ($allrelate as $rel) {
                    if ($rel->related_type == "post") {
                        $related["post"][] = $rel;
                    } else if ($rel->related_type == "comment") {
                        $related["comment"][] = $rel;
                    } else if ($rel->related_type == "like") {
                        $related["like"][] = $rel;
                    } else if ($rel->related_type == "report") {
                        $related["report"][] = $rel;
                    }
                }
                $results["related"][$wall->id] = $related;

                $tl = array("owner" => array(), "content" => array());
                $tl["owner"] = $wall;
                $rs = getTlContent($db, $wall->content_type, $wall->key_list);
                if (!empty($rs[0])) {
                    if ($wall->content_type == "game") {
                        $ownbetsql = "SELECT oddsTimestamp FROM bet WHERE `fb_uid`='$fb_uid' AND `mid`='{$wall->key_list}'";
                        $ownbetstmt = $db->query($ownbetsql);
                        $ownbet = $ownbetstmt->fetch();
                        if ($ownbet) {
                            $json = json_decode($ownbet['oddsTimestamp'], true);
                            $rs[0]->hdp = $json['hdp'];
                            $rs[0]->hdp_home = $json['hdp_home'];
                            $rs[0]->hdp_away = $json['hdp_away'];
                        }
                    }

                    foreach ($rs[0] as $key => $value) {
                        $newkey = strtolower($key);
                        // echo $newkey.":".$rs[0]->$key."\n";
                        $rs[0]->$newkey = $value;
                        // unset($rs[0]->$key);
                    }
                    if ($wall->content_type == "game") {
                        $mof = array();
                        if (file_exists('matches_comment/' . $wall->key_list . '.json')) {
                            $mof = json_decode(file_get_contents('matches_comment/' . $wall->key_list . '.json'), true);
                        }
                        $rs[0]->betdata = $mof;
                        $stsql = "SELECT tid,leagueId,no number,ml lastplay FROM stat_table WHERE leagueId={$rs[0]->_lid} AND tid IN ({$rs[0]->hid},{$rs[0]->gid})";
                        $ststmt = $db->query($stsql);
                        $teamstat = $ststmt->fetchAll(5);
                        $rs[0]->stat = $teamstat;
                    }
                }

                $tl["content"] = $rs;
                if ($wall->content_type == "image") {
                    $tl["owner"]->desc = $rs[0]->desc;
                    $tl["owner"]->like = $rs[0]->like;
                    $tl["owner"]->report = $rs[0]->report;
                }
                if (!empty($tl["content"][0])) {
                    $results["timelines"][] = $tl;


                    $replysql = "";
                    if ($wall->content_type == "game") {
                        //using key checked
                        $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$wall->key_list}
AND b.message IS NOT NULL
AND b.message <>''
AND b.parent_id = 0
AND b.remove='N'
ORDER BY  b.id DESC
LIMIT 4
";
                    } else {
                        //using key checked
                        $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
FROM timelines_reply tr
LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
WHERE tr.reply_to ={$wall->id}
AND tr.remove='N'
#AND tr.parent_id = 0
ORDER BY tr.id
#LIMIT 4
";
                    }
                    $srstmt = $db->query($replysql);
                    $allreply = $srstmt->fetchAll(5);
                    foreach ($allreply as $indreply) {
                        if ((int)$indreply->parent_id == 0) {
                            $results["main_reply"][(int)$wall->id][] = $indreply;
                        } else {
                            $results["sub_reply"][(int)$indreply->parent_id][] = $indreply;
                        }
                    }
                    $results['recommend'] = array();
                    $results['most_view'] = array();
                    $results['admin_post'] = array();
                    if ($viewtype == 'board' AND $topics != "1") {
                        $results['recommend'] = getRec($db, 0, 10, $topics);
                        $results['most_view'] = getRec($db, 0, 10, $topics, 'view');
                        $results['admin_post'] = getRec($db, 0, 10, $topics, 'admin');
                    }
                }
            }
            file_put_contents($cachefile, json_encode($results));
        }
    }
    $functionstop = Carbon::now();
    $results['function_times'] = $functionstop->diffInSeconds($functiostart);
    $results['category'] = $topics;
    if ($logOn == 'on') {
        ////Analog::log("function time = {$results['function_times']}s", 6);
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function getMobileFeed()
{
    $startf = Carbon::now();
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : 0;
    $viewtype = isset($_REQUEST["view_type"]) ? $_REQUEST["view_type"] : "all";
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $range = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 40;
    $topics = isset($_REQUEST['category']) ? $_REQUEST['category'] : "6";
    $lid = isset($_REQUEST['lid']) ? (int)$_REQUEST['lid'] : 0;
    $tid = isset($_REQUEST['tid']) ? (int)$_REQUEST['tid'] : 0;
    $mid = isset($_REQUEST['mid']) ? (int)$_REQUEST['mid'] : 0;
    $start = $offset * $range;

    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "related" => array(), 'recommend' => array(), 'most_view' => array());
    $cachefile = "cache/mobile/" . $viewtype . "_" . $offset . "_" . $range . "_" . $topics . "_" . $lid . "_" . $tid . "_" . $mid . ".json";
    $newquery = true;
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $dt = date('H');
        $filedt = date('H', $filem);
        if ($dt == $filedt) {
            $newquery = false;
            $results = json_decode(file_get_contents($cachefile), true);
        }
    }

    if ($newquery) {
//
//
//    if ($viewtype != 'board') {
//
//        if ($uid == 0) {
//            $uidsql = "SELECT uid FROM facebook_user WHERE fb_uid='$fb_uid'";
//            $ustmt = $db->query($uidsql);
//            $fuid = $ustmt->fetchColumn();
//            $uid = $fuid;
//        }
//
//        if (!file_exists("wb_setting/$uid.json")) {
//            initCatview($uid);
//        }
//
//        $json = json_decode(file_get_contents("wb_setting/$uid.json"), true);
//        $topics = "";
//        foreach ($json as $jsn) {
//            if (empty($topics)) {
//                $topics = $jsn['fid'];
//            } else {
//                $topics.=',' . $jsn['fid'];
//            }
//        }
//    }

        if (!empty($fb_uid)) {
            $knowuser = "";
            $knowuser = "('$fb_uid'";
            if ($viewtype == "all") {
                // $knowuser.=",'broadcast'";
//            $friend_sql = "SELECT  ff.friend_facebook_id as id
//FROM  facebook_friends ff
//WHERE ff.facebook_id = '$fb_uid'";
                $friend_sql = "SELECT id FROM
(
SELECT
ff.friend_facebook_id as id
FROM  facebook_friends ff
WHERE ff.facebook_id ='$fb_uid'
UNION
SELECT
    fl.fb_follow_uid as id
    FROM follow fl
    WHERE fl.fb_uid ='$fb_uid'
)
AS userlist
GROUP BY id";
                $stmtfriend = $db->query($friend_sql);
                $userlist = $stmtfriend->fetchAll(5);
                foreach ($userlist as $user) {
                    if (is_numeric($user->id))
                        $knowuser .= ",'{$user->id}'";
                }
            }
            $knowuser .= ")";

            $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND tl.type IN ($topics)
AND tl.content_type IN ('news','highlight')
#GROUP BY tl.keygen
ORDER BY tl.id DESC
#LIMIT 500";

            if ($viewtype == 'game') {
                $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND tl.content_type ='game'
AND tl.key_list='$mid'
AND tl.last_event='Y'
#GROUP BY tl.keygen
ORDER BY tl.id DESC
#LIMIT 500";
            }
            $tlstmt = $db->query($sql);
            $timelines = $tlstmt->fetchAll(5);
            $ind = 0;
            foreach ($timelines as $key => $wall) {
                $rs = getTlContent($db, $wall->content_type, $wall->key_list);
                $passed = TRUE;

                if (!empty($rs) && $ind < $range) {
                    if ($lid > 0) {
                        //echo "got lid\n";
                        if ($wall->content_type == 'news') {
                            if ($lid == $rs[0]->leagueId1 || $lid == $rs[0]->leagueId2 || $lid == $rs[0]->leagueId3 || $lid == $rs[0]->leagueId4) {
                                $passed = TRUE;
                            } else {
                                $passed = FALSE;
                            }
                        }
                        if ($wall->content_type == 'highlight') {
                            //echo "got HL\n";
                            if ($lid == (int)$rs[0]->leagueId) {
                                //echo "lid match\n";
                                $passed = TRUE;
                            } else {
                                $passed = FALSE;
                            }
                        }
                    }

                    if ($tid > 0) {
                        if ($wall->content_type == 'news') {
                            if ($tid == $rs[0]->teamId1 || $tid == $rs[0]->teamId2 || $tid == $rs[0]->teamId3 || $tid == $rs[0]->teamId4) {
                                $passed = TRUE;
                            } else {
                                $passed = FALSE;
                            }
                        }

                        if ($wall->content_type == 'highlight') {
                            if ($tid == $rs[0]->hid || $tid == $rs[0]->gid) {
                                $passed = TRUE;
                            } else {
                                $passed = FALSE;
                            }
                        }
                    }

                    if ($mid > 0) {
                        if ($wall->content_type == 'highlight') {
                            if ($mid == $rs[0]->mid) {
                                $passed = TRUE;
                            } else {
                                $passed = FALSE;
                            }
                        }
                    }
                } else {
                    $passed = false;
                }

                //echo $lid;
//            echo $wall->content_type . ":" . $wall->key_list . "\n";
//            var_dump($rs);
//            var_dump($passed);
//            echo "\n";
                //exit();

                if ($passed) {
                    $ind++;
                    $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
                    $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$wall->content_type}'
AND tr.key_list='{$wall->key_list}'
";

                    $relatestmt = $db->query($relatesql);
                    $allrelate = $relatestmt->fetchAll(5);
                    foreach ($allrelate as $rel) {
                        if ($rel->related_type == "post") {
                            $related["post"][] = $rel;
                        } else if ($rel->related_type == "comment") {
                            $related["comment"][] = $rel;
                        } else if ($rel->related_type == "like") {
                            $related["like"][] = $rel;
                        } else if ($rel->related_type == "report") {
                            $related["report"][] = $rel;
                        }
                    }
                    $results["related"][$wall->id] = $related;

                    $tl = array("owner" => array(), "content" => array());
                    $tl["owner"] = $wall;
//            $rs = getTlContent($wall->content_type, $wall->key_list);
                    if (!empty($rs[0])) {
                        if ($wall->content_type == "game") {
                            $ownbetsql = "SELECT oddsTimestamp FROM bet WHERE `fb_uid`='$fb_uid' AND `mid`='{$wall->key_list}'";
                            $ownbetstmt = $db->query($ownbetsql);
                            $ownbet = $ownbetstmt->fetch();
                            if ($ownbet) {
                                $json = json_decode($ownbet['oddsTimestamp'], true);
                                $rs[0]->hdp = $json['hdp'];
                                $rs[0]->hdp_home = $json['hdp_home'];
                                $rs[0]->hdp_away = $json['hdp_away'];
                            }
                        }

                        foreach ($rs[0] as $key => $value) {
                            $newkey = strtolower($key);
                            // echo $newkey.":".$rs[0]->$key."\n";
                            $rs[0]->$newkey = $value;
                            // unset($rs[0]->$key);
                        }
                        if ($wall->content_type == "game") {
                            $mof = array();
                            if (file_exists('matches_comment/' . $wall->key_list . '.json'))
                                $mof = json_decode(file_get_contents('matches_comment/' . $wall->key_list . '.json'), true);
                            $rs[0]->betdata = $mof;
                            $stsql = "SELECT tid,leagueId,no number,ml lastplay FROM stat_table WHERE leagueId={$rs[0]->_lid} AND tid IN ({$rs[0]->hid},{$rs[0]->gid})";
                            $ststmt = $db->query($stsql);
                            $teamstat = $ststmt->fetchAll(5);
                            $rs[0]->stat = $teamstat;
                        }
                    }

//            if ($wall->content_type == "game") {
//                $player = array("home" => array(), "away" => array());
//                $homesql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='home' ORDER BY betId DESC LIMIT 5";
//                $homestmt = $db->query($homesql);
//                $home = $homestmt->fetchAll(5);
//                $player["home"] = $home;
//                $awaysql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='away' ORDER BY betId DESC LIMIT 5";
//                $awaystmt = $db->query($homesql);
//                $away = $awaystmt->fetchAll(5);
//                $player["away"] = $away;
//                @$rs[0]->player = $player;
//            }

                    $tl["content"] = $rs;
                    if ($wall->content_type == "image") {
                        $tl["owner"]->desc = $rs[0]->desc;
                        $tl["owner"]->like = $rs[0]->like;
                        $tl["owner"]->report = $rs[0]->report;
                    }
                    if (!empty($tl["content"][0])) {
                        $results["timelines"][] = $tl;

                        if ($viewtype != 'board') {
                            $replysql = "";
                            if ($wall->content_type == "game") {
                                $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$wall->key_list}
AND b.message IS NOT NULL
AND b.message <>''
AND b.parent_id = 0
AND b.remove='N'
ORDER BY  b.id DESC
LIMIT 4
";
                            } else {
                                $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
FROM timelines_reply tr
LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
WHERE tr.reply_to ={$wall->id}
AND tr.remove='N'
AND tr.parent_id = 0
ORDER BY tr.created_at
LIMIT 4
";
                            }
                            $srstmt = $db->query($replysql);
                            $allreply = $srstmt->fetchAll(5);
                            foreach ($allreply as $indreply) {
                                if ((int)$indreply->parent_id == 0) {
                                    $results["main_reply"][(int)$wall->id][] = $indreply;
                                } else {
                                    $results["sub_reply"][(int)$indreply->parent_id][] = $indreply;
                                }
                            }
                        } else {

                            // $results['recommend'] = getRec(0, 10, $topics);
                            // $results['most_view'] = getRec(0, 10, $topics, 'view');
                        }
                    }
                }
            }
        }
        //querylog(__FUNCTION__, $db);
        file_put_contents($cachefile, json_encode($results));
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
    }
    echo json_encode($results);
}

function removeTimeline()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $timeline = isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $result = array("success" => FALSE, "desc" => "Nothing happen");
    if (!empty($timeline)) {
        $super = getSu();
        $sql = "UPDATE `timelines` SET `remove`='Y' WHERE  `id`='$timeline' AND `fb_uid`='$fb_uid'";
        if ($fb_uid == $super) {
            $sql = "UPDATE `timelines` SET `remove`='Y' WHERE  `id`='$timeline'";
        }
        $success = $db->exec($sql);
        if ($success) {
            $result["success"] = TRUE;
            $result['desc'] = "removed";
        } else {
            $result['desc'] = "permission denied";
        }
    }
    echo json_encode($result);
}

function removeTimelineReply()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $result = array("success" => FALSE, "desc" => "nothing happen");
    if (!empty($id)) {
        $super = getSu();
        $sql = "UPDATE `timelines_reply` SET `remove`='Y' WHERE `fb_uid`='$fb_uid' AND `id`='$id'";
        if ($fb_uid == $super) {
            $sql = "UPDATE `timelines_reply` SET `remove`='Y' WHERE `id`='$id'";
        }
        $success = $db->exec($sql);
        if ($success) {
            $result["success"] = TRUE;
            $sqlsub = "UPDATE `timelines_reply` SET `remove`='Y' WHERE  `parent_id`='$id'";
            $success = $db->exec($sqlsub);
        } else {
            $result["desc"] = "permisson denied";
        }
    }
    echo json_encode($result);
}

function getFriendTimelines()
{
    $startf = Carbon::now();
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    //$viewtype = isset($_REQUEST["view_type"]) ? $_REQUEST["view_type"] : "all";
    $offset = isset($_REQUEST["offset"]) ? (int)$_REQUEST["offset"] : 0;
    $range = isset($_REQUEST["limit"]) ? (int)$_REQUEST["limit"] : 20;
    $start = $offset * $range;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "related" => array());

    if (!empty($fb_uid)) {
        $knowuser = "";
        $knowuser = "('$fb_uid'";
//        if ($viewtype == "all") {
//            $friend_sql = "SELECT  ff.friend_facebook_id as id
//FROM  facebook_friends ff
//WHERE ff.facebook_id = '$fb_uid'";
//            $stmtfriend = $db->query($friend_sql);
//            $userlist = $stmtfriend->fetchAll(5);
//            foreach ($userlist as $user) {
//                if (is_numeric($user->id))
//                    $knowuser.=",'{$user->id}'";
//            }
//        }
        $knowuser .= ")";

        $sql = "SELECT CONCAT(tl.content_type,tl.key_list) AS tmpkey, tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.fb_uid in $knowuser
GROUP BY tmpkey
ORDER BY tl.updated_at DESC
LIMIT $start,$range";

        $tlstmt = $db->query($sql);
        $timelines = $tlstmt->fetchAll(5);

        foreach ($timelines as $wall) {
            $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
            $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$wall->content_type}'
AND tr.key_list='{$wall->key_list}'
";

            $relatestmt = $db->query($relatesql);
            $allrelate = $relatestmt->fetchAll(5);
            foreach ($allrelate as $rel) {
                if ($rel->related_type == "post") {
                    $related["post"][] = $rel;
                } else if ($rel->related_type == "comment") {
                    $related["comment"][] = $rel;
                } else if ($rel->related_type == "like") {
                    $related["like"][] = $rel;
                } else if ($rel->related_type == "report") {
                    $related["report"][] = $rel;
                }
            }
            $results["related"][$wall->id] = $related;

            $tl = array("owner" => array(), "content" => array());
            $tl["owner"] = $wall;
            $rs = getTlContent($db, $wall->content_type, $wall->key_list);
            if (!empty($rs[0])) {
                foreach ($rs[0] as $key => $value) {
                    $newkey = strtolower($key);
                    // echo $newkey.":".$rs[0]->$key."\n";
                    $rs[0]->$newkey = $value;
                    // unset($rs[0]->$key);
                }
            }

//            if ($wall->content_type == "game") {
//                $player = array("home" => array(), "away" => array());
//                $homesql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='home' ORDER BY betId DESC LIMIT 5";
//                $homestmt = $db->query($homesql);
//                $home = $homestmt->fetchAll(5);
//                $player["home"] = $home;
//                $awaysql = "SELECT fb_uid FROM bet WHERE mid='{$wall->key_list}' AND choose='away' ORDER BY betId DESC LIMIT 5";
//                $awaystmt = $db->query($homesql);
//                $away = $awaystmt->fetchAll(5);
//                $player["away"] = $away;
//                @$rs[0]->player = $player;
//            }

            $tl["content"] = $rs;

            if (!empty($tl["content"][0])) {
                $results["timelines"][] = $tl;
                $replysql = "";
                if ($wall->content_type == "game") {
                    $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
WHERE
b.match_id={$wall->key_list}
AND b.message IS NOT NULL
AND b.message <>''
AND b.fb_uid IN  $knowuser
ORDER BY  b.id DESC
";
                } else {
                    $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
FROM timelines_reply tr
LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
WHERE tr.reply_to ={$wall->id}
ORDER BY tr.created_at
";
                }
                $srstmt = $db->query($replysql);
                $allreply = $srstmt->fetchAll(5);
                foreach ($allreply as $indreply) {
                    if ((int)$indreply->parent_id == 0) {
                        $results["main_reply"][(int)$wall->id][] = $indreply;
                    } else {
                        $results["sub_reply"][(int)$indreply->parent_id][] = $indreply;
                    }
                }
            }
        }
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function getTlContent($db, $type, $key)
{
    //$db = DBConfig::getConnection();
    $results = array();
    $sql = "";
    if ($type == "game") {
        $sql = "SELECT  `id`,
	`mid`,
	`_lid`,
	`hid`,
	`gid`,
	`sid`,
	`c0`,
	`c1`,
	`c2`,
	`cx`,
	`competitionId`,
	`show_date`,
	`score`,
	`leagueName`,
	`leagueNameEn`,
	`leagueNameTh`,
	`homeName`,
	`homeNameEn`,
	`homeNameTh`,
	`awayName`,
	`awayNameEn`,
	`awayNameTh`,
	`hdp`,
	`hdp_home`,
	`hdp_away`,
	`type`,
	`match_id`,
	`h32x32`,
	`h64x64`,
	`h256x256`,
	`a32x32`,
	`a64x64`,
	`a256x256`,
	`like`,
	`report` FROM timelines_game WHERE mid=$key";
    } else if ($type == "image") {
        $sql = "SELECT *
FROM media_gallery mg
LEFT JOIN media_store ms ON mg.id = ms.gall_id
WHERE mg.id=$key";
    } else if ($type == "board") {
        $sql = "SELECT *
FROM board b
WHERE b.id=$key";
    } else if ($type == "video") {
        $sql = "SELECT *
FROM media_gallery mg
LEFT JOIN media_store ms ON mg.id = ms.gall_id
WHERE mg.id=$key";
    } else if ($type == "status") {
        $sql = "SELECT *
FROM `wall_status`
WHERE id=$key";
    } else if ($type == 'news') {
        $sql = "SELECT *
FROM `news`
WHERE newsId=$key";
    } else if ($type == 'highlight') {
        $sql = "SELECT *
FROM `video_highlight`
WHERE `video_id`=$key";
    }
    $stmt = $db->query($sql);
    $results = $stmt->fetchAll(5);

    if ($type == "board") {
        $results[0]->medialist = array();
        if (!empty($results[0]->media)) {
            $sql = "SELECT *
FROM media_store mt
WHERE mt.id IN ({$results[0]->media})";
            $stmt = $db->query($sql);
            $results[0]->medialist = $stmt->fetchAll(5);
        }
    }
    return $results;
}

function tlGamePlayer()
{
    $startf = Carbon::now();
    $mids = isset($_REQUEST["mids"]) ? $_REQUEST["mids"] : "";
    $list = array();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $mlist = explode(",", $mids);
    foreach ($mlist as $mid) {
        $player = array("home" => array(), "away" => array(), "bet" => 0, "comment" => 0);
        if (!empty($mid) && is_numeric($mid)) {
            $homesql = "SELECT fb_uid FROM bet WHERE mid='$mid' AND choose='home' ORDER BY betId DESC LIMIT 5";
            $homestmt = $db->query($homesql);
            $home = $homestmt->fetchAll(5);
            $player["home"] = $home;
            $awaysql = "SELECT fb_uid FROM bet WHERE mid='$mid' AND choose='away' ORDER BY betId DESC LIMIT 5";
            $awaystmt = $db->query($homesql);
            $away = $awaystmt->fetchAll(5);
            $player["away"] = $away;

            $betsql = "SELECT count(betId) as bet FROM bet WHERE mid='$mid'";
            $betstmt = $db->query($betsql);
            $bet = $betstmt->fetch(5);
            $player["bet"] = $bet->bet;

            $cmsql = "SELECT count(id) as comment FROM comment_on_match WHERE match_id='$mid'";
            $cmstmt = $db->query($cmsql);
            $cm = $cmstmt->fetch(5);
            $player["comment"] = $cm->comment;
        }
        $list[$mid] = $player;
    }

    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($list);
}

function getAchieve()
{
    $startf = Carbon::now();
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $tid = isset($_REQUEST["tid"]) ? $_REQUEST["tid"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array();
    $optional = "";
    if (!empty($tid)) {
        $optional = " AND t.id='$tid' ";
    }
    if (!empty($fb_uid)) {
        $sql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
$optional
GROUP BY ua.trophy_id
ORDER BY ua.earn_at DESC
";
        $stmt = $db->query($sql);
        $results = $stmt->fetchAll(5);
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function getAchieve2()
{
    $startf = Carbon::now();
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $tid = isset($_REQUEST["tid"]) ? $_REQUEST["tid"] : "";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array();
    $optional = "";
    if (!empty($tid)) {
        $optional = " AND t.id='$tid' ";
    }
    if (!empty($fb_uid)) {
        $sql = "SELECT ua.*, t.*,t.id as tid, count( ua.id ) AS number
FROM `achieve` ua
LEFT JOIN `trophy2` t ON ua.trophy_id = t.id
WHERE ua.fb_uid ='$fb_uid'
$optional
GROUP BY ua.trophy_id
ORDER BY ua.earn_at DESC
";
        $stmt = $db->query($sql);
        $results = $stmt->fetchAll(5);
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function getBestPrize($db, $fb_uid, $top)
{
    $last14day = date("Y-m-d H:i:s", strtotime(date("Y-m-d") . "-15 day"));
    //$db = DBConfig::getConnection();
    $sql = "SELECT ua.fb_uid, t.type, t.sequence, t.picture, count( ua.id ) AS number
FROM `user_achieve` ua
LEFT JOIN `trophy` t ON ua.trophy_id = t.id
WHERE ua.fb_uid = '$fb_uid'
AND ua.earn_at > '$last14day'
GROUP BY ua.trophy_id
ORDER BY sequence
LIMIT $top
";
    $stmt = $db->query($sql);
    $results = $stmt->fetchAll(5);
    //querylog(__FUNCTION__, $db);
    return $results;
}

function getBestPrize2($db, $fb_uid, $top)
{
    $last14day = date("Y-m-d", strtotime(date("Y-m-d") . "-16 day"));
    //$db = DBConfig::getConnection();
    $sql = "SELECT ua.fb_uid, t.type, t.sequence, t.picture, count( ua.id ) AS number
FROM `achieve` ua
LEFT JOIN `trophy2` t ON ua.trophy_id = t.id
WHERE ua.fb_uid = '$fb_uid'
AND ua.earn_at > '$last14day'
GROUP BY ua.trophy_id
ORDER BY sequence
LIMIT $top
";
//echo $sql."\n";
    $stmt = $db->query($sql);
    $results = $stmt->fetchAll(5);
    //querylog(__FUNCTION__, $db);
    return $results;
}

function nextReset()
{
    $nowday = date("Y-m-d H:i:s");
//    $nweek = date("W");
//    $dayofweek = date('w', strtotime($nowday));
    $resetday = date("Y-m-d H:i:s");
//    if ($nweek % 2 == 0) {
//        $resetday = date("Y-m-d H:i:s", strtotime("next monday +5hour 30min"));
//    } else {
//        $lastmonday = date("Y-m-d", strtotime("last monday"));
//        $day = date("Y-m-d");
//        $nowtime = date("H:i:s");
//        $runtime = date("H:i:s", strtotime("05:30:00"));
//        //echo $nowtime . "|" . $runtime;
//        if ($lastmonday == $day && $nowtime < $runtime) {
//            $resetday = date("Y-m-d H:i:s", strtotime($day . "+5hour 30min"));
//        } else {
//            $mday = date("Y-m-d H:i:s", strtotime("next monday +5hour 30min"));
//            $resetday = date("Y-m-d H:i:s", strtotime($mday . "next monday +5hour 30min"));
//        }
//    }
    $day = date("d");
    $h = date("H");
    $carbon = new Carbon();
    if ($day > 15) {
        //$resetday = date("Y-m-d H:i:s", strtotime("first day of next month +5hour"));
        $nmonth = date("m");
        $resetday = Carbon::create(date('Y'), $nmonth, 1, 5, 0, 0);
        $resetday->addMonth();
    } else {
        //$remain = 16 - (int) $day;
        //$resetday = date("Y-m-d H:i:s", strtotime("+$remain day +5hour"));
        $resetday = Carbon::create(date('Y'), date('m'), 16, 5, 0, 0);
    }

    if (((int)$day == 1 || (int)$day == 16) && (int)$h < 5) {
        $resetday = Carbon::create(date('Y'), date('m'), $day, 5, 0, 0);
    }
    $rs = dateDiff($nowday, $resetday);
    echo json_encode($rs);
}

function dateDiff($start, $end = false)
{
    $return = array();

    try {
        $start = new DateTime($start);
        $end = new DateTime($end);
        $form = $start->diff($end);
    } catch (Exception $e) {
        return $e->getMessage();
    }
    $display = array(
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second');
    foreach ($display as $key => $value) {
        //  if ($form->$key > 0) {
        //$return[] = $form->$key . ' ' . ($form->$key > 1 ? $value . 's' : $value);
        $return[] = $form->$key;
        //  }
    }

    return $return;
}

function getGameData()
{
    $startf = Carbon::now();
    $mid = isset($_REQUEST['mid']) ? $_REQUEST["mid"] : "";
    $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : "en";
    $functiostart = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("game" => array(), "stat" => array(), "compare" => array(), "last_result" => array("home" => array("all" => array(), "league" => array()), "away" => array("all" => array(), "league" => array())), 'statfrom' => 'file');
    if (!empty($mid)) {
        $sql = "SELECT * FROM `timelines_game` WHERE mid='$mid'";
        $stmt = $db->query($sql);
        $results["game"] = $stmt->fetch(5);
        if (!empty($results["game"])) {
            $filename = "gen_file_stat_table/{$results['game']->_lid}.json";
            $json = null;
            if (file_exists($filename) && FALSE) {
                $json = file_get_contents($filename);
                $results['statfrom'] = "file";
            } else {

                $sql = "select stat_table.*,team.tid,t.teamNameTh,team.tn from stat_table
            left join team on (team.tnPk=stat_table.tnPk)
            left join lang_team t on t.teamName = team.tn
            where stat_table.leagueId={$results['game']->_lid}  and subLeagueNamePk=''
            group by stat_table.tnPk order by stat_table.no ASC";
                $stmt = $db->query($sql);
                $stat_table = $stmt->fetchAll(PDO::FETCH_OBJ);
                $sql = "select league.*,lang_league.leagueNameTh from league
            left join lang_league on lang_league.leagueName = league.leagueName
            where league.leagueId={$results['game']->_lid}";
                $stmt = $db->query($sql);
                $info = $stmt->fetch(PDO::FETCH_OBJ);
                $obj = array(
                    'stat_table' => $stat_table,
                    'info' => $info
                );
                $json = json_encode($obj);
                $results['statfrom'] = "db";
            }
            $json = json_decode($json);
            $results["stat"] = $json;


            $ln = $lang ? $lang . '/' : 'en/';
            $filename = 'compare_team/' . $ln . $results["game"]->hid . '_' . $results["game"]->gid . '.json';
            $filename2 = 'compare_team/' . $results["game"]->hid . '_' . $results["game"]->gid . '.json';
            $json = null;
            if (file_exists($filename)) {
                $json = file_get_contents($filename);
            } else if (file_exists($filename2)) {
                $json = file_get_contents($filename2);
            } else {

                $obj = array(
                    'result_vs' => array(),
                    'result1' => array(),
                    'result2' => array(),
                    'team1' => array(),
                    'team2' => array(),
                );
                $json = json_encode($obj);
            }
            $json = json_decode($json, true);

            if (!empty($json["result1"])) {
                foreach ($json["result1"] as $result1) {
//                    if (!empty($result1)) {
//                        foreach ($result1['r'] as $key => $rs1) {
//                            if (empty($rs1)) {
//                                $result1['r'][$key] = 0;
//                            }
//                        }
//                    }


                    if (count($results["last_result"]["home"]["all"]) < 5) {
                        $results["last_result"]["home"]["all"][] = $result1["r"];
                    }
                    if (array_key_exists("leagueId", $result1)) {
                        if (count($results["last_result"]["home"]["league"]) < 5 && $result1["leagueId"] == $results['game']->_lid) {
                            $results["last_result"]["home"]["league"][] = $result1["r"];
                        }
                    }
                }
            }

            if (!empty($json["result2"])) {
                foreach ($json["result2"] as $result2) {
//                    if (!empty($result2)) {
//                        foreach ($result2['r'] as $key => $rs2) {
//                            if (empty($rs2)) {
//                                $result2['r'][$key] = 0;
//                            }
//                        }
//                    }
                    if (count($results["last_result"]["away"]["all"]) < 5) {
                        $results["last_result"]["away"]["all"][] = $result2["r"];
                    }
                    if (array_key_exists("leagueId", $result2)) {
                        if (count($results["last_result"]["away"]["league"]) < 5 && $result2["leagueId"] == $results['game']->_lid) {
                            $results["last_result"]["away"]["league"][] = $result2["r"];
                        }
                    }
                }
            }

            $results["compare"] = $json;
        }
    }
    $functionstop = Carbon::now();
    $results['functiontimes'] = $functionstop->diffInSeconds($functiostart);
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function sortedliveMatchOnly($id = null, $subleague = null)
{
    //date_default_timezone_set('Europe/London');
    $lang = !empty($id) ? $id : "en";
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array(
        'live_league' => array(),
        'live_sequence' => array(),
        'c3' => 0,
        'Teamlogos' => array(),
        'count' => array(),
        'hdp' => array(),
        'competitions' => array()
    );
    $tmpfile = "cronjob_gen_file/files/{$lang}/sortedliveMatchOnly.json";
    $newquery = false;
    if (file_exists($tmpfile)) {
        $mtime = filemtime($tmpfile);
        $updatetime = date('i', $mtime);
        $dt = date('i');

        if ((int)$dt != (int)$updatetime) {
            $newquery = true;
        } else {
            $newquery = false;
            $results = json_decode(file_get_contents($tmpfile), true);
            $results['source'] = 'file';
        }
    } else {
        $newquery = true;
    }

    if ($newquery) {
        //$filename = "http://ws.dowebsite.com/liveMatchOnly/$lang";
        $filename = "cronjob_gen_file/files/{$lang}/liveMatchOnly.json";
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
            //echo $json;
            $json = json_decode($json, true);
            $banned = array();
            $samenation = array();
            $banleague = array();
            $midlist = '0';
            foreach ($json["live_league"] as $league) {
                $results["live_league"][$league['subleagueId']]["data"] = $league;
                if (!array_key_exists($league["subleagueId"], $banleague)) {
                    $samenation[$league["competitionId"]][] = $league["subleagueId"];
                    $banleague[$league["subleagueId"]] = 1;
                    $results["live_sequence"][] = $league["subleagueId"];
                }
                $results["live_league"][$league['subleagueId']]["playable"] = 0;
                foreach ($json["live_match"] as $match) {
                    if ((int)$league["subleagueId"] == (int)$match["lid"]) {
                        if (!array_key_exists((int)$match["mid"], $banned)) {
                            $midlist .= ',' . $match['mid'];
                            $results["live_league"][$league['subleagueId']]["matches"][] = $match;
                            //$sql = "SELECT hdp,hdp_home,hdp_away,type,match_id FROM timelines_game WHERE mid={$match["mid"]}";
                            //$stmt = $db->query($sql);
                            //$rs = $stmt->fetch();
                            //if (!$rs) {

                            //} elseif (is_numeric($rs['hdp'])) {
                            //    $results["live_league"][$league['subleagueId']]["playable"]++;
                            //    $results["hdp"][$match['mid']]['hdp'] = $rs['hdp'];
                            //    $results["hdp"][$match['mid']]['hdp_home'] = $rs['hdp_home'];
                            //    $results["hdp"][$match['mid']]['hdp_away'] = $rs['hdp_away'];
                            //    $results["hdp"][$match['mid']]['type'] = $rs['type'];
                            $results["hdp"][$match['mid']]['hid'] = $match["hid"];
                            $results["hdp"][$match['mid']]['gid'] = $match["gid"];
                            $results["hdp"][$match['mid']]['homename'] = $match["hn"];
                            $results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                            $results["hdp"][$match['mid']]['date'] = $match["date"];
                            //    $results["hdp"][$match['mid']]['match_id'] = $rs["match_id"];
                            //}
                            $banned[(int)$match["mid"]] = 1;
                        }
                    }
                }
            }
            $sql = "SELECT hdp,hdp_home,hdp_away,type,match_id,mid,_lid
FROM timelines_game
WHERE mid IN ($midlist)";
            $stmt = $db->query($sql);
            $resultset = $stmt->fetchAll();
            foreach ($resultset as $rs) {
                if (array_key_exists($rs['_lid'], $results["live_league"])) {
                    $results["live_league"][$rs['_lid']]["playable"]++;
                    $results["hdp"][$rs['mid']]['hdp'] = $rs['hdp'];
                    $results["hdp"][$rs['mid']]['hdp_home'] = $rs['hdp_home'];
                    $results["hdp"][$rs['mid']]['hdp_away'] = $rs['hdp_away'];
                    $results["hdp"][$rs['mid']]['type'] = $rs['type'];
                    //$results["hdp"][$match['mid']]['hid'] = $match["hid"];
                    //$results["hdp"][$match['mid']]['gid'] = $match["gid"];
                    //$results["hdp"][$match['mid']]['homename'] = $match["hn"];
                    //$results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                    //$results["hdp"][$match['mid']]['date'] = $match["date"];
                    $results["hdp"][$rs['mid']]['match_id'] = $rs["match_id"];
                }
            }
            $results["c3"] = $json["c3"];
            $results["Teamlogos"] = $json["Teamlogos"];
            $results["count"] = $json["count"];
            $results["competitions"] = $samenation;
            file_put_contents("cronjob_gen_file/files/{$lang}/sortedliveMatchOnly.json", json_encode($results));
        }
    }
//    if (!empty($subleague)) {
//        $filename = "cronjob_gen_file/files/$subleague/competitions_sub.json";
//        if (file_exists($filename)) {
//            $sleagues = json_decode(file_get_contents($filename), true);
//            $results["competitions"] = $sleagues["competitions"];
//        }
//    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function sortedliveMatchWaitOnly($id = null, $subleague = NULL)
{
    $lang = !empty($id) ? $id : "en";    //echo $subleague;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array(
        'live_league' => array(),
        'live_sequence' => array(),
        'c3' => 0,
        'Teamlogos' => array(),
        'count' => array(),
        'hdp' => array(),
        'competitions' => array()
    );

    $tmpfile = "cronjob_gen_file/files/{$lang}/sortedliveMatchWaitOnly.json";
    $newquery = false;
    if (file_exists($tmpfile)) {
        $mtime = filemtime($tmpfile);
        $updatetime = date('i', $mtime);
        $dt = date('i');

        if ((int)$dt != (int)$updatetime) {
            $newquery = true;
        } else {
            $newquery = false;
            $results = json_decode(file_get_contents($tmpfile), true);
            $results['source'] = 'file';
        }
    } else {
        $newquery = true;
    }
    if ($newquery) {
        //$filename = "http://ws.dowebsite.com/liveMatchWaitOnly/$lang";
        $filename = "cronjob_gen_file/files/{$lang}/liveMatchWaitOnly.json";
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
            //echo $json;
            $json = json_decode($json, true);
            $banned = array();
            $samenation = array();
            $banleague = array();
            $midlist = '-1';
            foreach ($json["live_league"] as $league) {
                $results["live_league"][$league['subleagueId']]["data"] = $league;
                if (!array_key_exists($league["subleagueId"], $banleague)) {
                    $samenation[$league["competitionId"]][] = $league["subleagueId"];
                    $banleague[$league["subleagueId"]] = 1;
                    $results["live_sequence"][] = $league["subleagueId"];
                }
                $results["live_league"][$league['subleagueId']]["playable"] = 0;
                foreach ($json["live_match"] as $match) {
                    if ((int)$league["subleagueId"] == (int)$match["lid"]) {
                        if (!array_key_exists((int)$match["mid"], $banned)) {
                            $results["live_league"][$league['subleagueId']]["matches"][] = $match;
                            $midlist .= ',' . $match['mid'];
                            //$sql = "SELECT hdp,hdp_home,hdp_away,type,match_id FROM timelines_game WHERE mid={$match["mid"]}";
                            //$stmt = $db->query($sql);
                            //$rs = $stmt->fetch();
                            //if (!$rs) {

                            //} elseif (is_numeric($rs['hdp'])) {
                            //    $results["live_league"][$league['subleagueId']]["playable"]++;
                            //    $results["hdp"][$match['mid']]['hdp'] = $rs['hdp'];
                            //    $results["hdp"][$match['mid']]['hdp_home'] = $rs['hdp_home'];
                            //    $results["hdp"][$match['mid']]['hdp_away'] = $rs['hdp_away'];
                            //    $results["hdp"][$match['mid']]['type'] = $rs['type'];
                            //$results["hdp"][$match['mid']]['hid'] = $match["hid"];
                            //$results["hdp"][$match['mid']]['gid'] = $match["gid"];
                            //$results["hdp"][$match['mid']]['homename'] = $match["hn"];
                            //$results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                            //$results["hdp"][$match['mid']]['date'] = $match["date"];
                            //    $results["hdp"][$match['mid']]['match_id'] = $rs["match_id"];
                            //}
                            $banned[(int)$match["mid"]] = 1;
                        }
                    }
                }
            }
            $sql = "SELECT hdp,hdp_home,hdp_away,type,match_id,mid,_lid
FROM timelines_game tg
WHERE tg.mid IN ($midlist)
AND tg.hdp <>''
AND tg.match_id<>0";
            //echo $sql;exit;
            $stmt = $db->query($sql);
            $resultset = $stmt->fetchAll();
            foreach ($resultset as $rs) {
                if (array_key_exists($rs['_lid'], $results["live_league"])) {
                    //if (is_numeric($rs['hdp'])) {
                    $results["live_league"][$rs['_lid']]["playable"]++;
                    $results["hdp"][$rs['mid']]['hdp'] = $rs['hdp'];
                    $results["hdp"][$rs['mid']]['hdp_home'] = $rs['hdp_home'];
                    $results["hdp"][$rs['mid']]['hdp_away'] = $rs['hdp_away'];
                    $results["hdp"][$rs['mid']]['type'] = $rs['type'];
                    //$results["hdp"][$match['mid']]['hid'] = $match["hid"];
                    //$results["hdp"][$match['mid']]['gid'] = $match["gid"];
                    //$results["hdp"][$match['mid']]['homename'] = $match["hn"];
                    //$results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                    //$results["hdp"][$match['mid']]['date'] = $match["date"];
                    $results["hdp"][$rs['mid']]['match_id'] = $rs["match_id"];
                    //}
                }
            }
            $results["c3"] = $json["c3"];
            $results["Teamlogos"] = $json["Teamlogos"];
            $results["count"] = $json["count"];
            $results["competitions"] = $samenation;
            $results['sql'] = $sql;
            $results['source'] = 'db';
            file_put_contents("$tmpfile", json_encode($results));
        }
    }
//    if (!empty($subleague)) {
//        $filename = "cronjob_gen_file/files/$subleague/competitions_sub.json";
//        if (file_exists($filename)) {
//            $sleagues = json_decode(file_get_contents($filename), true);
//            $results["competitions"] = $sleagues["competitions"];
//        }
//    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function sortedliveMatchResultOnly($id = null, $subleague = NULL)
{
    $lang = !empty($id) ? $id : "en";    //echo $subleague;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array(
        'live_league' => array(),
        'live_sequence' => array(),
        'c3' => 0,
        'Teamlogos' => array(),
        'count' => array(),
        'hdp' => array(),
        'competitions' => array()
    );
    $tmpfile = "cronjob_gen_file/files/{$lang}/sortedliveMatchResultOnly.json";
    $newquery = false;
    if (file_exists($tmpfile)) {
        $mtime = filemtime($tmpfile);
        $updatetime = date('i', $mtime);
        $dt = date('i');

        if ((int)$dt - (int)$updatetime > 5 || (int)$updatetime - (int)$dt > 5) {
            $newquery = true;
        } else {
            $newquery = false;
            $results = json_decode(file_get_contents($tmpfile), true);
            $results['source'] = 'file';
        }
    } else {
        $newquery = true;
    }
    if ($newquery) {
        //$filename = "http://ws.dowebsite.com/liveMatchWaitOnly/$lang";
        $filename = "cronjob_gen_file/files/{$lang}/liveMatchResultOnly.json";
        if (file_exists($filename)) {
            $json = file_get_contents($filename);
            //echo $json;
            $json = json_decode($json, true);
            $banned = array();
            $samenation = array();
            $banleague = array();
            $midlist = '0';
            foreach ($json["live_league"] as $league) {
                $results["live_league"][$league['subleagueId']]["data"] = $league;
                if (!array_key_exists($league["subleagueId"], $banleague)) {
                    $samenation[$league["competitionId"]][] = $league["subleagueId"];
                    $banleague[$league["subleagueId"]] = 1;
                    $results["live_sequence"][] = $league["subleagueId"];
                }
                $results["live_league"][$league['subleagueId']]["playable"] = 0;
                foreach ($json["live_match"] as $match) {
                    if ($league["subleagueId"] == $match["lid"]) {
                        if (!array_key_exists((int)$match["mid"], $banned)) {
                            $results["live_league"][$league['subleagueId']]["matches"][] = $match;
                            $midlist .= ',' . $match['mid'];
                            //$sql = "SELECT hdp,hdp_home,hdp_away,type FROM timelines_game WHERE mid={$match["mid"]}";
                            //$stmt = $db->query($sql);
                            //$rs = $stmt->fetch();
                            //if (!$rs) {

                            //} elseif (is_numeric($rs['hdp'])) {
                            //    $results["live_league"][$league['subleagueId']]["playable"]++;
                            //    $results["hdp"][$match['mid']]['hdp'] = $rs['hdp'];
                            //    $results["hdp"][$match['mid']]['hdp_home'] = $rs['hdp_home'];
                            //    $results["hdp"][$match['mid']]['hdp_away'] = $rs['hdp_away'];
                            //    $results["hdp"][$match['mid']]['type'] = $rs['type'];
                            $results["hdp"][$match['mid']]['hid'] = $match["hid"];
                            $results["hdp"][$match['mid']]['gid'] = $match["gid"];
                            $results["hdp"][$match['mid']]['homename'] = $match["hn"];
                            $results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                            //    $results["hdp"][$match['mid']]['date'] = $match["date"];
                            //}
                            $banned[(int)$match["mid"]] = 1;
                        }
                    }
                }
            }
            $sql = "SELECT hdp,hdp_home,hdp_away,type,match_id,mid,_lid
FROM timelines_game
WHERE mid IN ($midlist)";
            $stmt = $db->query($sql);
            echo $sql;
            exit;
            $resultset = $stmt->fetchAll();
            foreach ($resultset as $rs) {
                if (array_key_exists($rs['_lid'], $results['live_league'])) {
                    if ($rs['hdp'] != "") {
                        $results["live_league"][$rs['_lid']]["playable"]++;
                        $results["hdp"][$rs['mid']]['hdp'] = $rs['hdp'];
                        $results["hdp"][$rs['mid']]['hdp_home'] = $rs['hdp_home'];
                        $results["hdp"][$rs['mid']]['hdp_away'] = $rs['hdp_away'];
                        $results["hdp"][$rs['mid']]['type'] = $rs['type'];
                        //$results["hdp"][$match['mid']]['hid'] = $match["hid"];
                        //$results["hdp"][$match['mid']]['gid'] = $match["gid"];
                        //$results["hdp"][$match['mid']]['homename'] = $match["hn"];
                        //$results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                        //$results["hdp"][$match['mid']]['date'] = $match["date"];
                        $results["hdp"][$rs['mid']]['match_id'] = $rs["match_id"];
                    }
                }
            }
            $results["c3"] = $json["c3"];
            $results["Teamlogos"] = $json["Teamlogos"];
            $results["count"] = $json["count"];
            $results["competitions"] = $samenation;
            file_put_contents("$tmpfile", json_encode($results));
        }
    }
//    if (!empty($subleague)) {
//        $filename = "cronjob_gen_file/files/$subleague/competitions_sub.json";
//        if (file_exists($filename)) {
//            $sleagues = json_decode(file_get_contents($filename), true);
//            $results["competitions"] = $sleagues["competitions"];
//        }
//    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function sortedyesterdayResultOnly($id = null, $subleague = NULL)
{
    $lang = !empty($id) ? $id : "en";    //echo $subleague;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array(
        'live_league' => array(),
        'live_sequence' => array(),
        'c3' => 0,
        'Teamlogos' => array(),
        'count' => array(),
        'hdp' => array(),
        'competitions' => array()
    );
    $tmpfile = "cronjob_gen_file/files/{$lang}/sortedliveMatchYesterdayResultOnly.json";
    $newquery = false;
    if (file_exists($tmpfile)) {
        $mtime = filemtime($tmpfile);
        $updatetime = date('i', $mtime);
        $dt = date('i');

        if ((int)$dt - (int)$updatetime > 30 || (int)$updatetime - (int)$dt > 30) {
            $newquery = true;
        } else {
            $newquery = false;
            $results = json_decode(file_get_contents($tmpfile), true);
            $results['source'] = 'file';
        }
    } else {
        $newquery = true;
    }
    if ($newquery) {
        //$filename = "http://ws.dowebsite.com/liveMatchWaitOnly/$lang";
        $filename = "cronjob_gen_file/files/{$lang}/liveMatchYesterdayResultOnly.json";
        if (file_exists($filename)) {
            //echo $json;
            $json = json_decode(file_get_contents($filename), true);
            $banned = array();
            $samenation = array();
            $banleague = array();
            $midlist = '0';
            foreach ($json["live_league"] as $league) {
                $results["live_league"][$league['subleagueId']]["data"] = $league;
                if (!array_key_exists($league["subleagueId"], $banleague)) {
                    $samenation[$league["competitionId"]][] = $league["subleagueId"];
                    $banleague[$league["subleagueId"]] = 1;
                    $results["live_sequence"][] = $league["subleagueId"];
                }
                $results["live_league"][$league['subleagueId']]["playable"] = 0;
                foreach ($json["live_match"] as $match) {
                    if ($league["subleagueId"] == $match["lid"]) {
                        if (!array_key_exists((int)$match["mid"], $banned)) {
                            $results["live_league"][$league['subleagueId']]["matches"][] = $match;
                            $midlist .= ',' . $match['mid'];
                            //$sql = "SELECT hdp,hdp_home,hdp_away,type FROM timelines_game WHERE mid={$match["mid"]}";
                            //$stmt = $db->query($sql);
                            //$rs = $stmt->fetch();
                            //if (!$rs) {

                            //} elseif (is_numeric($rs['hdp'])) {
                            //    $results["live_league"][$league['subleagueId']]["playable"]++;
                            //    $results["hdp"][$match['mid']]['hdp'] = $rs['hdp'];
                            //    $results["hdp"][$match['mid']]['hdp_home'] = $rs['hdp_home'];
                            //    $results["hdp"][$match['mid']]['hdp_away'] = $rs['hdp_away'];
                            //    $results["hdp"][$match['mid']]['type'] = $rs['type'];
                            $results["hdp"][$match['mid']]['hid'] = $match["hid"];
                            $results["hdp"][$match['mid']]['gid'] = $match["gid"];
                            $results["hdp"][$match['mid']]['homename'] = $match["hn"];
                            $results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                            //    $results["hdp"][$match['mid']]['date'] = $match["date"];
                            //}
                            $banned[(int)$match["mid"]] = 1;
                        }
                    }
                }
            }
            $sql = "SELECT hdp,hdp_home,hdp_away,type,match_id,mid,_lid
FROM timelines_game
WHERE mid IN ($midlist)";
            $stmt = $db->query($sql);
            $resultset = $stmt->fetchAll();
            foreach ($resultset as $rs) {
                if (array_key_exists($rs['_lid'], $results["live_league"])) {
                    $results["live_league"][$rs['_lid']]["playable"]++;
                    $results["hdp"][$rs['mid']]['hdp'] = $rs['hdp'];
                    $results["hdp"][$rs['mid']]['hdp_home'] = $rs['hdp_home'];
                    $results["hdp"][$rs['mid']]['hdp_away'] = $rs['hdp_away'];
                    $results["hdp"][$rs['mid']]['type'] = $rs['type'];
                    //$results["hdp"][$match['mid']]['hid'] = $match["hid"];
                    //$results["hdp"][$match['mid']]['gid'] = $match["gid"];
                    //$results["hdp"][$match['mid']]['homename'] = $match["hn"];
                    //$results["hdp"][$match['mid']]['awayname'] = $match["gn"];
                    //$results["hdp"][$match['mid']]['date'] = $match["date"];
                    $results["hdp"][$rs['mid']]['match_id'] = $rs["match_id"];
                }
            }
            $results["c3"] = $json["c3"];
            $results["Teamlogos"] = $json["Teamlogos"];
            $results["count"] = $json["count"];
            $results["competitions"] = $samenation;
            file_put_contents("$tmpfile", json_encode($results));
        }
    }
//    if (!empty($subleague)) {
//        $filename = "cronjob_gen_file/files/$subleague/competitions_sub.json";
//        if (file_exists($filename)) {
//            $sleagues = json_decode(file_get_contents($filename), true);
//            $results["competitions"] = $sleagues["competitions"];
//        }
//    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($results);
}

function initProfile()
{
    $start = date('Y-m-d h:i:s');
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $results = array("follwer" => array(), "following" => array(), "facebookinfo" => array(), "ranking" => array(), "betlist" => array(), "matchcommentlist" => array());
    if (!empty($fb_uid)) {
        $results["follwer"] = getfollowers($fb_uid);
        $results["following"] = getfollowing($fb_uid);
        $json = json_decode(file_get_contents("facebook_info/{$fb_uid}.json"), true);
        $results["facebookinfo"] = $json;
        $ranks = getRankingV4(0, 10, "all", "all", $fb_uid);
        $results["ranking"] = $ranks;
        $blist = json_decode(file_get_contents("bet_List/{$fb_uid}.json"), true);
        $results["betlist"] = $blist;
        $results["matchcommentlist"] = matchCommentList($fb_uid);
    }
    $stop = date('Y-m-d h:i:s');
    $results["time"] = strtotime($stop) - strtotime($start);
    echo json_encode($results);
}

function fbPoster($token, $msg, $name, $desc, $caption, $link, $picture)
{
    $postable = FALSE;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $results = array("success" => $postable, "msg" => "");
    $facebook = new Facebook(array(
        'appId' => '400809029976308',
        'secret' => '93e21cf1551a4536d20843fcdf236402',
    ));
    if (!empty($token)) {
        $facebook->setAccessToken($token);
        $user_id = $facebook->getUser();
        $tmplink = "fb400809029976308://authorize#expires_in=0&access_token=$token&target_url=$link";
        if (!empty($user_id)) {
            $sql = "SELECT * FROM fb_post WHERE fb_uid='$user_id' ORDER BY id DESC";
            $stmt = $db->query($sql);
            $lastpost = $stmt->fetch(5);
            $allow = TRUE;
            if (!empty($lastpost)) {
                $now = strtotime(date("Y-m-d H:i:s"));
                $lasttime = strtotime($lastpost->last_post . "+3hours");
                if ($now >= $lasttime) {
                    $allow = TRUE;
                } else {
                    $allow = FALSE;
                }
            }
            if ($allow) {
                try {
                    $ret_obj = $facebook->api('/me/feed', 'POST', array(
                        'link' => $link,
                        'message' => $msg,
                        'application' => '400809029976308',
                        'is_hidden' => TRUE,
                        'picture' => $picture,
                        "name" => "$name",
                        "caption" => "$caption",
                        "description" => "$desc"
                    ));
                    $postable = TRUE;
                    $addsql = "INSERT INTO `fb_post` (`fb_uid`, `last_post`) VALUES ('$user_id', NOW())";
                    $db->exec($addsql);
                    //echo "post";
                } catch (FacebookApiException $e) {
//            $login_url = $facebook->getLoginUrl(array(
//                'scope' => 'publish_actions'
//            ));
                    //echo "fail";
                    $results["msg"] = $e->getType() . ":" . $e->getMessage();
                    error_log($e->getType());
                    error_log($e->getMessage());
                }
            }
        }
    } else {
        $results["msg"] = "empty token";
    }
    //querylog(__FUNCTION__, $db);
    $results["success"] = $postable;
    return $results;
}

function initTrophydesc()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $pos = array("st", "nd", "rd", "th", "th", "th", "th", "th", "th", "th");
    $type = array("14d" => "NOW", "2w" => "2 WEEK", "all" => "ALL TIME", "r100" => "RATING", "r200" => "RATING");
    $sql = "SELECT * FROM trophy ORDER BY type,sequence";
    $stmt = $db->query($sql);
    $trophy = $stmt->fetchAll(5);
    $result = array();
    foreach ($trophy as $t) {
        $i = (int)$t->sequence - 1;
        //$desc = "{$t->sequence}{$pos[$i]} rank in {$type[$t->type]}";
        $desc = "{$type[$t->type]}";
        $updatesql = "UPDATE `trophy` SET `desc`='$desc' WHERE  `id`='{$t->id}'";
        //echo $updatesql . ";\n";
        $success = $db->exec($updatesql);
        if ($success) {
            $result[] = $desc;
        }
    }


    $dt = mktime($hour, $minute);
    echo json_encode($result);
}

function worldcupsRanking()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $fb_uid = isset($_REQUEST['fb_uid']) ? $_REQUEST['fb_uid'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 40;
    $results = array("success" => false, "ranks" => array(), "own" => array(), "curdate" => "0000-00-00");
    $lastdaysql = "SELECT update_at FROM worldcups_ranking ORDER BY update_at DESC limit 1";
    $stmt = $db->query($lastdaysql);
    $lastday = $stmt->fetchColumn();
    $own = array();
    //var_dump($lastday);
    $ranksql = "SELECT fb.fb_firstname,fb.w,fb.d,fb.l,fb.display_name,fb.overall_gp as user_gp,wr.* FROM worldcups_ranking  wr
LEFT JOIN facebook_user fb ON wr.fb_uid=fb.fb_uid
WHERE wr.update_at='$lastday'
ORDER BY rank
limit $limit
";
    $stmt = $db->query($ranksql);
    $ranks = $stmt->fetchAll(5);

    if ($fb_uid) {
        $ranksql = "SELECT fb.fb_firstname,fb.w,fb.d,fb.l,fb.display_name,fb.overall_gp as user_gp,wr.* FROM worldcups_ranking  wr
LEFT JOIN facebook_user fb ON wr.fb_uid=fb.fb_uid
WHERE wr.update_at='$lastday'
AND wr.fb_uid='$fb_uid'
";
        $stmt = $db->query($ranksql);
        $own = $stmt->fetch(5);
    }
    if ($ranks) {
        $results["success"] = true;
        $results['curdate'] = $lastday;
        $results['ranks'] = $ranks;
        $results['own'] = $own;
    }
    echo json_encode($results);
}

function initDisplayName()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "SELECT * FROM facebook_user WHERE display_name IS NULL OR display_name =''";
    $stmt = $db->query($sql);
    $userlist = $stmt->fetchAll(5);
    foreach ($userlist as $user) {
        if (is_numeric($user->fb_uid)) {
            $display = $user->fb_firstname;
            if ($user->user_status) {
                $display = $user->user_status;
            }
            $display = addslashes($display);
            $db->exec("UPDATE `facebook_user` SET `display_name`='$display' WHERE  `fb_uid`='{$user->fb_uid}'");
            FBtofile($user->fb_uid);
        }
    }
}

function regEvent($db, $fb_uid, $type, $sys_smg, $smg, $linktype, $key, $target = null)
{
    //$db = DBConfig::getConnection();
    $smg = addslashes($smg);
    $day = date("Y-m-d H:i:s");
    $stamp = strtotime($day);

    $sql = "INSERT INTO `event_list` (`fb_uid`, `type`, `sys_msg`, `message`,`link_type`,`link_key`, `event_at`, `event_stamp`) VALUES ('$fb_uid', '$type', '$sys_smg', '$smg','$linktype','$key', '$day', '$stamp');";
    $success = $db->exec($sql);
    $idsql = "SELECT LAST_INSERT_ID();";
    $stmt = $db->query($idsql);
    $lastid = $stmt->fetchColumn();
    if ($target) {
        $rsql = "INSERT INTO `event_list_related` (`rfb_uid`, `related_to`) VALUES ('$target', $lastid);";
        $db->exec($rsql);
    } else {


        $friend_sql = "SELECT id FROM
(
SELECT
ff.friend_facebook_id as id
FROM  facebook_friends ff
WHERE ff.facebook_id ='$fb_uid'
UNION
SELECT
    fl.fb_uid as id
    FROM follow fl
    WHERE fl.fb_follow_uid ='$fb_uid'
)
AS userlist
GROUP BY id";


        $super = getSu();
        if ($fb_uid == $super) {
            if ($type == 'post' and $linktype == 'timeline') {
                $nowday = date("Y-m-d");
                $lastlogin = date('Y-m-d', strtotime($nowday . "-30 day"));
                $friend_sql = "SELECT fb_uid as id FROM facebook_user WHERE last_login_date>='$lastlogin'";
                //echo $friend_sql;exit();
            }
        }

        $stmtfriend = $db->query($friend_sql);
        $userlist = $stmtfriend->fetchAll(5);
        $alluser = count($userlist);

        $subsql = "";
        foreach ($userlist as $user) {
//            $rsql = "INSERT INTO `event_list_related` (`rfb_uid`, `related_to`) VALUES ('{$user->id}', $lastid);";
//            $db->exec($rsql);
            if (empty($subsql)) {
                $subsql = "('{$user->id}', $lastid)";
            } else {
                $subsql .= "," . "('{$user->id}', $lastid)";
            }
        }
        if (!empty($subsql)) {
            $rsql = "INSERT INTO `event_list_related` (`rfb_uid`, `related_to`) VALUES $subsql;";
            $affectrow = $db->exec($rsql);
//            echo $afrow;
//            exit();
        }
    }
}

function getNotifications($fb_uid)
{
    $startf = Carbon::now();
    $results = array('success' => false, 'DESC' => 'nothing', 'all' => array('list' => array(), 'quantity' => 0, 'unseen' => 0), 'post' => array('list' => array(), 'quantity' => 0, 'unseen' => 0), 'game' => array('list' => array(), 'quantity' => 0, 'unseen' => 0), 'follow' => array('list' => array(), 'quantity' => 0, 'unseen' => 0));
    $cachefile = "cache/notification/$fb_uid.json";
    $newquery = true;
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $dt = date('i');
        $filedt = date('i', $filem);
        if (abs((int)$dt - (int)$filedt) >= 5) {
            $newquery = false;
            $results = json_decode(file_get_contents($cachefile), true);
        }
    }
    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");


        $sql = "
SELECT elr.id noti_id,elr.*,el.*,fb.display_name FROM `event_list_related` elr
LEFT JOIN `event_list` el ON elr.related_to=el.id
LEFT JOIN `facebook_user` fb ON el.fb_uid=fb.fb_uid
WHERE elr.rfb_uid='$fb_uid'
AND elr.remove='N'
ORDER BY elr.id DESC LIMIT 100";
        $stmt = $db->query($sql);
        $notifications = $stmt->fetchAll(5);
        $index = date('Y-m-d');
        $key = 0;
        foreach ($notifications as $key => $notification) {
            $results['success'] = true;
            $noday = date("Y-m-d", strtotime($notification->event_at));
//        if ($index != $noday) {
//            $index = $noday;
//            $key++;
//        }
            if ($key < 100) {
                $results['all']['list'][$key][] = $notification;
            }
            $results['all']['quantity']++;
            if ($notification->seen == 'N') {
                $results['all']['unseen']++;
            }
            if ($notification->link_type == 'game') {
                $results['game']['list'][$key][] = $notification;
                $results['game']['quantity']++;
                if ($notification->seen == 'N') {
                    $results['game']['unseen']++;
                }
            } else if ($notification->link_type == 'timeline') {
                $results['post']['list'][$key][] = $notification;
                $results['post']['quantity']++;
                if ($notification->seen == 'N') {
                    $results['post']['unseen']++;
                }
            } else if ($notification->link_type == 'follow') {
                $results['follow']['list'][$key][] = $notification;
                $results['follow']['quantity']++;
                if ($notification->seen == 'N') {
                    $results['follow']['unseen']++;
                }
            }
        }
        file_put_contents($cachefile, json_encode($results));
        //querylog(__FUNCTION__, $db);
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
    }
    echo json_encode($results);
}

function notiSeen($fb_uid, $noti_id = 0)
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $result = array('success' => false, 'decs' => 'nothing');
    $sql = "";
    if ($noti_id) {
        $sql = "UPDATE `event_list_related` SET `seen`='Y' WHERE  `id`=$noti_id;";
        $result['desc'] = "seen $noti_id";
    } else {
        $sql = "UPDATE `event_list_related` SET `seen`='Y' WHERE  `rfb_uid`='$fb_uid';";
        $result['desc'] = "seen all";
    }
    $success = $db->exec($sql);
    if ($success) {
        $result['success'] = true;
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
    //getNotifications($fb_uid);
}

function notiRemove($fb_uid, $noti_id = 0)
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "";
//    if ($noti_id) {
//        $sql = "UPDATE `event_list_related` SET `remove`='Y' WHERE  `id`=$noti_id;";
//    } else {
//        $sql = "UPDATE `event_list_related` SET `remove`='Y' WHERE  `rfb_uid`='$fb_uid';";
//    }
    if ($noti_id) {
        $sql = "DELETE FROM `event_list_related` WHERE  `id`=$noti_id;";
    } else {
        $sql = "DELETE FROM `event_list_related` WHERE  `rfb_uid`='$fb_uid';";
    }
    $db->exec($sql);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    getNotifications($fb_uid);
}

function dailyAlert($fb_uid)
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $yesterday = strtotime(date("Y-m-d", strtotime(date("Y-m-d") . "-1 day")));
    $day = strtotime(date("Y-m-d"));
    $now = date("Y-m-d");
    $yday = date("Y-m-d", strtotime(date("Y-m-d") . "-1 day"));

    $result = array("success" => false, "desc" => "seen", "user" => array(), "betlist" => array(), "rank" => 0, 'playdate' => $yday, 'play' => 0, 'win' => 0, 'lose' => 0, 'draw' => 0, 'wait' => 0, 'pta' => 0.0, 'gp' => 0.0, 'rating' => 0.0, 'score' => 0.0);


    $sql = "SELECT * FROM facebook_user WHERE fb_uid='$fb_uid'";
    $stmt = $db->query($sql);
    $user = $stmt->fetch(5);
    $result['user'] = $user;

    $sql = "SELECT * FROM bet b
LEFT JOIN timelines_game tg ON b.mid=tg.mid
WHERE b.fb_uid='$fb_uid'
AND b.betDatetime BETWEEN $yesterday AND $day
";

    //echo $sql;
    $stmt = $db->query($sql);
    $list = $stmt->fetchAll(5);

    $play = 0;
    $win = 0;
    $draw = 0;
    $lose = 0;
    $wait = 0;
    $pta = 0.0;
    $gp = 0.0;
    if ($user->pts == 0) {
        $rating = 0;
    } else {
        $rating = number_format(floatval($user->w / ($user->pts - $user->d) * 100), 2);
    }
    foreach ($list as $bet) {
        $play++;
        $gp += floatval($bet->betAmount);
        $result['betlist'][] = $bet;
        if ($bet->result == 'wait') {
            $wait++;
        } else {
            if ($bet->result == 'win') {
                $win++;
            } elseif ($bet->result == 'lose') {
                $lose++;
            } else {
                $draw++;
            }
        }
    }

    if (($win + $lose + $draw) == 0) {
        $pta = 0;
    } else {
        $pta = number_format(floatval(($win / ($win + $lose)) * 100), 2);
    }
    $result['play'] = $play;
    $result['win'] = $win;
    $result['lose'] = $lose;
    $result['draw'] = $draw;
    $result['wait'] = $wait;
    $result['pta'] = $pta;
    $result['rating'] = $rating;
    $result['score'] = $gp;


    // $result['betlist'] = $list;

    $json = json_decode(file_get_contents("ranking/alllist.json"), true);
    $rank = $json[$fb_uid];
    $result['rank'] = $rank;


    $result['success'] = true;
    $result['desc'] = "success";

//    $sql = "SELECT * FROM get_alert WHERE fb_uid='$fb_uid' AND alert_at='$now'";
//    $stmt = $db->query($sql);
//    $got = $stmt->fetch(5);
//    if (!$got) {
//        $db->exec("INSERT INTO `get_alert` (`fb_uid`, `alert_at`) VALUES ('$fb_uid', '$now');");
//    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function getTopictype()
{
    $startf = Carbon::now();
    $file = "cache/topictype.json";
    $newquery = false;
    $list = array();
    if (file_exists($file)) {
        $filem = filemtime($file);
        $dn = date('d');
        $fileday = date('d', $filem);
        if ($dn != $fileday) {
            $newquery = true;
        } else {
            $list = json_decode(file_get_contents($file), true);
        }
    } else {
        $newquery = true;
    }
    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT * FROM feed_type ORDER BY sequence";
        $stmt = $db->query($sql);
        $list = $stmt->fetchAll(5);
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
        file_put_contents($file, json_encode($list));
    }
    echo json_encode($list);
}

function initTlkey()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $hasnext = true;
    $i = 0;
    $start = $i * 100;
    while ($hasnext) {
        $sql = "SELECT * FROM `timelines` WHERE `keygen`='0' LIMIT $start,100";
        $stmt = $db->query($sql);
        $timelines = $stmt->fetchAll(5);
        //echo $sql . "\n";
        //var_dump($timelines);
        if ($timelines) {
            foreach ($timelines as $timeline) {
                $sql = "UPDATE `timelines` SET `keygen`='" . $timeline->content_type . $timeline->key_list . "' WHERE  `id`={$timeline->id};";
                $db->exec($sql);
                echo $sql . "\n";
            }
            $i++;
            $start = $i * 100;
        } else {
            $hasnext = FALSE;
        }
    }
}

function newTopic()
{
    $startf = Carbon::now();
    $uid = isset($_REQUEST["uid"]) ? $_REQUEST["uid"] : "";
    $fb_uid = isset($_REQUEST["fb_uid"]) ? $_REQUEST["fb_uid"] : "";
    $message = isset($_REQUEST["message"]) ? $_REQUEST["message"] : '';
    $pix = isset($_REQUEST["picture"]) ? $_REQUEST["picture"] : "";
    $video = isset($_REQUEST['video']) ? $_REQUEST['video'] : "";
    $title = isset($_REQUEST['title']) ? $_REQUEST['title'] : "";
    $category = isset($_REQUEST['category']) ? $_REQUEST['category'] : 7;
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $day = date("Y-m-d H:i:s");
    $stamp = strtotime($day);
    $videos = array();
    $images = array();
    $mediatype = 'none';
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array());
    if (!empty($uid) && !empty($fb_uid)) {
        if (!empty($title) || !empty($message)) {
            $message = addslashes($message);
            $title = addslashes($title);


            $keylist = "";
            if (!empty($pix)) {
                $pixlist = explode(",", $pix);
                //var_dump($pixlist);
                foreach ($pixlist as $img) {
                    $imgs = explode("|", $img);
                    $pixinsert = "";
                    if (count($imgs) == 2) {
                        $pixinsert = "INSERT INTO media_store (`gall_id`,`path`,`thumbnail`,`created_at`) VALUES ('0','{$imgs[0]}','{$imgs[1]}',NOW())";
                        $db->exec($pixinsert);
                        $lastid = $db->lastInsertId();
                        // echo $lastid;
                        if (!empty($keylist)) {
                            $keylist .= "," . $lastid;
                        } else {
                            $keylist .= $lastid;
                        }
                        //echo $keylist . "\n";
                        $lastimg = "SELECT * FROM media_store WHERE id IN ($lastid)";
                        $limgstmt = $db->query($lastimg);
                        $images = $limgstmt->fetch(5);
                    }
                }
                $mediatype = 'image';
            }


            if (!empty($video)) {
                $pixinsert = "INSERT INTO media_store (`gall_id`,`path`,`thumbnail`,`created_at`) VALUES ('0','$video','$video',NOW())";
                $db->exec($pixinsert);
                $keylist = $db->lastInsertId();
                $mediatype = 'video';
            }
            $sql = "INSERT INTO `board` (`uid`, `title`, `desc`, `media`,`media_type`, `categoty`) VALUES ('$uid', '$title', '$message', '$keylist','$mediatype', '$category');";
            $db->exec($sql);
            $lastid = $db->lastInsertId();
            $results = TimelineTrigger($fb_uid, "board", $lastid, $category);
        }
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function setCatview($uid, $list = 0)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $category = array();
    if ($list) {
        $sql = "SELECT * FROM feed_type WHERE fid IN ($list)";
        $stmt = $db->query($sql);
        $category = $stmt->fetchAll(5);
    } else {

    }
    $jsn = json_encode($category);
    file_put_contents("wb_setting/$uid.json", $jsn);
    //chmod("wb_setting/$uid.json", 0777);
    echo $jsn;
}

function initCatview($uid)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $category = array();
    $sql = "SELECT * FROM feed_type";
    $stmt = $db->query($sql);
    $category = $stmt->fetchAll(5);
    $jsn = json_encode($category);
    file_put_contents("wb_setting/$uid.json", $jsn);
    //chmod("wb_setting/$uid.json", 0777);
}

function getCatview($uid)
{
    $file = "wb_setting/$uid.json";
    if (!file_exists($file)) {
        initCatview($uid);
    }
    $jsn = file_get_contents($file);
    echo $jsn;
}

function getReccommend($cat = 0)
{


    $startf = Carbon::now();
    $topics = "1,2,3,4,5,6,7,8";
    if ($cat) {
        $topics = $cat;
    }
    $results = array("timelines" => array(), "main_reply" => array(), "sub_reply" => array(), "related" => array());
    $cachefile = "cache/topic/$topics.json";
    $newquery = true;
    if (file_exists($cachefile)) {
        $filem = filemtime($cachefile);
        $dt = date('H');
        $filedt = date('H', $filem);
        if ($dt == $filedt) {
            $results = json_decode(file_get_contents($cachefile), true);
            $newquery = false;
        }
    }

    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND tl.type IN ($topics)
AND tl.pin='Y'
GROUP BY tl.keygen
ORDER BY tl.updated_at DESC
";

        $tlstmt = $db->query($sql);
        $timelines = $tlstmt->fetchAll(5);

        foreach ($timelines as $wall) {
            $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
            $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$wall->content_type}'
AND tr.key_list='{$wall->key_list}'
";

            $relatestmt = $db->query($relatesql);
            $allrelate = $relatestmt->fetchAll(5);
            foreach ($allrelate as $rel) {
                if ($rel->related_type == "post") {
                    $related["post"][] = $rel;
                } else if ($rel->related_type == "comment") {
                    $related["comment"][] = $rel;
                } else if ($rel->related_type == "like") {
                    $related["like"][] = $rel;
                } else if ($rel->related_type == "report") {
                    $related["report"][] = $rel;
                }
            }
            $results["related"][$wall->id] = $related;
            $tl = array("owner" => array(), "content" => array());
            $tl["owner"] = $wall;
            $rs = getTlContent($db, $wall->content_type, $wall->key_list);
            if (!empty($rs[0])) {
                if ($wall->content_type == "game") {
                    $ownbetsql = "SELECT oddsTimestamp FROM bet WHERE `fb_uid`='$fb_uid' AND `mid`='{$wall->key_list}'";
                    $ownbetstmt = $db->query($ownbetsql);
                    $ownbet = $ownbetstmt->fetch();
                    if ($ownbet) {
                        $json = json_decode($ownbet['oddsTimestamp'], true);
                        $rs[0]->hdp = $json['hdp'];
                        $rs[0]->hdp_home = $json['hdp_home'];
                        $rs[0]->hdp_away = $json['hdp_away'];
                    }
                }

                foreach ($rs[0] as $key => $value) {
                    $newkey = strtolower($key);
                    // echo $newkey.":".$rs[0]->$key."\n";
                    $rs[0]->$newkey = $value;
                    // unset($rs[0]->$key);
                }
                if ($wall->content_type == "game") {
                    $mof = array();
                    if (file_exists('matches_comment/' . $wall->key_list . '.json'))
                        $mof = json_decode(file_get_contents('matches_comment/' . $wall->key_list . '.json'), true);
                    $rs[0]->betdata = $mof;
                    $stsql = "SELECT tid,leagueId,no number,ml lastplay FROM stat_table WHERE leagueId={$rs[0]->_lid} AND tid IN ({$rs[0]->hid},{$rs[0]->gid})";
                    $ststmt = $db->query($stsql);
                    $teamstat = $ststmt->fetchAll(5);
                    $rs[0]->stat = $teamstat;
                }
            }
            $tl["content"] = $rs;
            if ($wall->content_type == "image") {
                $tl["owner"]->desc = $rs[0]->desc;
                $tl["owner"]->like = $rs[0]->like;
                $tl["owner"]->report = $rs[0]->report;
            }
            if (!empty($tl["content"][0])) {
                $results["timelines"][] = $tl;
//
//            if ($viewtype != 'board') {
//                $replysql = "";
//                if ($wall->content_type == "game") {
//                    $replysql = "SELECT fb.display_name,fb.fb_firstname,b.parent_id as parent, b.* FROM comment_on_match b
//LEFT JOIN facebook_user fb ON fb.fb_uid=b.fb_uid
//WHERE
//b.match_id={$wall->key_list}
//AND b.message IS NOT NULL
//AND b.message <>''
//AND b.fb_uid IN  $knowuser
//AND b.remove='N'
//ORDER BY  b.id DESC
//";
//                } else {
//                    $replysql = "SELECT fb.display_name,fb.fb_firstname,tr.*
//FROM timelines_reply tr
//LEFT JOIN facebook_user fb ON fb.fb_uid=tr.fb_uid
//WHERE tr.reply_to ={$wall->id}
//AND tr.remove='N'
//ORDER BY tr.created_at
//";
//                }
//                $srstmt = $db->query($replysql);
//                $allreply = $srstmt->fetchAll(5);
//                foreach ($allreply as $indreply) {
//                    if ((int) $indreply->parent_id == 0) {
//                        $results["main_reply"][(int) $wall->id][] = $indreply;
//                    } else {
//                        $results["sub_reply"][(int) $indreply->parent_id][] = $indreply;
//                    }
//                }
//            }
            }
        }
        //querylog(__FUNCTION__, $db);
        $stop = Carbon::now();
        $duration = $stop->micro - $startf->micro;
        DBConfig::closeConnection($db, __FUNCTION__, $duration);
        file_get_contents($cachefile, json_encode($results));
    }
    echo json_encode($results);
}

function getRec($db, $offset, $limit = 10, $cat = 0, $order = null)
{
    //$db = DBConfig::getConnection();

    $dir = 'topics';
    if (!is_dir($dir)) {
        mkdir($dir);
        //chmod($dir, 0777);
    }
    $topics = "1,2,3,4,5,6,7,8";
    if ($cat) {
        $topics = $cat;
    }
    $tfile = $dir . "/$topics.json";
    $usingfile = FALSE;
    if (file_exists($tfile)) {
        $lastmod = date('h', filemtime($tfile));
        $currenthour = date('h');
        if ($lastmod == $currenthour) {
            $usingfile = true;
        }
    }
    //var_dump($usingfile);exit();
    $results = array("timelines" => array(), "related" => array(), "from" => 'file');
    if (!$usingfile) {
        $orderby = "ORDER BY tl.updated_at DESC";
        $pin = "AND tl.pin = 'Y'";
        $admin = "";
        if ($order == 'view') {
            $orderby = "ORDER BY tl.view DESC";
            $pin = "";
        } else if ($order == 'admin') {
            $admin = "AND tl.fb_uid IN (" . getSu() . ")";
            $orderby = "ORDER BY tl.id";
            $pin = "";
        }

        $start = $offset * $limit;

        $sql = "SELECT tl.*,fb.fb_firstname,fb.display_name FROM `timelines` tl
LEFT JOIN facebook_user fb ON tl.fb_uid=fb.fb_uid
WHERE tl.remove='N'
AND tl.type IN ($topics)
$admin
$pin
AND last_event='Y'
#GROUP BY tl.keygen
$orderby
 LIMIT $start,$limit
";

        $tlstmt = $db->query($sql);
        $timelines = $tlstmt->fetchAll(5);

        foreach ($timelines as $wall) {
            $related = array("post" => array(), "comment" => array(), "like" => array(), "report" => array());
            $relatesql = "SELECT tr . * , fb.fb_firstname, fb.display_name
FROM timelines_related tr
LEFT JOIN facebook_user fb ON tr.fb_uid = fb.fb_uid
WHERE tr.content_type='{$wall->content_type}'
AND tr.key_list='{$wall->key_list}'
";

            $relatestmt = $db->query($relatesql);
            $allrelate = $relatestmt->fetchAll(5);
            foreach ($allrelate as $rel) {
                if ($rel->related_type == "post") {
                    $related["post"][] = $rel;
                } else if ($rel->related_type == "comment") {
                    $related["comment"][] = $rel;
                } else if ($rel->related_type == "like") {
                    $related["like"][] = $rel;
                } else if ($rel->related_type == "report") {
                    $related["report"][] = $rel;
                }
            }
            $results["related"][$wall->id] = $related;
            $tl = array("owner" => array(), "content" => array());
            $tl["owner"] = $wall;
            $rs = getTlContent($db, $wall->content_type, $wall->key_list);
            if (!empty($rs[0])) {
                if ($wall->content_type == "game") {
                    $ownbetsql = "SELECT oddsTimestamp FROM bet WHERE `fb_uid`='{$wall->fb_uid}' AND `mid`='{$wall->key_list}'";
                    $ownbetstmt = $db->query($ownbetsql);
                    $ownbet = $ownbetstmt->fetch();
                    if ($ownbet) {
                        $json = json_decode($ownbet['oddsTimestamp'], true);
                        $rs[0]->hdp = $json['hdp'];
                        $rs[0]->hdp_home = $json['hdp_home'];
                        $rs[0]->hdp_away = $json['hdp_away'];
                    }
                }

                foreach ($rs[0] as $key => $value) {
                    $newkey = strtolower($key);
                    // echo $newkey.":".$rs[0]->$key."\n";
                    $rs[0]->$newkey = $value;
                    // unset($rs[0]->$key);
                }
                if ($wall->content_type == "game") {
                    $mof = array();
                    if (file_exists('matches_comment/' . $wall->key_list . '.json'))
                        $mof = json_decode(file_get_contents('matches_comment/' . $wall->key_list . '.json'), true);
                    $rs[0]->betdata = $mof;
                    $stsql = "SELECT tid,leagueId,no number,ml lastplay FROM stat_table WHERE leagueId={$rs[0]->_lid} AND tid IN ({$rs[0]->hid},{$rs[0]->gid})";
                    $ststmt = $db->query($stsql);
                    $teamstat = $ststmt->fetchAll(5);
                    $rs[0]->stat = $teamstat;
                }
            }
            $tl["content"] = $rs;
            if ($wall->content_type == "image") {
                $tl["owner"]->desc = $rs[0]->desc;
                $tl["owner"]->like = $rs[0]->like;
                $tl["owner"]->report = $rs[0]->report;
            }
            if (!empty($tl["content"][0])) {
                $results["timelines"][] = $tl;
            }
        }
        file_put_contents($dir . "/$topics.json", json_encode($results));
        $results["from"] = 'db';
    } else {
        $results = json_decode(file_get_contents($dir . "/$topics.json"), true);
        $results["from"] = 'file';
    }
    return $results;
}

function dailyBonus($uid)
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $tt = new TotalStatement();
    //$newlogin = $tt->continueLoginBonus($uid);
//    if ($newlogin) {
//        $tt->todayBonus($uid);
//    }
    $config = json_decode(file_get_contents('system_config/file.json'));
    $sql = "select *,datediff(current_date,last_login_date) as datediff from facebook_user where uid=$uid ";
    $row = $db->query($sql)->fetch(PDO::FETCH_OBJ);
    $newlogin = FALSE;
    if ($row) {
        //echo "dateDiff:" . $row->datediff . "\n";
        $login_day_count = 0;
        if ($row->datediff == null || $row->datediff == 1) {
            $login_day_count = $row->login_day_count;
            $login_day_count++;
        } else if ($row->datediff > 1) {
            $login_day_count = 1;
        }
        $login_day_count = (int)$login_day_count > 7 ? 7 : (int)$login_day_count;
        if ($row->datediff != 0) {
            $login_sgold_array = $config->login_sgold;
            $login_scoin_array = $config->login_scoin;
            $sgold_income = $login_sgold_array[$login_day_count - 1];
            $scoin_income = $login_scoin_array[$login_day_count - 1];
            $before_sgold = $row->sgold;
            $before_scoin = $row->scoin;

            $tsgold = $before_sgold + $sgold_income;
            $tscoin = $before_scoin + $scoin_income;
            // $tt->insertSt atement('today_bonus', "'login$login_day_count'", $uid, 'success', $before_sgold, $before_scoin, $sgold_income, $scoin_income, 0, 0);
            $sql = "INSERT INTO `total_statement` (`uid`, `statement_type`, `type_addition`, `before_sgold`, `before_scoin`, `sgold_income`, `scoin_income`, `sgold_outcome`, `scoin_outcome`, `balance_sgold`, `balance_scoin`, `result`, `statement_timestamp`) VALUES ($uid, 'today_bonus', 'login$login_day_count', $before_sgold, $before_scoin, $sgold_income, $scoin_income, 0, 0, $tsgold, $tscoin, 'success', NOW());";
            $db->exec($sql);
            $sql_update = "update facebook_user set login_day_count=$login_day_count ,last_login_date=current_date,`sgold`='$tsgold',`scoin`='$tscoin' WHERE uid=$uid";
            $db->query($sql_update);
        }
    }


    $day = date("Y-m-d");
    $results = array('success' => FALSE, 'desc' => 'nothing', "login" => array('day' => 0, 'sgold' => 0, 'scoin' => 0), 'bonus' => array('sgold' => 0, 'scoin' => 0), "balance_sgold" => 0, "balance_scoin" => 0, 'date' => $day);
    $sql = "SELECT * FROM `total_statement`
WHERE uid='$uid'
AND statement_type='today_bonus'
AND DATE(statement_timestamp)='$day'";
    $stmt = $db->query($sql);
    $rs = $stmt->fetchAll(5);
    //var_dump($rs);
    if (!empty($rs)) {
        foreach ($rs as $r) {
            if ($r->type_addition == 'scoin_bonus') {
                $results['bonus']['scoin'] = (int)$r->scoin_income;
            } else if ($r->type_addition == 'sgold_bonus') {
                $results['bonus']['sgold'] = (int)$r->sgold_income;
            } else {
                $days = explode("login", $r->type_addition);
                $results['login']['day'] = (int)$days[1];
                $results['login']['scoin'] = (int)$r->scoin_income;
                $results['login']['sgold'] = (int)$r->sgold_income;
            }
        }
        $results['success'] = TRUE;
    } else {
        $results['desc'] = 'login record not found';
    }

    $sql = "SELECT * FROM `facebook_user`
WHERE uid='$uid'";
    $stmt = $db->query($sql);
    $user = $stmt->fetch(5);
    FBtofile($user->fb_uid);

    $results["balance_sgold"] = (int)$user->sgold;
    $results["balance_scoin"] = (int)$user->scoin;
    $results["image"] = "http://api.ssporting.com/system_config/dailyalert/" . $results['login']['day'] . ".png";
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($results);
}

function sendMsg()
{

    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sender = isset($_REQUEST['sender']) ? $_REQUEST['sender'] : "";
    $receiverlist = isset($_REQUEST['receiver']) ? $_REQUEST['receiver'] : "";
    $message = isset($_REQUEST['message']) ? $_REQUEST['message'] : "";
    $images = isset($_REQUEST['images']) ? $_REQUEST['images'] : 0;
    $result = array("success" => false, "desc" => "nothing happen", "message" => array());
    if ($sender && $receiverlist && $message) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $mlist = "0";
        if ($images) {
            $imgs = explode(",", $images);
            foreach ($imgs as $img) {
                $pix = explode("|", $img);
                $path = "";
                $thumbnail = "";
                if (array_key_exists(0, $pix)) {
                    $path = $pix[0];
                }
                if (array_key_exists(1, $pix)) {
                    $thumbnail = $pix[1];
                }
                $day = date("Y-m-d H:i:s");
                $desc = "PM images";
                $sql = "INSERT INTO `media_store` (`path`, `thumbnail`, `created_at`, `desc`) VALUES ('$path', '$thumbnail', '$day', '$desc');";
                $db->exec($sql);
                $stmt = $db->query("SELECT LAST_INSERT_ID();");
                $lastid = $stmt->fetchColumn();
                $mlist .= ",$lastid";
            }
        }


        $receivers = explode(",", $receiverlist);
        foreach ($receivers as $receiver) {
            $key = $sender . $receiver;
            $sql = "INSERT INTO `message_box` (`sender`, `receiver`,`key`, `message`,`media`, `send_at`, `stamp_at`) VALUES ('$sender', '$receiver','$key', '$message','$mlist', '$day', '$stp')";
            $success = $db->exec($sql);
            if ($success) {
                $sql = "SELECT LAST_INSERT_ID()";
                $stmt = $db->query($sql);
                $lastid = $stmt->fetchColumn();
                $sql = "SELECT `id`, `sender`, `receiver`, `message`, `send_at`, `stamp_at`, `seen`, `remove`,`fb_uid`,`display_name` FROM `message_box` mb LEFT JOIN `facebook_user` fb ON mb.receiver=fb.uid   WHERE  `id`='$lastid';";
                $stmt = $db->query($sql);
                $msg = $stmt->fetch(5);
                $result['success'] = true;
                $result['message'][] = $msg;


                $sql = "SELECT fb_uid FROM `facebook_user` WHERE `uid`=$sender";
                $stmt = $db->query($sql);
                $sfb_uid = $stmt->fetchColumn();
                $sql = "SELECT fb_uid FROM `facebook_user` WHERE `uid`=$receiver";
                $stmt = $db->query($sql);
                $rfb_uid = $stmt->fetchColumn();
                // regEvent($db,$sfb_uid, 'message', 'ได้ส่งข้อความถึงคุณ', "$message", 'message', $lastid, $rfb_uid);
            } else {
                $result['desc'] = "Can not insert message into box";
            }
        }
    }

    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function getUserlist()
{
    $startf = Carbon::now();
    $display_name = isset($_REQUEST['display_name']) ? $_REQUEST['display_name'] : "admin";
    $display_name = addslashes($display_name);
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql = "SELECT uid,fb_uid,display_name FROM facebook_user WHERE display_name LIKE '%$display_name%' ORDER BY display_name";
    $stmt = $db->query($sql);
    $userlitst = $stmt->fetchAll(5);
    $result = array('success' => FALSE, 'desc' => 'nothing', 'userlist' => array());
    if (!empty($userlitst)) {
        $result['success'] = true;
        $result['userlist'] = $userlitst;
    } else {
        $result['desc'] = 'user not found.';
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function getMsg()
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $receiver = isset($_REQUEST['receiver']) ? $_REQUEST['receiver'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 5;
    $result = array("success" => false, "desc" => "nothing happen", "messagelist" => array(), 'unseen' => 0);
    if ($receiver) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $sql = "SELECT * FROM (
                        SELECT * FROM message_box
                        WHERE (`receiver`=$receiver OR `sender`=$receiver)
                        AND remove='N'
                        ORDER BY `id` DESC
                    ) mb
                GROUP BY `key`
                ORDER BY `id` DESC
                LIMIT $limit";
        $stmt = $db->query($sql);
        $mlist = $stmt->fetchAll(5);
        $seen = "0";
        $unseen = 0;

        foreach ($mlist as $key => $msg) {
            $usersql = "SELECT fb_uid,display_name FROM facebook_user WHERE uid=";
            if ($msg->sender == $receiver) {
                $usersql .= "'$msg->receiver'";
            } else {
                $seen .= ",{$msg->id}";
                $unseen++;
                $usersql .= "'$msg->sender'";
            }
            $userstmt = $db->query($usersql);
            $user = $userstmt->fetch(5);
            $mlist[$key]->fb_uid = $user->fb_uid;
            $mlist[$key]->display_name = $user->display_name;
            $media = array();
            if ($msg->media) {
                $mediasql = "SELECT * FROM media_store WHERE id IN ({$msg->media})";
                //echo $mediasql;
                $stmt = $db->query($mediasql);
                $media = $stmt->fetchAll(5);
                //var_dump($media);
            }
            $mlist[$key]->medialist = $media;
        }
        $result['success'] = true;
        $result['messagelist'] = $mlist;
        $result['unseen'] = $unseen;
        $db->exec("UPDATE `message_box` SET seen='Y' WHERE `id` IN ($seen)");
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function getInbox()
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $receiver = isset($_REQUEST['receiver']) ? $_REQUEST['receiver'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;

    $additional = "";
    if ($limit) {
        $additional = "LIMIT $limit";
    }

    $result = array("success" => false, "desc" => "nothing happen", "messagelist" => array(), 'unseen' => 0, 'quantity' => 0);
    if ($receiver) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $sql = "SELECT * FROM message_box
                        WHERE `receiver`=$receiver
                        AND remove='N'
                        ORDER BY `id` DESC
               $additional";
        $stmt = $db->query($sql);
        $mlist = $stmt->fetchAll(5);
        $seen = 0;
        $unseen = 0;

        foreach ($mlist as $key => $msg) {
            $usersql = "SELECT fb_uid,display_name FROM facebook_user WHERE uid=";
//            if ($msg->sender == $receiver) {
//                $usersql.="'$msg->receiver'";
//            } else {
            $seen .= ",{$msg->id}";
            if ($msg->seen == 'N') {
                $unseen++;
            }
            $usersql .= "'$msg->sender'";
//           }
            $userstmt = $db->query($usersql);
            $user = $userstmt->fetch(5);
            $mlist[$key]->fb_uid = $user->fb_uid;
            $mlist[$key]->display_name = $user->display_name;
            $media = array();
            if ($msg->media) {
                $mediasql = "SELECT * FROM media_store WHERE id IN ({$msg->media})";
                //echo $mediasql;
                $stmt = $db->query($mediasql);
                $media = $stmt->fetchAll(5);
                //var_dump($media);
            }
            $mlist[$key]->medialist = $media;
        }
        $sql = "SELECT COUNT(id) FROM message_box
                        WHERE `receiver`=$receiver
                        AND remove='N'
                        ORDER BY `id` DESC";
        $stmt = $db->query($sql);
        $nmsg = $stmt->fetchColumn();


        $result['success'] = true;
        $result['messagelist'] = $mlist;
        $result['unseen'] = $unseen;
        $result['quantity'] = $nmsg;
        $db->exec("UPDATE `message_box` SET seen='Y' WHERE `id` IN ($seen)");
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function inboxBySender()
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $receiver = isset($_REQUEST['receiver']) ? $_REQUEST['receiver'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;

    $result = array("success" => false, "desc" => "nothing happen", 'users' => array(), 'unseen' => 0, 'quantity' => 0);
    if ($receiver) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $sql = "SELECT sender FROM message_box WHERE receiver=$receiver
GROUP BY sender
ORDER BY send_at DESC
LIMIT $limit
";
        $stmt = $db->query($sql);
        $senderlist = $stmt->fetchAll(5);
        $seen = 0;
        $unseen = 0;

        foreach ($senderlist as $sender) {
            $sql = "SELECT * FROM message_box WHERE receiver=$receiver AND sender={$sender->sender}
ORDER BY send_at DESC
";
            $stmt = $db->query($sql);
            $mlist = $stmt->fetchAll(5);
            $users = array();
            foreach ($mlist as $key => $msg) {
                $usersql = "SELECT fb_uid,display_name FROM facebook_user WHERE uid={$sender->sender}";
                $userstmt = $db->query($usersql);
                $user = $userstmt->fetch(5);
                $mlist[$key]->fb_uid = $user->fb_uid;
                $mlist[$key]->display_name = $user->display_name;
                $users['user']['fb_uid'] = $user->fb_uid;
                $users['user']['display_name'] = $user->display_name;
                $media = array();
                if ($msg->media) {
                    $mediasql = "SELECT * FROM media_store WHERE id IN ({$msg->media})";
                    //echo $mediasql;
                    $stmt = $db->query($mediasql);
                    $media = $stmt->fetchAll(5);
                    //var_dump($media);
                }
                $seen .= ",{$msg->id}";
                if ($msg->seen == 'N') {
                    $unseen++;
                }
                $mlist[$key]->medialist = $media;
            }
            $users['messages'] = $mlist;
            $result['users'][] = $users;
        }
        $sql = "SELECT COUNT(id) FROM message_box
                        WHERE `receiver`=$receiver
                        AND remove='N'
                        ORDER BY `id` DESC";
        $stmt = $db->query($sql);
        $nmsg = $stmt->fetchColumn();


        $result['success'] = true;
        //$result['messagelist'] = $mlist;
        $result['unseen'] = $unseen;
        $result['quantity'] = $nmsg;
        $db->exec("UPDATE `message_box` SET seen='Y' WHERE `id` IN ($seen)");
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function getSendbox()
{
    $startf = Carbon::now();
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sender = isset($_REQUEST['sender']) ? $_REQUEST['sender'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 0;
    $additional = "";
    if ($limit) {
        $additional = "LIMIT $limit";
    }

    $result = array("success" => false, "desc" => "nothing happen", "messagelist" => array(), 'unseen' => 0, 'quantity' => 0);
    if ($sender) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $sql = "SELECT * FROM message_box
                        WHERE `sender`=$sender
                        AND remove='N'
                        ORDER BY `id` DESC
                $additional";
        $stmt = $db->query($sql);
        $mlist = $stmt->fetchAll(5);
        $seen = 0;
        $unseen = 0;

        foreach ($mlist as $key => $msg) {
            $usersql = "SELECT fb_uid,display_name FROM facebook_user WHERE uid=";
//            if ($msg->sender == $receiver) {
            $usersql .= "'$msg->receiver'";
//            } else {
//                $seen.=",{$msg->id}";
//                $unseen++;
//            $usersql.="'$msg->sender'";
//            }
            $userstmt = $db->query($usersql);
            $user = $userstmt->fetch(5);
            $mlist[$key]->fb_uid = $user->fb_uid;
            $mlist[$key]->display_name = $user->display_name;
            $media = array();
            if ($msg->media) {
                $mediasql = "SELECT * FROM media_store WHERE id IN ({$msg->media})";
                //echo $mediasql;
                $stmt = $db->query($mediasql);
                $media = $stmt->fetchAll(5);
                //var_dump($media);
            }
            $mlist[$key]->medialist = $media;
        }
        $sql = "SELECT COUNT(id) FROM message_box
                        WHERE `sender`=$sender
                        AND remove='N'
                        ORDER BY `id` DESC";
        $stmt = $db->query($sql);
        $nmsg = $stmt->fetchColumn();


        $result['success'] = true;
        $result['messagelist'] = $mlist;
        $result['unseen'] = $unseen;
        $result['quantity'] = $nmsg;
        //$db->exec("UPDATE `message_box` SET seen='Y' WHERE `id` IN ($seen)");
    }
    //querylog(__FUNCTION__, $db);
    $stop = Carbon::now();
    $duration = $stop->micro - $startf->micro;
    DBConfig::closeConnection($db, __FUNCTION__, $duration);
    echo json_encode($result);
}

function sendboxBySender()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sender = isset($_REQUEST['sender']) ? $_REQUEST['sender'] : "";
    $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
    $result = array("success" => false, "desc" => "nothing happen", 'users' => array(), 'unseen' => 0, 'quantity' => 0);
    if ($sender) {
        $day = date("Y-m-d H:i:s");
        $stp = strtotime($day);
        $sql = "SELECT receiver FROM message_box WHERE sender=$sender
GROUP BY receiver
ORDER BY send_at DESC
LIMIT $limit
";
        $stmt = $db->query($sql);
        $receiverlist = $stmt->fetchAll(5);
        $seen = 0;
        $unseen = 0;

        foreach ($receiverlist as $receiver) {
            $sql = "SELECT * FROM message_box WHERE receiver={$receiver->receiver} AND sender=$sender
ORDER BY send_at DESC
";
            $stmt = $db->query($sql);
            $mlist = $stmt->fetchAll(5);
            $users = array();
            foreach ($mlist as $key => $msg) {
                $usersql = "SELECT fb_uid,display_name FROM facebook_user WHERE uid={$receiver->receiver}";
                $userstmt = $db->query($usersql);
                $user = $userstmt->fetch(5);
                $mlist[$key]->fb_uid = $user->fb_uid;
                $mlist[$key]->display_name = $user->display_name;
                $users['user']['fb_uid'] = $user->fb_uid;
                $users['user']['display_name'] = $user->display_name;
                $media = array();
                if ($msg->media) {
                    $mediasql = "SELECT * FROM media_store WHERE id IN ({$msg->media})";
                    //echo $mediasql;
                    $stmt = $db->query($mediasql);
                    $media = $stmt->fetchAll(5);
                    //var_dump($media);
                }
                $seen .= ",{$msg->id}";
                if ($msg->seen == 'N') {
                    $unseen++;
                }
                $mlist[$key]->medialist = $media;
            }
            $users['messages'] = $mlist;
            $result['users'][] = $users;
        }
        $sql = "SELECT COUNT(id) FROM message_box
                        WHERE `sender`=$sender
                        AND remove='N'
                        ORDER BY `id` DESC";
        $stmt = $db->query($sql);
        $nmsg = $stmt->fetchColumn();


        $result['success'] = true;
        //$result['messagelist'] = $mlist;
        $result['unseen'] = $unseen;
        $result['quantity'] = $nmsg;
        $db->exec("UPDATE `message_box` SET seen='Y' WHERE `id` IN ($seen)");
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function delMsg($id = 0)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $result = array('success' => false, 'desc' => 'nothing happen');
    $sql = "UPDATE `message_box` SET `remove`='Y' WHERE `id`=$id";
    $success = $db->exec($sql);
    if ($success) {
        $result['success'] = TRUE;
    }

    echo json_encode($result);
}

function getInstruction()
{
    $file = "cache/howto.json";
    $newquery = false;
    $ins = array();
    if (file_exists($file)) {
        $filem = filemtime($file);
        $dn = date('d');
        $fileday = date('d', $filem);
        if ($dn != $fileday) {
            $newquery = true;
        } else {
            $ins = json_decode(file_get_contents($file), true);
        }
    } else {
        $newquery = true;
    }
    if ($newquery) {
        $db = DBConfig::getConnection();
        $db->exec("SET profiling = 1;");
        $sql = "SELECT * FROM instruction";
        $stmt = $db->query($sql);
        $ins = $stmt->fetchAll(5);
        file_put_contents($file, json_encode($ins));
    }
    echo json_encode($ins);
}

function openapp()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
    $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
    $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
    $country = isset($_REQUEST['country']) ? $_REQUEST['country'] : '';
    $device_timezone = isset($_REQUEST['device_timezone']) ? $_REQUEST['device_timezone'] : '';
    $lang = isset($_REQUEST['lang']) ? $_REQUEST['lang'] : 'en';
    $rs = $db->exec("INSERT INTO `used_list` (`apple_id`, `used_at`, `app`) VALUES ('$apple_id', NOW(), '$app');");
    $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
    if ($rs > 0) {
        $result['success'] = true;
        $result['desc'] = "Counted";
        $id = $db->query("SELECT LAST_INSERT_ID()")->fetch();
        $id = (int)$id[0];
        $result['result'] = $db->query("SELECT * FROM used_list WHERE `id`=$id")->fetchAll(PDO::FETCH_OBJ);
    }
    $sql = "select *from request_notification_count where device_id ='$apple_id' and app='$app' and platform='$platform'";
    $stmt = $db->query($sql);
    $obj = $stmt->fetch(PDO::FETCH_OBJ);
    //print_r($obj);exit;
    if ($obj == null) {
        $sql = "insert into request_notification_count (device_id,app,platform,qty,country,device_timezone,lang) values('" . $apple_id . "','$app','$platform',0,'$country','$device_timezone','$lang')";
        $db->exec($sql);
    } else {
        $sql = "update request_notification_count set qty = 0,country='$country',device_timezone='$device_timezone',lang='$lang' where device_id='$apple_id' and  app='$app' and platform='$platform'";
        $db->exec($sql);
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function RequestNotification()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
    $number = strlen($apple_id);

    $match_id = isset($_REQUEST['match_id']) ? $_REQUEST['match_id'] : '';
    $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
    if ($number <= 20) {
        $platform = 'web';
    }
    $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
    $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "Y";
    $rs = $db->query("SELECT * FROM request_notification WHERE match_id=$match_id AND apple_id='$apple_id' AND app='$app'")->fetch();
    //var_dump($rs);
    //echo 1;exit;
    $result = array('success' => false, 'desc' => 'Nothing happen', 'source' => 'index', 'result' => array());
    if (empty($rs)) {
        $rs = $db->exec("INSERT INTO `request_notification` (`match_id`, `apple_id`, `create_at`, `notification`, `app`,`platform`) VALUES ($match_id, '$apple_id', NOW(), '$status', '$app','$platform')");


        $result['success'] = true;
        $result['desc'] = "New insert";
    } else {
        $res = $db->exec("UPDATE `request_notification` SET `notification`='$status' WHERE  `id`={$rs['id']}");
        if ($res > 0) {
            $result['success'] = true;
            $result['desc'] = "Status updated";
        }
    }
    $r = $db->query("SELECT * FROM `request_notification` WHERE apple_id='$apple_id' AND app='$app' AND notification='Y'")->fetchAll();
    if (!empty($r)) {
        $result['result'] = $r;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function RequestNotificationWithUID()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
    $uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : 0;
    $number = strlen($apple_id);

    $match_id = isset($_REQUEST['match_id']) ? $_REQUEST['match_id'] : '';
    $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
    if ($number <= 20) {
        $platform = 'web';
    }
    $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
    $status = isset($_REQUEST['status']) ? $_REQUEST['status'] : "Y";
    $rs = $db->query("SELECT * FROM request_notification WHERE match_id=$match_id AND uid='$uid' AND app='$app'")->fetch();
    //var_dump($rs);
    //echo 1;exit;
    $result = array('success' => false, 'desc' => 'Already assigned.', 'source' => 'index', 'result' => array());
    if (empty($rs)) {
        $rs = $db->exec("INSERT INTO `request_notification` (`uid`,`match_id`, `apple_id`, `create_at`, `notification`, `app`,`platform`) VALUES ($uid,$match_id, '$apple_id', NOW(), '$status', '$app','$platform')");


        $result['success'] = true;
        $result['desc'] = "New insert";
    } else {
        $upsql = "UPDATE `request_notification` SET `notification`='$status' WHERE  `id`={$rs['id']}";
        //echo $upsql;
        $res = $db->exec($upsql);
        if ($res) {
            $result['success'] = true;
            $result['desc'] = "Status updated";
        }
    }
    $r = $db->query("SELECT * FROM `request_notification` WHERE uid='$uid' AND app='$app' AND notification='Y'")->fetchAll();
    if (!empty($r)) {
        $result['result'] = $r;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function NotificationList()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $apple_id = isset($_REQUEST['apple_id']) ? $_REQUEST['apple_id'] : '';
    $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
    $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
    $rs = $db->query("SELECT * FROM `request_notification` WHERE apple_id='$apple_id' AND app='$app' AND notification='Y'")->fetchAll();
    $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
    if (!empty($rs)) {
        $result['success'] = true;
        $result['desc'] = "get list";
        $result['result'] = $rs;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function NotificationListWithUID()
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $uid = isset($_REQUEST['uid']) ? $_REQUEST['uid'] : '';
    $app = isset($_REQUEST['app']) ? $_REQUEST['app'] : '';
    $platform = isset($_REQUEST['platform']) ? $_REQUEST['platform'] : 'ios';
    $rs = $db->query("SELECT * FROM `request_notification` WHERE uid='$uid' AND app='$app' AND notification='Y'")->fetchAll();
    $result = array('success' => false, 'desc' => 'Nothing happen', 'result' => array());
    if (!empty($rs)) {
        $result['success'] = true;
        $result['desc'] = "get list";
        $result['result'] = $rs;
    }
    //querylog(__FUNCTION__, $db);
    echo json_encode($result);
}

function playValue()
{
    $value = array('scoin' => array(100, 200, 300, 400, 500), 'sgold' => array(100, 200, 300, 400, 500));
    if (file_exists("system_config/file.json")) {
        $jsn = json_decode(file_get_contents("system_config/file.json"), true);
        $value['scoin'] = $jsn['ball_game_scoin'];
        $value['sgold'] = $jsn['ball_game_sgold'];
    }
    echo json_encode($value);
}

function rateSelect()
{
    $value = array('sgold_to_scoin' => array(), 'scoin_to_sgold' => array(), 'sgold_to_diamond' => array());
    $rate = array(1, 10, 20, 50);
    if (file_exists("system_config/file.json")) {
        $jsn = json_decode(file_get_contents("system_config/file.json"), true);
        foreach ($rate as $r) {
            $value['sgold_to_scoin'][] = $r;
            $value['scoin_to_sgold'][] = $r * $jsn['exchange_rate'][1][1];
            $value['sgold_to_diamond'][] = $r * $jsn['sgold_to_diamond'];
        }
    }
    echo json_encode($value);
}

function logSet($id, $stat = 'off')
{
    $loglist = array(1 => array('name' => "bet", 'status' => 'off'), 2 => array('name' => "timeline", 'status' => 'off'));
    $filepath = "logs/setting.json";
    if (!file_exists($filepath)) {
        file_put_contents($filepath, json_encode($loglist));
    }
    $setting = json_decode(file_get_contents($filepath), true);
    if (array_key_exists($id, $setting)) {
        $setting[$id]['status'] = ($stat == 'on') ? 'on' : 'off';
    }
    foreach ($setting as $key => $s) {
        echo $key . ":" . $s['name'] . "->" . $s['status'] . "\n";
    }
    file_put_contents($filepath, json_encode($setting));
}

function logGet($id)
{
    $filepath = "logs/setting.json";
    $setting = json_decode(file_get_contents($filepath), true);
    return $setting[$id]['status'];
}

function updateBetinfo($mid)
{
    $db = DBConfig::getConnection();
    $db->exec("SET profiling = 1;");
    $sql_bet = "select
sum(if(oddsType='hdp' and choose='home',1,0)) as hdp_home,
sum(if(oddsType='hdp' and choose='away',1,0)) as hdp_away,
sum(if(oddsType='1x2' and choose='home',1,0)) as odds1x2_home,
sum(if(oddsType='1x2' and choose='away',1,0)) as odds1x2_away,
sum(if(oddsType='1x2' and choose='draw',1,0)) as odds1x2_draw
from bet
where mid=$mid";

    $stmt = $db->query($sql_bet);
    //////Analog::log("1202:$sql_bet", 6);
    $betInfo = $stmt->fetch(PDO::FETCH_OBJ);
    //var_dump($betInfo);
    $write = false;
    if (!empty($betInfo)) {
        foreach ($betInfo as $key => $val) {
            if (is_numeric($val)) {
                $write = true;
            } else {
                $write = false;
                $json = json_encode(array(
                    'hdp_home' => "0",
                    'hdp_away' => "0",
                    'odds1x2_home' => "0",
                    'odds1x2_away' => "0",
                    'odds1x2_draw' => "0",
                    'source' => 'created from empty result'
                ));
                file_put_contents('bet_info/' . $mid . '.json', $json);
                //chmod('bet_info/' . $mid . '.json', 0777);
                break;
            }
        }
        if ($write) {
            file_put_contents('bet_info/' . $mid . '.json', json_encode($betInfo));
            //chmod('bet_info/' . $mid . '.json', 0777);
        }
    } else {
        $json = json_encode(array(
            'hdp_home' => "0",
            'hdp_away' => "0",
            'odds1x2_home' => "0",
            'odds1x2_away' => "0",
            'odds1x2_draw' => "0",
            'source' => 'created from empty result'
        ));
        file_put_contents('bet_info/' . $mid . '.json', $json);
        //chmod('bet_info/' . $mid . '.json', 0777);
    }
    //querylog(__FUNCTION__, $db);
    //echo file_get_contents('bet_info/' . $mid . '.json');
}

function querylog($fname, $db)
{
    $logdir = "logs/query";
    if (!is_dir($logdir)) {
        mkdir($logdir, 0777);
    }
    $logdate = date('Y-m-d');
    $file = $logdir . "/" . $logdate . ".log";
    if (!is_file($file)) {
        $ourFileName = $file;
        $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
        fclose($ourFileHandle);
        //chmod($file, 0777);
    }
    $stmt = $db->query("SHOW PROFILES;");
    $rs = $stmt->fetchAll(5);
    Analog::handler(\Analog\Handler\File::init($file));
    Analog::log("==========================", 6);
    Analog::log("$fname", 6);
    Analog::log(json_encode($rs), 6);
}

function sortedliveMatchUpdateOnly()
{
    $id = empty($_REQUEST['update_id']) ? 0 : $_REQUEST['update_id'];
    $result = array('live_match_update' => array(), 'update_id' => 0, 'c3' => 0);
    $file = "cronjob_gen_file/files/liveMatchUpdate.json";
    $update = json_decode(file_get_contents($file), true);
    if ((int)$id != (int)$update['update_id']) {
        $result = $update;
    } else {
        $result['c3'] = $update['c3'];
        $result['update_id'] = $update['update_id'];

    }
    echo json_encode($result);
}

function sendPushNotificationToGCM()
{

    $registatoin_ids = $_REQUEST['device'];
    $message = 'hello';
    define('API_ACCESS_KEY', 'AIzaSyClY09mndV6KBatf0jJFx_n2p3n8nNj_h0');
    $registrationIds = array($registatoin_ids);
// prep the bundle
    $db = DBConfig::getServerConnection();
    $data = array();
    $esql = "SELECT event.*,live_match.*,t.teamNameEn as teamHomeEn,t.teamNameVn as teamHomeVn,t.teamNameKr as teamHomeKr,t.teamNameLa as teamHomeLa,t.teamNameTh as teamHomeTh,t.teamNameBig as teamHomeBig,t.teamNameGb as teamHomeGb,t2.teamNameEn as teamAwayEn,t2.teamNameVn as teamAwayVn,t2.teamNameKr as teamAwayKr,t2.teamNameLa as teamAwayLa,t2.teamNameTh as teamAwayTh,t2.teamNameBig as teamAwayBig,t2.teamNameGb as teamAwayGb,  logo1.32x32 as logoHome32x32,logo1.64x64 as logoHome64x64, logo2.32x32 as logoAway32x32,logo2.64x64 as logoAway64x64,  com.comNameEn,com.comNameTh,com.comNameBig,com.comNameKr,com.comNameGb,com.comNameLa,com.comNameVn, league.leagueNameTh,league.leagueNameEn,league.leagueNameKr,league.leagueNameVn,league.leagueNameLa,league.leagueNameBig,league.leagueNameGb, live_league.*
FROM live_match
left join event on event.mid = live_match.mid
left join live_league on live_league.leagueId =live_match._lid  and live_match.showDate = live_league.date
left join lang_team as t on t.tid=live_match.hid
left join lang_team as t2 on t2.tid =live_match.gid
left join team_logos as logo1 on logo1.tid=live_match.hid
left join team_logos as logo2 on logo2.tid=live_match.gid
left join lang_competition as  com on com.cid = live_match.kid
left join lang_league as league on league.leagueId=live_match._lid
where live_match.showDate =current_date and event.data_checked='Y' and android_checked='N'
group by event.event_id limit 5";
    $eventlist = $db->query($esql);
    foreach ($eventlist as $e) {
        $data['teamHome']['en'] = empty($e['teamHomeEn']) ? $e['hn'] : $e['teamHomeEn'];
        $message = $e['message'];
    }
    $msg = array
    (
        'message' => $message,
        'title' => 'This is a title. title',
        'subtitle' => 'This is a subtitle. subtitle',
        'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
        'vibrate' => 1,
        'sound' => 1,
        'largeIcon' => 'large_icon',
        'smallIcon' => 'small_icon'
    );
    $fields = array
    (
        'registration_ids' => $registrationIds,
        'data' => $msg
    );

    $headers = array
    (
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
    echo $result;
}

function ServerStatus()
{
    $db = DBConfig::getConnection();
    $test = empty($_REQUEST['test']) ? 0 : $_REQUEST['test'];
    $file = "cache/server.json";
    if ($test) {
        $file = "cache/servertest.json";
    }
    $results = array();
    if (!file_exists($file)) {
        $dt = date('Y-m-d H:i:s');
        $sql = "SELECT * FROM `server_status` ORDER BY `id` DESC LIMIT 1";
        $stmt = $db->query($sql);
        $results = $stmt->fetch(5);
        if (empty($results)) {
            $db->exec("INSERT INTO `server_status` (`created_at`) VALUES ('$dt');");
            $stmt = $db->query($sql);
            $results = $stmt->fetch(5);
        }
        file_put_contents($file, json_encode($results));
    } else {
        $results = json_decode(file_get_contents($file), true);
    }
    echo json_encode($results);
}


?>
