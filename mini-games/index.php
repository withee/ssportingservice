<?php
require '../Slim/Slim.php';
require '../DBConfig.php';
require '../mini-games/card.php';

$app = new Slim();
$app->contentType('application/json; charset=utf-8');

$app->post('/play','play');
#$app->post('/exchange','exchange');
$app->get('/mini_game_statement','mini_game_statement');
#$app->get('/total_statement','total_statement');
$app->run();
date_default_timezone_set('UTC');

function mini_game_statement(){
    $ap = new Slim();
    $uid = $ap->request()->params('uid');
    $game_type = $ap->request()->params('game_type');
    $db = DBConfig::getConnection();
    if($game_type=='treasure_low' || $game_type=='treasure_medium' || $game_type=='treasure_high'){
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where game_type='$game_type' and g.uid=$uid order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list1=$stmt->fetchAll(PDO::FETCH_OBJ);
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where game_type='$game_type' order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list2=$stmt->fetchAll(PDO::FETCH_OBJ);
    }else if($game_type=='treasure'){
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where (game_type='treasure_low' or game_type='treasure_medium' or game_type='treasure_high' ) and g.uid=$uid order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list1=$stmt->fetchAll(PDO::FETCH_OBJ);
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where (game_type='treasure_low' or game_type='treasure_medium' or game_type='treasure_high' ) order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list2=$stmt->fetchAll(PDO::FETCH_OBJ);
    }else if($game_type){
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where game_type='$game_type' and g.uid=$uid order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list1=$stmt->fetchAll(PDO::FETCH_OBJ);
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where game_type='$game_type' order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list2=$stmt->fetchAll(PDO::FETCH_OBJ);
    }else{
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    where g.uid=$uid order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list1=$stmt->fetchAll(PDO::FETCH_OBJ);
        $sql ="select g.*,f.fb_uid,f.display_name from mini_game_statement g
    left join facebook_user as f on f.uid = g.uid
    order by id DESC limit 20";
        $stmt=$db->query($sql);
        $list2=$stmt->fetchAll(PDO::FETCH_OBJ);
    }

    $data=array(
        'user'=>$list1,
        'all'=>$list2
    );
    echo json_encode($data);
}
function play(){
    $config = json_decode(file_get_contents('../system_config/file.json'));
    $ap = new Slim();
    $uid=$ap->request()->params("uid");
    $game_type=$ap->request()->params("game_type");
    //$sgold = $ap->request()->params('sgold');
    //$sgold = $sgold?$sgold:0;
    $scoin = $ap->request()->params('scoin');
    //$scoin=$scoin?$scoin:100;

    $scoin_result=0;
    $sgold_result=0;
    $sgold_outcome =0;
    $scoin_outcome = $scoin;
    $sgold_income=0;
    $scoin_income=0;
    $data =array(
        'result'=>'',
        'description'=>'',
        'player1'=>'',
        'player2'=>'',
        'balance_sgold'=>'',
        'balance_scoin'=>'',
        'sgold_result_amount'=>'',
        'scoin_result_amount'=>'',
    );
    $fbObj=checkBalance($uid,$scoin);
    if($fbObj==null){
        $data['result']='credit_not_enough';
        $data['description']='Sgold or Scoin not enough.';
        echo json_encode($data);
        return;
    }else{

    }
    $before_sgold=$fbObj->sgold;
    $before_scoin=$fbObj->scoin;
    if($game_type=='pok_deng'){
        $d = new Card();
        $d->newDeck();

        $player1Cards = array();
        $player2Cards = array();

        //2 cards pick
        for($i=1; $i<=2; $i++) {
            $player1Cards[] = $d->pick();
            $player2Cards[] = $d->pick();
        }

        $player1Score = $d->score2Cards($player1Cards);
        $player2Score = $d->score2Cards($player2Cards);

        //3 card2 pick
        if(!($player1Score->special == "pok9" || $player1Score->special == "pok8" || $player2Score->special == "pok9" || $player2Score->special == "pok8")){
            if($player1Score->num < 4){
                $player1Cards[] = $d->pick();
                $player1Score = $d->score3Cards($player1Cards);
            }
            if($player2Score->num < 4){
                $player2Cards[] = $d->pick();
                $player2Score = $d->score3Cards($player2Cards);
            }
        }

//        $player1Cards[0]["num"] = "k";
//        $player1Cards[1]["num"] = "j";
//        $player1Cards[2]["num"] = "q";
//        $player1Score = $d->score3Cards($player1Cards);

        //play
        if($player1Score->special == "pok9" && $player2Score->special != "pok9"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "pok9" && $player1Score->special != "pok9"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if($player1Score->special == "pok8" && $player2Score->special != "pok8"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "pok8" && $player1Score->special != "pok8"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if($player1Score->special == "straightFlush" && $player2Score->special != "straightFlush"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "straightFlush" && $player1Score->special != "straightFlush"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if($player1Score->special == "tong3" && $player2Score->special != "tong3"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "tong3" && $player1Score->special != "tong3"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if($player1Score->special == "riang" && $player2Score->special != "riang"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "riang" && $player1Score->special != "riang"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if($player1Score->special == "yellow3" && $player2Score->special != "yellow3"){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if($player2Score->special == "yellow3" && $player1Score->special != "yellow3"){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else if((int)$player1Score->num > (int)$player2Score->num){
            $result = "win";
            $sgold_result = $scoin*(int)$player1Score->mul;
            $sgold_income = $scoin*(int)$player1Score->mul;
            $scoin_result = $scoin*(int)$player1Score->mul;
            $scoin_income = (int)($scoin*(int)$player1Score->mul)+(int)$scoin;
        }else if((int)$player1Score->num < (int)$player2Score->num){
            $result = "lose";
            $sgold_result = -1*$scoin*(int)$player2Score->mul;
            $sgold_outcome = $scoin*(int)$player2Score->mul;
            $scoin_result = -1*$scoin*(int)$player2Score->mul;
        }else{
            $result = "draw";
            $sgold_result=0;
            $scoin_result=0;
            $sgold_income=0;
            $scoin_income=$scoin;
        }
        $player_play = (array('player1Cards'=>$player1Cards, 'player1Score'=>$player1Score ));
        $com_play = (array('player2Cards'=>$player2Cards, 'player2Score'=>$player2Score ));
        $data=mysqlProcess($data,$uid,$game_type,$result,json_encode($player_play),$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result,false,true);
    }else if($game_type=='red_black'){
        $red_black_array = array('r','b');
        $player_play = $ap->request()->params('player_play');
        $random=rand(0,1);
        $com_play = $red_black_array[$random];
        if($com_play==$player_play){
            $result='win';
            $sgold_result=$scoin;
            $sgold_income=$scoin;
            $scoin_result =$scoin;
            $scoin_income = 2*$scoin;
        }else{
            $result='lose';
            $sgold_result=-1*$scoin;
            $sgold_outcome =$scoin;
            $scoin_result=-1*$scoin;
        }
        //$scoin_result=-1*$scoin;
        $data=mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result);

    }else if($game_type=='rock_paper_scissors'){
        $player_play = $ap->request()->params('player_play');
        $array =array('r','p','s');//r =rock(ค้อน),p=paper(กระดาษ),s=scissors(กรรไกร)
        $random=rand(0,count($array)-1);
        $com_play = $array[$random];
        $result=null;
        if($com_play==$player_play){
            $result='draw';
        }else{
            if($com_play=='r' && $player_play=='p'){
                $result='win';
            }else if($com_play=='r' && $player_play=='s'){
                $result='lose';
            }else if($com_play=='p' && $player_play=='r'){
                $result='lose';
            }else if($com_play=='p' && $player_play=='s'){
                $result='win';
            }else if($com_play=='s' && $player_play=='r'){
                $result='win';
            }else if($com_play=='s' && $player_play=='p'){
                $result='lose';
            }else{
                $data['result']='player_play_is_wrong';
                $data['description']='Player play is wrong.';
                echo json_encode($data);return;
            }
        }

        if($result=='draw'){
            $sgold_result=0;
            $scoin_result=0;
            $sgold_income=0;
            $scoin_income=$scoin;
        }else if($result=='win'){
            $sgold_result=$scoin;
            $scoin_result=$scoin;
            $sgold_income=$scoin;
            $scoin_income=2*$scoin;

        }else if($result=='lose'){
            $sgold_result=-1*$scoin;
            $scoin_result=-1*$scoin;
            $sgold_outcome=$scoin;

        }
        $data=mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result);


    }else if($game_type=='calabash_crap_fish'){
        $data['result']='no_service';
        $data['description']='No Service ';
        //echo json_encode($data);return;
        $player_play = $ap->request()->params('player_play');
        $array =array('ca','cr','f','t','s','ch');//ca = calabash(น้ำเต้า) cr = crap (ปู) f=fish(ปลา),t=tiger(เสือ),s=shrimp(กุ้ง),ch=chicken (ไก่)
        $correct_count=0;
        $com_play=array();
        $random=rand(0,count($array)-1);
        $com_play[0] = $array[$random];
        $random=rand(0,count($array)-1);
        $com_play[1] = $array[$random];
        $random=rand(0,count($array)-1);
        $com_play[2] = $array[$random];
        $result=null;
        foreach($com_play as $play){
            if($player_play==$play){
                $correct_count++;
            }
        }
        if($correct_count>0){
            $result='win';
            $sgold_result=$correct_count*$scoin;
            $scoin_result=$correct_count*$scoin;
            $sgold_income=($correct_count)*$scoin;
            $scoin_income = ($correct_count+1)*$scoin;
        }else{
            $sgold_result=-1*$scoin;
            $scoin_result=-1*$scoin;
            $sgold_outcome =$scoin;
            $result='lose';
        }
        //$com_play =json_encode($com_play);
        $data=mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result,false,true);



    }else if($game_type=='hi_low'){

        $player_play = $ap->request()->params('player_play');
        $result=null;
        $array = array(1,2,3,4,5,6);
        $sum=0;
        $com_play=array();
        $random =  rand(0,count($array)-1);

        $com_play[0]=$array[$random];
        $random =  rand(0,count($array)-1);
        $com_play[1]=$array[$random];
        $random =  rand(0,count($array)-1);
        $com_play[2]=$array[$random];
        $sum = $com_play[0]+$com_play[1]+$com_play[2];
        if($sum==11 && $player_play=="11"){
            $result='win';
            $sgold_result=5*$scoin;
            $scoin_result=5*$scoin;
            $sgold_income=5*$scoin;
            $scoin_income = 6*$scoin;


        }else if($sum<11 && $player_play=='l'){
            $result='win';
            $sgold_result=$scoin;
            $scoin_result=$scoin;
            $sgold_income=$scoin;
            $scoin_income = $scoin*2;
        }else if($sum>11 && $player_play=='h'){
            $result='win';
            $sgold_result=$scoin;
            $scoin_result=$scoin;
            $sgold_income=$scoin;
            $scoin_income = $scoin*2;
        }else{
            $result='lose';
            $sgold_result=-1*$scoin;
            $scoin_result=-1*$scoin;
            $sgold_outcome=$scoin;
        }


        $com_play = (array('com_play'=>$com_play,'sum'=>$sum));
        $data=mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result,false,true);


    }else if($game_type=='treasure_low' || $game_type=='treasure_medium' || $game_type=='treasure_high'){
        $between=null;
        $current_date = date('Y-m-d');
        if($game_type=='treasure_low'){
            $between=$config->treasure_low_between;
            $date = $fbObj->last_play_treasure_low;
            $count = $fbObj->treasure_low_count;
            if($current_date==$date && $config->treasure_limit_per_day < $count+1){
                $data['result']='game_limit';
                $data['description']='Game is limit '.$config->treasure_limit_per_day." per day";
                $data['balance_sgold']=$fbObj->sgold;
                $data['balance_scoin']=$fbObj->scoin;
                echo json_encode($data);return;
            }else if($date!=null && $date!=$current_date){
                $sql_update ="update facebook_user set ".$game_type."_count=0 where uid=$uid";
                $db= DBConfig::getConnection();
                $db->query($sql_update);

            }
        }else if($game_type=='treasure_medium'){
            $between=$config->treasure_medium_between;
            $date = $fbObj->last_play_treasure_medium;
            $count = $fbObj->treasure_medium_count;
            if($current_date==$date && $config->treasure_limit_per_day < $count+1){
                $data['result']='game_limit';
                $data['description']='Game is limit '.$config->treasure_limit_per_day." per day";
                $data['balance_sgold']=$fbObj->sgold;
                $data['balance_scoin']=$fbObj->scoin;
                echo json_encode($data);return;
            }else if($date!=null && $date!=$current_date){
                $sql_update ="update facebook_user set ".$game_type."_count=0 where uid=$uid";
                $db= DBConfig::getConnection();
                $db->query($sql_update);

            }
        }else if($game_type=='treasure_high'){
            $between=$config->treasure_high_between;
            $date = $fbObj->last_play_treasure_high;
            $count = $fbObj->treasure_high_count;
            if($current_date==$date && $config->treasure_limit_per_day < $count+1){
                $data['result']='game_limit';
                $data['description']='Game is limit '.$config->treasure_limit_per_day." per day";
                $data['balance_sgold']=$fbObj->sgold;
                $data['balance_scoin']=$fbObj->scoin;
                echo json_encode($data);return;
            }else if($date!=null && $date!=$current_date){
                    $sql_update ="update facebook_user set ".$game_type."_count=0 where uid=$uid";
                    $db= DBConfig::getConnection();
                    $db->query($sql_update);

            }
        }else{
            $data['result']='game_type_is_wrong';
            $data['description']='Game type is wrong.';
            echo json_encode($data);return;
        }

        //$between=$config->treasure_low_between;
        $com_play=rand($between[0],$between[1]);
        if($scoin==$com_play){
            $result='draw';
            $sgold_result=$scoin;
            $scoin_result=$scoin;
            $scoin_income=$scoin;

        }else if($scoin<$com_play){
            $result='win';
            $scoin_result=$com_play;
            $sgold_result=$com_play;
            $scoin_income =$com_play;
            $sgold_income =$com_play-$scoin;
        }else if($scoin>$com_play){
            $result='lose';
            $scoin_result=$com_play;
            $sgold_result=$com_play;
            $scoin_income=$com_play;
            $sgold_outcome=$scoin-$com_play;
        }
        $player_play='';
        $data=mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold=0,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result);

    }else{
        $data['result']='no_service';
        $data['description']='No Service ';
    }
    echo json_encode($data);

}
function mysqlProcess($data,$uid,$game_type,$result,$player_play,$com_play,$sgold,$scoin,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$sgold_result,$scoin_result,$player_play_json=false,$com_play_json=false){
    $statement_type ='mini_game';
    $db= DBConfig::getConnection();
    $com_play_insert = $com_play_json?json_encode($com_play):$com_play;
    $player_play_insert = $player_play_json?json_encode($player_play):$player_play;
    if($result=='win'){
        $scoin_income= $scoin_income-$sgold_income;
    }
    $balance_sgold = $before_sgold  -$sgold_outcome + $sgold_income;
    $balance_scoin = $before_scoin - $scoin_outcome + $scoin_income;
    $mini_game_statement_sql=getMiniGameStatementQuery($uid,$game_type,$sgold,$scoin,$sgold_result,$scoin_result,$result,$player_play_insert,$com_play_insert);
    $db->query($mini_game_statement_sql);
    $last_game_sql = getLastGameQuery($uid,$game_type);
    $stmt=$db->query($last_game_sql);
    $last_game=$stmt->fetch(PDO::FETCH_OBJ);
    $total_statement_sql=getTotalStatementQuery($uid,$statement_type,$game_type,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,$last_game->id,'');
    $db->query($total_statement_sql);

    $update_facebook_user_sql =getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin,$game_type);
    $db->query($update_facebook_user_sql);
    $data['result']=$result;
    $data['player1']=$player_play;//$player_play_json?json_decode($player_play):$player_play;
    $data['player2']=$com_play;//$com_play_json?json_decode($com_play):$com_play;
    $data['balance_sgold']=$balance_sgold;
    $data['balance_scoin']=$balance_scoin;
    $data['sgold_result_amount']=$sgold_result;
    $data['scoin_result_amount']=$scoin_result;
    return $data;
}

function getMiniGameStatementQuery($uid,$game_type,$sgold_amount,$scoin_amount,$sgold_result_amount,$scoin_result_amount,$result,$player1,$player2){

    $sql ="insert into mini_game_statement (uid,game_type,sgold_amount,scoin_amount,sgold_result_amount,scoin_result_amount,result,player1,player2) values(
        $uid,'$game_type',$sgold_amount,$scoin_amount,$sgold_result_amount,$scoin_result_amount,'$result','$player1','$player2'
        )";
    return $sql;
}
function getTotalStatementQuery($uid,$statement_type,$type_addition,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$result,$game_id,$note){
    $sql ="insert into total_statement
            (uid,
            statement_type,
            type_addition,
            before_sgold,
            before_scoin,
            sgold_income,
            scoin_income,
            sgold_outcome,
            scoin_outcome,
            balance_sgold,
            balance_scoin,
            result,
            game_id,
            note)
            values(
            '$uid',
            '$statement_type',
            '$type_addition',
            $before_sgold,
            $before_scoin,
            $sgold_income,
            $scoin_income,
            $sgold_outcome,
            $scoin_outcome,
            $balance_sgold,
            $balance_scoin,
            '$result',
            $game_id,
            '$note'
            )
    ";
    //echo $sql;exit;
    return $sql;
}
function getLastGameQuery($uid,$game_type){
    return "select *from mini_game_statement where uid=".$uid." and game_type='".$game_type."' order by id DESC";
}
function getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin,$game_type=null){
    $sql_addition='';
    if($game_type=='treasure_low' || $game_type=='treasure_medium' || $game_type=='treasure_high'){
        $sql_addition=(",$game_type".'_count = '.$game_type."_count +1,last_play_$game_type=current_date");
    }

    return "update facebook_user set sgold=$balance_sgold,scoin=$balance_scoin $sql_addition where uid=$uid";
}
function checkBalance($uid,$scoin){
    $db = DBConfig::getConnection();
    $sql ="select *from facebook_user where uid=$uid and  scoin>=$scoin";
    $stmt=$db->query($sql);
    $result=$stmt->fetch(PDO::FETCH_OBJ);
    return $result;
}
function getUser($uid){
    $db = DBConfig::getConnection();
    $sql ="select *from facebook_user where uid=$uid";
    $stmt=$db->query($sql);
    $result=$stmt->fetch(PDO::FETCH_OBJ);
    return $result;
}

function checkParam($uid,$game_type,$sgold,$scoin){


}
function exchange(){
    $config = json_decode(file_get_contents('../system_config/file.json'));
    $exchange_rate = $config->exchange_rate;
    $ap = new Slim();
    $exchange_array = array('sgold_to_scoin','scoin_to_sgold');
    $uid = $ap->request()->params('uid');
    $type = $ap->request()->params('type');//0 = sgold to scoin  1= scoin to sgold
    $amount = $ap->request()->params('amount');
    $user=getUser($uid);
    $data =array(
        'result'=>'success',
        'description'=>'',
        'balance_sgold'=>null,
        'balance_scoin'=>null,
        'sgold'=>null,
        'scoin'=>null,
    );
    $sgold_income=0;
    $sgold_outcome=0;
    $scoin_income=0;
    $scoin_outcome=0;
    $sgold=0;
    $scoin=0;
    $before_sgold = $user->sgold;
    $before_scoin = $user->scoin;
    if($type==0){
        if($amount<=$before_sgold){

            $scoin_income = $amount*$exchange_rate[0][1];
            $sgold_outcome = $amount;
            $sgold= $sgold_outcome;
            $scoin= $scoin_income;

        }else{
            $data['result']='fail';
            $data['description']='Sgold not enough.';
        }
    }else if($type==1){
        if($amount*$exchange_rate[1][1]<=$before_scoin){
            $sgold_income = $amount;
            $scoin_outcome = $amount * $exchange_rate[1][1];
            $sgold=$sgold_income;
            $scoin =$scoin_outcome;
        }else{
            $data['result']='fail';
            $data['description']='Scoin not enough.';
        }
    }else{
        $data['result']='type_is_wrong';
        $data['description']='Type is wrong.';
    }
    if($sgold_outcome>0 || $scoin_outcome>0){
        $data['result']='success';

        $statement_type='exchange';
        $type_addition = $exchange_array[$type];
        $db = DBConfig::getConnection();
        $balance_sgold = $before_sgold+($sgold_income-$sgold_outcome);
        $balance_scoin = $before_scoin+($scoin_income-$scoin_outcome);
        $total_statement_sql=getTotalStatementQuery($uid,$statement_type,$type_addition,$before_sgold,$before_scoin,$sgold_income,$scoin_income,$sgold_outcome,$scoin_outcome,$balance_sgold,$balance_scoin,$data['result'],'null','');
        $db->query($total_statement_sql);
        $update_facebook_user_sql =getUpdateFacebookUserQuery($uid,$balance_sgold,$balance_scoin);
        $db->query($update_facebook_user_sql);
        $data['balance_sgold']=$balance_sgold;
        $data['balance_scoin']=$balance_scoin;
        $data['scoin']=$scoin;
        $data['sgold']=$sgold;
    }else{
        $data['balance_sgold']=$before_sgold;
        $data['balance_scoin']=$before_scoin;
    }
    echo json_encode($data);

}