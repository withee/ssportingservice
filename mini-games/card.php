<?php


class Card
{
    public $deck = array();
    public $alphaMap = array(
        "a" => 1,
        "k" => 13,
        "q" => 12,
        "j" => 11
    );
    public $numMap = array(
        "1" => 'a',
        "13" => 'k',
        "12" => 'q',
        "11" => 'j'
    );

    public function newDeck()
    {
        $faces = array('d', 'h', 's', 'c');
        $nums = array('2','3','4','5','6','7','8','9','10','j','q','k','a');

        foreach ($faces as $val) {
            foreach ($nums as $num) {
                $this->deck[] = ['face' => $val, 'num' => $num];
            }
        }
    }

    public function getDeck()
    {
        return $this->deck;
    }

    public function pick()
    {
        shuffle($this->deck);
        $pick = $this->deck[0];
        unset($this->deck[0]);
        return $pick;
    }

    public function score2Cards($playerCard){
        $num = 0;
        $special = "none";
        $mul = 1;
        $score = new stdClass();

        foreach($playerCard as $card){
            if($card["num"] == 'a'){
                $num = (int)$num + 1;
            }else if($card["num"] == 'j' || $card["num"] == 'q' || $card["num"] == 'k'){

            }else{
                $num = (int)$num + (int)$card["num"];
            }
        }
        $num = (int)$num%10;

        if($playerCard[0]["num"] == $playerCard[1]["num"] || $playerCard[0]["face"] == $playerCard[1]["face"]){//deng2
            $special = "deng2";
            $mul = 2;
        }

        if($num == 9){
            $score->num = $num;
            $score->special = "pok9";
            $score->mul = $mul;
            return $score;
        }else if($num == 8){
            $score->num = $num;
            $score->special = "pok8";
            $score->mul = $mul;
            return $score;
        }else{
            $score->num = "$num";
            $score->special = $special;
            $score->mul = $mul;
            return $score;
        }
    }

    public function score3Cards($playerCard){
        $num = 0;
        $special = "none";
        $mul = 1;
        $score = new stdClass();

        $numsAr = array();
        foreach($playerCard as $card){
            if($card["num"] == 'a'){
                $num = (int)$num + 1;
                $numsAr[] = $this->alphaMap[$card["num"]];
            }else if($card["num"] == 'j' || $card["num"] == 'q' || $card["num"] == 'k'){
                $numsAr[] = $this->alphaMap[$card["num"]];
            }else{
                $num = (int)$num + (int)$card["num"];
                $numsAr[] = $card["num"];
            }
        }
        $num = (int)$num%10;

        sort($numsAr);
        $numsStr = "";
        foreach($numsAr as $value){
            if($value == 1){
                $numsStr .= 'a';
            }else if($value == 11 || $value == 12 || $value == 13){
                $numsStr .= $this->numMap[$value];
            }else{
                $numsStr .= $value;
            }
        }

        if($playerCard[0]["face"] == $playerCard[1]["face"] && $playerCard[1]["face"] == $playerCard[2]["face"])
        {//deng3
            $special = "deng3";
            $mul = 3;
        }else if($numsStr == "jjq" || $numsStr == "jjk" || $numsStr == "jqq" || $numsStr == "qqk"
            || $numsStr == "jkk" || $numsStr == "qkk"
        ){//yellow3
            $special = "yellow3";
            $mul = 3;
        }else if($playerCard[0]["num"] == $playerCard[1]["num"] && $playerCard[1]["num"] == $playerCard[2]["num"])
        {//tong3
            $special = "tong3";
            $mul = 5;
        }else if($numsStr == "234" || $numsStr == "345" || $numsStr == "456" || $numsStr == "567"
            || $numsStr == "678" || $numsStr == "789" || $numsStr == "8910" || $numsStr == "910j"
            || $numsStr == "10jq" || $numsStr == "jqk" || $numsStr == "aqk" )
        {//riang
            if($playerCard[0]["face"] == $playerCard[1]["face"] && $playerCard[1]["face"] == $playerCard[2]["face"]){
                $special = "straightFlush";
                $mul = 5;
            }else{
                $special = "riang";
                $mul = 4;
            }
        }

        $score->num = $num;
        $score->special = $special;
        $score->mul = $mul;
        return $score;
    }

}

