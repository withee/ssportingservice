<?php

/**
 * Created by PhpStorm.
 * User: Tingly
 * Date: 3/7/2557
 * Time: 15:59 น.
 */
require 'DBConfig.php';

class TotalStatement
{

    public function clearBill($list, $user_list)
    {
        echo "BET:" . count($list) . "\n";
        echo "USER:" . count($user_list) . "\n";
        $statement_type = 'ball_game';
        $is_exists = file_exists('system_config/file.json');
        if ($is_exists) {
            $config = json_decode(file_get_contents('system_config/file.json'));
        } else {
            $config = json_decode(file_get_contents('../system_config/file.json'));
        }

        $diamond_income_mul = $config->diamond_income;
        $diamond_outcome_mul = $config->diamond_outcome;
        //echo $diamond_outcome_mul.','.$diamond_income_mul;exit;
        $db = DBConfig::getConnection();
        //$db->beginTransaction();
        try {
            $count = 0;
            foreach ($list as $obj) {
                $user = $user_list[$obj['fb_uid']];
                $uid = $user['uid'];
                $result = $obj['result'];
                $bet_id = $obj['bet_id'];
                $before_sgold = $user['sgold'];
                $before_scoin = $user['scoin'];
                $before_diamond = $user['diamond'];
                $sgold_outcome = $obj['sgold_outcome'];
                $sgold_income = $obj['sgold_income'];
                $scoin_outcome = $obj['scoin_outcome'];
                $scoin_income = $obj['scoin_income'];
                $balance_sgold = $before_sgold + $sgold_income - $sgold_outcome;
                $balance_scoin = $before_scoin + $scoin_income;
                $diamond = $before_diamond + ($obj['mul']);
                $balance_diamond = $diamond > 0 ? $diamond : 0;
                $diamond_income = 0;
                $diamond_outcome = 0;
                if ($obj['mul'] > 0) {
                    $diamond_income = $obj['mul'] * $diamond_income_mul;
                } else {
                    $diamond_outcome = abs($obj['mul']) * $diamond_outcome_mul;
                }

//                if($obj['fb_uid']=='1022053875'){
                $count++;
                //echo $diamond_income.','.$diamond_outcome;
                if ($sgold_outcome != $sgold_income) {
                    $update_facebook_user_sql = "update facebook_user set sgold =$balance_sgold,scoin=$balance_scoin,diamond=$balance_diamond where uid =$uid";
                    //echo $update_facebook_user_sql;
                    $db->exec($update_facebook_user_sql);
                }
                $update_statement = "update total_statement set result='$result' where uid=$uid and bet_id=$bet_id and result='wait'";
                $db->exec($update_statement);
                $total_statement_insert_sql = $this->getTotalStatementQuery($uid, $statement_type, "NULL", $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, 'null', $bet_id, '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
                $db->exec($total_statement_insert_sql);
                $user['sgold'] = $balance_sgold;
                $user['scoin'] = $balance_scoin;
                $user['diamond'] = $balance_diamond;
                $user_list[$obj['fb_uid']] = $user;
//                }

            }
            if ($count > 0) {
                //$db->commit();
            } else {

                //$db->rollBack();
            }
        } catch (Exception $e) {
            //$db->rollBack();
            echo $e->getMessage();
        }
    }

    public function reClearBill($list, $user_list)
    {
        $statement_type = 'ball_game';

        $is_exists = file_exists('system_config/file.json');
        if ($is_exists) {
            $config = json_decode(file_get_contents('system_config/file.json'));
        } else {
            $config = json_decode(file_get_contents('../system_config/file.json'));
        }

        $diamond_income_mul = $config->diamond_income;
        $diamond_outcome_mul = $config->diamond_outcome;
        //echo $diamond_outcome_mul.','.$diamond_income_mul;exit;
        $db = DBConfig::getConnection();
        //$db->beginTransaction();
        try {
            $count = 0;
            foreach ($list as $obj) {
                if (array_key_exists($obj['fb_uid'], $user_list)) {
                    $user = $user_list[$obj['fb_uid']];
                    $uid = $user['uid'];
                    $result = $obj['result'];
                    $bet_id = $obj['bet_id'];

                    $before_sgold = $user['sgold'];
                    $before_scoin = $user['scoin'];
                    $before_diamond = $user['diamond'];
                    $sgold_outcome = $obj['sgold_outcome'];
                    $sgold_income = $obj['sgold_income'];
                    $scoin_outcome = $obj['scoin_outcome'];
                    $scoin_income = $obj['scoin_income'];
                    $re_sgold_income = $obj['re_sgold_income'];
                    $re_scoin_income = $obj['re_scoin_income'];
                    $re_sgold_outcome = $obj['re_sgold_outcome'];
                    $re_scoin_outcome = $obj['re_scoin_outcome'];
                    $re_diamond_income = $obj['re_diamond_income'];
                    $re_diamond_outcome = $obj['re_diamond_outcome'];
                    $re_balance_sgold = $before_sgold + $re_sgold_income - $re_sgold_outcome;
                    $re_balance_scoin = $before_scoin + $re_scoin_income - $re_scoin_outcome;
                    $re_balance_diamond = $before_diamond + $re_diamond_income - $re_diamond_outcome;
                    $re_balance_scoin = $re_balance_scoin > 0 ? $re_balance_scoin : 0;
                    $re_balance_diamond = $re_balance_diamond > 0 ? $re_balance_diamond : 0;

                    if ($re_sgold_outcome != $re_sgold_income) {
                        $update_facebook_user_sql = "update facebook_user set sgold =$re_balance_sgold,scoin=$re_balance_scoin,diamond=$re_balance_diamond where uid =$uid";
                        $db->exec($update_facebook_user_sql);
                    }
//                $update_statement = "update total_statement set result='$result' where uid=$uid and bet_id=$bet_id and result='wait'";
//                $db->exec($update_statement);
                    $total_statement_insert_sql = $this->getTotalStatementQuery($uid, $statement_type, "NULL", $before_sgold, $before_scoin, $re_sgold_income, $re_scoin_income, $re_sgold_outcome, $re_scoin_outcome, $re_balance_sgold, $re_balance_scoin, $result, 'null', $bet_id, '', $before_diamond, $re_diamond_income, $re_diamond_outcome, $re_balance_diamond);
                    $db->exec($total_statement_insert_sql);
                    $user['sgold'] = $re_balance_sgold;
                    $user['scoin'] = $re_balance_scoin;
                    $user['diamond'] = $re_balance_diamond;
                    $user_list[$obj['fb_uid']] = $user;

                    $before_sgold = $user['sgold'];
                    $before_scoin = $user['scoin'];
                    $before_diamond = $user['diamond'];
                    $balance_sgold = $before_sgold + $sgold_income - $sgold_outcome;
                    $balance_scoin = $before_scoin + $scoin_income;
                    $diamond = $before_diamond + ($obj['mul']);
                    $balance_diamond = $diamond > 0 ? $diamond : 0;
                    $diamond_income = 0;
                    $diamond_outcome = 0;
                    if ($obj['mul'] > 0) {
                        $diamond_income = $obj['mul'] * $diamond_income_mul;
                    } else {
                        $diamond_outcome = abs($obj['mul']) * $diamond_outcome_mul;
                    }

//                if($obj['fb_uid']=='1022053875'){
                    $count++;
                    //echo $diamond_income.','.$diamond_outcome;
                    $balance_scoin = $balance_scoin > 0 ? $balance_scoin : 0;
                    if ($sgold_outcome != $sgold_income) {
                        $update_facebook_user_sql = "update facebook_user set sgold =$balance_sgold,scoin=$balance_scoin,diamond=$balance_diamond where uid =$uid";
                        //echo $update_facebook_user_sql;
                        $db->exec($update_facebook_user_sql);
                    }
//                $update_statement = "update total_statement set result='$result' where uid=$uid and bet_id=$bet_id";
//                $db->exec($update_statement);
                    $total_statement_insert_sql = $this->getTotalStatementQuery($uid, $statement_type, "NULL", $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, 'null', $bet_id, '', $before_diamond, $diamond_income, $diamond_outcome, $balance_diamond);
                    $db->exec($total_statement_insert_sql);
                    $user['sgold'] = $balance_sgold;
                    $user['scoin'] = $balance_scoin;
                    $user['diamond'] = $balance_diamond;
                    $user_list[$obj['fb_uid']] = $user;
//                }
                }

            }
            $sql_update_bet = "update bet set rescore='N'  where rescore='Y'";
            Yii::app()->db->createCommand($sql_update_bet)->execute();

            if ($count > 0) {
                //$db->commit();
            } else {

                //$db->rollBack();
            }
        } catch (Exception $e) {
            //$db->rollBack();
            echo $e->getMessage();
        }
    }

    public function bet($uid, $bet_id, $scoin_outcome)
    {
        $result = 'wait';
        //$config = json_decode(file_get_contents('system_config/file.json'));
        $user = $this->getUser($uid);
        $before_sgold = $user->sgold;
        $before_scoin = $user->scoin;
        //$scoin_outcome = $config->ball_game_scoin;
        $scoin_income = 0;
        $sgold_income = 0;
        $this->mysqlProcess($uid, $bet_id, $result, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome = 0, $scoin_outcome);
    }

    public function mysqlProcess($uid, $bet_id, $result, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome)
    {
        $statement_type = 'ball_game';
        $db = DBConfig::getConnection();
        $balance_sgold = $before_sgold - $sgold_outcome + $sgold_income;
        $balance_scoin = $before_scoin - $scoin_outcome + $scoin_income;
        $total_statement_sql = $this->getTotalStatementQuery($uid, $statement_type, 'null', $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, 'null', $bet_id, '');
        //echo $total_statement_sql;exit;
        $db->query($total_statement_sql);
        $update_facebook_user_sql = $this->getUpdateFacebookUserQuery($uid, $balance_sgold, $balance_scoin);
        $db->query($update_facebook_user_sql);
        $update_bet = "update bet set bet_sgold=$scoin_outcome where betId=$bet_id";
        $db->query($update_bet);
    }

    public function insertStatement($statement_type, $type_addition, $uid, $result, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome)
    {
        //$statement_type ='ball_game';
        $db = DBConfig::getConnection();
        $balance_sgold = $before_sgold - $sgold_outcome + $sgold_income;
        $balance_scoin = $before_scoin - $scoin_outcome + $scoin_income;
        $total_statement_sql = $this->getTotalStatementQuery($uid, $statement_type, $type_addition, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, 'null', 'null', '');
        //echo $total_statement_sql;exit;
        $db->query($total_statement_sql);
        $update_facebook_user_sql = $this->getUpdateFacebookUserQuery($uid, $balance_sgold, $balance_scoin);
        $db->query($update_facebook_user_sql);
    }

    public function getTotalStatementQuery($uid, $statement_type, $type_addition, $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome, $balance_sgold, $balance_scoin, $result, $game_id, $bet_id, $note, $before_diamond = 0, $diamond_income = 0, $diamond_outcome = 0, $balance_diamond = 0)
    {
        $sql = "insert into total_statement
            (uid,
            statement_type,
            type_addition,
            before_sgold,
            before_scoin,
            sgold_income,
            scoin_income,
            sgold_outcome,
            scoin_outcome,
            balance_sgold,
            balance_scoin,
            result,
            bet_id,
            game_id,
            note,
            before_diamond,
            diamond_income,
            diamond_outcome,
            balance_diamond
            )
            values(
            $uid,
            '$statement_type',
            $type_addition,
            $before_sgold,
            $before_scoin,
            $sgold_income,
            $scoin_income,
            $sgold_outcome,
            $scoin_outcome,
            $balance_sgold,
            $balance_scoin,
            '$result',
            $bet_id,
            $game_id,
            '$note',
            $before_diamond,
            $diamond_income,
            $diamond_outcome,
            $balance_diamond
            )
    ";
        //echo $sql;exit;
        return $sql;
    }

    public function getUser($uid)
    {
        $db = DBConfig::getConnection();
        $sql = "select *from facebook_user where uid=$uid";
        $stmt = $db->query($sql);
        $result = $stmt->fetch(PDO::FETCH_OBJ);
        return $result;
    }

    public function getUpdateFacebookUserQuery($uid, $balance_sgold, $balance_scoin, $balance_diamond = null)
    {
        if ($balance_diamond == null) {
            return "update facebook_user set sgold=$balance_sgold,scoin=$balance_scoin where uid=$uid";
        } else {
            return "update facebook_user set sgold=$balance_sgold,scoin=$balance_scoin,diamond=$balance_diamond where uid=$uid";
        }

    }

    public function todayBonus($uid)
    {
        $sgold_bonus = 0;
        $scoin_bonus = 0;
        $config = json_decode(file_get_contents('system_config/file.json'));
        $free_treasure_sgold_percent = $config->free_treasure_sgold_percent;
        $free_treasure_sgold = $config->free_treasure_sgold;
        $free_treasure_scoin = $config->free_treasure_scoin;
        $free_treasure_scoin_percent = $config->free_treasure_scoin_percent;
        $arr = array(
            'win' => array('min' => 0, 'max' => $free_treasure_sgold_percent),
            'lose' => array('min' => $free_treasure_sgold_percent, 'max' => 100),
        );

        $rnd = rand(1, 100);
        foreach ($arr as $k => $v) {
            if ($rnd > $v['min'] && $rnd <= $v['max']) {
                if ($k == 'win') {
                    $sgold_bonus = $free_treasure_sgold;
                }
            }
        }
        $arr = array(
            'win' => array('min' => 0, 'max' => $free_treasure_scoin_percent),
            'lose' => array('min' => $free_treasure_scoin_percent, 'max' => 100),
        );

        $rnd = rand(1, 100);
        foreach ($arr as $k => $v) {
            if ($rnd > $v['min'] && $rnd <= $v['max']) {
                if ($k == 'win') {
                    $scoin_bonus = $free_treasure_scoin;
                }
            }
        }
        //echo "sgold :" . $sgold_bonus . "\n";
        //echo "scoin :" . $scoin_bonus . "\n";
        if ($sgold_bonus > 0 || $scoin_bonus > 0) {
            $db = DBConfig::getConnection();
            $sql = "select *from facebook_user where uid=$uid";
            $row = $db->query($sql)->fetch(PDO::FETCH_OBJ);
            $before_sgold = $row->sgold;
            $before_scoin = $row->scoin;

            if ($sgold_bonus > 0) {
                $sgold_income = $sgold_bonus;
                $scoin_income = 0;
                $sgold_outcome = 0;
                $scoin_outcome = 0;
                $before_sgold += $sgold_bonus;
                $this->insertStatement('today_bonus', "'sgold_bonus'", $uid, 'success', $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome);
            }
            if ($scoin_bonus > 0) {
                $sgold_income = 0;
                $scoin_income = $scoin_bonus;
                $sgold_outcome = 0;
                $scoin_outcome = 0;
                $this->insertStatement('today_bonus', "'scoin_bonus'", $uid, 'success', $before_sgold, $before_scoin, $sgold_income, $scoin_income, $sgold_outcome, $scoin_outcome);
            }
        }
    }

    public function continueLoginBonus($uid)
    {
        $config = json_decode(file_get_contents('system_config/file.json'));
        $db = DBConfig::getConnection();
        $sql = "select *,datediff(current_date,last_login_date) as datediff from facebook_user where uid=$uid ";
        $row = $db->query($sql)->fetch(PDO::FETCH_OBJ);
        $newlogin = FALSE;
        if ($row) {
            //echo "dateDiff:" . $row->datediff . "\n";
            if ($row->datediff > 0) {
                $newlogin = TRUE;
            }
            if ($row->datediff == null || $row->datediff == 1) {
                $login_day_count = $row->login_day_count;
                $login_sgold_array = $config->login_sgold;
                $login_scoin_array = $config->login_scoin;
                if ($login_day_count >= 7) {
                    $login_day_count--;
                }
                $sgold_income = $login_sgold_array[$login_day_count];
                $scoin_income = $login_scoin_array[$login_day_count];
                $login_day_count++;
                $before_sgold = $row->sgold;
                $before_scoin = $row->scoin;
                $login_day_count = (int)$login_day_count > 7 ? 7 : (int)$login_day_count;
                $this->insertStatement('today_bonus', "'login$login_day_count'", $uid, 'success', $before_sgold, $before_scoin, $sgold_income, $scoin_income, 0, 0);
                $sql_update = "update facebook_user set login_day_count=$login_day_count ,last_login_date=current_date WHERE uid=$uid";
                $db->query($sql_update);
            } else if ($row->datediff > 1) {
                $login_day_count = 0;
                $login_sgold_array = $config->login_sgold;
                $login_scoin_array = $config->login_scoin;
                $sgold_income = $login_sgold_array[$login_day_count];
                $scoin_income = $login_scoin_array[$login_day_count];
                $login_day_count++;
                $before_sgold = $row->sgold;
                $before_scoin = $row->scoin;
                $login_day_count = $login_day_count > 7 ? 7 : $login_day_count;
                $this->insertStatement('today_bonus', "'login$login_day_count'", $uid, 'success', $before_sgold, $before_scoin, $sgold_income, $scoin_income, 0, 0);
                $sql_update = "update facebook_user set login_day_count=$login_day_count ,last_login_date=current_date WHERE uid=uid";
                $db->query($sql_update);
            }
        }
        return $newlogin;
    }

    public function resetSgold($uid)
    {
        $config = json_decode(file_get_contents('../system_config/file.json'));
        $db = DBConfig::getConnection();
        $sql = "select *,datediff(current_date,last_reset_sgold_date) as datediff from facebook_user where uid=$uid ";
        $row = $db->query($sql)->fetch(PDO::FETCH_OBJ);
        if (($row->datediff == null || $row->datediff >= $config->reset_sgold_day) && $row->sgold <= $config->play_sgold_limit) {
            $this->insertStatement('reset_sgold', "NULL", $uid, 'success', $row->sgold, $row->scoin, abs($row->sgold), 0, 0, 0);
            $sql = "update facebook_user set last_reset_sgold_date=current_date where uid=$uid";
            $db->query($sql);
            //echo 1;
            return true;
        } else {
            //echo 2;
            return false;
        }
    }

}
